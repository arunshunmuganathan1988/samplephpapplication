ManageLeadsController.$inject=['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'DTDefaultOptions', '$window','$rootScope'];
 function ManageLeadsController ($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, DTDefaultOptions, $window,$rootScope) {
        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $scope.leadsdashboard = true;
            $scope.enableimportcsv = false;
            $scope.recordsmissing_rows = '';
            $scope.include_campaign_email = 'Y';
            $rootScope.activepagehighlight = 'leads';
            $scope.upgrade_status = $localStorage.upgrade_status; // for inactive studio-registration block
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            $scope.resetsubtabbarclass = function () {
                $('#li-leadsactive').addClass('event-list-title').removeClass('active-event-list');
                $('#li-leadsenrolled').addClass('event-list-title').removeClass('active-event-list');
                $('#li-leadsnotinterested').addClass('event-list-title').removeClass('active-event-list');
            };
            $scope.resetsubtabbarclasssource = function () {
                $('#li-pi').addClass('event-list-title').removeClass('active-event-list');
                $('#li-ls').addClass('event-list-title').removeClass('active-event-list');
            };
            
            $scope.clearLeadFields = function(){ //kamal
                //Program Interest clear value
                $scope.leadsselectedpicolindex = "";
                $scope.piview = true;
                $scope.editpiview = false;
                $scope.showleadspiview = false;
                $scope.leadsaddnewfieldvaluepi = "";
                //lead source clear value
                $scope.leadsselectedlscolindex = "";
                $scope.lsview = true;
                $scope.editlsview = false;
                $scope.showleadslsview = false;
                $scope.leadsaddnewfieldlsvalue = "";                
            }
            
            $scope.leadsdashboardview = function () {
                $scope.clearLeadFields();
                $scope.leadsdashboard = true;
                $scope.leads = false;
                $scope.leadssettings = false;
                $scope.resettabbarclass();
                $('#li-dashboard').addClass('active-event-list').removeClass('event-list-title');
                $('.leadstab-text-1').addClass('greentab-text-active');
                $scope.leadsdashboardTable();
            };
            $scope.leadsview = function () {
                $scope.clearLeadFields();
                $scope.leadsdashboard = false;
                $scope.leads = true;
                $scope.leadssettings = false;
                $scope.resettabbarclass();
                $('#li-leads').addClass('active-event-list').removeClass('event-list-title');
                $('.leadstab-text-2').addClass('greentab-text-active');
                if($localStorage.leadspagefrom !== 'enrolled' && $localStorage.leadspagefrom !== 'notinterested'){ 
                    $scope.active('1');
                }
            };
            $scope.leadssettingsview = function () {
                $scope.clearLeadFields();
                $scope.leadsdashboard = false;
                $scope.leads = false;
                $scope.leadssettings = true;
                $scope.resettabbarclass();
                $('#li-leadssettings').addClass('active-event-list').removeClass('event-list-title');
                $('.leadstab-text-3').addClass('greentab-text-active');
                $scope.getpiandlidetails();
                $scope.programinteresttab();
            };
            //leads dashboard left right icon function  
            $scope.netsales_flag = 0;
            $scope.conversion_flag = 0;
            $scope.leadsperiod = 'M';

            $scope.leads_dashboard_flag = function ($flag) {
                if ($flag === 'N') {
                    $scope.netsales_flag = $scope.netsales_flag - 1;
                    $scope.leadsdashboardTable($scope.netsales_flag);
                } else if ($flag === 'P') {
                    $scope.netsales_flag = $scope.netsales_flag + 1;
                    $scope.leadsdashboardTable($scope.netsales_flag);
                } else if ($flag === 'F') {
                    $scope.conversion_flag = $scope.conversion_flag - 1;
                    $scope.leadsdashboardTable($scope.conversion_flag);
                } else if ($flag === 'O') {
                    $scope.conversion_flag = $scope.conversion_flag + 1;
                    $scope.leadsdashboardTable($scope.conversion_flag);
                }
            };
            //leads monthly/yearly dashboard view
            $scope.leads_sales_period_selection = function (leadsperiod) {
                if (leadsperiod === 'M') {
                    $scope.netsales_flag = 0;
                    $scope.leadsperiod = 'M';
                    $scope.leadsdashboardTable();

                } else if (leadsperiod === 'A') {
                    $scope.netsales_flag = 0;
                    $scope.leadsperiod = 'A';
                    $scope.leadsdashboardTable();
                }
            };
            //leads dashboard server call
            $scope.leadsdashboardTable = function () {
                //Loads data and ToTal Amount In All the Tabs of Dashboard Page//
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getleadsdashboard',
                    data: {
                        "company_id": $localStorage.company_id,
                        "l_flag":$scope.netsales_flag,
                        "c_flag":$scope.conversion_flag,
                        "sales_period": $scope.leadsperiod

                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $scope.leadsdashboarddetails = response.data.msg;
                        $scope.leadsleadsourcevalue = $scope.leadsdashboarddetails.month_leadsource;
                        $scope.leadsconversionvalue = $scope.leadsdashboarddetails.month_conversion;
                        $scope.leadstotal = $scope.leadsdashboarddetails.leads;
                        $scope.leadsconversiondetails = $scope.leadsdashboarddetails.conversion;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            $scope.leadsdashboardTable();
            //get pi and li details
            $scope.getpiandlidetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getleadssettings',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.pi_columns  = response.data.msg.program_interest;
                        $scope.ls_columns  = response.data.msg.lead_source;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            //initialcall
            $scope.getpiandlidetails();
            //subtabs
            $scope.active = function (value) {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.activeleadsdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.leadsactiveview = true;
                $scope.leadsenrolledview = false;
                $scope.leadsnotinterestedview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.enrolledstartdate = '';
                $scope.enrolledenddate = '';
                $scope.NIleadsstartdate = '';
                $scope.NIleadsenddate = '';
                $scope.color_green = 'N';
                $scope.resetsubtabbarclass();
                $('#li-leadsactive').addClass('active-event-list').removeClass('event-list-title');
                if(value === '1' || value === 1){
                    $scope.getleadsMember('A');                    
                }
                
                if(!$localStorage.leadspagefrom){
                    $scope.leadsdtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }                
                $localStorage.leadspagefrom = "";                
            };
            $scope.enrolled = function () {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.enrolledleadsdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.leadsactiveview = false;
                $scope.leadsenrolledview = true;
                $scope.leadsnotinterestedview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.enrolledstartdate = '';
                $scope.enrolledenddate = '';
                $scope.NIleadsstartdate = '';
                $scope.NIleadsenddate = '';
                $scope.color_green = 'N';
                $scope.resetsubtabbarclass();
                $('#li-leadsenrolled').addClass('active-event-list').removeClass('event-list-title');
                $scope.getleadsMember('E');
                if(!$localStorage.leadspagefrom){
                    $scope.leadsdtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }              
                $localStorage.leadspagefrom = "";
            };
            $scope.notinterested = function () {
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.change_made = $scope.search_value = '';
                $scope.notinteresteddays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.leadsactiveview = false;
                $scope.leadsenrolledview = false;
                $scope.leadsnotinterestedview = true;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.enrolledstartdate = '';
                $scope.enrolledenddate = '';
                $scope.NIleadsstartdate = '';
                $scope.NIleadsenddate = '';
                $scope.color_green = 'N';
                $scope.resetsubtabbarclass();
                $('#li-leadsnotinterested').addClass('active-event-list').removeClass('event-list-title');
                $scope.getleadsMember('NI');
                if(!$localStorage.leadspagefrom){
                    $scope.leadsdtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.leadspagefrom = "";
            };

            $scope.programinteresttab = function () {
                $scope.clearLeadFields();
                $scope.programinterestview = true;
                $scope.leadsourceview = false;
                $scope.resetsubtabbarclasssource();
                $('#li-pi').addClass('active-event-list').removeClass('event-list-title');
            };
            $scope.sourcetab = function () {
                $scope.clearLeadFields();
                $scope.programinterestview = false;
                $scope.leadsourceview = true;
                $scope.resetsubtabbarclasssource();
                $('#li-ls').addClass('active-event-list').removeClass('event-list-title');
            };

            $scope.dateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            };
            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = "";
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
            $scope.clearSelectedList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                if ($scope.clearfromindividual === true){//clear if modal open from individual
                    $scope.selectedData = [];
                }
                $scope.clearfromindividual = false;
            };
            $scope.clearmodal = function () {
                $scope.pifromaddmodal = '';
                $scope.lsfromaddmodal = '';
                $scope.buyerfname = '';
                $scope.buyerlname = '';
                $scope.email = '';
                $scope.mobile = '';
                $scope.participantfname = '';
                $scope.participantlname = '';
            };
            $scope.$on('$viewContentLoaded', function () {
                $('#activeleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#activeleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#enrolledleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#enrolledleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#notinterestedleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#notinterestedleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.leadsclearDateFields = function () {
                $scope.activestartdate = $scope.activeenddate = $scope.enrolledstartdate = $scope.enrolledenddate = $scope.NIleadsstartdate = $scope.NIleadsenddate = '';

                $('#activeleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#activeleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#enrolledleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#enrolledleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#notinterestedleadsstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#notinterestedleadsenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            };
            $scope.leadsdayselection = function (value) {
                var daystype;
                this.activestartdate = '';
                this.activeenddate = '';
                this.enrolledstartdate = '';
                this.enrolledenddate = '';
                this.NIleadsstartdate = '';
                this.NIleadsenddate = '';

                if (value === 'A') {
                    daystype = this.activeleadsdays;
                } else if (value === 'E') {
                    daystype = this.enrolledleadsdays;
                } else if (value === 'NI') {
                    daystype = this.notinteresteddays;
                }

                if (daystype === '1' || daystype === '2' || daystype === '3' || daystype === '5') {
                    $scope.leadsclearDateFields();
                    $scope.leadsfilterByDate(value, daystype);
                } else {
                    $scope.leadsclearDateFields();
                    // DATE RANGE SELECTION FILTER
                }
            };
            $scope.leadsfilterByDate = function (value, daystype) {
                $scope.current_tab = value;
                $scope.leads_array = [];

//               $('#progress-full').show();
                if (value === 'A') { // FILTERS DATA FOR ACTIVE TAB //
                    value = 'A';
                    $scope.activeleadsdays = daystype;
                    if ($scope.activeleadsdays === '4') {
                        if (this.activestartdate === '' || this.activestartdate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_startdate_limit = $scope.dateformatfilter(this.activestartdate);
                        }

                        if (this.activeenddate === '' || this.activeenddate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_enddate_limit = $scope.dateformatfilter(this.activeenddate);
                        }
                    } else {
                        $scope.leads_startdate_limit = '';
                        $scope.leads_enddate_limit = '';
                    }
                }

                if (value === 'E') { // FILTERS DATA FOR ON HOLD TAB //
                    value = 'E';
                    $scope.enrolledleadsdays = daystype;
                    if ($scope.enrolledleadsdays === '4') {
                        if (this.enrolledstartdate === '' || this.enrolledstartdate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_startdate_limit = $scope.dateformatfilter(this.enrolledstartdate);
                        }

                        if (this.enrolledenddate === '' || this.enrolledenddate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_enddate_limit = $scope.dateformatfilter(this.enrolledenddate);
                        }
                    } else {
                        $scope.leads_startdate_limit = '';
                        $scope.leads_enddate_limit = '';
                    }
                }
                if (value === 'NI') { // FILTERS DATA FOR CANCELLED TAB //
                    value = 'NI';
                    $scope.notinteresteddays = daystype;

                    if ($scope.notinteresteddays === '4') {
                        if (this.NIleadsstartdate === '' || this.NIleadsstartdate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_startdate_limit = $scope.dateformatfilter(this.NIleadsstartdate);
                        }

                        if (this.NIleadsenddate === '' || this.NIleadsenddate === undefined) {
                            $scope.leads_startdate_limit = '';
                            $scope.leads_enddate_limit = '';
                        } else {
                            $scope.leads_enddate_limit = $scope.dateformatfilter(this.NIleadsenddate);
                        }
                    } else {
                        $scope.leads_startdate_limit = '';
                        $scope.leads_enddate_limit = '';
                    }
                }
                $scope.getleadsdetailsbyFilter(value,daystype);
            };
            $scope.leadsclearfilter = function (type) { // CLEAR THE APPLIED FILTER //   
                if (type === 'A') {
                    this.activestartdate = '';
                    this.activeenddate = '';
                    this.activeleadsdays = '5';
                    $scope.active('1');
                } else if (type === 'E') {
                    this.enrolledstartdate = '';
                    this.enrolledenddate = '';
                    this.enrolledleadsdays = '5';
                    $scope.enrolled();
                } else if (type === 'NI') {
                    this.NIleadsstartdate = '';
                    this.NIleadsenddate = '';
                    this.notinteresteddays = '5';
                    $scope.notinterested();
                }
                $scope.clearSelectedList();
            };
            $scope.catchactivesdate = function () {
                if ($scope.activestartdate !== '') {
                    $('#activeleadsenddate').datepicker('setStartDate', $scope.activestartdate);
                } else {
                    $('#activeleadsenddate').datepicker('setStartDate', null);
                    $scope.activeenddate = '';
                }
            };
            $scope.catchactiveedate = function () {
                if ($scope.activeenddate !== '') {
                    $('#activeleadsstartdate').datepicker('setEndDate', $scope.activeenddate);
                } else {
                    $('#activeleadsstartdate').datepicker('setEndDate', null);
                }
            };
            $scope.catchenrolledsdate = function () {
                if ($scope.enrolledstartdate !== '') {
                    $('#enrolledleadsenddate').datepicker('setStartDate', $scope.enrolledstartdate);
                } else {
                    $('#enrolledleadsenddate').datepicker('setStartDate', null);
                    $scope.enrolledenddate = '';
                }
            };
            $scope.catchenrollededate = function () {
                if ($scope.enrolledenddate !== '') {
                    $('#enrolledleadsstartdate').datepicker('setEndDate', $scope.enrolledenddate);
                } else {
                    $('#enrolledleadsstartdate').datepicker('setEndDate', null);
                }
            };
            $scope.catchNIStartDate = function () {
                if ($scope.NIleadsstartdate !== '') {
                    $('#notinterestedleadsenddate').datepicker('setStartDate', $scope.NIleadsstartdate);
                } else {
                    $('#notinterestedleadsenddate').datepicker('setStartDate', null);
                    $scope.NIleadsenddate = '';
                }
            };
            $scope.catchNIEndDate = function () {
                if ($scope.NIleadsenddate !== '') {
                    $('#notinterestedleadsstartdate').datepicker('setEndDate', $scope.NIleadsenddate);
                } else {
                    $('#notinterestedleadsstartdate').datepicker('setEndDate', null);
                }
            };
            //settings starts
            //PROGRAM INTEREST
            $scope.showleadspi = function (){
                $scope.showleadspiview = true;
            };
            $scope.cancelShowleadspi = function (){
                $scope.showleadspiview = false;
                $scope.leadsaddnewfieldvaluepi = "";
            };
            $scope.editleadspicolm = function (leads_pi_column, index) {
                this.leadspi_column_name = leads_pi_column;
                $scope.leadsselectedpicolindex = index;
                $scope.piview = false;
                $scope.editpiview = false;
            };
            $scope.cancelleadspiChange = function(leads_pi_column){
                this.leadspi_column_name = leads_pi_column;
                $scope.leadsselectedpicolindex = "";
                $scope.piview = true;
                $scope.editpiview = false;
            };
            //lead source
            $scope.showleadsls = function (){
                $scope.showleadslsview = true;
            };
            $scope.cancelShowleadsls = function (){
                $scope.showleadslsview = false;
                $scope.leadsaddnewfieldlsvalue = "";
            };
            $scope.editleadslscolm = function (leads_ls_column, index) {
                this.leadsls_column_name = leads_ls_column;
                $scope.leadsselectedlscolindex = index;
                $scope.lsview = false;
                $scope.editlsview = false;
            };
            $scope.cancelleadslsChange = function(leads_ls_column){
                this.leadsls_column_name = leads_ls_column;
                $scope.leadsselectedlscolindex = "";
                $scope.lsview = true;
                $scope.editlsview = false;
            };
            $scope.leadsremoveDrag = function (event) {
                $(event.currentTarget).prop("draggable", false);
                $(event.currentTarget.closest('li')).prop("draggable", false);
            };
            $scope.leadsaddDrag = function (event) {
                $(event.currentTarget).prop("draggable", true);
                $(event.currentTarget.closest('li')).prop("draggable", true);
            };
             //open lead support link in new tab
            $scope.openleadsupport = function(){
              window.open("https://intercom.help/mystudio/lead-management",'_blank'); 
            };
            //add new member/imort starts
            $scope.openaddmembermodal = function(){
                $('#addleads-modal').modal('show');
            };
            $scope.openindividualaddmembermodal = function(){
                $scope.include_campaign_email = 'Y';
                $('#addleadsindividual-modal').modal('show');
                
            };
            $scope.openimportmodal = function(){
                $scope.include_campaign_email = 'Y';
                $('#import-modal').modal('show');
            };
            //check and delete settings fields
            $scope.checkparticipantexist = function(type,id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'checkparticipantexist',
                    data: {
                        "id": id,
                        "type":type,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $('#progress-full').hide();
                        $scope.to_be_deleted = response.data.to_be_deleted;
                        if (response.data.msg.participant_exist === 'Y') {
                            $('#progress-full').hide();
                            if (response.data.msg.type === 'pi') {
                                $scope.deletetype = response.data.msg.type;
                                $scope.deletelist = response.data.msg.program_interest;
                                $scope.moverecordto = $scope.deletelist[0].id;
                                $('#deletefield-modal').modal('show');
                            } else if (response.data.msg.type === 'ls') {
                                $scope.deletetype = response.data.msg.type;
                                $scope.deletelist = response.data.msg.lead_source;
                                $scope.moverecordto = $scope.deletelist[0].id;
                                $('#deletefield-modal').modal('show');
                            }
                        }else{
                            $scope.pi_columns = response.data.msg.program_interest;
                            $scope.ls_columns = response.data.msg.lead_source;
                            $("#leadsmessageModal").modal('show');
                            $("#leadsmessagetitle").text('Message');
                            $("#leadsmessagecontent").text(response.data.msgs);
                        }
                    }else if(response.data.status === 'Expired'){
                         $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            //delete and move
            $scope.moveanddeletefield = function(id){
              $('#deletefield-modal').modal('show');
              $('#progress-full').show();
              $http({
                    method: 'POST',
                    url: urlservice.url+'moveanddeletefield',
                    data: {
                        "id": id,
                        "type":$scope.deletetype,
                        "to_be_deleted":$scope.to_be_deleted,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.to_be_deleted = "";
                        $scope.pi_columns = response.data.msg.program_interest;
                        $scope.ls_columns = response.data.msg.lead_source;
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msgs);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            //settings drag and drop
            $scope.leadspiCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_leads_id = $scope.pi_columns[ind].id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.pi_columns.length;i++){ 
                            if($scope.pi_columns[i].id === $scope.start_leads_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_pi_id, pi_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_pi_id = $scope.pi_columns[$scope.new_location_id].id;
                                pi_sort_order = parseFloat($scope.pi_columns[$scope.new_location_id + 1].category_sort_order) / 2;
                                $scope.sortsettinglist('pi',sort_pi_id, pi_sort_order);
                            } else if ($scope.new_location_id === $scope.pi_columns.length - 1) {
                                sort_pi_id = $scope.pi_columns[$scope.new_location_id].id;
                                pi_sort_order = parseFloat($scope.pi_columns[$scope.new_location_id - 1].category_sort_order) + 1;
                                $scope.sortsettinglist('pi',sort_pi_id, pi_sort_order);
                            } else {
                                sort_pi_id = $scope.pi_columns[$scope.new_location_id].id;
                                previous_sort_order = parseFloat($scope.pi_columns[$scope.new_location_id + 1].category_sort_order);
                                next_sort_order = parseFloat($scope.pi_columns[$scope.new_location_id - 1].category_sort_order);
                                pi_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortsettinglist('pi',sort_pi_id, pi_sort_order);
                            }
                        }else{
                           $('#progress-full').hide();
                        }

                    }, 1000);
                }
            };
            $scope.leadslsCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_leads_id = $scope.ls_columns[ind].id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.ls_columns.length;i++){ 
                            if($scope.ls_columns[i].id === $scope.start_leads_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_ls_id, ls_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_ls_id = $scope.ls_columns[$scope.new_location_id].id;
                                ls_sort_order = parseFloat($scope.ls_columns[$scope.new_location_id + 1].category_sort_order) / 2;
                                $scope.sortsettinglist('ls',sort_ls_id, ls_sort_order);
                            } else if ($scope.new_location_id === $scope.ls_columns.length - 1) {
                                sort_ls_id = $scope.ls_columns[$scope.new_location_id].id;
                                ls_sort_order = parseFloat($scope.ls_columns[$scope.new_location_id - 1].category_sort_order) + 1;
                                $scope.sortsettinglist('ls',sort_ls_id, ls_sort_order);
                            } else {
                                sort_ls_id = $scope.ls_columns[$scope.new_location_id].id;
                                previous_sort_order = parseFloat($scope.ls_columns[$scope.new_location_id + 1].category_sort_order);
                                next_sort_order = parseFloat($scope.ls_columns[$scope.new_location_id - 1].category_sort_order);
                                ls_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortsettinglist('ls',sort_ls_id, ls_sort_order);
                            }
                        }else{
                           $('#progress-full').hide();
                        }

                    }, 1000);
                }
            };
            $scope.sortsettinglist = function(type,id,sort_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'sortsettingslist',
                    data: {
                        "sort_id": sort_id,
                        "id": id,
                        "type":type,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $('#progress-full').hide();
                        $scope.pi_columns  = response.data.msg.program_interest;
                        $scope.ls_columns  = response.data.msg.lead_source;
                    }else if(response.data.status === 'Expired'){
                         $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            //add/update pi name
            $scope.updateleadspi  = function (type,id) {
                if(type === 'add'){
                    $scope.leadspiid = "";
                    $scope.piname = $scope.leadsaddnewfieldvaluepi;
                }else if(type === 'update'){
                    $scope.piname = this.leadspi_column_name;
                    $scope.leadspiid = id;
                }
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateprograminterest',
                    data: {
                        "leads_pi_id" : $scope.leadspiid,
                        "company_id": $localStorage.company_id,
                        "pi_name": $scope.piname,
                        "type":type
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.pi_columns  = response.data.msg.program_interest;
                        $scope.ls_columns  = response.data.msg.lead_source;
                        $scope.cancelShowleadspi();
                        $scope.leadsselectedpicolindex = "";
                        $scope.piview = true;
                        $scope.editpiview = false;
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        if(type === 'add'){
                            $("#leadsmessagecontent").text("Program Interest field successfully added");
                        }else{
                            $("#leadsmessagecontent").text("Program Interest field successfully updated");
                        }                        
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            //add/update leadsource
            $scope.updateleadsls  = function (type,id) {
                if(type === 'add'){
                    $scope.leadslsid = "";
                    $scope.lsname = $scope.leadsaddnewfieldlsvalue;
                }else if(type === 'update'){
                    $scope.leadslsid = id;
                    $scope.lsname = this.leadsls_column_name;
                }
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateleadsource',
                    data: {
                        "leads_ls_id" : $scope.leadslsid,
                        "company_id": $localStorage.company_id,
                        "ls_name": $scope.lsname,
                        "type":type
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.pi_columns  = response.data.msg.program_interest;
                        $scope.ls_columns  = response.data.msg.lead_source;
                        $scope.cancelShowleadsls();
                        $scope.leadsselectedlscolindex = "";
                        $scope.lsview = true;
                        $scope.editlsview = false;
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        if(type === 'add'){
                            $("#leadsmessagecontent").text("Lead source field successfully added");
                        }else{
                            $("#leadsmessagecontent").text("Lead source field successfully updated");
                        }
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            $scope.leadstoggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
            };
          
           $scope.leadstoggleOne= function (selectedItems) {
                var check = 0;
                $scope.maintainselected = selectedItems;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        } else {
                            $scope.selectedData.push({"id": id});
                            $scope.color_green = 'Y';
                        }

                    }
                }
                
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
            };
            
            //without filter datatable ajax
            $scope.getleadsMember = function(tab){
                 $scope.current_tab=tab;
                    DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="leadstoggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
                if ($scope.current_tab === 'A') {
                   $scope.leadsdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "leads_status_tab": tab,
                                 "leads_days_type": $scope.activeleadsdays
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'active'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance = {};
                }if ($scope.current_tab === 'E') {
                   $scope.leadsdtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "leads_status_tab": tab,
                                 "leads_days_type": $scope.enrolledleadsdays
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'enrolled'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance1 = {};
                }if ($scope.current_tab === 'NI') {
                   $scope.leadsdtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "leads_status_tab": tab,
                                 "leads_days_type": $scope.notinteresteddays
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'notinterested'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance2 = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })
            };
            
            //with filter
            $scope.getleadsdetailsbyFilter = function(value, daystype){
                $scope.current_tab=value;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="leadstoggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
                if ($scope.current_tab === 'A') {
                    $scope.leadsdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                    "company_id": $localStorage.company_id,
                                    "leads_status_tab": $scope.current_tab,
                                    "leads_days_type": daystype,
                                    "leads_startdate_limit": $scope.leads_startdate_limit,
                                    "leads_enddate_limit": $scope.leads_enddate_limit
                                },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'active'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance = {};
                }if ($scope.current_tab === 'E') {
                   $scope.leadsdtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                               $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                    "leads_status_tab": $scope.current_tab,
                                    "leads_days_type": daystype,
                                    "leads_startdate_limit": $scope.leads_startdate_limit,
                                    "leads_enddate_limit": $scope.leads_enddate_limit
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'enrolled'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance1 = {};
                }if ($scope.current_tab === 'NI') {
                   $scope.leadsdtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'leadscustomers',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.leadslistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                    "leads_status_tab": $scope.current_tab,
                                    "leads_days_type": daystype,
                                    "leads_startdate_limit": $scope.leads_startdate_limit,
                                    "leads_enddate_limit": $scope.leads_enddate_limit
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.adminlogout();
                                } else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [10, 'desc'])
                    $scope.leadsdtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="leadstoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                                DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="leadsIndividualDetail(\'' + full.id + '\' ,' + "'leads'" + ',' + full.leads_id + ',' + "'notinterested'" + ');">' + full.buyer_name + '</a>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full, meta) {
                                    if (full.participant_email !== "") {
                                        if (full.bounce_flag === 'N'){
                                            if (full.email_subscription_status === 'U') {
                                                return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                            } else {
                                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendleadsindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                            }
                                        }else{
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                        }
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '>' + '' + '</a>';
                                    }
                                }),
                                 DTColumnBuilder.newColumn('leads_status').withTitle('Status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leads_status + '</span>';
                                }),
                                DTColumnBuilder.newColumn('program_interest').withTitle('Program Interest').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.program_interest + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Source').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.lead_source + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_first_name').withTitle('Participant First Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_first_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_last_name').withTitle('Participant Last Name').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_last_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('opt_in_date').withTitle('Opt in Date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.opt_in_date + '</span>';
                                })
                    ];
                   $scope.dtInstance2 = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })
            };
            //add new member from individual modal
            $scope.addnewleadsparticipant  = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'addLeadsMember',
                    data: {
                        "company_id": $localStorage.company_id,
                        "pi_id":$scope.pifromaddmodal,
                        "ls_id":$scope.lsfromaddmodal,
                        "buyer_first_name":$scope.buyerfname,
                        "buyer_last_name":$scope.buyerlname,
                        "email":$scope.email,
                        "phone":$scope.mobile,
                        "participant_first_name":$scope.participantfname,
                        "participant_last_name":$scope.participantlname,
                        "include_campaign_email":$scope.include_campaign_email
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        if($scope.activeleadsdays === '5' && $scope.leadsactiveview == true){
                            $scope.active('2');
                            $scope.leadsdayselection('A'); 
                        }else{
                            $scope.active('1');
                            setTimeout(function(){
                            document.getElementById('clear1').click(); 
                        },500);  
                        }
                        $scope.clearmodal();
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            //group mail/csv
            $scope.leadssendGroupMsg = function (tab,type) { 
                $scope.current_tab = tab;
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 || $scope.all_select_flag==='Y') {
                    if ($scope.message_type === 'mail') {
                        $("#leadsgrpemailmessagecontModal").modal('show');
                        $("#leadsgrpemailmessageconttitle").text('Send Email');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if ($scope.message_type === 'csv') {
                        $("#leadsgrpcsvdownloadModal").modal('show');
                        $("#leadsgrpcsvdownloadtitle").text('Message');
                        $("#leadsgrpcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
                } else {
                    var x = document.getElementById("leadssnackbar")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            };
            //csv export
            $scope.leadsexportcheckedmembersexcel = function () {
                var selected_days_type;
                if($scope.current_tab === 'A'){
                    selected_days_type = $scope.activeleadsdays;
                }else if($scope.current_tab === 'E'){
                    selected_days_type = $scope.enrolledleadsdays;
                }else if($scope.current_tab === 'NI'){
                    selected_days_type = $scope.notinteresteddays;
                }
                
             function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createleadsSelectonCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_leads_list": $scope.selectedData,
                        "all_select_flag": $scope.all_select_flag,
                        "leads_status": $scope.current_tab,
                        "leads_days_type": selected_days_type,
                        "leads_startdate_limit": $scope.leads_startdate_limit,
                        "leads_enddate_limit": $scope.leads_enddate_limit,
                        "search":$scope.searchTerm,
                        "from":"leads",
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    $scope.clearSelectedList();
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.msg);                        
                        return false;
                    } else {
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text("Participant Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "leads_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearSelectedList();
                    $('#progress-full').hide();
                    $("#leadsmessageModal").modal('show');
                    $("#leadsmessagetitle").text('Message');
                    $("#leadsmessagecontent").text(msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#leadsmessageModal").modal('show');
                    $("#leadsmessagetitle").text('Message');
                    $("#leadsmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            //navigate leads detail
            $scope.leadsIndividualDetail = function ( leads_reg_id, pagename, leads_id,page) {
                $localStorage.currentLeadsregid = leads_reg_id;
                $localStorage.currentLeads_id = leads_id;
                $localStorage.leadspagefrom = page;
                $location.path('/leadsdetail');
            };
            
            //individual mail
            $scope.sendleadsindividualpush = function (selectedMember, type) { // INDIVIDUAL PUSH MESSAGE
                $scope.clearfromindividual = true;
                $scope.selectAll = false;
                $scope.leadstoggleAll(false,$scope.maintainselected);
                $scope.message_type = type;
                $scope.selectedData = [];
//                $scope.clearSelectedList();
                $scope.selectedData.push({"id":selectedMember});
                $scope.grpbuttonText = "Send"; 
                if (type === 'mail') {
                   $("#leadsgrpemailmessagecontModal").modal('show');
                    $("#leadsgrpemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            //grp mail
            $scope.leadssendGroupEmailPush = function () {
                $('#progress-full').show();
                var selected_days_type;
                if($scope.current_tab === 'A'){
                    selected_days_type = $scope.activeleadsdays;
                }else if($scope.current_tab === 'E'){
                    selected_days_type = $scope.holdleadsdays;
                }else if($scope.current_tab === 'C'){
                    selected_days_type = $scope.cancelleadsdays;
                }else if($scope.current_tab === 'D'){
                    selected_days_type = $scope.didntstartleadsdays;
                }
                var msg = '';
                var subject,unsubscribe_email = '';
                if ($scope.message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if ($scope.message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if($scope.message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendleadsGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": $scope.message_type,
                        "subject": subject,
                        "message": msg,
                        "selected_leads_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "all_select_flag": $scope.all_select_flag,
                        "leads_status": $scope.current_tab,
                        "leads_days_type": selected_days_type,
                        "leads_startdate_limit": $scope.leads_startdate_limit,
                        "leads_enddate_limit": $scope.leads_enddate_limit,
                        "search":$scope.searchTerm,
                        "from" : "leads",
                        "unsubscribe_email":unsubscribe_email

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#leadsgrpemailmessagecontModal").modal('hide');
                        $("#leadsgrpemailmessageconttitle").text('');
                    if (response.data.status === 'Success') {                        
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                          $scope.clearSelectedList();
                         $("#leadsgrpemailmessagecontModal").modal('hide');
                        $("#leadsgrpemailmessageconttitle").text('');
                        $scope.handleFailure(response.data.msg);
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.leadsCSV = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getleadCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    var response = '';
                    if ('TextDecoder' in window) {
                        // Decode as UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if($scope.isJson(decoder.decode(dataView))===true){
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // Fallback decode as ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if($scope.isJson(decodedString)===true){
                            response = JSON.parse(decodedString);
                        }
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", 'lead' +".csv"); 
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if(msg===''){
                        msg = "CSV file downloaded Successfully";
                    }
                    $('#progress-full').hide();
                    $("#leadsmessageModal").modal('show');
                    $("#leadsmessagetitle").text('Message');
                    $("#leadsmessagecontent").text(msg);
                }).error(function () {
                    $('#progress-full').hide();
                    $("#leadsmessageModal").modal('show');
                    $("#leadsmessagetitle").text('Message');
                    $("#leadsmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            //csv import
            $scope.openAttachment = function(){
              document.getElementById('attachment').click();  
            };
            $scope.changecsvimportfile = function(){
                $scope.leadImportCSV();
            };
            $scope.clearimportmodal = function(){
                $scope.filepreview = "";
                $scope.attachcsvfile = "";
                $scope.enableimportcsv = false;
            };
            
            $scope.leadImportCSV = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'leadImportCSVCheck',
                    data: {
                        "company_id": $localStorage.company_id,
                        "uploadedCSV_file":$scope.filepreview
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $scope.importcsv = $scope.filepreview;
                        $scope.clearimportmodal();
                        if(response.data.msg =='csv can be imported'){
                            $scope.enableimportcsv = true;   
                            $("#leadsmessageModal").modal('show');
                            $("#leadsmessagetitle").text('Message');
                            $("#leadsmessagecontent").text("List loaded successfully");
                        }
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Failed') {
                        $scope.importcsv = $scope.filepreview;
                        $scope.clearimportmodal();
                        if(response.data.msgs === 'Required field is missing at records :'){
                            $scope.enableimportcsv = false;
                            $('#progress-full').hide();
                            $("#leadscsvfailmsgeModal").modal('show');
                            $scope.recordsmissing_rows = response.data.records;
                        }else{
                            $scope.enableimportcsv = false;
                            $('#progress-full').hide();
                            $("#leadsfailuremessageModal").modal('show');
                            $("#leadsfailuremessagetitle").text('Message');
                            $("#leadsfailuremessagecontent").text(response.data.msg);}
                        }else {
                        $scope.enableimportcsv = false;
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
//            final importfrm modal
            $scope.addfromimportmodal = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'ConfirmImportCSV',
                    data: {
                        "company_id": $localStorage.company_id,
                        "uploadedCSV_file": $scope.importcsv,
                        "include_campaign_email": $scope.include_campaign_email
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        if($scope.activeleadsdays === '5' && $scope.leadsactiveview == true){
                            $scope.active('2');
                            $scope.leadsdayselection('A'); 
                        }else{
                            $scope.active('1');
                            setTimeout(function(){
                            document.getElementById('clear1').click(); 
                        },500);  
                        }
                        $('#progress-full').hide();
                        $("#import-modal").modal('hide');
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }; 
            //from leads detail
            if($localStorage.leadspagefrom === 'active'){                
                $scope.leadsview();
            }else if($localStorage.leadspagefrom === 'enrolled'){
                $scope.leadsview();
                setTimeout(function(){
                    $scope.enrolled();
                },100);                 
            }else if($localStorage.leadspagefrom === 'notinterested'){
                $scope.leadsview();
                setTimeout(function(){
                    $scope.notinterested();
                },100); 
            }
            //bounce email
            $scope.showemailerror = function (row, type, data, event, id) {
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.leads_bmail_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.leadslistcontents[row].error;
                $scope.bouncedemail = data;
                $("#leadsemailerrormessageModal").modal('show');
                $("#leadsemailerrormessagetitle").text('Message');
                $("#leadsemailerrormessagecontent").text($scope.errormsg);
            }
            
             $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#leadsemailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                    url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendleadsindividualpush(\'' + $scope.leads_bmail_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
                        $compile(angular.element(document.getElementById('DataTables_Table_leads_1')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_leads_2')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_leads_3')).contents())($scope);
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#leadsfailuremessageModal").modal('show');
                        $("#leadsfailuremessagetitle").text('Message');
                        $("#leadsfailuremessagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };
        } else {
            $location.path('/login');
        }
    }
    module.exports =  ManageLeadsController;
// CSV attach file upload
    