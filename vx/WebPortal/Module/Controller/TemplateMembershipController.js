TemplateMembershipController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter','$rootScope'];
 function TemplateMembershipController($scope, $location, $http, $route, $localStorage, urlservice, $filter,$rootScope) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.activepagehighlight = 'membership';
            $localStorage.templateMembership = '';
            $localStorage.currentpage = 'templatemembership';
            
            $scope.membershiptemplate=function(){    
                  $http({
                    method: 'POST',
                    url: urlservice.url + 'getMembershipTemplate',
                    data: {
                     "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {  
                       $localStorage.templateMembership = $scope.templatemembership = response.data.msg.live;
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        console.log(response.data);
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.membershiptemplate();
            
            $scope.tempcopymembership = function(membership_id){
                $('#progress-full').show(); 
                
                $http({
                method: 'POST',
                url: urlservice.url+'copyMembershipCategory',
                
                data:{
                    'membership_id':membership_id,
                    'copy_membership_type':'Main',
                    'membership_template_flag':'Y',
                    'company_id':$localStorage.company_id
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.redirectedfrmtemplate = true;
                        $location.path('/managemembership');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if(response.data.status === 'Failed'){
                        $scope.handleFailure('Membership category duplicate copy failed to add');
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });  
            };
            
            $scope.addmembership = function () {
                $localStorage.membershippagetype = 'new';
                $localStorage.membershiplocaldata = "";
                $localStorage.membership_details = "";
                $localStorage.isAddedCategoryDetails = "N";
                $localStorage.currentpage = "addmembership";
                $localStorage.preview_membershipcontent = '';
                $location.path("/addmembership");
            };
            
        } else {
            $location.path('/login');
        }
    }
    module.exports =  TemplateMembershipController;







