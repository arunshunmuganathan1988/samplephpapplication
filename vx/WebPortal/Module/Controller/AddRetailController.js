AddRetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', 'retailListDetails', '$filter', '$route', '$timeout','$rootScope','$window'];
function AddRetailController ($scope, $location, $http, $localStorage, urlservice, retailListDetails, $filter, $route, $timeout,$rootScope,$window) {

        if ($localStorage.islogin === 'Y') {
            $window.scrollTo(0, angular.element('#retail-add').offsetTop); 
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'Retail';
            $scope.company_id = $localStorage.company_id;
            $scope.retaildetailsview = true;
            $scope.retailvariantsview = false;
            $scope.taxoption = 'Y';
            $scope.addproduct = true;
            $scope.addretailcategoryview = false;
            $scope.afterpublish = false;
            $scope.cropper = {};
            $scope.cropper.categorySourceImage = $scope.cropper.productSourceImage = null;
            $scope.cropper.croppedCategoryImage = $scope.cropper.croppedProductImage = null;
            $scope.retailcroppedCategoryImage = $scope.retailcroppedProductImage = "";
            $scope.retailproducturl = $scope.retailcroppedProductImage = '';
            $scope.retailassignprod = "S";//standalone
            $scope.product_image_update = 'N';
            $scope.retailprocessingfee = 2;
            $scope.showAddVariant = false;  
            $scope.showaddedvariant = true;
            $scope.variantflag_status = $scope.variantflag = 'Y';
            $scope.delete_all_variants_flag = 'N';
            $scope.variantslist = $scope.retailcategorylist = [];
            $scope.isDirty = false;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            
            $scope.opentabbarretail = function (viewname) {
                $scope.canceladdvariantview();
                $scope.isDirty = $scope.addretailproduct.$dirty;
                if ($scope.isDirty === true) {
                    $("#retailmessageModal").modal('show');
                    $("#retailmessagecontent").text("Save your detail changes to proceed.");
                } else {
                    $scope.isDirty = false;
                    if (viewname === 'details') {                    
                        $localStorage.currentretailpreviewtab = "retaildetails";
                        $scope.retaildetailsview = true;
                        $scope.retailvariantsview = false;
                        $scope.resettabbarclass();
                        $('#retailtab1').addClass('done');

                    } else if (viewname === 'variants' && $scope.afterpublish) {
                        $localStorage.currentretailpreviewtab = "retailvariants";
                        $scope.retaildetailsview = false;
                        $scope.retailvariantsview = true;
                        $scope.resettabbarclass();
                        $('#retailtab2').addClass('done');
                        $scope.getretailvariants();
                    }else {
                        $localStorage.currentretailpreviewtab = "retaildetails";
                        $scope.retaildetailsview = true;
                        $scope.retailvariantsview = false;
                        $scope.resettabbarclass();
                        $('#retailtab1').addClass('done');
                    }
                }
            };
            
             //open retail support link in new tab
            $scope.openRetailSupport = function(){
                window.open("https://intercom.help/mystudio/retail-and-merchandise-shop/3-set-up-products-and-variant-options-such-as-colors-and-sizing/adding-a-product-to-your-retail-shop",'_blank'); 
            };
            
            //catch variant flag
            $scope.catchvariantflag = function () {
                if ($scope.afterpublish && $scope.variantflag === "N" && $scope.variantslist.length > 0) {
                    $("#deleteallvariants-modal").modal('show');
                }
               $localStorage.PreviewRetailVariantFlag =  $scope.variantflag;
            };
            
            $scope.resettabbarclass = function (viewname) {
                $('.done').addClass('disabled').removeClass('done');
            };
            //Product image
            $scope.retailProductImgChanged = function () {
                $("#retailproductimgcropmodal").modal('show');
                $scope.product_image_update = 'N';
            };
            $scope.retailProductimgcropcancel = function () {
                $scope.product_image_update = 'N';
            };
            $scope.retailProductresetimgcropped = function () {               
                $scope.product_image_update = 'Y';
                $scope.retailcroppedProductImage = null;
                $scope.retailcroppedProductImage = '';
            };
            $scope.retailProductimgcropped = function () {
                $scope.product_image_update = 'Y';
                $scope.retailcroppedProductImage = $scope.cropper.croppedProductImage;
                $localStorage.PreviewRetailImage = $scope.retailcroppedProductImage;
            };
            // category Image
            $scope.retailCategoryImgChanged = function () {
                $("#retailcategoryimgcropmodal").modal('show');
                 $scope.category_image_update = 'N';
            };
            $scope.retailCategoryimgcropcancel = function () {
                $scope.category_image_update = 'N';
            };
            $scope.retailCategoryresetimgcropped = function () {               
                $scope.category_image_update = 'Y';
                $scope.retailcroppedCategoryImage = null;
                $scope.retailcroppedCategoryImage = '';
            };
            $scope.retailCategoryimgcropped = function () {
                $scope.category_image_update = 'Y';
                $scope.retailcroppedCategoryImage = $scope.cropper.croppedCategoryImage;
                $localStorage.PreviewRetailCatImage = $scope.retailcroppedCategoryImage;
            };
            
            //go back to manage page
             $scope.gobacktomanagepage = function () {
                if($scope.retailvariantsview == true){
                    $scope.opentabbarretail('details'); 
                }else{
                    $location.path('/manageretail');
                }
            };
            
            //get catergory list
            $http({
                method: 'GET',
                url: urlservice.url + 'getretailcategorylist',
                params: {
                    "company_id": $scope.company_id
                },
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.retailcategorylist = response.data.msg;
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
            
            
            
            if($localStorage.retailpagetype === 'edit'){
                $localStorage.currentpage = "editretail";
                if ($localStorage.retaillocaldata === undefined || $localStorage.retaillocaldata === "") {
                    $localStorage.retaillocaldata = retailListDetails.getSingleRetail();                  
                    $scope.retaildata = $localStorage.retaillocaldata;
                    $localStorage.preview_retailcontent = $scope.retaildata[0];
                } else {
                    $scope.retaildata = $localStorage.retaillocaldata;
                    $localStorage.preview_retailcontent = $scope.retaildata[0];
                } 
                if($scope.retaildata[0].retail_product_type === 'C'){
                    $localStorage.reloadfrom = "addcategory";
                    $localStorage.reload_cat_data = $scope.retaildata[0];
                    $localStorage.addretailtype = 'C';
                }else{
                    $localStorage.reloadfrom = "addproduct";
                    $localStorage.reload_prod_data = $scope.retaildata[0];  
                    $localStorage.addretailtype = 'P';
                }
                $localStorage.retailpagetype = "";
            }            
            
            if($localStorage.addretailtype === 'P') {
                $scope.addproduct = true;
                $scope.addretailcategoryview = false;
            } else {
                $scope.addproduct = false;
                $scope.addretailcategoryview = true;
            }           
            
            //retain data when reload page
            if($localStorage.reloadfrom === "addcategory"){
                var retailcategory_reloaddata = $localStorage.preview_retailcontent = $localStorage.reload_cat_data;
                $localStorage.retailpagetype = "add";
                $scope.afterpublish = true;
                $scope.retailcategory_status = retailcategory_reloaddata.retail_product_status;
                $scope.retailproduct_type = retailcategory_reloaddata.retail_product_type;
                $scope.retail_id = retailcategory_reloaddata.retail_product_id;
                $scope.retailcategoryname = retailcategory_reloaddata.retail_product_title;
                $scope.retailcategorysubname = retailcategory_reloaddata.retail_product_subtitle;
                $scope.retailcroppedCategoryImage = retailcategory_reloaddata.retail_banner_img_url;
                $scope.retailcategoryurl = retailcategory_reloaddata.retail_product_url;
            }else if($localStorage.reloadfrom === "addproduct"){
                $localStorage.retailpagetype = "add";
                $scope.afterpublish = true;
                $scope.retailproduct_details = $localStorage.preview_retailcontent = $localStorage.reload_prod_data; 
                $scope.retailproduct_status = $scope.retailproduct_details.retail_product_status;
                $scope.retailproduct_type = $scope.retailproduct_details.retail_product_type;
                $scope.retail_id = $scope.retailproduct_details.retail_product_id;
                $scope.retailproductname = $scope.retailproduct_details.retail_product_title;
                $scope.retailcroppedProductImage = $scope.retailproduct_details.retail_banner_img_url;
                $scope.retailproducturl = $scope.retailproduct_details.retail_product_url;
                $scope.retailproductdesc = $scope.retailproduct_details.retail_product_desc;
                $scope.retailprice = $scope.retailproduct_details.retail_product_price;
                $scope.retailcompareprice = $scope.retailproduct_details.retail_product_compare_price;
                $scope.taxrate = $scope.retailproduct_details.product_tax_rate;
                $scope.retailnovariantinventory = $scope.retailproduct_details.retail_product_inventory;
                $scope.variantflag_status = $scope.variantflag = $scope.retailproduct_details.retail_variant_flag;
                if (!$scope.retailproduct_details.retail_parent_id) {
                    $scope.retailassignprod = 'S';
                } else {
                    $scope.retailassignprod = $scope.retailproduct_details.retail_parent_id;
                }
            }
            $('#progress-full').hide();
            
            //add/update retail
            $scope.addRetail = function (category_status,type) { 

                if (parseFloat($scope.retailprice) < 5 && parseFloat($scope.retailprice) != 0) {
                    $("#retailmessageModal").modal('show');
                    $("#retailmessagecontent").text("Product price should be greater than " + $scope.wp_currency_symbol + " 5.00 or equal to " + $scope.wp_currency_symbol + " 0.00");
                    $('#progress-full').hide();
                    return;
                }               
                $('#progress-full').show();
                var retailtitle,parent_id,addorupdate,retail_id = "";
                if($scope.afterpublish){
                    addorupdate = "update";
                    retail_id = $scope.retail_id;
                }else{
                    addorupdate = "add";
                    retail_id = "";
                }
                if(type === "C") {
                    if ($scope.retailcroppedCategoryImage === "" || $scope.retailcroppedCategoryImage === undefined || $scope.retailcroppedCategoryImage === null) {
                        $scope.retail_category_picture_type = "";
                        $scope.retail_category_picture = "";
                    } else {
                        $scope.retail_category_picture_type = "png";
                        $scope.retail_category_picture = $scope.retailcroppedCategoryImage.substr($scope.retailcroppedCategoryImage.indexOf(",") + 1);
                    }
                    retailtitle = $scope.retailcategoryname;
                }
                
                if(type === "P"){
                    if ($scope.retailcroppedProductImage === "" || $scope.retailcroppedProductImage === undefined || $scope.retailcroppedProductImage === null) {
                        $scope.retail_product_picture_type = "";
                        $scope.retail_category_picture = "";
                    } else {
                        $scope.retail_category_picture_type = "png";
                        $scope.retail_category_picture = $scope.retailcroppedProductImage.substr($scope.retailcroppedProductImage.indexOf(",") + 1);
                    }
                    retailtitle = $scope.retailproductname;
                }
                if(type === "P" && $scope.retailassignprod === "S"){
                    parent_id = "";
                    type = "S";
                }else if(type === "C") {
                    parent_id = "";
                }else{
                    parent_id = $scope.retailassignprod;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateRetail',
                    data: {
                        "retail_product_title": retailtitle,
                        "retail_product_subtitle": $scope.retailcategorysubname,
                        "retail_banner_img_content": $scope.retail_category_picture,
                        "retail_banner_img_type": $scope.retail_category_picture_type,
                        "retail_product_status": category_status,
                        "company_id": $scope.company_id,
                        "retail_product_type":type,
                        "retail_product_desc":$scope.retailproductdesc,
                        "retail_parent_id":parent_id,
                        "add_or_update":addorupdate,
                        "retail_id":retail_id,
                        "retail_product_image_update":(type === 'C' ? $scope.category_image_update : $scope.product_image_update),
                        "retail_product_price":$scope.retailprice,
                        "tax_rate":$scope.taxrate,
                        "variant_flag":$scope.variantflag,
                        "retail_novariant_inventory":$scope.retailnovariantinventory,
                        "retail_product_compare_price":$scope.retailcompareprice ? $scope.retailcompareprice : "",
                        "retail_old_image_url":$scope.retail_old_image_url
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#retailmessageModal").modal('show');
                        $("#retailmessagecontent").text(response.data.msgs);
                        $localStorage.retailpagetype = 'add';                       
                        $scope.product_image_update = $scope.category_image_update = 'N';
                        $scope.afterpublish = true;
                        $scope.isDirty = $scope.addretailproduct.$dirty = false;
                        if(response.data.msg.retail_product_type === "C"){    
                            $scope.retailcategory_details = response.data.msg;
                            $localStorage.reloadfrom = "addcategory";
                            $localStorage.preview_retailcontent = $localStorage.reload_cat_data = $scope.retailcategory_details;
                            $scope.retailcategory_status = $scope.retailcategory_details.retail_product_status;
                            $scope.retailproduct_type = $scope.retailcategory_details.retail_product_type;
                            $scope.retail_id = $scope.retailcategory_details.retail_product_id;
                            $scope.retailcategoryname = $scope.retailcategory_details.retail_product_title;
                            $scope.retailcategorysubname = $scope.retailcategory_details.retail_product_subtitle;
                            $scope.retail_old_image_url = $scope.retailcroppedCategoryImage = $scope.retailcategory_details.retail_banner_img_url;
                            $scope.retailcategoryurl = $scope.retailcategory_details.retail_product_url;
                        }else{
                            $scope.retailproduct_details = response.data.msg;
                            $localStorage.reloadfrom = "addproduct";
                            $localStorage.preview_retailcontent = $localStorage.reload_prod_data = $scope.retailproduct_details;
                            $scope.retailproduct_status = $scope.retailproduct_details.retail_product_status;
                            $scope.retailproduct_type = $scope.retailproduct_details.retail_product_type;
                            $scope.retail_id = $scope.retailproduct_details.retail_product_id;
                            $scope.retailproductname = $scope.retailproduct_details.retail_product_title;
                            $scope.retail_old_image_url = $scope.retailcroppedProductImage = $scope.retailproduct_details.retail_banner_img_url;
                            $scope.retailproducturl = $scope.retailproduct_details.retail_product_url;
                            $scope.retailproductdesc = $scope.retailproduct_details.retail_product_desc;
                            $scope.retailprice = $scope.retailproduct_details.retail_product_price;
                            $scope.retailcompareprice = $scope.retailproduct_details.retail_product_compare_price;
                            $scope.taxrate = $scope.retailproduct_details.product_tax_rate;
                            $scope.retailnovariantinventory = $scope.retailproduct_details.retail_product_inventory;
                            $scope.variantflag_status = $scope.variantflag = $scope.retailproduct_details.retail_variant_flag;
                            if(!$scope.retailproduct_details.retail_parent_id){
                                $scope.retailassignprod = 'S';
                            } else {
                                $scope.retailassignprod = $scope.retailproduct_details.retail_parent_id;
                                console.log();
                            }
                        }
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#retailmessageModal").modal('show');
                        $("#retailmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $scope.handleError(response.data);
                });
               
            };
                
            //variant starts
            $scope.shownewvariants = function () {
                $scope.showAddVariant = true;
            };

            $scope.canceladdvariantview = function () {
                $scope.showAddVariant = false;
                $scope.variantname = "";
                $scope.inventorycount = "";
                $scope.variantprice = "";
                $scope.variantcompareprice = "";
            };
            
            $scope.editvariant = function (name,count,price,compare_price,ind) {             
                $scope.showaddedvariant = false; 
                this.variant_column_name = name;
                this.variant_inventory_count_updated = count;
                this.variant_price_updated = price;
                this.variant_compare_price_updated = compare_price;
                $scope.selectedvariantcolindex = ind;  
                $scope.edit_showaddedvariant = false;
            };
            
            $scope.cancelsave = function () {
                $scope.selectedvariantcolindex = "";
                $scope.showaddedvariant = true;
                $scope.edit_showaddedvariant = false;                
            };
            
            //get retail variants
            $scope.getretailvariants = function(){
            $('#progress-full').show();
              $http({
                    method: 'Get',
                    url: urlservice.url + 'getretailvariants',
                    params: {
                        "company_id": $localStorage.company_id,
                        "retail_id":$scope.retail_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.retailpagetype = 'add';
                        $scope.variantslist = response.data.msg;
                        $localStorage.PreviewRetailVariants = $scope.variantslist;
                        $localStorage.PreviewRetailVariantsprice = (response.data.retail_variant_price) ? (response.data.retail_variant_price) :  ($scope.wp_currency_symbol+'0.00');
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            
            //add/update/delete variants
            $scope.addorupdatedeletevariants = function(type,name,count,price,compareprice,id){

                if ((parseFloat($scope.variantprice) < 5 && parseFloat($scope.variantprice) != 0) || (parseFloat(price) < 5 && parseFloat(price) != 0)) {
                    $("#retailmessageModal").modal('show');
                    $("#retailmessagecontent").text("Variant price should be greater than " + $scope.wp_currency_symbol + " 5.00 or equal to " + $scope.wp_currency_symbol + " 0.00");
                    $('#progress-full').hide();
                    return;
                }
              $('#progress-full').show();
              $http({
                    method: 'POST',
                    url: urlservice.url + 'addorupdatedeletevariants',
                    data: {
                        "variant_name": name,
                        "variant_inventory_count": count,
                        "variant_price": price,
                        "variant_compare_price":compareprice,
                        "variant_id": id,
                        "company_id": $localStorage.company_id,
                        "retail_id":$scope.retail_id,
                        "type":type
//                        "delete_all_variants_flag":$scope.delete_all_variants_flag
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.retailpagetype = 'add';
                        $scope.canceladdvariantview();
                        $scope.cancelsave();
                        $scope.variantslist = response.data.msg;
                        $localStorage.PreviewRetailVariants = $scope.variantslist;
                        $localStorage.PreviewRetailVariantsprice = (response.data.retail_variant_price) ? (response.data.retail_variant_price) :  ($scope.wp_currency_symbol+'0.00');
                        $scope.delete_all_variants_flag = 'N';
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            //delete all variants
            $scope.deleteallvariants = function () {
                $scope.delete_all_variants_flag = 'Y';
            }
            $scope.deleteConfirmModal = function (type,variantid) {  
                $("#retaildeleteModal").modal('show');
                if(type === 'product'){
                    $scope.deleteconfirmok = 'product';
                    $("#retaildeletemessagecontent").text('Are you sure want to delete this product?');
                }else if(type === 'category'){
                    $scope.deleteconfirmok = 'category';
                    $("#retaildeletemessagecontent").text('Are you sure want to delete this category?');
                }else if(type === 'variant'){
                    $scope.deleteconfirmok = 'variant';
                    $scope.deletevariantid = variantid;
                    $("#retaildeletemessagecontent").text('Are you sure want to delete this variant?');
                }
            };
            $scope.cancelretaildelete = function (){ 
                $scope.deleteconfirmok = '';
                $scope.deletevariantid = '';
            }
            
            //delete product
            $scope.deleteproduct = function(){
                $('#progress-full').show();
              $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteretailproduct',
                    data: {
                        "company_id": $localStorage.company_id,
                        "retail_product_id":$scope.retail_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.retailpagetype = 'add';
                        $('#progress-full').hide();
                        $("#productdeleted-modal").modal('show');
                        $("#productdeletedmessage").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status == 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else {
                        $('#progress-full').hide();
                        $("#retailmessageModal").modal('show');
                        $("#retailmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            //dnd
            $scope.dragvariantfieldCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_variant_id = $scope.variantslist[ind].variant_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.variantslist.length;i++){ 
                            if($scope.variantslist[i].variant_id === $scope.start_variant_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_variant_id, variant_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_variant_id = $scope.variantslist[$scope.new_location_id].variant_id;
                                previous_sort_order = parseFloat($scope.variantslist[$scope.new_location_id + 1].variant_order);
                                variant_sort_order = previous_sort_order + 1;
                                $scope.update_variantfieldSorting(sort_variant_id, variant_sort_order);
                            } else if ($scope.new_location_id === $scope.variantslist.length - 1) {
                                sort_variant_id = $scope.variantslist[$scope.new_location_id].variant_id;
                                variant_sort_order = parseFloat($scope.variantslist[$scope.new_location_id - 1].variant_order) / 2;
                                $scope.update_variantfieldSorting(sort_variant_id, variant_sort_order);
                            } else {
                                sort_variant_id = $scope.variantslist[$scope.new_location_id].variant_id;
                                previous_sort_order = parseFloat($scope.variantslist[$scope.new_location_id + 1].variant_order);
                                next_sort_order = parseFloat($scope.variantslist[$scope.new_location_id - 1].variant_order);
                                variant_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.update_variantfieldSorting(sort_variant_id, variant_sort_order);
                            }
                        }

                    }, 1000);
                }
            };
            
            $scope.update_variantfieldSorting = function(variant_id,variant_sort_order){
                $('#progress-full').show();
                $localStorage.retailVariantSorted = 'N';     
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateVariantSortOrder',
                    data: {
                        "sort_id": variant_sort_order,
                        "variant_id": variant_id,
                        "retail_id": $scope.retail_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.retailVariantSorted = 'Y';     
                        $localStorage.retailpagetype = 'add';
                        $scope.canceladdvariantview();
                        $scope.cancelsave();
                        $scope.variantslist = response.data.msg;
                        $localStorage.PreviewRetailVariants = $scope.variantslist;
                        $localStorage.PreviewRetailVariantsprice = (response.data.retail_variant_price) ? (response.data.retail_variant_price) : ($scope.wp_currency_symbol+'0.00');
                        $scope.delete_all_variants_flag = 'N';
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $localStorage.retailVariantSorted = 'N';     
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            $scope.retailvariantremoveDrag = function (event) {
                $(event.currentTarget).prop("draggable", false);
                $(event.currentTarget.closest('li')).prop("draggable", false);
            };

            $scope.retailvariantaddDrag = function (event) {
                $(event.currentTarget).prop("draggable", true);
                $(event.currentTarget.closest('li')).prop("draggable", true);
            };
            //redirect manage page
            $scope.redirectmanagepage = function(){
              $("#productdeleted-modal").modal('hide');
                $('#productdeleted-modal').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/manageretail';
                })  
            };
            $scope.catchRetailProdName = function (){
                if($scope.retailproductname === undefined || $scope.retailproductname === ""){
                    $localStorage.PreviewRetailName = '';
                }else{
                    $localStorage.PreviewRetailName = $scope.retailproductname;
                }
            };
            $scope.catchRetailProdDesc = function (){
                if($scope.retailproductdesc === undefined || $scope.retailproductdesc === ""){
                    $localStorage.PreviewRetailDesc = '';
                }else{
                    $localStorage.PreviewRetailDesc = $scope.retailproductdesc;
                }
            };
            $scope.catchRetailProdprice = function (){
                if($scope.retailprice === undefined || $scope.retailprice === ""){
                    $localStorage.PreviewRetailPrice = '';
                }else{
                    $localStorage.PreviewRetailPrice = $scope.retailprice;
                }
            };
            $scope.catchRetailCompareprice = function (){
                if($scope.retailcompareprice === undefined || $scope.retailcompareprice === ""){
                    $localStorage.PreviewRetailComparePrice = '';
                }else{
                    $localStorage.PreviewRetailComparePrice = $scope.retailcompareprice;
                }
            };
            $scope.catchRetailTax = function (){
                if($scope.taxrate === undefined || $scope.taxrate === ""){
                    $localStorage.PreviewRetailTax = '';
                }else{
                    $localStorage.PreviewRetailTax = $scope.taxrate;
                }
            };
            $scope.catchRetCategoryTitle = function (){
                if($scope.retailcategoryname === undefined || $scope.retailcategoryname === ""){
                    $localStorage.PreviewRetailCatName = '';
                }else{
                    $localStorage.PreviewRetailCatName = $scope.retailcategoryname;
                }
            };
            $scope.catchRetCategorySubtitle = function (){
                if($scope.retailcategorysubname === undefined || $scope.retailcategorysubname === ""){
                    $localStorage.PreviewRetailCatSubname = '';
                }else{
                    $localStorage.PreviewRetailCatSubname = $scope.retailcategorysubname;
                }
            };
            $scope.catchRetailvariantname = function (){
                if($scope.variantname === undefined || $scope.variantname === ""){
                    $localStorage.PreviewRetailVariantName = '';
                }else{
                    $localStorage.PreviewRetailVariantName = $scope.variantname;
                }
            };
            if($localStorage.retailpagetype === 'add'){
                $localStorage.currentpage = "addretail";
            }

        } else {
            $location.path('/login');
        }

    };
    
    module.exports = AddRetailController;
   