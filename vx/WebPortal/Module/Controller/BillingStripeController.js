BillingStripeController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', '$rootScope'];
function BillingStripeController($scope, $location, $http, $localStorage, urlservice, $filter, $route, $rootScope) {

//        alert('My account controller ');

        if ($localStorage.islogin === 'Y') {
            $rootScope.sideBarHide = false;
            $rootScope.hidesidebarlogin = false;
            $rootScope.adminsideBarHide = false;
            $rootScope.activepagehighlight = 'myaccount';
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $scope.usstateSelection = true;
            $scope.castateSelection = false;
            $scope.stripeagreementchecked = false;
            $scope.showcarddetailbox = true;
            $scope.studio_expiry_level = $localStorage.studio_expiry_level;
            $scope.newcard = false;
            $scope.studio_discount_available = 'N';
            
            $scope.usstates = [{name: 'Alabama', code: 'AL'}, {name: 'Alaska', code: 'AK'}, {name: 'Arizona', code: 'AZ'}, {name: 'Arkansas', code: 'AR'}, {name: 'California', code: 'CA'},
                {name: 'Colorado', code: 'CO'}, {name: 'Connecticut', code: 'CT'}, {name: 'Delaware', code: 'DE'}, {name: 'Florida', code: 'FL'}, {name: 'Georgia', code: 'GA'},
                {name: 'Hawaii', code: 'HI'}, {name: 'Idaho', code: 'ID'}, {name: 'Illinois', code: 'IL'}, {name: 'Indiana', code: 'IN'}, {name: 'Iowa', code: 'IA'},
                {name: 'Kansas', code: 'KS'}, {name: 'Kentucky', code: 'KY'}, {name: 'Louisiana', code: 'LA'}, {name: 'Maine', code: 'ME'}, {name: 'Maryland', code: 'MD'},
                {name: 'Massachusetts', code: 'MA'}, {name: 'Michigan', code: 'MI'}, {name: 'Minnesota', code: 'MN'}, {name: 'Mississippi', code: 'MS'}, {name: 'Missouri', code: 'MO'},
                {name: 'Montana', code: 'MT'}, {name: 'Nebraska', code: 'NE'}, {name: 'Nevada', code: 'NV'}, {name: 'New Hampshire', code: 'NH'}, {name: 'New Jersey', code: 'NJ'},
                {name: 'New Mexico', code: 'NM'}, {name: 'New York', code: 'NY'}, {name: 'North Carolina', code: 'NC'}, {name: 'North Dakota', code: 'ND'}, {name: 'Ohio', code: 'OH'},
                {name: 'Oklahoma', code: 'OK'}, {name: 'Oregon', code: 'OR'}, {name: 'Pennsylvania', code: 'PA'}, {name: 'Rhode Island', code: 'RI'}, {name: 'South Carolina', code: 'SC'},
                {name: 'South Dakota', code: 'SD'}, {name: 'Tennessee', code: 'TN'}, {name: 'Texas', code: 'TX'}, {name: 'Utah', code: 'UT'}, {name: 'Vermont', code: 'VT'},
                {name: 'Virginia', code: 'VA'}, {name: 'Washington', code: 'WA'}, {name: 'West Virginia', code: 'WV'}, {name: 'Wisconsin', code: 'WI'}, {name: 'Wyoming', code: 'WY'},
                {name: 'District of Columbia', code: 'DC'}, {name: 'American Samoa', code: 'AS'}, {name: 'Guam', code: 'GU'}, {name: 'Northern Mariana Islands', code: 'MP'}, {name: 'Puerto Rico', code: 'PR'},
                {name: 'United States Minor Outlying Islands', code: 'UM'}, {name: 'Virgin Islands, U.S.', code: 'VI'}];

            $scope.castates = [{name: 'Alberta', code: 'AB'}, {name: 'British Columbia', code: 'BC'}, {name: 'Manitoba', code: 'MB'}, {name: 'New Brunswick', code: 'NB'}, {name: 'Newfoundland and Labrador', code: 'NL'},
                {name: 'Nova Scotia', code: 'NS'}, {name: 'Ontario', code: 'ON'}, {name: 'Prince Edward Island', code: 'PE'}, {name: 'Quebec', code: 'QC'}, {name: 'Saskatchewan', code: 'SK'},
                {name: 'Northwest Territories', code: 'NT'}, {name: 'Nunavut', code: 'NU'}, {name: 'Yukon', code: 'YT'}];               
                
            
            // IDENTIFYING VERSION IN URL
            if (window.location.hostname === 'localhost'){
               $scope.version = window.location.pathname.split("/")[3]; 
            }else{
               $scope.version = window.location.pathname.split("/")[1]; 
            }
            
            $scope.billinginfo = function () {
                $scope.emailaddresses = $localStorage.loginDetails.referral_email_list;
                //hardcode
//            $localStorage.upgrade_status = 'WM';
                $scope.upgrade_status = $localStorage.upgrade_status;
                if ($localStorage.upgrade_status === 'T') {
                    $scope.current_plan = 'Free Trial';
                    $scope.current_bill_amount = 'N/A';
                    if ($scope.remng_days > 1) {
                        $scope.planTerm = $scope.remng_days + ' days ';
                    } else if ($scope.remng_days < 1) {
                        $scope.planTerm = 'N/A';
                    } else if ($scope.remng_days === "1") {
                        $scope.planTerm = $scope.remng_days + ' day ';
                    }
                }else if ($localStorage.upgrade_status === 'F') {
                    $scope.current_plan = 'Inactive';
                    $scope.current_bill_amount = 'N/A';
                } else if ($localStorage.upgrade_status === 'B') {
                    $scope.current_plan = 'Basic';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Monthly Auto Renewal';
                } else if ($localStorage.upgrade_status === 'P') {
                    $scope.current_plan = 'Premium / Annual';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Annual Auto Renewal';
                } else if ($localStorage.upgrade_status === 'M') {
                    $scope.current_plan = 'Premium / Monthly';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Monthly Auto Renewal';
                } else if ($localStorage.upgrade_status === 'W' && $scope.current_bill_amount === 588) {
                    $scope.current_plan = 'White Label / Annual';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Annual Auto Renewal';
                } else if ($localStorage.upgrade_status === 'WM') {
                    $scope.current_plan = 'White Label / Monthly';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Monthly Auto Renewal';
                }else if ($localStorage.upgrade_status === 'W' && $scope.current_bill_amount === 348) {
                    $scope.current_plan = 'Legacy White Label';
                    $scope.expiry_date = $localStorage.loginDetails.expiry_dt;
                    $scope.planTerm = 'Annual Auto Renewal';
                }
            };
            
            $scope.getCompanyInfo = function (no_of_reload) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getCompanyInfo',
                    data: {
                        'company_id': $localStorage.company_id,
                        'user_id': $localStorage.user_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.loginDetails = response.data.msg;
                        $localStorage.subscription_status = response.data.msg.subscription_status;
                        $localStorage.whilelabel_setup_page_access = response.data.msg.whilelabel_setup_page_access;
                        $localStorage.upgrade_status = response.data.msg.upgrade_status;
                        $localStorage.loginDetails.expiry_dt = response.data.msg.expiry_dt;
                        $localStorage.processing_fee_percentage = response.data.msg.processing_fee_percentage;
                        $localStorage.processing_fee_transaction = response.data.msg.processing_fee_transaction;
                        $scope.studio_created_date = response.data.msg.studio_created_date;
                        $scope.last_login = response.data.msg.last_login_date;
                        $scope.owner_name = response.data.msg.owner_name;
                        $scope.remng_days = response.data.msg.remaining_days;
                        $localStorage.studio_expiry_level = response.data.msg.studio_expiry_level;
                        $scope.studio_expiry_level = $localStorage.studio_expiry_level;
                        $scope.stripe_currency_symbol = $localStorage.wp_currency_symbol;
                        $scope.current_bill_amount=response.data.msg.subscription_fee.current_plan_fee;
                        $scope.studio_discount_available = response.data.msg.studio_discount_available;
                        //last login day calculation
                        var d = new Date($scope.last_login);
                        var n = d.getDay();
                        $scope.last_login_day = (n == 1 ? 'Monday' : ((n == 2 ? 'Tuesday' : ((n == 3 ? 'Wednesday' : ((n == 4 ? 'Thursday' : ((n == 5 ? 'Friday' : ((n == 6 ? 'Saturday' : ((n == 7 ? 'Sunday' : (''))))))))))))));
                        $localStorage.user_id = response.data.msg.user_id;
                        if (no_of_reload === '1') {
                            setTimeout(function () {
                                $route.reload();
                            }, 200);
                        }                        
                        $scope.billinginfo();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.getCompanyInfo('0');           

            $scope.countryselection = function () {
                if ($scope.countrycode === 'CA') {
                    $scope.castateSelection = true;
                    $scope.usstateSelection = false;
                } else {
                    $scope.castateSelection = false;
                    $scope.usstateSelection = true;
                }
            };

            $scope.updateEmailAccount = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateReferralEmailList',
                    data: {
                        'email_list': $scope.emailaddresses,
                        'company_id': $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.emailaddresses = response.data.msg.email_list;
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Success Message');
                        $("#messagecontent").text('Additional emails for notifications added');
                        $route.reload();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };


            $scope.changeAccountPassword = function () {
                $("#changepasswordmodal").modal('show');
            };

            $scope.changePasswordClose = function () {
                $("#changepasswordmodal").modal('hide');
                $scope.oldpassword = $scope.newpassword = $scope.retypenewpassword = '';
                $scope.passwordchange.$setPristine();
                $scope.passwordchange.$setUntouched();
            };

            $scope.updatepassword = function () {
                $('#progress-full').show();

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatePassword',
                    data: {
                        'old_password': $scope.oldpassword,
                        'new_password': $scope.newpassword,
                        'company_id': $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#changepasswordmodal").modal('hide');
                        $scope.passwordchange.$setPristine();
                        $scope.passwordchange.$setUntouched();
                        $scope.oldpassword = $scope.newpassword = $scope.retypenewpassword = '';
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Success Message');
                        $("#messagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        console.log(response.data);
                        $scope.passwordchange.$setPristine();
                        $scope.passwordchange.$setUntouched();
                        $scope.changepasswordsuccess = true;
                        $scope.oldpassword = $scope.newpassword = $scope.retypenewpassword = '';
                        $("#pswdresponse").text(response.data.msg);
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $scope.changepasswordsuccess = true;
                    $("#pswdresponse").text('Invalid server response');
                });
            };



            $scope.initialGetToken = function () {
                // Create a Stripe client
                $scope.valid_card = false;
                var stripe = Stripe($localStorage.stripe_publish_key);

                // Create an instance of Elements
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                var card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                card.mount('#card-element');

                // Handle real-time validation errors from the card Element.
                card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                    } else {
                        displayError.textContent = '';
                    }
                });

                // Handle form submission
                var form = document.getElementById('stripepaymentform');
                form.addEventListener('submit', function (event) {
                    event.preventDefault();

                    stripe.createToken(card).then(function (result) {
                        if (result.error) {
                            // Inform the user if there was an error
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                        } else {
                            // Send the token to your server                           
                            stripeTokenHandler(result.token);
                        }
                    });
                });

                function stripeTokenHandler(token) {
                    // Insert the token ID into the form so it gets submitted to the server
                    var form = document.getElementById('stripepaymentform');
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token.id);
                    form.appendChild(hiddenInput);
                    $scope.token_id = token.id;
                    //add/update
                    if ($scope.cardexist) {
                        $scope.addstripecard('update_card', 'U', 'N');
                    } else {
                        $scope.addstripecard('registration', 'A', 'N');
                    }
                }

            };

            $scope.initialGetToken();

            //New Stripe Design 
            $scope.planview = false;
            $scope.accountview = true;
            $scope.billingview = false;
            
            //show plan view
            $scope.showplanview = function () {
                $scope.planview = true;
                $scope.accountview = false;
                $scope.billingview = false;
            };
            
            //cancel plan view
            $scope.cancelplanview = function () {
                $scope.planview = false;
                $scope.accountview = true;
                $scope.billingview = false;
            };
            
            $scope.backfromcheckout = function () {
                $scope.planview = true;
                $scope.accountview = false;
                $scope.billingview = false;
                $scope.clearaddcardscopes();
                $scope.canceladdcard();
                $scope.referraltype = "";
                $scope.referredby = "";
                $scope.stripeagreementchecked = false;
            }

                        
            //change card drop down
            $scope.showBillingForm = function () {
                if ($scope.carddetail === 'newcard') {
                    $scope.newcard = true;
                    $scope.hideagreement = true;
                } else if ($scope.carddetail === 'existingcard') {
                    $scope.newcard = false;
                    $scope.hideagreement = false;
                }
            };
            
            //replace card
            $scope.replacecard = function () {
                $scope.newcard = true;
                $scope.hideagreement = true;
                $scope.showcarddetailbox = false;
            };
            
            //cancel add card
            $scope.canceladdcard = function () {
                var displayError = document.getElementById('card-errors');
                displayError.textContent = '';
                $scope.initialGetToken();
                $scope.showcarddetailbox = true;
                $scope.newcard = false;
                $scope.hideagreement = false;
                $scope.carddetail = "";
                $scope.showcarddetailbox = true;
                $scope.clearaddcardscopes();
                $scope.stripepaymentform.$setPristine();
                $scope.stripepaymentform.$setUntouched();
            }
            
            //clearscopefrm addcard
            $scope.clearaddcardscopes = function () {
                $scope.firstname = "";
                $scope.lastname = "";
//                $scope.email = "";
//                $scope.phnum = "";
                $scope.address = "";
                $scope.city = "";
                $scope.statecode = "";
                $scope.countrycode = "";
                $scope.billingzipcode = "";
            };
            
            //get upgrade plan details
            $scope.getplan = function (status) {
                if($localStorage.studio_expiry_level === 'L1'){
                    $("#expired-modal").modal('show');
                    return false;
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getPlanDetails',
                    data: {
                        'company_id': $localStorage.company_id,
                        'payment_type': status
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.upgradeplandetails = response.data.msg;
                        $scope.upgradeplanlength = Object.keys($scope.upgradeplandetails).length;
                        $scope.currentplan = response.data.current_status;
                        if ($scope.upgradeplanlength === 1) {
                            $scope.upgradeto = $scope.arrayonestatus = $scope.upgradeplandetails['yearly'].plan_type;
                            $scope.arrayone_stripe_plan_amount = $scope.upgradeplandetails['yearly'].plan_fee;
                            $scope.arrayone_stripe_renewal_date = $scope.upgradeplandetails['yearly'].next_payment_date;
                            $scope.arrayone_stripe_renewal_date = $filter('date')($scope.arrayone_stripe_renewal_date, 'MMMM d, y');
                            $scope.arrayoneprorate = $scope.upgradeplandetails['yearly'].prorated_fee;
                            $scope.arrayonefee = $scope.upgradeplandetails['yearly'].plan_fee;
                            $scope.arrayoneofferfee = $scope.upgradeplandetails['yearly'].plan_offer_fee;
                            $scope.arrayonetotal_due = $scope.upgradeplandetails['yearly'].total_due;
                            $scope.arrayoneoffertotal_due = $scope.upgradeplandetails['yearly'].total_due;
                            $scope.stripe_payment_type = $scope.arrayonestatus;
                        }
                        if ($scope.upgradeplanlength === 2) {
                            $scope.upgradeto = $scope.arrayonestatus = $scope.upgradeplandetails['monthly'].plan_type;
                            $scope.arrayone_stripe_plan_amount = $scope.upgradeplandetails['monthly'].plan_fee;
                            $scope.arrayone_stripe_renewal_date = $scope.upgradeplandetails['monthly'].next_payment_date;
                            $scope.arrayone_stripe_renewal_date = $filter('date')($scope.arrayone_stripe_renewal_date, 'MMMM d, y');
                            $scope.arrayoneprorate = $scope.upgradeplandetails['monthly'].prorated_fee;
                            $scope.arrayonefee = $scope.upgradeplandetails['monthly'].plan_fee;
                            $scope.arrayonetotal_due = $scope.upgradeplandetails['monthly'].total_due;
                            $scope.arraytwostatus = $scope.upgradeplandetails['yearly'].plan_type;
                            $scope.arraytwo_stripe_plan_amount = $scope.upgradeplandetails['yearly'].plan_fee;
                            $scope.arraytwo_stripe_renewal_date = $scope.upgradeplandetails['yearly'].next_payment_date;
                            $scope.arraytwo_stripe_renewal_date = $filter('date')($scope.arraytwo_stripe_renewal_date, 'MMMM d, y');
                            $scope.arraytwofee = $scope.upgradeplandetails['yearly'].plan_fee;
                            $scope.arraytwoofferfee = $scope.upgradeplandetails['yearly'].plan_offer_fee;
                            $scope.arraytwoprorate = $scope.upgradeplandetails['yearly'].prorated_fee;
                            $scope.arraytwototal_due = $scope.upgradeplandetails['yearly'].total_due;
                            $scope.arraytwooffertotal_due = $scope.upgradeplandetails['yearly'].total_due;
                            $scope.stripe_payment_type = $scope.arraytwostatus;
                        }
                        $scope.planview = false;
                        $scope.accountview = false;
                        $scope.billingview = true;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //add stripe card
            $scope.addstripecard = function (payee_type, payment_type, charge_flag) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateStudioSubscriptionCard',
                    data: {
                        'company_id': $localStorage.company_id,
                        'user_id': $localStorage.user_id,
                        'charge_flag': charge_flag,
                        'token_id': $scope.token_id,
                        'payment_type': payment_type,
                        'payee_type': payee_type,
                        'FirstName': $scope.firstname,
                        'LastName': $scope.lastname,
//                        'Email': $scope.email,
//                        'Phone': $scope.phnum,
                        'StreetAddress': $scope.address,
                        'City': $scope.city,
                        'StateCode': $scope.statecode,
                        'Country': $scope.countrycode,
                        'BillingZipcode': $scope.billingzipcode,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.stripe_card_detail = response.data.account_details;
                        $scope.stripe_cardname = $scope.stripe_card_detail.stripe_card_name;
                        $scope.stripe_streetaddress = $scope.stripe_card_detail.streetaddress;
                        $scope.stripe_city = $scope.stripe_card_detail.city;
                        $scope.stripe_state = $scope.stripe_card_detail.state;
                        $scope.stripe_zip = $scope.stripe_card_detail.zip;
                        $scope.stripe_country = $scope.stripe_card_detail.country;
                        $scope.stripe_card_exp_month = $scope.stripe_card_detail.card_exp_month;
                        $scope.stripe_card_exp_year = $scope.stripe_card_detail.card_exp_year;
                        $localStorage.studio_expiry_level = response.data.studio_expiry_level;
                        $scope.studio_expiry_level = $localStorage.studio_expiry_level;
                        $scope.cardexist = true;
                        $scope.showcarddetailbox = true;
                        $scope.clearaddcardscopes();
                        $scope.closeaddcardmodal();
                        $scope.canceladdcard();
                        $("#addcardfromexpiredview-modal").modal('hide');
                        $("#expired-modal").modal('hide');
                        $('#progress-full').hide();
                        if (response.data.from_rerun === "Y") {
                            $("#rerunsuccess-modal").modal('show');
                        } else {
                            $("#stripemessageModal").modal('show');
                            $("#stripemessagetitle").text('Message');
                            $("#stripemessagecontent").text(response.data.msg);
                        }
                    } else if (response.data.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#stripemessageModal").modal('show');
                        $("#stripemessagetitle").text('Message');
                        $("#stripemessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //get stripe card details
            $scope.getCardList = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getStripeCreditCardAccounts',
                    data: {
                        'company_id': $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.cardexist = true;
                        $scope.stripe_card_detail = response.data.account_details;
                        $scope.stripe_cardname = $scope.stripe_card_detail.stripe_card_name;
                        $scope.stripe_streetaddress = $scope.stripe_card_detail.streetaddress;
                        $scope.stripe_city = $scope.stripe_card_detail.city;
                        $scope.stripe_state = $scope.stripe_card_detail.state;
                        $scope.stripe_zip = $scope.stripe_card_detail.zip;
                        $scope.stripe_country = $scope.stripe_card_detail.country;
                        $scope.stripe_card_exp_month = $scope.stripe_card_detail.card_exp_month;
                        $scope.stripe_card_exp_year = $scope.stripe_card_detail.card_exp_year;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $scope.cardexist = false;
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getCardList();
            
//            open add card modal
            $scope.openaddcardmodal = function () {
                $("#addstripecard-modal").modal('show');
            };
            
            //close addcard modal
            $scope.closeaddcardmodal = function () {
                var displayError = document.getElementById('card-errors-formodal');
                displayError.textContent = '';
                $scope.initialGetTokenformodal();
                $scope.clearaddcardscopes();
                $scope.stripepaymentform_frommodal.$setPristine();
                $scope.stripepaymentform_frommodal.$setUntouched();
                $("#addstripecard-modal").modal('hide');
            };
            
            //change refreal dropdown
            $scope.changeleadsource = function () {
                $scope.referredby = "";
            }
            
            //modal stripe api
            $scope.initialGetTokenformodal = function () {
                // Create a Stripe client
                $scope.valid_card = false;
                var stripe = Stripe($localStorage.stripe_publish_key);

                // Create an instance of Elements
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: '#32325d',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                var card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                card.mount('#card-element-formodal');

                // Handle real-time validation errors from the card Element.
                card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('card-errors-formodal');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                    } else {
                        displayError.textContent = '';
                    }
                });

                // Handle form submission
                var form = document.getElementById('stripepaymentform_frommodal');
                form.addEventListener('submit', function (event) {
                    event.preventDefault();

                    stripe.createToken(card).then(function (result) {
                        if (result.error) {
                            // Inform the user if there was an error
                            var errorElement = document.getElementById('card-errors-formodal');
                            errorElement.textContent = result.error.message;
                        } else {
                            // Send the token to your server                           
                            stripeTokenHandler(result.token);
                        }
                    });
                });

                function stripeTokenHandler(token) {
                    // Insert the token ID into the form so it gets submitted to the server
                    var form = document.getElementById('stripepaymentform_frommodal');
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token.id);
                    form.appendChild(hiddenInput);
                    $scope.token_id = token.id;
                    //add/update
                    if ($localStorage.studio_expiry_level === 'L1' || $localStorage.studio_expiry_level === 'L2') {
                        $scope.addstripecard('update_card', 'U', 'Y');
                    } else {
                        if ($scope.cardexist) {
                            $scope.addstripecard('update_card', 'U', 'N');
                        } else {
                            $scope.addstripecard('registration', 'A', 'N');
                        }
                    }

                }

            };
            $scope.initialGetTokenformodal();
            
            
            //plan expired checking
            if ($localStorage.studio_expiry_level === 'L1' || $localStorage.studio_expiry_level === 'L2') {
                $("#expired-modal").modal('show');
            } else if($localStorage.subscription_status === 'N' &&   $localStorage.upgrade_status === 'F'){
                $("#inactive-expired-modal").modal('show');
            }
            
            
            //re reun payment
            $scope.rereunpayment = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'reRunSubscription',
                    data: {
                        'company_id': $localStorage.company_id,
                        'payee_type': 'rerun_payment',
                        'charge_flag': 'Y',
                        'payment_amount': $scope.current_bill_amount
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.studio_expiry_level = response.data.studio_expiry_level;
                        $scope.studio_expiry_level = $localStorage.studio_expiry_level;
                        $('#progress-full').hide();
                        $("#expired-modal").modal('hide');
                        $("#rerunsuccess-modal").modal('show');
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'UpdateFailed') {
                        $scope.studio_expiry_level = $localStorage.studio_expiry_level;
                        $('#progress-full').hide();
                        $("#expired-modal").modal('hide');
                        $("#addcardfromexpiredview-modal").modal('show');
                    }else if (response.data.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#expired-modal").modal('hide');
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            //open add card modal from expired view
            $scope.addcardfromexpiredview = function () {
                $("#addstripecard-modal").modal('show');
            }
            
            
            //change plan call
            $scope.updateplan = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateStudioSubscription',
                    data: {
                        'company_id': $localStorage.company_id,
                        'user_id': $localStorage.user_id,
                        'payment_type': $scope.stripe_payment_type,
                        'payee_type': 'update_plan',
                        'charge_flag': 'Y',
                        'lead_source': $scope.referraltype,
                        'referer_name': $scope.referredby
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#rerunsuccess-modal").modal('show');
                        $localStorage.upgrade_status = response.data.new_upgrade_status;
                        $scope.upgrade_status = $localStorage.upgrade_status;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //reload page
            $scope.reloadpage = function () {
                $('#rerunsuccess-modal').on('hidden.bs.modal', function (e) {
                    if ($localStorage.access_page === 'message') {
                        $("#sidetopbar").removeClass('modal-open');
                        $(".modal-backdrop").css("display", "none");
                        $localStorage.access_page = 'message_premium_ok';
                        $location.path('/message');
                    } else if ($localStorage.access_page === 'social') {
                        $("#sidetopbar").removeClass('modal-open');
                        $(".modal-backdrop").css("display", "none");
                        $localStorage.access_page = 'social_premium_ok';
                        $location.path('/social');
                    } else {
                        $scope.getCompanyInfo('1');
                    }
                })
            };
            
//            WHITE LABEL SETUP REDIRECTION
            $scope.openwhitelabelsetup = function(){
                if($scope.upgrade_status === 'W' || $scope.upgrade_status === 'WM' || $localStorage.whilelabel_setup_page_access == 'Y'){
                    $location.path('/whitelabelsetup');
                }else{
                    $("#failure-modal").modal('show');     
                }
            };
            
            //show select plan page
            if($localStorage.viewselectplanpage){
                $scope.planview = true;
                $scope.accountview = false;
                $scope.billingview = false;
                $localStorage.viewselectplanpage = false;
            }

        } else {
            $location.path('/login');
        }

    }
    module.exports = BillingStripeController;



