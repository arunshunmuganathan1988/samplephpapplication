ForgotPasswordController.$inject=['$scope','$location','$http','$localStorage','urlservice','$route','$rootScope'];
function ForgotPasswordController($scope,$location,$http,$localStorage,urlservice,$route,$rootScope)
    {
        $rootScope.hidesidebarlogin = true;
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        tooltip.hide();
       
        $scope.frgtPswdCancel = function(){
                $location.path('/login');
            };
            
        $scope.frgtPswdModalOk = function(){
                $location.path('/forgotpassword');
            };
            
          $scope.forgotPswdSubmit = function(){
              $('#progress-full').show();          
              $http({
                        method: 'POST',
                        url: urlservice.url+'forgotPassword',
                        data: {
                            "email":$scope.regtdemail
                        },
                        headers:{"Content-Type":'application/json; charset=utf-8',},
                        withCredentials: true
                        
                    }).then(function (response) {
                        if(response.data.status == 'Success'){
                            $('#progress-full').hide();
                            $localStorage.company_id =response.data.msg.company_id; 
                            $scope.regtdemail = "";
                            $scope.forgotpassword.$setPristine();
                            $scope.forgotpassword.$setUntouched();
                            $("#forgotpassword").modal('show');
                            $("#forgotpasswordtitle").text('Message');
                            $("#forgotpasswordcontent").text('Reset Password Url Link has been send to your Email ID.');
                        }else if(response.data.status === 'Expired'){
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();  
                        }else{
                            $scope.handleFailure(response.data.msg);
                        }   
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
          };   
        
    }
    module.exports = ForgotPasswordController;

