HomeController.$inject=['$scope','$compile', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope','$filter',  'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions'];
 function HomeController($scope,$compile, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter,  DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions) {
      

          if ($localStorage.islogin == 'Y') {
              
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().regComplete();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.hidesidebarlogin = false;
            $rootScope.adminsideBarHide = false;
            $scope.showedit= false;
            $rootScope.activepagehighlight = 'home';
            $localStorage.PreviewEventNamee="";
            $scope.editIndex="";
            $scope.editSingle="";
            $scope.message = "MyStudio";
            $scope.currentPage = 0;
            $scope.pageSize = 5;
            $scope.lastFilterNumber = $scope.pageSize;
            $scope.studentlist = [];
//            $scope.dtOptions = DTOptionsBuilder.newOptions()
//                    .withOption('scrollX', '100%')
//                    .withOption('order', [])
//                    .withOption('scrollY', '')
//                    .withOption('paging', false)
//                    .withOption('info', false);
            $scope.check_all_student = false;
            $scope.also_send_email = false;
            $scope.maxEmailAttachmentSizeError = false;
            $scope.filepreview = ""; 
            $scope.attachfile = '';
            $scope.file_name = '';
            $scope.fileurl = '';
            $scope.change_made = $scope.search_value = '';
            $scope.data_loaded_length = 0;
            //First time login message Pop-up ----START 
              $scope.selectedData = [];
            $scope.selected = {};
            $scope.selectAll = false;
                $scope.all_select_flag='N';
                  $scope.color_green='N';
            tooltip.hide();
//            $scope.hometip = true;
            
            if($localStorage.verfication_complete !== 'U'){
                if($localStorage.firstlogin){
                    if($localStorage.firstlogin_home === 'Y'){
                    $("#tooltipfadein").css("display", "block");
                    $localStorage.firstlogin_home = 'N';
                    $scope.hometip = false;                    
                    tooltip.pop("homelink", "#hometip", {position: 1});
                    }
                }
            }
            
            $("#homeNext").click(function(){  
                $("#tooltipfadein").css("display", "none");
                $scope.hometip = true;
                tooltip.hide();   
            });
             //First time login message Pop-up ----END
            
           
           
            
            $scope.updatestudent = function(sid,sname,slname,email){
            
                $scope.errortext = "";
                if (!sname || !slname) {
                    $scope.errortext = "Please Enter the student name";
                    return;
                }

                $scope.showedit = !$scope.showedit;
                $scope.updatestudentName(sid,sname,slname,email);
            };
            
            $scope.cancelstudent = function (svalue, ivalue) {
                $scope.errortext = "";
                $scope.showedit = !$scope.showedit;
                if (!svalue) {
//                    $scope.studentlist[ivalue].student_name = $scope.editSingle;
                    return;
                } else {
//                    $scope.studentlist[ivalue].student_name = $scope.editSingle;
                    return;
                }
            };
            
            $scope.getActiveUser = function () {
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getTotalUsers',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.Tstudent = response.data.msg.total_students.total_students;
                        $scope.Astudent = response.data.msg.total_active_students.total_active_students;
                        $scope.Cdate = response.data.msg.created_date.created_date;
                        $scope.CreatedDate = $scope.Cdate.substr(0, $scope.Cdate.indexOf(' '));

                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.getMembersDetails = function () {
                $('#progress-full').show();
                $scope.selected = {};
                $scope.selectAll = false;
                var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)">';
                var phone = '<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>';
                $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            method: 'POST',
                            url: urlservice.url + 'getstudent',
                            xhrFields: {
                                withCredentials: true
                             },
                            dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                "company_id": $localStorage.company_id,
                            },
                              error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {
                            $('#progress-full').show();
                        })
                        .withOption('fnDrawCallback', function () {
                            $('#progress-full').hide();
                        })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('order', [6, 'desc'])

                $scope.dtColumns = [
                    DTColumnBuilder.newColumn(null).withTitle(titleHtml).withClass('home_col5').notSortable()
                            .renderWith(function (data, type, full, meta) {
                                if ($scope.selectAll == true) {
                                    $scope.selected[full.id] = true;
                                } else {
                                    $scope.selected[full.id] = false;
                                    for (var id in  $scope.selected) {
                                        if ($scope.selectedData.length > 0) {
                                            for (var x = 0; x < $scope.selectedData.length; x++) {
                                                if (id === $scope.selectedData[x].id) {
                                                    $scope.selected[id] = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                            }),
                    DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('home_col6').renderWith(
                            function (data, type, full) {
                                if (data === 'Y')
                                    return  ' <a ng-click="sendindividualpushmail(\'' + full.id + '\' ,'+ "'push'" + ' );"> ' + phone + '</a>';
                                else
                                    return "";
                            }).notSortable(),
                    DTColumnBuilder.newColumn('student_name').withTitle('First Name').withClass('home_col').
                            renderWith(function (data, type, full, meta) {


                                return '<span  data-toggle="tooltip" title="' + data + '" ng-hide="showedit  && editIndex ==  ' + meta.row + ' "> ' + data + '</span>' +
                                        '<input type="text" ng-show="showedit  && editIndex == ' + meta.row + '"  ng-model="stud_name"/>'
                                        ;
                            }),
                    DTColumnBuilder.newColumn('student_lastname').withTitle('Last Name').withClass('home_col').
                            renderWith(function (data, type, full, meta) {


                                return '<span  data-toggle="tooltip" title="' + data + '" ng-hide="showedit  && editIndex ==  ' + meta.row + ' "> ' + data + '</span>' +
                                        '<input type="text" ng-show="showedit  && editIndex == ' + meta.row + '"  ng-model="stud_lastname"/>'
                                        ;
                            }),
                     DTColumnBuilder.newColumn('student_username').withTitle('User Name').withClass('home_col'),
                    DTColumnBuilder.newColumn('student_email').withTitle('Email').withClass('home_col1').renderWith(
                            function (data, type, full,meta) {
                                if(full.bounce_flag === 'N'){
                                    if(full.email_subscription_status === 'U'){
                                        return '<a class="buyername1" ng-hide="showedit  && editIndex ==  ' + meta.row + ' " data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'unsubscribed\',\''+ data +'\');"> <img src="image/mail_error.png"  width="20" height="20"> '+"(Unsubscribed) "+ full.student_email + '</a>'+
                                          '<input type="text" ng-show="showedit  && editIndex == ' + meta.row + '"  ng-model="stud_email"/>';
                                    }else{
                                        return '  <a class="buyername"  ng-hide="showedit  && editIndex ==  ' + meta.row + ' " data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpushmail(\'' + full.id + '\' ,' + "'mail'" + ');"> ' + full.student_email + '</a>' +
                                            '<input type="text" ng-show="showedit  && editIndex == ' + meta.row + '"  ng-model="stud_email"/>';
                                    }
                                }else{
                                    return '<a class="buyername1" ng-hide="showedit  && editIndex ==  ' + meta.row + ' " data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.type +'\',\''+ data +'\');"> <img src="image/mail_error.png"  width="20" height="20"> '+"(" + full.type +") "+ full.student_email + '</a>'+
                                          '<input type="text" ng-show="showedit  && editIndex == ' + meta.row + '"  ng-model="stud_email"/>';
                                }                                     
                            }),
                    DTColumnBuilder.newColumn('first_login_dt').withTitle('First Login').withClass('home_col2'),
                    DTColumnBuilder.newColumn('last_login_dt').withTitle('Last Login').withClass('home_col3'),
                    DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('home_col4').notSortable()
                            .renderWith(function (data, type, full, meta, index) {
                                return '<a class="card-icon"><i class="fa fa-2x fa-pencil" ng-hide="showedit  && editIndex == ' + meta.row + '" ng-click="editstudent(\'' + full.student_name + '\' ,\'' + meta.row + '\' ,\'' + full.student_lastname + '\' ,\'' + full.student_email + '\')"></i></a>' +
                                        '<a class="card-icon"><i class="fa fa-2x fa-trash" ng-hide="showedit && editIndex == ' + meta.row + ' " ng-click="deletestudent(\'' + data.id + '\' ,\'' + meta.row + '\' )"></i></a>' +
                                        ' <a class="card-icon"><i class="fa fa-2x fa-close" ng-show="showedit && editIndex == ' + meta.row + '" ng-click="cancelstudent(\'' + data.id + '\' ,\'' + meta.row + '\')"></i></a>' +
                                        '<button class="col-md-3 btn btn-primary savebutton-small" style="margin-top: 7px;" ng-show="showedit  && editIndex == ' + meta.row + '" ng-click="updatestudent(\'' + full.id + '\' ,' + 'stud_name' +','+ 'stud_lastname' + ',' + 'stud_email' + ' )" ng-disabled="!stud_name || !stud_lastname || !stud_email">Save</button>'
                                        ;
                            })
                ];
                $scope.dtInstance = {};
                $('#progress-full').hide();
                 angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })

            };  
              $scope.getMembersDetails();
              $scope.getActiveUser();
              
              
              $scope.showemailerror = function (row,type,data) {
                 $scope.errormsg=$scope.paymentdetails_array[row].error;
                 $scope.bouncedemail=data;
                if (type === 'Bounce') {
                    $("#emailerrormessageModal").modal('show');
                    $("#emailerrormessagetitle").text('Message');
                    $("#emailerrormessagecontent").text( $scope.errormsg);
                }else if (type === 'unsubscribed') {
                    $scope.unsubscribe_email = data;
                    $("#sendsubscriptioninviteModal").modal('show');
                } else {
                    $("#emailerrormessageModal1").modal('show');
                    $("#emailerrormessagetitle1").text('Message');
                    $("#emailerrormessagecontent1").text( $scope.errormsg);
                }
            }
            
            $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text('Student email unblocked successfully ');
//                        $scope.getMembersDetails();
                        $scope.dtInstance.rerender();
                        $scope.getActiveUser();
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };

               $scope.editstudent = function (svalue,row,slvalue,email) {
                   $scope.stud_name=svalue;
                   $scope.stud_email=email;
                   $scope.stud_lastname=slvalue;
                if ($scope.showedit) {
                    //alert('true');
                }
                else {
                    this.showedit = !this.showedit;
                    this.editIndex = row;
                    $scope.editSingle = svalue;
                }
            };
              
              
                $scope.toggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
//                            $scope.selectedData.push({"id":id});
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
            };
            
          
            
          
           $scope.toggleOne= function (selectedItems) {
               $scope.maintainselected = selectedItems;
                var check=0;
                  $scope.all_select_flag='N';
                   $scope.color_green='N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        }else{
                            $scope.selectedData.push({"id":id});
                             $scope.color_green='Y';
                        }
                         
                    }
                }
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
            }
            
            
            $scope.numberOfPages = function () {   
//                if($scope.currentPage > $scope.lastpage){
//                    $scope.currentPage=0;
//                }
                if($scope.lastFilterNumber != $scope.pageSize){
                    $scope.currentPage=0;
                }
                $scope.lastFilterNumber = $scope.pageSize;                   
                $scope.lastpage = Math.ceil($scope.studentlist.length / $scope.pageSize);
                return Math.ceil($scope.studentlist.length / $scope.pageSize);
                
            }; 
            
            $scope.updatestudentName = function (sid,s,sl,e) {
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'updatestudent',
                   params: {
                        "student_id": sid,
                        "student_name":s,
                        "student_lastname":sl,
                        "student_email":e,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text('Student name successfully Updated');
                        $scope.studentlist = response.data.msg.student_details;
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            
         //Delete Student content
            $scope.deletestudent = function (sid,indvalue) {
                $scope.deleteindvalue = indvalue;
                $scope.stud_id = sid;
                $("#studentDeleteModal").modal('show');
                $("#studentDeletetitle").text('Delete');
                $("#studentDeletecontent").text("Are you sure want to delete student? ");
            };
            
            $scope.deletestudentConfirm = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteStudent',
                    data: {
                        "student_id": $scope.stud_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text('Student successfully deleted');
//                        $scope.getMembersDetails();
                        $scope.dtInstance.rerender();
                        $scope.getActiveUser();
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            $scope.clearMessageContent = function () {
                $scope.pushmsgdata = $scope.emailsubjectdata = '';
                $scope.removeAttachfile();
            };
            
            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
            
            
            
            
            $scope.clearSelectedStudentList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                $scope.individual_check_student = $scope.check_all_student = false;
                if($scope.clearfromindividual){//clear if modal open from individual
                   $scope.selectedData = [];
                }
                $scope.clearfromindividual = false; 
            }; 
            
           
            
            $scope.sendindividualpushmail = function (selectedMember, type) {  // INDIVIDUAL PUSH / EMAIL MESSAGE
                $scope.clearfromindividual = true; 
                $scope.message_type = type;
//                $scope.clearSelectedStudentList();
                $scope.selectAll = false;
//                $scope.selectedData = [];
                $scope.toggleAll(false,$scope.maintainselected);
                $scope.message_type = type;
                $scope.selectedData = [];
                $scope.selectedData.push({"id":selectedMember});
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#grpstudentpushmessagecontModal").modal('show');
                    $("#grpstudentpushmessageconttitle").text('Send Push Message');
                } else if (type === 'mail'){
                    $("#grpstudentemailmessagecontModal").modal('show');
                    $("#grpstudentemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            }
            
            $scope.sendGroupMsg = function (type) { 
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 ||  $scope.all_select_flag=='Y') {
                    if (type === 'push') {
//                        for (i = 0; i < $scope.selectedstudentlist.length; i++) {
//                            if ($scope.selectedstudentlist[i].show_icon === 'N') {
//                                $("#cusmessageModal").modal('show');
//                                $("#cusmessagetitle").text('Message');
//                                $("#cusmessagecontent").text('One of the selected member doesnot have registered mobile number to receive push message. Kindly deselect the member who didnot registered mobile number.');
//                                return;
//                            } else {
//                                //SELECTED MEMBER HAS MOBILE DEVICE
//                            }
//                        }
                        $("#grpstudentpushmessagecontModal").modal('show');
                        $("#grpstudentpushmessageconttitle").text('Send Push Message');
                    } else if (type === 'mail') {
                        $("#grpstudentemailmessagecontModal").modal('show');
                        $("#grpstudentemailmessageconttitle").text('Send Email Message');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if (type === 'csv') {
                        $("#grpstudentcsvdownloadModal").modal('show');
                        $("#grpstudentcsvdownloadtitle").text('Message');
                        $("#grpstudentcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
                } else {
                    var x = document.getElementById("snackbar")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            }

            $scope.sendGroupEmailPush = function (message_type) {
                $('#progress-full').show();
                var msg = '';
                var subject,unsubscribe_email = '';
                if (message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if (message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if(message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendStudentGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_student_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "select_all_flag": $scope.all_select_flag,
                        "search": $scope.searchTerm,
                        "unsubscribe_email":unsubscribe_email
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        
                    if (response.data.status === 'Success') {  
                        $('#progress-full').hide();
                        $scope.clearSelectedStudentList();
                        $('#progress-full').hide();                        
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $("#grpstudentemailmessagecontModal").modal('hide');
                        $("#grpstudentemailmessageconttitle").text('');
                        $("#grpstudentpushmessagecontModal").modal('hide');
                        $("#grpstudentpushmessageconttitle").text('');
                        $("#hommessageModal").modal('show');
                        $("#hommessagetitle").text('Message');
                        $("#hommessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Failed') {
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                         $("#grpstudentemailmessagecontModal").modal('hide');
                        $("#grpstudentemailmessageconttitle").text('');
                        $("#grpstudentpushmessagecontModal").modal('hide');
                        $("#grpstudentpushmessageconttitle").text('');
                         $scope.clearSelectedStudentList();
                        $("#cusmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Invalid server response');
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.exportStudentDetails = function (){
                function formatTodayDate() {
                    var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'exportStudentDetails',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_student_list": $scope.selectedData,
                        "select_all_flag": $scope.all_select_flag,
                        "search":$scope.searchTerm
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.msg);
                        return false;
                    } else {
                        $('#progress-full').hide();
                        $("#hommessageModal").modal('show');
                        $("#hommessagetitle").text('Message');
                        $("#hommessagecontent").text("Student Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "StudentDetails_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearSelectedStudentList();
                    
                }).error(function () {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            
//            
        } else {
            $location.path('/login');
        }
        
    } 
    module.exports =  HomeController;




