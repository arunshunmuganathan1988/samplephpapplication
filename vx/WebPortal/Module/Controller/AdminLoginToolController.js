

AdminLoginToolController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice',  '$filter', '$route','$rootScope'];
function  AdminLoginToolController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route,$rootScope) {
if($localStorage.isadminlogin === 'Y') {
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        $rootScope.hidesidebarlogin = false;
        $rootScope.activepagehighlight = 'adminlogintool';
        $rootScope.admincmpnynme = $localStorage.admincmpnynme;
        tooltip.hide();
        $scope.userProfileLogin = function () {
                $localStorage.secondStage = false;
                $localStorage.thirdStage = false;
                $('#progress-full').show();

                $http({
                    method: 'POST',
                    url: urlservice.url + 'login',
                    data: {
                        "email": $scope.useremailadrs,
                        "login_from": "adminpanel"
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
//                        $scope.newsignup = false;
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $localStorage.company_id = response.data.msg.company_id;
                        $localStorage.user_email_id = $scope.useremailadrs;
                        if (response.data.msg.company_code == null) {
                            // app code stage .may be closed before the studio code screen.
                            $localStorage.thirdStage = true;
                            $scope.openRegistration();
                            return;
                        }
                        $localStorage.studio_code = response.data.msg.company_code;
                        $localStorage.phone_number = response.data.msg.phone_number;
                        $localStorage.secondStage = false;
                        $localStorage.thirdStage = false;
                        $localStorage.islogin = 'Y';

                        $localStorage.student_id = response.data.msg.student_id;
                        $localStorage.user_id = response.data.msg.user_id;
                        $localStorage.event_flag = response.data.msg.event_flag;
                        $localStorage.event_b_id = response.data.msg.eventbrite_id;
                        $localStorage.event_b_url = response.data.msg.eventbrite_url;
                        if ($localStorage.event_flag == "E" || $localStorage.event_flag == null) {
                            $localStorage.sideEvent = false;
                        } else {
                            $localStorage.sideEvent = true;
                        }

                        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
                        $localStorage.loginDetails = response.data.msg;
                        $localStorage.subscription_status = response.data.msg.subscription_status;
                        $localStorage.upgrade_status = response.data.msg.upgrade_status;
                        $localStorage.user_id = response.data.msg.user_id;
                        $localStorage.wepay_level = response.data.msg.wp_level;
                        $localStorage.wepay_client_id = response.data.msg.wp_client_id;
                        $localStorage.regstr_complete = response.data.msg.registration_complete;
                        $localStorage.verfication_complete = response.data.msg.verification_complete;
                        $localStorage.processing_fee_percentage = response.data.msg.processing_fee_percentage;
                        $localStorage.processing_fee_transaction = response.data.msg.processing_fee_transaction;
                        $localStorage.wp_currency_code = response.data.msg.wp_currency_code;
                        $localStorage.wp_currency_symbol = response.data.msg.wp_currency_symbol;
                        $localStorage.plan_processing_fee = response.data.msg.plan_processing_fee.all_processing_fee;
                        $localStorage.studio_expiry_level = response.data.msg.studio_expiry_level;
                        
                        // STRIPE CRENDENTIALS
                        $localStorage.stripe_upgrade_status = response.data.msg.stripe_upgrade_status;
                        $localStorage.stripe_subscription = response.data.msg.stripe_subscription;
                        $localStorage.stripe_publish_key = response.data.msg.stripe_publish_key;
                        $localStorage.stripe_country_support = response.data.msg.stripe_country_support;

                        //  Stripe alert text show when charges not enabled
                        $localStorage.stripe_status = $localStorage.loginDetails.stripe_status;
                        $localStorage.stripe_charges_enabled = $localStorage.loginDetails.stripe_charges_enabled;
                        
                    if (($localStorage.studio_expiry_level === 'L1' || $localStorage.studio_expiry_level === 'L2') || ($localStorage.subscription_status === 'N' &&   $localStorage.upgrade_status === 'F')) {
                            $rootScope.sideBarHide = false;
                            $rootScope.hidesidebarlogin = false;
                            $rootScope.adminsideBarHide = false;
                            $scope.studioexpired();
                        } else {
                            $scope.dashboard();
                        }
                        $("#homelink").addClass('active');
//                        $location.path('/home');
                    } else if (response.data.status === 'Expired') {
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleAdminError(response.data);
                });

            };
             //Delete user account
            $scope.deleteuser = function () {
                $("#userDeleteModal").modal('show');
                $("#userDeletetitle").text('Delete');
                $("#userDeletecontent").text("Are you sure want to delete user account? ");
            };

            $scope.deleteuserCancel = function () {
                console.log('cancel delete');
            };
            
            if($localStorage.redirectedfromwl){
                $scope.useremailadrs = $localStorage.redirectloginid;
                $localStorage.redirectedfromwl = false;
            }
            
            $scope.deleteuserConfirm = function () {

                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteUserAccount',
                    data: {
                        "email": $scope.useremailadrs
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.useremailadrs = '';
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $scope.adminlogout();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg); 
                    } else {
                        console.log(response.data);
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleAdminError(response.data);
                });
            };
         
    }else {
            $location.path('/adminpanel');
    }
        
}
module.exports = AdminLoginToolController;