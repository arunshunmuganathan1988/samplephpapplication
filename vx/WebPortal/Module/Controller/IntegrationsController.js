IntegrationsController.$inject=['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', '$window','$rootScope'];
 function IntegrationsController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route, $window,$rootScope) {
        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'integrations';
            $scope.upgrade_status = $localStorage.upgrade_status; // for inactive studio-registration block
            $scope.zapierview = true;
            $scope.customAPIview = $scope.zapierurlerror = false;;
            $scope.month_list = ['January','Febrauary','March','April','May','June','July','August','September','October','November','December'];
            $scope.zap_event_type = '';
            $scope.zap_event_type_add = $scope.zap_event_type_update = 'N';
            
            if(window.location.hostname === 'www.mystudio.academy'){
                $scope.zapActionUrl = 'https://www.mystudio.academy/Api/v1/Leads/';
                $scope.zapRetrievepollUrl = 'https://www.mystudio.academy/Api/v1/Leads';
            }else if(window.location.hostname === 'stage.mystudio.academy'){
                $scope.zapActionUrl = 'http://stage.mystudio.academy/Api/v1/Leads/';
                $scope.zapRetrievepollUrl = 'http://stage.mystudio.academy/Api/v1/Leads';
            }else if(window.location.hostname === 'dev2.mystudio.academy'){
                $scope.zapActionUrl = 'http://dev2.mystudio.academy/Api/v1/Leads/';
                $scope.zapRetrievepollUrl = 'http://dev2.mystudio.academy/Api/v1/Leads';
            }else{
                $scope.zapActionUrl = 'http://dev.mystudio.academy/Api/v1/Leads/';
                $scope.zapRetrievepollUrl = 'http://dev.mystudio.academy/Api/v1/Leads';
            }
            
            $scope.zap_details_list = [];
            $scope.openCustomAPI = function(){
                $scope.customAPIview = true;
                $scope.zapierview = false;
                $scope.resettabbarclass();
                $scope.getCustomAPIkeyDetails();
                $('#li-customAPI').addClass('active-event-list').removeClass('event-list-title');
            };
            $scope.openZapier = function(){
                $scope.zapierview = true;
                $scope.customAPIview = false;
                $scope.resettabbarclass();
                $scope.getZapierDetails();
                $('#li-zapier').addClass('active-event-list').removeClass('event-list-title');
            };
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-integrations').addClass('active-event-list').removeClass('event-list-title');
            };
            
            $scope.openAPIkeymodal = function(){
                $('#createAPIkey-modal').modal('show');
            };
            $scope.clearCreateCustomAPIkey = function(){
                $scope.customAPI_name  = '';
            };
            $scope.deleteApIkey = function(customlist){
                $scope.deleteCustomAPIkey = customlist.api_key;
                $scope.deleteCustomAPIname = customlist.integration_name;
                $('#deleteAPIkey-modal').modal('show');
            };
            $scope.cancelDeleteAPIkey = function(){
                $('#deleteAPIkey-modal').modal('hide');
            };
            
            $scope.toastIntegrations = function (integrationtype) {   
                var id_name = "";
                if(integrationtype === 'custom_api'){
                   id_name = "custom_api";
                }else if(integrationtype === 'zap_api'){
                   id_name = "zap_api";
                }else if(integrationtype === 'zap_retrievepoll'){
                   id_name = "zap_retrievepoll";
                }else{
                   id_name = "zap_url";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            $scope.openZapTriggerAddmodal = function(){
                
                if ($scope.upgrade_status === 'T' || $scope.upgrade_status === 'B' || $scope.upgrade_status === 'F') {
                    $('#zapBillingModal').modal('show'); //Move either premium or whitelabel
                }else if ($scope.upgrade_status === 'M' || $scope.upgrade_status === 'P'){
                    if($scope.zap_details_list.length < 2){
                        $('#selectzaptrigger-modal').modal('show');
                    }else{
                        $('#zapBillingModal').modal('show');//Move to white label
                    }
                }else if($scope.upgrade_status === 'WM' || $scope.upgrade_status === 'W'){
                    if($scope.zap_details_list.length < 5){
                        $('#selectzaptrigger-modal').modal('show');
                    }else{
                        $('#zapBillingModal').modal('show');//only 5 zaps you can create
                    }
                }
            };
            $scope.openRetrievePollmodal = function(){
                $('#retrievepollzaptrigger-modal').modal('show');
            };
            $scope.openCatchHookmodal = function(){
                $('#createzaptrigger-modal').modal('show');
            };
            $scope.clearCreateZapTrigger = function(){
                $scope.customAPI_name  = '';
            };
            $scope.getCustomAPIkeyDetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url+'getCustomAPIKeyDetails',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $('#progress-full').hide();
                        $scope.custom_API_list = response.data.list;
                        $scope.companytimezone = response.data.tz;
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
                        
            $scope.getZapierDetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url+'getZapierDetails',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
                        $('#progress-full').hide();
                        $scope.zapAPIKey = response.data.api_key;
                        $scope.zap_details_list = (response.data.list === undefined ? [] : response.data.list);
                        $scope.companytimezone = response.data.tz;
                    }else if(response.data.status === 'Expired'){
                         $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else{
                        $scope.handleFailure(response.data.msg);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            $scope.getZapierDetails();
            
            $scope.deleteZapierTrigger = function(zaplist){
                $scope.del_target_url = zaplist.target_url;
                $('#deletezaptrigger-modal').modal('show');
            };
            $scope.createCustomAPIkey = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createCustomAPIkey',
                    data: {
                        "company_id": $localStorage.company_id,
                        "custom_api_name":$scope.customAPI_name,
                        "api_type":'C'
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $('#createAPIkey-modal').modal('hide');
                        $scope.customAPI_name = '';
                        $scope.custom_API_list = response.data.list;
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text('Custom API key successfully created.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.confirmDeleteCustomAPIkey = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteCustomAPIkey',
                    data: {
                        "company_id": $localStorage.company_id,
                        "delete_api_key":$scope.deleteCustomAPIkey,
                        "delete_api_name":$scope.deleteCustomAPIname
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $('#deleteAPIkey-modal').modal('hide');
                        $scope.custom_API_list = response.data.list;
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text('Custom API key successfully deleted.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            $scope.catchzaptargeturl = function () {
                $scope.defaultzapierurl = 'https://hooks.zapier.com/hooks/catch/';
                if($scope.zaptargeturl){
                    if($scope.zaptargeturl.indexOf($scope.defaultzapierurl) === -1)
                        $scope.zapierurlerror = true;
                    else
                        $scope.zapierurlerror = false;
                }
            };
            $scope.addZapierTrigger = function () {
                if(($scope.zap_event_type_add === 'Y') && ($scope.zap_event_type_update === 'N')){
                    $scope.zap_event_type = 'A'; //Add only
                }
                if(($scope.zap_event_type_add === 'N')&& ($scope.zap_event_type_update === 'Y')){
                    $scope.zap_event_type = 'U'; //Update only
                }
                if(($scope.zap_event_type_add === 'Y') && ($scope.zap_event_type_update === 'Y')){
                    $scope.zap_event_type = 'B'; //Add and Update
                }
               
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'addZapierTrigger',
                    data: {
                        "company_id": $localStorage.company_id,
                        "target_url":$scope.zaptargeturl,
                        "zap_name":$scope.zapname,
                        "zap_event_type":$scope.zap_event_type,
                        "zap_category_type": 'L',
                        "zap_api_key":$scope.zapAPIKey
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $('#progress-full').hide();
                        $scope.zap_details_list = (response.data.list.list === undefined ? [] : response.data.list.list);
                        $scope.cancelZapierTrigger();
                        $("#createzaptrigger-modal").modal('hide');
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            $scope.cancelZapierTrigger = function(){
                $scope.zapierurlerror = false;
                $scope.zapname = $scope.zaptargeturl = $scope.zap_event_type = '';
                $scope.zap_event_type_add = $scope.zap_event_type_update = 'N';
            };
            $scope.sendSampleTriggerData = function(zap_list){
                $scope.test_target_url = zap_list.target_url;
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'testZapTrigger',
                    data: {
                        "company_id": $localStorage.company_id,
                        "test_target_url":$scope.test_target_url
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                        "Access-Control-Allow-Origin": "*"
                    }
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.confirmDeleteZapierTrigger = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteZapierTrigger',
                    data: {
                        "company_id": $localStorage.company_id,
                        "del_target_url":$scope.del_target_url
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#deletezaptrigger-modal").modal('hide');
                        $scope.zap_details_list = (response.data.list.list === undefined ? [] : response.data.list.list);
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#integrationsmessageModal").modal('show');
                        $("#integrationsmessagetitle").text('Message');
                        $("#integrationsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            $scope.paymentnow = function () {
                $("#sidetopbar").removeClass('modal-open');
                $(".modal-backdrop").css("display", "none");
                $(".modal-backdrop").css("display", "none");
                $localStorage.viewselectplanpage = true;
                $location.path('/billingstripe');
            };
            function formatAMPM(date) {
                var date = new Date(date);
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                if(hours===0){
                    hours=12;
                }
                hours = hours < 10 ? '0' + hours : hours; // the hour '0' should be '12'
                minutes = (minutes < 10 && minutes >= 0) ? '0' + minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return strTime;
            }
            function formatAMPMSafaritime(ddate) {
                var dda = ddate.toString();
                var ddarray = dda.split(' ');
                var time = ddarray[1];
                var ddtime=time.split(':');
                var hour=parseInt(ddtime[0]);
//                var hour = temp_hour < 10 ? '0' + temp_hour : temp_hour;
                var temp_minutes = ddtime[1];
//                var minutes = (temp_minutes < 10 && temp_minutes > 0) ? '0' + temp_minutes : temp_minutes;
                var minutes='';
                if(parseInt(temp_minutes) < 10){
                   minutes = '0' + temp_minutes; 
                }else{
                   minutes = temp_minutes; 
                }
                // format AM PM start
                var ampm = hour >= 12 ? 'PM' : 'AM';
                var hour_format = hour % 12;
                if(hour_format===0){
                    hour_format=12;
                }
                var local_hour = '';
                if(parseInt(hour_format) < 10){
                   local_hour = '0' + hour_format; 
                }else{
                   local_hour = hour_format; 
                }
//                var local_hour = parseInt(hour_format) < 10 ? '0' + hour_format : hour_format; // the hour '0' should be '12'
                var localTime = local_hour + ':' + minutes + ' ' + ampm;
                return localTime;
            }

            $scope.toDisplayDateFormat = function(displaydate){
                var fulldate;
                var time;
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    time = formatAMPMSafaritime(displaydate);
                    fulldate = displaydate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                } else {
                    time = formatAMPM(displaydate);
                    fulldate = displaydate.replace(/GMT.*/g, "");
                    fulldate = fulldate.slice(0, -9);
                }
                var splitdate = fulldate.split('-');
                var displayformat = $scope.month_list[parseInt(splitdate[1]-1)]+' '+splitdate[2]+', '+splitdate[0];
                return "created on <b> "+ displayformat + "</b> at <b>"+ time +" " +$scope.companytimezone + " </b>";
            };
        }
    }
    module.exports = IntegrationsController;
