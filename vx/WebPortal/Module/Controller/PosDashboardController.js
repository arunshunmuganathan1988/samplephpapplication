/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

PosDashboardController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope'];
 function PosDashboardController($scope, $location, $http, $localStorage, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'pos';
            tooltip.hide(); 
            $scope.posdashboardview = true;
            $scope.possettingsview = false;
            $scope.miscswitch= true;
            $scope.retailswitch= true;
            $scope.eventswitch= true;
            $scope.membershipswitch= true;
            $scope.trialswitch= true;
            $scope.leadsswitch= true;
            $scope.misc_cash_flag= $scope.misc_check_flag=$scope.retail_cash_flag=$scope.retail_check_flag=$scope.event_cash_flag=$scope.event_check_flag=$scope.trial_cash_flag=$scope.trial_check_flag=false;
            $scope.misc_ach_flag = $scope.retail_ach_flag = $scope.event_ach_flag = $scope.trial_ach_flag = $scope.membership_ach_flag = false;
            $scope.membership_cash_flag = $scope.membership_check_flag = false;
            $scope.processing_fee_type = 'Pass fees on';
            $scope.include_email_type = 'Include in auto-generated emails';
            $scope.dashboard_formchange = false;
            //change value of drop downs
            $scope.pfo = function(){
                $scope.dashboard_formchange = true;
                $scope.processing_fee_type = 'Pass fees on';
            };
            $scope.afo = function(){
                $scope.dashboard_formchange = true;
                $scope.processing_fee_type = 'Absorb fees on';
            };
            $scope.include_email = function(){
                $scope.dashboard_formchange = true;
                $scope.include_email_type = 'Include in auto-generated emails';
            };
            $scope.exclude_email = function(){
                $scope.dashboard_formchange = true;
                $scope.include_email_type = 'Exclude in auto-generated emails';
            };
            $scope.openagreementModal = function(){
                $('#miscAgreement').modal('show');
            };
            $scope.closemodal = function(){
                $('#miscAgreement').modal('hide');
            };
            $scope.goposdashboardview = function(){
                $scope.posdashboardview = true;
                $scope.possettingsview = false;
                $('#li-onhold').removeClass('active-event-list').addClass('event-list-title');
                $('#li-active').addClass('active-event-list').removeClass('event-list-title');
            };
            
            $scope.gopossettingsview = function(){
                $scope.posdashboardview = false;
                $scope.possettingsview = true;
                $('#li-active').removeClass('active-event-list').addClass('event-list-title');
                $('#li-onhold').addClass('active-event-list').removeClass('event-list-title');
            };
            $scope.toastPOS = function (postype) {   
                var id_name = "";
                if(postype === 'staff'){
                   id_name = "staffpos";
                }else{
                   id_name = "publicpos";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            $scope.launchPOS = function (user_type) {
                var redirect_url;
                if(user_type === 'staff'){
                    redirect_url = $scope.posstaffurl.replace('/c/', '/l/');
                }else if(user_type === 'public'){
                    redirect_url = $scope.pospublicurl.replace('/c/', '/l/');
                }
                $scope.alleditClosed();
                $scope.menuhide();
                $localStorage.pos_company_id = $localStorage.company_id;
                $localStorage.pos_user_email_id = $localStorage.user_email_id; 
                window.open(redirect_url, '_blank');
            };
            
            $scope.catchretailswitch = function(){
                console.log($scope.controlpanel_retail_status);
                if($scope.controlpanel_retail_status == 'N'){
                    console.log("enterd");
                    $scope.retailswitch = false;
                    $('#controlpanelvalidation-modal').modal('show'); 
                }
            };
             $scope.openRetDisSupport = function(){
                window.open('https://intercom.help/mystudio/collections/1800850-point-of-sale-system',"_blank");
            };
            
            $scope.saveposchanges = function(type){
                $('#progress-full').show();
                    $scope.miscstatus = ($scope.miscswitch) ? 'E' : 'D';
                    $scope.retailstatus = ($scope.retailswitch) ? 'E' : 'D';
                    $scope.eventstatus = ($scope.eventswitch) ? 'E' : 'D';
                    $scope.membershipstatus = ($scope.membershipswitch) ? 'E' : 'D';
                    $scope.trialstatus = ($scope.trialswitch) ? 'E' : 'D';
                    $scope.leadsstatus = ($scope.leadsswitch) ? 'E' : 'D';
                    $scope.misc_payment_code = '1' + (($scope.misc_cash_flag) ? '1' : '0') + (($scope.misc_check_flag) ? '1' : '0') + (($scope.misc_ach_flag) ? '1' : '0');
                    $scope.retail_payment_code = '1' + (($scope.retail_cash_flag) ? '1' : '0') + (($scope.retail_check_flag) ? '1' : '0') + (($scope.retail_ach_flag) ? '1' : '0');
                    $scope.event_payment_code = '1' + (($scope.event_cash_flag) ? '1' : '0') + (($scope.event_check_flag) ? '1' : '0') + (($scope.event_ach_flag) ? '1' : '0');
                    $scope.membership_payment_code = '1' + (($scope.membership_cash_flag) ? '1' : '0') + (($scope.membership_check_flag) ? '1' : '0') + (($scope.membership_ach_flag) ? '1' : '0');
                    $scope.trial_payment_code = '1' + (($scope.trial_cash_flag) ? '1' : '0') + (($scope.trial_check_flag) ? '1' : '0') + (($scope.trial_ach_flag) ? '1' : '0');
                    $scope.leads_payment_code = '000';
                $http({
                    method: 'POST',
                    url: urlservice.url + 'savepossettings',
                    data: {
                        'company_id': $localStorage.company_id,
                        'miscstatus': $scope.miscstatus,
                        'retailstatus': $scope.retailstatus,
                        'eventstatus': $scope.eventstatus,
                        'membershipstatus': $scope.membershipstatus,
                        'trialstatus': $scope.trialstatus,
                        'leadsstatus': $scope.leadsstatus,
                        'misc_processingfee_type':($scope.processing_fee_type === 'Pass fees on' ? '2' : '1'),
                        'leads_email_status':($scope.include_email_type === 'Include in auto-generated emails'? 'I' : 'E'),
                        'misc_payment_code':$scope.misc_payment_code,
                        'retail_payment_code':$scope.retail_payment_code,
                        'event_payment_code':$scope.event_payment_code,
                        'membership_payment_code':$scope.membership_payment_code,
                        'trial_payment_code': $scope.trial_payment_code,
                        'leads_payment_code': $scope.leads_payment_code,
                        'misc_agreement': $scope.misc_agreement,
                        'type': type
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#miscAgreement').modal('hide'); 
                        $('#progress-full').hide();
                        $('#savepos-modal').modal('show');
                        $("#saveposmsg").text(response.data.msg);
                        if(type === 'Agreement'){
                            $scope.misc_agreement = response.data.possettings.misc_sales_agreement;
                        }else{
                            $scope.posstaffurl = response.data.staff_url;
                            $scope.pospublicurl = response.data.public_url;
                            $scope.miscswitch = (response.data.possettings.misc_charge_status === 'E' ? true : false);
                            $scope.retailswitch = (response.data.possettings.retail_status === 'E' ? true : false);
                            $scope.eventswitch = (response.data.possettings.event_status === 'E' ? true : false);
                            $scope.membershipswitch = (response.data.possettings.membership_status === 'E' ? true : false);
                            $scope.trialswitch = (response.data.possettings.trial_status === 'E' ? true : false);
                            $scope.leadsswitch = (response.data.possettings.leads_status === 'E' ? true : false);
                            $scope.misc_cash_flag = (response.data.possettings.misc_payment_method[1] === '1' ? true : false);
                            $scope.misc_check_flag = (response.data.possettings.misc_payment_method[2] === '1' ? true : false);          
                            $scope.misc_ach_flag = (response.data.possettings.misc_payment_method[3] === '1' ? true : false);          
                            $scope.retail_cash_flag = (response.data.possettings.retail_payment_method[1] === '1' ? true : false);
                            $scope.retail_check_flag = (response.data.possettings.retail_payment_method[2] === '1' ? true : false);   
                            $scope.retail_ach_flag = (response.data.possettings.retail_payment_method[3] === '1' ? true : false);   
                            $scope.event_cash_flag = (response.data.possettings.event_payment_method[1] === '1' ? true : false);
                            $scope.event_check_flag = (response.data.possettings.event_payment_method[2] === '1' ? true : false);             
                            $scope.event_ach_flag = (response.data.possettings.event_payment_method[3] === '1' ? true : false);             
                            $scope.trial_cash_flag = (response.data.possettings.trial_payment_method[1] === '1' ? true : false);
                            $scope.trial_check_flag = (response.data.possettings.trial_payment_method[2] === '1' ? true : false);
                            $scope.trial_ach_flag = (response.data.possettings.trial_payment_method[3] === '1' ? true : false);
                            $scope.membership_cash_flag = (response.data.possettings.membership_payment_method[1] === '1' ? true : false);
                            $scope.membership_check_flag = (response.data.possettings.membership_payment_method[2] === '1' ? true : false);
                            $scope.membership_ach_flag = (response.data.possettings.membership_payment_method[3] === '1' ? true : false);
                            $scope.include_email_type = (response.data.possettings.leads_email_status === 'I' ? 'Include in auto-generated emails' : 'Exclude in auto-generated emails');
                            $scope.processing_fee_type = (response.data.possettings.misc_charge_fee_type === '1' ? 'Absorb fees on' : 'Pass fees on');
                            $scope.dashboard_formchange = false;
                            $scope.possettings_form.$setPristine(true); //set the form to pristine state $scope.formName.$setUntouched(true);
                            $scope.possettings_form.$setUntouched(true); 
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.getsettingdetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getpossettingdetails',
                    params: {
                        'company_id': $localStorage.company_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8'},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $scope.posstaffurl = response.data.staff_url;
                        $scope.pospublicurl = response.data.public_url;
                        $scope.misc_agreement = response.data.possettings.misc_sales_agreement;
                        $scope.miscswitch = (response.data.possettings.misc_charge_status === 'E' ? true : false);
                        $scope.retailswitch = (response.data.possettings.retail_status === 'E' ? true : false);
                        $scope.eventswitch = (response.data.possettings.event_status === 'E' ? true : false);
                        $scope.membershipswitch = (response.data.possettings.membership_status === 'E' ? true : false);
                        $scope.trialswitch = (response.data.possettings.trial_status === 'E' ? true : false);
                        $scope.leadsswitch = (response.data.possettings.leads_status === 'E' ? true : false);
                        $scope.misc_cash_flag = (response.data.possettings.misc_payment_method[1] === '1' ? true : false);
                        $scope.misc_check_flag = (response.data.possettings.misc_payment_method[2] === '1' ? true : false);
                        $scope.misc_ach_flag = (response.data.possettings.misc_payment_method[3] === '1' ? true : false);         
                        $scope.retail_cash_flag = (response.data.possettings.retail_payment_method[1] === '1' ? true : false);
                        $scope.retail_check_flag = (response.data.possettings.retail_payment_method[2] === '1' ? true : false);     
                        $scope.retail_ach_flag = (response.data.possettings.retail_payment_method[3] === '1' ? true : false);     
                        $scope.event_cash_flag = (response.data.possettings.event_payment_method[1] === '1' ? true : false);
                        $scope.event_check_flag = (response.data.possettings.event_payment_method[2] === '1' ? true : false);           
                        $scope.event_ach_flag = (response.data.possettings.event_payment_method[3] === '1' ? true : false);           
                        $scope.trial_cash_flag = (response.data.possettings.trial_payment_method[1] === '1' ? true : false);
                        $scope.trial_check_flag = (response.data.possettings.trial_payment_method[2] === '1' ? true : false);
                        $scope.trial_ach_flag = (response.data.possettings.trial_payment_method[3] === '1' ? true : false);
                        $scope.membership_cash_flag = (response.data.possettings.membership_payment_method[1] === '1' ? true : false);
                        $scope.membership_check_flag = (response.data.possettings.membership_payment_method[2] === '1' ? true : false);
                        $scope.membership_ach_flag = (response.data.possettings.membership_payment_method[3] === '1' ? true : false);
                        $scope.include_email_type = (response.data.possettings.leads_email_status === 'I' ? 'Include in auto-generated emails' : 'Exclude in auto-generated emails');
                        $scope.processing_fee_type = (response.data.possettings.misc_charge_fee_type === '1' ? 'Absorb fees on' : 'Pass fees on');
                        $scope.controlpanel_retail_status = response.data.possettings.controlpanel_retail_status;
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            $scope.getsettingdetails();
            
        } else {
            $location.path('/login');
        }
    }
    module.exports =  PosDashboardController;