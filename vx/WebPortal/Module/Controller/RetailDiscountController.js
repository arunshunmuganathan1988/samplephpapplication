RetailDiscountController.$inject=['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'DTDefaultOptions', '$window','$rootScope'];
function RetailDiscountController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, DTDefaultOptions, $window,$rootScope) {
if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'Retail';
            $scope.activesorttab = 'all';
            //sort retail discounts 
            $scope.sortretaildiscounts = function(type){
                $scope.selectedData = [];
                $scope.selected = {};
                $localStorage.activesorttab = $scope.activesorttab = type;
                $scope.getretaildiscount();
            };
            $scope.discounttableview = true;
            $scope.creatediscountview = false;
            $scope.discounttype = 'V';
            $scope.discountrequirement = 'N';
            $scope.setenddiscdate = false;
            $scope.discountcode = "";
            $scope.maintainselected = [];
            $scope.fromminusbutton = false;
            $scope.appliesto = "O";
            $scope.selectedproducts = [];
            $scope.selectedproducts_copy = [];
            $scope.productdetails = [];
            $scope.limit = 0;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.discountnotvalid = $scope.discvalexceeds = false;
            
            $scope.retaildiscounttoggleAll = function (selectAll, selectedItems) {
                if($scope.fromminusbutton){
                    $('#DataTables_Table_retaildiscount_1 tr').removeClass('selected');
                    $('#DataTables_Table_retaildiscount_2 tr').removeClass('selected');
                    $('#DataTables_Table_retaildiscount_3 tr').removeClass('selected');
                    $('#DataTables_Table_retaildiscount_4 tr').removeClass('selected');
                    $scope.fromminusbutton = false;
                }else{
                    $('#DataTables_Table_retaildiscount_1 tr').toggleClass('selected');
                    $('#DataTables_Table_retaildiscount_2 tr').toggleClass('selected');
                    $('#DataTables_Table_retaildiscount_3 tr').toggleClass('selected');
                    $('#DataTables_Table_retaildiscount_4 tr').toggleClass('selected');
                }
                $scope.selectedData = [];
                 $scope.all_select_flag='N';
//                 console.log(selectedItems);
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
                            $scope.all_select_flag='Y';
                            $scope.selectedData.push({"id":id});
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
//                console.log($scope.all_select_flag);
            };
            
            $scope.retaildiscounttoggleOne= function (selectedItems) {
                $scope.maintainselected = selectedItems;
//                console.log($scope.maintainselected);
                var check=0;
                  $scope.all_select_flag='N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        }else{
                            $scope.selectedData.push({"id":id});
                        }
                         
                    }
                }
//                console.log($scope.selectedData);
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
//                console.log($scope.all_select_flag);
            }
            
            //unselect all discount
            $scope.unselectalldiscount = function(){
                $scope.selectAll = false;
                $scope.fromminusbutton = true;
                $scope.retaildiscounttoggleAll(false,$scope.maintainselected);  
            };
            
            //server date format
            $scope.serverdateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                }else{
                    var datevalue = "";
                    return datevalue;
                }
            };
            
            $scope.timeServerformat = function (time) {
                if (time.length > 0) {
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    var formatedtime = sHours + ":" + sMinutes + ":01";
                    return formatedtime;
                } else {
                    var formatedtime = "";
                    return formatedtime;
                }
            };
            
            // timezone format
            $scope.slashdateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    var datevalue = month + '/' + day + '/' + year;
                    return datevalue;
                }else{
                    var datevalue = "";
                    return datevalue;
                }
            }
            
            $scope.$on('$viewContentLoaded', function () {
                $('#startdiscdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#startdisctime').timepicker({ 
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });
                $('#enddiscdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#enddisctime').timepicker({ 
                    timeFormat: 'h:i A',
                    step: 15,
                    ignoreReadonly: true
                });
            });            
            
            //catch start date
            $scope.catchstartdate = function(){
                if($scope.startdiscdate > ($scope.setenddiscdate && $scope.enddiscdate && $scope.enddiscdate)){
                    $scope.enddiscdate = $scope.startdiscdate;
                }
                $('#enddiscdate').datepicker('setStartDate', $scope.startdiscdate);
                
                if(($scope.startdiscdate == $scope.enddiscdate) && $scope.setenddiscdate){
                    var discount_start_date_time, discount_end_date_time;
                    
                    if ($scope.startdiscdate && $scope.startdisctime && $scope.enddiscdate && $scope.enddisctime) {
                        discount_start_date_time = $scope.serverdateformat($scope.startdiscdate) + " " + $scope.timeServerformat($scope.startdisctime);
                        discount_end_date_time = $scope.serverdateformat($scope.enddiscdate) + " " + $scope.timeServerformat($scope.enddisctime);
                        if (Date.parse(discount_start_date_time) > Date.parse(discount_end_date_time)) {                            
                            $scope.enddisctime = $scope.startdisctime;
                            $('#enddisctime').timepicker('setTime', new Date(discount_start_date_time));
                        }
                    }
                }
            };          
            
             //open retail discount support link in new tab
            $scope.openRetDisSupport = function(){
                window.open("https://intercom.help/mystudio/retail-and-merchandise-shop/4-create-retail-discount-codes/creating-discount-codes",'_blank'); 
            };
            
            //catch discount requirement
            $scope.catchdiscountrequirement = function(){
                $scope.discountminimumamount = "";
            };
            
            //discount datatable
            $scope.getretaildiscount = function(){    
                    DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input class="bigcheckbox" style="margin: 2px;" type="checkbox" ng-model="selectAll" ng-click="retaildiscounttoggleAll(selectAll, selected)">';
                $scope.selected = {};
                $scope.selectedData = [];
                $scope.selectAll = false;
//                console.log($scope.selected);
                   $scope.retaildiscountdtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'getRetailDiscounts',
                            dataSrc: function (json) {
                                $scope.retaildiscountlistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json', 
                            xhrFields: {
                                withCredentials: true
                            },
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "discount_tab": $scope.activesorttab,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })
                        
                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 550)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [0, 'desc'])
                        .withOption('search', false)
                        .withOption('bFilter', false)
                    $scope.retaildiscountdtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" class="dicountcellcheckbox" ng-model="selected[' + data.id + ']" ng-click="retaildiscounttoggleOne(selected)"/>';
                                }),
                                DTColumnBuilder.newColumn('Code').withTitle('Code').withClass('dicountcellcode').renderWith(
                                function (data, type, full,meta) {
                                    var master_txt,disc_usage_txt,min_pur_amnt_txt,min_pur_qty_txt;
                                    master_txt = disc_usage_txt = min_pur_amnt_txt = min_pur_qty_txt = '';
                                    if(full.retail_discount_usage_type === 'P'){
                                        disc_usage_txt = 'specific products';
                                    }else if(full.retail_discount_usage_type === 'O'){
                                        disc_usage_txt = 'entire order';
                                    }
                                    if(full.retail_discount_type === "P"){                                        
                                        master_txt = '<span class="buyername" style="line-height: 20px;font-size: 14px;" ng-click="editdiscountcode(\'' + meta.row + '\')">' + full.discount_code + '</span>'+'<br>'
                                        +'<span style="line-height: 20px;">'+full.retail_discount_amount+'% off '+disc_usage_txt+'</span>';
                                    }else{
                                        master_txt = '<span class="buyername" style="line-height: 20px;font-size: 14px;" ng-click="editdiscountcode(\'' + meta.row + '\')">' + full.discount_code + '</span>'+'<br>'
                                        +'<span style="line-height: 20px;">{{wp_currency_symbol}}'+full.retail_discount_amount+' off '+disc_usage_txt+'</span>';
                                    }
//                                    if(parseInt(full.min_purchase_amount) > 0){
//                                        min_pur_amnt_txt = ' Minimum purchase of '+$scope.wp_currency_symbol+full.min_purchase_amount+' ';
//                                        master_txt += '<span class="dot black"></span>'+min_pur_amnt_txt;
//                                    }
//                                    if(parseInt(full.min_purchase_quantity) > 0){
//                                        min_pur_qty_txt = ' Minimum purchase of '+full.min_purchase_quantity+' item(s)';
//                                        master_txt += '<span class="dot black"></span>'+min_pur_qty_txt;
//                                    }
                                    return master_txt;
                                }).notSortable(),
                                DTColumnBuilder.newColumn('status').withTitle('Status').withClass('dicountcell').renderWith(
                                function (data, type, full) {
                                    if(full.status === "Expired" || full.status === "Scheduled"){
                                    return '<div class="greybg"'+(full.status === "Expired"?"style='width: 70px'":"")+'  >' + full.status + '</div>';
                                    }else{
                                    return '<div class="greenbg"  >' + full.status + '</div>';
                                    }
                                }).notSortable(),
                                DTColumnBuilder.newColumn('discount_used').withTitle('Used').withClass('dicountcell').renderWith(
                                function (data, type, full) {
                                    if(full.discount_limit_flag === 'N') {
                                    if (full.status === "Expired") {
                                        return '<span class="greytext" >' + full.discount_used + '</span>';
                                    } else {
                                        return '<span style="font-size: 14px;" >' + full.discount_used + '</span>';
                                    }
                                    }else{
                                        if (full.status === "Expired") {
                                        return '<span class="greytext" >' + full.discount_used +'/' + full.discount_limit_value + '</span>';
                                    } else {
                                        return '<span style="font-size: 14px;" >' + full.discount_used +'/' + full.discount_limit_value + '</span>';
                                    }
                                    }
                                }).notSortable(),
                                DTColumnBuilder.newColumn('Start').withTitle('Start').withClass('dicountcell').renderWith(
                                function (data, type, full) {
                                    if(full.status === "Expired"){
                                        return '<span class="greytext"  >' + full.start_date + '</span>';
                                    } else {
                                        return '<span style="font-weight: 400;font-size: 14px;"  >' + full.start_date + '</span>';
                                    }
                                }).notSortable(),
                                DTColumnBuilder.newColumn('end').withTitle('End').withClass('dicountcell').renderWith(
                                function (data, type, full) {
                                    if(full.status === "Expired"){
                                        return '<span class="greytext"  >' + full.end_date + '</span>';
                                    }else{
                                        return '<span style="font-weight: 400;font-size: 14px;"  >' + full.end_date + '</span>';
                                    }
                                }).notSortable()
                    ];
                   $scope.discountdtInstance1 = {};
            };
            
            $('body').on('click', '#DataTables_Table_retaildiscount_1 tr td input', function () {
                var tr = $(this).closest('tr');
                tr.toggleClass('selected');
            })
             $('body').on('click', '#DataTables_Table_retaildiscount_2 tr td input', function () {
                var tr = $(this).closest('tr');
                tr.toggleClass('selected');
            })
             $('body').on('click', '#DataTables_Table_retaildiscount_3 tr td input', function () {
                var tr = $(this).closest('tr');
                tr.toggleClass('selected');
            })
             $('body').on('click', '#DataTables_Table_retaildiscount_4 tr td input', function () {
                var tr = $(this).closest('tr');
                tr.toggleClass('selected');
            })
            $scope.getretaildiscount();
            
            
            //go back to manage page
            $scope.gobacktomanagepage = function () {
                $scope.updatediscounttype = "";//clear
                if ($scope.fromcreatediscount) {//back navigation
                    $scope.discounttableview = true;
                    $scope.creatediscountview = false;
                    $scope.fromcreatediscount = false;
                    $route.reload();
                } else {
                    $location.path('/manageretail');
                }
            };
            
            //go create discount view
            $scope.gocreatediscountview = function(){
                $scope.selectedproductsview = [];
                $scope.selectedproducts = [];
                $scope.productdetails = [];
                $scope.clearscope();
                $scope.updatediscounttype = "add";//add
                $scope.fromcreatediscount = true;//for back navigation
                $scope.discounttableview = false;
                $scope.creatediscountview = true;
            }
            
            //clear discount fields
            $scope.clearscope = function(){
                $scope.updatedisocuntid = "";
                $scope.discountcode = "";
                $scope.discountvalue = "";
                $scope.discounttype = "V";
                $scope.discountlimitcheckbox = false;
                $scope.discountlimit = "";
                $scope.discountrequirement = "N";
                $scope.discountminimumamount = "";
                $scope.startdiscdate = "";
                $scope.startdisctime = "";
                $scope.enddiscdate = "";
                $scope.enddisctime = "";
                $scope.setenddiscdate = false;
            };
            
            //clear discount fields
            $scope.discardChangesConfirm = function(){  
                $("#discountdiscardModal").modal('show');
            };
            
            $scope.discardChanges = function(){                
                if($scope.updatediscounttype === "update"){
                    $scope.editdiscountcode($scope.disc_edit);
                }else{
                   $scope.clearscope(); 
                }
                $scope.catchdiscountamount();
                $scope.adddiscountretail.$setPristine();
                $scope.adddiscountretail.$setUntouched();
            };
            
            //creat/update ediscount code
            $scope.updatediscountcode = function(){
                if($scope.appliesto === "P" && $scope.selectedproducts.length == 0){
                    $("#dicountfail-modal").modal('show');
                    $("#failmessage").text("Please select a product to proceed!");
                    return false;
                }
                //merging date and time
                var s = new Date($scope.startdiscdate + " " + $scope.startdisctime);
                var startdiscdate = $scope.serverdateformat($scope.startdiscdate) + ' ' + s.getHours() + ':' + s.getMinutes(); 
                var e = new Date($scope.enddiscdate + " " + $scope.enddisctime);
                var enddiscdate = $scope.serverdateformat($scope.enddiscdate) + ' ' + e.getHours() + ':' + e.getMinutes(); 
                //end date check
                if(!$scope.setenddiscdate){
                    enddiscdate = "";
                }
              $http({
                    method: 'POST',
                    url: urlservice.url + 'addOrUpdateRetailProductDiscount',
                    data: {
                        "company_id": $localStorage.company_id,
                        "discount_code":$scope.discountcode,
                        "start_date":startdiscdate,
                        "end_date":enddiscdate,
                        "discount_type":$scope.discounttype,
                        "discount_amount":$scope.discountvalue,
                        "min_purchase_flag":$scope.discountrequirement,
                        "min_purchase_amount":$scope.discountminimumamount,
                        "discount_limit_flag":$scope.discountlimitcheckbox?"Y":"N",
                        "discount_limit":$scope.discountlimit,   
                        "type":$scope.updatediscounttype,
                        "discount_id":$scope.updatedisocuntid,
                        "applies_entire_or_specific":$scope.appliesto,
                        "selected_products":$scope.selectedproducts
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.updatedisocuntid = "";   
                        $('#progress-full').hide();
                        $("#dicountsuccess-modal").modal('show');
                        $("#successmessage").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();                        
                        $("#dicountfail-modal").modal('show');
                        $("#failmessage").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };             
            
            //update discount code
            $scope.editdiscountcode = function(row){
                $scope.selectedproducts = [];
                $scope.disc_edit = row;
                $scope.updatediscounttype = "update";//update
                $scope.discountcode = $scope.retaildiscountlistcontents[row].discount_code;
                $scope.discountvalue = $scope.retaildiscountlistcontents[row].retail_discount_amount;
                $scope.discounttype = $scope.retaildiscountlistcontents[row].retail_discount_type;
                if($scope.retaildiscountlistcontents[row].discount_limit_flag === "Y") {
                    $scope.discountlimitcheckbox = true;
                } else {
                    $scope.discountlimitcheckbox = false;
                }
                $scope.discountlimit = $scope.retaildiscountlistcontents[row].discount_limit_value;
                $scope.discountrequirement = $scope.retaildiscountlistcontents[row].min_purchase_flag;
                if($scope.discountrequirement === "V"){
                    $scope.discountminimumamount = $scope.retaildiscountlistcontents[row].min_purchase_amount;
                }else if($scope.discountrequirement === "Q"){
                    $scope.discountminimumamount = $scope.retaildiscountlistcontents[row].min_purchase_quantity;
                }
                var startdatewithtime = $scope.retaildiscountlistcontents[row].discount_begin_dt.split(" ");
                var enddatewithtime = $scope.retaildiscountlistcontents[row].discount_end_dt.split(" ");;
                $scope.startdiscdate = $scope.slashdateformat(startdatewithtime[0]);
                $scope.startdisctime = $scope.retaildiscountlistcontents[row].start_time;
                $scope.enddiscdate = $scope.slashdateformat(enddatewithtime[0]);
                $scope.enddisctime = $scope.retaildiscountlistcontents[row].end_time;
                if($scope.retaildiscountlistcontents[row].discount_end_dt === "0000-00-00 00:00:00"){
                    $scope.setenddiscdate = false;
                }else{
                    $scope.setenddiscdate = true;
                }
                
                $scope.fromcreatediscount = true;//for back navigation
                $scope.discounttableview = false;
                $scope.creatediscountview = true;
                $scope.updatedisocuntid = $scope.retaildiscountlistcontents[row].id;
                $scope.appliesto = $scope.retaildiscountlistcontents[row].retail_discount_usage_type; 
                if($scope.appliesto === "P"){
                    $scope.selectedproductsview = $scope.selectedproducts = $scope.retaildiscountlistcontents[row].discount_products;               
                    $scope.selectedproducts_copy = angular.copy($scope.retaildiscountlistcontents[row].discount_products);
                }else{
                    $scope.selectedproductsview = $scope.selectedproducts = $scope.selectedproducts_copy = [];
                }
            };
            
            $scope.confirmdiscountdelete = function(){
                $("#deletediscountmessageModal").modal('show');
            };
            
            //delete discount code
            $scope.deletediscountcode = function(){
              $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteRetailDiscounts',
                    data: {
                        "company_id": $localStorage.company_id,
                        "discount_tab":$scope.activesorttab,
                        "selected_id_list":$scope.selectedData,
                        "select_all_flag":$scope.all_select_flag,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#dicountsuccess-modal").modal('show');
                        $("#successmessage").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();                        
                        $("#dicountfail-modal").modal('show');
                        $("#failmessage").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });  
            };
            
            //get products and show modal
            $scope.browseproducts = function(){
                $('#progress-full').show();
                $scope.limit = 0;
                $("#browsediscount-modal").modal('show');
                $scope.getproducts();
            };
            
            //browse products modal
            $scope.getproducts = function(){
                var discount_amount = "0";
                if($scope.discounttype === "V" && $scope.discountvalue){
                    discount_amount = $scope.discountvalue;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getproducts',
                    data: {
                        "company_id": $localStorage.company_id,
                        "search_term":$scope.searchproducts,
                        "limit":$scope.limit,
                        "discount_amount":discount_amount
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if(response.data.status === 'Success'){
                        if(response.data.msgs !== "No details found") {
                            if(response.data.msg.length === "0"){
                                $scope.productdetails = [];
                                $scope.limit = 0;
                            }else {
                                for (var i = 0; i < response.data.msg.length; i++) {
                                    $scope.productdetails.push(response.data.msg[i]);
                                }
                            }
                        }else{
                            $scope.limit = 0;
                        }
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $('#progress-full').hide();                        
                        $("#dicountfail-modal").modal('show');
                        $("#failmessage").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //catch search products
            $scope.catchsearchproducts = function(){
                $scope.productdetails = [];
                $scope.getproducts();
            };
            
            //close browse products
            $scope.closebrowseproducts = function(){
                $("#browsediscount-modal").modal('hide');
                $scope.productdetails = [];
                $scope.selectedproducts = angular.copy($scope.selectedproducts_copy);
                $scope.selectedproductsview = $scope.selectedproducts;
            };
            
            //push product ids to array checkbox on change
            $('body').on('click', '#selectproducts input', function () {
                var checked = JSON.parse($(this).val());
                if ($(this).is(':checked')) {
                    $scope.selectedproducts.push(checked);
                } else {
                    $scope.selectedproducts.splice($.inArray(checked, $scope.selectedproducts), 1);
                }
                // console.log($scope.selectedproducts);
            });
            
            //view selected products
            $scope.viewselectedproducts =  function(){
                $scope.productdetails = [];
                $scope.selectedproductsview = $scope.selectedproducts;
                $scope.selectedproducts_copy = $scope.selectedproducts;
            };
            
            //remove selected ID
            $scope.removeselectedID = function(removeid){
//                $scope.selectedproducts.splice($.inArray(removeid,$scope.selectedproducts), 1);
                for(var k = 0 ; k < $scope.selectedproducts.length ;k++){
                    if($scope.selectedproducts[k] == removeid){
                        $scope.selectedproducts.splice(k,1);
                    }
                }
                $scope.catchdiscountamount();
                $scope.viewselectedproducts();
            };
            
            //find scroll end touched
            jQuery(
                    function ($)
                    {
                        $('#productscrollbox').bind('scroll', function ()
                        {
                            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                            {
                                $scope.limit += 50;
                                $scope.getproducts();
                            }
                        })
                    }
            );
    
            //catch discount amount
            $scope.catchdiscountamount = function(){
                if($scope.discounttype === "P"){   
                    if(parseFloat($scope.discountvalue) > 100){
                        $scope.discvalexceeds = true;
                        return false;
                    }else{
                        $scope.discvalexceeds = false;
                        $scope.discountnotvalid = false;
                        return false;
                    }
                }
                if($scope.selectedproducts && $scope.selectedproducts.length > 0 && $scope.discounttype === "V" && $scope.discountvalue > 0){
                    for(var j=0 ; j < $scope.selectedproducts.length ;j++){
                        if(parseFloat($scope.selectedproducts[j].retail_product_price) < parseFloat($scope.discountvalue)){
                            $scope.discountnotvalid = true;
                            return false;
                        }else{
                            $scope.discountnotvalid = false;
                        }
                    }
                }else{
                    $scope.discountnotvalid = false;
                }
            }
    
            $scope.checkalreadychecked = function (id) {
                if ($scope.selectedproducts.length != 0) {
                    for (var i = 0; i < $scope.selectedproducts.length; i++) {
                        if (id == $scope.selectedproducts[i].retail_product_id) {
                            var domid = 'checkbox['+id+']';
                            setTimeout(function () {
//                                console.log(domid);
                                document.getElementById(domid).checked = true;
                            }, 200);                            
//                            console.log("found id");
                        }else{
//                            console.log("not found id");
                        }
                    }
                }
            };
            
        } else {
            $location.path('/login');
        }

    };
    
    module.exports= RetailDiscountController;