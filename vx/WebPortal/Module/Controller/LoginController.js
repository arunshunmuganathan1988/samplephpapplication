LoginController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope'];

function LoginController ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope) {

        // LOGIN VARIABLES
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
        $scope.message = "MyStudio";
        $scope.urlFormat = '^((https)://)?([a-z]+[.])?[a-z0-9-]+([.][a-z]{1,4}){1,2}(/.*[?].*)?$';
        $rootScope.hidesidebarlogin = true;
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = false;        
        $scope.showpassword = false; // TOGGLE PASSWORD/TEXT 
        $scope.activepagename = 'loginscreen';
        $scope.activepageid = '1';
        $scope.login_email = $scope.login_password = $scope.reset_email = $scope.failure_response_text = '';
        $scope.notReadyToLogin = true;
        
        // LOGIN FUNCTIONS
        $("#verifycodemodal").modal('hide');
        $(".modal-backdrop").removeClass('modal-backdrop fade in');

        // SESSION VERIFICATION TO PREVENT MULTIPLE USERS SIMULTANEOUSLY
        if ($localStorage.islogin == 'Y') {
            $('#progress-full').show();
            $http({
                method: 'GET',
                url: urlservice.url + 'verifySession',
                params: {
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        if ($location.absUrl().indexOf("dashboard") < 0) {
                            angular.element(document.getElementById('sidetopbar')).scope().chatBot();
                            $scope.dashboard();
                        }
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                }
                
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        }

        $scope.togglePage = function (page, pageid) {
            $scope.activepagename = page;
            $scope.activepageid = pageid;
        };

        $scope.redirectTo = function (to) {
            var URL = '';
            if (to == 'landingpage') {
                URL = 'https://www.mystudio.app';
            }

            if (URL != '') {
                window.open(URL, '_blank');
            }
        };

        $scope.operatorLogin = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'login',
                data: {
                    "email": $scope.login_email,
                    "password": $scope.login_password
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $localStorage.company_id = response.data.msg.company_id;
                    $localStorage.user_id = response.data.msg.user_id;
                    $localStorage.usrname = $scope.login_email;
                    $localStorage.pswdname = $scope.login_password;
                    $localStorage.user_email_id = $scope.login_email;
                    if (response.data.msg.company_code == null) {
                        // app code stage .may be closed before the studio code screen.
                        $('#progress-full').hide();
                        $scope.openRegistration();
                        return;
                    }                    
                    $localStorage.studio_code = response.data.msg.company_code;
                    $localStorage.phone_number = response.data.msg.phone_number;
                    $localStorage.secondStage = false;
                    $localStorage.thirdStage = false;
                    $localStorage.islogin = 'Y';
                    $localStorage.isadminlogin = 'N';

                    $localStorage.student_id = response.data.msg.student_id;
                    $localStorage.user_id = response.data.msg.user_id;
                    $localStorage.event_flag = response.data.msg.event_flag;
                    $localStorage.event_b_id = response.data.msg.eventbrite_id;
                    $localStorage.event_b_url = response.data.msg.eventbrite_url;
                    if ($localStorage.event_flag == "E" || $localStorage.event_flag == null) {
                        $localStorage.sideEvent = false;
                    } else {
                        $localStorage.sideEvent = true;
                    }
                    $localStorage.loginDetails = response.data.msg;
                    $localStorage.subscription_status = response.data.msg.subscription_status;
                    $localStorage.upgrade_status = response.data.msg.upgrade_status;
                    $localStorage.no_of_login = response.data.msg.expiry_msg_flag;
                    $localStorage.user_id = response.data.msg.user_id;
                    $localStorage.wepay_level = response.data.msg.wp_level;
                    $localStorage.wepay_client_id = response.data.msg.wp_client_id;
                    $localStorage.regstr_complete = response.data.msg.registration_complete;
                    $localStorage.verfication_complete = response.data.msg.verification_complete;
                    $localStorage.processing_fee_percentage = response.data.msg.processing_fee_percentage;
                    $localStorage.processing_fee_transaction = response.data.msg.processing_fee_transaction;
                    $localStorage.wp_currency_code = response.data.msg.wp_currency_code;
                    $localStorage.wp_currency_symbol = response.data.msg.wp_currency_symbol;
                    $localStorage.plan_processing_fee = response.data.msg.plan_processing_fee.all_processing_fee;
                    $localStorage.studio_expiry_level = response.data.msg.studio_expiry_level;
                    
                    // STRIPE CRENDENTIALS
                    $localStorage.stripe_upgrade_status = response.data.msg.stripe_upgrade_status;
                    $localStorage.stripe_subscription = response.data.msg.stripe_subscription;
                    $localStorage.stripe_publish_key = response.data.msg.stripe_publish_key;
                    $localStorage.stripe_country_support = response.data.msg.stripe_country_support;
                                        
//                    Stripe alert text show when charges not enabled
                    $localStorage.stripe_status = $localStorage.loginDetails.stripe_status;
                    $localStorage.stripe_charges_enabled = $localStorage.loginDetails.stripe_charges_enabled;
                    
                    $("#homelink").addClass('active');
                    $rootScope.getinstantmessage();
                    $rootScope.convCountTriggers = false;
                    angular.element(document.getElementById('sidetopbar')).scope().chatBot();
                    if (($localStorage.studio_expiry_level === 'L1' || $localStorage.studio_expiry_level === 'L2') || ($localStorage.subscription_status === 'N' &&   $localStorage.upgrade_status === 'F')) {
                        $rootScope.sideBarHide = false;
                        $rootScope.hidesidebarlogin = false;
                        $rootScope.adminsideBarHide = false;
                        $scope.studioexpired();
                    } else {
                        $scope.dashboard();
                    }
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                }else {
                    $('#progress-full').hide();
                    $('#loginfailure-modal').modal('show');
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
                $scope.failure_response_text = 'Invalid server response';
                $('#response-modal').modal('show');
            });
            $scope.failure_response_text = '';
        };

        // TOGGLE PASSWORD/TEXT 
        $scope.togglePasswordText = function () {
            $scope.showpassword = !$scope.showpassword;
        };

        $scope.resetPassword = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'forgotPassword',
                data: {
                    "email": $scope.reset_email
                },
                headers: {"Content-Type": 'application/json; charset=utf-8'},
                withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $localStorage.company_id = response.data.msg.company_id;
                    $('#resetpassword-modal').modal('show');
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                    $scope.failure_response_text = response.data.msg;
                    $('#response-modal').modal('show');
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
                $scope.failure_response_text = 'Invalid server response';
                $('#response-modal').modal('show');
            });
            $scope.failure_response_text = '';
        };

        
        $scope.catchInputChange = function (id, inputvalue) {
            if (inputvalue) {
                if (inputvalue.length > 0) {
                    $('#' + id).parents('.form-group-new').addClass('focused');
                }
            }
        };

        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('.form-input').focus(function () {
                $(this).parents('.form-group-new').addClass('focused');
            });

            $('.form-input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group-new').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });

            // LOGO CLICK REDIRECTION
            $('#mystudiologo').click(function () {
                $scope.redirectTo('landingpage');
            });
        });

    }
    module.exports = LoginController;