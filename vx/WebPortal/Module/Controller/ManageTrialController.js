ManageTrialController.$inject=['$scope',  '$compile','$location', '$http', '$localStorage', 'urlservice', 'trialListDetails', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$rootScope'];
 function ManageTrialController($scope, $compile, $location, $http, $localStorage, urlservice, trialListDetails, $filter, $route,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,$window,$rootScope) {
        if ($localStorage.islogin === 'Y') {
            
        angular.element(document.getElementById('sidetopbar')).scope().getName();
        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
        angular.element(document.getElementById('sidetopbar')).scope().regComplete(); 
        angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//        angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
        angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
        angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
        angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
        tooltip.hide(); 
        $rootScope.activepagehighlight = 'trials';
            $scope.trialdashboard = true;
            $scope.triallive = false;
            $scope.trialdraft = false;           
            $scope.trialoptions = false;
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            $scope.selectedData = [];
            $scope.selected = {};
            $scope.selectAll = false;
            $scope.onetimecheckout = 0;
            $scope.waiverchecked = $scope.trial_chk_ach = $scope.stripeccchecked = false;
            $scope.discountvalidation = false;
            $localStorage.currentpage = "trial";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.nolivetriallisted = $scope.nodrafttriallisted = true;             
            $scope.NumofLive = $scope.NumofDraft = 0;
            $scope.participant_studentname = '';
            $scope.trialwaiverarea = "";
            $scope.trialurlhide = false;
            $localStorage.triallocaldata = "";
            $localStorage.addtriallocaldata = "";
            $scope.mainttrialview = true;//initialview
            $scope.ccyearlist = ["2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036","2037"];
            $scope.ccmonthlist = ["1","2","3","4","5","6","7","8","9","10","11","12"];
            $scope.upgrade_status = $localStorage.upgrade_status; // for inactive studio-registration block
            $scope.stripe_subscription = $localStorage.stripe_subscription;
            $scope.trial_payment_method = 'CC';
            $scope.stripe_card = '';
            $scope.stripe = "";
            
            if($localStorage.trialcategorydeleteredirect === 'Y'){
                $("#trialmessageModal").modal('show');
                $("#trialmessagetitle").text('Success Message');
                $("#trialmessagecontent").text('Trial program successfully deleted');   
                $localStorage.trialcategorydeleteredirect = 'N'; 
            }
    
            if($localStorage.wepay_level === 'P'){
               $scope.wepay_env = "production";
               $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            }else if($localStorage.wepay_level === 'S'){
               $scope.wepay_env = "stage";
               $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            
            WePay.set_endpoint($scope.wepay_env); // stage or production      

            // Shortcuts
            var d = document;
                d.id = d.getElementById,
                valueById = function(id) {
                    return d.id(id).value;
                };

            // For those not using DOM libraries
            var addEvent = function(e,v,f) {
                if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
                else { e.addEventListener(v, f, false); }
            };

            // Attach the event to the DOM
            addEvent(d.id('cc-submittrial'), 'click', function() {
                $('#progress-full').show();
                var userName = [valueById('firstName')]+' '+[valueById('lastName')];
                var user_first_name = valueById('firstName');
                var user_last_name = valueById('lastName');
                var email = valueById('email').trim();
                var phone = valueById('phone');
                var postal_code = valueById('postal_code');
                var country = valueById('country');
                    response = WePay.credit_card.create({
                    "client_id":        $localStorage.wepay_client_id,
                    "user_name":        userName,
                    "email":            valueById('email').trim(),
                    "cc_number":        valueById('cc-number'),
                    "cvv":              valueById('cc-cvv'),
                    "expiration_month": valueById('cc-month'),
                    "expiration_year":  valueById('cc-year'),
                    "virtual_terminal": "web",
                    "address": {
                        "country": valueById('country'),
                        "postal_code": valueById('postal_code')
                    }
                }, function(data) {
                    if (data.error) {
                        $('#progress-full').hide();
                        console.log(data);
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Wepay Message');
                        $("#trialmessagecontent").text(data.error_description);
                        // handle error response
                    } else {
                        if ($scope.onetimecheckout == 0) {
                            $scope.onetimecheckout = 1;
                            $scope.trialCheckout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                            console.log(data);
                        }
                        // call your own app's API to save the token inside the data;
                        // show a success page
                    }
                });
            });
            
        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });            
        };
        
        $scope.submitStripeForm = function(){
            $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error. 
                    var errorElement = document.getElementById('stripe-card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send paymentMethod.id to server 
                    (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                    $scope.preloader = true;
                    $scope.stripeTrialCheckout();
                }
            });
        };
        
        $scope.stripeTrialCheckout = function(){
            $('#progress-full').show();
            var trialstartdate = $scope.trialdateformat($scope.trialstartdate);
            var birthdate = document.getElementById("trialcolumn2").value;
            var final_total_trial_amount = (Math.round((+$scope.payment_amount + 0.00001) * 100) / 100).toFixed(2);
            var registration_amount = (Math.round((+$scope.registration_amount + 0.00001) * 100) / 100).toFixed(2);
            $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeTrialprogramCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        trial_id:  $scope.trialid,
                        trial_name: $scope.trialtitle,
                        desc: $scope.trialtitle,
                        studentid: $scope.student_id,
                        buyer_first_name: $scope.firstname,
                        buyer_last_name: $scope.lastname,
                        email : $scope.email,
                        phone : $scope.phone,
                        postal_code : $scope.postal_code,
                        country : $scope.country,
                        payment_method_id : ($scope.trial_payment_method == 'CC') ? $scope.payment_method_id : '',
                        registration_amount : registration_amount,
                        discount : $scope.discount,
                        payment_amount: final_total_trial_amount,
                        processing_fee_type: $scope.current_processing_fee_type,
                        reg_col1: $scope.trial_reg_col_value[0],
                        reg_col2: $scope.trial_reg_col_value[1],
                        reg_col3: birthdate,
                        lead_source : $scope.leadsource,
                        reg_col5: $scope.trial_reg_col_value[4],
                        reg_col6: $scope.trial_reg_col_value[5],
                        reg_col7: $scope.trial_reg_col_value[6],
                        reg_col8: $scope.trial_reg_col_value[7],
                        reg_col9: $scope.trial_reg_col_value[8],
                        reg_col10: $scope.trial_reg_col_value[9],
                        trial_participant_Streetaddress : $scope.trial_participant_Streetaddress,
                        trial_participant_City : $scope.trial_participant_City,
                        trial_participant_State : $scope.trial_participant_State,
                        trial_participant_Zip : $scope.trial_participant_Zip,
                        trial_participant_Country : $scope.trial_participant_Country,
                        upgrade_status: $localStorage.upgrade_status,
                        program_length:$scope.triallength,
                        program_length_type:$scope.triallengthtype,
                        payment_startdate : trialstartdate,
                        payment_method : $scope.trial_payment_method,
                        cc_type: ($scope.selectedcardtype === 'new') ? 'N' : 'E',
                        stripe_customer_id: ($scope.selectedcardtype === 'old') ? $scope.stripe_customer_id : ''
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        // Show success message 
                        $scope.cancelAddParticipant();
                        $("#trialpaymentmessageModal").modal('show');
                        $("#trialpaymentmessagetitle").text('Message');
                        $("#trialpaymentmessagecontent").text(response.data.msg);
                        $scope.redirect_conversionpage = "";
                        $scope.selectedcard = "";
                        if ($scope.conversionpage) {
                            if ($scope.conversionpage.indexOf("http://") === 0 || $scope.conversionpage.indexOf("https://") === 0) {
                                $scope.redirect_conversionpage = $scope.conversionpage;
                            } else {
                                $scope.redirect_conversionpage = 'http://' + $scope.conversionpage;
                            }
                        }
                        $scope.onetimecheckout = 0;
                        // CLEAR STRIPE FORM
                        if ($scope.stripe_card) {
                            $scope.stripe_card.clear();
                        }
                        $scope.manageLiveTrial();
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg); 
                        $scope.logout();
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        if(response.data.error){
                            $("#trialmessagetitle").text(response.data.error);
                            $("#trialmessagecontent").text(response.data.error_description);
                        }else{
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
        };
        
            // Trial Dashboard Initialisation
            
            $scope.netsales_flag=0;
            $scope.conversion_flag=0;
            $scope.trial_disable='E';
            $scope.trialperiod ='M';
          
        //trial dashboard left right icon function  
        $scope.trial_dashboard_flag=function($flag){
                if($flag==='N'){
                    $scope.netsales_flag=$scope.netsales_flag-1;
                     $scope.trialdashboardTable($scope.netsales_flag);
                }else if($flag==='P'){
                    $scope.netsales_flag=$scope.netsales_flag+1;
                    $scope.trialdashboardTable($scope.netsales_flag);
                }
                else if($flag==='F'){
                     $scope.conversion_flag=$scope.conversion_flag-1;
                     $scope.trialdashboardTable($scope.conversion_flag);
                }else if($flag==='O'){
                    $scope.conversion_flag=$scope.conversion_flag+1;
                    $scope.trialdashboardTable($scope.conversion_flag);
                }
        };
        //trial monthly/yearly dashboard view
        $scope.trial_sales_period_selection = function(trialperiod){
            if(trialperiod === 'M'){
                $scope.netsales_flag=0;
                $scope.trialperiod = 'M';
                $scope.trialdashboardTable();
                
            }else if(trialperiod === 'A'){
                $scope.netsales_flag=0;
                $scope.trialperiod = 'A';
                $scope.trialdashboardTable();
            }            
        };
            //trial dashboard server call
            $scope.trialdashboardTable = function () {
                //Loads data and ToTal Amount In All the Tabs of Dashboard Page//
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getTrialdashboard',
                    data: {
                        "company_id": $localStorage.company_id,
                        "l_flag":$scope.netsales_flag,
                        "c_flag":$scope.conversion_flag,
                        "sales_period": $scope.trialperiod

                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') { 
                        $scope.trialdashboarddetails = response.data.msg;
                        $scope.trialleadsourcevalue = $scope.trialdashboarddetails.month_leadsource;
                        $scope.trialconversionvalue = $scope.trialdashboarddetails.month_conversion;
                        $scope.trialtotal = $scope.trialdashboarddetails.leads;
                        $scope.trialconversiondetails = $scope.trialdashboarddetails.trials;
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            
            //clear tab classes
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };   
            //info madal
            $scope.managetrialinfo = function(info){
                var msg;
                if(info === 'publicurl'){
                   msg = "<p>This URL is a web link to your Trial list, allowing users without the app to browse and register for anyone of your Trials.<p><p>Give this link to your web developer to link it your website. Use this link to promote your events on your Facebook page and Google ads.</p>"; 
                }               
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };
            //dashboard tab view
            $scope.trialdashboardview = function () {
                $localStorage.trialredirectto = 'DA';
                $scope.trialdashboard = true;
                $scope.triallive = false;
                $scope.trialdraft = false;
                $scope.trialoptions = false;
                $scope.resettabbarclass();
                $('#li-dashboard').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-1').addClass('greentab-text-active');                
                $scope.trialdashboardTable();
            };
            //live tab view
            $scope.triallivelistview = function () {
                $localStorage.trialredirectto = 'L';
                $localStorage.currentpage = "trial";
                $scope.trialdashboard = false;
                $scope.trialoptions = true;
                $scope.triallive = true;
                $scope.trialdraft = false;
                $scope.resettabbarclass();
                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                $('#li-trialoptions').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-3').addClass('greentab-text-active');
                $scope.gettrialdetails('P');
            };

            //draft tab view
            $scope.trialdraftlistview = function () {
                $localStorage.trialredirectto = 'L';
                $scope.trialdashboard = false;
                $scope.trialoptions = true;
                $scope.triallive = false;
                $scope.trialdraft = true;
                setTimeout(function () {
                    $scope.resettabbarclass();
                    $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
                    $('#li-trialoptions').addClass('active-event-list').removeClass('event-list-title');
                    $('.trialtab-text-3').addClass('greentab-text-active');
                }, 100);
                $localStorage.redirectedfrmtemplate = false;
                $scope.gettrialdetails('S');
            };
            //trial option view
            $scope.trialoptionsview = function () {
                $localStorage.trialredirectto = 'L';
                $scope.trialdashboard = false;
                $scope.trialoptions = true;
                $scope.triallive = true;
                $scope.trialdraft = false;
                $scope.resettabbarclass();
                $('#li-trialoptions').addClass('active-event-list').removeClass('event-list-title');
                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
                $('.trialtab-text-3').addClass('greentab-text-active');
                $scope.gettrialdetails('P');
            };
            
            $scope.trialsview = function () {
                $location.path('/trials');
            };
            //student list (crdit card details etc.)
            $scope.getStudentListForTrial = function(){
//                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstudent',
                    params: {
                        "company_id": $localStorage.company_id,
                         "for": "trial"
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function(response){
                    if (response.data.status === 'Success') {
                        $scope.studentlist  = response.data.msg.student_details;
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg); 
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.studentlist  = [];
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getStudentListForTrial();
            //live draft list details
            $scope.gettrialdetails = function (list_type) {
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'gettrialdetails',
                    params: {
                        "company_id": $localStorage.company_id,
                        "list_type" : list_type
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
//                        $scope.triallivelist = response.data.msg.live;
//                        $scope.trialdraftlist = response.data.msg.draft;
//                        $scope.trialunpublishedlist = response.data.msg.unpublished;
//                        $scope.trialurl = response.data.trial_list_url;

                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.trialurl = response.data.trial_list_url;
                        if(parseInt($scope.NumofLive) > 0){
                            $scope.nolivetriallisted = false; 
                        }else{
                            $scope.nolivetriallisted = true; 
                        }
                        if(parseInt($scope.NumofDraft) > 0){
                            $scope.nodrafttriallisted = false;
                        }else{
                            $scope.nodrafttriallisted = true;
                        } 
                        
                        if(list_type === 'P'){
                            $scope.triallivelist = response.data.msg.live;
                            $scope.trialdraftlist = [];
                        }else if(list_type === 'S'){
                            $scope.trialdraftlist = response.data.msg.draft;
                            $scope.triallivelist = [];
                        }
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Failed') {
                        $scope.triallivelist = [];
                        $scope.trialdraftlist = [];
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
//                        $scope.trialunpublishedlist = [];
                        $scope.trialurlhide = true;
                        $('#progress-full').hide();
                        $("#trialfailuretrialmessageModal").modal('show');
                        $("#trialfailuretrialmessagetitle").text('Message');
                        $("#trialfailuretrialmessagecontent").text(response.data.msg);
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuretrialmessageModal").modal('show');
                        $("#trialfailuretrialmessagetitle").text('Message');
                        $("#trialfailuretrialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
//            $scope.gettrialdetails('P');
            $scope.selected = {};
            $scope.selectAll = false;
            $scope.all_select_flag = 'N';
            $scope.color_green = 'N';
            //redirected from trials page
            if($localStorage.trialredirectto === "DA"){
                $scope.trialdashboardview();
            }else if($localStorage.trialredirectto === "L"){
                $scope.triallivelistview();
            }
            //active tab
            $scope.manageLiveTrial = function () {
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                $scope.mainttrialview = false;
                $scope.trialdatatableview = true;
                $scope.active = true;
                $scope.enrolled = false;
                $scope.cancel = false;
                $scope.didntstart = false;
                $scope.resettabbarclass();
                $('#li-active').addClass('active-event-list').removeClass('event-list-title');
                $('.managetrial-subtabtext-1').addClass('greentab-text-active');
                $scope.getTrialMembers($scope.trialid,'A');
                if(!$localStorage.detail_closetrialpagefrom){
                     $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.detail_closetrialpagefrom='';
            };
            //enrolled tab
            $scope.manageenrolledTrial = function(){
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                $scope.mainttrialview = false;
                $scope.trialdatatableview = true;
                $scope.active = false;
                $scope.enrolled = true;
                $scope.cancel = false;
                $scope.didntstart = false;
                $scope.getTrialMembers($scope.trialid,'E');
                setTimeout(function(){
                    $scope.resettabbarclass();
                    $('#li-enrolled').addClass('active-event-list').removeClass('event-list-title');
                    $('.managetrial-subtabtext-2').addClass('greentab-text-active');
                },100);
                if(!$localStorage.detail_closetrialpagefrom){
                     $scope.dtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.detail_closetrialpagefrom='';
                 
            };
            //cancelled tab
            $scope.managecancelledTrial = function(){
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                $scope.mainttrialview = false;
                $scope.trialdatatableview = true;
                $scope.active = false;
                $scope.enrolled = false;
                $scope.cancel = true;
                $scope.didntstart = false;
                $scope.getTrialMembers($scope.trialid,'C');
                setTimeout(function(){
                    $scope.resettabbarclass();
                    $('#li-cancel').addClass('active-event-list').removeClass('event-list-title');
                    $('.managetrial-subtabtext-3').addClass('greentab-text-active');
                },500);
                 if(!$localStorage.detail_closetrialpagefrom){
                     $scope.dtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.detail_closetrialpagefrom='';
            };
            //did not start tab
            $scope.managedidntstartTrial = function(){
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                $scope.mainttrialview = false;
                $scope.trialdatatableview = true;
                $scope.active = false;
                $scope.enrolled = false;
                $scope.cancel = false;
                $scope.didntstart = true;
                $scope.getTrialMembers($scope.trialid,'D');
                setTimeout(function(){
                    $scope.resettabbarclass();
                    $('#li-didntstart').addClass('active-event-list').removeClass('event-list-title');
                    $('.managetrial-subtabtext-4').addClass('greentab-text-active');
                },500);
                 if(!$localStorage.detail_closetrialpagefrom){
                     $scope.dtOptions3
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                $localStorage.detail_closetrialpagefrom='';
            };
            //getting and storing particular details
            $scope.manageinitialcall = function(list){
                $scope.particulartrialdetails = list;
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.all_select_flag = 'N';
                $scope.color_green = 'N';
                $scope.selectedData = [];
                $scope.fromtab = list.trial_status;
                $scope.triallength = list.program_length;
                $scope.triallengthtype = list.program_length_type;
                $scope.trialid = list.trial_id;
                $scope.trial_reg_columns = list.reg_columns;
                $scope.trial_lead_columns = list.lead_columns;
                $scope.leadsource = $scope.trial_lead_columns[0].lead_col_name;
                $scope.mainttrialview = false;
                $scope.trialdatatableview = true;
                $scope.conversionpage = list.trial_welcome_url;	
                $scope.trialprice = $scope.particulartrialdetails.price_amount;
                $scope.trialwaiverarea = $scope.particulartrialdetails.trial_waiver_policies;
                $scope.active = true;
                $('#li-active').addClass('active-event-list').removeClass('event-list-title');
                $('.managetrial-subtabtext-1').addClass('greentab-text-active');
                $scope.trialtitle = list.trial_title;
                $scope.trialsubtitle = list.trial_subtitle;
                var trialtitle = angular.element(document.querySelector('#trialtitleaddpart'));
                trialtitle.text($scope.trialtitle);  
                var trialsubtitle = angular.element(document.querySelector('#trialsubtitleaddpart'));
                trialsubtitle.text($scope.trialsubtitle);  
                $scope.getTrialMembers($scope.trialid,'A');
                $scope.particulartrialdetailsview = angular.copy($scope.particulartrialdetails);
                $localStorage.currentManageTrialDetails = list;
                if(!$localStorage.detail_closetrialpagefrom){
                     $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
            };
            //back button function
            $scope.closeTableView = function () {
                if ($scope.fromtab === 'P') {
                    $scope.active = false;
                    $scope.enrolled = false;
                    $scope.cancel = false;
                    $scope.didntstart = false;
                    $scope.triallivelistview();
                    $scope.mainttrialview = true;
                    $scope.trialdatatableview = false;
                    this.triallivelist = $scope.triallivelist;
                } else if ($scope.fromtab === 'S') {
                    $scope.active = false;
                    $scope.enrolled = false;
                    $scope.cancel = false;
                    $scope.didntstart = false;
                    $scope.trialdraftlistview();
                    $scope.mainttrialview = true;
                    $scope.trialdatatableview = false;
                    this.trialdraftlist = $scope.trialdraftlist;
                }  else {
                    $route.reload();
                }
//                else if ($scope.fromtab === 'U') {
//                    $scope.active = false;
//                    $scope.enrolled = false;
//                    $scope.cancel = false;
//                    $scope.didntstart = false;
//                    $scope.trialunpublishedlistview();
//                    $scope.mainttrialview = true;
//                    $scope.trialdatatableview = false;
//                    this.trialunpublishedlist = $scope.trialunpublishedlist;
//                }
            };
            //registration trail
            $scope.addParticipantview = function(){
                $('#trialstartdate').datepicker('setStartDate', new Date());
                var current_date = new Date();
                $scope.trialstartdate = ('0' + (current_date.getMonth() + 1)).slice(-2) + '/' + ('0' + current_date.getDate()).slice(-2) + '/' + current_date.getFullYear();
                $scope.mainttrialview = false;
                $scope.trialdatatableview = false;
                $scope.addtrialparticipantview = true;
                $scope.trial_reg_col_value[0] = '';
                $scope.trial_reg_col_value[1] = '';
                $scope.trial_reg_col_value[2] = '';
                $scope.trial_reg_col_value[3] = '';
                $scope.trial_reg_col_value[4] = '';
                $scope.trial_reg_col_value[5] = '';
                $scope.trial_reg_col_value[6] = '';
                $scope.trial_reg_col_value[7] = '';
                $scope.trial_reg_col_value[8] = '';
                $scope.trial_reg_col_value[9] = '';
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.studentselected = '';
                $scope.discount = '';
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.selectedcard = "";
                $scope.country = "";
                $scope.postal_code = "";
                $scope.waiverchecked = $scope.trial_chk_ach = $scope.stripeccchecked = false;
                $scope.valid_stripe_card = false;
//                $scope.trialwaiverarea = "";
                $scope.trialpaymentform.$setPristine();
                $scope.trialpaymentform.$setUntouched();
                document.getElementById("trialpaymentform").reset();
                $scope.carddetails = [];
                $scope.showcardselection = false;
                $scope.shownewcard = true;   
                $scope.selectedcardtype = 'new';
                $scope.showTrialTotal();
                
                if($scope.stripe_enabled){
                    $scope.stripe = Stripe($localStorage.stripe_publish_key);
                    $scope.getStripeElements();
                }
                
                // CLEAR STRIPE FORM
                if ($scope.stripe_card) {
                    $scope.stripe_card.clear();
                }

                
                if ($('#trialcolumn2').is(':visible')) { //if the container is visible on the page
                    $('#progress-full').show();
                } else {
                    setTimeout(function () {
                        $("#trialcolumn2").dateDropdowns({//date of birth picker
                            displayFormat: "mdy",
                            required: true
                        });
                        $('#progress-full').hide();
                    }, 50);
                }
            };
            // "/" to "-" date format
            $scope.trialdateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            }
            //internet date format
            $scope.trialsafariiosdate = function (dat) {
                var dda = dat.toString();
                dda = dda.replace(/-/g, "/");
                var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

                var given_start_year = curr_date.getFullYear();
                var given_start_month = curr_date.getMonth();
                var given_start_day = curr_date.getDate();
                given_start_month = curr_date.getMonth() + 1;
                var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
                dda = dda.toString();
                dda = dda.replace(/-/g, "/");
                var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 

                return dup_date;
            
            };
            //oct 06,1995 like format
            $scope.trialdatestringview = function (ddate) {
                var Displaydate = ddate.toString();
                Displaydate = ddate.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1] - 1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            };
            //cost less than 5 check
            $scope.trialCostMax = function(disc){
                $scope.actual_trial_cost = 0;
                $scope.actual_trial_cost = +$scope.trialprice;
                if(disc === '' || disc === undefined || disc == 0){
                    $scope.discountvalidation = false;
                    return false;
                }
                if ((parseFloat(disc) > 0 && parseFloat('5') <= (parseFloat($scope.actual_trial_cost) - parseFloat(disc))) || (parseFloat(disc) > 0 && parseFloat('0') == (parseFloat($scope.actual_trial_cost) - parseFloat(disc)))) { 
                    $scope.discountvalidation = false;
                    return false;
                } else {
                    $scope.discountvalidation = true;
                    return true;
                }
               
            };
            //cost less than 5 when discount applied check
            $scope.trialdiscountMax = function(disc){
                $scope.actual_trial_cost = 0;
                $scope.actual_trial_cost = +$scope.trialprice;
                if(disc==='' || disc === undefined || disc == 0){
                    $scope.discountvalidation = false;
                    return false;
                }
                if(parseFloat(disc)>0 && parseFloat(disc)>parseFloat($scope.actual_trial_cost)){
                    $scope.discountvalidation = true;
                    return true;
                }else{
                    $scope.discountvalidation = false;
                    return false;
                }
            };
            //cancel registration view
            $scope.cancelAddParticipant = function () {
                $scope.addtrialparticipantview = false;
                $scope.trialdatatableview = true;
            };
            //getting auto fill details
            $scope.$watch(function () {
                return $scope.studentselected;
            }, function (newVal, oldVal) {
                $scope.participant_studentname = newVal;
                if ($scope.participant_studentname && ($scope.participant_studentname.student_name !== '' || $scope.participant_studentname.student_name !== undefined)) {
                    if ($scope.studentlist.length > 0) {
                        for (i = 0; i < $scope.studentlist.length; i++) {
                            if ($scope.participant_studentname.student_name === $scope.studentlist[i].student_name && $scope.studentlist[i].buyer_first_name !== null  && $scope.studentlist[i].buyer_last_name !== null && $scope.participant_studentname.stud_index === $scope.studentlist[i].stud_index) {
                                $scope.student_id = $scope.studentlist[i].student_id;
                                $scope.student_name = $scope.studentlist[i].student_name;
                                $scope.trial_reg_col_value[0] = $scope.studentlist[i].reg_col_1;
                                $scope.trial_reg_col_value[1] = $scope.studentlist[i].reg_col_2;
                                $scope.trial_reg_col_value[2] = $scope.studentlist[i].reg_col_3;
                                var bchoose_date = $scope.trial_reg_col_value[2].split("-");
                                document.getElementsByClassName("dob_month")[0].value = bchoose_date[1]; 
                                document.getElementsByClassName("dob_day")[0].value = bchoose_date[2]; 
                                document.getElementsByClassName("dob_year")[0].value = bchoose_date[0]; 
                                document.getElementById("trialcolumn2").value = $scope.trial_reg_col_value[2]; 
                                $scope.firstname = $scope.studentlist[i].buyer_first_name;
                                $scope.lastname = $scope.studentlist[i].buyer_last_name;
                                $scope.email = $scope.studentlist[i].buyer_email;
                                $scope.phone = $scope.studentlist[i].buyer_phone;
                                $scope.trial_participant_Streetaddress = $scope.studentlist[i].participant_street; 
                                $scope.trial_participant_City = $scope.studentlist[i].participant_city;
                                $scope.trial_participant_State = $scope.studentlist[i].participant_state;
                                $scope.trial_participant_Zip = $scope.studentlist[i].participant_zip;
                                $scope.trial_participant_Country = $scope.studentlist[i].participant_country;
                                if(!$scope.studentlist[i].card_details || $scope.studentlist[i].card_details.length == 0) {
                                    $scope.showcardselection = false;
                                    $scope.shownewcard = true;   
                                    $scope.selectedcard = "";
                                    $scope.carddetails = [];
                                } else {
                                    $scope.shownewcard = false;   
                                    $scope.showcardselection = true;
                                    $scope.carddetails = $scope.studentlist[i].card_details;
                                }
                                $scope.showTrialTotal(); 
                                return false;
                            } else {
                                $scope.student_id = 0;
                                $scope.student_name = $scope.participant_studentname;
                                $scope.trial_reg_col_value[0] = '';
                                $scope.trial_reg_col_value[1] = '';
                                $scope.trial_reg_col_value[2] = '';
                                document.getElementsByClassName("dob_month")[0].value = ""; 
                                document.getElementsByClassName("dob_day")[0].value = ""; 
                                document.getElementsByClassName("dob_year")[0].value = "";  
                                document.getElementById("trialcolumn2").value = "";
                                $scope.firstname = '';
                                $scope.lastname = '';
                                $scope.email = '';
                                $scope.phone = '';
                                $scope.carddetails = [];
                                $scope.showcardselection = false;
                                $scope.selectedcard = "";
                                $scope.cardnumber = '';
                                $scope.ccmonth = '';
                                $scope.ccyear = '';
                                $scope.cvv = '';
                                $scope.country = '';
                                $scope.postal_code = '';
                                $scope.trial_participant_Streetaddress = '';
                                $scope.trial_participant_City = ''; 
                                $scope.trial_participant_State = ''; 
                                $scope.trial_participant_Zip = ''; 
                                $scope.trial_participant_Country = '';
                                $scope.showTrialTotal(); 
                            }
                        }
                    }

                } else {
                    $scope.student_id = 0;
                    $scope.student_name = $scope.participant_studentname;
                    $scope.trial_reg_col_value[0] = '';
                    $scope.trial_reg_col_value[1] = '';
                    $scope.trial_reg_col_value[2] = '';
                    if(document.getElementsByClassName("dob_month")[0] && document.getElementById("trialcolumn2")){
                        document.getElementsByClassName("dob_month")[0].value = ""; 
                        document.getElementsByClassName("dob_day")[0].value = ""; 
                        document.getElementsByClassName("dob_year")[0].value = "";  
                        document.getElementById("trialcolumn2").value = "";
                    }
                    $scope.firstname = '';
                    $scope.lastname = '';
                    $scope.email = '';
                    $scope.phone = '';
                    $scope.carddetails = [];
                    $scope.showcardselection = false;
                    $scope.selectedcard = "";
                    $scope.cardnumber = '';
                    $scope.ccmonth = '';
                    $scope.ccyear = '';
                    $scope.cvv = '';
                    $scope.country = '';
                    $scope.postal_code = '';
                    $scope.trial_participant_Streetaddress = '';
                    $scope.trial_participant_City = ''; 
                    $scope.trial_participant_State = ''; 
                    $scope.trial_participant_Zip = ''; 
                    $scope.trial_participant_Country = ''; 
                    $scope.showTrialTotal(); 
                }
                               
            });
            //selecting cards
            $scope.selectTrialCardDetail = function () {
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.country = '';
                $scope.postal_code = '';
                if ($scope.selectedcard === 'Add New credit card') {
                    $scope.shownewcard = true;   
                    $scope.selectedcardtype = 'new';
                    $scope.credit_card_id = '';
                    $scope.credit_card_status = '';
                    $scope.credit_card_name = '';
                    $scope.credit_card_expiration_month = '';
                    $scope.credit_card_expiration_year = '';
                    $scope.postal_code = '';
                    $scope.country = "";
                    $scope.cardnumber = '';
                    $scope.ccmonth = '';
                    $scope.ccyear = '';
                    $scope.cvv = '';
                    $scope.payment_method_id = '';
                    $scope.stripe_customer_id = '';    
                } else {
                    $scope.cardindex = $scope.selectedcard;
                    if($scope.stripe_enabled){
                        $scope.payment_method_id = $scope.carddetails[$scope.cardindex].payment_method_id;
                        $scope.stripe_customer_id = $scope.carddetails[$scope.cardindex].stripe_customer_id;
                    }else{
                        $scope.credit_card_id = parseInt($scope.carddetails[$scope.cardindex].credit_card_id);
                        $scope.credit_card_status = $scope.carddetails[$scope.cardindex].credit_card_status;
                        $scope.credit_card_name = $scope.carddetails[$scope.cardindex].credit_card_name;
                        $scope.credit_card_expiration_month = $scope.carddetails[$scope.cardindex].credit_card_expiration_month;
                        $scope.credit_card_expiration_year = $scope.carddetails[$scope.cardindex].credit_card_expiration_year;
                        $scope.postal_code = $scope.carddetails[$scope.cardindex].postal_code;
                        $scope.country = $scope.carddetails[$scope.cardindex].country;
                    }
                    $scope.shownewcard = false;
                    $scope.selectedcardtype = 'old';
                }
            };
            //trial end date calc
            $scope.trialdateview = function () {
                if($scope.trialstartdate){
                    var dupdate = $scope.trialsafariiosdate($scope.trialstartdate);
                    var trialenddatequantity, trial_end;
                    if ($scope.triallengthtype == 'W') {
                        trialenddatequantity = $scope.triallength * 7;   
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    } else if ($scope.triallengthtype == 'D'){
                        trialenddatequantity = $scope.triallength;
                        dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
                        trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
                    }
                    var trial_start = $scope.trialdateformat($scope.trialstartdate); 
                    $scope.trial_start_date_view = $scope.trialdatestringview(trial_start);
                    $scope.trial_end_date_view = $scope.trialdatestringview(trial_end);
                }
            };
            //final amount view with tax
            $scope.showTrialTotal = function (){
                $scope.trialdateview(); var trialprice = 0; var process_fee_per =0; var process_fee_val =0;
                var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                var process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                if($scope.particulartrialdetails)
                    var trialprice = $scope.particulartrialdetails.price_amount;
                $scope.registration_amount = trialprice;
                if ($scope.discount > 0) {
                    trialprice = +trialprice - +$scope.discount;
                }
                if($scope.particulartrialdetails)
                    $scope.current_processing_fee_type = $scope.particulartrialdetails.processing_fee_type;
                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (trialprice > 0) {
                        var process_fee1 = +(parseFloat(trialprice)) + +((parseFloat(trialprice) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = +parseFloat(trialprice) + +$scope.processing_fee_value;
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (trialprice > 0) {
                        var process_fee = +((parseFloat(trialprice) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = trialprice;
                } 
                $scope.payment_amount = trialprice;
                
                if (parseFloat($scope.total_amount_view) == parseFloat('0')) {                    
                        $scope.showpaymentopions = false;
                        $scope.shownewcard = false;
                        $scope.selectedcard = '';
                } else {
                    $scope.showpaymentopions = true;
                    if ($scope.carddetails.length == 0 || !$scope.carddetails)
                        $scope.shownewcard = true;
                }
            };
            //select all-datatable
            $scope.trialtoggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
//                            $scope.selectedData.push({"id":id});
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
            };
           //select individuals-datatables
           $scope.trialtoggleOne= function (selectedItems) {
               $scope.maintainselected = selectedItems;
                var check=0;
                  $scope.all_select_flag='N';
                   $scope.color_green='N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        }else{
                            $scope.selectedData.push({"id":id});
                             $scope.color_green='Y';
                        }
                         
                    }
                }
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
            };
            //datatable ajax calls
            $scope.getTrialMembers = function(trialid, tab){
                 $scope.current_tab=tab;
                 $localStorage.trialdetail_pagefrom = $scope.current_tab;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="trialtoggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
               
                if ($scope.current_tab === 'A') {
                   $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'gettrialparticipants',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallist_temp = json.data;
                                    $scope.count_active = json.active;
                                    $scope.count_enrolled = json.enrolled;
                                    $scope.count_cancel = json.cancelled;
                                    $scope.count_didntstart = json.didnotstart;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_id": trialid,
                                 "trial_status_tab": tab,
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.dtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_ta1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'managetrial'" + ',' + full.trial_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_ta2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),                                
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_ta3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_ta4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_ta5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_ta6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_ta7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_ta8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_ta9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_ta10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }else if ($scope.current_tab === 'E') {
                   $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'gettrialparticipants',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallist_temp = json.data;
                                     $scope.count_active = json.active;
                                    $scope.count_enrolled = json.enrolled;
                                    $scope.count_cancel = json.cancelled;
                                    $scope.count_didntstart = json.didnotstart;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_id": trialid,
                                 "trial_status_tab": tab,
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.dtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_te1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'managetrial'" + ',' + full.trial_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_te2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_te3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_te4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_te5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_te6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_te7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_te8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_te9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_te10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }else if ($scope.current_tab === 'C') {
                   $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'gettrialparticipants',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallist_temp = json.data;
                                     $scope.count_active = json.active;
                                    $scope.count_enrolled = json.enrolled;
                                    $scope.count_cancel = json.cancelled;
                                    $scope.count_didntstart = json.didnotstart;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_id": trialid,
                                 "trial_status_tab": tab,
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.dtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_tc1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'managetrial'" + ',' + full.trial_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_tc2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_tc3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_tc4').renderWith(
                                function (data, type, full,meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                            }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_tc5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_tc6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_tc7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_tc8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_tc9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_tc10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }else if ($scope.current_tab === 'D') {
                   $scope.dtOptions3 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                           url: urlservice.url + 'gettrialparticipants',
                           xhrFields: {
                            withCredentials: true
                         },
                            dataSrc: function (json) {
                                $scope.triallist_temp = json.data;
                                     $scope.count_active = json.active;
                                    $scope.count_enrolled = json.enrolled;
                                    $scope.count_cancel = json.cancelled;
                                    $scope.count_didntstart = json.didnotstart;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "trial_id": trialid,
                                 "trial_status_tab": tab,
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [7, 'desc'])
                    $scope.dtColumns3 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="trialtoggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_td1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="TrialIndividualDetail(\'' + full.id + '\' ,' + "'managetrial'" + ',' + full.trial_id + ');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_td2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                                }),
                                DTColumnBuilder.newColumn('age').withTitle('Age').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.age + '</span>';
                                }),
                                DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_td3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_phone + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_td4').renderWith(
                                function (data, type, full,meta) {
                                    if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendTrialindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';

                                }
                                }),
                                 DTColumnBuilder.newColumn('trial_status').withTitle('Trial status').withClass('col_td5').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_status + '</span>';
                                }),
                                 DTColumnBuilder.newColumn('Registered_date').withTitle('Registered date').withClass('col_td6').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.registration_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('trial_name').withTitle('Trial Program').withClass('col_td7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.trial_program + '</span>';
                                }),
                                DTColumnBuilder.newColumn('Start_date').withTitle('Start date').withClass('col_td8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.start_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('end_date').withTitle('End date').withClass('col_td9').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                                DTColumnBuilder.newColumn('lead_source').withTitle('Lead Source').withClass('col_td10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.leadsource + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }
                $('#progress-full').hide();
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
//                    console.log('dataTables search : ' + $scope.searchTerm);
                })
            };
            
            
             $scope.showemailerror = function (row, type, data, event, id) {
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.mem_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.triallist_temp[row].error;
                $scope.bouncedemail = data;
                $("#emailerrormessageModal").modal('show');
                $("#emailerrormessagetitle").text('Message');
                $("#emailerrormessagecontent").text($scope.errormsg);
            };
            
            $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
//                      angular.element($scope.email_updt).text($scope.bouncedemail);
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendTrialindividualpush(\'' + $scope.mem_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
//                      angular.element($scope.email_updt).attr('ng-click',"sendindividualpush('"+$scope.mem_id+"','email')");
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_5')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_6')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_7')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_trail_8')).contents())($scope);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            //redirected from trialdetails page
            if($localStorage.detail_closetrialpagefrom === 'A' || $localStorage.detail_closetrialpagefrom === 'E' ||$localStorage.detail_closetrialpagefrom === 'C' || $localStorage.detail_closetrialpagefrom === 'D'){
              
                $scope.currentManageTrialDetails = $localStorage.currentManageTrialDetails;
                $scope.manageinitialcall($scope.currentManageTrialDetails);
                if ($localStorage.detail_closetrialpagefrom === 'A') {
                    $scope.manageLiveTrial();
                }else if($localStorage.detail_closetrialpagefrom === 'E') {
                    $scope.manageenrolledTrial();
                }else if($localStorage.detail_closetrialpagefrom === 'C') {
                    $scope.managecancelledTrial();
                }else if($localStorage.detail_closetrialpagefrom === 'D') {
                    $scope.managedidntstartTrial();
                }
//                $localStorage.detail_closetrialpagefrom = "";
            }
            
            $scope.trialCheckout = function(credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code,country){
                $('#progress-full').show();
                var trialstartdate = $scope.trialdateformat($scope.trialstartdate);
                var birthdate = document.getElementById("trialcolumn2").value;
                var final_total_trial_amount = (Math.round((+$scope.payment_amount + 0.00001) * 100) / 100).toFixed(2);
                var registration_amount = (Math.round((+$scope.registration_amount + 0.00001) * 100) / 100).toFixed(2);
                
                if (!user_first_name) {
                    user_first_name = $scope.firstname;
                }
                if (!user_last_name) {
                    user_last_name = $scope.lastname;
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'wepayTrialprogramCheckout',
                    data: {
                        company_id:  $localStorage.company_id,
                        trial_id:  $scope.trialid,
                        trial_name: $scope.trialtitle,
                        desc: $scope.trialtitle,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email : email,
                        phone : phone,
                        postal_code : postal_code,
                        country : country,
                        registration_amount : registration_amount,
                        discount : $scope.discount,
                        payment_amount: final_total_trial_amount,
                        processing_fee_type: $scope.current_processing_fee_type,
                        reg_col1: $scope.trial_reg_col_value[0],
                        reg_col2: $scope.trial_reg_col_value[1],
                        reg_col3: birthdate,
                        lead_source : $scope.leadsource,
                        reg_col5: $scope.trial_reg_col_value[4],
                        reg_col6: $scope.trial_reg_col_value[5],
                        reg_col7: $scope.trial_reg_col_value[6],
                        reg_col8: $scope.trial_reg_col_value[7],
                        reg_col9: $scope.trial_reg_col_value[8],
                        reg_col10: $scope.trial_reg_col_value[9],
                        trial_participant_Streetaddress : $scope.trial_participant_Streetaddress,
                        trial_participant_City : $scope.trial_participant_City,
                        trial_participant_State : $scope.trial_participant_State,
                        trial_participant_Zip : $scope.trial_participant_Zip,
                        trial_participant_Country : $scope.trial_participant_Country,
                        cc_id : credit_card_id,
                        cc_state : state,
                        upgrade_status: $localStorage.upgrade_status,
                        program_length:$scope.triallength,
                        program_length_type:$scope.triallengthtype,
                        payment_startdate : trialstartdate
                    },
                
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.cancelAddParticipant();
                        $('#progress-full').hide();
                        $("#trialpaymentmessageModal").modal('show');
                        $("#trialpaymentmessagetitle").text('Message');
                        $("#trialpaymentmessagecontent").text(response.data.msg);
                        $scope.redirect_conversionpage = "";
                        $scope.selectedcard = "";
                        if($scope.conversionpage) {
                            if ($scope.conversionpage.indexOf("http://") === 0 || $scope.conversionpage.indexOf("https://") === 0) {
                                $scope.redirect_conversionpage = $scope.conversionpage;
                            } else {
                                $scope.redirect_conversionpage = 'http://' + $scope.conversionpage;
                            }
                        }
                        $scope.onetimecheckout = 0;
                        $scope.manageLiveTrial();
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg); 
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        $scope.onetimecheckout = 0;
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        if(response.data.error){
                            $("#trialmessagetitle").text(response.data.error);
                            $("#trialmessagecontent").text(response.data.error_description);
                        }else{
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                        }
                    }   
                }, function (response) {
                    $scope.onetimecheckout = 0;
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.redirecturl = function(){
                if($scope.redirect_conversionpage){
                    window.open($scope.redirect_conversionpage,"_blank");
                }
            };
            
            $scope.copytrial = function (trial_detail) {
                $('#progress-full').show();
                $http({
                method: 'POST',
                        url: urlservice.url + 'copytrialprogram',
                        data: {
                            'trial_id': trial_detail.trial_id,
                            'list_type': trial_detail.trial_status,
                            'trial_template_flag': 'N',
                            'company_id': $localStorage.company_id
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                        }).then(function (response) {
                            if (response.data.status === 'Success') {
                                $('#progress-full').hide();
                                $("#trialmessageModal").modal('show');
                                $("#trialmessagetitle").text('Success Message');
                                $("#trialmessagecontent").text('Duplicate trial category copied successfully');
//                                $scope.triallist = response.data.msg;
//                                $scope.triallivelist = $scope.triallist.live;
//                                $scope.trialdraftlist = $scope.triallist.draft;
//                                $scope.trialunpublishedlist = $scope.triallist.unpublished;

                                $scope.NumofLive = response.data.count.P;
                                $scope.NumofDraft = response.data.count.S;
                                $scope.trialurl = response.data.trial_list_url;
                                if(parseInt($scope.NumofLive) > 0){
                                    $scope.nolivetriallisted = false; 
                                }else{
                                    $scope.nolivetriallisted = true; 
                                }
                                if(parseInt($scope.NumofDraft) > 0){
                                    $scope.nodrafttriallisted = false;
                                }else{
                                    $scope.nodrafttriallisted = true;
                                }
                                if(trial_detail.trial_status === 'P'){
                                    $scope.triallivelist = response.data.msg.live;
                                    $scope.trialdraftlist = [];
                                }else if(trial_detail.trial_status === 'S'){
                                    $scope.trialdraftlist = response.data.msg.draft;
                                    $scope.triallivelist = [];
                                }
                                
                            }else if(response.data.status === 'Expired'){
                                $('#progress-full').hide();         
                                $("#trialmessageModal").modal('show');
                                $("#trialmessagetitle").text('Message');
                                $("#trialmessagecontent").text(response.data.msg); 
                                $scope.logout();
                            }else{ 
                                 $scope.handleFailure(response.data.msg);
                            }   
                        }, function (response) {
                            console.log(response.data);
                            $scope.handleError(response.data);
                });
            };
            //navigate trial detail
            $scope.TrialIndividualDetail = function ( trial_reg_id, page, trial_id) {
                $localStorage.currentTrialregid = trial_reg_id;
                $localStorage.currentTrial_id = trial_id;
                $localStorage.trialpagefrom = page;
                $localStorage.trialfromtab = $scope.fromtab;
                $location.path('/trialdetail');
            };
            // export csv
            $scope.trialexportcsv = function(trial_id){
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'exporttrialdata',
                responseType: 'arraybuffer',
                data: {
                    "company_id": $localStorage.company_id,
                    "trial_id" : trial_id,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).success(function (data, status, headers) {
                var response = '';
                if ('TextDecoder' in window) {
                    // Decode as UTF-8
                    var dataView = new DataView(data);
                    var decoder = new TextDecoder('utf8');
                    if($scope.isJson(decoder.decode(dataView))===true){
                        response = JSON.parse(decoder.decode(dataView));
                    }
                } else {
                    // Fallback decode as ASCII
                    var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                    if($scope.isJson(decodedString)===true){
                        response = JSON.parse(decodedString);
                    }
                }
                if(response.status !== undefined && response.status==='Failed'){
                    $("#trialmessageModal").modal('show');
                    $("#trialmessagetitle").text('Message');
                    $("#trialmessagecontent").text(response.msg);
                    $('#progress-full').hide();
                    return false;
                }else{
                    $("#trialmessageModal").modal('show');
                    $("#trialmessagetitle").text('Message');
                    $("#trialmessagecontent").text("Participant Details exported as csv file successfully.");
                }
                var msg = '';
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], {type: "application/x-download"});
                    var objectUrl = URL.createObjectURL(blob);
                    linkElement.setAttribute('href', objectUrl);
                    linkElement.setAttribute("download", 'Trial program' +".csv"); 
                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    msg = ex;
                    console.log(ex);
                }
                if(msg===''){
                    msg = "CSV file downloaded Successfully";
                }
                $('#progress-full').hide();
                $("#trialmessageModal").modal('show');
                $("#trialmessagetitle").text('Message');
                $("#trialmessagecontent").text(msg);

            }).error(function () {
                $('#progress-full').hide();
                $("#trialmessageModal").modal('show');
                $("#trialmessagetitle").text('Message');
                $("#trialmessagecontent").text("Unable to reach Server, Please Try again.");
            });
        };
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
           //Trial program drag and drop on live 
            $scope.dragtrialCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_trial_id = $scope.triallivelist[ind].trial_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.triallivelist.length;i++){ 
                            if($scope.triallivelist[i].trial_id === $scope.start_trial_id){
                                $scope.new_location_id = i;
                            }
                        }
            
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_trial_id, sort_trial_status, trial_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_trial_id = $scope.triallivelist[$scope.new_location_id].trial_id;
                                sort_trial_status = $scope.triallivelist[$scope.new_location_id].trial_status;
                                previous_sort_order = parseFloat($scope.triallivelist[$scope.new_location_id + 1].trial_sort_order);
                                trial_sort_order = previous_sort_order + 1;
                                $scope.sortordertrialupdate(sort_trial_id, trial_sort_order, sort_trial_status);
                            } else if ($scope.new_location_id === $scope.triallivelist.length - 1) {
                                sort_trial_id = $scope.triallivelist[$scope.new_location_id].trial_id;
                                sort_trial_status = $scope.triallivelist[$scope.new_location_id].trial_status;
                                trial_sort_order = parseFloat($scope.triallivelist[$scope.new_location_id - 1].trial_sort_order) / 2;
                                $scope.sortordertrialupdate(sort_trial_id, trial_sort_order, sort_trial_status);
                            } else {
                                sort_trial_id = $scope.triallivelist[$scope.new_location_id].trial_id;
                                sort_trial_status = $scope.triallivelist[$scope.new_location_id].trial_status;
                                previous_sort_order = parseFloat($scope.triallivelist[$scope.new_location_id + 1].trial_sort_order);
                                next_sort_order = parseFloat($scope.triallivelist[$scope.new_location_id - 1].trial_sort_order);
                                trial_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortordertrialupdate(sort_trial_id, trial_sort_order, sort_trial_status);
                            }
                        }else{
                           $('#progress-full').hide();
                        }
            
                    }, 1000);
                }
            };
            
             //updating Trial program sort order after drag and drop on live 
            $scope.sortordertrialupdate = function(trial_id,sort_id, sort_status){
                $('#progress-full').show();
                $localStorage.trialSorted = 'N';     
                $http({
                    method: 'POST',
                    url: urlservice.url+'updateTrialCategorySortOrder',
                    data: {
                        "sort_id": sort_id,
                        "trial_id": trial_id,
                        "list_type": sort_status,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status === 'Success'){
//                        $scope.triallivelist = response.data.trial_details.msg.live;
//                        $scope.trialdraftlist = response.data.trial_details.msg.draft;
//                        $scope.trialunpublishedlist = response.data.trial_details.msg.unpublished;
//                        $scope.trialurl = response.data.trial_details.trial_list_url;
                        $localStorage.trialSorted = 'Y';
                        
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.trialurl = response.data.trial_list_url;
                        if(parseInt($scope.NumofLive) > 0){
                            $scope.nolivetriallisted = false; 
                        }else{
                            $scope.nolivetriallisted = true; 
                        }
                        if(parseInt($scope.NumofDraft) > 0){
                            $scope.nodrafttriallisted = false;
                        }else{
                            $scope.nodrafttriallisted = true;
                        }
                        if(sort_status === 'P'){
                            $scope.triallivelist = response.data.msg.live;
                            $scope.trialdraftlist = [];
                        }else if(sort_status === 'S'){
                            $scope.trialdraftlist = response.data.msg.draft;
                            $scope.triallivelist = [];
                        }
                        $('#progress-full').hide();
                    }else if(response.data.status === 'Expired'){
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);  
                        $scope.logout();
                    }else{
                        $localStorage.trialSorted = 'N';                        
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            //value copied on clipboard message
            $scope.toastTrialmsg = function (trialtype) {   
                var id_name = "";
                if(trialtype === 'triallist'){
                    id_name = "triallistsnackbar";
                }else if(trialtype === 'triallive'){
                    id_name = "triallivesnackbar";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            //Edit action for Trial program
            $scope.edittrial = function (detail) {
                $localStorage.trialpagetype = 'edittrial';
//                $localStorage.isAddedCategoryDetails = 'N';
                trialListDetails.addTrialDetail(detail);
                $location.path('/addtrial');
            };
            
            
            $scope.sendTrialGroupMsg = function (type) { 
             
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 || $scope.all_select_flag==='Y') {
                    if (type === 'push') {
//                        for (i = 0; i < $scope.membershiplistcontents.length; i++) {
//                            if ($scope.membershiplistcontents[i].show_icon === 'N') {
//                                $("#msgModal").modal('show');
//                                $("#msgtitle").text('Message');
//                                $("#msgcontent").text('One of the selected member doesnot have registered mobile number to receive push message. Kindly deselect the member who didnot registered mobile number.');
//                                return;
//                            } else {
//                                //SELECTED MEMBER HAS MOBILE DEVICE
//                            }
//                        }
                        $("#grpTrialpushmessagecontModal").modal('show');
                        $("#grpTrialpushmessageconttitle").text('Send Push Message');
                    } else if (type === 'mail') {
                        $("#grpTrialemailmessagecontModal").modal('show');
                        $("#grpTrialemailmessageconttitle").text('Send Email');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if (type === 'csv') {
                        $("#grpTrialcsvdownloadModal").modal('show');
                        $("#grpTrailcsvdownloadtitle").text('Message');
                        $("#grpTrialcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
           } else {
                    var x = document.getElementById("trailselection")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            };
           
            $scope.sendGroupTrialEmailPush = function (message_type) {
                $('#progress-full').show();
                var msg = '';
                var subject,unsubscribe_email = '';
                if (message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if (message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if(message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendTrialGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_trial_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "all_select_flag":$scope.all_select_flag,
                        "trial_id": $scope.trialid ,
                        "from":"Trial",
                        "trial_status":  $scope.current_tab ,
                        "search": $scope.searchTerm,
                        "unsubscribe_email":unsubscribe_email

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $scope.clearTrialSelectedList();
                         $scope.color_green = 'N';
                        $('#progress-full').hide();
                        $("#grpTrialemailmessagecontModal").modal('hide');
                        $("#grpTrialemailmessageconttitle").text('');
                        $("#grpTrialpushmessagecontModal").modal('hide');
                        $("#grpTrialpushmessageconttitle").text('');
                        $("#sendsubscriptioninviteModal").modal('hide');
                    if (response.data.status === 'Success') { 
                         $('#progress-full').hide();
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                           $scope.clearTrialSelectedList();
                        $("#grpTrialemailmessagecontModal").modal('hide');
                        $("#grpTrialemailmessageconttitle").text('');
                        $("#grpTrialpushmessagecontModal").modal('hide');
                        $("#grpTrialpushmessageconttitle").text('');
                        $scope.handleFailure(response.data.msg);
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            //clear email modal
            $scope.clearTrialSelectedList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                if($scope.clearfromindividual){//clear if modal open from individual
                   $scope.selectedData = [];
                }
                $scope.clearfromindividual = false; 
            };
            //clear email modal
             $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
            
            
            
            
            $scope.sendTrialindividualpush = function (selectedMember, type) {  // INDIVIDUAL PUSH MESSAGE
                $scope.clearfromindividual = true; 
                $scope.selectAll = false;
                $scope.trialtoggleAll(false,$scope.maintainselected);
                $scope.message_type = type;
                 $scope.selectedData = [];
//                $scope.clearSelectedList();
                $scope.selectedData.push({"id":selectedMember});
//                $scope.membershiplistcontents.push(selectedMember);
               
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#grpTrialpushmessagecontModal").modal('show');
                    $("#grpTrialpushmessageconttitle").text('Send Push Message');
                } else {
                    $("#grpTrialemailmessagecontModal").modal('show');
                    $("#grpTrialemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            
            
            $scope.exportcheckedTrialexcel = function () {
             function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                 angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
//                    console.log('dataTables search : ' + $scope.searchTerm);
                })
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createTrialSelectonCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_trial_list": $scope.selectedData,
                        "all_select_flag":$scope.all_select_flag,
                        "trial_id":$scope.trialid,
                        "from":"Trial",
                        "trial_status": $scope.current_tab,
                        "search":$scope.searchTerm
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    $scope.clearTrialSelectedList();
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.msg);                        
                        return false;
                    } else {
                       $scope.clearTrialSelectedList();
                        $('#progress-full').hide();
                        $("#msgModal").modal('show');
                        $("#msgtitle").text('Message');
                        $("#msgcontent").text("Participant Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "Trial_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
//                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearTrialSelectedList();
                    $('#progress-full').hide();
                    $("#msgModal").modal('show');
                    $("#msgtitle").text('Message');
                    $("#msgcontent").text(response.data.msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            
            
            $scope.openAddnewTrialModal = function () {
                $('#addnewtrial-modal').modal('show');
            };
            //add trial page
            $scope.addTrial = function () {
               $('#addnewtrial-modal').modal('hide');
               $localStorage.trialpagetype = '';
               $localStorage.addtriallocaldata = '';
               $localStorage.triallocaldata = '';
               $('#addnewtrial-modal').on('hidden.bs.modal', function (e) {
                   $window.location.href = window.location.origin+window.location.pathname+'#/addtrial';
//                 console.log($window.location.href);
               })
           };
            //template page
            $scope.getTemplate = function () {
                $('#addnewtrial-modal').modal('hide');
                $localStorage.currentpage = '';
                $('#addnewtrial-modal').on('hidden.bs.modal', function (e) {
                   $window.location.href = window.location.origin+window.location.pathname+'#/trialtemplate';
//                 console.log($window.location.href);
               })
            };
            
           if($localStorage.redirectedfrmtemplate) {
                $("#trialtemplatemessageModal").modal('show');
                $("#trialtemplatemessagetitle").text('Message');
                $("#trialtemplatemessagecontent").text('Trial category successfully copied inside "Draft" folder');
            }
            
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };
            
            $scope.handleCheckout = function(){
                if($scope.stripe_enabled){
                    $scope.stripeTrialCheckout();
                }else{
                    $scope.trialCheckout('','',$scope.firstname, $scope.lastname, $scope.email, $scope.phone,'','')
                }
            };
        
           } else {
            $location.path('/login');
        }

    }
    module.exports =  ManageTrialController;

