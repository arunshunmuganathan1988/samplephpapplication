PaymentController.$inject=['$scope','$compile', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','$window','$route'];
 function PaymentController($scope,$compile, $location, $http, $localStorage, urlservice, $filter, $rootScope, DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions, $window, $route)
    {


        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;

//            $scope.dtOptions = DTOptionsBuilder.newOptions()
//                    .withOption('scrollX', '100%')
//                    .withOption('order', [])
//                    .withOption('scrollY', '100%')
//                    .withOption('paging', false)
//                    .withOption('info', false);
//            $scope.dtColumnDefs = [
//                DTColumnDefBuilder.newColumnDef([0]).withOption('type', 'date')
//            ];
            
            $rootScope.activepagehighlight = 'home';
            $scope.Approvedlistshow = true;
            $scope.PastDuelistshow = false;
            $scope.Failedlistshow = false;
            $scope.Upcominglistshow = false;
//            $scope.actionlist = ["Refund Payment", "Resend Receipt"];
//            $scope.actionlistpast = ["Re-run Payment", "Edit Payment Method", "Apply Payment Credit"];
//            $scope.actionlistupcoming = ["Edit Order", "Edit Payment Method", "Apply Payment Credit"];
            $scope.editPaymentMethod_category = '';
            $scope.approvestartdate = '';
            $scope.approveenddate = '';
            $scope.approvepaymentdays = '1';
            $scope.paststartdate = '';
            $scope.pastenddate = '';
            $scope.pastpaymentdays = '5';
            $scope.upcomingpaymentdays = '1';
            $scope.upcomingstartdate = '';
            $scope.upcomingenddate = '';
//            $scope.failedpaymentdays = '1';
//            $scope.failedstartdate = '';
//            $scope.failedenddate = '';
//            $scope.paymentcategory='';
            $scope.approvepaymentcategory = 'A';
            $scope.pastpaymentcategory = 'A';
            $scope.upcomingpaymentcategory = 'A';
            $scope.failedpaymentcategory = 'A';
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.paymanual='';
            $scope.editIndex = "";
            $scope.editSingle = "";
            $scope.showquantityedit = false;
            $scope.creditamount = 0;
            $scope.editquantityIndex = "";
            $scope.qtyvalue = "";
            $scope.cancellation_button_text = "Process Cancellation";
            $scope.showChangesinCancellation = false;
//            $scope.currency = '$';
            $scope.cancel_type = '';
            $scope.update_Payment_plan_status = 'N';
            $scope.remainingDueValue = 0;
            $scope.remDueFeeText = '';
            $scope.showRemDueProccessingFee = false;
            $scope.manual_credit_show = false;
            $scope.manual_credit_refund = 0;
            $scope.current_manual_credit_details = [];
            $scope.new_manual_credit_details = [];
            $scope.event_startdate_limit = '';
            $scope.event_enddate_limit = '';
            $localStorage.buyerName = "";
            $localStorage.participantName = "";
            $localStorage.currentMembershipregid = "";
            $localStorage.currentEventregid ='';
            $scope.data_loaded_length = 0;
            $localStorage.retailpagefrom = '';
            $scope.redirect_to_stripecreation = false;

            $scope.Approved_total_final = $scope.Past_total_final = $scope.Upcoming_total_final = $scope.Manual_total_final=0;
            $scope.paycash = '';
            $scope.paycheck = '';
            $scope.payment_tab_view = true;
            $scope.payout_view = false;
            $scope.valid_stripe_card = false;
            $scope.stripe_ach_route_no = '';
            $scope.stripe_ach_acc_number = '';
            $scope.stripe_ach_holder_name = '';
            $scope.stripe_ach_acc_type = '';
            $scope.stripe_payment_method = ''; 
            $scope.stripecurrencycode = '';
            $scope.payout_list = [];
            $scope.external_accounts = [];
            $scope.stripe_name = '';
            $scope.stripe_business_name = '';
            $scope.stripe_card = $scope.stripe_publish_key = $scope.window_hostname = '';
            $scope.stripe_publish_key = $localStorage.stripe_publish_key; 
            $scope.stripe_available_balance = '';
            $scope.stripe_pending_balance = '';
            
            if($localStorage.loginDetails.country){
                $scope.company_country = $localStorage.loginDetails.country;
            }

            $scope.paymentdetails_list=function(flag){
                 $scope.new_list_flag=flag;
                 if ($scope.new_list_flag === 'A') {
                     
                       var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                        $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetails',
                          xhrFields: {
                            withCredentials: true
                         },
//                            type: 'POST',
                            dataType: 'json',
                                data: {
                                    "payment_type": $scope.new_list_flag,
                                    "company_id": $localStorage.company_id,
                                },
                                dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                $scope.paytotal=json.paytotal;
                                $scope.paymanual=json.paymanual;
                                $scope.paycash = json.paycash;
                                $scope.paycheck = json.paycheck;
                                $scope.$apply();
                                return json.data;
                            },
                           error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                      
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'desc'])
                  
                    $scope.dtColumns = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('last_updt_dt').withTitle('Payment Date').withClass('col_a1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_a2').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_a3').renderWith(
                                function (data, type, full) {
                        return '<a class="buyername"   data-toggle="tooltip" title="' + data + '" ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'approved'" +');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_a4').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_a5'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_a6').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_a7'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_a8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_a9').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('net_amount').withTitle('Net').withClass('col_a10').renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        DTColumnBuilder.newColumn('tax_value').withTitle('Tax Collected').withClass('col_a11').renderWith(function(data,type,full){
                            return  data;
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_a11'),
                        DTColumnBuilder.newColumn('card_credit_string').withTitle('Payment Method').withClass('col_a12').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.card_credit_string + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }else if($scope.new_list_flag === 'P') {
                     var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                    $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetails',
                          xhrFields: {
                            withCredentials: true
                         },
                         dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data; 
                                  $scope.paytotal=json.paytotal;
                                   $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                                data: {
                                    "payment_type": $scope.new_list_flag,
                                    "company_id": $localStorage.company_id,
                                },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })


//                        .withDataProp(function (json) {
//                              $scope.paytotal=json.paytotal;
//                                 console.log($scope.paytotal);
//                                return json.data;
//                        })
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'desc'])
                  
                    $scope.dtColumns1 = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_p1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }), 
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_p2').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'past'" +');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_p3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_p4'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_p5').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_p6'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_p7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_p8').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_p9').notSortable(),
                        DTColumnBuilder.newColumn('credit_card_name').withTitle('Payment Method').withClass('col_p10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.credit_card_name + '</span>';
                                }),
                    ];
                    
                }else if($scope.new_list_flag === 'U'){
                    var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                    $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetails',
                          xhrFields: {
                            withCredentials: true
                         },
                         dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                  $scope.paytotal=json.paytotal;
                                   $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                                data: {
                                    "payment_type": $scope.new_list_flag,
                                    "company_id": $localStorage.company_id,
                                },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'asc'])
                  
                    $scope.dtColumns2 = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_u1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }), 
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_u2').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'upcoming'" +');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_u3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_u4'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_u5').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_u6'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_u7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_u8').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_u9').notSortable(),
                        DTColumnBuilder.newColumn('credit_card_name').withTitle('Payment Method').withClass('col_u10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.credit_card_name + '</span>';
                                }),
                    ];
                }
                  $('#progress-full').hide();
             }
             
             
             $scope.showemailerror = function (row, type, data, event, id,cate) {
//                angular.element(event.target).parent().html('<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + id + '\' ,' + "'mail'" + ');">' + data + '</a>');
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.mem_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.paymentdetails_array[row].error;
                $scope.bouncedemail = data;
                $scope.cate_type=cate;
                $("#emailerrormessageModal").modal('show');
                $("#emailerrormessagetitle").text('Message');
                $("#emailerrormessagecontent").text($scope.errormsg);
            };
            
            $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
//                      angular.element($scope.email_updt).text($scope.bouncedemail);\'' + full.Category + '\'
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendindividualpush(\'' + $scope.mem_id + '\' ,\''+  $scope.cate_type + '\',' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
//                      angular.element($scope.email_updt).attr('ng-click',"sendindividualpush('"+$scope.mem_id+"','email')");
                        $compile(angular.element(document.getElementById('DataTables_Table_4')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_5')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_6')).contents())($scope);
                       
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };

            $scope.Approvedlistview = function () { // APPROVED TAB VIEW
                //      $scope.listview_flag = 'A';
                $scope.paymentdate = '1';
                $scope.approvepaymentcategory = 'A';
                $scope.approvestartdate = '';
                $scope.approveenddate = '';
                $scope.approvepaymentdays = '1';
//                $scope.paymentcategory= $scope.approvepaymentcategory;
                $scope.paymentoption = "1";
                $scope.payment_tab_view = true;
                $scope.payout_view = false;
                $scope.Approvedlistshow = true;
                $scope.PastDuelistshow = false;
                $scope.Failedlistshow = false;
                $scope.Upcominglistshow = false;
                $scope.paymentdetails_list('A');
//                $scope.scrollByTableId('payment_Table_Approved', 'A');
                $scope.resettabbarclass();
                $('#li-approved').addClass('active-event-list').removeClass('event-list-title');
                if(!$localStorage.page_from){
                    $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                 $localStorage.page_from=''
            };

            $scope.PastDuelistview = function () { //PAST TAB VIEW
                $scope.listview_flag = 'P';
                $scope.paymentdate = '1';
                $scope.pastpaymentcategory = 'A';
                $scope.paststartdate = '';
                $scope.pastenddate = '';
                $scope.pastpaymentdays = '5';
//                $scope.paymentcategory= $scope.pastpaymentcategory;
                $scope.paymentoption = "1";
                $scope.payment_tab_view = true;
                $scope.payout_view = false;
                $scope.Approvedlistshow = false;
                $scope.PastDuelistshow = true;
                $scope.Failedlistshow = false;
                $scope.Upcominglistshow = false;
                $scope.paymentdetails_list('P');
//               console.log(JSON.stringify($scope.paymentdetails_array));
                $scope.resettabbarclass();
//                $scope.scrollByTableId('payment_Table_Past', 'P');
                $('#li-past').addClass('active-event-list').removeClass('event-list-title');
               if(!$localStorage.page_from){
                    $scope.dtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                 $localStorage.page_from=''
            };

//            $scope.Failedlistview=function(){ //FAILED TAB VIEW//
//                $scope.listview_flag='F';
//                $scope.paymentdate ='1'; 
//                $scope.Approvedlistshow = false;
//                $scope.PastDuelistshow = false;
//                $scope.Failedlistshow = true;
//                $scope.Upcominglistshow =false;
//                $scope.resettabbarclass();
//                $('#li-failed').addClass('active-event-list').removeClass('event-list-title');
//            };

            $scope.Upcominglistview = function () { //UPCOMMING TAB VIEW
                $scope.listview_flag = 'U';
                $scope.paymentdate = '1';
                $scope.upcomingpaymentcategory = 'A';
                $scope.upcomingpaymentdays = '1';
                $scope.upcomingstartdate = '';
                $scope.upcomingenddate = '';
//                $scope.paymentcategory= $scope.failedpaymentcategory;
                $scope.payment_tab_view = true;
                $scope.payout_view = false;
                $scope.Approvedlistshow = false;
                $scope.PastDuelistshow = false;
                $scope.Failedlistshow = false;
                $scope.Upcominglistshow = true;
                $scope.paymentdetails_list('U');
                $scope.resettabbarclass();
//                $scope.scrollByTableId('payment_Table_Upcomming', 'U');
                $('#li-upcoming').addClass('active-event-list').removeClass('event-list-title');
                if(!$localStorage.page_from){
                    $scope.dtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                }
                 $localStorage.page_from=''
            };

            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-payment').addClass('active-event-list').removeClass('event-list-title');
            };
            // DATE PICKER FUNCTION CALL
            $scope.$on('$viewContentLoaded', function () {
                $('#aeventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#aeventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#peventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#peventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#feventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#feventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
            });
            $scope.$on('$viewContentLoaded', function () {
                $('#ueventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '+1d',
                    ignoreReadonly: true
                });
            });

            $scope.$on('$viewContentLoaded', function () {
                $('#ueventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '+1d',
                    ignoreReadonly: true
                });
            });
            
            $scope.getCurrentTabId = function(tab){
                $scope.current_tab_id = '';
                if(tab === 'A'){
                   $scope.current_tab_id = 'payment_Table_Approved'; 
                }else if(tab === 'P'){
                   $scope.current_tab_id = 'payment_Table_Past';  
                }else{
                   $scope.current_tab_id = 'payment_Table_Upcomming';   
                }
            };
            
           

            $scope.filterByDate = function (type,daystype,catetype) { // FILTERS THE PAYMENTS PAGE RESULTS BY APPLYING DATE
                $('#progress-full').show();
//                console.log('data_received_filter : ' + data_received_filter);
                if (type === 'A') { // FILTERS DATA OF APPROVED TAB
                    type = 'A';
                    $scope.payment_array_approved = '';
                    $scope.Approved_total_final = $scope.Manual_total_final = 0;
                    $scope.event_paymentdaystype = daystype;
                    $scope.paymentcategorytype = catetype;
                    if ($scope.event_paymentdaystype === '4') {
                        if (this.approvestartdate === '' || this.approvestartdate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_startdate_limit = $scope.dateformat(this.approvestartdate);
                        }

                        if (this.approveenddate === '' || this.approveenddate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_enddate_limit = $scope.dateformat(this.approveenddate);
                        }
                    } else {
                        $scope.event_startdate_limit = '';
                        $scope.event_enddate_limit = '';
                    }


                } else if (type === 'P') { // FILTERS DATA OF PAST TAB
                    type = 'P';
                    $scope.payment_array_past = '';
                    $scope.Past_total_final = 0;
                    $scope.event_paymentdaystype = daystype;
                    $scope.paymentcategorytype = catetype;
                    if ($scope.event_paymentdaystype === '4') {
                        if (this.paststartdate === '' || this.paststartdate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_startdate_limit = $scope.dateformat(this.paststartdate);
                        }

                        if (this.pastenddate === '' || this.pastenddate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_enddate_limit = $scope.dateformat(this.pastenddate);
                        }
                    } else {
                        $scope.event_startdate_limit = '';
                        $scope.event_enddate_limit = '';
                    }

                }
//                else if (type === 'F') { //Filters data of Failed Tab//
//                    type = 'F';
//                    $scope.payment_array_failed = '';
//                    $scope.Failed_total_final = '';
//                    $scope.event_paymentdaystype = $scope.failedpaymentdays;
//                    $scope.paymentcategorytype = $scope.failedpaymentcategory;
//                    if ($scope.failedpaymentdays === '4') {
//                        if ($scope.failedstartdate === '' || $scope.failedstartdate === undefined) {
//                            $scope.event_startdate_limit = '';
//                            $scope.event_enddate_limit = '';
//                        } else {
//                            $scope.event_startdate_limit = $scope.dateformat($scope.failedstartdate);
//                        }
//
//                        if ($scope.failedenddate === '' || $scope.failedenddate === undefined) {
//                            $scope.event_startdate_limit = '';
//                            $scope.event_enddate_limit = '';
//                        } else {
//                            $scope.event_enddate_limit = $scope.dateformat($scope.failedenddate);
//                        }
//                    } else {
//                        $scope.event_startdate_limit = '';
//                        $scope.event_enddate_limit = '';
//                    }
//
//                }
                else if (type === 'U') { //Filters data of Upcoming Tab//
                    type = 'U';
                    $scope.payment_array_upcoming = '';
                    $scope.Upcoming_total_final = 0;
                    $scope.event_paymentdaystype = daystype;
                    $scope.paymentcategorytype = catetype;
                    if ($scope.event_paymentdaystype === '4') {
                        if (this.upcomingstartdate === '' || this.upcomingstartdate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_startdate_limit = $scope.dateformat(this.upcomingstartdate);
                        }

                        if (this.upcomingenddate === '' || this.upcomingenddate === undefined) {
                            $scope.event_startdate_limit = '';
                            $scope.event_enddate_limit = '';
                        } else {
                            $scope.event_enddate_limit = $scope.dateformat(this.upcomingenddate);
                        }
                    } else {
                        $scope.event_startdate_limit = '';
                        $scope.event_enddate_limit = '';
                    }

                }
                $scope.getAllPaymentDetailsByFilter(type,$scope.current_tab_id);
            };
            
            $scope.getAllPaymentDetailsByFilter = function(type,current_tab_id){
               
                $scope.new_list_flag = type;
                if ($scope.new_list_flag === 'A') {
                       var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                   $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetailsbyfilter',
                          xhrFields: {
                            withCredentials: true
                         },
                         dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                $scope.paytotal=json.paytotal;
                                $scope.paymanual=json.paymanual;
                                $scope.paycash = json.paycash;
                                $scope.paycheck = json.paycheck;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                        data: {
                            "payment_type": type,
                            "category_type": $scope.paymentcategorytype,
                            "payment_days_type": $scope.event_paymentdaystype,
                            "payment_startdate_limit": $scope.event_startdate_limit,
                            "payment_enddate_limit": $scope.event_enddate_limit,
                            "company_id": $localStorage.company_id,

                        },
                             error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'desc'])
                  
                    $scope.dtColumns = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('last_updt_dt').withTitle('Payment Date').withClass('col_a1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_a2').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }),
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_a3').renderWith(
                                function (data, type, full) {
                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'approved'" +');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_a4').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_a5'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_a6').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_a7'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_a8').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_a9').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('net_amount').withTitle('Net').withClass('col_a10').renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        DTColumnBuilder.newColumn('tax_value').withTitle('Tax Collected').withClass('col_a11').renderWith(function(data,type,full){
                            return  data;
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_a11'),
                        DTColumnBuilder.newColumn('card_credit_string').withTitle('Payment Method').withClass('col_a12').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.card_credit_string + '</span>';
                                }),
                    ];
//                   $scope.dtInstance = {};
                }else if($scope.new_list_flag === 'P') {
                     var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                    $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetailsbyfilter',
                          xhrFields: {
                            withCredentials: true
                         },
                         dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                $scope.paytotal=json.paytotal;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                        data: {
                                    "payment_type": type,
                                    "category_type": $scope.paymentcategorytype,
                                    "payment_days_type": $scope.event_paymentdaystype,
                                    "payment_startdate_limit": $scope.event_startdate_limit,
                                    "payment_enddate_limit": $scope.event_enddate_limit,
                                    "company_id": $localStorage.company_id,

                        },
                             error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
                        
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'desc'])
                  
                    $scope.dtColumns1 = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_p1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }), 
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_p2').renderWith(
                                function (data, type, full,meta) {
                                    return '<a class="buyername"   data-toggle="tooltip" title="' + data + '" ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'past'" +');">' + full.buyer_name + '</a>'; 
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_p3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_p4'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_p5').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_p6'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_p7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_p8').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_p9').notSortable(),
                        DTColumnBuilder.newColumn('credit_card_name').withTitle('Payment Method').withClass('col_p10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.credit_card_name + '</span>';
                                }),
                    ];
                    
                }else if($scope.new_list_flag === 'U'){
                    var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                    $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                             method: 'POST',
                          url: urlservice.url + 'getPaymentDetailsbyfilter',
                          xhrFields: {
                            withCredentials: true
                         },
                         dataSrc: function (json) {
                                $scope.paymentdetails_array = json.data;
                                $scope.paytotal=json.paytotal;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                        data: {
                            "payment_type": type,
                            "category_type": $scope.paymentcategorytype,
                             "payment_days_type": $scope.event_paymentdaystype,
                             "payment_startdate_limit": $scope.event_startdate_limit,
                             "payment_enddate_limit": $scope.event_enddate_limit,
                             "company_id": $localStorage.company_id,

                        },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            },
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [1, 'asc'])
                  
                    $scope.dtColumns2 = [
                      
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'push'" +');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('schedule_date').withTitle('Due Date').withClass('col_u1').renderWith(
                                function (data, type) {
                                    return $filter('date')(data,
                                            'MMM d, y'); // date filter
                                }), 
                        DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_u2').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="getPaymentHistory(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' +"'upcoming'" +');">' + full.buyer_name + '</a>';
                                }),
                        DTColumnBuilder.newColumn('Participant').withTitle('Participant').withClass('col_u3').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.Participant + '</span>';
                                }),
                        DTColumnBuilder.newColumn('buyer_phone').withTitle('Phone Number').withClass('col_u4'),
                        DTColumnBuilder.newColumn('buyer_email').withTitle('Email').withClass('col_u5').renderWith(
                                function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                     return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.buyer_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.buyer_email + '</a>';} else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.event_reg_id + '\' ,\'' + full.Category + '\' ,' + "'mail'" + ');">' + full.buyer_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.event_reg_id + '\',\'' + full.Category + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.buyer_email + '</a>';
                                }
                            }),
                        DTColumnBuilder.newColumn('Category').withTitle('Category').withClass('col_u6'),
                        DTColumnBuilder.newColumn('event_title').withTitle('Detail').withClass('col_u7').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.event_title + '</span>';
                                }),
                        DTColumnBuilder.newColumn('payment_amount').withTitle('Gross').withClass('col_u8').renderWith(function(data,type,full){
                            if(full.card_credit_string === "Cash Payment" || full.card_credit_string === "Check Payment" || full.card_credit_string === "Manual Credit") {
                            return  data;
                             } else {
                            return  $scope.wp_currency_symbol + '' + data;
                            }
                        }),
                        DTColumnBuilder.newColumn('view_status').withTitle('Status').withClass('col_u9').notSortable(),
                        DTColumnBuilder.newColumn('credit_card_name').withTitle('Payment Method').withClass('col_u10').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.credit_card_name + '</span>';
                                }),
                    ];
                }
                  $scope.clearDateFields();
                  $('#progress-full').hide();
            };

            $scope.clearfilter = function (type) { // CLEAR THE APPLIED FILTER
                if (type === 'A') {
                    this.approvestartdate = '';
                    this.approveenddate = '';
                    this.approvepaymentdays = '1';
                    this.approvepaymentcategory = 'A';
                    $scope.paymentdetails_list('A');
                    $scope.Approved_total_final = $scope.Manual_total_final = 0;
                } else if (type === 'P') {
                    this.paststartdate = '';
                    this.pastenddate = '';
                    this.pastpaymentdays = '5';
                    this.pastpaymentcategory = 'A';
                    $scope.Past_total_final = 0;
                    $scope.paymentdetails_list('P');
//                    $scope.filterByDate('P');
                }
//                else if (type === 'F') {
//                    $scope.failedpaymentdays = '1';
//                    $scope.failedstartdate = '';
//                    $scope.failedenddate = '';
//                    $scope.payment_array_failed = $scope.payment_array.Failed;
//                    $scope.Failed_total_final = $scope.total;
//                } 
                else if (type === 'U') {
                    this.upcomingpaymentdays = '1';
                    this.upcomingstartdate = '';
                    this.upcomingenddate = '';
                    this.upcomingpaymentcategory = 'A';
                    $scope.paymentdetails_list('U');
                    $scope.Upcoming_total_final = 0;
                }
            };

            //Date picker Id//
            $("#aeventstartdate").on("dp.change", function () {
                $scope.aeventstartdate = $("#aeventstartdate").val();
            });
            $("#aeventenddate").on("dp.change", function () {
                $scope.aeventenddate = $("#aeventenddate").val();
            });
            $("#peventstartdate").on("dp.change", function () {
                $scope.peventstartdate = $("#peventstartdate").val();
            });
            $("#peventenddate").on("dp.change", function () {
                $scope.peventenddate = $("#peventenddate").val();
            });
            $("#feventstartdate").on("dp.change", function () {
                $scope.feventstartdate = $("#feventstartdate").val();
            });
            $("#feventenddate").on("dp.change", function () {
                $scope.feventenddate = $("#feventenddate").val();
            });
            $("#ueventstartdate").on("dp.change", function () {
                $scope.ueventstartdate = $("#ueventstartdate").val();
            });
            $("#ueventenddate").on("dp.change", function () {
                $scope.ueventenddate = $("#ueventenddate").val();
            });

            $scope.getPaymentdate = function (dat) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    var dat = dat + ' 00:00:00';
                    var dda = dat.toString();
                    dda = dda.replace(/ /g, "T");
                    var time = new Date(dda);
                    var dateonly = time.toString();
                    var fulldateonly = dateonly.replace(/GMT.*/g, "");
                    var localdate = fulldateonly.slice(4, -9);
                    return localdate;
                } else {
                    var ddateonly = dat + 'T00:00:00+00:00';
                    var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                    var localdate = $filter('date')(newDate, 'MMM d, y');
                    return localdate;
                }
            };


            $scope.dateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            };

            $scope.resetForm = function () { // RESET FORM
                $scope.editPaymentMethod_reg_id = '';
                $scope.editPaymentMethod_category = '';
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.cardnumber = '';
                $scope.$parent.ccmonth = '';
                $scope.$parent.ccyear = '';
                $scope.cvv = '';
                $scope.$parent.country = '';
                $scope.postal_code = '';
                $scope.paymentform.$setPristine();
                $scope.paymentform.$setUntouched();

            };

            $scope.datetimestring = function (ddate) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari)
                {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var safaritimecheck = ddate.split(" ");
                        if (safaritimecheck[1] === "00:00:00") {
                            var ddateonly = safaritimecheck[0];
                            return ddateonly;
                        } else {
                            var ddateonly = safaritimecheck[0];
                            return ddateonly;
                        }
                    }
                } else
                {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var timecheck = ddate.split(" ");
                        if (timecheck[1] === "00:00:00") {
                            var ddateonly = timecheck[0];
                            return ddateonly;
                        } else {
                            var ddateonly = timecheck[0];
                            return ddateonly;

                        }

                    }
                }
            };

            $scope.getPaymentHistory = function (reg_id, category, current_tab) {
                console.log(reg_id);
//                $localStorage.buyerName = buyername;
//                $localStorage.participantName = participantname;
                $localStorage.manage_page_from = "";
                $localStorage.page_from = 'payment_'+current_tab;
                if(category === 'Events'){
                    $localStorage.currentEventregid = reg_id;
//                    $localStorage.currentOrderDetails = reg_details;
                    $location.path('/eventdetail');
                }else if(category === 'Membership'){
                    $localStorage.currentMembershipregid = reg_id;
                    $location.path('/membershipdetail');
                }else if(category === 'Trial'){
                    $localStorage.trialpagefrom = 'payment';
                    $localStorage.currentTrialregid = reg_id;
                    $location.path('/trialdetail');
                }else if(category === 'Retail'){
                    $localStorage.retailpagefrom = 'payment';
                    $localStorage.currentRetailorderid = parseInt(reg_id);
                    $location.path('/retailfulfillment');
                }else if(category === 'Miscellaneous'){
                    $localStorage.Miscellaneouspagefrom = 'Miscellaneous';
                    $localStorage.currentmiscorderid = parseInt(reg_id);
                    $location.path('/miscdetail');
                }
            };

            $scope.dayselection = function (value) {
                
                var daystype;
                var catetype;
                this.approvestartdate = this.approveenddate = this.paststartdate = this.pastenddate = this.upcomingstartdate = this.upcomingenddate = '';  
                
                if(value === 'A'){
                    daystype= this.approvepaymentdays;
                    catetype=this.approvepaymentcategory;
                }else if(value === 'P'){
                    daystype= this.pastpaymentdays;
                    catetype=this.pastpaymentcategory;
                }else{
                    daystype= this.upcomingpaymentdays;
                    catetype=this.upcomingpaymentcategory;
                }
                if (daystype === '1' || daystype === '2' || daystype === '3' || daystype === '5') {
                    $scope.clearDateFields();
                    $scope.filterByDate(value,daystype,catetype);
                } else {
                     $scope.clearDateFields();
                    // DATE RANGE SELECTION FILTER
                }
            };
            
            $scope.openCsvModal = function(tab){
                $scope.currentTab = tab;
                if(tab === 'A'){
                    $scope.paymentcategory = this.approvepaymentcategory;
                     $scope.paymentdays = this.approvepaymentdays;
                }else if(tab === 'P'){
                    $scope.paymentcategory = this.pastpaymentcategory;
                     $scope.paymentdays = this.pastpaymentdays;
                }else{
                     $scope.paymentcategory = this.upcomingpaymentcategory;
                     $scope.paymentdays = this.upcomingpaymentdays;
                }
                $("#grpcsvdownloadModal").modal('show');
                $("#grpcsvdownloadtitle").text('Message');
                $("#grpcsvdownloadcontent").text('Download payment selection as a .csv file?');
            };
            
            $scope.exportPaymentDetails = function(tab){
                function formatTodayDate() {
                    var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'exportPaymentDetails',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "payment_type":tab,
                        "category":$scope.paymentcategory,
                        "payment_days_type":$scope.paymentdays,
                        "payment_startdate_limit":$scope.event_startdate_limit,
                        "payment_enddate_limit":$scope.event_enddate_limit
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.msg);
                        return false;
                    } else {
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text("Payment Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "PaymentDetails_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                  
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $('#progress-full').hide();
                    $("#cusmessageModal").modal('show');
                    $("#cusmessagetitle").text('Message');
                    $("#cusmessagecontent").text(msg);
                    
                }).error(function () {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            
            $scope.catchAStartDate = function(){
                if($scope.approvestartdate !== ''){
                    $('#aeventenddate').datepicker('setStartDate', $scope.approvestartdate);
                }else{
                    $('#aeventenddate').datepicker('setStartDate', null);
                    $scope.approveenddate = '';
                }
            };
            
            $scope.catchAEndDate = function(){
                if($scope.approveenddate !== ''){
                    $('#aeventstartdate').datepicker('setEndDate', $scope.approveenddate );
                }else{
                    $('#aeventstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchPStartDate = function(){
                if($scope.paststartdate !== ''){
                    $('#peventenddate').datepicker('setStartDate', $scope.paststartdate);
                }else{
                    $('#peventenddate').datepicker('setStartDate', null);
                    $scope.pastenddate = '';
                }
            };
            
            $scope.catchPEndDate = function(){
                if($scope.pastenddate !== ''){
                    $('#peventstartdate').datepicker('setEndDate', $scope.pastenddate);
                }else{
                    $('#peventstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchUStartDate = function(){
                if($scope.upcomingstartdate !== ''){
                    $('#ueventenddate').datepicker('setStartDate', $scope.upcomingstartdate);
                }else{
                    $('#ueventenddate').datepicker('setStartDate', null);
                    $scope.upcomingenddate = '';
                }
            };
            
            $scope.catchUEndDate = function(){
                if($scope.upcomingenddate !== ''){
                    $('#ueventstartdate').datepicker('setEndDate', $scope.upcomingenddate);
                }else{
                    $('#ueventstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.clearDateFields = function(){
                $scope.approvestartdate = $scope.approveenddate = $scope.paststartdate = $scope.pastenddate = $scope.upcomingstartdate = $scope.upcomingenddate = '';     
                
                $('#aeventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#aeventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#peventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#peventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });

                $('#ueventstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '+1d',
                    ignoreReadonly: true
                });
                
                $('#ueventenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '+1d',
                    ignoreReadonly: true
                });
            };
            
               
            
            $scope.clearPushEmailData = function(){
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                $scope.paymentlistcontents = []; 
            };
            
            
            $scope.sendindividualpush = function (selectedMember, category, type) {  // INDIVIDUAL PUSH MESSAGE
//              console.log(category);
                $scope.message_type = type;               
                $scope.clearPushEmailData();                
                $scope.paymentlistcontents.push({'event_reg_id':selectedMember,'Category':category});
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#paypushmessagecontModal").modal('show');
                    $("#paypushmessageconttitle").text('Send Push Message');
                } else {
                    $("#payemailmessagecontModal").modal('show');
                    $("#payemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            
            $scope.sendGroupEmailPush = function (message_type) {
                $('#progress-full').show();
                var msg = '';
                var subject,unsubscribe_email = '';
                if (message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if (message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if($scope.message_type === 'subscribe'){
                    message_type = $scope.message_type;
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendPaymentIndividualPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_payment_list": $scope.paymentlistcontents,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "unsubscribe_email":unsubscribe_email

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {    
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $scope.clearPushEmailData();
                        $('#progress-full').hide();
                        $("#payemailmessagecontModal").modal('hide');
                        $("#payemailmessageconttitle").text('');
                        $("#paypushmessagecontModal").modal('hide');
                        $("#paypushmessageconttitle").text('');
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Invalid server response');
                    }                   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            

            if ($localStorage.paymentdshboard === 'Y') {
                $scope.listview_flag = 'P';
                $scope.paymentdate = '1';
                $scope.pastpaymentcategory = 'A';
                $scope.paststartdate = '';
                $scope.pastenddate = '';
                $scope.pastpaymentdays = '5';
//                $scope.paymentcategory= $scope.pastpaymentcategory;
                $scope.paymentoption = "1";
                $scope.payment_tab_view = true;
                $scope.payout_view = false;
                $scope.Approvedlistshow = false;
                $scope.PastDuelistshow = true;
                $scope.Failedlistshow = false;
                $scope.Upcominglistshow = false;
                $scope.paymentdetails_list('P');
                $scope.resettabbarclass();
                $('#li-past').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.paymentdshboard = 'N';
            }
            
            if ($localStorage.paymentdshboard === 'U') {
                $scope.listview_flag = 'U';
                $scope.paymentdate = '1';
                this.upcomingpaymentcategory = 'A';
                this.upcomingstartdate = '';
                this.upcomingenddate = '';
                this.upcomingpaymentdays = '1';
//                $scope.paymentcategory= $scope.pastpaymentcategory;
                $scope.paymentoption = "1";
                $scope.payment_tab_view = true;
                $scope.payout_view = false;
                $scope.Approvedlistshow = false;
                $scope.PastDuelistshow = false;
                $scope.Failedlistshow = false;
                $scope.Upcominglistshow = true;
                $scope.dayselection('U');
//                $scope.paymentdetails_list('U');
                $scope.resettabbarclass();
                $('#li-upcoming').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.paymentdshboard = 'N';

            }
//          location.path('/miscdetail');
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };
            
            // Muthulakshmi
            $scope.viewPayout = function(type){
                $scope.payment_tab_view = false;
                $scope.payout_view = true;
                $scope.stripepayouts_view = true;
                $scope.stripeaccount_view = false;
                $scope.resettabbarclass();
                $('#li-viewpayouts').addClass('active-event-list').removeClass('event-list-title');
                $('#li-payouts').addClass('payouts-active-tab');
                $('#li-account').removeClass('payouts-active-tab');
                $localStorage.stripe_onboard_status = "";
                $scope.clearStripeAccountData();
                $scope.getPayoutDetails(); // Payout dashboard
                if(type === 'personnel'){
                    $scope.stripeAccount();
                }
            };
            $scope.stripePayouts = function(){
                $scope.stripepayouts_view = true;
                $scope.stripeaccount_view = false;
                $('#li-payouts').addClass('payouts-active-tab');
                $('#li-account').removeClass('payouts-active-tab');
            };
            $scope.stripeAccount = function(){
                $scope.stripepayouts_view = false;
                $scope.stripeaccount_view = true;
                $('#li-payouts').removeClass('payouts-active-tab');
                $('#li-account').addClass('payouts-active-tab');
            };
            $scope.getPayoutDetails = function(){
                $('#stripe-progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getStripePayoutDetails',
                    params: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $('#stripe-progress-full').hide();
                        $scope.external_accounts = response.data.external_accounts;
                        $scope.stripe_payout_details = response.data.msg;
                        $scope.stripe_pending_balance = response.data.stripe_pending_balance;
                        $scope.connected_account_details = response.data.connected_account_detail.external_accounts.data; // default for currency 
                        $scope.stripecurrencycode = $scope.stripe_payout_details.stripe_currency_code;  // errortofix
                        $scope.payout_list = response.data.payout_list;
                        $scope.stripe_available_balance = $scope.stripe_payout_details.stripe_available_balance;
                        $scope.stripe_payout_time_period = $scope.stripe_payout_details.payout_time_period;
                        $scope.stripe_relationship_status = $scope.stripe_payout_details.relationship_status;
                        $scope.stripe_requirement_status = $scope.stripe_payout_details.requirement_status;
                        if($scope.stripe_payout_details.first_name || $scope.stripe_payout_details.last_name)
                            $scope.stripe_name = $scope.stripe_payout_details.first_name+'  '+$scope.stripe_payout_details.last_name;
                        else
                            $scope.stripe_name = '';
                        $scope.stripe_business_name = $scope.stripe_payout_details.stripe_business_name;
                    } else if (response.data.status === 'Expired') {
                        $('#stripe-progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Failed') {
                        $('#stripe-progress-full').hide();
                        if(response.data.msg === 'Create stripe account first'){
                            $scope.redirect_to_stripecreation = true; // Redirect page to stripe creation page
                            $("#stripeRedirection-Modal").modal('show');
                            $("#stripeRedirection-title").text('Message');
                        }else{
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                    }                 
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.redirectToStripecreation = function(){
                $localStorage.linkfrompage = 'payment';
                $('#stripeRedirection-Modal').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/stripecreation';
                })
            };
            // STRIPE TOKEN CALL
            $scope.getStripeElements = function () {
                // Create a Stripe client
                $scope.valid_stripe_card = false;

                // Create an instance of Elements
                var elements =  $scope.stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: 'rgb(74,74,74)',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                $scope.stripe_card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                $scope.stripe_card.mount('#stripe-card-element');

                // Handle real-time validation errors from the card Element.
                $scope.stripe_card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('stripe-card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                        $scope.valid_stripe_card = false;
                    } else {
                        displayError.textContent = '';
                        $scope.valid_stripe_card = true;
                    }
                });
            };
            
            $scope.openAccUpdateModal = function(){
                if($scope.redirect_to_stripecreation){
                    $("#stripeRedirection-Modal").modal('show');
                    $("#stripeRedirection-title").text('Message');
                }else{
                    if($scope.stripe_publish_key){
                        $scope.stripe = Stripe($scope.stripe_publish_key);
                    }
                    $scope.clearStripeAccountData();
                    $scope.getStripeElements();
                    $("#BankUpdateAccountModal").modal('show');
                }
            };
            
            $scope.createStripeCardtoken = function(){
                var extraDetails = {
                    currency: $scope.stripecurrencycode,    // errortofix
                };
                $scope.stripe.createToken($scope.stripe_card,extraDetails).then(function(result) {
                    if (result.error) {
                      // Inform the user if there was an error.
                      var errorElement = document.getElementById('stripe-card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                        // stripeTokenHandler(result.token);
                        $scope.stripecardtokendetails = result.token;
                        $scope.stripecardtoken = $scope.stripecardtokendetails.id;
                        $scope.updateStripePayoutMethodInDB('C',$scope.stripecardtoken);
                    }
                });
            };
            $scope.updateStripePayoutMethodInDB = function(type, tokenvalue){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateStripePayoutMethod',
                    data: {
                        "company_id": $localStorage.company_id,
                        "user_id":$localStorage.user_id,
                        "method_type": type, // Method type card or ACH
                        "card_token_value": (type === 'C' ? tokenvalue : ''), // card token value
                        "routing_no": $scope.stripe_ach_route_no,
                        "account_no": $scope.stripe_ach_acc_number,
                        "acc_holder_name": $scope.stripe_ach_holder_name,
                        "acc_holder_type": $scope.stripe_ach_acc_type === 'I' ? 'individual' : 'company',
//                        "default_account": ($scope.stripe_default_currency) ? 'Y' : 'N' // true or false
                },
                headers: {"Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#BankUpdateAccountModal").modal('hide');
                    $scope.external_accounts = response.data.external_accounts;
//                    $scope.stripe_payout_details = response.data.msg;
//                    $scope.stripecurrencycode = $scope.stripe_payout_details.stripe_currency_code;  // errortofix
//                    $("#cusmessageModal").modal('show');
//                    $("#cusmessagetitle").text('Message');
//                    $("#cusmessagecontent").text(response.data.msg);
                }else if (response.data.status === 'Failed') {
                    $('#progress-full').hide();
                    if(response.data.msg === 'Create stripe account first'){
                        $('#progress-full').hide();
                        if(response.data.msg === 'Create stripe account first'){
                            $scope.redirect_to_stripecreation = true; // Redirect page to stripe creation page
                            $("#stripeRedirection-Modal").modal('show');
                            $("#stripeRedirection-title").text('Message');
                        }else{
                            $("#pushmessageModal").modal('show');
                            $("#pushmessagetitle").text('Message');
                            $("#pushmessagecontent").text(response.data.msg);
                        }
                    }
                    $("#cusmessageModal").modal('show');
                    $("#cusmessagetitle").text('Message');
                    $("#cusmessagecontent").text(response.data.msg);
                }else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#cusmessageModal").modal('show');
                    $("#cusmessagetitle").text('Message');
                    $("#cusmessagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $('#progress-full').hide();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        $scope.clearStripeAccountData = function(){
            $scope.stripe_ach_route_no = '';
            $scope.stripe_ach_acc_number = '';
            $scope.stripe_ach_holder_name = '';
            $scope.stripe_ach_acc_type = '';
            $scope.valid_stripe_card = false;
            $scope.stripe_payment_method = ''; 
            $localStorage.stripe_payout_view = 'N';
            if ($scope.stripe_card) {
                $scope.stripe_card.clear();
            }
        };
        $scope.updateStripePersonalDetails = function(){
            $localStorage.stripe_onboard_status = "start";
            if (window.location.hostname === 'localhost') {
                $scope.window_hostname = window.location.protocol+'//'+window.location.hostname+'/nila/Mystudio_webapp';
            }else{
               $scope.window_hostname = window.location.protocol+'//'+window.location.hostname; 
            }
            if($scope.window_hostname){
                $window.open($scope.window_hostname+'/stripeUpdation.php?c_id='+$localStorage.company_id+'&type=stripeOnboarding' , '_blank');
            }
        };
         // GET stripe connected account update status
         
         if($localStorage.page_from === 'payment_approved'){
                $scope.Approvedlistview();

        }else if($localStorage.page_from === 'payment_past'){
            $scope.PastDuelistview();

        }else if($localStorage.page_from === 'payment_upcoming'){
            $scope.Upcominglistview();

        }else if($localStorage.stripe_payout_view === 'Y'){
            $scope.viewPayout();
        }else{
            $scope.paymentdetails_list('A');
            $scope.dtOptions
                 .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
            $localStorage.page_from = '';
        }
         
         
        var url = window.location.href;
        if (url.indexOf("=") > 0) {
            var status = url.substr(url.indexOf("=") + 1);
            $localStorage.stripe_onboard_status = status;            
            window.location = url.substring(0, url.indexOf('?'));     // remove stripe_status from 
        }
       
        $scope.$watch(function () {
            return $localStorage.stripe_onboard_status;
        }, function (newVal, oldVal) {
            if (newVal == 'success' && newVal != '' && newVal) {
                setTimeout(function ()
                {
                   $scope.viewPayout('personnel');
                }, 1000);
            }
        });
            
    } else {
        $location.path('/login');
    }

}
    module.exports = PaymentController;


