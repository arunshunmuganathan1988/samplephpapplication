
AddTrialController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', 'trialListDetails', '$filter', '$route', '$timeout','$rootScope'];
function AddTrialController ($scope, $location, $http, $localStorage, urlservice, trialListDetails, $filter, $route, $timeout,$rootScope) {

        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'trials';
            
            $localStorage.currenttrialpreviewtab = "trialdetails";
            
//        $scope.urlfmt =  /^\s*((ftp|http|https)\:\/\/)?([a-z\d\-]{1,63}\.)*[a-z\d\-]{1,255}\.[a-z]{2,6}\s*.*$/; 
            $scope.company_id = $localStorage.company_id;
            $scope.trialview = true;
            $scope.trialpaymentview = false;
            $scope.trialregistrationview = false;
            $scope.leadsourceview = false;
            $scope.trialwaiverview = false;
            $scope.programlengthperiod = 'D';
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            $scope.trialcroppedCategoryImage = null;
            //start
            $scope.trialcategoryname = undefined;
            $scope.trialprocessingfee = '2';
            $scope.trialprice = "0.00";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            $scope.trialdiscnt_type = 'V';
            $scope.trialcategoryname = "";
            $scope.trialsubcategoryname = "";
            $scope.trialcatregurl = "";
            $scope.trialcroppedCategoryImage = "";
            $scope.trialwebsiteurl = "";
            $scope.trialdesc = "";
            $localStorage.current_page_from = 'trial';
            $scope.showtrialregistrationview = false;
            $scope.showleadsourceview = false;
            $scope.disablediscount = true;
            $scope.programlengthquantity = "";
            $scope.afterpublish = true;
            $scope.regview = true;
            $scope.lsview = true;
            $scope.trialdiscountListView = true;
            $scope.trialdiscountView = true;
            $scope.disablewaivercall = true;
            $scope.trial_category_image_update = 'N';
            $scope.trialid = "";              
            $scope.trialwebsiteurl = "";
            $scope.category_picture = "";
            $scope.trialcroppedCategoryImage = "";
            $scope.cropper.sourceImage = "";
            $scope.trialdesc = "";
            $scope.programlengthquantity = "";
            $scope.trialdiscountList = "";
            $scope.trial_registration_columns = "";
            $scope.leadsourcetrial_registration_columns = "";
            $scope.trialselectedregcolindex = "";
            $('#progress-full').hide();
            
            $scope.opentabbartrial = function (viewname) {
                if (viewname === 'trial') {                    
                    $localStorage.currenttrialpreviewtab = "trialdetails";
                    $scope.trialview = true;
                    $scope.trialpaymentview = false;
                    $scope.trialregistrationview = false;
                    $scope.leadsourceview = false;
                    $scope.trialwaiverview = false;
                    $scope.resettabbarclass();
                    $('#trialtab1').addClass('done');
                    
                } else if (viewname === 'payment' && $scope.afterpublish == false) {
                    $localStorage.currenttrialpreviewtab = "trialpayment";
                    $scope.trialview = false;
                    $scope.trialpaymentview = true;
                    $scope.trialregistrationview = false;
                    $scope.leadsourceview = false;
                    $scope.trialwaiverview = false;
                    $scope.resettabbarclass();
                    $('#trialtab2').addClass('done');
                    
                } else if (viewname === 'registration' && $scope.afterpublish == false) {
                    $localStorage.currenttrialpreviewtab = "trialregistration";
                    $scope.trialview = false;
                    $scope.trialpaymentview = false;
                    $scope.trialregistrationview = true;
                    $scope.leadsourceview = false;
                    $scope.trialwaiverview = false;
                    $scope.resettabbarclass();
                    $('#trialtab3').addClass('done');

                } else if (viewname === 'leadsource' && $scope.afterpublish == false) {
                    $localStorage.currenttrialpreviewtab = "trialleadsource";
                    $scope.trialview = false;
                    $scope.trialpaymentview = false;
                    $scope.trialregistrationview = false;
                    $scope.leadsourceview = true;
                    $scope.trialwaiverview = false;
                    $scope.resettabbarclass();
                    $('#trialtab4').addClass('done');

                } else if (viewname === 'waiver' && $scope.afterpublish == false) {
                    $localStorage.currenttrialpreviewtab = "trialwaiver";
                    $scope.trialview = false;
                    $scope.trialpaymentview = false;
                    $scope.trialregistrationview = false;
                    $scope.leadsourceview = false;
                    $scope.trialwaiverview = true;
                    $scope.resettabbarclass();
                    $('#trialtab5').addClass('done');

                }
                else {
                    $scope.trialview = true;
                    $scope.trialpaymentview = false;
                    $scope.trialregistrationview = false;
                    $scope.leadsourceview = false;
                    $scope.trialwaiverview = false;
                    $scope.resettabbarclass();
                    $('#trialtab1').addClass('done');
                }
            };

            $scope.resettabbarclass = function (viewname) {
                $('.done').addClass('disabled').removeClass('done');
            };
            
            $scope.trialinfo = function(info){
                var msg;
                if(info === 'processingfee'){
                    msg = "These are merchant fees that are charged by credit card companies and the processing bank.";
                }
                if(info === 'discount'){
                    msg = "<p>Members can apply two discount codes when registering.<p><p>One discount code for Trial Fee and one discount code for Sign-Up Fee.</p>";
                }
                if(info === 'publiccaturl'){
                    msg = "<p>This URL is a web link to this trial category.</p>";
                }
                if(info === 'publicopturl'){
                    msg = "<p>This URL is a web link to this trial option.</p>";
                }
                if(info === 'leadsource'){
                    msg = "<p>(1) Click [ Add New Field ] to add additional source fields.<br>(2) Click & drag the field to re-arrange the order.<br>(3) Fields will be displayed in checkout drop down menu.</p>";
                }
                
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };

            $scope.getTrialTotalprocessingfee = function (total_amount) {
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var process_total_cost1 = parseFloat(total_amount) + (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var p_fee2 = (((parseFloat(process_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var process_total_cost = parseFloat(total_amount) + Math.round((+p_fee2 + +0.00001) * 100) / 100;
                return process_total_cost;
            };

            $scope.getTrialAbsorbFees = function (total_amount) {
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var p_fee = (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var process_total_cost = parseFloat(total_amount) - Math.round((+p_fee + +0.00001) * 100) / 100;
                return process_total_cost;
            };

            $scope.trialdiscounttype = function (discounttype) {
                if (discounttype === 'dollar') {
                    $scope.trialdiscnt_type = 'V';

                } else if (discounttype === 'percent') {
                    $scope.trialdiscnt_type = 'P';
                }
            };
            
            $scope.catchDiscountField = function (){
                if($scope.trialdiscountamount === undefined || $scope.trialdiscountcode === undefined || $scope.trialdiscountamount === "" || $scope.trialdiscountcode === ""){
                    $scope.disablediscount = true;
                }else{
                    $scope.disablediscount = false;
                }
            };
            
            $scope.catchTrialName = function (){
                if($scope.trialcategoryname === undefined || $scope.trialcategoryname === ""){
                    $localStorage.PreviewTrialName = '';
                }else{
                    $localStorage.PreviewTrialName = $scope.trialcategoryname;
                }
            };
            
            $scope.catchTrialSubTitle = function (){
                if($scope.trialsubcategoryname === undefined || $scope.trialsubcategoryname === ""){
                    $localStorage.PreviewTrialSubName = '';
                }else{
                    $localStorage.PreviewTrialSubName = $scope.trialsubcategoryname;
                }
            };            
            
            $scope.catchTrialdesc = function (){
                if($scope.trialdesc === undefined || $scope.trialdesc === ""){
                    $localStorage.PreviewTrialDesc = '';
                }else{
                    $localStorage.PreviewTrialDesc = $scope.trialdesc;
                }
                 $scope.catchTrialProcessingFee();
            };
            
            $scope.catchWaiverText = function (){
                if($scope.waiverpolicy === undefined || $scope.waiverpolicy === ""){
                    $scope.disablewaivercall = true;
                    $localStorage.PreviewTrialWaiver = '';
                }else{
                    $scope.disablewaivercall = false;
                    $localStorage.PreviewTrialWaiver = $scope.waiverpolicy;
                }
            };
            
            $scope.catchTrialPrice = function (){
                if($scope.trialprice === undefined || $scope.trialprice === ""){
                    $localStorage.PreviewTrialPrice = '';
                }else{
                    $localStorage.PreviewTrialPrice = $scope.trialprice;
                }
                $scope.catchTrialProcessingFee();
            };
            
            $scope.catchTrialProcessingFee = function (){
                if($scope.trialprocessingfee === undefined || $scope.trialprocessingfee === ""){
                    $localStorage.PreviewTrialProcessingFee = '2';
                }else{
                    $localStorage.PreviewTrialProcessingFee = $scope.trialprocessingfee;
                }
            };
            
            $scope.catchProgramdate = function (){
                if(!$scope.programlengthquantity === undefined || !$scope.programlengthperiod === "" ){
                    $localStorage.PreviewTrialprogramlengthquantity = '';
                    $localStorage.PreviewTrialprogramlengthperiod = '';
                }else{
                    $localStorage.PreviewTrialprogramlengthquantity = $scope.programlengthquantity;
                    $localStorage.PreviewTrialprogramlengthperiod = $scope.programlengthperiod;
                }
            };

            //Particular trial value from manage trial listing page for editingdeepak
            if ($localStorage.trialpagetype === 'edittrial') {
                $localStorage.currentpage = "edittrial";
                if ($localStorage.triallocaldata === undefined || $localStorage.triallocaldata === "") {
                    $localStorage.triallocaldata = trialListDetails.getTrialDetail();
                    $scope.trialdata = $localStorage.triallocaldata;
                    $localStorage.preview_trialcontent = $scope.trialdata[0];
                } else {
                    $scope.trialdata = $localStorage.triallocaldata;
                    $localStorage.preview_trialcontent = $scope.trialdata[0];
                }  
                $scope.afterpublish = false;
                $scope.trialid = $scope.trialdata[0].trial_id;                                
                $scope.trialcategoryname = $scope.trialdata[0].trial_title;
                $scope.trialsubcategoryname = $scope.trialdata[0].trial_subtitle;
                $scope.trialstatus = $scope.trialdata[0].trial_status;
                $scope.trialwebsiteurl = $scope.trialdata[0].trial_welcome_url;
                $scope.category_picture = $scope.trialdata[0].trial_banner_img_url;
                $scope.trialcroppedCategoryImage = $scope.trialdata[0].trial_banner_img_url;
                $scope.oldimage = $scope.trialdata[0].trial_banner_img_url;
                $scope.cropper.sourceImage = $scope.trialcroppedCategoryImage;
                $scope.trialdesc = $scope.trialdata[0].trial_desc;
                $scope.trialprice = $scope.trialdata[0].price_amount;
                $scope.programlengthquantity = $scope.trialdata[0].program_length;
                $scope.programlengthperiod = $scope.trialdata[0].program_length_type;
                $scope.trialprocessingfee = $scope.trialdata[0].processing_fee_type;
                $scope.trialdiscountList = $scope.trialdata[0].discount;
                $scope.trial_registration_columns = $scope.trialdata[0].reg_columns;
                $scope.leadsourcetrial_registration_columns = $scope.trialdata[0].lead_columns;
                $scope.trialselectedregcolindex = "";
                if($scope.trialdata[0].trial_waiver_policies === null) {
                    $scope.waiverpolicy = '';
                } else {
                    $scope.waiverpolicy = $scope.trialdata[0].trial_waiver_policies;
                }
                $scope.trialcatregurl = $scope.trialdata[0].trial_prog_url;
                $('#progress-full').hide();
            };
            
            if ($localStorage.trialpagetype === 'addtrial') {
                $localStorage.currentpage = "addtrial";
                $scope.addtrialdata = $localStorage.addtriallocaldata; 
                $localStorage.preview_trialcontent = $scope.addtrialdata;
                $scope.afterpublish = false;
                $scope.trialid = $scope.addtrialdata.trial_id;                                
                $scope.trialcategoryname = $scope.addtrialdata.trial_title;
                $scope.trialsubcategoryname = $scope.addtrialdata.trial_subtitle;
                $scope.trialstatus = $scope.addtrialdata.trial_status;
                $scope.trialwebsiteurl = $scope.addtrialdata.trial_welcome_url;
                $scope.category_picture = $scope.addtrialdata.trial_banner_img_url;
                $scope.trialcroppedCategoryImage = $scope.addtrialdata.trial_banner_img_url;
                $scope.cropper.sourceImage = $scope.trialcroppedCategoryImage;
                $scope.oldimage = $scope.addtrialdata.trial_banner_img_url;
                $scope.trialdesc = $scope.addtrialdata.trial_desc;
                $scope.trialprice = $scope.addtrialdata.price_amount;
                $scope.programlengthquantity = $scope.addtrialdata.program_length;
                $scope.programlengthperiod = $scope.addtrialdata.program_length_type;
                $scope.trialprocessingfee = $scope.addtrialdata.processing_fee_type;
                $scope.trialdiscountList = $scope.addtrialdata.discount.discount;
                $scope.trial_registration_columns = $scope.addtrialdata.reg_columns;
                $scope.leadsourcetrial_registration_columns = $scope.addtrialdata.lead_columns;
                $scope.regview = true;
                $scope.ls = true;
                $scope.trialselectedregcolindex = "";
                if($scope.addtrialdata.trial_waiver_policies === null) {
                    $scope.waiverpolicy = '';
                } else {
                    $scope.waiverpolicy = $scope.addtrialdata.trial_waiver_policies;
                }
                $scope.trialcatregurl = $scope.addtrialdata.trial_prog_url;
                $('#progress-full').hide();
            };
            
            $scope.trialimgchanged = function () {
                $("#trialimgcropmodal").modal('show');
                $scope.trial_category_image_update = 'N';
            };

            $scope.trialimgcropped = function () {
                $scope.trial_category_image_update = 'Y';
                $scope.trialcroppedCategoryImage = $scope.cropper.croppedImage;
                $localStorage.PreviewTrialImage = $scope.trialcroppedCategoryImage;
            };
            
            $scope.trialresetimgcropped = function () {               
                $scope.trial_category_image_update = 'Y';
                $scope.trialcroppedCategoryImage = null;
                $scope.trialcroppedCategoryImage = '';
            };
            
            $scope.trialimgcropcancel = function () {
                $scope.trial_category_image_update = 'N';
            };
            
            //value copied on clipboard message
            $scope.toastAddTrialmsg = function (trialtype) {   
                var id_name = "";
                if(trialtype === 'trialadd'){
                    id_name = "trialaddsnackbar";
                }
                var x = document.getElementById(id_name)
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            $scope.htmltoPlainTextTrial = function (event) {
                event.preventDefault();
                event.stopPropagation();
                var plaintext = event.originalEvent.clipboardData.getData('text/plain');
                return document.execCommand('inserttext', false, plaintext);
            };
            
            $scope.paymentnow = function(){
                  $("#sidetopbar").removeClass('modal-open');
                  $(".modal-backdrop").css("display", "none");
                  $location.path('/billingstripe');                
            };
            
            $scope.cancelTrialCategory = function () {
//                $localStorage.trialredirectto = "";
                $location.path('/managetrial');
            };
            
            $scope.deleteTrialCategory = function (trial_id) {
                $scope.delete_trial_category_id = trial_id;
                $("#trialCategoryDeleteModal").modal('show');
                $("#trialCategoryDeletetitle").text('Delete');
                $("#trialCategoryDeletecontent").text('Are you sure want to delete this trial?');
            }; 
            
            $scope.trialCategorydeleteConfirm = function () {                
                $("#trialCategoryDeleteModal").modal('hide');
                $('#progress-full').show();
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteTrialCategory',
                    data: {
                        "trial_id": $scope.delete_trial_category_id,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.triallocaldata = "";
                        $localStorage.addtriallocaldata = "";
                        $('#progress-full').hide();
                        $localStorage.trialcategorydeleteredirect = 'Y'; 
                        $timeout(function () {
                            $location.path('/managetrial');
                        }, 1000);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.showTrialRegistration = function (){
                $scope.showtrialregistrationview = true;
            };
            
            $scope.showTrialLeadsource = function (){
                $scope.showtrialleadsourceview = true;
            };
            
            $scope.cancelShowTrialRegistration = function (){
                $localStorage.PreviewNewTrialRegCancel = true;
                $localStorage.PreviewNewTrialRegFieldMandatory = false;
                $localStorage.PreviewNewTrialRegistrationField = '';
                $scope.showtrialregistrationview = false;
                $scope.trialaddnewfieldvalue = "";
                $scope.trial_checked = false;
            };
            
            $scope.cancelShowTrialLeadsource = function (){
                $scope.showtrialleadsourceview = false;
                $scope.trialaddnewfieldvaluels = "";
                $scope.trial_checkedls = false;
            };
            
            $scope.showLeadSource = function (){
                $scope.showleadsourceview = true;
            };
            
            $scope.cancelShowLeadSource = function (){
                $scope.showleadsourceview = false;
            };
            
            $scope.trialremoveDrag = function (event) {
                $(event.currentTarget).prop("draggable", false);
                $(event.currentTarget.closest('li')).prop("draggable", false);
            };

            $scope.trialaddDrag = function (event) {
                $(event.currentTarget).prop("draggable", true);
                $(event.currentTarget.closest('li')).prop("draggable", true);
            };
            //superstar
            $scope.catchTrialNewRegFieldText = function () {                
                $localStorage.PreviewNewTrialRegistrationField = $scope.trialaddnewfieldvalue;
            };
                  
            $scope.catchTrialNewRegFieldMandatory = function () {
                $localStorage.PreviewNewTrialRegFieldMandatory = $scope.trial_checked;
            };
            
            
            $scope.catchOldTrialRegFieldText = function () {
                $localStorage.PreviewOldTrialRegistrationField = this.trialregister_column_name;
            };
            
            $scope.catchOldTrialRegFieldMandatory = function () {
                $localStorage.PreviewOldTrialRegFieldMandatory = this.trialregister_mandatory_check;
            };
            
            $scope.edittrialregcolm = function (trial_registration_column, trial_mandatory_check, ind) {
                this.trialregister_column_name = trial_registration_column;
                this.mandatory_check = trial_mandatory_check;
                $scope.trialselectedregcolindex = ind;
                if (this.mandatory_check === 'Y') {
                    this.trialregister_mandatory_check = true;
                } else if (this.mandatory_check === 'N') {
                    this.trialregister_mandatory_check = false;
                }
                $scope.regview = false;
                $scope.editregview = false;
                
                $localStorage.preview_Trial_reg_fieldindex = ind;
                $localStorage.preview_Trial_reg_col_name_edit = trial_registration_column; 
                if(trial_mandatory_check === 'Y'){
                     $localStorage.PreviewOldTrialRegFieldMandatory = true;
                }else if(trial_mandatory_check === 'N'){
                    $localStorage.PreviewOldTrialRegFieldMandatory = false;
                }
                $localStorage.PreviewOldTrialRegCancel = false;
            };
            
            $scope.edittriallscolm = function (trial_ls_column, index) {
                this.trialls_column_name = trial_ls_column;
                $scope.trialselectedlscolindex = index;
                $scope.lsview = false;
                $scope.editlsview = false;
            };
            
            $scope.cancelTrialRegChange = function (trial_registration_column, trial_mandatory_check) {
                $localStorage.PreviewOldTrialRegCancel = true;
                $localStorage.preview_Trial_reg_fieldindex = '';
                $localStorage.preview_Trial_reg_col_name_edit = trial_registration_column;
                this.trialregister_column_name = trial_registration_column;
                this.mandatory_check = trial_mandatory_check;
                $scope.trialselectedregcolindex = "";
                if (this.mandatory_check === 'Y') {
                    this.trialregister_mandatory_check = true;
                } else if (this.mandatory_check === 'N') {
                    this.trialregister_mandatory_check = false;
                }
                $scope.regview = true;
                $scope.editregview = false;
                if(trial_mandatory_check === 'Y'){
                     $localStorage.PreviewOldTrialRegFieldMandatory = true;
                }else if(trial_mandatory_check === 'N'){
                    $localStorage.PreviewOldTrialRegFieldMandatory = false;
                }
                $localStorage.PreviewNewTrialRegCancel = true;
                $localStorage.PreviewNewTrialRegFieldMandatory = false;
                $localStorage.PreviewNewTrialRegistrationField = '';
            };
            
            $scope.cancelTriallsChange = function(trial_ls_column){
                this.trialls_column_name = trial_ls_column;
                $scope.trialselectedlscolindex = "";
                
                    $scope.lsview = true;
                    $scope.editlsview = false;
            };
            
            $scope.edittrialdiscounttype = function (discnttype) {
                if (discnttype === 'dollar') {
                    this.edit_discnt_type = 'V';

                } else if (discnttype === 'percent') {
                    this.edit_discnt_type = 'P';
                }
            } 
            
            $scope.trialdragregfieldCallback = function(drag_type,ind,reg_col_index) {
               if(drag_type === 'start'){
                   $scope.old_location_id = ind + 1;
                   $scope.old_location_col_index = reg_col_index;
               } else if(drag_type === 'end'){
                    setTimeout(function(){ 
                        $("li.dndPlaceholder").remove();
                    for (var i=0;i<$scope.trial_registration_columns.length;i++){ 
                       if($scope.trial_registration_columns[i].reg_col_index === $scope.old_location_col_index){
                           $scope.new_location_id = +i + 1;
                    }
                    }
                    if($scope.old_location_id !== $scope.new_location_id){
                        $scope.sortTrialRegister();
                    }
                }, 1000);          
            }
           }
           
           $scope.triallsdragregfieldCallback = function(drag_type,ind,lead_col_index) {
               if(drag_type === 'start'){
                   $scope.old_location_id = ind + 1;
                   $scope.old_location_col_index = lead_col_index;
               } else if(drag_type === 'end'){
                   $("li.dndPlaceholder").remove();
                    setTimeout(function(){                   
                    for (var i=0;i<$scope.leadsourcetrial_registration_columns.length;i++){ 
                       if($scope.leadsourcetrial_registration_columns[i].lead_col_index === $scope.old_location_col_index){
                           $scope.new_location_id = +i + 1;
                    }
                    }
                    if($scope.old_location_id !== $scope.new_location_id){
                        $scope.sortTrialLeadsource();
                    }
                }, 1000);          
            }
           }
           
            $scope.sortTrialRegister = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'TrialRegistrationSorting',
                    data: {
                        "old_location_id": $scope.old_location_id,
                        "new_location_id": $scope.new_location_id,
                        "trial_id": $scope.trialid,
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.registrationdetails = $scope.trialdetails.reg_columns;
                        $scope.trial_registration_columns = $scope.registrationdetails;
                        $localStorage.PreviewTrialRegcols = $scope.trial_registration_columns;
                        $scope.showtrialregistrationview = false;
                        $scope.regview = true;
                        $scope.editregview = false;
                        $scope.trialselectedregcolindex = "";
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Registration details has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };            
            
            $scope.sortTrialLeadsource = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'TrialLeadsourceSorting',
                    data: {
                        "old_location_id": $scope.old_location_id,
                        "new_location_id": $scope.new_location_id,
                        "trial_id": $scope.trialid,
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata =  response.data.msg;
                        $scope.leadsourcetrial_registration_columns = $scope.trialdetails.lead_columns;
                        $localStorage.PreviewTrialLeadSource = $scope.leadsourcetrial_registration_columns;
                        $scope.showtrialleadsourceview = false;
                        $scope.lsview = true;
                        $scope.editlsview = false;
                        $scope.trialselectedlscolindex = "";
                        $scope.trialaddnewfieldvaluels = "";
                        $scope.trial_checkedls = false;
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Registration details has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.addTrialDiscount = function () {
                $('#progress-full').show();

                if (parseFloat($scope.trialdiscountamount) <= 0) {
                    $scope.trialdiscountamount = '';
                    $("#trialfailuremessageModal").modal('show');
                    $("#trialfailuremessagetitle").text('Message');
                    $("#trialfailuremessagecontent").text("Discount amount should be greater than " + $scope.wp_currency_symbol + "0");
                    $('#progress-full').hide();
                    return;
                }

                if ($scope.trialdiscnt_type === 'V') {
                    var dollar_value = parseFloat($scope.trialprice) - parseFloat($scope.trialdiscountamount);
                    if (parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                        $scope.trialdiscnt_type = "V";
                        $scope.discount_code = $scope.trialdiscountcode;
                        $scope.discount_value = $scope.trialdiscountamount;
                    } else {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text("Trial Cost cannot be less than 5" + $scope.wp_currency_symbol + " when discount value is applied");
                        return false;
                    }

                } else if ($scope.trialdiscnt_type === 'P') {
                    var percent_value = parseFloat($scope.trialprice) - ((parseFloat($scope.trialprice) * parseFloat($scope.trialdiscountamount)) / parseFloat(100));
                    if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                        $scope.trialdiscnt_type = "P";
                        $scope.discount_code = $scope.trialdiscountcode;
                        $scope.discount_value = $scope.trialdiscountamount;
                    } else {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text("Trial Cost cannot be less than 5" + $scope.wp_currency_symbol + " when discount percentage is applied");
                        return false;
                    }

                }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'addtrialdiscount',
                    data: {
                        "trial_id" : $scope.trialid,
                        "discount_type": $scope.trialdiscnt_type,
                        "discount_code": $scope.trialdiscountcode,
                        "discount_amount": $scope.discount_value,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.discount_value = "";
                        $scope.trialdiscountcode = "";
                        $scope.trialdiscnt_type = 'V';
                        $scope.trialdiscountamount = "";
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        if (response.data.msg.discount.status === "Success") {
                            $scope.trialdiscountList = response.data.msg.discount.discount;
                            $scope.trialdiscountListView = true;
                            $scope.trialdiscountView = true;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                            $scope.disablediscount = true;
                        } else {
                            $scope.trialdiscountList = "";
                            $scope.trialdiscountListView = false;
                            $scope.trialdiscountView = false;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                        }
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Discount value Added successfully');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);;
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.deleteTrialDiscount = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deletetrialDiscount',
                    data: {
                        "trial_id": $scope.trialid,
                        "trial_discount_id": $scope.trialdeletediscountdiscnt_id,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Discount value deleted successfully');
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.trialdiscountList = response.data.msg.discount.discount;
                        if (response.data.msg.discount.status === "Success") {
                            $scope.trialdiscountListView = true;
                            $scope.trialdiscountView = true;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                        } else {
                            $scope.trialdiscountListView = false;
                            $scope.trialdiscountView = false;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                        }
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            $scope.updatetrialdiscount = function (discnt_id) {
                $('#progress-full').show();
                
                if(parseFloat(this.editdiscountamount) <= 0){
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text("Discount amount should be greater than "+$scope.wp_currency_symbol+"0");
                    $('#progress-full').hide();
                    return;
                }

                    if (this.edit_discnt_type === 'V') {
                         var dollar_value = parseFloat($scope.trialprice) - parseFloat(this.editdiscountamount);
                        if (parseFloat('5') < parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                $scope.discount_type = "V";
                                $scope.discount_code = this.editdiscountcode;
                                $scope.discount_value = this.editdiscountamount;
                                $scope.trial_discount_id = discnt_id;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Trial cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                return false;
                            }

                    } else if (this.edit_discnt_type === 'P') {
                         var percent_value = parseFloat($scope.trialprice) - ((parseFloat($scope.trialprice) * parseFloat(this.editdiscountamount)) / parseFloat(100));
                        if (parseFloat('5') < parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                $scope.discount_type = "P";
                                $scope.discount_code = this.editdiscountcode;
                                $scope.discount_value = this.editdiscountamount;
                                $scope.trial_discount_id = discnt_id;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Trial cost cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                return false;

                            }
                        }


                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatetrialdiscount',
                    data: {
                        "trial_id": $scope.trialid,
                        "trial_discount_id": $scope.trial_discount_id,
                        "discount_type": $scope.discount_type,
                        "discount_code": $scope.discount_code,
                        "discount_amount": $scope.discount_value,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.trialdiscountList = response.data.msg.discount.discount;
                        if (response.data.msg.discount.status === "Success") {
                            $scope.trialdiscountListView = true;
                            $scope.trialdiscountView = true;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                        } else {
                            $scope.trialdiscountListView = false;
                            $scope.trialdiscountView = false;
                            $scope.trialdiscountEditView = false;
                            $scope.trialselecteddiscountindex = "";
                        }
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Discount value updated successfully');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                   console.log(response.data);
                   $scope.handleError(response.data);
                });
            };

            $scope.addTrial = function (trial_status) {
                if ($scope.trialcroppedCategoryImage === "" || $scope.trialcroppedCategoryImage === undefined || $scope.trialcroppedCategoryImage === null) {
                    $scope.category_picture_type = "";
                    $scope.trial_category_picture = "";
                } else {
                    $scope.category_picture_type = "png";
                    $scope.trial_category_picture = $scope.trialcroppedCategoryImage.substr($scope.trialcroppedCategoryImage.indexOf(",") + 1);
                }
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'addTrialCategory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_category_title": $scope.trialcategoryname,
                        "trial_subcategory_title": $scope.trialsubcategoryname,
                        "trial_category_image": $scope.trial_category_picture,
                        "trial_banner_img_type": $scope.category_picture_type,
                        "trial_conversion_url": $scope.trialwebsiteurl,
                        "trial_category_desc": $scope.trialdesc,
                        "trial_category_type": trial_status
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.trialstatus = $scope.trialdetails.trial_status;
                        $scope.trialid = $scope.trialdetails.trial_id;
                        $scope.trialcatregurl = $scope.trialdetails.trial_prog_url;
                        $scope.trialcategoryname = $scope.trialdetails.trial_title;
                        $scope.trialsubcategoryname = $scope.trialdetails.trial_subtitle;
                        $scope.trialcroppedCategoryImage = $scope.trialdetails.trial_banner_img_url;
                        $scope.oldimage = $scope.trialdetails.trial_banner_img_url;
                        $scope.trialdesc = $scope.trialdetails.trial_desc;
                        $scope.leadsourcetrial_registration_columns = $scope.trialdetails.lead_columns;
                        $scope.registrationdetails = $scope.trialdetails.reg_columns;
                        $scope.trial_registration_columns = $scope.registrationdetails;
                        $scope.trialdiscountList = response.data.msg.discount.discount;
                        $scope.trial_category_image_update = 'N';                        
                        $localStorage.PreviewTrialRegcols = $scope.trial_registration_columns;
                        $localStorage.PreviewTrialLeadSource = $scope.leadsourcetrial_registration_columns;
                        if (response.data.msg.discount.status === "Success") {
                            $scope.trialdiscountListView = true;
                            $scope.trialdiscountView = true;
                            $scope.trialdiscountEditView = false;
                        } else {
                            $scope.trialdiscountListView = false;
                            $scope.trialdiscountView = false;
                            $scope.trialdiscountEditView = false;
                        }
                        if($scope.trialstatus === "P" || $scope.trialstatus === "S"){
                            $scope.afterpublish = false;
                        }
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial has been added.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.updateTrial = function (trial_status, trialid) {
                
                if ($scope.trialcategoryname === "" || $scope.trialcategoryname === undefined || $scope.trialcategoryname === null) {
                    $scope.categoryname = "";
                } else {
                    $scope.categoryname = $scope.trialcategoryname;
                }

                if ($scope.trialsubcategoryname === "" || $scope.trialsubcategoryname === undefined || $scope.trialsubcategoryname === null) {
                    $scope.categorysubname = "";
                } else {
                    $scope.categorysubname = $scope.trialsubcategoryname;
                }


                if ($scope.trialwebsiteurl === "" || $scope.trialwebsiteurl === undefined || $scope.trialwebsiteurl === null) {
                    $scope.conversionurl = "";
                } else {
                    if ($scope.trialwebsiteurl.indexOf("http://") === 0 || $scope.trialwebsiteurl.indexOf("https://") === 0) {
                        $scope.conversionurl = $scope.trialwebsiteurl;
                    } else {
                        $scope.conversionurl = 'http://' + $scope.trialwebsiteurl;
                    }
                }

                if ($scope.trialcroppedCategoryImage === "" || $scope.trialcroppedCategoryImage === undefined || $scope.trialcroppedCategoryImage === null) {
                    $scope.category_picture_type = "";
                    $scope.trial_category_picture = "";
                } else {
                    $scope.category_picture_type = "png";
                    $scope.trial_category_picture = $scope.trialcroppedCategoryImage.substr($scope.trialcroppedCategoryImage.indexOf(",") + 1);
                }

                $scope.categorydesc = document.getElementById("trialtextarea").innerHTML;
                    
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatetrialcategory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_category_title": $scope.categoryname,
                        "trial_subcategory_title": $scope.categorysubname,
                        "trial_category_image": $scope.trial_category_picture,
                        "old_trial_banner_img_url":$scope.oldimage,
                        "trial_banner_img_type": $scope.category_picture_type,
                        "trial_conversion_url": $scope.trialwebsiteurl,
                        "trial_category_desc": $scope.trialdesc,
                        "trial_image_update":$scope.trial_category_image_update,
                        "trial_category_type": trial_status,
                        "trial_id": trialid
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.trialstatus = $scope.trialdetails.trial_status;
                        $scope.trialid = $scope.trialdetails.trial_id;
                        $scope.trialcatregurl = $scope.trialdetails.trial_prog_url;
                        $scope.trialcategoryname = $scope.trialdetails.trial_title;
                        $scope.trialsubcategoryname = $scope.trialdetails.trial_subtitle;
                        $scope.trialcroppedCategoryImage = $scope.trialdetails.trial_banner_img_url;
                        $scope.trialdesc = $scope.trialdetails.trial_desc;
                        $scope.oldimage = $scope.trialdetails.trial_banner_img_url;
                        $scope.leadsourcetrial_registration_columns = $scope.trialdetails.lead_columns;
                        $scope.registrationdetails = $scope.trialdetails.reg_columns;
                        $scope.trial_registration_columns = $scope.registrationdetails;
                        $scope.trialdiscountList = response.data.msg.discount.discount;
                        $scope.trial_category_image_update = 'N';
                        if (response.data.msg.discount.status === "Success") {
                            $scope.trialdiscountListView = true;
                            $scope.trialdiscountView = true;
                            $scope.trialdiscountEditView = false;
                        } else {
                            $scope.trialdiscountListView = false;
                            $scope.trialdiscountView = false;
                            $scope.trialdiscountEditView = false;
                        }
                        if($scope.trialstatus === "P" || $scope.trialstatus === "S"){
                            $scope.afterpublish = false;
                        }
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };

            $scope.updateTrialPayment = function () {
                var trial_price = 0;
                if ($scope.trialprice === "" || $scope.trialprice === undefined || $scope.trialprice === null || (parseFloat($scope.trialprice) == 0)) {
                        trial_price = "0.00";
                } else if ($scope.trialdiscountList.length > 0 && $scope.trialdiscountList !== undefined && $scope.trialdiscountList !== '' && $scope.trialdiscountList !== null) {
                        for (var i = 0; i < $scope.trialdiscountList.length; i++) {
                                if ($scope.trialdiscountList[i].discount_type === 'V') {
                                    var check_dollar_discount = parseFloat($scope.trialprice) - parseFloat($scope.trialdiscountList[i].discount_amount);
                                    if (parseFloat('5') <= parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount)) {    
                                        trial_price = $scope.trialprice;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text('Trial amount must be at-least '+$scope.wp_currency_symbol+'5 when discount value is applied');
                                        return false;
                                    }
                                } else if ($scope.trialdiscountList[i].discount_type === 'P') {
                                    var check_percent_discount = parseFloat($scope.trialprice) - (((parseFloat($scope.trialprice) * parseFloat($scope.trialdiscountList[i].discount_amount)) / parseFloat(100)));
                                    if (parseFloat('5') <= parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                        trial_price = $scope.trialprice;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Trial amount must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                        return false;
                                    }
                                }else {
                                    if (parseFloat($scope.trialprice) >= 5) {
                                        trial_price = $scope.trialprice;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Trial amount must be at-least "+$scope.wp_currency_symbol+"5");
                                        return false;
                                    }
                                }
                        }

                } else {
                        if (parseFloat($scope.trialprice) >= 5) {
                            trial_price = $scope.trialprice;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Trial amount must be at-least "+$scope.wp_currency_symbol+"5");
                            return false;
                        }
                    }                
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialPaymentDetails',
                    data: {
                        "trial_id" : $scope.trialid,
                        "trial_price": parseFloat(trial_price),
                        "trial_programlengthquantity": $scope.programlengthquantity,
                        "trial_programlengthperiod": $scope.programlengthperiod,
                        "trial_processingfeetype": $scope.trialprocessingfee,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Payment has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.updateTrialRegistration  = function (type,index) {
                
                if(type === 'Add'){
                $scope.fieldcount = $scope.trial_registration_columns.length + 1;
                $scope.trialregisterfieldname = "trial_registration_column_" + $scope.fieldcount;
                $scope.trialregisterfieldmandatoryname = "trial_registration_column_" + $scope.fieldcount + "_flag";
                $scope.trialregisterfieldvalue = $scope.trialaddnewfieldvalue;
                if ($scope.trial_checked) {
                    $scope.trial_mandatory_flag = 'Y';
                } else {
                    $scope.trial_mandatory_flag = 'N';
                }
                }else if (type === 'update') {
                       if (this.trialregister_column_name !== "" ||this.trialregister_column_name !== undefined ) {
                        $scope.regfieldcount = index + 1;
                        $scope.trialregisterfieldname = "trial_registration_column_" + $scope.regfieldcount;
                        $scope.trialregisterfieldmandatoryname =  "trial_registration_column_"+$scope.regfieldcount+"_flag";
                        $scope.trialregisterfieldvalue = this.trialregister_column_name;
                        if (this.trialregister_mandatory_check) {
                            $scope.trial_mandatory_flag = 'Y';
                        } else {
                            $scope.trial_mandatory_flag = 'N';
                        }
                    } else {
                        $scope.trialregisterfieldname = "";
                        $scope.trialregisterfieldmandatoryname= 'N';
                        $scope.trialregisterfieldvalue = "";
                    }
                }
            
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialRegistration',
                    data: {
                        "trial_id" : $scope.trialid,
                        "company_id": $localStorage.company_id,
                        "trial_column_name" : $scope.trialregisterfieldname, 
                        "trial_column_value" : $scope.trialregisterfieldvalue,   
                        "trial_mandatory_column_name" : $scope.trialregisterfieldmandatoryname, 
                        "trial_mandatory_column_value" : $scope.trial_mandatory_flag
                        
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.registrationdetails = $scope.trialdetails.reg_columns;
                        $scope.trial_registration_columns = $scope.registrationdetails;
                        $localStorage.PreviewTrialRegcols = $scope.trial_registration_columns;
                        $scope.regview = true;
                        $scope.editregview = false;
                        $scope.trialselectedregcolindex = "";
                        $scope.cancelShowTrialRegistration();
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Registration details has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.updateTrialLeadsource  = function (type,index) {
                
                if(type === 'Add'){
                $scope.lsfieldcount = $scope.leadsourcetrial_registration_columns.length + 1;
                $scope.triallsfieldname = "trial_lead_source_" + $scope.lsfieldcount;
                $scope.triallsfieldvalue = $scope.trialaddnewfieldvaluels;
                }else if (type === 'update') {
                       if (this.trialls_column_name !== "" ||this.trialls_column_name !== undefined ) {
                        $scope.lsfieldcount = index + 1;
                        $scope.triallsfieldname = "trial_lead_source_" + $scope.lsfieldcount;
                        $scope.triallsfieldvalue = this.trialls_column_name;
                       
                    } else {
                        $scope.triallsfieldname = "";
                        $scope.triallsfieldvalue = "";
                    }
                }
            
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialLeadSource',
                    data: {
                        "trial_id" : $scope.trialid,
                        "company_id": $localStorage.company_id,
                        "trial_leadsource_name" : $scope.triallsfieldname, 
                        "trial_leadsource_value" : $scope.triallsfieldvalue   
                        
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.leadsourcetrial_registration_columns = $scope.trialdetails.lead_columns;
                        $localStorage.PreviewTrialLeadSource = $scope.leadsourcetrial_registration_columns;
                        $scope.showtrialleadsourceview = false;
                        $scope.lsview = true;
                        $scope.editlsview = false;
                        $scope.trialselectedlscolindex = "";
                        $scope.trialaddnewfieldvaluels = "";
                        $scope.trial_checkedls = false;
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Lead Source details has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.trialdeletecol = function (registration_index) {
                $scope.trial_registration_colunm_count = registration_index+1;
                $scope.trial_registration_colunm_del = "trial_registration_column_" + $scope.trial_registration_colunm_count;
                $("#trialregisterDeleteModal").modal('show');
                $("#trialregisterDeletetitle").text('Delete');
                $("#trialregisterDeletecontent").text("Are you sure want to delete field? ");
            };
            
            $scope.triallsdeletecol = function (ls_index) {
                $scope.trial_ls_colunm_count = ls_index+1;
                $scope.trial_ls_colunm_del = "trial_lead_source_" + $scope.trial_ls_colunm_count;
                $("#triallsDeleteModal").modal('show');
                $("#triallsDeletetitle").text('Delete');
                $("#triallsDeletecontent").text("Are you sure want to delete field? ");
            };
            
            $scope.deleteTrialdiscount = function (discnt_id) {
                $scope.trialdeletediscountdiscnt_id = discnt_id;
                $("#trialdiscountDeleteModal").modal('show');
                $("#trialdiscountDeletetitle").text('Delete');
                $("#trialdiscountDeletecontent").text("Are you sure want to delete discount detail? ");
            };
            
            $scope.edittrialdiscount = function (discount_details, ind) {
                $scope.trialdiscountView = false;
                $scope.trialdiscountEditView = false;
                $scope.trialselecteddiscountindex = ind;
                $scope.edit_discount_id = discount_details.trial_discount_id;
                $scope.editdiscountcode = discount_details.discount_code;
                $scope.edit_discount_off = discount_details.discount_off; 
                $scope.edit_discnt_type = discount_details.discount_type;
                $scope.editdiscountamount = discount_details.discount_amount;
            }
            
            $scope.canceledittrialdiscount = function (discount_details) {
                this.editdiscountcode = discount_details.discount_code;
                this.edit_discount_off = discount_details.discount_off;                
                this.edit_discnt_type = discount_details.discount_type;
                this.editdiscountamount = discount_details.discount_amount;
                $scope.trialdiscountView = true;
                $scope.trialdiscountEditView = false;
                $scope.trialselecteddiscountindex = "";
            };            
            
            $scope.trialregisterdeleteConfirm = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteTrialRegistration',
                    data: {
                        "trial_id" : $scope.trialid,
                        "company_id": $localStorage.company_id,
                        "trial_column_name" : $scope.trial_registration_colunm_del
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.registrationdetails = $scope.trialdetails.reg_columns;
                        $scope.trial_registration_columns = $scope.registrationdetails;
                        $localStorage.PreviewTrialRegcols = $scope.trial_registration_columns;
                        $scope.showtrialregistrationview = false;
                        $scope.regview = true;
                        $scope.editregview = false;
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Registration has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.triallsdeleteConfirm = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteTrialLeadSource',
                    data: {
                        "trial_id" : $scope.trialid,
                        "company_id": $localStorage.company_id,
                        "trial_leadsource_name" : $scope.trial_ls_colunm_del
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        $scope.leadsourcetrial_registration_columns = $scope.trialdetails.lead_columns;
                        $localStorage.PreviewTrialLeadSource = $scope.leadsourcetrial_registration_columns;
                        $scope.showtrialleadsourceview = false;
                        $scope.lsview = true;
                        $scope.editlsview = false;
                        $scope.trialaddnewfieldvaluels = "";
                        $scope.trial_checkedls = false;
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial Lead source has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.updateTrialWaiverDetails = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialWaiver',
                    data: {
                        "trial_id" : $scope.trialid,
                        "company_id": $localStorage.company_id,
                        "waiver_policies" : $scope.waiverpolicy
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.trialpagetype = 'addtrial';
                        $scope.trialdetails = $localStorage.addtriallocaldata = response.data.msg;
                        
                        $('#progress-full').hide();
                        $("#AddtrialmessageModal").modal('show');
                        $("#Addtrialmessagetitle").text('Success Message');
                        $("#Addtrialmessagecontent").text('Trial waiver policy has been updated.');
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialfailuremessageModal").modal('show');
                        $("#trialfailuremessagetitle").text('Message');
                        $("#trialfailuremessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };

        } else {
            $location.path('/login');
        }

    }

    module.exports = AddTrialController;

//Numeric digit validation
