StripeCreationController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter'];
 function StripeCreationController($scope, $location, $http, $route, $localStorage, urlservice, $filter) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
//            $scope.stripeemail = $localStorage.user_email_id;
//            $scope.stripephone = $localStorage.phone_number;
            $scope.stripeemail = '';
            $scope.emailstatus = 'S';
            $scope.stripeagreement = false;
            $localStorage.stripe_payout_view = 'N';
            
            $scope.stripeAccountRegClose = function(){
                if($localStorage.linkfrompage === 'managemembership'){
                    $location.path('/managemembership');
                }else if($localStorage.linkfrompage === 'manageevent'){
                    $location.path('/manageevent');
                }else if($localStorage.linkfrompage === 'managetrial'){
                    $location.path('/managetrial');
                }else if($localStorage.linkfrompage === 'manageretail'){
                    $location.path('/manageretail');
                }else if($localStorage.linkfrompage === 'addmembership'){
                    $location.path('/addmembership');
                }else if($localStorage.linkfrompage === 'addevent'){
                    $location.path('/addevent');
                }else if($localStorage.linkfrompage === 'addtrial'){
                    $location.path('/addtrial');
                }else if($localStorage.linkfrompage === 'home'){
                    $location.path('/home');
                }else if($localStorage.linkfrompage === 'customers'){
                    $location.path('/customers');
                }else if($localStorage.linkfrompage === 'payment'){
                    $localStorage.stripe_payout_view = 'Y';
                    $location.path('/payment');
                }
            };
            //get stripe email for this company
            $scope.getStripeEmail = function () {
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripeemail',
                    params: {
                        "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.stripeemail = response.data.msg;
                        $scope.emailstatus = response.data.email_from; // If studio email empty then user email
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getStripeEmail();
            //wepay account creation for enabling payment
            $scope.stripeAccountRegistration = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeGenerateUserToken',
                    data: {
                        company_id: $localStorage.company_id,
                        studio_code:$localStorage.studio_code,
                        email: $scope.stripeemail
                    },                            
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                   if (response.data.status === 'Success') {                          
                        $localStorage.preview_wepaystatus = $localStorage.preview_stripestatus = 'Y';
                        $('#progress-full').hide();
//                        $("#stripemessageModal").modal('show');
//                        $("#stripemessageTitle").text('Message');
//                        $("#stripemessageContent").text(response.data.msg); 
                        $scope.stripeAccountRegClose();
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $('#progress-full').hide();
                        $("#failuremessageModal").modal('show');
                        if(response.data.error){
                            $("#failuremessagetitle").text(response.data.error);
                            $("#failuremessagecontent").text(response.data.error_description);
                        }else{
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text(response.data.msg);
                        }
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };  
            $(".stripe-email-input").hover(
                function () {
                    $(".toggle-tooltip-stripe").css("visibility","visible");
                }, function () {
                    $(".toggle-tooltip-stripe").css("visibility","hidden");
                }
            );
            
        } else {
            $location.path('/login');
        }
    }
    module.exports = StripeCreationController;