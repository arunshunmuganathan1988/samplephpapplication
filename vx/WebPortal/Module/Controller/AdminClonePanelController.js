AdminClonePanelController.$inject=['$scope', '$http', '$location', '$route', '$localStorage', 'urlservice', '$rootScope'];
function AdminClonePanelController($scope, $http, $location, $route, $localStorage, urlservice, $rootScope) {
    if($localStorage.isadminlogin === 'Y') {
        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = true;
        $rootScope.hidesidebarlogin = false;
        tooltip.hide();
        $rootScope.activepagehighlight = 'adminclonepanel';
        $rootScope.admincmpnynme = $localStorage.admincmpnynme;
        $scope.selectedevent = $scope.selectedmembership = $scope.selectedtrial = $scope.selectedcurriculum = '1';
        $scope.admincompanyid = $localStorage.admincompanyid;
        $scope.fromlookup = $scope.tolookup = "";
        $scope.fromcompanyerrorshow = $scope.tocompanyerrorshow = false;       
        $scope.selectedeventlistcontents = $scope.selectedmemlistcontents = $scope.selectedtriallistcontents = $scope.selectedcurriculumlistcontents = [];
        $scope.cloneeventlist = $scope.clonemembershiplist = $scope.clonetriallist = $scope.clonecurriculumlist = [];       
        $scope.catchIndividualEvents = function(eventchoice){
            $scope.selectedeventlistcontents = [];
            angular.forEach(eventchoice, function (value, key) {
                if (eventchoice[key].checked) {
                    $scope.selectedeventlistcontents.push({event_id: eventchoice[key].event_id});
                }
            });
        };

        $scope.catchIndividualMembership = function(memchoice){
            $scope.selectedmemlistcontents = [];
            angular.forEach(memchoice, function (value, key) {
                if (memchoice[key].checked) {
                    $scope.selectedmemlistcontents.push({membership_id: memchoice[key].membership_id});
                }
            });
        };
        $scope.catchIndividualTrials = function(trialchoice){
            $scope.selectedtriallistcontents = [];
            angular.forEach(trialchoice, function (value, key) {
                if (trialchoice[key].checked) {
                    $scope.selectedtriallistcontents.push({trial_id: trialchoice[key].trial_id});
                }
            });
        };
        $scope.catchIndividualCurriculum = function(curriculumchoice){
            $scope.selectedcurriculumlistcontents = [];
            angular.forEach(curriculumchoice, function (value, key) {
                if (curriculumchoice[key].checked) {
                    $scope.selectedcurriculumlistcontents.push({curriculum_id: curriculumchoice[key].curriculum_id});
                }
            });
        };
        
        $scope.closeCloningPanel = function(){
            $scope.resetCloningForm();
            $location.path('/adminhome');
        };
        
        $scope.clonefromLookup = function (){
             $('#progress-full').show();
             $scope.fromcompanyerrorshow = false;
            $http({
                method: 'POST',
                url: urlservice.url + 'getFromAccountForClone',
                data: {
                     "email":$scope.fromlookup,
                     "company_id":$scope.admincompanyid
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $scope.clonefromcompanyname = response.data.company_name;
                    $scope.clonefromcompanycode = response.data.company_code;
                    $scope.clonefromcompanyId = response.data.company_id;
                    $scope.clonefrom_userid = response.data.user_id;
                    $scope.cloneeventlist = response.data.event;
                    $scope.clonemembershiplist = response.data.membership;
                    $scope.clonetriallist = response.data.trial;
                    $scope.clonecurriculumlist = response.data.curriculum;                    
                }else if(response.data.status === 'Expired'){
                    $scope.adminlogout();
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text(response.data.msg);
                }else if(response.data.status === 'Failed'){
                    $('#progress-full').hide();
                    $scope.fromcompanyerrorshow = true;
                    $scope.clonefromcompanyname = $scope.clonefromcompanyId = "";
                    $scope.clonefromcompanycode = $scope.clonefrom_userid = "";
                    $scope.cloneeventlist = $scope.clonemembershiplist = $scope.clonetriallist = $scope.clonecurriculumlist = [];
                    $scope.selectedevent = $scope.selectedmembership = $scope.selectedtrial = $scope.selectedcurriculum = '1';
                }else{
                    console.log(response.data);
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                    console.log(response.data);
                    $scope.handleAdminError(response.data);
            });
        };
        
        $scope.clonetoLookup = function (){
             $('#progress-full').show();
             $scope.tocompanyerrorshow = false;
            $http({
                method: 'POST',
                url: urlservice.url + 'getToAccountForClone',
                data: {
                     "email":$scope.tolookup,
                     "company_id":$scope.admincompanyid
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $scope.clonetocompanyname = response.data.company_name;
                    $scope.clonetocompanycode = response.data.company_code;
                    $scope.clonetocompanyId = response.data.company_id;
                    $scope.cloneto_userid = response.data.user_id;
                }else if(response.data.status === 'Expired'){
                    $scope.adminlogout();
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text(response.data.msg);  
                }else if(response.data.status === 'Failed'){
                    $('#progress-full').hide();
                    $scope.tocompanyerrorshow = true;
                    $scope.clonetocompanyname = $scope.clonetocompanycode = "";
                    $scope.clonetocompanyId = $scope.cloneto_userid = "";
                    $scope.selectedevent = $scope.selectedmembership = $scope.selectedtrial = $scope.selectedcurriculum = '1';
                }else{
                    console.log(response.data);
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                    console.log(response.data);
                    $scope.handleAdminError(response.data);
            });
        };  
        
        $scope.updateCloning = function (){
            $('#progress-full').show();
            var clone_error_message = 'Kindly select one of the ';
            $scope.error_msg = false;
            if(($scope.selectedevent === '3' || parseInt($scope.selectedevent) == 3) && $scope.selectedeventlistcontents.length == 0  && $scope.cloneeventlist.length > 0){  
                clone_error_message += "event, ";
                $scope.error_msg = true;
            }
            if(($scope.selectedmembership === '3'|| parseInt($scope.selectedmembership) == 3) && $scope.selectedmemlistcontents.length == 0 && $scope.clonemembershiplist.length > 0){
                clone_error_message += "membership, ";
                $scope.error_msg = true;
            }
            if(($scope.selectedtrial === '3' || parseInt($scope.selectedtrial) == 3) && $scope.selectedtriallistcontents.length == 0 && $scope.clonetriallist.length > 0){
                clone_error_message += "trial program, ";
                $scope.error_msg = true;
            }
            if(($scope.selectedcurriculum === '3' || parseInt($scope.selectedcurriculum) == 3)  && $scope.selectedcurriculumlistcontents.length == 0 && $scope.clonecurriculumlist.length > 0){
                clone_error_message += "curriculum, ";
                $scope.error_msg = true;
            }         
            
            if ($scope.error_msg) {
                $('#progress-full').hide();
                $("#cloningMessageModal").modal('show');
                $("#cloningmessagetitle").text('Message');
                $("#cloningmessagecontent").text(clone_error_message.slice(0, -2)+' in the list.');
                return false;
            }
            
            $http({
                method: 'POST',
                url: urlservice.url + 'getCloneDetailsFromUser',
                data: {
                    "company_id":$scope.admincompanyid,
                    "from_company_id":$scope.clonefromcompanyId,
                    "to_company_id":$scope.clonetocompanyId,
                    "from_user_id":$scope.clonefrom_userid ,
                    "to_user_id":$scope.cloneto_userid,
                    "event_details": parseInt($scope.selectedevent) < 3 ? "" : $scope.selectedeventlistcontents,
                    "membership_details":parseInt($scope.selectedmembership) < 3 ? "" : $scope.selectedmemlistcontents,
                    "trial_details":parseInt($scope.selectedtrial) < 3 ? "" : $scope.selectedtriallistcontents,
                    "curriculum_details":parseInt($scope.selectedcurriculum) < 3 ? "" : $scope.selectedcurriculumlistcontents,
                    "event_clone_flag":$scope.selectedevent,
                    "membership_clone_flag":$scope.selectedmembership,
                    "trial_clone_flag":$scope.selectedtrial,
                    "curriculum_clone_flag":$scope.selectedcurriculum,
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',},
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $scope.resetCloningForm();
                    $("#cloningSuccessMessageModal").modal('show');
                    $("#cloningSuccessmessagetitle").text('Message');
                    $("#cloningSuccessmessagecontent").text(response.data.msg);
                }else if(response.data.status === 'Expired'){
                    $scope.adminlogout();
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text(response.data.msg);   
                }else{
                    console.log(response.data);
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                    console.log(response.data);
                    $scope.handleAdminError(response.data);
            });
        };
        
        $scope.resetCloningForm = function (){
            $scope.clonefromcompanyname = $scope.clonefromcompanycode = $scope.clonefromcompanyId = $scope.clonefrom_userid = '';
            $scope.clonetocompanyname = $scope.clonetocompanycode = $scope.clonetocompanyId = $scope.cloneto_userid = '';
            $scope.cloneeventlist = $scope.clonemembershiplist = $scope.clonetriallist = $scope.clonecurriculumlist = [];
            $scope.fromlookup = $scope.tolookup = "";
            $scope.fromcompanyerrorshow = $scope.tocompanyerrorshow = false;
            $scope.error_msg = false;
            $scope.selectedevent = $scope.selectedmembership = $scope.selectedtrial = $scope.selectedcurriculum = '1';
            $scope.selectedeventlistcontents = $scope.selectedmemlistcontents = $scope.selectedtriallistcontents = $scope.selectedcurriculumlistcontents = [];
        };
        
    }else {
            $location.path('/adminhome');
    }
        
}
module.exports = AdminClonePanelController;