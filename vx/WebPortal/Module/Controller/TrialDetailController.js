/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
TrialDetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope'];
 function  TrialDetailController($scope, $location, $http, $localStorage, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();           
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'trials';
            tooltip.hide(); 
            $scope.trialdetailsview = true;
            $scope.trialparticipantinfoview = $scope.trialhistoryview = $scope.trialpaymenthistoryview = $scope.trialattendancedetailview = false;
            $scope.trialdetailsview = false;
            $scope.trialactionValue = $scope.trial_nxt_pay_date = '';
            $scope.trialparfirstname = $scope.trialparlastname = "";
            $scope.buyerfirstname = $scope.buyerlastname = "";
            $localStorage.detail_closetrialpagefrom = '';
            $scope.showTrialEditconfirm = $scope.showTrialParEdit = $scope.showTrialCusParEdit = $scope.trialeditStatus = false;
            $scope.trialleadeditStatus = $scope.trialstarteditStatus = $scope.trialEndeditStatus = false;
            $scope.currentTrial_reg_id = $localStorage.currentTrialregid;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.ccyearlist = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.applypaymentcredit_option = 'MC';
            $scope.stripe_card = $scope.stripe_publish_key = $scope.window_hostname = '';
            $scope.stripe_publish_key = $localStorage.stripe_publish_key;
            
            $scope.$on('$viewContentLoaded', function () {
                    $('#trialbirthstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });
            
            $scope.$on('$viewContentLoaded', function () {
                    $('#trial_nxt_pay_date').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true,
                    endDate: ('setEndDate', new Date()),
                });
            });

            $('#selected_trialstartdate').datepicker({//M
                format: 'yyyy-mm-dd',
                ignoreReadonly: true,
                autoclose: true,
            }).on("change", function() {
                $('#selected_trialenddate').datepicker('setStartDate', $scope.selected_trialstartdate);
            });

            $('#selected_trialenddate').datepicker({
                format: 'yyyy-mm-dd',
                ignoreReadonly: true,
                autoclose: true,
            }).on("change", function() {
                $('#selected_trialstartdate').datepicker('setEndDate', $scope.selected_trialenddate);;
            });
            
            $scope.Trial_Detail = function () {
                $('body').removeClass('modal-open');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'trialRegDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_reg_id": $scope.currentTrial_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.trial_detail = response.data.msg;
//                        $scope.trial_list = $scope.trial_detail.All;
                        $scope.trialstatus = $scope.trial_detail.trial_status;
                        $scope.student_id = $scope.trial_detail.student_id;
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        $scope.editTrial_reg_id = $scope.trial_detail.trial_reg_id;
                        $scope.editTrial_id = $scope.trial_detail.trial_id;
                        $scope.editTrialStatus = $scope.trial_detail.trial_status;
                        $scope.editTrialCategoryTitle = $scope.trial_detail.trial_category;
                        $scope.trial_leadsource_arr = $scope.trial_detail.reg_columns.lead_source;
                        $scope.selected_trialeditleadsource = $scope.trial_detail.leadsource;
                        $scope.trial_prolength_type = $scope.trial_detail.program_length_type;
                        $scope.trial_prolength_value = $scope.trial_detail.program_length;
                        $scope.trialname= $scope.trial_detail.buyer_name.split(" ");
                        $scope.firstname = $scope.trialname[0];
                        $scope.lastname = $scope.trialname[1];
                        $scope.email = $scope.trial_detail.buyer_email;
                    } else if (response.data.status === 'Expired') {
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.Trial_Participant_Info = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'trialParticipantInfoDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_reg_id": $scope.currentTrial_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.trial_participant_info = response.data.msg; 
                        
                        $scope.trialeditparticipant = angular.copy($scope.trial_participant_info);
                        $scope.trialparfirstname = $scope.trialeditparticipant.trial_registration_column_1;
                        $scope.trialparlastname = $scope.trialeditparticipant.trial_registration_column_2;
                        $scope.buyerfirstname = $scope.trialeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.trialeditparticipant.buyer_last_name;
                        $scope.trialparfirst_temp = $scope.trialparfirstname;
                        $scope.trialparlastname_temp = $scope.trialparlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        var trial_bchoose_date = $scope.trialeditparticipant.trial_registration_column_3.split("-");                   
                        $scope.trialbirthdate_curr = trial_bchoose_date[1] + "/" + trial_bchoose_date[2] + "/" + trial_bchoose_date[0];
                        $scope.trial_participant_birthdate_temp = $scope.trialbirthdate_curr;
                        $scope.trial_participant_phone_temp = $scope.trialeditparticipant.buyer_phone;
                        $scope.trial_participant_email_temp = $scope.trialeditparticipant.buyer_email;
                        $scope.trial_participant_street_temp = $scope.trialeditparticipant.trial_participant_street;
                        $scope.trial_participant_city_temp = $scope.trialeditparticipant.trial_participant_city;
                        $scope.trial_participant_state_temp = $scope.trialeditparticipant.trial_participant_state;
                        $scope.trial_participant_country_temp = $scope.trialeditparticipant.trial_participant_country;
                        $scope.trial_participant_zip_temp = $scope.trialeditparticipant.trial_participant_zip;
                        $scope.trial_participant_col_4_temp = $scope.trialeditparticipant.trial_registration_column_4;
                        $scope.trial_participant_col_5_temp = $scope.trialeditparticipant.trial_registration_column_5;
                        $scope.trial_participant_col_6_temp = $scope.trialeditparticipant.trial_registration_column_6;
                        $scope.trial_participant_col_7_temp = $scope.trialeditparticipant.trial_registration_column_7;
                        $scope.trial_participant_col_8_temp = $scope.trialeditparticipant.trial_registration_column_8;
                        $scope.trial_participant_col_9_temp = $scope.trialeditparticipant.trial_registration_column_9;
                        $scope.trial_participant_col_10_temp = $scope.trialeditparticipant.trial_registration_column_10;
                        
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.Trial_Payment_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'paymentHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id" : $scope.currentTrial_reg_id,
                        "category": 'trial'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.trial_payment_history = response.data.msg.history; 
                        for (i=0;i<(response.data.msg.upcoming).length;i++){
                            $scope.trial_payment_history.push(response.data.msg.upcoming[i]);
                        }
                        $scope.trial_detail = response.data.reg_details.msg;
//                        $scope.trial_category_list = $scope.trial_detail.All;
                        
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        if($scope.trial_payment_history.length > 0){
                            $scope.editTrial_reg_id = $scope.trial_payment_history[0].reg_id;
                            $scope.editTrial_id = $scope.trial_payment_history[0].trial_id;
                            $scope.editTrialStatus = $scope.trial_payment_history[0].trial_status;
                        }                                               
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.Trial_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'trialHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_reg_id" : $scope.currentTrial_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.trial_history = response.data.msg;
                    }  else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
//            MMM
            $scope.Trial_Attendance_detail = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getAttendanceDetail',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id" : $scope.currentTrial_reg_id,
                        "category" : "T" //Trial
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                    if (response.data.status === 'Success') {
                        $scope.trial_attendance_history = response.data.msg.attendance_history;
                        $scope.trial_attendance_history_array = angular.copy($scope.trial_attendance_history);
                        $scope.trial_attendance_days_count = response.data.days_count;
                        $scope.trial_active_dates = $scope.trial_checkedin_dates = [];
                        $scope.trial_active_dates = response.data.msg.checkin_dates;
                        $("#trialselectedDateCalender").datepicker('remove');
                        if($scope.trial_active_dates.length > 0){
                            for(i=0;i<$scope.trial_active_dates.length;i++){
                                $scope.trial_checkedin_dates.push($scope.trial_active_dates[i]);
                            }
                        }else{
                            $scope.trial_active_dates = $scope.trial_checkedin_dates = [];
                        }
                        var trial_active_dates = $scope.trial_checkedin_dates;
                        $scope.trial_calender_attendancedate = '';
                        $("#trialselectedDateCalender").datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true,                           
                            beforeShowDay: function(date){
                                 var d = date;
                                 var curr_date = d.getDate();
                                 var curr_month = d.getMonth() + 1; //Months are zero based
                                 var curr_year = d.getFullYear();
                                 var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
                                    if ($.inArray(formattedDate, trial_active_dates) !== -1){
                                       return {
                                          classes: 'activeDates'
                                       };
                                   }
                                  return;
                            }
                        }).on("changeDate", function (e) {
                            var selected_date = new Date(e.dates);
                            var currentDate = ('0' + (selected_date.getMonth() + 1)).slice(-2) + '/' + ('0' + selected_date.getDate()).slice(-2) + '/' + selected_date.getFullYear();
                            $scope.trial_calender_attendancedate = $scope.trialattendancedate = currentDate;
                        });
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
          
            $scope.ShowNewEditTrialDetails = function () {
                $scope.showTrialEditconfirm = true;
                $scope.selected_trialstartdate = $scope.trial_detail.start_date;
                $scope.selected_trialenddate = $scope.trial_detail.end_date;
                $('#selected_trialenddate').datepicker('setStartDate', $scope.selected_trialstartdate);
            };
            
            $scope.performTrialAction = function (a) { //Show list of Actions 
             
                if (a === 'A') {//Active
                    $scope.trialactionValue = 0;
                    $("#trialStatusModal").modal('show');
                    $("#trialStatustitle").text('Update Trial Status');
                    $("#trialStatuscontent").text('Are you sure you want to change the status to Active');
                }else if (a === 'E') {//Enrolled
                    $scope.trialactionValue = 1;
                    $("#trialStatusEnrolledModal").modal('show');
                    $("#trialStatusEnrolledtitle").text('Update Trial Status');
                } else if (a === 'C') {//cancel
                    $scope.trialactionValue = 2;
                    $("#trialStatusModal").modal('show');
                    $("#trialStatustitle").text('Update Trial Status');
                    $("#trialStatuscontent").text('Are you sure you want to change the status to Cancel');
                } else if (a === 'D') {//Did not start
                    $scope.trialactionValue = 3;
                    $("#trialStatusModal").modal('show');
                    $("#trialStatustitle").text('Update Trial Status');
                    $("#trialStatuscontent").text('Are you sure you want to change the status to Did not start');
                } 
                          
            };
            
            $scope.showTrialLeadEdit = function () {
                $scope.trialleadeditStatus = true;
            };
            
            $scope.showTrialLeadEditCancel = function () {
                $scope.trialleadeditStatus = false;
           };
            $scope.showTrialStartdateEdit = function () {
                $scope.trialstarteditStatus = true;
            };
            
            $scope.showTrialStartdateCancel = function () {
                $scope.trialstarteditStatus = false;
            };
            
            $scope.showTrialEnddateEdit = function () {
                $scope.trialEndeditStatus = true;
            };
            
            $scope.showTrialEnddateCancel = function () {
                $scope.trialEndeditStatus = false;
            };
            
            $scope.showTrialStatusEdit = function () {
                $scope.trialeditStatus = true;
                $scope.trialstarteditStatus = false;
                $scope.trialleadeditStatus = false;
                $scope.trialEndeditStatus = false;
            };
            
            $scope.showTrialStatusEditCancel = function (exist_status) {
                $scope.trialstatus = exist_status;
                $scope.trialeditStatus = false;
             };
            
            $scope.actionTrialConfirm = function (value) {//work
                if (value === 0) {
                      $scope.updateTrialStatus('A');
                }
                if (value === 1) {
                      $scope.updateTrialStatus('E');
                }
                if (value === 2) {
                     $scope.updateTrialStatus('C');
                }
                if (value === 3) {
                     $scope.updateTrialStatus('D');
                }
            };
            
            
            $scope.updateTrialStatus = function (status) { 
                $('#progress-full').show();
                if(status === 'E'){
                $("#trialStatusEnrolledModal").modal('hide');
                var d = new Date($scope.trial_nxt_pay_date);
                $scope.trial_nxt_pay_date = d.getFullYear() + '-' + ('0' + '-' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) ;
                }else{
                  $("#trialStatusModal").modal('hide');
                  var date = new Date();
                  $scope.trial_nxt_pay_date = date.getFullYear() + '-' + ('0' + '-' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) ;  
                }  
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialParticipantStatus',
                    data: {
                        "company_id": $localStorage.company_id,
                        "student_id":$scope.student_id,
                        "trial_id": $scope.editTrial_id,
                        "trial_reg_id": $scope.editTrial_reg_id,
                        "trial_status": status,
                        "next_date": $scope.trial_nxt_pay_date
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.resettabbarclass();
                        $('#li-trialdetails').addClass('active-event-list').removeClass('event-list-title'); 
                        $('.trialdetail-tabtext-1').addClass('greentab-text-active');
                        $scope.trial_detail = response.data.reg_details.msg;
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        $scope.editTrial_reg_id = $scope.trial_detail.trial_reg_id;
                        $scope.editTrial_id = $scope.trial_detail.trial_id;
                        $scope.editTrialStatus = $scope.trial_detail.trial_status;
                        $scope.showTrialStatusEditCancel($scope.editTrialStatus);
                        $scope.trial_nxt_pay_date = '';
                   
                }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                }else{
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
//            $scope.catcheditstartdate = function(){
//                var dupdate = $scope.trialsafariiosdate($scope.selected_trialstartdate);
//                var trialenddatequantity, trial_end;
//                if($scope.trial_prolength_type === 'W'){
//                    trialenddatequantity = $scope.trial_prolength_value * 7;   
//                    dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
//                    trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
//                }else if($scope.trial_prolength_type === 'D'){
//                    trialenddatequantity = $scope.trial_prolength_value;
//                    dupdate.setDate(parseInt(dupdate.getDate()) + parseInt(trialenddatequantity));
//                    trial_end = dupdate.getFullYear() + '-' + ('0' + (dupdate.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate.getDate()).slice(-2);
//                }
//                $scope.trial_detail.end_date = trial_end;
//                $scope.selected_trialenddate = trial_end;
//            };    
//            
//            $scope.catcheditenddate = function (){
//                var dupdate1 = $scope.trialsafariiosdate($scope.selected_trialenddate);
//                var trialstartdatequantity, trial_start;
//                if($scope.trial_prolength_type === 'W'){
//                    trialstartdatequantity = $scope.trial_prolength_value * 7;   
//                    dupdate1.setDate(parseInt(dupdate1.getDate()) - parseInt(trialstartdatequantity));
//                    trial_start = dupdate1.getFullYear() + '-' + ('0' + (dupdate1.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate1.getDate()).slice(-2);
//                }else if($scope.trial_prolength_type === 'D'){
//                    trialstartdatequantity = $scope.trial_prolength_value;
//                    dupdate1.setDate(parseInt(dupdate1.getDate()) - parseInt(trialstartdatequantity));
//                    trial_start = dupdate1.getFullYear() + '-' + ('0' + (dupdate1.getMonth() + 1)).slice(-2) + '-' + ('0' + dupdate1.getDate()).slice(-2);
//                }
//                $scope.trial_detail.start_date = trial_start;
//                $scope.selected_trialstartdate = trial_start;
//            };
            
            $scope.UpdateTrialDetail = function (){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editTrialDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "student_id":$scope.student_id,
                        "trial_id": $scope.editTrial_id,
                        "trial_reg_id": $scope.editTrial_reg_id,
                        "trial_lead_source": $scope.selected_trialeditleadsource,
                        "trial_start_date": $scope.selected_trialstartdate,
                        "trial_end_date": $scope.selected_trialenddate
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.trial_detail = response.data.reg_details.msg;
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        $scope.editTrial_reg_id = $scope.trial_detail.trial_reg_id;
                        $scope.editTrial_id = $scope.trial_detail.trial_id;
                        $scope.trialstarteditStatus = $scope.trialEndeditStatus = $scope.trialleadeditStatus = false;
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                }else{
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.cancelTrialDetailUpdate = function (a) { //Show list of Actions 
               $scope.showTrialEditconfirm = false;
            };
            
            $scope.showTrialParticipantEdit = function () {
                $scope.resetTrialEditParticipant();
                $scope.showTrialParEdit = true;
                $scope.showTrialCusParEdit = false;
            };
            
            $scope.showTrialCustomParticipantEdit = function () {
                $scope.resetTrialEditParticipant();
                $scope.showTrialParEdit = false;
                $scope.showTrialCusParEdit = true;
            }
            
            $scope.resetTrialEditParticipant = function () {
                $scope.showTrialParEdit = $scope.showTrialCusParEdit = false;
                $scope.trialparfirstname = $scope.trialparfirst_temp;
                $scope.trialparlastname = $scope.trialparlastname_temp;
                $scope.buyerfirstname = $scope.buyerfirst_temp;
                $scope.buyerlastname = $scope.buyerlastname_temp;
                $scope.trialbirthdate_curr = $scope.trial_participant_birthdate_temp;
                $scope.trialeditparticipant.buyer_phone = $scope.trial_participant_phone_temp;
                $scope.trialeditparticipant.buyer_email = $scope.trial_participant_email_temp;
                $scope.trialeditparticipant.trial_participant_street = $scope.trial_participant_street_temp;
                $scope.trialeditparticipant.trial_participant_city = $scope.trial_participant_city_temp;
                $scope.trialeditparticipant.trial_participant_state = $scope.trial_participant_state_temp;
                $scope.trialeditparticipant.trial_participant_country = $scope.trial_participant_country_temp;
                $scope.trialeditparticipant.trial_participant_zip = $scope.trial_participant_zip_temp;
                $scope.trialeditparticipant.trial_registration_column_4 = $scope.trial_participant_col_4_temp;
                $scope.trialeditparticipant.trial_registration_column_5 = $scope.trial_participant_col_5_temp;
                $scope.trialeditparticipant.trial_registration_column_6 = $scope.trial_participant_col_6_temp;
                $scope.trialeditparticipant.trial_registration_column_7 = $scope.trial_participant_col_7_temp;
                $scope.trialeditparticipant.trial_registration_column_8 = $scope.trial_participant_col_8_temp;
                $scope.trialeditparticipant.trial_registration_column_9 = $scope.trial_participant_col_9_temp;
                $scope.trialeditparticipant.trial_registration_column_10 = $scope.trial_participant_col_10_temp;
            };
            
            // UPDATE PARTICIPANT NAME ON THE TABLE LISTING
            $scope.updateTrialParticipantName = function (fname, lname, bfname, blname, birthdate, phone, email, street, city, state, country, zip, value5, value6, value7, value8, value9, value10) {
                $('#progress-full').show();
                
                var value_birth_date = birthdate.split("/");
                $scope.trial_birth_day = value_birth_date[2] + "-" + value_birth_date[0] + "-" + value_birth_date[1];
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateTrialParticipantName',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_id": $scope.editTrial_id,
                        "reg_id": $scope.currentTrial_reg_id,
                        "trial_status": $scope.editTrialStatus,
                        "pfirst_name": fname,
                        "plast_name": lname,
                        "bfirst_name": bfname,
                        "blast_name": blname,
                        "date_of_birth": $scope.trial_birth_day,
                        "buyer_phone": phone,
                        "buyer_email": email,
                        "trial_participant_street": street,
                        "trial_participant_city": city,
                        "trial_participant_state": state,
                        "trial_participant_zip": zip,
                        "trial_participant_country": country,
                        "trial_field_5": value5,
                        "trial_field_6": value6,
                        "trial_field_7": value7,
                        "trial_field_8": value8,
                        "trial_field_9": value9,
                        "trial_field_10": value10
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                   
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.showTrialParEdit = $scope.showTrialCusParEdit = false;
                        
                        $scope.trial_participant_info = response.data.participant_details.msg; 
                        
                        $scope.trialeditparticipant = angular.copy($scope.trial_participant_info);
                        $scope.trialparfirstname = $scope.trialeditparticipant.trial_registration_column_1;
                        $scope.trialparlastname = $scope.trialeditparticipant.trial_registration_column_2;
                        $scope.buyerfirstname = $scope.trialeditparticipant.buyer_first_name;
                        $scope.buyerlastname = $scope.trialeditparticipant.buyer_last_name;
                        $scope.trialparfirst_temp = $scope.trialparfirstname;
                        $scope.trialparlastname_temp = $scope.trialparlastname;
                        $scope.buyerfirst_temp = $scope.buyerfirstname;
                        $scope.buyerlastname_temp = $scope.buyerlastname;
                        var trial_bchoose_date = $scope.trialeditparticipant.trial_registration_column_3.split("-");                   
                        $scope.trialbirthdate_curr = trial_bchoose_date[1] + "/" + trial_bchoose_date[2] + "/" + trial_bchoose_date[0];
                        $scope.trial_participant_birthdate_temp = $scope.trialbirthdate_curr;
                        $scope.trial_participant_phone_temp = $scope.trialeditparticipant.buyer_phone;
                        $scope.trial_participant_email_temp = $scope.trialeditparticipant.buyer_email;
                        $scope.trial_participant_street_temp = $scope.trialeditparticipant.trial_participant_street;
                        $scope.trial_participant_city_temp = $scope.trialeditparticipant.trial_participant_city;
                        $scope.trial_participant_state_temp = $scope.trialeditparticipant.trial_participant_state;
                        $scope.trial_participant_country_temp = $scope.trialeditparticipant.trial_participant_country;
                        $scope.trial_participant_zip_temp = $scope.trialeditparticipant.trial_participant_zip;
                        $scope.trial_participant_col_5_temp = $scope.trialeditparticipant.trial_registration_column_5;
                        $scope.trial_participant_col_6_temp = $scope.trialeditparticipant.trial_registration_column_6;
                        $scope.trial_participant_col_7_temp = $scope.trialeditparticipant.trial_registration_column_7;
                        $scope.trial_participant_col_8_temp = $scope.trialeditparticipant.trial_registration_column_8;
                        $scope.trial_participant_col_9_temp = $scope.trialeditparticipant.trial_registration_column_9;
                        $scope.trial_participant_col_10_temp = $scope.trialeditparticipant.trial_registration_column_10;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };            
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.trialdetails = function () {
                $scope.trialdetailsview = true;
                $scope.trialparticipantinfoview = false;
                $scope.trialhistoryview = false;
                $scope.trialpaymenthistoryview = false;
                $scope.trialattendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-trialdetails').addClass('active-event-list').removeClass('event-list-title'); 
                $('.trialdetail-tabtext-1').addClass('greentab-text-active');
                $scope.Trial_Detail();
            };
            
            $scope.trialparticipantinfo = function () {
                $scope.trialdetailsview = false;
                $scope.trialparticipantinfoview = true;
                $scope.trialhistoryview = false;
                $scope.trialpaymenthistoryview = false; 
                $scope.trialattendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-trialparticipantinfo').addClass('active-event-list').removeClass('event-list-title');
                $('.trialdetail-tabtext-2').addClass('greentab-text-active');
                $scope.Trial_Participant_Info();
            };
            
            $scope.trialhistory = function () {
                $scope.trialdetailsview = false;
                $scope.trialparticipantinfoview = false;
                $scope.trialhistoryview = true;
                $scope.trialpaymenthistoryview = false; 
                $scope.trialattendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-trialhistory').addClass('active-event-list').removeClass('event-list-title');
                $('.trialdetail-tabtext-3').addClass('greentab-text-active');
                $scope.Trial_History();
            };
            
            $scope.trialpaymenthistory = function () {
                $scope.trialdetailsview = false;
                $scope.trialparticipantinfoview = false;
                $scope.trialhistoryview = false;
                $scope.trialpaymenthistoryview = true; 
                $scope.trialattendancedetailview = false;
                $scope.resettabbarclass();
                $('#li-trialpaymenthistory').addClass('active-event-list').removeClass('event-list-title');
                $('.trialdetail-tabtext-4').addClass('greentab-text-active');
                $scope.Trial_Payment_History();
            };
            
            $scope.trialattendanceDetails = function(){
                $scope.trialdetailsview = false;
                $scope.trialparticipantinfoview = false;
                $scope.trialhistoryview = false;
                $scope.trialpaymenthistoryview = false;
                $scope.trialattendancedetailview = true;
                $scope.resettabbarclass();
                $('#li-trialattendancedetails').addClass('active-event-list').removeClass('event-list-title');  
                $('.trialdetail-tabtext-5').addClass('greentab-text-active');
                $scope.trialdatetimeerror = false;
                $scope.trial_attendance_date_change = 'N';
                $scope.trial_calender_attendancedate = '';
                $scope.Trial_Attendance_detail();                
            };
            
            $scope.editTrialHistoryNote = function (ind, hist_id, action) {
                if(action === 'add'){
                    $scope.trial_edit_status = 'add';
                    $scope.edited_trial_history_id = '';
                    $scope.trialhistorynote = '';
                    $scope.TrialHistoryNoteButton = 'Add';
                    $("#trialHistoryNoteModal").modal('show');
                    $("#trialHistoryNotetitle").text('Add Note');                    
                }else if(action === 'edit'){
                    $scope.edited_trial_history_id = hist_id ;
                    $scope.trial_edit_status = 'update';
                    $scope.TrialHistoryNoteButton = 'Update';
                    $scope.trialhistorynote = $scope.trial_history[ind].activity_text;
                    $("#trialHistoryNoteModal").modal('show');
                    $("#trialHistoryNotetitle").text('Update Note');
                }else if(action === 'delete'){
                    $scope.edited_trial_history_id = hist_id ;
                    $scope.trialhistorynote =  '';
                    $scope.trial_edit_status = 'delete';
                    $("#trialHistoryNoteDeleteModal").modal('show');
                    $("#trialHistoryNoteDeletetitle").text('Delete Note');
                    $("#trialHistoryNoteDeletecontent").text('Are you sure want to delete the note?');
                }
            };
            
            $scope.confirmtrialeditHistoryNote = function () {
                $("#trialHistoryNoteModal").modal('hide');
                $("#trialHistoryNoteDeleteModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'TrialHistoryNotes',
                    data: {
                        "company_id": $localStorage.company_id,
                        "student_id":$scope.student_id,
                        "history_id": $scope.edited_trial_history_id,
                        "status":$scope.trial_edit_status,
                        "trial_reg_id": $scope.editTrial_reg_id,
                        "note_text": $scope.trialhistorynote
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.trial_history = response.data.trial_history.msg; 
                        
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.sendTrialPaymentHistory = function () {
                $("#payTrialHistoryEmailModal").modal('show');
                $("#payTrialHistoryEmailtitle").text('Send Email');
                $("#payTrialHistoryEmailcontent").text('Are you sure want to send a email with payment history details?');
            };
            
             $scope.confirmsendTrialPayHistoryEmail = function () {
                $("#payTrialHistoryEmailModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendPaymentHistory',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_reg_id": $scope.editTrial_reg_id, //common call
                        "category": 'trial'
                        
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
//                        $("#refundModal").modal('hide');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text('Trial Payment History Sent Successfully.');
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.performTrialActionUpdate = function (ind, payment_status,credit_method) { //Show list of Actions in Past tab//
                
                $scope.trialactionpayment_array = $scope.trial_payment_history[ind];
                $scope.editTrial_reg_id = $scope.trialactionpayment_array.reg_id;
                $scope.editTrial_id=$scope.trialactionpayment_array.trial_id;
                $scope.editTrialPaymentMethod_payment_id = $scope.trialactionpayment_array.payment_id;
                $scope.temp_past_paymentarray1 = angular.copy($scope.trialactionpayment_array.payment_amount_without_pf);
                $scope.temp_past_paymentarray2 = angular.copy($scope.trialactionpayment_array.payment_amount_without_pf);
                
                
               if (payment_status === 'S' || payment_status === 'M') { // REFUND
                    $scope.trialactionValue = 1;
                    if($scope.trialactionpayment_array.checkout_status==='released' && payment_status === 'S'){
                        $("#trialRefundActionModal").modal('show');
                        $("#trialRefundActiontitle").text('Edit Order');
                        $("#trialRefundActioncontent").text("Are you sure you want to process the Trial refund?");
                    }else if(payment_status === 'M' && (credit_method === 'CA' || credit_method === 'CH' )){
                        $("#trialRefundActionModal").modal('show');
                        $("#trialRefundActiontitle").text('Edit Order');
                        $("#trialRefundActioncontent").text("Are you sure you want to process the Trial refund?");
                    }else{
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Edit Order');
                        $("#trialmessagecontent").text('Payment can be refunded only after it is released. Please try again after it is released.');
                    }
                }else if (payment_status === 'F' || payment_status === 'N'){ // APPLY MANUAL CREDIT
                        $scope.trialactionValue = 2;
                        $("#paymentHistoryModal1").modal('show');
                        $("#paymentHistorytitle").text('Apply Payment Credit');
                        $("#paymentHistorytitle1").text('Enter credit to be applied to this bill');
                }
            };
            
            $scope.confirmTrialRefund = function () { //Refund the edited Payment Plan details//

                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'processTrialPaymentRefund',
                    data: {
                        "company_id": $localStorage.company_id,
                        "trial_reg_id": $scope.editTrial_reg_id,
                        "trial_payment_id": $scope.editTrialPaymentMethod_payment_id,
                        "trial_id":  $scope.editTrial_id,
                        'credit_method':$scope.applypaymentcredit_option
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.trial_payment_history = response.data.payment_history.msg.history;
                        for (i=0;i<(response.data.payment_history.msg.upcoming).length;i++){
                            $scope.trial_payment_history.push(response.data.payment_history.msg.upcoming[i]);
                        }
                    } else if (response.data.status === 'Expired') {
//                        $('#progress-full').hide();
//                        $("#trialmessageModal").modal('show');
//                        $("#trialmessagetitle").text('Message');
//                        $("#trialmessagecontent").text(response.data.msg);
//                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        if (response.data.error) {
                            $("#trialmessagetitle").text(response.data.error);
                            $("#trialmessagecontent").text(response.data.error_description);
                        } else {
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                        }
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };        
            
            $scope.closeTrialDetailView = function () {
              $('#progress-full').show();
              if ($localStorage.trialpagefrom === 'managetrial'){
//                  $('#progress-full').show();
                    if($localStorage.trialdetail_pagefrom === 'A'){
                        $localStorage.detail_closetrialpagefrom = 'A';
                        $location.path('/managetrial');
                    }else if($localStorage.trialdetail_pagefrom === 'E'){
                        $localStorage.detail_closetrialpagefrom = 'E';
                        $location.path('/managetrial');
                    }else if($localStorage.trialdetail_pagefrom === 'C'){
                        $localStorage.detail_closetrialpagefrom = 'C';
                        $location.path('/managetrial');
                    }else{
                        $localStorage.detail_closetrialpagefrom = 'D';
                        $location.path('/managetrial');
                    }
              }else if ($localStorage.trialpagefrom === 'payment'){
                  $location.path('/payment');
              }else{
                  $location.path('/trials');
              }  
            };
            
            $scope.openTrialDetailsPage = function () {
                if ($localStorage.trialpagefrom === 'payment') {
                    $scope.trialpaymenthistory();
                } else {
                    $scope.trialdetails();
                }
            };
            
//            MM Attendance
            $scope.AttendanceTrialDateAdd =function() {
                if($scope.trial_calender_attendancedate === "aN/aN/NaN"){
                   $scope.trial_calender_attendancedate = ''; 
                }
                $scope.showtrialAttendanceModal();
            };
            $scope.showtrialAttendanceModal =function() {
                if($scope.trial_attendance_date_change === 'N'){
                    $scope.trialdatepickershow = $scope.trialtimepickershow = false;
                    $("#trialattendancemodal").modal('show');
                    $scope.trialattendancedate = $scope.trial_calender_attendancedate;
                    $scope.trialattendancetime = $scope.trial_delete_attendance_id ='';
                    if($scope.trialattendancedate){
                        var cur_atten_date = $scope.trialattendancedate.split('/');
                        cur_atten_date = cur_atten_date[2]+"-"+cur_atten_date[0]+"-"+cur_atten_date[1];                    
                        if(new Date(cur_atten_date) > new Date()){
                            var date = new Date();
                            $scope.trialattendancedate = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
                        }
                        $scope.catchTrialAttendanceDate('N');                    
                        $scope.showTrialDatePicker();
                    }else{
                        $scope.trialattendancedate = "";
                    }                    
                }else{
                    $scope.trial_attendance_date_change = 'N';
                    $scope.addorDeleteTrialAttendance('A');
                }                
            };
            $scope.showTrialDatePicker =function() {
                $scope.trialdatepickershow = true;
                $scope.trial_attendance_date_change = 'N';
                $('#trialattendancedate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true                    
                });
                $('#trialattendancedate').datepicker('setEndDate', new Date());
            };
            $scope.showTrialTimePicker =function() {
                $scope.trialtimepickershow = true;
            };
            $scope.catchTrialAttendanceDate =function(onselect) {
                var date = new Date();
                $scope.trialcurrentDate = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
                $scope.trialattendancetime = "";
                if($scope.trialattendancedate === $scope.trialcurrentDate ){                     
                    $('#trialattendancetime').timepicker("remove");
                    $('#trialattendancetime').timepicker({
                        timeFormat: 'h:i A',
                        step: 15,
                        ignoreReadonly: true,
                        maxTime : new Date()
                    });  
                }else{
                    $('#trialattendancetime').timepicker("remove");
                    $('#trialattendancetime').timepicker({
                        timeFormat: 'h:i A',
                        step: 15,
                        ignoreReadonly: true
                    }); 
                }
                $scope.attendance_date_change = 'N';
            };   
            $scope.cancelTrialAttendance =function() {
                $scope.trialdatepickershow = $scope.trialtimepickershow = false;                
                $("#trialattendancemodal").modal('hide');
                $scope.trialattendancedate = $scope.trialattendancetime = $scope.trial_delete_attendance_id = '';
                $scope.trialdatetimeerror = false;
                $scope.trial_attendance_date_change = 'N';
            };
            $scope.addorDeleteTrialAttendance =function(flag) {
                    var attendance_date_time;
                    var date = new Date();
                    var current_time =  date.getHours() + ':' + date.getMinutes()+":" + date.getSeconds();
                    var current_Date_Time = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) +" "+current_time;
                    if($scope.trialattendancedate && $scope.trialattendancetime){
                        attendance_date_time = $scope.dateformat($scope.trialattendancedate)+" "+$scope.timeServerformat($scope.trialattendancetime);
                        if(Date.parse(attendance_date_time) > Date.parse(current_Date_Time)){
                            $scope.trialdatetimeerror = true;
                            return false;
                        }else{
                             $scope.trialdatetimeerror = false;
                        }
                    }else{
                        attendance_date_time = "";
                    }
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'deleteorinsertAttendance',
                        data: {
                            "company_id": $localStorage.company_id,
                            "reg_id": $scope.currentTrial_reg_id,
                            "attendance_date_time": attendance_date_time,
                            "attendance_id": $scope.trial_delete_attendance_id,
                            "flag": flag,
                            "category": "T",
                        },
                        headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();
                            $("#trialattendancemodal").modal('hide');
                            $("#trialattendanceHistoryDeleteModal").modal('hide');
                            $("#trialmessageModal").modal('show');
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                            $scope.trial_attendance_history = response.data.attendance_details.msg.attendance_history;
                            $scope.trial_attendance_history_array = angular.copy($scope.trial_attendance_history); 
                            $scope.trial_attendance_days_count = response.data.attendance_details.days_count;
                            $scope.trialattendancedate = $scope.trialattendancetime = '';
                            $scope.trial_active_dates = $scope.trial_checkedin_dates = [];
                            $scope.trial_active_dates = response.data.attendance_details.msg.checkin_dates;
                            $scope.trialdatetimeerror = false;
                            $scope.trial_attendance_date_change = 'N';
                            $scope.trial_calender_attendancedate = '';
                            $("#trialselectedDateCalender").datepicker('remove');
                            if($scope.trial_active_dates.length > 0){
                                for(i=0;i<$scope.trial_active_dates.length;i++){
                                    $scope.trial_checkedin_dates.push($scope.trial_active_dates[i]);
                                }
                            }else{
                                $scope.trial_active_dates = $scope.trial_checkedin_dates = [];
                            }                        
                            var active_dates = $scope.trial_checkedin_dates;
                            $("#trialselectedDateCalender").datepicker({
                                 beforeShowDay: function(date){
                                     var d = date;
                                     var curr_date = d.getDate();
                                     var curr_month = d.getMonth() + 1; //Months are zero based
                                     var curr_year = d.getFullYear();
                                     var formattedDate = curr_date + "/" + curr_month + "/" + curr_year;
                                        if ($.inArray(formattedDate, active_dates) !== -1){
                                           return {
                                              classes: 'activeDates'
                                           };
                                       }
                                      return;
                                }
                            }).on("changeDate", function (e) {
                                var selected_date = new Date(e.dates);
                                var currentDate = ('0' + (selected_date.getMonth() + 1)).slice(-2) + '/' + ('0' + selected_date.getDate()).slice(-2) + '/' + selected_date.getFullYear();
                                $scope.trial_calender_attendancedate = $scope.trialattendancedate = currentDate;
                            });
                        } else if (response.data.status === 'Expired') {
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#trialmessageModal").modal('show');
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                            $scope.logout();
                        }else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        } else {
                            $('#progress-full').hide();
                            $("#trialsuccessModal").modal('show');
                            $("#trialsuccesstitle").text('Message');
                            $("#trialsuccesscontent").text(response.data.msg);
                        }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });
              };
              $scope.deleteTrialAttendanceHistory = function(att_id){
                  $("#trialattendanceHistoryDeleteModal").modal('show');
                  $("#trialattendanceHistoryDeletetitle").text('Message');
                  $("#trialattendanceHistoryDeletecontent").text('Are you sure you want to delete Attendance History');
                  $scope.trial_delete_attendance_id = att_id ;
            };
            $scope.cancelDeleteTrialAttendance =function() {
                $scope.trialdatepickershow = $scope.trialtimepickershow = false;   
                $scope.trialattendancedate = $scope.trialattendancetime = $scope.trial_delete_attendance_id = '';
                $("#trialattendanceHistoryDeleteModal").modal('hide');
            };
            
            
            $scope.timeServerformat = function (time) {
                if (time.length > 0) {
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    var formatedtime = sHours + ":" + sMinutes + ":01";
                    return formatedtime;
                } else {
                    var formatedtime = "";
                    return formatedtime;
                }
            };
            
            $scope.dateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }
            };
            $scope.formatserverdate = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    return month + '/' + day + '/' + year;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            };
            $scope.trialsafariiosdate = function (dat) {

                var dda = dat.toString();
                dda = dda.replace(/-/g, "/");
                var curr_date = new Date(dda);   //  FOR DATE FORMAT IN SAFARI 

                var given_start_year = curr_date.getFullYear();
                var given_start_month = curr_date.getMonth();
                var given_start_day = curr_date.getDate();
                given_start_month = curr_date.getMonth() + 1;
                var dda = given_start_year + "-" + given_start_month + "-" + given_start_day;
                dda = dda.toString();
                dda = dda.replace(/-/g, "/");
                var dup_date = new Date(dda);  //  FOR DATE FORMAT IN SAFARI 

                return dup_date;
            
            };
            
            $scope.trialdatestringview = function (ddate) {
                var Displaydate = ddate.toString();
                Displaydate = ddate.split("-");
                var year = Displaydate[0];
                var month = Displaydate[1] - 1;
                var date = Displaydate[2];
                var ReadDate = new Date(year, month, date);
                var datesuccess = ReadDate.toString();
                var finaldateformat = datesuccess.split(" ");
                var finalized = finaldateformat[0] + " " + finaldateformat[1] + " " + finaldateformat[2] + " " + finaldateformat[3];
                return finalized;
            };
            
            $scope.showTrialPaymentMethodModal = function () { // SHOW EDIT PAYMENT METHOD FORM
                if($scope.wepay_enabled){
                    $("#editTrialpaymentMethodModal").modal('show');
                    $("#editTrialpaymentMethodActiontitle").text('Edit Payment Method');
                }else if($scope.stripe_enabled){
                    $("#UpdateTrialPaymentMethodModal").modal('show');
                    $("#UpdateTrialPaymentMethodModaltitle").text('Edit Payment Method');
                    if($scope.stripe_publish_key){
                        $scope.stripe = Stripe($scope.stripe_publish_key);
                    }
                    $scope.clearStripeCardData();
                    $scope.getStripeElements();
                }
                
            };
             $scope.clearStripeCardData = function(){
                $scope.stripe_first_name = "";
                $scope.stripe_last_name = "";
                $scope.stripe_email = "";
                $scope.valid_stripe_card = false;
                if ($scope.stripe_card) {
                    $scope.stripe_card.clear();
                }
            };
            // STRIPE TOKEN CALL
            $scope.getStripeElements = function () {
                // Create a Stripe client
                $scope.valid_stripe_card = false;

                // Create an instance of Elements
                var elements =  $scope.stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                // (Note that this demo uses a wider set of styles than the guide below.)
                var style = {
                    base: {
                        color: 'rgb(74,74,74)',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                // Create an instance of the card Element
                $scope.stripe_card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>
                $scope.stripe_card.mount('#trial-stripe-card-element');

                // Handle real-time validation errors from the card Element.
                $scope.stripe_card.addEventListener('change', function (event) {
                    var displayError = document.getElementById('trial-stripe-card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                        $scope.valid_stripe_card = false;
                    } else {
                        displayError.textContent = '';
                        $scope.valid_stripe_card = true;
                    }
                });
            };
            
            $scope.trialresetForm = function () { // RESET CREDIT CARD FORM
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.cardnumber = '';
                $scope.$parent.ccmonth = '';
                $scope.$parent.ccyear = '';
                $scope.cvv = '';
                $scope.$parent.country = '';
                $scope.postal_code = '';
                $scope.tdpaymentform.$setPristine();
                $scope.tdpaymentform.$setUntouched();               

            };
            
            if ($localStorage.wepay_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($localStorage.wepay_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }

            WePay.set_endpoint($scope.wepay_env); //    CHANGE TO "PRODUCTION" WHEN LIVE
            // Shortcuts
            var d = document;
                d.id = d.getElementById,
                valueById = function(id) {
                    return d.id(id).value;
                };
                
                // FOR THOSE NOT USING DOM LIBRARIES
            var addEvent = function (e, v, f) {
                if (!!window.attachEvent) {
                    e.attachEvent('on' + v, f);
                } else {
                    e.addEventListener(v, f, false);
                }
            };
            
            // Attach the event to the DOM
            addEvent(d.id('cc-submittrials'), 'click', function() {
                $('#progress-full').show();
                var userName = [valueById('firstName')]+' '+[valueById('lastName')];
                var user_first_name = valueById('firstName');
                var user_last_name = valueById('lastName');
                var email = valueById('email').trim();
                var postal_code = valueById('postal_code');
                var country = valueById('country');
                    response = WePay.credit_card.create({
                    "client_id":        $localStorage.wepay_client_id,
                    "user_name":        userName,
                    "email":            valueById('email').trim(),
                    "cc_number":        valueById('cc-number'),
                    "cvv":              valueById('cc-cvv'),
                    "expiration_month": valueById('cc-month'),
                    "expiration_year":  valueById('cc-year'),
                    "virtual_terminal": "web",
                    "address": {
                        "country": valueById('country'),
                        "postal_code": valueById('postal_code')
                    }
                }, function(data) {
                    if (data.error) {
                        $('#progress-full').hide();
                        console.log(data);
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Wepay Message');
                        $("#trialmessagecontent").text(data.error_description);
                        // handle error response
                    } else {
                        $scope.editTrialDBPaymentMethod(data.credit_card_id, data.state, user_first_name, user_last_name, email, postal_code);
                            console.log(data);
                        // call your own app's API to save the token inside the data;
                        // show a success page
                    }
                });
            });
            
            $scope.editTrialDBPaymentMethod = function (cc_id, cc_state, user_first_name, user_last_name, email, postal_code) { // UPDATE CREDIT CARD DETAILS
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatePaymentMethod',
                    data: {
                        company_id: $localStorage.company_id,
                        reg_id: $scope.currentTrial_reg_id,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email: email,
                        postal_code: postal_code,
                        cc_id: cc_id,
                        cc_state: cc_state,
                        category_type: 'T'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#editTrialpaymentMethodModal").modal('hide');
                        $("#editTrialpaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text('Payment method successfully updated.');
                        $scope.trialresetForm();
                        $scope.trial_detail = response.data.reg_details.msg;
//                        $scope.trial_list = $scope.trial_detail.All;
                        $scope.trialstatus = $scope.trial_detail.trial_status;
                        $scope.student_id = $scope.trial_detail.student_id;
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        $scope.editTrial_reg_id = $scope.trial_detail.trial_reg_id;
                        $scope.editTrial_id = $scope.trial_detail.trial_id;
                        $scope.editTrialStatus = $scope.trial_detail.trial_status;
                        $scope.editTrialCategoryTitle = $scope.trial_detail.trial_category;
                        $scope.trial_leadsource_arr = $scope.trial_detail.reg_columns.lead_source;
                        $scope.selected_trialeditleadsource = $scope.trial_detail.leadsource;
                        $scope.trial_prolength_type = $scope.trial_detail.program_length_type;
                        $scope.trial_prolength_value = $scope.trial_detail.program_length;
                        $scope.trialname= $scope.trial_detail.buyer_name.split(" ");
                        $scope.firstname = $scope.trialname[0];
                        $scope.lastname = $scope.trialname[1];
                        $scope.email = $scope.trial_detail.buyer_email;
                        
                    } else if (response.data.status === 'Expired') {
                        $("#editTrialpaymentMethodModal").modal('hide');
                        $("#editTrialpaymentMethodActiontitle").text('');
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $("#editTrialpaymentMethodModal").modal('hide');
                        $("#editTrialpaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        if (response.data.error) {
                            $("#trialmessagetitle").text(response.data.error);
                            $("#trialmessagecontent").text(response.data.error_description);
                        } else {
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                        }
                    }
                }, function (response) {
                    $("#editTrialpaymentMethodModal").modal('hide');
                    $("#editTrialpaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.performTrialRerunAction = function (ind, payment_status) { //Show list of Actions in Past tab//
                $scope.trialactionpayment_array = $scope.trial_payment_history[ind];

                $scope.editTrial_reg_id = $scope.trialactionpayment_array.reg_id;
                $scope.editTrial_id = $scope.trialactionpayment_array.trial_id;
                $scope.editTrialPaymentMethod_payment_id = $scope.trialactionpayment_array.payment_id;
                $scope.temp_past_paymentarray1 = angular.copy($scope.trialactionpayment_array.payment_amount_without_pf);
                $scope.temp_past_paymentarray2 = angular.copy($scope.trialactionpayment_array.payment_amount_without_pf);
                if (payment_status === 'F' || payment_status === 'N') { //RE-RUN
                    $scope.trialactionValue = 0;
                    $("#paymentHistoryModal").modal('show');
                    $("#paymentHistorytitle").text('Re-Run Payment');
                    $("#paymentHistorycontent").text("Are you sure to Re-Run the Payment?");
                }
            };
            
            $scope.actionReRunConfirm = function (value) { //Function call after confirmation//
                $scope.editSingle = "";
                $scope.actionPastchoosen = "";
                if (value === 0) {
                    $scope.reRunPayment();
                }
            };
            
            $scope.reRunPayment = function () { //update credit card details
                
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'reRunPayment',
                    data: {
                        "company_id": $localStorage.company_id,
                        "reg_id": $scope.editTrial_reg_id,
                        "payment_id": $scope.editTrialPaymentMethod_payment_id,
                        "category_type": 'T'
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                     withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {                        
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.trial_payment_history = response.data.payment_history.msg.history;
                        for (i=0;i<(response.data.payment_history.msg.upcoming).length;i++){
                            $scope.trial_payment_history.push(response.data.payment_history.msg.upcoming[i]);
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#trialsuccessModal").modal('show');
                        $("#trialsuccesstitle").text('Message');
                        $("#trialsuccesscontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updatePaidquantity = function (value, newval) { //Validate The Credit amount Entered  By User//
                $scope.upcoming_creditamount = 0;
                if (!value || parseFloat(newval) > parseFloat(value) || parseFloat(newval) < parseFloat(value)) {
                    $("#trialsuccessModal").modal('show');
                    $("#trialsuccesstitle").text('Message');
                    $("#trialsuccesscontent").text('Credit amount should be lesser than bill amount.');
                }else {
                    $scope.upcoming_creditamount = newval;
                    $("#trialsuccessModal").modal('hide');
                    $("#paymentHistoryModal3").modal('show');
                    $("#paymentHistorytitle3").text('Message');
                    $("#paymentHistorycontent3").text('Are you sure want to add a credit to this bill?');
                    return;
                }
            };
            
            $scope.confirmCreditAmount = function (creditamount) { //Function call After Getting Credit amount From user//
                $scope.markAsTrialPaid($scope.trialactionpayment_array, creditamount);
            };
            
            $scope.markAsTrialPaid = function (payment_array, creditamount) { //Mark the membership as Paid
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'trialPaymentMarkedAsPaid',
                    data: {
                        "company_id": $localStorage.company_id,
                        "payment_id": payment_array.payment_id,
                        "reg_id": payment_array.reg_id,
                        "credit_amount": creditamount,
                        "credit_method":$scope.applypaymentcredit_option
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $("#paymentHistoryModal1").modal('hide');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.upcoming_creditamount = '';
                        $scope.trial_payment_history = response.data.payment_history.msg.history;
                        for (i=0;i<(response.data.payment_history.msg.upcoming).length;i++){
                            $scope.trial_payment_history.push(response.data.payment_history.msg.upcoming[i]);
                        }
                    } else if (response.data.status === 'Expired') {
//                        console.log(response.data);
//                        $('#progress-full').hide();
//                        $("#trialmessageModal").modal('show');
//                        $("#trialmessagetitle").text('Message');
//                        $("#trialmessagecontent").text(response.data.msg);
//                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.trialdashboardredirectview2 = function(){
                $localStorage.trialredirectto = "DA";
                $location.path('/managetrial');
            };
            $scope.trialredirectlivelistview2 = function(){
                $localStorage.trialredirectto = "L";
                $location.path('/managetrial');
            };
            $scope.trialredirectdraftlistview2 = function(){
                $localStorage.trialredirectto = "D";
                $location.path('/managetrial');
            };
            $scope.createStripePaymentMethod = function(){
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error. 
                        var errorElement = document.getElementById('event-stripe-card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.updateStripePaymentMethodInDB($scope.payment_method_id, $scope.stripe_first_name, $scope.stripe_last_name, $scope.stripe_email);
                    }
                });
            };
            
            $scope.updateStripePaymentMethodInDB = function (payment_method_id, user_first_name, user_last_name, email) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateStripePaymentMethod',
                    data: {
                        company_id: $localStorage.company_id,
                        reg_id: $scope.currentTrial_reg_id,
                        buyer_first_name: user_first_name,
                        buyer_last_name: user_last_name,
                        email: email,
                        category_type: 'T',
                        payment_method: payment_method_id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8', },
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $("#UpdateTrialPaymentMethodModal").modal('hide');
                        $("#UpdateTrialPaymentMethodModaltitle").text('');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text('Payment method successfully updated.');
                        $scope.clearStripeCardData();
                        $scope.trial_detail = response.data.reg_details.msg;
//                        $scope.trial_list = $scope.trial_detail.All;
                        $scope.trialstatus = $scope.trial_detail.trial_status;
                        $scope.student_id = $scope.trial_detail.student_id;
                        $scope.trialedit = angular.copy($scope.trial_detail);
                        $scope.editTrial_reg_id = $scope.trial_detail.trial_reg_id;
                        $scope.editTrial_id = $scope.trial_detail.trial_id;
                        $scope.editTrialStatus = $scope.trial_detail.trial_status;
                        $scope.editTrialCategoryTitle = $scope.trial_detail.trial_category;
                        $scope.trial_leadsource_arr = $scope.trial_detail.reg_columns.lead_source;
                        $scope.selected_trialeditleadsource = $scope.trial_detail.leadsource;
                        $scope.trial_prolength_type = $scope.trial_detail.program_length_type;
                        $scope.trial_prolength_value = $scope.trial_detail.program_length;
                        $scope.trialname= $scope.trial_detail.buyer_name.split(" ");
                        $scope.first_name = $scope.trialname[0];
                        $scope.last_name = $scope.trialname[1];
                        $scope.email = $scope.trial_detail.buyer_email;
                        
                    } else if (response.data.status === 'Expired') {
                        $("#editTrialpaymentMethodModal").modal('hide');
                        $("#editTrialpaymentMethodActiontitle").text('');
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        $("#trialmessagetitle").text('Message');
                        $("#trialmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $("#editTrialpaymentMethodModal").modal('hide');
                        $("#editTrialpaymentMethodActiontitle").text('');
                        $('#progress-full').hide();
                        $("#trialmessageModal").modal('show');
                        if (response.data.error) {
                            $("#trialmessagetitle").text(response.data.error);
                            $("#trialmessagecontent").text(response.data.error_description);
                        } else {
                            $("#trialmessagetitle").text('Message');
                            $("#trialmessagecontent").text(response.data.msg);
                        }
                    }
                }, function (response) {
                    $("#editTrialpaymentMethodModal").modal('hide');
                    $("#editTrialpaymentMethodActiontitle").text('');
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

             
        } else {
            $location.path('/login');
        }
    }
    module.exports =   TrialDetailController;