MembersController.$inject=['$scope', '$sce', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope'];
function MembersController($scope, $sce, $location, $http, $localStorage, urlservice, $route, $rootScope) {

            if ($localStorage.islogin == 'Y') {  
                angular.element(document.getElementById('sidetopbar')).scope().getName();
                angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
                angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
                angular.element(document.getElementById('sidetopbar')).scope().regComplete(); 
                angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
                angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
               
//                $scope.homeurlfrmt =  /^\s*((ftp|http|https)\:\/\/)?([a-z\d\-]{1,63}\.)*[a-z\d\-]{1,255}\.[a-z]{2,6}\s*.*$/;
                $rootScope.hidesidebarlogin = false;
                $rootScope.sideBarHide = false;                
                $rootScope.adminsideBarHide = false;
                $rootScope.activepagehighlight = 'appmenu';
                $scope.editclose = false;
                $scope.editcmpnycode = $scope.CompanyCodeLabel= $scope.CompanyCodeInput= $scope.editshow= true;
                $scope.message = "MyStudio";
                $scope.verifyconfirm = $localStorage.verfication_complete;
                $scope.cropper = {};
                $scope.cropper.sourceImage = null;
                $scope.cropper.croppedImage = null;
                $scope.bounds = {};
                $scope.bounds.left = 0;
                $scope.bounds.right = 0;
                $scope.bounds.top = 0;
                $scope.bounds.bottom = 0;
                $scope.croppedImage = "";

            $scope.studiotypes = [ "Martial Arts", "Dance", "Yoga", "Crossfit", "Gymnastics", "Summer Camps", "Nonprofit", "Church", "Consulting", "Other"];             
        
            //First time login message Pop-up ----START 
            tooltip.hide();
            $scope.studiotip = $scope.welcometip =  $scope.previewtip = true;
            
            if($localStorage.verfication_complete !== 'U'){
                  if($localStorage.firstlogin){
                  if($localStorage.firstlogin_welcome === 'Y'){             //Welcome Tooltip show
                    $('#tooltipfadein').css("display", "block");
                    $localStorage.firstlogin_welcome = 'N';
                    $scope.welcometip = false;
                    tooltip.pop("navlink", "#welcometip",{position:4});
                    }
                }
            }
            
            $scope.welcomeNext = function () {                      //Preview Tooltip show
               if($localStorage.firstlogin_preview === 'Y'){
                $localStorage.firstlogin_preview = 'N';
                $scope.welcometip = true;
                $scope.previewtip = false;
                $("#homelink").removeClass('active');
                $("#companylink").addClass('active');
                tooltip.pop("profileframe", "#previewtip",{position:3});
                }
            }; 
                
            $scope.previewNext = function () {                  //Studio Profile Tooltip show
               if($localStorage.firstlogin_profile === 'Y'){
                $localStorage.firstlogin_profile = 'N';
                $scope.previewtip = true;
                $scope.studiotip = false;
                tooltip.pop("companylink", "#studioprofiletip",{position:1});
                }
            }; 
            
            $("#tooltipfadein").click(function(){                 
                $("#tooltipfadein").css("display", "none");
                tooltip.hide();
               });

            $("#studiodetailNext").click(function(){                 
                $("#tooltipfadein").css("display", "none");
                $scope.studiotip = true;
                tooltip.hide();
            });
            
            //First time login message Pop-up ----END
            
            
          $scope.studioselection = function(){ 
            if($scope.studiotype === 'Other'){
                $scope.otherstudio = true;
            }else{
                $scope.otherstudio = false;
            }
            };
            
            //For Social Url to load on preview screen----START
            
            $localStorage.currentpage = "social";
            $localStorage.PreviewEventNamee="";
            
            $scope.getSocial = function(){
            $('#progress-full').show(); 
               $http({
                   method: 'GET',
                   url: urlservice.url+'getsocialdetails',
                   params: {
                    "company_id":$localStorage.company_id
                   },
                   headers:{"Content-Type":'application/json; charset=utf-8',},
                   withCredentials: true  
               
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide(); 
                        $scope.socialData = response.data.msg[0];
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.getSocial();
            
            $scope.$watch('socialData.google_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.google_url){
                    $localStorage.previewMenuGoogle = "true";
                }
                else{
                    $localStorage.previewMenuGoogle = "false";
                }
            });
            
            $scope.$watch('socialData.yelp_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.yelp_url){
                    $localStorage.previewMenuYelp = "true";
                }
                else{
                    $localStorage.previewMenuYelp = "false";
                }
            });
            
            $scope.$watch('socialData.facebook_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.facebook_url){
                    $localStorage.previewMenuFBID = "true";
                }
                else{
                    $localStorage.previewMenuFBID = "false";
                }
            });
            $scope.$watch('socialData.twitter_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.twitter_url){
                    $localStorage.previewMenuTweet = "true";
                }
                else{
                    $localStorage.previewMenuTweet = "false";
                }
            });
            $scope.$watch('socialData.instagram_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.instagram_url){
                    $localStorage.previewMenuInst = "true";
                }
                else{
                    $localStorage.previewMenuInst = "false";
                }
            });
            $scope.$watch('socialData.vimeo_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.vimeo_url){
                    $localStorage.previewMenuVimeo = "true";
                }
                else{
                    $localStorage.previewMenuVimeo = "false";
                }
            });
            $scope.$watch('socialData.youtube_url', function(newVal,oldVal) {
                if($scope.socialData && $scope.socialData.youtube_url){
                    $localStorage.previewMenuYou = "true";
                }
                else{
                    $localStorage.previewMenuYou = "false";
                }
            });
            
            //For Social Url to load on preview screen----END
            $scope.imgchanged = function () {
                $("#cropmodal").modal('show');
                 $scope.isImageUpdate = 'N';

            };
             $scope.imgcropcancel = function () {
                $scope.isImageUpdate = 'N';
            };

            $scope.imgcropped = function () {
               $scope.isImageUpdate = 'Y';
               $scope.croppedImage = $scope.cropper.croppedImage;
               $localStorage.previewDLogoScreen = $scope.croppedImage;
            };
            
            $scope.removelogo = function () {
                $scope.isImageUpdate = 'Y'; 
                $scope.croppedImage = "";
                $localStorage.previewDLogoScreen="";
            };
            
            $scope.removepdffile = function () {
                $scope.isPdfScheduleUpdate = 'Y';
                $scope.pdfpreview = ""; 
                $scope.pdfiframepreview = "";
                $scope.schedulefile = "";
            };
            
            $scope.$watch(function () {
                return $scope.croppedImage;
            }, function (newVal, oldVal) {
                $localStorage.previewDLogoScreen = newVal;
            });
            
            $scope.editCompanycode = function(){
                var msg='Modifying Company code will affect the user login and event details.';
                $("#cmpnycodeeditModal").modal('show');
                $("#cmpnycodeedittitle").text('Message');
                $("#cmpnycodeeditcontent").text(msg);
            };
            
            $scope.editAccept = function(){
                $scope.editcmpnycode = false;
                $scope.CompanyCodeLabel= false;
                $scope.CompanyCodeInput= false; 
                $scope.editclose = true;
                $scope.editshow= false;
            };
            
            $scope.editCancel = function(){
                $scope.editcmpnycode = true;
                $scope.CompanyCodeLabel= true;
                $scope.CompanyCodeInput= true;
                $scope.editclose = false;
                $scope.editshow= true;
            }; 
            
            $scope.editClose = function(){
                $scope.editcmpnycode = true;
                $scope.CompanyCodeLabel= true;
                $scope.CompanyCodeInput= true;
                $scope.editclose = false;
                $scope.editshow= true;
                $scope.appcode = $scope.companyDetails.company_code;
            }; 
            
            $scope.editSave = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatecompanycode',
                    data: {
                        "company_code": $scope.appcode,
                        "company_id": $scope.companyid,
                        "user_id": $scope.userid
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {  
                        $localStorage.loginDetails = response.data.msg;
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Success Message');
                        $("#messagecontent").text('Company Code successfully updated');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }; 
            
            
//            $scope.catchLogoChange = function(){
//                $localStorage.previewDLogoScreen = $scope.croppedImage;
//            };
            
            $scope.$watch('schedulefile', function(newVal,oldVal) {
                if(!$scope.schedulefile){
                    $localStorage.previewMenuschedulefile = "false";
                }
                else{
                    $localStorage.previewMenuschedulefile = "true";
                }
            });
            
            $scope.topreview=function(){
                    document.getElementById('previewapp').scrollIntoView();
            };
            
            
            $scope.$watch('webpage', function(newVal,oldVal) {
                if(!$scope.webpage){
                    $localStorage.previewMenuWebPage = "false";
                }
                else{
                    $localStorage.previewMenuWebPage = "true";
                }
            });
            $scope.$watch('[street,city,state,postalcode,country,timezone]', function(newVal,oldVal) {
                if(!$scope.street && !$scope.city && !$scope.state && !$scope.postalcode && !$scope.country&& !$scope.timezone){
                    $localStorage.previewMenuDirection = "false";
                }
                else{
                    $localStorage.previewMenuDirection = "true";
                }
            });
            $scope.$watch('phone', function(newVal,oldVal) {
                if(!$scope.phone){
                    $localStorage.previewMenuPhone = "false";
                }else{
                    $localStorage.previewMenuPhone = "true";
                }
                
            });
            $scope.$watch('email', function(newVal,oldVal) {
                 if($scope.email.length >= 1){
                    $localStorage.previewMenuEmail = "true";
                }else{
                    $localStorage.previewMenuEmail = "false";
                }
            });
            
            
           
            $scope.submit = function () {
                $('#progress-full').show();
               
                if ($scope.croppedImage === "" || $scope.croppedImage === undefined || $scope.croppedImage === null) {
                    $scope.imagetype = "";
                    $scope.logourl = "";
                } else {
                    $scope.imagetype = "png";
                    $scope.logourl = $scope.croppedImage.substr($scope.croppedImage.indexOf(",") + 1);
                }
                    
                $scope.schedulefile ? $scope.filetype = $scope.schedulefile.substr($scope.schedulefile.lastIndexOf('.') + 1) : $scope.filetype = $scope.pdfpreview.substr($scope.pdfpreview.lastIndexOf(".") + 1);
                $scope.fileurl = $scope.pdfpreview.substr($scope.pdfpreview.indexOf(",") + 1);
                
                 if($scope.webpage === "" || $scope.webpage === undefined){
                        $scope.webpageformat = "";
                 }else{
                     if($scope.webpage.indexOf("http://") === 0 || $scope.webpage.indexOf("https://") === 0) {
                      $scope.webpageformat = $scope.webpage;
                    }else{
                          $scope.webpageformat = 'http://'+$scope.webpage;
                    }
                     
                 }
                 
                
                if($scope.vcode === undefined || $scope.vcode === ""){
                       $scope.studioverificationcode = "";
                 }else{
                     $scope.studioverificationcode = $scope.vcode;                    
                 }
                 $scope.registration_complete = $localStorage.regstr_complete;
                 
                 if($scope.studiotype === 'Other'){
                     $scope.studiotype = $scope.otherstudiotype;
                 }
                 
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatecompany',
                    data: {
                        "image_type": $scope.imagetype,
                        "image_update": $scope.isImageUpdate ? 'Y' : 'N',
                        "logo_url": $scope.logourl,
                        "old_logo_url": $scope.old_logo_url,
                        
                        "class_schedule_type": $scope.filetype,
                        "class_schedule_update": $scope.isPdfScheduleUpdate ? 'Y' : 'N',
                        "class_schedule": $scope.fileurl,
                        "old_schedule_url": $scope.old_file_url,
                        
                        "company_code": $scope.appcode,
                        "company_name": $scope.name,
                        "company_id": $scope.companyid,
                        "user_id": $scope.userid,
                        "web_page": $scope.webpageformat,
                        "street_address": $scope.street,
                        "city": $scope.city,
                        "state": $scope.state,
                        "postal_code": $scope.postalcode,
                        "country": $scope.country,
                        "timezone": $scope.timezone,
                        "phone_number": $scope.phone,
                        "company_type": $scope.studiotype,
                        "verification_code": $scope.studioverificationcode,
                        "registration_complete": $scope.registration_complete,
                        "email_id": $scope.email

                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        
                        $localStorage.verfication_complete = $scope.verifyconfirm = response.data.msg.verification_complete;
                        $localStorage.company_id = response.data.msg.company_id;
                        $localStorage.loginDetails = response.data.msg;
                        $localStorage.studio_code = response.data.msg.company_code;
                        $localStorage.phone_number = response.data.msg.phone_number;
                        $('#progress-full').hide();
                        
                        if($localStorage.firstlogin && $localStorage.firstlogin_profile === 'Y' ){
                            $localStorage.firstlogin_profile = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();
                            
                        }else if($localStorage.firstlogin_profile === 'N' ||  $localStorage.firstlogin_profile === undefined){
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Success Message');
                            $("#messagecontent").text('Company details successfully updated');
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.vcode = "";
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            $scope.resendVerfyCode = function(){
                $('#progress-full').show();
          
              $http({
                        method: 'POST',
                        url: urlservice.url+'resendverificationlink',
                        data: {
                            "company_id":$localStorage.company_id
                        },
                        headers:{"Content-Type":'application/json; charset=utf-8',},
                        withCredentials: true
                        
                    }).then(function (response) {
                        if(response.data.status == 'Success'){
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text('Please check your email for a verification code that was sent from us, it may be in your spam/promotional folder. Copy and paste the verification code here to complete your registration.');
                        }else if(response.data.status === 'Expired'){
                            console.log(response.data);
                            $('#progress-full').hide();
                            $("#messageModal").modal('show');
                            $("#messagetitle").text('Message');
                            $("#messagecontent").text(response.data.msg);
                            $scope.logout();  
                        }else{                           
                            $scope.handleFailure(response.data.msg);                           
                        }   
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    }); 
                
            
            };
            
            
            
            $scope.companyDetails = $localStorage.loginDetails;            
            $scope.croppedImage = $scope.companyDetails.logo_URL;
            $scope.cropper.sourceImage = $scope.croppedImage;
            if($scope.croppedImage === null){
                   $scope.croppedImage = ""; 
                }
            $scope.pdfpreview = $scope.companyDetails.class_schedule;  
            $scope.pdfiframepreview = $sce.trustAsResourceUrl($scope.companyDetails.class_schedule);; 
            if($scope.pdfpreview === null){
               $scope.pdfpreview = ""; 
               $scope.pdfiframepreview = "";
            }
            $scope.schedulefile = $scope.companyDetails.class_schedule;
            $scope.old_file_url = $scope.companyDetails.class_schedule;            
            $scope.old_logo_url = $scope.companyDetails.logo_URL;
            $scope.appcode = $scope.companyDetails.company_code;
            $scope.companyid = $scope.companyDetails.company_id;
            $scope.userid = $scope.companyDetails.user_id;
            $scope.name = $scope.companyDetails.company_name;
            $scope.webpage = $scope.companyDetails.web_page;
            $scope.street = $scope.companyDetails.street_address;
            $scope.city = $scope.companyDetails.city;
            $scope.state = $scope.companyDetails.state;
            $scope.postalcode = $scope.companyDetails.postal_code;
            $scope.country = $scope.companyDetails.country;
            $scope.timezone = $scope.companyDetails.timezone;
            $scope.phone = $scope.companyDetails.phone_number;
            $scope.email = $scope.companyDetails.email_id; 
            $scope.othercompanytype =  $scope.studiotypes.indexOf($scope.companyDetails.company_type);
            $localStorage.regstr_complete = $scope.companyDetails.registration_complete;
            $localStorage.verfication_complete = $scope.companyDetails.verification_complete;
            if(  $scope.othercompanytype !== -1 || $scope.companyDetails.company_type === "" || $scope.companyDetails.company_type === undefined){
                $scope.studiotype = $scope.companyDetails.company_type; 
                $scope.otherstudio = false;
            }else if($scope.othercompanytype === -1){
                $scope.otherstudio = true;
                $scope.studiotype = 'Other';
                $scope.otherstudiotype = $scope.companyDetails.company_type; 
            } 
            
            if($localStorage.scroll_to === 'companycode'){
                var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
                if(isChrome){
                    // TO ADJUST VIEW TO TOP FOR CHROME
                    var elmnt = document.getElementById("companycode_sec");
                    elmnt.scrollIntoView(true,{ behavior: 'smooth', block: 'start', inline: 'start' });
                    window.scrollBy(0, -50);  // TO ADJUST VIEW TO TOP FOR OTHER
                }else{
                    window.scrollTo(0,570);   // TO ADJUST VIEW TO TOP FOR OTHER
                }
            }
        } else {
            $location.path('/login');
        }

    }
    module.exports = MembersController;

