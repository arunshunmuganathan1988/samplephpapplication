CustomerController.$inject=['$scope','$compile', '$location','$http', '$localStorage', 'urlservice', '$filter', '$rootScope','$route', 'DTOptionsBuilder', 'DTColumnDefBuilder','DTColumnBuilder','DTDefaultOptions','attendanceFilterDetails','$window'];
function CustomerController($scope,$compile, $location, $http, $localStorage, urlservice, $filter, $rootScope,$route, DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder,DTDefaultOptions,attendanceFilterDetails,$window)
    {

        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'membership';
            tooltip.hide();
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
//             $scope.init_table_Show = 2;
//            $scope.dtOptions = DTOptionsBuilder.newOptions()
//                    .withOption('scrollX', '100%')
//                    .withOption('order', [])
//                    .withOption('scrollY', '')
//                    .withOption('paging', false)
//                    .withOption('info', false);
////            $scope.dtColumnDefs = [
////                DTColumnDefBuilder.newColumnDef([6, 10, 10, 12]).withOption('type', 'date')
//                 $scope.dtColumn = [
//                    DTColumnBuilder.newColumn('id').withTitle('ID'),
//                    DTColumnBuilder.newColumn('firstName').withTitle('First name')
//                ];
//            ];

            $scope.membershiplistcontents = [];
            $scope.selectedData = [];
            $scope.selected = {};
            $scope.selectAll = false;
                $scope.all_select_flag='N';
                  $scope.color_green='N';
//            vm.toggleAll = toggleAll;
//            vm.toggleOne = toggleOne;
             
         
          
           $scope.getcustomers_list = function(tab){
                $scope.current_tab=tab;
                //                var vm = this;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
                $scope.selectAll = false;
                if ($scope.current_tab === 'R') {
                   $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            url: urlservice.url + 'Customers',
                            xhrFields: {
                                withCredentials: true
                             },
                            dataSrc: function (json) {
                                $scope.membershiplistcontents = json.data;
                                $scope.liveviewcount = json.live_count;
                                $scope.draftviewcount = json.draft_count;
                                $scope.membershipcatergory = json.mem_details;
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                "company_id": $localStorage.company_id,
                                "membership_status": $scope.current_tab,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            }
                           
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
                          

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [14, 'desc'])
                  
                    $scope.dtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else {
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  '<a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="showName(\'' + full.id + '\' ,' + "'active'" + ');">' +full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col3'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col4').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col5'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col6'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col7').renderWith(
                                function (data, type, full) {
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col8').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        
                        DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col9').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('att_req').withTitle('Attendance count').withClass('col10').renderWith(
                                function (data, type, full) {
                                    if ((data == "0" || data == "") && full.act_att > 0)
                                        return full.act_att+' total';
                                    else if(data > 0)
                                        return full.act_att+ ' out of '+ (data) ;
                                    else
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col18'),
                        DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col18'),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col11'),
                        DTColumnBuilder.newColumn('m_rg_ty_us').withTitle('Registration Method').withClass('col12'),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col13').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('nt_py_dt').withTitle('Next Payment Date').withClass('col14'),
                        DTColumnBuilder.newColumn('nt_py_am').withTitle('Next Payment Amount').withClass('col17').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col16').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                        
                         DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                        
                        
                        
                        
                        
                    ];
                   $scope.dtInstance = {};
                } else if ($scope.current_tab === 'P') {
                    $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                url: urlservice.url + 'Customers',
                                xhrFields: {
                                    withCredentials: true
                                 },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    "company_id": $localStorage.company_id,
                                    "membership_status": $scope.current_tab,
                                },
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    }else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                         
                    $scope.dtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col_hold1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="showName(\'' + full.id + '\' ,' + "'onhold'" + ');">' +full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col_hold2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col_hold3'),
                        DTColumnBuilder.newColumn('h_dt').withTitle('Date Placed On Hold').withClass('col_hold4'),
                        DTColumnBuilder.newColumn('r_dt').withTitle('Payment Resume Date').withClass('col_hold5'),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col_hold6'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col_hold7').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col_hold8'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col_hold9'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_hold10').renderWith(
                                function (data, type, full) {
//                                    return '<a  class="buyername"  ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'P'"+ ',' + full.m_id + ',' + full.id +','+'$event'+');">' +full.rank_status + '</a>';
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col_hold11').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_hold12').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_hold13').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                          DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                    ];
                      $scope.dtInstance1 = {};
                }else if($scope.current_tab === 'C'){
                    $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                url: urlservice.url + 'Customers',
                                xhrFields: {
                                    withCredentials: true
                                 },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    "company_id": $localStorage.company_id,
                                    "membership_status": $scope.current_tab,
                                },
                                
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    }else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                    
                    $scope.dtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                   if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  ' <a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col_can1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername"  data-toggle="tooltip" title="' + data + '" ng-click="showName(\'' + full.id + '\' ,' + "'cancelled'" + ');">' + full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col_can2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('m_s').withTitle('Status').withClass('col_can3').renderWith(
                                function (data, type, full) {
                                    if (data === 'C')
                                        return 'Cancelled';
                                    else
                                        return  'Completed';
                                }),,
                        DTColumnBuilder.newColumn('m_rg_co_dt').withTitle('Date').withClass('col_can4'),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col_can5'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col_can6').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col_can7'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col_can8'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_can9'),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col_can10').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_can11').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_can12').notSortable().renderWith(function(data,type,full){
                            return  $scope.wp_currency_symbol +''+data;
                        }),
                    ];
                     $scope.dtInstance2 = {};
                }
                $('#progress-full').hide();
                
                angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    console.log('dataTables search : ' + $scope.searchTerm);
                })
                 
           }
          
             
            
           $scope.toggleAll = function (selectAll, selectedItems) {
                $scope.selectedData = [];
                 $scope.color_green='N';
                 $scope.all_select_flag='N';
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        selectedItems[id] = selectAll;
                        if(selectAll!==false){
//                            $scope.selectedData.push({"id":id});
                            $scope.all_select_flag='Y';
                            $scope.color_green='Y';
                        }
                    }
                }
                $scope.maintainselected = selectedItems;
            };
            
          
            
          
           $scope.toggleOne= function (selectedItems) {
               $scope.maintainselected = selectedItems;
                var check=0;
                  $scope.all_select_flag='N';
                   $scope.color_green='N';
                $scope.selectedData = [];
                for (var id in selectedItems) {
                    if (selectedItems.hasOwnProperty(id)) {
                        if (!selectedItems[id]) {
                            check = 1;
                        }else{
                            $scope.selectedData.push({"id":id});
                             $scope.color_green='Y';
                        }
                         
                    }
                }
                if(check === 1){
                    $scope.selectAll = false;
                }else{
                    $scope.selectAll = true;
                }
            }
           
           $scope.showName=function(full,page) {
                $localStorage.currentMembershipregid = full;
                $localStorage.manage_page_from = "";
                $localStorage.page_from = 'members_'+page;
                $location.path('/membershipdetail');
               
            }   
          
          $scope.showemailerror = function (row, type, data, event, id) {
                $scope.email_updt = event.target;
                $scope.row=row;
                $scope.mem_id = id;
                $scope.btype=type;
                $scope.errormsg = $scope.membershiplistcontents[row].error;
                $scope.bouncedemail = data;
                $("#emailerrormessageModal").modal('show');
                $("#emailerrormessagetitle").text('Message');
                $("#emailerrormessagecontent").text($scope.errormsg);
            };
         

          

            $scope.actionlist = ['Cancel', 'Pause', 'Resume', 'Edit Payment Method', 'Edit Participant Name'];
//            $('#progress-full').show();

            $scope.activeview = true;
            $scope.onholdview = false;
            $scope.cancelview = false;
            $scope.showedit = false;
            $scope.activemembershipdays = '5';
            $scope.editIndex = "";
            $scope.editSingle = $scope.parfirstname = $scope.parlastname = "";
            $scope.membership_reg_id = "";
            $localStorage.currentpage = "customers";
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.pushmsgdata = '';
            $scope.activestartdate = '';
            $scope.holdstartdate = '';
            $scope.cancelstartdate = '';
            $scope.activeenddate = '';
            $scope.holdenddate = '';
            $scope.cancelenddate = '';
           
            $scope.also_send_email = false;
            $localStorage.buyerName = "";
            $localStorage.participantName = "";
            $localStorage.currentMembershipregid = ""; 
            $scope.maxEmailAttachmentSizeError = false;
            $scope.filepreview = ""; 
            $scope.attachfile = '';
            $scope.file_name = '';
            $scope.fileurl = '';
            $scope.data_loaded_length = 0;
            $scope.rankupdatestatus = false;
            $scope.registertypeselection='1';
            $scope.registertypechangeshow = true;
            $scope.registertypechangeshowattended=false;
             $scope.registertypechangeshowattended1 = false;
            
          
            
            
            $scope.removeAttachfile = function () {
                $scope.maxEmailAttachmentSizeError = false;
                $scope.filepreview = ""; 
                $scope.attachfile = '';
                $scope.file_name = '';
                $scope.fileurl = '';
            };
           
           
            $scope.registertypechange = function () {
                $scope.registertype=this.registertypeselection;
                if (this.registertypeselection === '1') {
                    $scope.registertypechangeshow = true;
                    $scope.registertypechangeshowattended = false;
                    $scope.registertypechangeshowattended1 = false;
                    $scope.getcustomers_list('R');
                } else if (this.registertypeselection === '2') {
                    $scope.notattended1='';
                    $scope.notattended = '';
                    $scope.registertypechangeshow = false;
                    $scope.showtextbefore = " to ";
                    $scope.showtextafter = " days ago "
                    $scope.registertypechangeshowattended = true;
                    $scope.registertypechangeshowattended1 = false;
                } else if (this.registertypeselection === '3') {
                    $scope.notattenedday = 2;
                    $scope.notattended1='14';
                    $scope.notattenedday1 = this.notattended1;
                    $scope.registertypechangeshow = false;
                    $scope.showtextbefore = "2 or less class(es) in the last ";
                    $scope.showtextafter = " days "
                    $scope.registertypechangeshowattended1 = true;
                    $scope.registertypechangeshowattended = false;                   
                    $scope.getCustomerdetailsbyFilter('R','',3);
                }
            };
            
            $scope.filterattend = function (value, daystyp) {
                $scope.notattenedday = this.notattended;
                $scope.notattenedday1 = this.notattended1;
                $scope.registertype=daystyp;
                $scope.getCustomerdetailsbyFilter(value, '', '')
            };

            $scope.clearfilterattend = function () {
//                $scope.notattenedday = this.notattended = '';
//                $scope.notattenedday1 = this.notattended1 = '';
//                $scope.registertypeselection = '1';
//                $scope.registertypechangeshow = true;
//                $scope.registertypechangeshowattended = false;
//                $scope.registertypechangeshowattended1 = false;
//                $scope.getcustomers_list(value);
                    $route.reload();
            };

            $scope.lastattenedclasschange = function (value, daystyp) {
                $scope.notattenedday = 2;
                $scope.notattenedday1 = this.notattended1;
                $scope.registertype=daystyp;
                $scope.getCustomerdetailsbyFilter(value, '', '');
            };
            
            $scope.exportcheckedmembersexcel = function () {
             function formatTodayDate() {
                    var d = new Date,
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
//                 angular.element('body').on('search.dt', function () {
//                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
//                    console.log('dataTables search : ' + $scope.searchTerm);
//                })
                $scope.today_date = formatTodayDate();
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'createMemSelectonCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "selected_membership_list": $scope.selectedData,
                        "all_select_flag": $scope.all_select_flag,
                        "membership_status": $scope.current_tab,
                        "membership_days_type": $scope.active_membership_daystype,
                        "membership_startdate_limit": $scope.membership_startdate_limit,
                        "membership_enddate_limit": $scope.membership_enddate_limit,
                        "register_type": $scope.registertype,
                        "notattendeddays": $scope.notattenedday,
                        "notattendeddays1": $scope.notattenedday1,
                        "search":$scope.searchTerm
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    $scope.clearSelectedList();
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') { 
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.msg);                        
                        return false;
                    } else {
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text("Participant Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "Membership_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $scope.clearSelectedList();
                    $('#progress-full').hide();
                    $("#cusmessageModal").modal('show');
                    $("#cusmessagetitle").text('Message');
                    $("#cusmessagecontent").text(msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
                });
            };
            
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
            
            $scope.sendindividualpush = function (selectedMember, type) { // INDIVIDUAL PUSH MESSAGE
                console.log("test");
                $scope.clearfromindividual = true;
                $scope.message_type = type;
                $scope.selectAll = false;
                $scope.toggleAll(false,$scope.maintainselected);
                $scope.selectedData = [];
//                $scope.clearSelectedList();
                $scope.selectedData.push({"id":selectedMember});
                $scope.grpbuttonText = "Send"; 
                if (type === 'push') {
                    $("#grppushmessagecontModal").modal('show');
                    $("#grppushmessageconttitle").text('Send Push Message');
                } else {
                    $("#grpemailmessagecontModal").modal('show');
                    $("#grpemailmessageconttitle").text('Send Email');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            };
            
            $scope.sendGroupMsg = function (type) { 
                $scope.message_type = type;
                $scope.grpbuttonText = "Send";
                if ($scope.selectedData.length > 0 || $scope.all_select_flag==='Y') {
                    if ($scope.message_type === 'push') {
//                        for(var j = 0; j < $scope.selectedData.length; j++){
//                            for (var i = 0; i < $scope.membershiplistcontents.length; i++) {
//                                if ($scope.membershiplistcontents[i].id === $scope.selectedData[j].id && $scope.membershiplistcontents[i].show_icon === 'N') {
//                                    $("#cusmessageModal").modal('show');
//                                    $("#cusmessagetitle").text('Message');
//                                    $("#cusmessagecontent").text('One of the selected member doesnot have registered mobile number to receive push message. Kindly deselect the member who didnot registered mobile number.');
//                                    return;
//                                } else {
//                                    //SELECTED MEMBER HAS MOBILE DEVICE
//                                }
//                            }
//                        }
                        $("#grppushmessagecontModal").modal('show');
                        $("#grppushmessageconttitle").text('Send Push Message');
                    } else if ($scope.message_type === 'mail') {
                        $("#grpemailmessagecontModal").modal('show');
                        $("#grpemailmessageconttitle").text('Send Email');
                        $("#message_content_1_desclink").hide();
                        $("#message_content_1_maillink").show();
                    } else if ($scope.message_type === 'csv') {
                        $("#grpcsvdownloadModal").modal('show');
                        $("#grpcsvdownloadtitle").text('Message');
                        $("#grpcsvdownloadcontent").text('Do you want to download csv file for selected members?');
                    }
                } else {
                    var x = document.getElementById("snackbar")
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                }
            }

            $scope.sendGroupEmailPush = function () {
                $('#progress-full').show();
//                angular.element('body').on('search.dt', function () {
//                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
//                    console.log('dataTables search : ' + $scope.searchTerm);
//                })
               
                var msg,unsubscribe_email = '';
                var subject = '';
                if ($scope.message_type === 'push') {
                    if ($scope.grppushmsgdata.trim().length === 0) {
                        return;
                    }
                    subject = '';
                    msg = $scope.grppushmsgdata;
                } else if ($scope.message_type === 'mail') {
                    if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                        return;
                    }
                    subject = $scope.grpemailsubjectdata;
                    msg = $scope.grpemailmsgdata;
                    
                    if($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null){
                        $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                        $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                    }else{
                        $scope.fileurl = '';
                        $scope.file_name = '';
                    }
                }else if($scope.message_type === 'subscribe'){
                    subject = 'Subscribe to our email communications!';
                    msg = 'click here to subscribe';
                    unsubscribe_email = $scope.unsubscribe_email;
                }
                if($scope.current_tab=='R'){
                    
                }else if($scope.current_tab==='P'){
                    
                }else if($scope.current_tab==='C'){
                    
                }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'sendMembershipGroupPush',
                    data: {
                        "company_id": $localStorage.company_id,
                        "type": $scope.message_type,
                        "subject": subject,
                        "message": msg,
                        "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                        "selected_membership_list": $scope.selectedData,
                        "file_name": $scope.file_name,
                        "attached_file": $scope.fileurl,
                        "all_select_flag": $scope.all_select_flag,
                        "membership_status": $scope.current_tab,
                        "membership_days_type": $scope.membership_daystype,
                        "membership_startdate_limit": $scope.membership_startdate_limit,
                        "membership_enddate_limit": $scope.membership_enddate_limit,
                         "register_type": $scope.registertype,
                        "notattendeddays": $scope.notattenedday,
                        "notattendeddays1": $scope.notattenedday1,
                        "from":"homeMebership",
                        "search":$scope.searchTerm,
                        "unsubscribe_email":unsubscribe_email
                        

                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') { 
                        $("#sendsubscriptioninviteModal").modal('hide');
                        $scope.clearSelectedList();
                        $('#progress-full').hide();
                        $("#grpemailmessagecontModal").modal('hide');
                        $("#grpemailmessageconttitle").text('');
                        $("#grppushmessagecontModal").modal('hide');
                        $("#grppushmessageconttitle").text('');
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg);
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else{
                        $("#sendsubscriptioninviteModal").modal('hide');
                        console.log(response.data);
                        $scope.clearSelectedList();
                        $("#grpemailmessagecontModal").modal('hide');
                        $("#grpemailmessageconttitle").text('');
                        $("#grppushmessagecontModal").modal('hide');
                        $("#grppushmessageconttitle").text('');
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.active = function () {
//                 $scope.customer_array_show_all=$scope.customer_array_show_r_thirty= $scope.customer_array_show_r_sixty=$scope.customer_array_show_r_ninty=$scope.customer_array_show_r_range=$scope.customer_array_show_r=[];
                $scope.change_made = $scope.search_value = '';
                $scope.activemembershipdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
//                $scope.dtInstance.rerender()
                $scope.getcustomers_list('R');
                $scope.activeview = true;
                $scope.onholdview = false;
                $scope.cancelview = false;
                $scope.activestartdate = '';
                $scope.activeenddate = '';
                $scope.color_green='N';
                $scope.resettabbarclass();
               $('#li-active').addClass('active-event-list').removeClass('event-list-title');
                $scope.registertypeselection = '1';
                 $scope.registertypechangeshow = true;
                $scope.registertypechangeshowattended = false;
                $scope.registertypechangeshowattended1 = false;
                $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
          };
          
            $scope.onhold = function () {
                $scope.change_made = $scope.search_value = '';
                $scope.holdmembershipdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.getcustomers_list('P');
                $scope.onholdview = true;
                $scope.activeview = false;
                $scope.cancelview = false;
                $scope.holdstartdate = '';
                $scope.holdenddate = '';
                $scope.color_green='N';
                $scope.resettabbarclass();
               $('#li-onhold').addClass('active-event-list').removeClass('event-list-title');
               $scope.dtOptions1
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
               
          };
          
            $scope.cancel = function () {
                $scope.change_made = $scope.search_value = '';
                $scope.cancelmembershipdays = '5';
                $scope.selected = {};
                $scope.selectAll = false;
                $scope.selectedData = [];
                $scope.getcustomers_list('C');
                $scope.cancelview = true;
                $scope.activeview = false;
                $scope.onholdview = false;
                $scope.cancelstartdate = '';
                $scope.cancelenddate = '';
                $scope.color_green = 'N';
                $scope.resettabbarclass();
                $('#li-cancel').addClass('active-event-list').removeClass('event-list-title');
                $scope.dtOptions2
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                
           };
          
           $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('#li-customers').addClass('active-event-list').removeClass('event-list-title');
            };

             $scope.reloadData=function(cur_tab){
                 $scope.refresh_tab=cur_tab;
             }
            
            $scope.getRankDetails = function(rank_id, current_tab,m_id,m_rg_id,event){
                $scope.event_compile=event;
                $scope.evntpara=angular.element(event.target);
               if(current_tab=='R'){
                    $scope.las_adv= angular.element(event.target).parent().next('.col8')
               }else if(current_tab=='P'){
                    $scope.las_adv= angular.element(event.target).parent().next('.col_hold11');
               }
                $scope.event_Rank_updt=event.target;
                 $scope.m_id=m_id;
                 $scope.membership_reg_id=m_rg_id
                $('#progress-full').show();
                $scope.rank_array = [];
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getAllRankDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "membership_id": $scope.m_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                   
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.rank_array = response.data.msg;
                        $('#progress-full').hide();
                        $scope.existing_rank_array = [];
                        $scope.existing_rank_id = '';
                        $scope.current_tab = current_tab;
                        $scope.existing_rank_id = rank_id;
                        $scope.mem_status = current_tab;
                        for (var j = 0; j < $scope.rank_array.length; j++) {
                            $scope.individual_rank = {};
                            if ($scope.rank_array[j].membership_id ==  $scope.m_id) {
                                $scope.individual_rank['membership_rank_id'] = $scope.rank_array[j].membership_rank_id;
                                $scope.individual_rank['rank_name'] = $scope.rank_array[j].rank_name;
                                $scope.individual_rank['membership_id'] = $scope.rank_array[j].membership_id;
                                $scope.existing_rank_array.push($scope.individual_rank);
                            }
                        }
                        if($scope.mem_status === 'R' || $scope.mem_status === 'P'){
                            $("#updateRankModal").modal('show');
                            $("#updateRanktitle").text('Update Rank Details');
                        }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };            
           
             $scope.$on('$viewContentLoaded', function () {
                $('#amemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#amemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
              $scope.$on('$viewContentLoaded', function () {
                $('#hmemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#hmemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
            
            $scope.$on('$viewContentLoaded', function () {
                $('#cmemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });               
            });
            
             $scope.$on('$viewContentLoaded', function () {
                $('#cmemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });                
            });
            
            
            $scope.dateformatfilter = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            };   
            
            $scope.dayselection = function (value) {
//                $scope.clearSelectedList();
                var daystype;
                this.activestartdate = '';
                this.activeenddate = '';
                this.holdstartdate = '';
                this.holdenddate = '';
                this.cancelstartdate = '';
                this.cancelenddate = '';
                
                if(value === 'R'){
                    daystype= this.activemembershipdays;
                }else if(value === 'P'){
                    daystype= this.holdmembershipdays;
                }else{
                    daystype= this.cancelmembershipdays;
                }
                if (daystype === '1' || daystype === '2' || daystype === '3' || daystype === '5') {
                    $scope.clearDateFields();
                    $scope.filterByDate(value,daystype);
                } else {
                    $scope.clearDateFields();
                    // DATE RANGE SELECTION FILTER
                }
            }; 

            $scope.clearSelectedList = function () {
                $scope.removeAttachfile();
                $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
                $scope.also_send_email = false;
                $scope.mem_status='';
                if($scope.clearfromindividual){//clear if modal open from individual
                   $scope.selectedData = [];
                }
               $scope.clearfromindividual = false;
            }; 
            
            $scope.filterByDate = function (value,daystype) {
                $scope.current_tab = value;
                $scope.customer_array = [];
                $scope.rank_array = [];
                var current_tab_id = '';
               
               $('#progress-full').show();
                if (value === 'R') { // FILTERS DATA FOR ACTIVE TAB //
                    value = 'R';
                    $scope.active_membership_daystype = daystype;
                    if ($scope.active_membership_daystype === '4') {
                        if (this.activestartdate === '' || this.activestartdate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_startdate_limit = $scope.dateformatfilter(this.activestartdate);
                        }

                        if (this.activeenddate === '' || this.activeenddate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_enddate_limit = $scope.dateformatfilter(this.activeenddate);
                        }
                    } else {
                        $scope.membership_startdate_limit = '';
                        $scope.membership_enddate_limit = '';
                    }
                    current_tab_id = 'customer_table_active';
                }

                if (value === 'P') { // FILTERS DATA FOR ON HOLD TAB //
                    value = 'P';
//                    alert(daystype);
                    $scope.hold_membership_daystype = daystype;
//                    alert( $scope.hold_membership_daystype);
                    if ($scope.hold_membership_daystype === '4') {
                        if (this.holdstartdate === '' || this.holdstartdate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_startdate_limit = $scope.dateformatfilter(this.holdstartdate);
                        }

                        if (this.holdenddate === '' || this.holdenddate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_enddate_limit = $scope.dateformatfilter(this.holdenddate);
                        }
                    } else {
                        $scope.membership_startdate_limit = '';
                        $scope.membership_enddate_limit = '';
                    }
                     current_tab_id = 'customer_table_onhold';
                }
                if (value === 'C') { // FILTERS DATA FOR CANCELLED TAB //
                    value = 'C';
                    $scope.cancel_membership_daystype = daystype;

                    if ($scope.cancel_membership_daystype === '4') {
                        if (this.cancelstartdate === '' || this.cancelstartdate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_startdate_limit = $scope.dateformatfilter(this.cancelstartdate);
                        }

                        if (this.cancelenddate === '' || this.cancelenddate === undefined) {
                            $scope.membership_startdate_limit = '';
                            $scope.membership_enddate_limit = '';
                        } else {
                            $scope.membership_enddate_limit = $scope.dateformatfilter(this.cancelenddate);
                        }
                    } else {
                        $scope.membership_startdate_limit = '';
                        $scope.membership_enddate_limit = '';
                    }
                     current_tab_id = 'customer_table_cancelled';
                }

                $scope.getCustomerdetailsbyFilter(value,current_tab_id,daystype);
            };
            
            $scope.getCustomerdetailsbyFilter = function (value, current_tab_id, daystype) {
              $scope.membership_daystype=daystype;   
                $('#progress-full').show();
               $scope.current_tab=value;
//                var vm = this;
                DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)">';
                var phone ='<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>'; 
                $scope.selected = {};
            $scope.selectAll = false;
                if ($scope.current_tab === 'R') {
                   $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            url: urlservice.url + 'Customers',
                            xhrFields: {
                                withCredentials: true
                             },
                            dataSrc: function (json) {
                                $scope.membershiplistcontents = json.data;
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                "company_id": $localStorage.company_id,
                                "membership_status": $scope.current_tab,
                                "membership_days_type": $scope.membership_daystype,
                                "membership_startdate_limit": $scope.membership_startdate_limit,
                                "membership_enddate_limit": $scope.membership_enddate_limit,
                                "register_type": $scope.registertype,
                                "notattendeddays": $scope.notattenedday,
                                "notattendeddays1": $scope.notattenedday1
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();  
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            }
                           
                        })
                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)                        
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [14, 'desc'])
                    $scope.dtColumns = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  '<a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="showName(\'' + full.id + '\');">' + full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col3'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col4').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col5'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col6'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col7').renderWith(
                                function (data, type, full) {
//                                    return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id +','+'$event'+');">' + full.rank_status + '</a>';
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col8').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                                DTColumnBuilder.newColumn('l_att').withTitle('Last Attendance').withClass('col9').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('att_req').withTitle('Attendance count').withClass('col10').notSortable().renderWith(
                                function (data, type, full) {
                                    if ((data == "0" || data == "") && full.act_att > 0)
                                        return full.act_att+' total';
                                    else if(data > 0)
                                        return full.act_att+ ' out of '+ (data) ;
                                    else
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('14_att').withTitle('Attendance Last 14 Days').withClass('col18'),
                        DTColumnBuilder.newColumn('30_att').withTitle('Attendance Last 30 Days').withClass('col18'),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col11'),
                        DTColumnBuilder.newColumn('m_rg_ty_us').withTitle('Registration Method').withClass('col12'),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col13').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('nt_py_dt').withTitle('Next Payment Date').withClass('col14'),
                        DTColumnBuilder.newColumn('nt_py_am').withTitle('Next Payment Amount').withClass('col17').notSortable(),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col16').notSortable(),
                          DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                    ];
                } else if ($scope.current_tab == 'P') {
                    $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                url: urlservice.url + 'Customers',
                                xhrFields: {
                                    withCredentials: true
                                 },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    "company_id": $localStorage.company_id,
                                    "membership_status": $scope.current_tab,
                                    "membership_days_type": $scope.hold_membership_daystype,
                                    "membership_startdate_limit": $scope.membership_startdate_limit,
                                    "membership_enddate_limit": $scope.membership_enddate_limit
                                },                                
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    }else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                          
                    $scope.dtColumns1 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                    if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  '<a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col_hold1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="showName(\'' + full.id + '\');">' + full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col_hold2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('created_dt').withTitle('Registration Date').withClass('col_hold3'),
                        DTColumnBuilder.newColumn('h_dt').withTitle('Date Placed On Hold').withClass('col_hold4'),
                        DTColumnBuilder.newColumn('r_dt').withTitle('Payment Resume Date').withClass('col_hold5'),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col_hold6'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col_hold7').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col_hold8'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col_hold9'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_hold10').renderWith(
                                function (data, type, full) {
//                                    return '<a  class="buyername"  ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'P'" + ',' + full.m_id + ',' + full.id +','+'$event'+');">' + full.rank_status + '</a>';
                                    if (data == "") {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + "None" + '</a>';
                                    } else {
                                        return '<a class="buyername" ng-click="getRankDetails(\'' + full.rank_id + '\' ,' + "'R'" + ',' + full.m_id + ',' + full.id + ',' + '$event' + ');">' + full.rank_status + '</a>';
                                    }
                                }),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col_hold11').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_hold12').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_hold13').notSortable(),
                          DTColumnBuilder.newColumn('t').withTitle('End Date').withClass('col16').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.end_date + '</span>';
                                }),
                    ];
                }else if($scope.current_tab === 'C'){
                 
                    $scope.dtOptions2 = DTOptionsBuilder.newOptions()
                            .withOption('ajax', {
                                url: urlservice.url + 'Customers',
                                xhrFields: {
                                    withCredentials: true
                                 },
                                dataSrc: function (json) {
                                    $scope.membershiplistcontents = json.data;
                                    return json.data;
                                },
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    "company_id": $localStorage.company_id,
                                    "membership_status": $scope.current_tab,
                                    "membership_days_type": $scope.cancel_membership_daystype,
                                    "membership_startdate_limit": $scope.membership_startdate_limit,
                                    "membership_enddate_limit": $scope.membership_enddate_limit
                                },                                
                                error: function (xhr, error, thrown) {
                                    if (xhr.responseJSON.status === "Failed") {
                                        $('#progress-full').hide();
                                        $("#pushmessageModal").modal('show');
                                        $("#pushmessagetitle").text('Message');
                                        $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                    }else if(xhr.responseJSON.status === 'Expired'){
                                        $('#progress-full').hide();
                                        $("#messageModal").modal('show');
                                        $("#messagetitle").text('Message');
                                        $("#messagecontent").text(xhr.responseJSON.msg);
                                        $scope.logout();  
                                    }else if (xhr.responseJSON.status === 'Version') {
                                        $('#progress-full').hide();
                                        $scope.handleFailure(xhr.responseJSON.msg);
                                    }else{
                                        $('#progress-full').hide();
                                    }
                                }
                            })

                            .withOption('createdRow', function (row, data, dataIndex) {
                                // Recompiling so we can bind Angular directive to the DT
                                $compile(angular.element(row).contents())($scope);
                            })
                            .withOption('headerCallback', function (header) {
                                // Use this headerCompiled field to only compile header once
                                $scope.headerCompiled = true;
                                $compile(angular.element(header).contents())($scope);
                            })

                            .withDataProp('data')
                            .withDOM('lfrti')
                            .withScroller()
                            .withOption('deferRender', true)
                            .withOption('scrollY', 350)
                            .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                            .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                            .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                            .withOption('serverSide', true)
                            .withOption('search', true)
                            .withOption('info', false)
                            .withOption('stateSave',true)
                            .withOption('stateDuration', 60*5)
                            .withOption('order', [5, 'desc'])
                    
                    $scope.dtColumns2 = [
                        DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                                .renderWith(function (data, type, full, meta) {
                                   if($scope.selectAll==true){
                                         $scope.selected[full.id] = true;
                                    }else{
                                        $scope.selected[full.id] = false;
                                        for (var id in  $scope.selected) {
                                            if ($scope.selectedData.length > 0) {
                                                for (var x = 0; x < $scope.selectedData.length; x++) {
                                                    if (id === $scope.selectedData[x].id) {
                                                        $scope.selected[id] = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                                }),
                        DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                                function (data, type, full) {
                                    if (data === 'Y')
                                        return  '<a ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'push'" + ');">' + phone + '</a>';
                                    else
                                        return "";
                                }).notSortable(),
                        DTColumnBuilder.newColumn('b_na').withTitle('Buyer').withClass('col_can1').renderWith(
                                function (data, type, full) {
                                    return '<a class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="showName(\'' + full.id + '\');">' + full.b_na + '</a>';
                                }),
                        DTColumnBuilder.newColumn('p_na').withTitle('Participant').withClass('col_can2').renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.p_na + '</span>';
                                }),
                        DTColumnBuilder.newColumn('m_s').withTitle('Status').withClass('col_can3').renderWith(
                                function (data, type, full) {
                                    if (data == 'C')
                                        return 'Cancelled';
                                    else
                                        return  'Completed';
                                }),,
                        DTColumnBuilder.newColumn('m_rg_co_dt').withTitle('Date').withClass('col_can4'),
                        DTColumnBuilder.newColumn('b_ph').withTitle('Phone').withClass('col_can5'),
                        DTColumnBuilder.newColumn('b_em').withTitle('Email').withClass('col_can6').renderWith(
                                function (data, type, full,meta) {
                                    if(full.bounce_flag==='N'){
                                        
                                        if (full.email_subscription_status === 'U') {
                                            return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.b_em + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.b_em + '</a>';
                                        }else{
                                            return '<a  class="buyername" data-toggle="tooltip" title="' + data + '" ng-click="sendindividualpush(\'' + full.id + '\' ,' + "'mail'" + ');">' +full.b_em + '</a>';
                                        }
                                        
                                         
                                    }else{
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '" ng-click="showemailerror(\'' + meta.row + '\',\''+ full.bounce_flag +'\',\''+ data +'\','+'$event'+',\''+full.id+'\');"><img src="image/mail_error.png"  width="20" height="20">' +"(" + full.type +") "+full.b_em + '</a>'; 
                                  
                                    }
                                }),
                        DTColumnBuilder.newColumn('p_b_dt').withTitle('Birthday').withClass('col_can7'),
                        DTColumnBuilder.newColumn('p_age').withTitle('Age').withClass('col_can8'),
                        DTColumnBuilder.newColumn('rank_status').withTitle('Rank').withClass('col_can9'),
                        DTColumnBuilder.newColumn('l_adv').withTitle('Last Advanced').withClass('col_can10').renderWith(
                                function (data, type, full) {
                                    if (data == '0' || data == 0)
                                        return 'Today';
                                    else if (data == '1' || data == 1)
                                        return (data) + ' day ago';
                                    else if (parseInt(data) > 1)
                                        return (data) + ' days ago';
                                    else 
                                        return '';
                                }),
                        DTColumnBuilder.newColumn('t').withTitle('Membership').withClass('col_can11').notSortable().renderWith(
                                function (data, type, full) {
                                    return '<span  data-toggle="tooltip" title="' + data + '" >' + full.t + '</span>';
                                }),
                        DTColumnBuilder.newColumn('past_due').withTitle('Bill Past Due').withClass('col_can12').notSortable(),
                    ];
                }
                 $scope.color_green='N';
               $('#progress-full').hide();
               angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                    console.log('dataTables search : ' + $scope.searchTerm);
                })
            };
            
            
            $scope.unblockbounceemail = function () {
                $('#progress-full').show();
                 $("#emailerrormessageModal").modal('hide');
                $http({
                    method: 'POST',
                   url: urlservice.url + 'unblockbounceemail',
                   data: {
                        "bounced_email": $scope.bouncedemail,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
//                      angular.element($scope.email_updt).text($scope.bouncedemail);
                        angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendindividualpush(\'' + $scope.mem_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
//                      angular.element($scope.email_updt).attr('ng-click',"sendindividualpush('"+$scope.mem_id+"','email')");
                        $compile(angular.element(document.getElementById('DataTables_Table_0')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_1')).contents())($scope);
                        $compile(angular.element(document.getElementById('DataTables_Table_2')).contents())($scope);
                        $('#progress-full').hide();
                        $("#cusmessageModal").modal('show');
                        $("#cusmessagetitle").text('Message');
                        $("#cusmessagecontent").text('Student email unblocked successfully ');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
                  $('#progress-full').hide();
            };
            
            $scope.clearfilter = function (type) { // CLEAR THE APPLIED FILTER //                
                    
                if (type === 'R') {
                    this.activestartdate = '';
                    this.activeenddate = '';
                    this.activemembershipdays = '5';
                    $scope.active();
                } else if (type === 'P') {
                    this.holdstartdate = '';
                    this.holdenddate = '';
                    this.holdmembershipdays = '5';
                    $scope.onhold();
                } else if (type === 'C') {
                    this.cancelstartdate = '';
                    this.cancelenddate = '';
                    this.cancelmembershipdays = '5';
                    $scope.cancel();
//                   
                } 
                $scope.clearSelectedList();
            };
            
            $scope.last_advncedays_count = function(date) {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                  dd = '0' + dd
                }
                if (mm < 10) {
                  mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                $scope.today = today;
                var date2 = new Date(today);
                var date1 = new Date(date);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if($scope.dayDifference == 0){
                    return "Today";
                }else{
                    return $scope.dayDifference+" days ago";
                }
            }
            
            $scope.showMessage = function () {
                $("#infomessageModal").modal('show');
                $("#infomessagetitle").text('Buyer Info');
                $("#infomessagecontent").text("Coming soon.  This page will provide the following\n\
                data for the Participant from this Buyer : Previous payment history. Upcoming scheduled payments. \n\
                Events that have been purchased. Total revenue generated from Participant.");
            }
            
            $scope.rankupdateChange = function (membership_rank_id) {
               $scope.rankupdatestatus = false;
               if(membership_rank_id === $scope.existing_rank_id){
                   $scope.rankupdatestatus = false;
               }else{
                   $scope.rankupdatestatus = true;
               }
            };
            
            $scope.updateRankCancel = function () {
                $scope.existing_rank_array = [];
                $scope.existing_rank_id = "";
                $scope.mem_rank_details = "";
                $scope.mem_status = "";
                $scope.new_adv_date = "";
                $scope.lst_adv_date = "";
                $scope.rankupdatestatus = false;
            };            

            $scope.showRankEditConfirmModel = function (membership_rank_id) { // SHOW RANK EDIT FORM
                $scope.rank_id='';
                $scope.rank_name ='';
                for (var j = 0; j < $scope.rank_array.length; j++) {
                    if ($scope.rank_array[j].membership_rank_id === membership_rank_id) {
                        $scope.rank_id = membership_rank_id;
                        $scope.rank_name = $scope.rank_array[j].rank_name;
                    }
                }
                $("#rankActionModal").modal('show');
                $("#rankActiontitle").text('Confirmation');
                $("#rankActioncontent").text("Are you sure want to Update Membership Rank?");
            };

            $scope.lastAdvancedDateUpdate = function () { // SHOW LAST ADVANCED CHANGES  
                $scope.new_adv_date = "";
                $scope.lst_adv_date = "";
                $('#lstadvdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
                $('#lstadvdate').datepicker('setEndDate', new Date());
                $("#updateRankModal").modal('hide');
                $("#lastAdvancedDateModal").modal('show');
                $("#lastAdvancedDatetitle").text('Update');
            };
            
            $scope.cancelChange = function () {
                $('#rank_value option').prop('selected', function () {
                    return this.defaultSelected;
                });
            }            

      // ENDING OF EDIT PARTICIPPANT
            $scope.updateRankDetails = function (lst_adv_date) { 
                $scope.new_rank_value = $scope.rank_name;
                $scope.new_adv_date = $scope.dateformatfilter(lst_adv_date);
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateParticipantMembershipRank',
                    data: {
                        "company_id": $localStorage.company_id,
                        "rank_id": $scope.rank_id,
                        "membership_reg_id":  $scope.membership_reg_id,
                        "membership_status": $scope.mem_status,
                        "membership_rank": $scope.new_rank_value,
                        "advance_date": $scope.new_adv_date                       
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        
                       angular.element($scope.event_Rank_updt).text($scope.rank_name);
                       angular.element($scope.event_Rank_updt).attr('ng-click',"getRankDetails('"+$scope.rank_id+"','"+$scope.current_tab+"',"+$scope.m_id+","+$scope.membership_reg_id+",$event)");
                       $scope.las_adv.text($scope.last_advncedays_count($scope.new_adv_date));
                       $compile(angular.element(document.getElementById('DataTables_Table_0')).contents())($scope);
                       $compile(angular.element(document.getElementById('DataTables_Table_1')).contents())($scope);
                       $compile(angular.element(document.getElementById('DataTables_Table_2')).contents())($scope);
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text('Membership Rank successfully updated.');
                        $scope.rank_id = '';
                        $scope.rank_name = '';
                        $scope.new_adv_date = "";
                        $scope.lst_adv_date = "";
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#pushmessageModal").modal('show');
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };

            $scope.dateformat = function (date) {
                if(date){
                    $scope.datefor = date.split(" ");
                    return  $scope.datefor[0];
                }
            };
            
            $scope.membershipDetail = function (buyername, participantname, membership_reg_id, current_tab) {
                $localStorage.buyerName = buyername;
                $localStorage.participantName = participantname; 
                $localStorage.currentMembershipregid = membership_reg_id;
                $localStorage.manage_page_from = "";
                $localStorage.page_from = 'members_'+current_tab;
                $location.path('/membershipdetail');
            };
            

            $scope.getCurrentdetails = function (current_tab) {
               if(current_tab === 'R'){
                   $scope.active(); 
               }else if(current_tab === 'P'){
                    $scope.onhold(); 
               }else if(current_tab === 'C'){
                   $scope.cancel();
               }
            };
            
            
            $scope.catchAStartDate = function(){
                if($scope.activestartdate !== ''){
                    $('#amemenddate').datepicker('setStartDate', $scope.activestartdate);
                }else{
                    $('#amemenddate').datepicker('setStartDate', null);
                    $scope.activeenddate = '';
                }
            };
            
            $scope.catchAEndDate = function(){
                if($scope.activeenddate !== ''){
                    $('#amemstartdate').datepicker('setEndDate', $scope.activeenddate);
                }else{
                    $('#amemstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchPStartDate = function(){
                if($scope.holdstartdate !== ''){
                    $('#hmemenddate').datepicker('setStartDate', $scope.holdstartdate);
                }else{
                    $('#hmemenddate').datepicker('setStartDate', null);
                    $scope.holdenddate = '';
                }
            };
            
            $scope.catchPEndDate = function(){
                if($scope.holdenddate !== ''){
                    $('#hmemstartdate').datepicker('setEndDate', $scope.holdenddate);
                }else{
                    $('#hmemstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.catchCStartDate = function(){
                if($scope.cancelstartdate !== ''){
                    $('#cmemenddate').datepicker('setStartDate', $scope.cancelstartdate);
                }else{
                    $('#cmemenddate').datepicker('setStartDate', null);
                    $scope.cancelenddate = '';
                }
            };
            
            $scope.catchCEndDate = function(){
                if($scope.cancelenddate !== ''){
                    $('#cmemstartdate').datepicker('setEndDate', $scope.cancelenddate);
                }else{
                    $('#cmemstartdate').datepicker('setEndDate', null);
                }
            };
            
            $scope.clearDateFields = function(){
                $scope.activestartdate = $scope.activeenddate = $scope.holdstartdate = $scope.holdenddate = $scope.cancelstartdate = $scope.cancelenddate = '';     
                
                $('#amemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                 $('#amemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#hmemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });
                
                $('#hmemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                });  
                
                $('#cmemstartdate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
                
                $('#cmemenddate').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true
                }); 
            };
            
            $scope.viewTrials = function(){
                $location.path('/trials');
            };
            
           
           $scope.memredirectlivelistview = function(){
//               $localStorage.memredirectto = "L";
                $localStorage.addmembershipfrommemberstab = false;
               $location.path('/managemembership');
           };
           
            $scope.openAddNewMemberModal = function(){
                $('#addnewmember-modal').modal('show');
                $localStorage.addmembershipfrommemberstab = true;
            };
            //for getting membership options
            $scope.catchmembershipoptions = function(ind){
                $scope.membershipoptions = $scope.membershipcatergory[ind].options;
                $localStorage.redirectmembershipid = $scope.membershipcatergory[ind].membership_id;
            };
             $scope.catchmembershipoptionscall = function(id){
                $localStorage.redirectmembershipoptionsid = id;
            };
            $scope.redirectaddmembership = function(){
                $('#addnewmember-modal').modal('hide');
                $('#addnewmember-modal').on('hidden.bs.modal', function (e) {
                   $window.location.href = window.location.origin+window.location.pathname+'#/managemembership';
               })
            };
            
            if ($localStorage.customerdshboard === 'Y') {
                $scope.change_made = $scope.search_value = '';
                $scope.holdmembershipdays = '5';
                $scope.onholdview = true;
                $scope.activeview = false;
                $scope.cancelview = false;
                 $scope.getcustomers_list('P');
                $scope.resettabbarclass();
                $('#li-onhold').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.customerdshboard = 'N';
            }else if ($localStorage.attendancedashboard === 'Y') {
                $scope.change_made = $scope.search_value = '';  
                $scope.clearSelectedList();
                $scope.activeview = true;
                $scope.onholdview = false;
                $scope.cancelview = false;
                $scope.resettabbarclass();  
                $('#li-active').addClass('active-event-list').removeClass('event-list-title');
                $localStorage.attendancedashboard = 'N';
                $scope.attendancefilterdata = attendanceFilterDetails.getattendanceFilter(); 
                $scope.registertypeselection = $scope.attendancefilterdata[0].type;
                if($scope.registertypeselection === '2'){                   
                    $scope.notattended = $scope.attendancefilterdata[0].frequency_from;
                    $scope.notattended1 =$scope.attendancefilterdata[0].frequency_to;
                    $scope.registertypechangeshow = false;
                    $scope.showtextbefore = " to ";
                    $scope.showtextafter = " days ago "
                    $scope.registertypechangeshowattended = true;
                    $scope.registertypechangeshowattended1 = false;
                    $scope.filterattend('R','2');
                }else{
                    $scope.notattended1 = $scope.attendancefilterdata[0].frequency_from;
                    $scope.notattenedday = 2;
                    $scope.notattenedday1 = $scope.notattended1;
                    $scope.registertypechangeshow = false;
                    $scope.showtextbefore = " 2 or less class(es) in the last ";
                    $scope.showtextafter = " days "
                    $scope.registertypechangeshowattended1 = true;
                    $scope.registertypechangeshowattended = false;   
                    $scope.registertype = '3';
                    $scope.getCustomerdetailsbyFilter('R','','');
                }
                
            }  else {
                if ($localStorage.page_from === 'members_active') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.activemembershipdays = '5';
                    $scope.getcustomers_list('R');
                    $scope.clearSelectedList();
                    $scope.activeview = true;
                    $scope.onholdview = false;
                    $scope.cancelview = false;
                    $scope.resettabbarclass();
                    $('#li-active').addClass('active-event-list').removeClass('event-list-title');
                    $localStorage.page_from = '';
                } else if ($localStorage.page_from === 'members_onhold') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.holdmembershipdays = '5';
                    $scope.getcustomers_list('P');
                    $scope.clearSelectedList();
                    $scope.activeview = false;
                    $scope.onholdview = true;
                    $scope.cancelview = false;
                    $scope.resettabbarclass();
                    $('#li-onhold').addClass('active-event-list').removeClass('event-list-title');
                    $localStorage.page_from = '';
                } else if ($localStorage.page_from === 'members_cancelled') {
                    $scope.change_made = $scope.search_value = '';
                    $scope.cancelmembershipdays = '5';
                    $scope.getcustomers_list('C');
                    $scope.clearSelectedList();
                    $scope.activeview = false;
                    $scope.onholdview = false;
                    $scope.cancelview = true;
                    $scope.resettabbarclass();
                    $('#li-cancel').addClass('active-event-list').removeClass('event-list-title');
                    $localStorage.page_from = '';
                } else {
                    $scope.change_made = $scope.search_value = '';
                    $scope.getcustomers_list('R');
                    $scope.dtOptions
                     .withOption('stateLoadParams', function (settings, data) {  data.search.search = ""; })
                    $localStorage.page_from = '';
                }
            }
            $scope.sendSubscribeEmail = function(mail){
                $scope.unsubscribe_email = mail;
                $scope.message_type = 'subscribe';
                $("#sendsubscriptioninviteModal").modal('show');
            };
            
        } else {
            $location.path('/login');
        }

    }
    module.exports =  CustomerController;

