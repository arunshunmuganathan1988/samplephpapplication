AdminpageController.$inject=['$scope','$http','$location', '$route','$localStorage','urlservice','$rootScope'];
function AdminpageController ($scope, $http, $location, $route, $localStorage,urlservice,$rootScope) {

        $rootScope.sideBarHide = true;
        $rootScope.adminsideBarHide = false;
        $rootScope.hidesidebarlogin = true;
        tooltip.hide();
        
   if($localStorage.adminusrname !== "" && $localStorage.adminusrname !== undefined ){
       $scope.adminusername = $localStorage.adminusrname;
       $scope.adminpassword = $localStorage.adminpswdname;
   }
     $scope.adminLogin = function(){
              $('#progress-full').show();
              $http({
                        method: 'POST',
                        url: urlservice.url+'adminlogin',
                        data: {
                            "email":$scope.adminusername,
                            "password":$scope.adminpassword
                        },
                        headers:{"Content-Type":'application/json; charset=utf-8',},
                        withCredentials: true
                        
                    }).then(function (response) {
                        if(response.data.status == 'Success'){
                            $('#progress-full').hide();
                            $localStorage.adminusrname = $scope.adminusername;
                            $localStorage.adminpswdname = $scope.adminpassword;
                            $localStorage.admincompanyid = response.data.company_id;
                            $rootScope.admincmpnynme = response.data.company_name;
                            $localStorage.admincmpnynme = response.data.company_name;
                            $localStorage.isadminlogin = 'Y';
                            $location.path('/adminhome');
                        }else if(response.data.status === 'Expired'){
                            $scope.adminlogout();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text(response.data.msg);
                        }else if (response.data.status === 'Version') {
                            $('#progress-full').hide();
                            $scope.handleFailure(response.data.msg);
                        }else{
                            $('#progress-full').hide();
                             $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text(response.data.msg);
                        }   
                    }, function (response) {
                            $scope.handleAdminError(response.data);
                    });
          };
        
    }
    module.exports =  AdminpageController;