AttendanceDashboardController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$filter','attendanceFilterDetails'];
function AttendanceDashboardController($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter, attendanceFilterDetails)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'attendance';
            $localStorage.currentpage = "attendancedashboard";
            $scope.wp_currency_code = $localStorage.wp_currency_code;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.csvexportperiod = 'W';
            $scope.csv_checked = false;    
            $localStorage.attendancedashboard = 'N';
            $scope.attendance_details = "";

            $scope.AttendanceDashboardTable = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getAttendanceDashboard',
                    data: {
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.attendance_details = response.data.msg.attendance;
                        $scope.trial_attendance_details=response.data.msg.trial_attendance;
                        $scope.attopenurl = response.data.msg.url;
                        $scope.csv_checked = response.data.msg.csv.attendance_report_flag ==='Y' ? true : false;
                        $scope.csvexportperiod = response.data.msg.csv.attendance_report_frequency;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout(); 
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.updateAttendanceReportFrequency = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateAttendanceReportFrequency',
                    data: {
                        "attendance_report_flag": $scope.csv_checked ? "Y":"N",
                        'attendance_report_frequency': $scope.csv_checked ? $scope.csvexportperiod : "W",
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.csv_checked = response.data.report_frequency_details.attendance_report_flag ==='Y' ? true : false;
                        $scope.csvexportperiod = response.data.report_frequency_details.attendance_report_frequency;
                        
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout(); 
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
            
            $scope.getMembershipFilterData = function (type,frequency_from,frequency_to,category) {   
                var filterdata = {};
                filterdata["type"] = type;
                filterdata["frequency_from"] = frequency_from;
                filterdata["frequency_to"] = frequency_to;
                $localStorage.attendancedashboard = 'Y';
                $scope.attendancedashboardfrom = category;
                attendanceFilterDetails.addattendanceFilter(filterdata);
                if($scope.attendancedashboardfrom === 'T'){
                   $location.path('/trials'); 
                }else{
                   $location.path('/customers'); 
                }
                
            };
            
            
            //Attendance Portal URL copied on clipboard
            $scope.copyAttURL = function () { 
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            $scope.attendanceDashboardInfo = function(attinfo){
                var msg;
                if(attinfo === 'publicattendurl'){
                    msg = "<p>This URL is a web link to your attendance check in portal.<p><p>You can launch this from a desktop, laptop, ipad or mobile phone to check students in.</p>";
                }                
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };

            $scope.attendancePortalLink = function () {
                $scope.alleditClosed();
                $scope.menuhide();
                $localStorage.att_company_id = $localStorage.company_id;
                $localStorage.att_user_email_id = $localStorage.user_email_id; 
                $localStorage.membership_id = $localStorage.menuLockStatus = $localStorage.memCategoryTitle = "";
                var hosturl;
                if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                    if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                        var origin = window.location.origin;
                        hosturl = origin;
                    }

                } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'http://' + www_url;
                    }
                }  else if (window.location.hostname === 'beta.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'https://' + www_url;
                    }
                }else if (window.location.hostname === 'stage.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'http://' + www_url;
                    }
                } else if (window.location.hostname === 'localhost') {
                    hosturl = 'http://' + window.location.hostname + attendance_location;
                } else {
                    hosturl = 'http://' + window.location.hostname + attendance_location;
                }
                window.open(hosturl + '/attendance/?=l', '_blank');
            };


        } else {
            $location.path('/login');
        }
    }
    module.exports =  AttendanceDashboardController;

//Transferring attendance filter details value from 
