
AddMembershipController.$inject=['$scope', '$location', '$http', '$localStorage','membershipListDetails', 'urlservice', '$filter', '$route','$timeout','$rootScope'];
 function AddMembershipController ($scope, $location, $http, $localStorage, membershipListDetails, urlservice, $filter, $route, $timeout,$rootScope) {

        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
            angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $scope.message = "MyStudio";
            $rootScope.activepagehighlight = 'membership';
            tooltip.hide();
           
            //PREVIEW MEMBERSHIP ADD
            $localStorage.preview_membershipcontent = '';
            $localStorage.PreviewMembershipCategoryName = '';
            $localStorage.PreviewMembershipSubCategoryName = '';
            $localStorage.PreviewMembershipCategoryImage = '';
            $localStorage.PreviewMembershipCategoryVideoUrl = '';
            $localStorage.PreviewMembershipCategoryDesc = '';
            $localStorage.PreviewMembershipOptionTitle = '';
            $localStorage.PreviewMembershipOptionSubtitle = '';
            $localStorage.PreviewMembershipOptionDesc = '';
            $localStorage.PreviewMembershipWaiver = '';
            $localStorage.PreviewMembershipRegcols = '';
            $localStorage.currentmembershippreviewtab = '';
            $localStorage.childmembershipindex = '';
            $localStorage.preview_membershipchildlist = false; 
            $localStorage.preview_member_childlisthide = false;
            $localStorage.childmembershipoptionadd = false;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.wepaystatus = $localStorage.preview_wepaystatus;
            
            //Scope Initializations
            $scope.membership_id = '';
            $scope.company_id = $localStorage.company_id;
            $scope.discnt_type = $scope.mdiscnt_type = 'V';
            $scope.discount_off = 'M';
            $scope.detailsview = $scope.memoptdetailsview = $scope.showaddedrank = $scope.childmembershipCardView = true;
            $scope.ranklevelsview = $scope.membershipoptionsview = $scope.memoptpaymentview = $scope.msadded = $scope.msoptionadded =false;
            $scope.memoptdiscountcodesview = $scope.memoptregistrationview = $scope.memoptwaiverview = false;
            $scope.showAddRank =  $scope.rankfieldexceeds = $scope.showmembershipoption =  $scope.show_in_app = false;
            $scope.msdiscountListView = $scope.msdiscountEditView = $scope.showregtext = $scope.ms_checked = false;
            $scope.ms_col_view = true;
            $scope.ms_edit_col_view = $scope.ms_regfieldexceeds = false;
            $scope.Memsdesc = {text: ""}
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            $scope.bounds = {};
            $scope.bounds.left = 0;
            $scope.bounds.right = 0;
            $scope.bounds.top = 0;
            $scope.bounds.bottom = 0;
            $scope.croppedCategoryImage = $scope.selectedMemOptIndex = "";
            $scope.memstructure = 'OE';
            $scope.mem_fee_include = 'Y';
            $scope.prorateValue = 'Y';
            $scope.recpayStartdate = 'N';
            $scope.memProcessingfee = '2';
            $scope.mempayfrequency = 'M';            
            $scope.child_membership_options = [];
            
            $scope.memexpiryvalue = 1;
            $scope.billing_depositamount = 0;
            $scope.billing_noofpayment = 1;
            $scope.noofclass = 0;
            $scope.billingoptionvalue = 'PF';
            $scope.memexpiryperiod = 'M';
            $scope.billing_paymentstartdateValue = '5';
            $scope.billing_paymentfreqValue = 'M';
            $scope.no_of_payment_status = $scope.memexpiryvalue_status = false;
            $scope.memfreqcustomvalue_status = $scope.memrecstartvalue_status = false;
            $scope.recur_Amount_status = true;
            $scope.recur_Amount_status_TMF = $scope.recur_Amount_status_DepAmnt = $scope.recur_Amount_status_NOP = true;
            $scope.deposit_Amount_status = false;
            $scope.excludeBillingButton_status = false;
            $scope.excludeBillingFormSaveButton_status=false;
            $scope.excludeBillingFormSaveButton_status=false;
            $localStorage.current_page_from = 'membership';
            $scope.deposit_Amount_exists =false;
            $scope.exp_date_status ='Y';
            $scope.addstartdate = $scope.addenddate = ''; 
            $scope.specific_payment_frequency = 'PW';
            $scope.exclude_from_billing_flag = 'N';
             $scope.billingSEoptionvalue = 'PF';
            $scope.SEmemtotal_fee = 0;
            $scope.SEmemabsorb_fee = 0; 
            $scope.selecteddays = '';
            $scope.exclude_days_array = [];
            $scope.daySelectionExists =false;
            $scope.editExcludeDatesView= true;
            $scope.editExcludeDatesStatus=false; 
            $scope.addstartenddateError = $scope.addstartenddateExcludelimitError = false;
            $scope.addstartenddateErrortxt = $scope.addstartenddateExcludelimitErrortxt = ""; 
            $scope.addexcludestartenddateError = $scope.addExcludelimitError = false;
            $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = ""; 
            $scope.attendance_status='N';
            $scope.attendance_period_type ='CPW';
            $scope.days_list = [{'day':'Monday','Selected': true,'formatted':'mon'},
                {'day':'Tuesday','Selected': true,'formatted':'tue'}, 
                {'day':'Wednesday','Selected': true,'formatted':'wed'}, 
                {'day':'Thursday','Selected': true,'formatted':'thu'}, 
                {'day':'Friday','Selected': true,'formatted':'fri'},
                {'day':'Saturday','Selected': false,'formatted':'sat'}, 
                {'day':'Sunday','Selected': false,'formatted':'sun'}];            
            
            $('#memcustompaymntstrtdte').datepicker('setStartDate', new Date()); 
            var curr_date = new Date();
            $scope.memcustompaymntstrtdte = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
            $scope.MemOptregurl = $scope.MemCatregurl = '';
            
            $("#addstartdate").datepicker({
                format: 'mm/dd/yyyy',
                startDate: new Date(),
                ignoreReadonly: true,
                autoclose: true
            }) .on('changeDate',function(selected){
                    var startDate = new Date(selected.date.valueOf());
                    $('#addenddate').datepicker('setStartDate',startDate);
                    $('#excludebillingstartdate').datepicker('setStartDate',startDate);
                });

            $("#addenddate").datepicker({
                format: 'mm/dd/yyyy',
                ignoreReadonly: true,
                autoclose: true
            }) .on('changeDate',function(selected){
                    var endDate = new Date(selected.date.valueOf());
                    $('#addstartdate').datepicker('setEndDate',endDate);
                    $('#excludebillingenddate').datepicker('setEndDate',endDate);
                }); 
                

            $("#excludebillingstartdate").datepicker({
                format: 'mm/dd/yyyy',
                startDate: new Date(),
                ignoreReadonly: true,
                autoclose: true
            }).on('changeDate',function(selected){
                  var startDate = new Date(selected.date.valueOf());
                  $('#excludebillingenddate').datepicker('setStartDate',startDate);
               });


            $("#excludebillingenddate").datepicker({
                format: 'mm/dd/yyyy',
                ignoreReadonly: true,
                autoclose: true
            }).on('changeDate',function(selected){
                    var endDate = new Date(selected.date.valueOf());
                    $('#excludebillingstartdate').datepicker('setEndDate',endDate);
                });
                
                                    
            $scope.imgchanged = function () {
                $("#membershipcropmodal").modal('show');
                $scope.category_image_update = 'N';
            };

            $scope.imgcropcancel = function () {
                $scope.category_image_update = 'N';
            };

            $scope.imgcropped = function () {
                $scope.category_image_update = 'Y';
                $scope.croppedCategoryImage = $scope.cropper.croppedImage;
                $localStorage.PreviewMembershipCategoryImage = $scope.croppedCategoryImage;
            };

            $scope.resetimgcropped = function () {               
                $scope.category_image_update = 'Y';
                $scope.croppedCategoryImage = null;
                $scope.croppedCategoryImage = '';
                $localStorage.PreviewMembershipCategoryImage = $scope.croppedCategoryImage;
            };
            
            $scope.discounttype = function (discounttype) {
                if (discounttype === 'dollar') {
                    $scope.discnt_type = 'V';

                } else if (discounttype === 'percent') {
                    $scope.discnt_type = 'P';
                }
            }
            
            $scope.editdiscounttype = function (discnttype) {
                if (discnttype === 'dollar') {
                    this.edit_discnt_type = 'V';

                } else if (discnttype === 'percent') {
                    this.edit_discnt_type = 'P';
                }
            } 
            
            $scope.toastmsg = function () {
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            $scope.htmltoPlainTextMembership = function (event) {
                event.preventDefault();
                event.stopPropagation();
                var plaintext = event.originalEvent.clipboardData.getData('text/plain');
                return document.execCommand('inserttext', false, plaintext);
            };
            
           
            $scope.opentabbar = function (viewname) {
                $localStorage.currentmembershippreviewtab ='';               
                if ($localStorage.isAddedCategoryDetails === 'Y' || $localStorage.membershippagetype === 'edit') {
                    if (viewname === 'details') {
                        $localStorage.currentmembershippreviewtab = 'details';
                        $scope.detailsview = true;
                        $scope.ranklevelsview = false;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.membershipoptionsview=false;
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.resettabbarclass();
                        $('#tab1').addClass('done');
                        
                    }
                    else if (viewname === 'ranklevels') {
                        $localStorage.currentmembershippreviewtab = 'ranklevels';
                        $scope.detailsview = false;
                        $scope.ranklevelsview = true;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.membershipoptionsview=false;
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.resettabbarclass();
                        $('#tab2').addClass('done');                        
                         
                    } 
                    else if (viewname === 'memoptregistration') {
                        $localStorage.currentmembershippreviewtab = 'memoptregistration';
                        $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                        $scope.detailsview = false;
                        $scope.ranklevelsview = false;
                        $scope.memoptregistrationview=true;
                        $scope.memoptwaiverview=false;
                        $scope.membershipoptionsview=false;
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.resettabbarclass();
                        $('#tab3').addClass('done');                         
                    } 
                    else if (viewname === 'memoptwaiver') {
                        $localStorage.currentmembershippreviewtab = 'memoptwaiver';
                        $localStorage.PreviewMembershipWaiver = $scope.ms_waiverpolicy;
                        $scope.exist_ms_waiverpolicy = $scope.ms_waiverpolicy;
                        $scope.detailsview = false;
                        $scope.ranklevelsview = false;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=true;
                        $scope.membershipoptionsview=false;
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.resettabbarclass();
                        $('#tab4').addClass('done'); 
                        
                    }else if (viewname === 'membershipoptions') {                        
                        $localStorage.currentmembershippreviewtab = 'membershipoptions';
                        $scope.detailsview = false;
                        $scope.ranklevelsview = false;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.membershipoptionsview=true;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.resettabbarclass();
                        $('#tab5').addClass('done');
                        $('#innertab1').addClass('done');    // INNER TAB CONTENTS SHOW
                        $scope.memoptdetailsview = true;
                    }  
                    else if (viewname === 'memoptdetails') {
                        $localStorage.currentmembershippreviewtab = 'memoptdetails';
                        $scope.memoptdetailsview = true;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=false;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.resetinnertabbarclass();
                        $('#innertab1').addClass('done');
                        
                    } 
                    else if (viewname === 'memoptpayment' && $scope.isAddedMemOptions === 'Y') {
                        $localStorage.currentmembershippreviewtab = 'memoptpayment';
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = true;
                        $scope.memoptdiscountcodesview=false;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.resetinnertabbarclass();
                        $('#innertab2').addClass('done');
                        
                    } 
                    else if (viewname === 'memoptdiscountcodes' && $scope.isAddedMemOptions === 'Y') {
                        $localStorage.currentmembershippreviewtab = 'memoptdiscountcodes';
                        $scope.memoptdetailsview = false;
                        $scope.memoptpaymentview = false;
                        $scope.memoptdiscountcodesview=true;
                        $scope.memoptregistrationview=false;
                        $scope.memoptwaiverview=false;
                        $scope.resetinnertabbarclass();
                        $('#innertab3').addClass('done');
                        
                    } 
                }
                else {
                    $scope.detailsview = true;
                    $scope.ranklevelsview = false;
                    $scope.membershipoptionsview=false;
                    $scope.resettabbarclass();
                    $('#tab1').addClass('done');
                }
            };

            $scope.resettabbarclass = function (viewname) {
                $('.done').addClass('disabled').removeClass('done');
            };
            
//          RESET INNER TAB BAR
            $scope.resetinnertabbarclass = function (viewname) {
                $('.done').addClass('disabled').removeClass('done');
                $('#tab5').addClass('done');
            };
                 
            $scope.catchMSTitle = function () {
                $localStorage.PreviewMembershipCategoryName = $scope.mscategory;
            };
            
            $scope.catchMSSubTitle = function () {
                $localStorage.PreviewMembershipSubCategoryName = $scope.mssubcategory;
            };
            
             $scope.catchMSUrl = function () {
                $('#progress-full').show();
                if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F') {
                    $("#urlexpiryModal").modal('show');
                    $("#urlexpirytitle").text('Message');
                    $("#urlexpirycontent").text('Easily link URLs to your membership. Upgrade your account to enable this functionality.');
                    $('#progress-full').hide();
                    document.getElementById("memsurl").readOnly = true;
                    return false;
                }else{
                    document.getElementById("memsurl").readOnly = false;
                    $('#progress-full').hide();
                    $localStorage.PreviewMembershipCategoryVideoUrl = $scope.msurl;
                }
            };
            
             $scope.catchMSDesc = function () {
                $localStorage.PreviewMembershipCategoryDesc = $scope.msdesc;
            };
            
            $scope.catchMSOPTTitle = function () {
                $localStorage.PreviewMembershipOptionTitle = $scope.msoptCategory;
            };
            
            $scope.catchMSOPTSubTitle = function () {
                $localStorage.PreviewMembershipOptionSubtitle = $scope.msoptSubCategory;
            };
            
             $scope.catchMSOPTDesc = function () {
                $localStorage.PreviewMembershipOptionDesc = $scope.optioncategorydesc;
            };            
            
            $scope.catchMemProcessingFee = function () {
                $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
            };
                  
            $scope.catchSignupFee = function () {
                $localStorage.PreviewMemSignUpFee = $scope.memsignupfee;
            };
            
            
            $scope.catchMembershipFee = function () {
                if (parseFloat($scope.memfee) == 0 || $scope.memfee==='' || $scope.memfee=== undefined || $scope.memfee=== null) {
                    $scope.billing_depositamount = 0;
                }
                
                if (($scope.memstructure === 'NC' || $scope.memstructure === 'C') && $scope.billingoptionvalue === 'PP') {
                    if (parseFloat($scope.memfee) > 0 && parseFloat($scope.memfee) < parseFloat($scope.billing_depositamount)) {
                        $scope.deposit_Amount_exists = true;
                        $scope.deposit_Amount_status = false;
                        $scope.recur_Amount_status_DepAmnt = true;                        
                    } else {
                        $scope.deposit_Amount_exists = false;
                        $scope.deposit_Amount_status = false;
                        $scope.recur_Amount_status_DepAmnt = true;
                    }
                    $scope.min_Mem_Recur_amount_check();
                    $scope.recur_Amount_status_TMF = $scope.recur_Amount_status;
                    
                } else {
                    $scope.recur_Amount_status_TMF = true;
                    $scope.deposit_Amount_exists = false;
                    $scope.deposit_Amount_status = false;
                    $scope.recur_Amount_status_DepAmnt = true;
                }
                $localStorage.PreviewMembershipFee = $scope.memfee;
            };
            
            $scope.mem_fee_include_selection = function () { 
                $localStorage.PreviewMembershipFeeInclude = $scope.mem_fee_include;
            };
            
            $scope.catchMembershipCustomPeriod = function () {
                $localStorage.PreviewMembershipCustPeriod = $scope.memfreqcustomperiod;
                if ($scope.memfreqcustomperiod === 'CM') {
                    $scope.prorateValue = 'N';
                    $scope.memrecstartperiod = 'DM';
                } else if ($scope.memfreqcustomperiod === 'CW') {
                    $scope.prorateValue = 'N';
                    $scope.memrecstartperiod = 'DW';
                }
            };
            
            $scope.catchMembershipCustomValue = function () {
                $localStorage.PreviewMembershipCustValue = $scope.memfreqcustomvalue;
                if($scope.memstructure === 'OE' && $scope.mempayfrequency==='C' && parseFloat($scope.memfreqcustomvalue) < 1){
                    $scope.memfreqcustomvalue_status = true;
                }else{
                    $scope.memfreqcustomvalue_status = false;
                }
            };
            
            $scope.catchMemrecpayStartdate = function () {
                if($scope.memstructure === 'OE' && $scope.mempayfrequency==='C' && $scope.recpayStartdate==='Y' && parseFloat($scope.memrecstartvalue) < 1){
                    $scope.memrecstartvalue_status = true;
                }else{
                    $scope.memrecstartvalue_status = false;
                }
            };
            
            $scope.memstructureSelection = function () {
                $localStorage.PreviewMembershipStructure = $scope.memstructure; 
                $scope.attendance_status = 'N';
                $scope.attendance_period = '';
                $scope.attendance_period_type = 'CPW';
                if($scope.memstructure === 'OE'){  
                    
                    $scope.memProcessingfee = '2' ;     // Open Enrollment in payment
                    $scope.memsignupfee = $scope.memfee = '';
                    $scope.mempayfrequency = 'M';
                    $scope.memfreqcustomperiod = 'CM';
                    $scope.memfreqcustomvalue = '';
                    $scope.prorateValue ='Y';
                    $scope.recpayStartdate = 'N';                
                    $scope.memrecstartperiod = 'DM';
                    $scope.memrecstartvalue ='';
                    $scope.mem_fee_include = 'Y';  
                    $scope.billing_depositamount = 0;                    
                    
                    $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                    $localStorage.PreviewMembershipCustPeriod = $scope.memfreqcustomperiod;
                    $localStorage.PreviewMembershipCustValue = $scope.memfreqcustomvalue; 
                    
                }else{
                    $scope.memexpiryvalue = 1;              // Number of class in payment
                    $scope.billing_depositamount = 0;
                    $scope.billing_noofpayment = 1;
                    $scope.noofclass = 0;
                    $scope.billingoptionvalue = 'PF';
                    $scope.memexpiryperiod = 'M';                
                    $scope.exp_date_status = 'Y';
                    $scope.billing_paymentstartdateValue = '5';
                    $scope.billing_paymentfreqValue = 'M';
                    $scope.memexpiryvalue_status = false;
                    
                    $scope.memsignupfee = $scope.memfee = '';
                    $scope.addstartdate = $scope.addenddate = ''; 
                    $scope.specific_payment_frequency = 'PW';
                    $scope.exclude_from_billing_flag = 'N';
                    $scope.billingSEoptionvalue = 'PF';
                    $scope.SEmemtotal_fee = 0;
                    $scope.SEmemabsorb_fee = 0; 
                    $scope.selecteddays = '';
                    $scope.daySelectionExists =false;
                    $scope.days_list = [{'day':'Monday','Selected': true,'formatted':'mon'},
                    {'day':'Tuesday','Selected': true,'formatted':'tue'}, 
                    {'day':'Wednesday','Selected': true,'formatted':'wed'}, 
                    {'day':'Thursday','Selected': true,'formatted':'thu'}, 
                    {'day':'Friday','Selected': true,'formatted':'fri'},
                    {'day':'Saturday','Selected': false,'formatted':'sat'}, 
                    {'day':'Sunday','Selected': false,'formatted':'sun'}]; 
                    $scope.excludeBillingButton_status = false;
                    $scope.editExcludeDatesStatus= false; 
                    $scope.excludebillingstartdate = '';
                    $scope.excludebillingenddate = '';
                    $scope.exclude_days_array = [];
                    $('#addstartdate').datepicker('setStartDate',new Date());
                    $('#addstartdate').datepicker('setEndDate',null);
                    $('#addenddate').datepicker('setStartDate',new Date());
                    $('#addenddate').datepicker('setEndDate',null);
                    $scope.selecteddays =  $scope.current_days_order = "";
                    $scope.selecteddays === '1111100';
                    for (var i = 0; i < $scope.days_list.length; i++) {
                            if ($scope.days_list[i].Selected) {
                                $scope.selecteddays +=  "1";
                            }else{
                                $scope.selecteddays +=  "0" ;
                            }
                            if(i < 6){
                                $scope.current_days_order += $scope.days_list[i].formatted+ ",";
                            }else{
                                $scope.current_days_order += $scope.days_list[i].formatted;
                            }
                    }
                    
                    $localStorage.PreviewMemRecurringFrequency = $scope.billing_paymentfreqValue;
                    $localStorage.Previewspecific_payment_frequency = 'PW';
                    $localStorage.Previewspecific_selecteddays = '1111100';
                    $localStorage.Previewexclude_from_billing_flag = 'N';
                }    
                $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
                $localStorage.PreviewMembershipFee = 0;
                $localStorage.PreviewMemSignUpFee = 0;
                $localStorage.PreviewMembershipExpiryValue = $scope.memexpiryvalue;
                $localStorage.PreviewMembershipExpiryStatus = $scope.exp_date_status;
                $localStorage.PreviewMembershipExpiryPeriod = $scope.memexpiryperiod;
                $localStorage.PreviewMembershipDepositValue = 0;
                $localStorage.PreviewMembershipBillingOptionValue = $scope.billingoptionvalue;
                $localStorage.PreviewMembershipNoOfPayments = $scope.billing_noofpayment;
            };
            
            $scope.catchDepositAmount = function () {
                if ((parseFloat($scope.memfee) >= parseFloat($scope.billing_depositamount)) && $scope.billing_depositamount !== '')
                {
                    $scope.deposit_Amount_exists = false;
                    if (($scope.memstructure === 'NC' || $scope.memstructure === 'C') && $scope.billingoptionvalue === 'PP') {
                        if (parseFloat($scope.billing_depositamount) >= 5 || parseFloat($scope.billing_depositamount) == 0 || $scope.billing_depositamount == '') {
                            $scope.deposit_Amount_status = false;
                            $scope.min_Mem_Recur_amount_check();
                            $scope.recur_Amount_status_DepAmnt = $scope.recur_Amount_status;
                        } else {
                            $scope.deposit_Amount_status = true;
                            $scope.recur_Amount_status_DepAmnt = true;
                        }

                    } else {
                        $scope.recur_Amount_status_DepAmnt = true;
                    }
                    $localStorage.PreviewMembershipDepositValue = $scope.billing_depositamount;
                } else {

                    if ($scope.billing_depositamount === undefined) {
                        $scope.deposit_Amount_status = true;
                        $scope.deposit_Amount_exists = false;
                        $scope.recur_Amount_status_DepAmnt = true;
                    } else {
                        $scope.deposit_Amount_exists = true;
                        $scope.deposit_Amount_status = false;
                        $scope.recur_Amount_status_DepAmnt = true;
                    }

                }
            };
            
            $scope.catchNoOfClass = function(){
                $localStorage.PreviewNoofClass = $scope.noofclass;
            };
            
            
            $scope.catchMembershipExpirationDate = function () {
                $localStorage.PreviewMembershipExpiryValue = $scope.memexpiryvalue;
                if(parseFloat($scope.memexpiryvalue) > 0){
                    $scope.memexpiryvalue_status = false;
                }else{
                    $scope.memexpiryvalue_status = true;
                }
            };
            
            $scope.catchMembershipExpirationPeriod = function () {
                $localStorage.PreviewMembershipExpiryPeriod = $scope.memexpiryperiod;
            };
            
            $scope.expiryStatusChange = function () {
                    $scope.memexpiryvalue = 1;   
                    $scope.memexpiryperiod = 'M'; 
                    $localStorage.PreviewMembershipExpiryStatus = $scope.exp_date_status;
            };
            
            
            $scope.billingOptionSelection = function () {
                $localStorage.PreviewMembershipBillingOptionValue = $scope.billingoptionvalue;
                if($scope.billingoptionvalue === 'PP' && parseFloat($scope.billing_noofpayment) > 0){
                    $scope.no_of_payment_status = false;
                }else{
                    $scope.no_of_payment_status = true;
                }
            };
            
            $scope.catchNoOfPayment = function () {
                $localStorage.PreviewMembershipNoOfPayments = $scope.billing_noofpayment;
                if($scope.billingoptionvalue === 'PP'){
                    if(parseFloat($scope.billing_noofpayment) > 0){
                        $scope.no_of_payment_status = false;  
                        $scope.min_Mem_Recur_amount_check();
                        $scope.recur_Amount_status_NOP = $scope.recur_Amount_status;
                    }else{
                        $scope.no_of_payment_status = true;
                        $scope.recur_Amount_status_NOP = true;
                    }
                }else{
                    $scope.no_of_payment_status = false;
                    $scope.recur_Amount_status_NOP = true;
                }
            };            
            
            $scope.catchMSNewRegFieldText = function () {                
                $localStorage.PreviewNewMemRegistrationField = $scope.ms_reg_col_newvalue;
            };
                  
            $scope.catchMSNewRegFieldMandatory = function () {
                $localStorage.PreviewNewMemRegFieldMandatory = $scope.ms_checked;
            };
            
            
            $scope.catchOldMemRegFieldText = function () {
                $localStorage.PreviewOldMemRegistrationField = this.ms_register_column_name;
            };
            
            $scope.catchOldMemRegFieldMandatory = function () {
                $localStorage.PreviewOldMemRegFieldMandatory = this.ms_mand_checked;
            };
            
            $scope.catchMSWaiverText = function () {
                $localStorage.PreviewMembershipWaiver = $scope.ms_waiverpolicy;
            };
            
            $scope.paste = function (e) {
                var pasted_value = e.originalEvent.clipboardData.getData('text/plain');
                var pasted_text_length = pasted_value.length;  
                if(pasted_text_length > 100){
                    var x = document.getElementById("snackbarpaste")
                    x.className = "showpaste";
                    setTimeout(function(){ x.className = x.className.replace("showpaste", ""); }, 3000);
                }
            };
            
            $scope.paymentnow = function(){
                  $("#sidetopbar").removeClass('modal-open');
                  $(".modal-backdrop").css("display", "none");
                  $location.path('/billingstripe');                
            };
            
// MINIMUM RECURRING AMOUNT CHECK
            $scope.min_Mem_Recur_amount_check = function () {
                var mem_fee = parseFloat($scope.memfee);
                var deposit_amnt = parseFloat($scope.billing_depositamount);
                var no_of_paymnt = parseFloat($scope.billing_noofpayment);
                if (mem_fee >= 0 && deposit_amnt >= 0) {
                    if (mem_fee > deposit_amnt) {
                        var Balance_Amount = mem_fee - deposit_amnt;
                        if (parseFloat($scope.billing_noofpayment) > 0) {
                            var Recurr_Amount = Balance_Amount / no_of_paymnt;
                            if (parseFloat(Recurr_Amount) >= 5) {
                                $scope.recur_Amount_status = true;
                                $scope.recur_Amount_status_TMF = true;
                                $scope.recur_Amount_status_DepAmnt = true;
                                $scope.recur_Amount_status_NOP = true;
                            } else {
                                $scope.recur_Amount_status = false;
                            }
                        } else {
                            $scope.recur_Amount_status = false;
                        }
                    } else {
                        $scope.recur_Amount_status = false;
                    }
                } else {
                    $scope.recur_Amount_status = false;
                }    
            };
                       
// Membership processing fee calculation       
            $scope.getMemTotalprocessingfee = function (total_amount) { 
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var process_total_cost1 = parseFloat(total_amount) + (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var p_fee2 = (((parseFloat(process_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                var process_total_cost = parseFloat(total_amount) + Math.round((+p_fee2 + +0.00001) * 100) / 100;
                return process_total_cost;               
            };

// Membership absorb fee calculation
            $scope.getMemAbsorbFees = function (total_amount) {
                $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                var p_fee = (((parseFloat(total_amount) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)); 
                var process_total_cost = parseFloat(total_amount) - Math.round((+p_fee + +0.00001) * 100) / 100;
                return process_total_cost;               
            };
            
//    Membership Fee calculation for SE        
            $scope.getMemabsorbfeeforSE = function () {
                
                if(parseFloat($scope.memfee) >= 0.00 && parseFloat($scope.memfee) != '' && $scope.selecteddays){
                  var checkbox_count = $scope.selecteddays.match(/1/gi).length;
                  if($scope.specific_payment_frequency ==='PW'){
                      $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                      $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                      var SEMem_cost =($scope.memfee)/checkbox_count;  
                      var SEmemp_fee = (((parseFloat(SEMem_cost) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)); 
                      $scope.SEmemabsorb_fee = SEMem_cost - Math.round((+SEmemp_fee + +0.00001) * 100) / 100;
                      return SEMem_cost; 
                   }
                    else if($scope.specific_payment_frequency ==='PM'){
                       $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                       $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                       var SEMem_cost = ($scope.memfee)/(checkbox_count * 4.345);
                       var SEmemp_fee = (((parseFloat(SEMem_cost) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val)); 
                       $scope.SEmemabsorb_fee = SEMem_cost - Math.round((+SEmemp_fee + +0.00001) * 100) / 100;
                       return SEMem_cost; 
                    } 
                 } else{
                     $scope.SEmemabsorb_fee = 0;
                     return 0;
                 }   
            };
            
            $scope.getMemfeeforSE = function () {
                
                if(parseFloat($scope.memfee) >= 0.00 && parseFloat($scope.memfee) != '' && $scope.selecteddays){
                  var checkbox_count = $scope.selecteddays.match(/1/gi).length;
                      if($scope.specific_payment_frequency ==='PW'){
                      $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                      $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                      var SEMem_cost =($scope.memfee)/checkbox_count;  
                      var SEprocess_total_cost1 = parseFloat(SEMem_cost) + (((parseFloat(SEMem_cost) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                      var SEp_fee2 = (((parseFloat(SEprocess_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                      $scope.SEmemtotal_fee = SEMem_cost + Math.round((+SEp_fee2 + +0.00001) * 100) / 100;
                      return SEMem_cost; 
                   }
                    else if($scope.specific_payment_frequency ==='PM'){
                       $scope.process_fee_per = parseFloat($localStorage.processing_fee_percentage);
                       $scope.process_fee_val = parseFloat($localStorage.processing_fee_transaction);
                       var SEMem_cost =($scope.memfee)/(checkbox_count * 4.345);
                       var SEprocess_total_cost1 = parseFloat(SEMem_cost) + (((parseFloat(SEMem_cost) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                       var SEp_fee2 = (((parseFloat(SEprocess_total_cost1) * parseFloat($scope.process_fee_per)) / 100) + parseFloat($scope.process_fee_val));
                       $scope.SEmemtotal_fee = SEMem_cost + Math.round((+SEp_fee2 + +0.00001) * 100) / 100; 
                       return SEMem_cost; 
                    } 
                } else{
                     $scope.SEmemtotal_fee = 0;
                     return 0;
                }  
            };
            
            
            $scope.getDateToday = function(){
                var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                return [year, month, day].join('-');
            };
            
            $scope.checkStartEndDate = function(){ 
                $scope.curr_date_new = $scope.getDateToday(); //End date should not be greater Start date
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                if($scope.addstartdate && $scope.addenddate){
                    var membership_startdate = new Date($scope.dateformat($scope.addstartdate));
                    var membership_enddate = new Date($scope.dateformat($scope.addenddate));
                    
                    if((+membership_enddate >= +membership_startdate) && (+membership_startdate >= +server_curr_date) && (+membership_enddate >= +server_curr_date)){
                        $scope.addstartenddateError = false;  
                        $scope.addstartenddateErrortxt = "Start date should be past of End date / Start and End date should be future of current date.";
                    }else{
                        $scope.addstartenddateError = true;
                        $scope.addstartenddateErrortxt = "";
                    }
                    
                    if($scope.exclude_days_array.length > 0){
                        for(var ex_day_count = 0; ex_day_count < $scope.exclude_days_array.length; ex_day_count++ ){
                            var exclude_start_date = new Date($scope.exclude_days_array[ex_day_count].billing_exclude_startdate);
                            var exclude_end_date = new Date($scope.exclude_days_array[ex_day_count].billing_exclude_enddate);
                            if((+membership_startdate > +exclude_start_date) || (+membership_enddate < +exclude_end_date)){
                                $scope.addstartenddateExcludelimitError = true;
                                $scope.addstartenddateExcludelimitErrortxt = "Start and End date should covers excluded days added.";
                                return false;
                            }else{
                                $scope.addstartenddateExcludelimitError = false; 
                                $scope.addstartenddateExcludelimitErrortxt = "";                                
                            }
                        }
                    }else{
                        $scope.addstartenddateExcludelimitError = false;   
                        $scope.addstartenddateExcludelimitErrortxt = "";                         
                    }
                }else{
                    $scope.addstartenddateError = true;
                    $scope.addstartenddateErrortxt = "";                    
                }
            };
            
            
            $scope.catchaddstartdate = function(){
                $scope.checkStartEndDate();
                $localStorage.Previewspecific_start_date = $scope.dateformat($scope.addstartdate);
            }
            
            $scope.catchaddenddate = function(){
                $scope.checkStartEndDate();
                $localStorage.Previewspecific_end_date = $scope.dateformat($scope.addenddate);
            }
            
            $scope.catchSelectedDays = function () { 
                $scope.selecteddays =  "";
                $scope.current_days_order = "";
                for (var i = 0; i < $scope.days_list.length; i++) {
                    if ($scope.days_list[i].Selected) {
                        $scope.selecteddays +=  "1";
                    }else{
                        $scope.selecteddays +=  "0" ;
                    }
                    if(i < 6){
                        $scope.current_days_order += $scope.days_list[i].formatted+ ",";
                    }else{
                        $scope.current_days_order += $scope.days_list[i].formatted;
                    }
                    
                }
                if($scope.selecteddays === '0000000'){
                    $scope.daySelectionExists =true;
                }else{
                    $scope.daySelectionExists =false;
                }
                $localStorage.Previewspecific_selecteddays = $scope.selecteddays;
            };
            $scope.catchSelectedDays();
            
            
            $scope.catchspecific_payment_frequency = function () { 
               $localStorage.Previewspecific_payment_frequency = $scope.specific_payment_frequency;
            };
            
            $scope.billingSEOptionSelection = function () {
               $localStorage.PreviewMembershipBillingOptionValue = $scope.billingSEoptionvalue;
            };
            
             
            $scope.timeServerformat = function (time) {
                if (time.length > 0) {
                    var hours = Number(time.match(/^(\d+)/)[1]);
                    var minutes = Number(time.match(/:(\d+)/)[1]);
                    var AMPM = time.match(/\s(.*)$/)[1];
                    if (AMPM == "PM" && hours < 12)
                        hours = hours + 12;
                    if (AMPM == "AM" && hours == 12)
                        hours = hours - 12;
                    var sHours = hours.toString();
                    var sMinutes = minutes.toString();
                    if (hours < 10)
                        sHours = "0" + sHours;
                    if (minutes < 10)
                        sMinutes = "0" + sMinutes;
                    var formatedtime = sHours + ":" + sMinutes + ":01";
                    return formatedtime;
                } else {
                    var formatedtime = "";
                    return formatedtime;
                }
            };

            $scope.dateformat = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("/");
                    var year = datePart[2];
                    var month = datePart[0];
                    var day = datePart[1];
                    var datevalue = year + '-' + month + '-' + day;
                    return datevalue;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }

            $scope.formatserverdate = function (date) {
                if (date !== '0000-00-00' && date !== '') {
                    var datePart = date.split("-");
                    var day = datePart[2];
                    var year = datePart[0];
                    var month = datePart[1];
                    return month + '/' + day + '/' + year;
                } else {
                    var datevalue = "";
                    return datevalue;
                }

            }

            $scope.formatAMPM = function (dtime) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                
                if (isSafari) {                                         //For safari browser
                    var safaritimecheck = dtime.split(" ");
                    if (safaritimecheck[1] === "00:00:00") {
                        var localTime = "";
                        return localTime;
                    } else {
                        var dda = dtime.toString();
                        dda = dda.replace(/ /g, "T");
                        var date = new Date(dda).toUTCString(); //For date in safari format

                        var time = date.slice(17, 25);
                        // format AM PM start
                        var hours = time.slice(0, 2);
                        var minutes = time.slice(3, 5);
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        var localTime = hours + ':' + minutes + ' ' + ampm;
                        // format AM PM end 
                        return localTime;
                    }

                } else {                                              //For other browsers
                    var servertime = dtime.split(" ");
                    if (servertime[1] !== "00:00:00") {                 
                        var date = new Date(dtime);
                        var hours = date.getHours();
                        var minutes = date.getMinutes();
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        minutes = minutes < 10 ? '0' + minutes : minutes;
                        var strTime = hours + ':' + minutes + ' ' + ampm;
                        return strTime;
                    } else {
                        var strTime = "";
                        return strTime;
                    }
                }
            };

            $scope.datetimestring = function (ddate) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari) {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var safaritimecheck = ddate.split(" ");
                        if (safaritimecheck[1] === "00:00:00") {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var time = new Date(dda).toUTCString();
                            var dateonly = time.toString();
                            var fulldateonly = dateonly.replace(/GMT.*/g, "");
                            var localdate = fulldateonly.slice(0, -9);
                            return localdate;
                        } else {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var date = new Date(dda).toUTCString(); //For date in UTC format (without GMT) for safari
                            var datealone = date.slice(0, 12);
                            var time = date.slice(17, 25);
                            // format AM PM start
                            var hours = time.slice(0, 2);
                            var minutes = time.slice(3, 5);
                            var ampm = hours >= 12 ? 'PM' : 'AM';
                            hours = hours % 12;
                            hours = hours ? hours : 12; // the hour '0' should be '12'
                            var localTime = hours + ':' + minutes + ' ' + ampm;
                            var localdatetime = datealone + "  " + localTime;
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                } else {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var timecheck = ddate.split(" ");
                        if (timecheck[1] === "00:00:00") {
                            var ddateonly = timecheck[0] + 'T00:00:00+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdate = $filter('date')(newDate, 'EEE, MMM d');
                            return localdate;
                        } else {
                            var ddateonly = timecheck[0] + 'T' + timecheck[1] + '+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdatetime = $filter('date')(newDate, 'EEE, MMM d h:mm a');
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                }
            };


            $scope.dateonly = function (d) {
                if (d !== '0000-00-00 00:00:00') {
                    var datealone = d.split(" ")[0];
                    return datealone;
                } else {
                    var datealone = "";
                    return datealone;
                }
            };
            
            $scope.CompareTime = function (stime, etime) {
//                alert('Im in Compare time');
                var regex = /^([0-1][0-9])\:[0-5][0-9]\s*[ap]m$/i;
                var smatch = stime.match(regex);
                var ematch = etime.match(regex);
                if (smatch && ematch) {
                    var shour = parseInt(smatch[1]);
                    var ehour = parseInt(ematch[1]);
                    var smin = parseInt(stime.split(':')[1].substring(0, 2));
                    var sfrmt = stime.split(':')[1].substring(2, 5).trim();
                    var emin = parseInt(etime.split(':')[1].substring(0, 2));
                    var efrmt = etime.split(':')[1].substring(2, 5).trim();
//                    alert(sfrmt + ' ' + efrmt);
                    // if ( !isNaN( shour) && shour <= 11 && !isNaN( ehour) && ehour <= 11 ) { 
                    if (sfrmt == efrmt) {
                        if (shour > ehour) {
                            if (shour == 12) {
//                                    alert(shour + '>' + ehour + '::F');
                                return false;
                            }
//                            alert(shour + '>' + ehour + '::F');
                            return false;
                        } else if (shour === ehour) {
                            if (emin >= smin) {
//                                alert('H: ' + shour + '===' + ehour + ' M:' + emin + '>' + smin + '::T');
                                return true;
                            } else
                            {
//                                alert('H: ' + shour + '===' + ehour + ' M:' + emin + '>' + smin + '::F');
                                return false;
                            }
                        } else {
                            if (sfrmt == 'AM' || sfrmt == 'am' || sfrmt == 'PM' || sfrmt == 'pm') {
                                if (parseInt(ehour) == 12) {
//                                    alert(shour + '>' + ehour + '::F');
                                    return false;
                                }
                            }
//                            alert(shour + '<' + ehour + ':::T');
                            return true;
                        }
                    } else if (sfrmt == 'AM' || sfrmt == 'am') {
//                        alert(sfrmt + '<' + efrmt + '::T');
                        return true;
                    } else {
//                        alert(sfrmt + '>' + efrmt + '::F');
                        return false;
                    }
                }
//                alert('IM out in compare');
            };

            $scope.listmemberships = function () {
                $location.path('/managemembership');
            };
            
            $scope.editmembershipinfo = function(editmembershipinfo){
                var msg;
                if(editmembershipinfo === 'optionhide'){
                    msg = "<p>This option allows for internal facing only memberships that will not show on the member app.<p><p>Use this as a tool to transfer memberships from another platform.</p><p>Family and friends discounted memberships is another example of internal only memberships.</p>";
                }
                if(editmembershipinfo === 'prorate'){
                    msg = "<p>This option auto calculates the first payment so that all monthly recurring payments will come out on the 1st of the month.<p><p>All Semi-Monthly recurring payments will come out on the 1st and 16th of the month.</p>";
                }
                if(editmembershipinfo === 'delaypayment'){                     
                    msg = "<p>This option delays the first recurring payment date.<p><p>An example of this use case is a 6 month pre-payment by the customer, follow by monthly payments starting 6 months later.</p>";
                }
                if(editmembershipinfo === 'discount'){
                    msg = "<p>Members can apply two discount codes when registering.<p><p>One discount code for Membership Fee and one discount code for Sign-Up Fee.</p>";
                }
                if(editmembershipinfo === 'publiccaturl'){
                    msg = "<p>This URL is a web link to this membership category.</p>";
                }
                if(editmembershipinfo === 'publicopturl'){
                    msg = "<p>This URL is a web link to this membership option.</p>";
                }
                
                $("#failuremessageModal").modal('show');
                $("#failuremessagetitle").text('Info');
                $("#failuremessagecontent").html(msg);
            };
            
            $scope.listcategory = function () {
                $location.path('/managemembership');
            };
            
            //Particular membership value from manage membership listing page for editing
            if ($localStorage.membershippagetype === 'edit') {
                $localStorage.currentpage = "editmembership";
                if ($localStorage.membershiplocaldata === undefined || $localStorage.membershiplocaldata === "") {
                    $localStorage.membershiplocaldata = membershipListDetails.getMembershipDetail();                   
                    $scope.membershipdata = $localStorage.membershiplocaldata;
                    $localStorage.preview_membershipcontent = $scope.membershipdata[0];
                } else {
                    $scope.membershipdata = $localStorage.membershiplocaldata;
                    $localStorage.preview_membershipcontent = $scope.membershipdata[0];
                }                
                $scope.msadded = true;
                $scope.membership_id = $scope.membershipdata[0].membership_id;                                
                $scope.mscategory = $scope.membershipdata[0].category_title;
                $scope.mssubcategory = $scope.membershipdata[0].category_subtitle;
                $scope.category_status = $scope.membershipdata[0].category_status;
                $scope.msurl = $scope.membershipdata[0].category_video_url;
                $scope.category_picture = $scope.membershipdata[0].category_image_url;
                $scope.croppedCategoryImage = $scope.membershipdata[0].category_image_url;
                $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                $scope.msdesc = $scope.membershipdata[0].category_description;
                $scope.rank_columns = $scope.membershipdata[0].rank_details;
                $scope.selectedrankcolindex = "";
                $scope.ms_registration_columns = $scope.membershipdata[0].reg_columns;
                if($scope.ms_registration_columns.length >= 10) {
                    $scope.ms_regfieldexceeds = true;
                } else {
                    $scope.ms_regfieldexceeds = false;
                }
                $scope.ms_selectedregcolindex = "";
                if($scope.membershipdata[0].waiver_policies === null) {
                    $scope.ms_waiverpolicy = '';
                } else {
                    $scope.ms_waiverpolicy = $scope.membershipdata[0].waiver_policies;
                }
                if( $scope.membershipdata[0].membership_options.length > 0){
                    $scope.child_membership_options = $scope.membershipdata[0].membership_options;
                }else{
                    $scope.child_membership_options = '';
                }
                $scope.MemCatregurl = $scope.membershipdata[0].membership_category_url;
                $('#progress-full').hide();
            };
            
            if ($localStorage.isAddedCategoryDetails === 'Y') {
                $scope.membership_data = $localStorage.membership_details;
                $localStorage.currentpage = "addmembership";
                $localStorage.preview_membershipcontent = $scope.membership_data;
                
                $scope.msadded = true;
                $scope.membership_id = $scope.membership_data.membership_id;                                
                $scope.mscategory = $scope.membership_data.category_title;
                $scope.mssubcategory = $scope.membership_data.category_subtitle;
                $scope.category_status = $scope.membership_data.category_status;
                $scope.msurl = $scope.membership_data.category_video_url;
                $scope.category_picture = $scope.membership_data.category_image_url;
                $scope.croppedCategoryImage = $scope.membership_data.category_image_url;
                $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                $scope.msdesc = $scope.membership_data.category_description;
                $scope.rank_columns = $scope.membership_data.rank_details;
                $scope.selectedrankcolindex = "";
                $scope.ms_registration_columns = $scope.membership_data.reg_columns;                
                if($scope.ms_registration_columns.length >= 10) {
                    $scope.ms_regfieldexceeds = true;
                } else {
                    $scope.ms_regfieldexceeds = false;
                }
                $scope.ms_selectedregcolindex = "";
                if($scope.membership_data.waiver_policies === null) {
                    $scope.ms_waiverpolicy = '';
                } else {
                    $scope.ms_waiverpolicy = $scope.membership_data.waiver_policies;
                }
                if( $scope.membership_data.membership_options.length > 0){
                    $scope.child_membership_options = $scope.membership_data.membership_options;
                }else{
                    $scope.child_membership_options = '';
                }
                $scope.MemCatregurl = $scope.membership_data.membership_category_url;
                $('#progress-full').hide();
                
            };            
            
            //category URL copied on clipboard
            $scope.copyCatURL = function () { //kamal
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
            //option value copied on clipboard
            $scope.copyOptURL = function () { //kamal
                var x = document.getElementById("membershipsnackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            };
            
             $scope.addMembershipCategory = function (evnt_status) {
                $('#progress-full').show();

                    if ($scope.mscategory === "" || $scope.mscategory === undefined || $scope.mscategory === null ) {
                        $scope.categoryname = "";
                    } else {
                        $scope.categoryname = $scope.mscategory;
                    }
                    
                    if ($scope.mssubcategory === "" || $scope.mssubcategory === undefined || $scope.mssubcategory === null ) {
                        $scope.categorysubname = "";
                    } else {
                        $scope.categorysubname = $scope.mssubcategory;
                    }
                    

                    if ($scope.msurl === "" || $scope.msurl === undefined || $scope.msurl === null ) {
                        $scope.CategoryUrlformat = "";
                    } else {
                        if ($scope.msurl.indexOf("http://") === 0 || $scope.msurl.indexOf("https://") === 0) {
                            $scope.CategoryUrlformat = $scope.msurl;
                        } else {
                            $scope.CategoryUrlformat = 'http://' + $scope.msurl;
                        }
                    }

                    if ($scope.croppedCategoryImage === "" || $scope.croppedCategoryImage === undefined || $scope.croppedCategoryImage === null) {
                        $scope.category_picture_type = "";
                        $scope.mem_category_picture = "";
                    } else {
                        $scope.category_picture_type = "png";
                        $scope.mem_category_picture = $scope.croppedCategoryImage.substr($scope.croppedCategoryImage.indexOf(",") + 1);
                    }

                    $scope.categorydesc = document.getElementById("memstextarea").innerHTML;
                    $scope.category_status = evnt_status;
                    

                $http({
                    method: 'POST',
                    url: urlservice.url + 'addMembershipCategory',
                    data: {
                        "category_title": $scope.categoryname,
                        "category_subtitle": $scope.categorysubname,
                        "category_banner_img_content": $scope.mem_category_picture,
                        "category_banner_img_type": $scope.category_picture_type,
                        "category_video_url": $scope.CategoryUrlformat,
                        "category_description": $scope.categorydesc,
                        "category_status": $scope.category_status,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "addmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('The membership category has been added.');

                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                        $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                        if($scope.ms_registration_columns.length >= 10) {
                            $scope.ms_regfieldexceeds = true;
                        } else {
                            $scope.ms_regfieldexceeds = false;
                        }
                        $scope.ms_selectedregcolindex = "";
                        if($scope.membership_details.waiver_policies === null) {
                            $scope.ms_waiverpolicy = '';
                        } else {
                            $scope.ms_waiverpolicy = $scope.membership_details.waiver_policies;
                        }
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                        }else{
                            $scope.child_membership_options = [];
                        }
                        $scope.MemCatregurl = $scope.membership_details.membership_category_url;
                            
                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
             $scope.updateMembershipCategory = function (evnt_status,mem_id) {
                $('#progress-full').show();

                    if ($scope.mscategory === "" || $scope.mscategory === undefined || $scope.mscategory === null ) {
                        $scope.categoryname = "";
                    } else {
                        $scope.categoryname = $scope.mscategory;
                    }
                    
                    if ($scope.mssubcategory === "" || $scope.mssubcategory === undefined || $scope.mssubcategory === null ) {
                        $scope.categorysubname = "";
                    } else {
                        $scope.categorysubname = $scope.mssubcategory;
                    }
                    

                    if ($scope.msurl === "" || $scope.msurl === undefined || $scope.msurl === null ) {
                        $scope.CategoryUrlformat = "";
                    } else {
                        if ($scope.msurl.indexOf("http://") === 0 || $scope.msurl.indexOf("https://") === 0) {
                            $scope.CategoryUrlformat = $scope.msurl;
                        } else {
                            $scope.CategoryUrlformat = 'http://' + $scope.msurl;
                        }
                    }

                    if ($scope.croppedCategoryImage === "" || $scope.croppedCategoryImage === undefined || $scope.croppedCategoryImage === null) {
                        $scope.category_picture_type = "";
                        $scope.mem_category_picture = "";
                    } else {
                        $scope.category_picture_type = "png";
                        $scope.mem_category_picture = $scope.croppedCategoryImage.substr($scope.croppedCategoryImage.indexOf(",") + 1);
                    }

                    $scope.categorydesc = document.getElementById("memstextarea").innerHTML;
                    $scope.category_status = evnt_status;
                    $scope.category_membership_id = mem_id;
                    

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipCategory',
                    data: {
                        "membership_id": $scope.membership_id,
                        "category_title": $scope.categoryname,
                        "category_subtitle": $scope.categorysubname,
                        "category_banner_img_content": $scope.mem_category_picture,
                        "old_category_image_url": $scope.category_picture,                        
                        "category_banner_img_type": $scope.category_picture_type,
                        "category_image_update": $scope.category_image_update,
                        "category_video_url": $scope.CategoryUrlformat,
                        "category_description": $scope.categorydesc,
                        "category_status": $scope.category_status,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();


                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Membership category successfully updated');

                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                        $scope.selectedMemOptIndex = '';
                        $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                        if($scope.ms_registration_columns.length >= 10) {
                            $scope.ms_regfieldexceeds = true;
                        } else {
                            $scope.ms_regfieldexceeds = false;
                        }
                        $scope.ms_selectedregcolindex = "";
                        if($scope.membership_details.waiver_policies === null) {
                            $scope.ms_waiverpolicy = '';
                        } else {
                            $scope.ms_waiverpolicy = $scope.membership_details.waiver_policies;
                        }
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                        }else{
                            $scope.child_membership_options = [];
                        }
                        $scope.MemCatregurl = $scope.membership_details.membership_category_url;
                                
                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.deleteMembershipCategory = function (category_status, mem_id) {
                $scope.delete_mem_category_id = mem_id;
                $scope.delete_mem_category_status = category_status;
                $("#memCategoryDeleteModal").modal('show');
                $("#memCategoryDeletetitle").text('Delete');
                $("#memCategoryDeletecontent").text('Are you sure want to delete membership?');
            };  
            
            $scope.memCategorydeleteConfirm = function () {                
                $("#memCategoryDeleteModal").modal('hide');
                $('#progress-full').show();
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMembershipCategory',
                    data: {
                        "membership_id": $scope.delete_mem_category_id,
                        "company_id": $scope.company_id,
                        "list_type":$scope.delete_mem_category_status

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                           
                        $localStorage.membershipcategorydeleteredirect = 'Y'; 
                         $timeout( function(){
                             $location.path('/managemembership');
                        }, 1000 );

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.showNewRank = function () {
                $scope.rank_col_newvalue = '';
                $scope.required_attendance_count = '';
                $scope.showAddRank = true;
            };
            
            $scope.editrank = function (rank_value,required_count,ind) {
                this.rank_column_name = rank_value;
                this.required_attendance_updated_count = required_count;
                $scope.selectedrankcolindex = ind;               
                $scope.showaddedrank = false; 
                $scope.edit_showaddedrank = false;
            };
            
            $scope.cancelnewsave = function () {//deepak
                $scope.rank_col_newvalue = '';
                $scope.required_attendance_count = '';
                $scope.selectedrankcolindex = "";
                $scope.showAddRank = false;                
            };
            
            $scope.cancelsave = function (rank_value) {
                this.rank_col_newvalue = rank_value;
                $scope.selectedrankcolindex = "";
                $scope.showaddedrank = true;
                $scope.edit_showaddedrank = false;                
            };
            
             $scope.addMembershipRank = function () {
                $('#progress-full').show();

                    if ($scope.rank_col_newvalue === "" || $scope.rank_col_newvalue === undefined || $scope.rank_col_newvalue === null ) {
                        $scope.rank_name = "";
                    } else {
                        $scope.rank_name = $scope.rank_col_newvalue;
                    }
                    
                    if($scope.required_attendance_count === '' || $scope.required_attendance_count === undefined || $scope.required_attendance_count === null){
                        $scope.required_attendance = '';
                    } else {
                        $scope.required_attendance = $scope.required_attendance_count;
                    }
                $http({
                    method: 'POST',
                    url: urlservice.url + 'addMembershipRank',
                    data: {
                        "membership_id": $scope.membership_id,
                        "required_attendance": $scope.required_attendance,
                        "rank_name": $scope.rank_name,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.rank_col_newvalue = '';
                        $scope.selectedrankcolindex = "";
                         $scope.showAddRank = false;
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('The rank field has been added.');

                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                                
                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updateMembershipRank = function (ind) {//kumar
                $('#progress-full').show();

                    if(this.rank_column_name === "" || this.rank_column_name === undefined || this.rank_column_name === null ) {
                        $scope.rank_name_updated = "";
                    } else {
                        $scope.rank_name_updated = this.rank_column_name;
                    }
                    $scope.mem_rank_id = $scope.rank_columns[ind].rank_id;
                    
                    if(this.required_attendance_updated_count === '' || this.required_attendance_updated_count === undefined || this.required_attendance_updated_count === null){
                        $scope.required_attendance_updated = '';
                    } else {
                        $scope.required_attendance_updated = this.required_attendance_updated_count;
                    }
                    
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipRank',
                    data: {
                        "membership_id": $scope.membership_id,
                        "required_attendance":$scope.required_attendance_updated,
                        "membership_rank_id": $scope.mem_rank_id,
                        "rank_name": $scope.rank_name_updated,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $scope.selectedrankcolindex = "";
                        $scope.showaddedrank = true; 
                        $scope.edit_showaddedrank = false;                         
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Rank field successfully updated');

                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.categoryname = $scope.membership_details.category_title;
                        $scope.categorysubname = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.CategoryUrlformat = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.deleterank = function (ind) {
                $scope.delete_mem_rank_id = $scope.rank_columns[ind].rank_id;
                $("#rankDeleteModal").modal('show');
                $("#rankDeletetitle").text('Delete');
                $("#rankDeletecontent").text('Are you sure want to delete rank detail?');
            };  
            
            $scope.rankdeleteConfirm = function () {                
                $("#rankDeleteModal").modal('hide');                
                $('#progress-full').show();
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMembershipRank',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_rank_id": $scope.delete_mem_rank_id,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Rank field successfully deleted');

                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.categoryname = $scope.membership_details.category_title;
                        $scope.categorysubname = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.CategoryUrlformat = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                                
                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                       $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
            //Rank field drag and drop sorting
            $scope.dragrankfieldCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_rank_id = $scope.rank_columns[ind].rank_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.rank_columns.length;i++){ 
                            if($scope.rank_columns[i].rank_id === $scope.start_rank_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_rank_id, rank_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                               sort_rank_id = $scope.rank_columns[$scope.new_location_id].rank_id;
                               rank_sort_order = parseFloat($scope.rank_columns[$scope.new_location_id + 1].rank_order) / 2;
                               $scope.update_rankfieldSorting(sort_rank_id, rank_sort_order);
                           } else if ($scope.new_location_id === $scope.rank_columns.length - 1) {
                               sort_rank_id = $scope.rank_columns[$scope.new_location_id].rank_id;                                
                               next_sort_order = parseFloat($scope.rank_columns[$scope.new_location_id - 1].rank_order);
                               rank_sort_order = next_sort_order + 1;
                               $scope.update_rankfieldSorting(sort_rank_id, rank_sort_order);
                           } else {
                               sort_rank_id = $scope.rank_columns[$scope.new_location_id].rank_id;
                               previous_sort_order = parseFloat($scope.rank_columns[$scope.new_location_id + 1].rank_order);
                               next_sort_order = parseFloat($scope.rank_columns[$scope.new_location_id - 1].rank_order);
                               rank_sort_order = (previous_sort_order + next_sort_order) / 2;
                               $scope.update_rankfieldSorting(sort_rank_id, rank_sort_order);
                           }
                        }

                    }, 1000);
                }
            };
            
             $scope.update_rankfieldSorting = function(rank_id,sort_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipRankSortOrder',
                    data: {
                        "sort_id": sort_id,
                        "membership_rank_id": rank_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $scope.selectedrankcolindex = "";
                        $scope.showaddedrank = true;
                        $scope.edit_showaddedrank = false;
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();
                        $scope.msadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;
                        $scope.categoryname = $scope.membership_details.category_title;
                        $scope.categorysubname = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.CategoryUrlformat = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    }else {
                        $localStorage.isAddedCategoryDetails = 'N';
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
             $scope.addMemOptDetail = function () {
                $('#progress-full').show();

                    if ($scope.msoptCategory === "" || $scope.msoptCategory === undefined || $scope.msoptCategory === null ) {
                        $scope.optioncategoryname = "";
                    } else {
                        $scope.optioncategoryname = $scope.msoptCategory;
                    }
                    
                    if ($scope.msoptSubCategory === "" || $scope.msoptSubCategory === undefined || $scope.msoptSubCategory === null ) {
                        $scope.optioncategorysubname = "";
                    } else {
                        $scope.optioncategorysubname = $scope.msoptSubCategory;
                    }
                    $scope.optioncategorydesc = document.getElementById("memsopttextarea").innerHTML;
                    

                $http({
                    method: 'POST',
                    url: urlservice.url + 'addMembershipOptions',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_title": $scope.optioncategoryname,
                        "membership_subtitle": $scope.optioncategorysubname,
                        "membership_description": $scope.optioncategorydesc,
                        "category_status": $scope.category_status,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $scope.isAddedMemOptions = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('The membership option has been added.');

                        $scope.msadded =  $scope.msoptionadded = true;                               
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                        $scope.selectedMemOptIndex = 0;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                            $scope.membership_option_id = $scope.child_membership_options[0].membership_option_id;
                            $scope.msoptCategory = $scope.child_membership_options[0].membership_title;
                            $scope.msoptSubCategory = $scope.child_membership_options[0].membership_subtitle;                                    
                            $scope.MemOptregurl = $scope.child_membership_options[0].membership_option_url;
                            $scope.optioncategorydesc = $scope.child_membership_options[0].membership_description;
                            $scope.ms_registration_columns = $scope.membership_details.membership_options[0].reg_columns;
                            $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
                            $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                            $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            $localStorage.childmembershipindex = $scope.selectedMemOptIndex;
                            $localStorage.preview_membership_childlist = $scope.child_membership_options;
                        }else{
                            $scope.child_membership_options = [];
                        }
                        $localStorage.preview_membership_childlist = $scope.child_membership_options;   
                        $localStorage.childmembershipoptionadd = false;
                        $localStorage.childmembershipindex = $scope.selectedMemOptIndex;
                        $localStorage.preview_member_childlisthide = true;
                        $localStorage.preview_membershipchildlist = false;
                        $localStorage.childmembershipoptionadd = true;

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.updateMemOptDetail = function () {
                $('#progress-full').show();

                    if ($scope.msoptCategory === "" || $scope.msoptCategory === undefined || $scope.msoptCategory === null ) {
                        $scope.optioncategoryname = "";
                    } else {
                        $scope.optioncategoryname = $scope.msoptCategory;
                    }
                    
                    if ($scope.msoptSubCategory === "" || $scope.msoptSubCategory === undefined || $scope.msoptSubCategory === null ) {
                        $scope.optioncategorysubname = "";
                    } else {
                        $scope.optioncategorysubname = $scope.msoptSubCategory;
                    }
                    $scope.optioncategorydesc = document.getElementById("memsopttextarea").innerHTML;
                    

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipOptions',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_option_id": $scope.membership_option_id,
                        "membership_title": $scope.optioncategoryname,
                        "membership_subtitle": $scope.optioncategorysubname,
                        "membership_description": $scope.optioncategorydesc,
                        "category_status": $scope.category_status,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $scope.isAddedMemOptions = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Membership option successfully updated');

                        $scope.msadded = $scope.msoptionadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                            $scope.membership_option_id = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_option_id;
                            $scope.show_in_app_flg = $scope.child_membership_options[$scope.selectedMemOptIndex].show_in_app_flg;
                            if($scope.show_in_app_flg === 'Y'){
                                $scope.show_in_app = true;
                            }else{
                                $scope.show_in_app = false;
                            }
                            $scope.msoptActiveMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_active;
                            $scope.msoptOnHoldMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_onhold;
                            $scope.msoptCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_title;
                            $scope.msoptSubCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_subtitle;
                            $scope.MemOptregurl = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_option_url;
                            $scope.optioncategorydesc = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_description;
                            $scope.memstructure = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_structure;
                            $scope.memProcessingfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_processing_fee_type;
                            $scope.memsignupfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_signup_fee;
                            $scope.memfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_fee;
                            $scope.mem_fee_include = $scope.child_membership_options[$scope.selectedMemOptIndex].initial_payment_include_membership_fee;
                            $scope.mempayfrequency = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_recurring_frequency;
                            $scope.memfreqcustomperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_type;
                            $scope.memfreqcustomvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_val;
                            $scope.prorateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].prorate_first_payment_flg;
                            $scope.recpayStartdate = $scope.child_membership_options[$scope.selectedMemOptIndex].delay_recurring_payment_start_flg;
                            $scope.memrecstartperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_type;
                            $scope.memrecstartvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_val;

                            if ($scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc.length > 0) {
                                    $scope.msdiscountList = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc;
                                    $scope.msdiscountListView = true;
                                    $scope.msdiscountView = true;
                                    $scope.msdiscountEditView = false;
                            } else {
                                $scope.msdiscountList = "";
                                $scope.msdiscountListView = false;
                                $scope.msdiscountView = false;
                                $scope.msdiscountEditView = false;
                            }
                            $scope.selecteddiscountindex = "";
                            $scope.ms_registration_columns = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].reg_columns;
                            $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies;
                            }
                            $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
                            $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                            $localStorage.preview_membership_childlist = $scope.child_membership_options;
                            $localStorage.childmembershipindex = $scope.selectedMemOptIndex;
                            $localStorage.preview_member_childlisthide = true;
                            $localStorage.preview_membershipchildlist = false;
                            $localStorage.childmembershipoptionadd = false;
                        }else{
                            $scope.child_membership_options = [];
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }; 
            
            
            $scope.clearAllLocalFields = function(){
                $localStorage.PreviewMemProcessingFeeType = "";
                $localStorage.PreviewMemSignUpFee = "";
                $localStorage.PreviewMembershipFee = "";
                
                $localStorage.PreviewMembershipStructure = "";
                $localStorage.PreviewMembershipDepositValue = "";
                $localStorage.PreviewNoofClass = "";
                $localStorage.PreviewMembershipExpiryValue = "";
                $localStorage.PreviewMembershipExpiryStatus = "";
                $localStorage.PreviewMembershipExpiryPeriod = "";
                
                $localStorage.PreviewMembershipNoOfPayments = "";
                $localStorage.PreviewMembershipPaymentStartDate = "";
                $localStorage.PreviewMemCustomPaymentStartDate = "";
                $localStorage.Previewspecific_payment_frequency = "";
                $localStorage.Previewspecific_selecteddays = "";
                $localStorage.Previewexclude_from_billing_flag = "";
                
                $localStorage.Previewspecific_start_date = '';
                $localStorage.Previewspecific_end_date = '';
                $localStorage.Previewspecific_payment_frequency = '';
                $localStorage.Previewspecific_selecteddays = '';
                $localStorage.Previewexclude_from_billing_flag = '';
                $localStorage.preview_exclude_days_array = '';
            };
            
            $scope.showMemOption = function () {
                $scope.showmembershipoption = $scope.msadded = true;
                $scope.selectedMemOptIndex = '';
                $scope.msoptionadded = false;
                $scope.no_of_payment_status = false;
                $scope.excludeBillingButton_status = false;
                $scope.isAddedMemOptions = 'N';
                $scope.membership_option_id = $scope.msoptCategory = $scope.msoptSubCategory = $scope.optioncategorydesc ='';
                
                $scope.addexcludestartenddateError = $scope.addExcludelimitError = $scope.addstartenddateExcludelimitError = false;
                $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = $scope.addstartenddateExcludelimitErrortxt = "";
                $scope.addstartenddateError = false;
                $scope.addstartenddateErrortxt = "";
                
                $scope.memstructure = 'OE';          // Open Enrollment in payment
                $scope.memProcessingfee = '2' ;
                $scope.memsignupfee = $scope.memfee = '';
                $scope.mempayfrequency = 'M';
                $scope.memfreqcustomperiod = 'CM';
                $scope.memfreqcustomvalue = '';
                $scope.prorateValue ='Y';
                $scope.recpayStartdate = 'N';                
                $scope.memrecstartperiod = 'DM';
                $scope.memrecstartvalue ='';
                $scope.mem_fee_include = 'Y';
                
                $scope.memexpiryvalue = 1;              // Number of class in payment
                $scope.billing_depositamount = 0;
                $scope.billing_noofpayment = 1;
                $scope.noofclass = 0;
                $scope.billingoptionvalue = 'PF';
                $scope.memexpiryperiod = 'M';                
                $scope.exp_date_status = 'Y';
                $scope.billing_paymentstartdateValue = '5';
                $scope.billing_paymentfreqValue = 'M';
                
                $scope.attendance_status='N';
                $scope.attendance_period_type ='CPW';
                $scope.attendance_period = '';
                
                $scope.addstartdate = $scope.addenddate = ''; 
                $scope.specific_payment_frequency = 'PW';
                $scope.exclude_from_billing_flag = 'N';
                $scope.billingSEoptionvalue = 'PF';
                $scope.SEmemtotal_fee = 0;
                $scope.SEmemabsorb_fee = 0; 
                $scope.selecteddays = '';
                $scope.daySelectionExists =false;
                $scope.days_list = [{'day':'Monday','Selected': true,'formatted':'mon'},
                {'day':'Tuesday','Selected': true,'formatted':'tue'}, 
                {'day':'Wednesday','Selected': true,'formatted':'wed'}, 
                {'day':'Thursday','Selected': true,'formatted':'thu'}, 
                {'day':'Friday','Selected': true,'formatted':'fri'},
                {'day':'Saturday','Selected': false,'formatted':'sat'}, 
                {'day':'Sunday','Selected': false,'formatted':'sun'}]; 
                $scope.catchSelectedDays();      
                $scope.excludeBillingButton_status = false;
                $scope.editExcludeDatesStatus= false; 
                $scope.excludebillingstartdate = '';
                $scope.excludebillingenddate = '';
                $scope.exclude_days_array = [];
                $scope.MemOptregurl = '';
                
                $scope.msdiscountList = '';
//                $scope.ms_registration_columns = '';
//                $scope.ms_waiverpolicy = '';
                $scope.discount_off = 'M';
                $localStorage.childmembershipindex = '';
                $localStorage.preview_member_childlisthide = false;
                $localStorage.preview_membershipchildlist = true;
                $localStorage.PreviewMembershipOptionTitle = '';
                $localStorage.PreviewMembershipOptionSubtitle = '';
                $localStorage.PreviewMembershipOptionDesc = '';
//                $localStorage.PreviewMembershipWaiver = '';
//                $localStorage.PreviewMembershipRegcols = '';
                $localStorage.childmembershipoptionadd = true;
                $localStorage.PreviewMemProcessingFeeType = '';
                $localStorage.PreviewMemSignUpFee = '';
                $localStorage.PreviewMembershipFee = '';
                $localStorage.PreviewMemRecurringFrequency = '';
                $localStorage.PreviewMembershipBillingOptionValue = '';
                
                $localStorage.PreviewMembershipStructure = 'OE';
                $localStorage.PreviewMembershipDepositValue = 0;
                $localStorage.PreviewNoofClass = '';
                $localStorage.PreviewMembershipExpiryValue = 1;
                $localStorage.PreviewMembershipExpiryStatus = 'Y';
                $localStorage.PreviewMembershipExpiryPeriod = 'M';
                $localStorage.PreviewMembershipBillingOptionValue = 'PF';
                $localStorage.PreviewMembershipNoOfPayments = 0;
                $localStorage.PreviewMembershipPaymentStartDate = '1'; 
                $localStorage.preview_exclude_days_array = '';
                $localStorage.Previewspecific_start_date = '';
                $localStorage.Previewspecific_end_date = '';
                
                $localStorage.preview_membership_option_add = 'add';
                $scope.opentabbar('memoptdetails');
            };    
            
            $scope.cancelMemOptDetail = function () {
                $scope.msadded = true;
                $scope.selectedMemOptIndex = '';
                $localStorage.preview_membership_childlist = $scope.child_membership_options;   
                $localStorage.childmembershipindex = '';
                $localStorage.preview_member_childlisthide = false;
                $localStorage.preview_membershipchildlist = true;
                $scope.showmembershipoption = $scope.msoptionadded = false;
                $scope.excludeBillingButton_status = false;
                $scope.no_of_payment_status = false;
                $scope.membership_option_id = $scope.msoptCategory = $scope.msoptSubCategory = $scope.optioncategorydesc ='';
                
                $scope.addexcludestartenddateError = $scope.addExcludelimitError = $scope.addstartenddateExcludelimitError = false;
                $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = $scope.addstartenddateExcludelimitErrortxt = "";
                $scope.addstartenddateError = false;
                $scope.addstartenddateErrortxt = "";
                
                $scope.memstructure = 'OE';          // Open Enrollment in payment
                $scope.memProcessingfee = '2' ;
                $scope.memsignupfee = $scope.memfee = '';
                $scope.mempayfrequency = 'M';
                $scope.memfreqcustomperiod = 'CM';
                $scope.memfreqcustomvalue = '';
                $scope.prorateValue ='Y';
                $scope.recpayStartdate = 'N';                
                $scope.memrecstartperiod = 'DM';
                $scope.memrecstartvalue ='';
                $scope.mem_fee_include = 'Y';
                
                $scope.memexpiryvalue = 1;              // Number of class in payment
                $scope.billing_depositamount = 0;
                $scope.billing_noofpayment = 1;
                $scope.noofclass = 0;
                $scope.billingoptionvalue = 'PF';                
                $scope.exp_date_status = 'Y';
                $scope.memexpiryperiod = 'M';
                $scope.billing_paymentstartdateValue = '5';
                $scope.billing_paymentfreqValue = 'M';
                
                $scope.msdiscountList = '';
//                $scope.ms_registration_columns = '';
//                $scope.ms_waiverpolicy = '';                
                $scope.ms_waiverpolicy = $scope.exist_ms_waiverpolicy;
                $scope.discount_off = 'M';
                
                $scope.attendance_status='N';
                $scope.attendance_period_type ='CPW';
                $scope.attendance_period = '';
                
                $scope.addstartdate = $scope.addenddate = ''; 
                $scope.specific_payment_frequency = 'PW';
                $scope.exclude_from_billing_flag = 'N';
                $scope.billingSEoptionvalue = 'PF';
                $scope.SEmemtotal_fee = 0;
                $scope.SEmemabsorb_fee = 0; 
                $scope.selecteddays = '';
                $scope.daySelectionExists =false;
                $scope.days_list = [{'day':'Monday','Selected': true,'formatted':'mon'},
                {'day':'Tuesday','Selected': true,'formatted':'tue'}, 
                {'day':'Wednesday','Selected': true,'formatted':'wed'}, 
                {'day':'Thursday','Selected': true,'formatted':'thu'}, 
                {'day':'Friday','Selected': true,'formatted':'fri'},
                {'day':'Saturday','Selected': false,'formatted':'sat'}, 
                {'day':'Sunday','Selected': false,'formatted':'sun'}]; 
                $scope.catchSelectedDays(); 
                $scope.excludeBillingButton_status = false;
                $scope.editExcludeDatesStatus= false; 
                $scope.excludebillingstartdate = '';
                $scope.excludebillingenddate = '';
                $scope.exclude_days_array = [];
                $scope.MemOptregurl = '';
                
                $localStorage.PreviewMembershipOptionTitle = '';
                $localStorage.PreviewMembershipOptionSubtitle = '';
                $localStorage.PreviewMembershipOptionDesc = '';
                $localStorage.PreviewMembershipWaiver = $scope.ms_waiverpolicy;
//                $localStorage.PreviewMembershipRegcols = '';
                $localStorage.PreviewMemProcessingFeeType = '';
                $localStorage.PreviewMemSignUpFee = '';
                $localStorage.PreviewMembershipFee = '';
                $localStorage.PreviewMemRecurringFrequency = '';
                $localStorage.childmembershipoptionadd = false;
                $localStorage.preview_membership_option_add = ''; 
                $localStorage.PreviewMembershipBillingOptionValue = '';
                
                $localStorage.PreviewMembershipStructure = '';
                $localStorage.PreviewMembershipDepositValue = "";
                $localStorage.PreviewNoofClass = '';
                $localStorage.PreviewMembershipExpiryValue = "";
                $localStorage.PreviewMembershipExpiryStatus = "";
                $localStorage.PreviewMembershipExpiryPeriod = "";
                $localStorage.PreviewMembershipBillingOptionValue = "";
                $localStorage.PreviewMembershipNoOfPayments = "";
                $localStorage.PreviewMembershipPaymentStartDate = "";
                $localStorage.preview_exclude_days_array = "";                
                $scope.clearAllLocalFields();
                $scope.opentabbar('membershipoptions');
            };
            
            $scope.editMemOptDetail = function (mem_detail,ind) { 
                $scope.selectedMemOptIndex = ind;
                $scope.recur_Amount_status_TMF = true;
                $scope.deposit_Amount_status = false;
                $scope.deposit_Amount_exists = false;
                $scope.recur_Amount_status_DepAmnt = true;
                $scope.showmembershipoption = true;
                $scope.isAddedMemOptions = 'Y';
                $scope.billingSEoptionvalue ='PF';
                $localStorage.preview_membership_option_add = '';
                $localStorage.PreviewMembershipBillingOptionValue = '';
                $localStorage.Previewspecific_selecteddays = '';
                $localStorage.childmembershipindex = $scope.selectedMemOptIndex;
                $localStorage.preview_member_childlisthide = true;
                $localStorage.preview_membershipchildlist = false;
                $scope.msadded = $scope.msoptionadded = true;
                $localStorage.PreviewMemProcessingFeeType = '';
                $localStorage.PreviewMemSignUpFee = '';
                $localStorage.PreviewMembershipFee = '';
                $localStorage.PreviewMemRecurringFrequency = '';
                $localStorage.PreviewNoofClass = '';
                
                $scope.clearAllLocalFields();
                
                $scope.addexcludestartenddateError = $scope.addExcludelimitError = $scope.addstartenddateExcludelimitError = false;
                $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = $scope.addstartenddateExcludelimitErrortxt = "";
                $scope.addstartenddateError = false;
                $scope.addstartenddateErrortxt = "";
                
                $scope.membership_option_id = mem_detail.membership_option_id;
                $scope.msoptCategory = mem_detail.membership_title;
                $scope.msoptSubCategory = mem_detail.membership_subtitle;
                $scope.optioncategorydesc = mem_detail.membership_description;
                $scope.memstructure = mem_detail.membership_structure;
                $scope.memProcessingfee = mem_detail.membership_processing_fee_type;
                $scope.memsignupfee = mem_detail.membership_signup_fee;
                $scope.memfee = mem_detail.membership_fee;
                $scope.mem_fee_include = mem_detail.initial_payment_include_membership_fee;
                $scope.attendance_status= mem_detail.attendance_limit_flag;
                $scope.attendance_period_type = mem_detail.attendance_period;
                $scope.attendance_period = mem_detail.no_of_classes;
                if($scope.memstructure === 'OE'){                                        
                    $scope.mempayfrequency = mem_detail.membership_recurring_frequency;
                    $scope.memfreqcustomperiod = mem_detail.custom_recurring_frequency_period_type;
                    $scope.memfreqcustomvalue = mem_detail.custom_recurring_frequency_period_val;
                    $scope.prorateValue = mem_detail.prorate_first_payment_flg;
                    $scope.recpayStartdate = mem_detail.delay_recurring_payment_start_flg;
                    $scope.memrecstartperiod = mem_detail.delayed_recurring_payment_type;
                    $scope.memrecstartvalue = mem_detail.delayed_recurring_payment_val;
                    $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                    $localStorage.PreviewMembershipCustPeriod = $scope.memfreqcustomperiod;
                    $localStorage.PreviewMembershipCustValue = $scope.memfreqcustomvalue; 
                }else if($scope.memstructure === 'NC' || $scope.memstructure === 'C'){
                    $scope.billing_paymentfreqValue = mem_detail.membership_recurring_frequency;
                    $scope.billingoptionvalue = mem_detail.billing_options;
                    $localStorage.PreviewMembershipBillingOptionValue = $scope.billingoptionvalue;
                    $scope.noofclass = mem_detail.no_of_classes;
                    $scope.exp_date_status = mem_detail.expiration_status;
                    $scope.memexpiryperiod = mem_detail.expiration_date_type;
                    $scope.memexpiryvalue = mem_detail.expiration_date_val;
                    if(parseInt($scope.memexpiryvalue) === 0){
                       $scope.exp_date_status = 'N'; 
                    }
                    if($scope.memstructure === 'C' && $scope.exp_date_status === 'N'){
                        $scope.exp_date_status = 'Y';
                        $scope.memexpiryperiod = 'M';
                        $scope.memexpiryvalue = 1;
                    }
                    $scope.billing_depositamount = mem_detail.deposit_amount;
                    $scope.billing_noofpayment = mem_detail.no_of_payments;
                    if($scope.billingoptionvalue === 'PP' && parseFloat($scope.billing_noofpayment) > 0){
                        $scope.no_of_payment_status = false;
                    }else{
                        $scope.no_of_payment_status = true;
                    }
                    $scope.billing_paymentstartdateValue = mem_detail.billing_payment_start_date_type;                    
                    if($scope.billing_paymentstartdateValue === '4'){
                        $scope.memcustompaymntstrtdte = $scope.formatserverdate(mem_detail.billing_options_payment_start_date);
                    }else{
                        var curr_date = new Date();
                        $scope.memcustompaymntstrtdte = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                    }
                    $localStorage.PreviewMemRecurringFrequency = $scope.billing_paymentfreqValue; 
                    
                }else if($scope.memstructure === 'SE'){
                    
                    $scope.billingSEoptionvalue = mem_detail.billing_options;  
                    $localStorage.PreviewMembershipBillingOptionValue = $scope.billingSEoptionvalue;
                    $scope.addstartdate = $scope.formatserverdate(mem_detail.specific_start_date);
                    $scope.addenddate = $scope.formatserverdate(mem_detail.specific_end_date);  
                    $localStorage.Previewspecific_start_date = $scope.dateformat($scope.addstartdate);
                    $localStorage.Previewspecific_end_date = $scope.dateformat($scope.addenddate);
                    $scope.specific_payment_frequency =  mem_detail.specific_payment_frequency;
                    $scope.exclude_from_billing_flag = mem_detail.exclude_from_billing_flag;
                    $scope.daySelectionExists = false;
                    $scope.selecteddays = mem_detail.billing_days;//kamal
                    for (var i = 0; i < $scope.selecteddays.length; i++) {
                        if($scope.selecteddays[i] === '1'){
                            $scope.days_list[i].Selected = true;
                        }else{
                            $scope.days_list[i].Selected = false;
                        }
                    }
                    $scope.catchSelectedDays(); 
                    $scope.exclude_days_array = mem_detail.mem_billing_exclude; 
                    if($scope.exclude_days_array.length > 0){
                        $localStorage.preview_exclude_days_array = $scope.exclude_days_array;
                    }else{
                        $localStorage.preview_exclude_days_array = '';
                    }
                    
                    var curr_date = new Date();
                    var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                    var membership_startdate = new Date($scope.dateformat($scope.addstartdate));
                    var membership_enddate = new Date($scope.dateformat($scope.addenddate));
                    if((+server_curr_date > +membership_startdate) && (+server_curr_date < +membership_enddate) ){   
                        $('#addstartdate').datepicker('setStartDate', new Date());
                        $('#addstartdate').datepicker('setEndDate', $scope.addenddate);
                        $('#addenddate').datepicker('setStartDate', new Date());
                        $('#addenddate').datepicker('setEndDate', $scope.addenddate);
                    }else if((+server_curr_date > +membership_startdate) && (+server_curr_date > +membership_enddate) ){   
                        $('#addstartdate').datepicker('setStartDate', new Date());
                        $('#addstartdate').datepicker('setEndDate', null);
                        $('#addenddate').datepicker('setStartDate', new Date());
                        $('#addenddate').datepicker('setEndDate', null);
                    }else{
                        $('#addstartdate').datepicker('setStartDate',new Date());
                        $('#addstartdate').datepicker('setEndDate', $scope.addenddate);
                        $('#addenddate').datepicker('setStartDate', $scope.addstartdate);
                        $('#addenddate').datepicker('setEndDate', null);
                    }
                    $scope.openexcludeBilling();
                    $scope.excludeBillingButton_status = false;
                }
                
                if (mem_detail.membership_disc.length > 0) {
                    $scope.msdiscountList = mem_detail.membership_disc;
                    $scope.msdiscountListView = true;
                    $scope.msdiscountView = true;
                    $scope.msdiscountEditView = false;
                } else {
                    $scope.msdiscountList = "";
                    $scope.msdiscountListView = false;
                    $scope.msdiscountView = false;
                    $scope.msdiscountEditView = false;
                }
                $scope.selecteddiscountindex = "";
                $scope.ms_registration_columns = mem_detail.reg_columns;
                if($scope.ms_registration_columns.length >= 10) {
                    $scope.ms_regfieldexceeds = true;
                } else {
                    $scope.ms_regfieldexceeds = false;
                }
                $scope.ms_selectedregcolindex = "";
                if(mem_detail.waiver_policies === null) {
                    $scope.ms_waiverpolicy = '';
                } else {
                    $scope.ms_waiverpolicy = mem_detail.waiver_policies;
                } 
                $scope.MemOptregurl = mem_detail.membership_option_url;                
                               
                $localStorage.PreviewMembershipOptionTitle = $scope.msoptCategory;
                $localStorage.PreviewMembershipOptionSubtitle = $scope.msoptSubCategory;
//                $localStorage.PreviewMembershipOptionDesc = $scope.optioncategorydesc;
                $localStorage.PreviewMembershipWaiver = $scope.ms_waiverpolicy;
                $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                $localStorage.childmembershipoptionadd = false;
                $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
                $localStorage.PreviewMemSignUpFee = $scope.memsignupfee;
                $localStorage.PreviewMembershipFee = $scope.memfee;
                
                $localStorage.PreviewMembershipStructure = $scope.memstructure;
                $localStorage.PreviewMembershipDepositValue = $scope.billing_depositamount;
                $localStorage.PreviewNoofClass = $scope.noofclass;
                $localStorage.PreviewMembershipExpiryValue = $scope.memexpiryvalue;
                $localStorage.PreviewMembershipExpiryStatus = $scope.exp_date_status;
                $localStorage.PreviewMembershipExpiryPeriod = $scope.memexpiryperiod;
                
                $localStorage.PreviewMembershipNoOfPayments = $scope.billing_noofpayment;
                $localStorage.PreviewMembershipPaymentStartDate = $scope.billing_paymentstartdateValue;
                $localStorage.PreviewMemCustomPaymentStartDate = $scope.memcustompaymntstrtdte;
                $localStorage.Previewspecific_payment_frequency = $scope.specific_payment_frequency;
                $localStorage.Previewspecific_selecteddays = $scope.selecteddays;
                $localStorage.Previewexclude_from_billing_flag = $scope.exclude_from_billing_flag;
                
                $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                $scope.opentabbar('memoptdetails');
            };
           
           $scope.mempayfreqSelection = function () { 
                    $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                    $scope.memfreqcustomvalue = '';
                    $scope.memfreqcustomperiod = 'CM';
                    if($scope.mempayfrequency==='W'){                       
                        $scope.prorateValue = 'N';
                        $scope.memrecstartperiod = 'DW';
                    }else if($scope.mempayfrequency==='B'){
                        $scope.prorateValue = 'Y';
                        $scope.memrecstartperiod = 'DM';
                    }else if($scope.mempayfrequency==='M'){
                        $scope.prorateValue = 'Y';
                        $scope.memrecstartperiod = 'DM';
                    }else if($scope.mempayfrequency==='A'){
                        $scope.prorateValue = 'N';
                        $scope.memrecstartperiod = 'DM';
                    }else if($scope.mempayfrequency==='C'){
                        $scope.prorateValue = 'N';
                        $scope.memrecstartperiod = 'DM';
                        $localStorage.PreviewMembershipCustPeriod = 'CM';
                    }else if($scope.mempayfrequency==='N'){
                        $scope.prorateValue = 'N';
                        $scope.recpayStartdate = 'N';
                    }
                    
           };           
           
            $scope.$on('$viewContentLoaded', function () {
                $('#memcustompaymntstrtdte').datepicker({
                    format: 'mm/dd/yyyy',
                    ignoreReadonly: true,
                    autoclose: true
                });
            });

            $("#memcustompaymntstrtdte").on("dp.change", function () {
                $scope.memcustompaymntstrtdte = $("#memcustompaymntstrtdte").val();
                
            });
           
           $scope.paymentStartdateSelection = function () { 
               $localStorage.PreviewMembershipPaymentStartDate = $scope.billing_paymentstartdateValue;
           };
           
           $scope.catchMemPaymntStrtDte = function () { 
               $localStorage.PreviewMemCustomPaymentStartDate = $scope.dateformat($scope.memcustompaymntstrtdte);
           };
           
           $scope.paymentfreqSelection = function () { 
               $localStorage.PreviewMemRecurringFrequency = $scope.billing_paymentfreqValue;
           };
           
           
            $scope.recpayStartdateSelection = function () {
                    $scope.memrecstartvalue = '';
            };
            
           $scope.updateMemPayment = function () {
                $('#progress-full').show();

                    if ($scope.memstructure === "" || $scope.memstructure === undefined || $scope.memstructure === null ) {
                        $scope.membership_structure = "";
                    } else {
                        $scope.membership_structure = $scope.memstructure;
                    }
                    
                    if ($scope.memProcessingfee === "" || $scope.memProcessingfee === undefined || $scope.memProcessingfee === null ) {
                        $scope.membership_processing_fee = "2";
                    } else {
                        $scope.membership_processing_fee = $scope.memProcessingfee;
                    }
                    
                    if ($scope.memsignupfee === "" || $scope.memsignupfee === undefined || $scope.memsignupfee === null || (parseFloat($scope.memsignupfee) == 0)) {
                        $scope.membership_signup_fee = "0.00";
                    } else if ($scope.msdiscountList.length > 0 && $scope.msdiscountList !== undefined && $scope.msdiscountList !== '' && $scope.msdiscountList !== null) {
                        for (var i = 0; i < $scope.msdiscountList.length; i++) {
                            if ($scope.msdiscountList[i].discount_off === 'S') {
                                if ($scope.msdiscountList[i].discount_type === 'V') {
                                    var check_dollar_discount = parseFloat($scope.memsignupfee) - parseFloat($scope.msdiscountList[i].discount_amount);
                                    if (parseFloat('5') <= parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount)) {    
                                        $scope.membership_signup_fee = $scope.memsignupfee;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text('Sign up fee must be at-least '+$scope.wp_currency_symbol+'5 when discount value is applied');
                                        return false;
                                    }
                                } else if ($scope.msdiscountList[i].discount_type === 'P') {
                                    var check_percent_discount = parseFloat($scope.memsignupfee) - (((parseFloat($scope.memsignupfee) * parseFloat($scope.msdiscountList[i].discount_amount)) / parseFloat(100)));
                                    if (parseFloat('5') <= parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                        $scope.membership_signup_fee = $scope.memsignupfee;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Sign up fee must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                        return false;
                                    }
                                }
                            } else {
                                if (parseFloat($scope.memsignupfee) >= 5) {
                                    $scope.membership_signup_fee = $scope.memsignupfee;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Sign up fee must be at-least "+$scope.wp_currency_symbol+"5");
                                    return false;
                                }
                            }
                        }

                    } else {
                        if (parseFloat($scope.memsignupfee) >= 5) {
                            $scope.membership_signup_fee = $scope.memsignupfee;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Sign up fee must be at-least "+$scope.wp_currency_symbol+"5");
                            return false;
                        }
                    }
                    
                    if ($scope.memfee === "" || $scope.memfee === undefined || $scope.memfee === null || (parseFloat($scope.memfee) == 0)) {
                        $scope.membership_fee = "0.00";
                    } else if ($scope.msdiscountList.length > 0 && $scope.msdiscountList !== undefined && $scope.msdiscountList !== '' && $scope.msdiscountList !== null) {
                        for (var i = 0; i < $scope.msdiscountList.length; i++) {
                            if ($scope.msdiscountList[i].discount_off === 'M') {
                                if ($scope.msdiscountList[i].discount_type === 'V') {
                                    var check_dollar_discount = parseFloat($scope.memfee) - parseFloat($scope.msdiscountList[i].discount_amount);
                                    if (parseFloat('5') <= parseFloat(check_dollar_discount) || parseFloat('0') == parseFloat(check_dollar_discount)) {                                        
                                        if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (+$scope.memfee - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.membership_fee = $scope.memfee;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.membership_fee = $scope.memfee;
                                        }
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Membership fee must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                        return false;
                                    }
                                } else if ($scope.msdiscountList[i].discount_type === 'P') {
                                    var check_percent_discount = parseFloat($scope.memfee) - (((parseFloat($scope.memfee) * parseFloat($scope.msdiscountList[i].discount_amount)) / parseFloat(100)));
                                    if (parseFloat('5') <= parseFloat(check_percent_discount) || parseFloat('0') == parseFloat(check_percent_discount)) {
                                        if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (+$scope.memfee - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.membership_fee = $scope.memfee;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.membership_fee = $scope.memfee;
                                        }
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Membership fee must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                        return false;
                                    }
                                }
                            } else {
                                if (parseFloat($scope.memfee) >= 5) {
                                    $scope.membership_fee = $scope.memfee;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Membership fee must be at-least "+$scope.wp_currency_symbol+"5");
                                    return false;
                                }
                            }
                        }

                    } else {
                        if (parseFloat($scope.memfee) >= 5) {
                            $scope.membership_fee = $scope.memfee;
                        } else {
                            $('#progress-full').hide();
                            $("#failuremessageModal").modal('show');
                            $("#failuremessagetitle").text('Message');
                            $("#failuremessagecontent").text("Membership fee must be at-least "+$scope.wp_currency_symbol+"5");
                            return false;
                        }
                    }
                    
                    
                    if($scope.memstructure === "OE" ){
                    
                        if ($scope.mempayfrequency === "C") {
                            $scope.membership_pay_frequency = $scope.mempayfrequency;
                            $scope.membership_pay_frequency_custom_period =  $scope.memfreqcustomperiod;
                            $scope.membership_pay_frequency_custom_value =  $scope.memfreqcustomvalue;
                        } else {
                            $scope.membership_pay_frequency = $scope.mempayfrequency;
                            $scope.membership_pay_frequency_custom_period = '';
                            $scope.membership_pay_frequency_custom_value = '';
                        }

                        if ($scope.mempayfrequency === "M" || $scope.mempayfrequency === "B") {
                            $scope.membership_prorate = $scope.prorateValue;
                        } else {
                            $scope.membership_prorate = 'N';
                        }

                        if ($scope.recpayStartdate === "Y") {
                            $scope.membership_delay_rec_frequency = $scope.recpayStartdate;
                            $scope.membership_delay_rec_period = $scope.memrecstartperiod;
                            $scope.membership_delay_rec_value = $scope.memrecstartvalue;
                        } else {
                            $scope.membership_delay_rec_frequency = $scope.recpayStartdate;
                            $scope.membership_delay_rec_period = $scope.memrecstartperiod;
                            $scope.membership_delay_rec_value = '';
                        } 
                        
                        if($scope.attendance_status === "Y"){                            
                            if ($scope.attendance_period === "" || $scope.attendance_period === undefined || $scope.attendance_period === null ) {
                                $scope.membership_no_of_class = 0;
                            } else {
                                $scope.membership_no_of_class = $scope.attendance_period;
                            } 
                        }else{                            
                            $scope.membership_no_of_class = 0;
                            $scope.attendance_period_type = "CPW";
                        }
                        
                        $scope.membership_expiry_value = '';
                        $scope.membership_expiry_type = '';
                        $scope.membership_depositamount = 0;
                        $scope.billing_option_type = 'PF';
                        $scope.paymentstartdate_type = '5';
                        $scope.membership_no_of_payment = 0;
                        
                        $scope.specific_start_date = "";
                        $scope.specific_end_date = "";
                        $scope.specific_payment_frequency = 'PW';
                        $scope.exclude_from_billing = "";
                        $scope.selecteddays = "";
                        $scope.current_days_order = "";
                        
                    }else if($scope.memstructure === "NC" || $scope.memstructure === "C"){
                        
                        if($scope.memstructure === "NC"){                            
                            if ($scope.noofclass === "" || $scope.noofclass === undefined || $scope.noofclass === null ) {
                                $scope.membership_no_of_class = 0;
                            } else {
                                $scope.membership_no_of_class = $scope.noofclass;
                            } 
                            $scope.attendance_status = "Y";
                            $scope.attendance_period_type = "CPW";
                        }else if($scope.memstructure === "C"){                            
                           if($scope.attendance_status === "Y"){                            
                                if ($scope.attendance_period === "" || $scope.attendance_period === undefined || $scope.attendance_period === null ) {
                                    $scope.membership_no_of_class = 0;
                                } else {
                                    $scope.membership_no_of_class = $scope.attendance_period;
                                } 
                            }else{                            
                                $scope.membership_no_of_class = 0;
                                $scope.attendance_period_type = "CPW";
                            }
                        }
                        
                        
                        if ($scope.memexpiryvalue === "" || $scope.memexpiryvalue === undefined || $scope.memexpiryvalue === null || $scope.exp_date_status === 'N') {
                            $scope.membership_expiry_value = '';
                            $scope.membership_expiry_type = '';
                        } else {
                            $scope.membership_expiry_value = $scope.memexpiryvalue;
                            $scope.membership_expiry_type = $scope.memexpiryperiod;
                        }
                        
                        if ($scope.billing_depositamount === "" || $scope.billing_depositamount === undefined || $scope.billing_depositamount === null ) {
                            $scope.membership_depositamount = 0;
                        } else {
                            $scope.membership_depositamount = $scope.billing_depositamount;
                        }
                        
                        if ($scope.billingoptionvalue === "PP") {
                            $scope.billing_option_type = $scope.billingoptionvalue;
                            $scope.paymentstartdate_type = $scope.billing_paymentstartdateValue;
                            $scope.membership_pay_frequency = $scope.billing_paymentfreqValue;

                            if ($scope.billing_noofpayment === "" || $scope.billing_noofpayment === undefined || $scope.billing_noofpayment === null) {
                                $scope.membership_no_of_payment = 0;
                            } else {
                                $scope.membership_no_of_payment = $scope.billing_noofpayment;
                            }
                            
                            if($scope.paymentstartdate_type === '4'){
                                $scope.custom_paymentstartdate = $scope.dateformat($scope.memcustompaymntstrtdte);
                            }else{
                                $scope.custom_paymentstartdate = '';
                            }

                        } else {
                            $scope.billing_option_type = 'PF';
                            $scope.paymentstartdate_type = '5';
                            $scope.membership_pay_frequency = 'M';
                            $scope.membership_no_of_payment = 1;
                        }                        
                        
                        $scope.membership_pay_frequency_custom_period = 'CM';
                        $scope.membership_pay_frequency_custom_value = '';
                        $scope.membership_prorate = 'N';
                        $scope.membership_delay_rec_period = 'DM';
                        $scope.membership_delay_rec_value = '';                        
                        
                        $scope.specific_start_date = "";
                        $scope.specific_end_date = "";
                        $scope.specific_payment_frequency = 'PW';
                        $scope.exclude_from_billing = "";
                        $scope.selecteddays = "";
                        $scope.current_days_order = "";
                        
                    }else if($scope.memstructure === "SE" ){
                        $scope.specific_start_date = $scope.dateformat($scope.addstartdate);
                        $scope.specific_end_date = $scope.dateformat($scope.addenddate); //kamal
                        $scope.exclude_from_billing = $scope.exclude_from_billing_flag;
                        
                        if ($scope.billingSEoptionvalue === "PP") {
                            $scope.billing_option_type = $scope.billingSEoptionvalue;
                        } else {
                            $scope.billing_option_type = 'PF';
                        }
                        
                        if($scope.attendance_status === "Y"){                            
                            if ($scope.attendance_period === "" || $scope.attendance_period === undefined || $scope.attendance_period === null ) {
                                $scope.membership_no_of_class = 0;
                            } else {
                                $scope.membership_no_of_class = $scope.attendance_period;
                            } 
                        }else{                            
                            $scope.membership_no_of_class = 0;
                            $scope.attendance_period_type = "CPW";
                        }
                        
                        $scope.membership_pay_frequency_custom_period = 'CM';
                        $scope.membership_pay_frequency_custom_value = '';
                        $scope.membership_prorate = 'N';
                        $scope.membership_delay_rec_period = 'DM';
                        $scope.membership_delay_rec_value = ''; 
                        $scope.membership_pay_frequency = 'M';  
                        
                        $scope.membership_expiry_value = '';
                        $scope.membership_expiry_type = '';
                        $scope.membership_depositamount = 0;
                        $scope.paymentstartdate_type = '5';
                        $scope.membership_no_of_payment = 0;
                    }

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipPaymentDetails',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_option_id": $scope.membership_option_id,
                        "membership_structure": $scope.membership_structure,
                        "processing_fee": $scope.membership_processing_fee,
                        "signup_fee": $scope.membership_signup_fee,
                        "membership_fee": $scope.membership_fee,
                        "initial_payment_include_membership_fee": $scope.mem_fee_include,                        
                        "recurring_membership_payment_frequency": $scope.membership_pay_frequency,
                        "custom_recurring_frequency_period_type": $scope.membership_pay_frequency_custom_period,
                        "custom_recurring_frequency_period_val": $scope.membership_pay_frequency_custom_value,
                        "prorate_first_payment_period_flg": $scope.membership_prorate,                        
                        "delay_recurring_payment_flg": $scope.membership_delay_rec_frequency,
                        "delayed_recurring_payment_type": $scope.membership_delay_rec_period,
                        "delayed_recurring_payment_val": $scope.membership_delay_rec_value,
                        "no_of_classes": $scope.membership_no_of_class,
                        "expiration_date_type": $scope.membership_expiry_type,                        
                        "expiration_date_val": $scope.membership_expiry_value,
                        "billing_options": $scope.billing_option_type,
                        "deposit_amount": $scope.membership_depositamount,
                        "no_of_payments": $scope.membership_no_of_payment,
                        "billing_payment_start_date_type": $scope.paymentstartdate_type,
                        "billing_options_payment_start_date": $scope.custom_paymentstartdate,                        
                        "category_status": $scope.category_status,
                        "company_id": $scope.company_id,
                        "expiration_status":$scope.exp_date_status,
                        "specific_start_date":$scope.specific_start_date,
                        "specific_end_date":$scope.specific_end_date,
                        "specific_payment_frequency":$scope.specific_payment_frequency,
                        "exclude_from_billing_flag":$scope.exclude_from_billing,
                        "billing_days":$scope.selecteddays,
                        "week_days_order":$scope.current_days_order,
                        "attendance_limit_flag":$scope.attendance_status,
                        "attendance_period_type":$scope.attendance_period_type
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $scope.isAddedMemOptions = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Membership option payment successfully updated');

                        $scope.msadded = $scope.msoptionadded = true;
                        $scope.membership_id = $scope.membership_details.membership_id;                                
                        $scope.mscategory = $scope.membership_details.category_title;
                        $scope.mssubcategory = $scope.membership_details.category_subtitle;
                        $scope.category_status = $scope.membership_details.category_status;
                        $scope.msurl = $scope.membership_details.category_video_url;
                        $scope.category_picture = $scope.membership_details.category_image_url;
                        $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                        $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                        $scope.msdesc = $scope.membership_details.category_description;
                        $scope.rank_columns = $scope.membership_details.rank_details;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                            $scope.membership_option_id = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_option_id;
                            $scope.show_in_app_flg = $scope.child_membership_options[$scope.selectedMemOptIndex].show_in_app_flg;
                            if($scope.show_in_app_flg === 'Y'){
                                $scope.show_in_app = true;
                            }else{
                                $scope.show_in_app = false;
                            }
                            $scope.msoptActiveMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_active;
                            $scope.msoptOnHoldMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_onhold;
                            $scope.msoptCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_title;
                            $scope.msoptSubCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_subtitle;
                            $scope.optioncategorydesc = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_description;
                            $scope.memstructure = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_structure; 
                            $scope.memProcessingfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_processing_fee_type;
                            $scope.memsignupfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_signup_fee;                                    
                            $scope.memfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_fee;
                            $scope.mem_fee_include = $scope.child_membership_options[$scope.selectedMemOptIndex].initial_payment_include_membership_fee;
                            if($scope.memstructure === 'OE'){                                        
                                $scope.mempayfrequency = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_recurring_frequency;
                                $scope.memfreqcustomperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_type;
                                $scope.memfreqcustomvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_val;
                                $scope.prorateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].prorate_first_payment_flg;
                                $scope.recpayStartdate = $scope.child_membership_options[$scope.selectedMemOptIndex].delay_recurring_payment_start_flg;
                                $scope.memrecstartperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_type;
                                $scope.memrecstartvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_val;

                                $scope.billingoptionvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_options;
                                $scope.noofclass = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_classes;
                                $scope.memexpiryperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_type;
                                $scope.memexpiryvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_val;
                                $scope.billing_depositamount = $scope.child_membership_options[$scope.selectedMemOptIndex].deposit_amount;
                                $scope.billing_noofpayment = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_payments;
                                $scope.billing_paymentstartdateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_payment_start_date_type;

                                $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                            }else if($scope.memstructure === 'NC' || $scope.memstructure === 'C'){
                                $scope.billing_paymentfreqValue = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_recurring_frequency;
                                $scope.billingoptionvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_options;
                                $scope.noofclass = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_classes;
                                $scope.exp_date_status = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_status;
                                $scope.memexpiryperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_type;
                                $scope.memexpiryvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_val;
                                $scope.billing_depositamount = $scope.child_membership_options[$scope.selectedMemOptIndex].deposit_amount;
                                $scope.billing_noofpayment = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_payments;
                                $scope.billing_paymentstartdateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_payment_start_date_type;
                                if($scope.billing_paymentstartdateValue === '4'){
                                    $scope.memcustompaymntstrtdte = $scope.formatserverdate($scope.child_membership_options[$scope.selectedMemOptIndex].billing_options_payment_start_date);
                                }else{
                                    var curr_date = new Date();
                                    $scope.memcustompaymntstrtdte = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                                }

                            }else if($scope.memstructure === 'SE'){
                                $scope.addstartdate = $scope.formatserverdate($scope.child_membership_options[$scope.selectedMemOptIndex].specific_start_date);
                                $scope.addenddate = $scope.formatserverdate($scope.child_membership_options[$scope.selectedMemOptIndex].specific_end_date);
                                $scope.billingSEoptionvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_options;   
                                $scope.specific_payment_frequency = $scope.child_membership_options[$scope.selectedMemOptIndex].specific_payment_frequency; 
                                $scope.exclude_from_billing_flag = $scope.child_membership_options[$scope.selectedMemOptIndex].exclude_from_billing_flag;
                                $scope.selecteddays = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_days; 
                                $scope.daySelectionExists = false;
                                if($scope.child_membership_options[$scope.selectedMemOptIndex].mem_billing_exclude.length > 0){
                                    $scope.exclude_days_array = $scope.child_membership_options[$scope.selectedMemOptIndex].mem_billing_exclude;
                                }else{
                                    $scope.exclude_days_array = [];
                                }
                                $localStorage.preview_exclude_days_array = $scope.exclude_days_array;
                            }
                            $scope.attendance_status= $scope.child_membership_options[$scope.selectedMemOptIndex].attendance_limit_flag;
                            $scope.attendance_period_type = $scope.child_membership_options[$scope.selectedMemOptIndex].attendance_period;
                            $scope.attendance_period = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_classes;

                            $localStorage.PreviewMemProcessingFeeType = $scope.memProcessingfee;
                            $localStorage.PreviewMemSignUpFee = $scope.memsignupfee;
                            $localStorage.PreviewMembershipFee = $scope.memfee;

                            if ($scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc.length > 0) {
                                    $scope.msdiscountList = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc;
                                    $scope.msdiscountListView = true;
                                    $scope.msdiscountView = true;
                                    $scope.msdiscountEditView = false;
                            } else {
                                $scope.msdiscountList = "";
                                $scope.msdiscountListView = false;
                                $scope.msdiscountView = false;
                                $scope.msdiscountEditView = false;
                            }
                            $scope.selecteddiscountindex = "";
                            $scope.ms_registration_columns = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].reg_columns;
                            $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies;
                            }

                        }else{
                            $scope.child_membership_options = [];
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
          $scope.catchExcludeBilling = function (exclude_flag) {
              if(exclude_flag==='N'){
                  if($scope.exclude_days_array.length > 0){
                      $("#AddCategorymessageModal").modal('show');
                      $("#AddCategorymessagetitle").text('Message');
                      $("#AddCategorymessagecontent").text("There are current existing dates to exclude billing. This option can only be changed after all current existing dates have been deleted.");
                      $scope.exclude_from_billing_flag = 'Y';
                  }else{
                      $scope.exclude_from_billing_flag = 'N';
                  }
              }else{
                  //exclude days addition                  
              }
              $localStorage.Previewexclude_from_billing_flag = $scope.exclude_from_billing_flag;
          };
           
           $scope.openexcludeBilling = function () {
               var check_start_date = $scope.child_membership_options[$scope.selectedMemOptIndex].specific_start_date;
               var check_end_date = $scope.child_membership_options[$scope.selectedMemOptIndex].specific_end_date;
               if(check_start_date !== null && check_end_date !== null && check_start_date !== '0000-00-00' && check_end_date !== '0000-00-00'){
                   $scope.addstartdate = $scope.formatserverdate($scope.child_membership_options[$scope.selectedMemOptIndex].specific_start_date);
                   $scope.addenddate = $scope.formatserverdate($scope.child_membership_options[$scope.selectedMemOptIndex].specific_end_date);
                   var server_exclude_flag = $scope.child_membership_options[$scope.selectedMemOptIndex].exclude_from_billing_flag;
                   if(server_exclude_flag === 'N' && $scope.exclude_from_billing_flag === 'Y'){
                       $("#AddCategorymessageModal").modal('show');
                       $("#AddCategorymessagetitle").text('Message');
                       $("#AddCategorymessagecontent").text("Kindly save Exclude billing option to proceed adding exclude dates");
                       return false;
                   }
               }else{
                   $("#AddCategorymessageModal").modal('show');
                   $("#AddCategorymessagetitle").text('Message');
                   $("#AddCategorymessagecontent").text("Kindly save this membership structure to proceed adding exclude dates");
                   return false;
               }
               $scope.excludeBillingButton_status=true;
               $scope.excludebillingstartdate = '';
               $scope.excludebillingenddate = '';
               $scope.listExcludeDatesView = true;
               $scope.editExcludeDatesView = false;
               $scope.selectedExcludeDateindex = '';
               $scope.addexcludestartenddateError = $scope.addExcludelimitError = $scope.addstartenddateExcludelimitError = false;
               $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = $scope.addstartenddateExcludelimitErrortxt = "";
               if($scope.child_membership_options[$scope.selectedMemOptIndex].mem_billing_exclude.length > 0){
                    $scope.exclude_days_array = $scope.child_membership_options[$scope.selectedMemOptIndex].mem_billing_exclude;
                }else{
                    $scope.exclude_days_array = "";
                }
                $localStorage.preview_exclude_days_array = $scope.exclude_days_array;
                $localStorage.Previewspecific_start_date =  $scope.dateformat($scope.addstartdate);
                $localStorage.Previewspecific_end_date =$scope.dateformat($scope.addenddate);
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                var membership_startdate = new Date($scope.dateformat($scope.addstartdate));
                var membership_enddate = new Date($scope.dateformat($scope.addenddate));
                if((+server_curr_date > +membership_startdate) && (+server_curr_date < +membership_enddate) ){   
                    $('#excludebillingstartdate').datepicker('setStartDate', new Date());
                    $('#excludebillingstartdate').datepicker('setEndDate', $scope.addenddate);
                    $('#excludebillingenddate').datepicker('setStartDate', new Date());
                    $('#excludebillingenddate').datepicker('setEndDate', $scope.addenddate);
                }else if((+server_curr_date > +membership_startdate) && (+server_curr_date > +membership_enddate) ){   
                    $('#excludebillingstartdate').datepicker('setStartDate', new Date());
                    $('#excludebillingstartdate').datepicker('setEndDate', null);
                    $('#excludebillingenddate').datepicker('setStartDate', new Date());
                    $('#excludebillingenddate').datepicker('setEndDate', null);
                }else{
                    $('#excludebillingstartdate').datepicker('setStartDate',$scope.addstartdate);
                    $('#excludebillingstartdate').datepicker('setEndDate', $scope.addenddate);
                    $('#excludebillingenddate').datepicker('setStartDate', $scope.addstartdate);
                    $('#excludebillingenddate').datepicker('setEndDate', $scope.addenddate);
                }
                
           };
           
           $scope.excludeCancel = function () {
               $scope.excludeBillingButton_status = false;
               $scope.excludebillingstartdate = '';
               $scope.excludebillingenddate = '';
           }
           
           $scope.editExcludeDates = function (exdates, ind) { //kamal
               $scope.editExcludeDatesStatus= true; 
               $scope.listExcludeDatesView = false;
               $scope.editExcludeDatesView = false;
               $scope.selectedExcludeDateindex = ind;                
               $scope.edittedexcludestartdate = $scope.formatserverdate(exdates.billing_exclude_startdate); 
               $scope.edittedexcludeenddate = $scope.formatserverdate(exdates.billing_exclude_enddate); 
               $scope.editexcludestartdate = $scope.formatserverdate(exdates.billing_exclude_startdate); 
               $scope.editexcludeenddate = $scope.formatserverdate(exdates.billing_exclude_enddate); 
                               
                $('.editexcludestartdatepicker').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: new Date(),
                    ignoreReadonly: true,
                    autoclose: true
                }).on('click', function(){
                    $(this).datepicker({showOn:'focus'}).focus();
                    
                }).on('changeDate',function(selected){
                  var startDate = new Date(selected.date.valueOf());
                  $('.editexcludeenddatepicker').datepicker('setStartDate',startDate);
                });
                
                $('.editexcludeenddatepicker').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: new Date(),
                    ignoreReadonly: true,
                    autoclose: true
                }).on('click', function(){
                    $(this).datepicker({showOn:'focus'}).focus();
                    
                }).on('changeDate',function(selected){
                    var endDate = new Date(selected.date.valueOf());
                    $('.editexcludestartdatepicker').datepicker('setEndDate',endDate);
                });  
                
                var curr_date = new Date();
                var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
                var membership_startdate = new Date($scope.dateformat($scope.addstartdate));
                var membership_enddate = new Date($scope.dateformat($scope.addenddate));
                if((+server_curr_date > +membership_startdate) && (+server_curr_date < +membership_enddate) ){   
                    $('.editexcludestartdatepicker').datepicker('setStartDate', new Date());
                    $('.editexcludestartdatepicker').datepicker('setEndDate', $scope.addenddate);
                    $('.editexcludeenddatepicker').datepicker('setStartDate', new Date());
                    $('.editexcludeenddatepicker').datepicker('setEndDate',  $scope.addenddate);
                }else if((+server_curr_date > +membership_startdate) && (+server_curr_date > +membership_enddate) ){   
                    $('.editexcludestartdatepicker').datepicker('setStartDate', new Date());
                    $('.editexcludestartdatepicker').datepicker('setEndDate', null);
                    $('.editexcludeenddatepicker').datepicker('setStartDate', new Date());
                    $('.editexcludeenddatepicker').datepicker('setEndDate', null);
                }else{
                    $('.editexcludestartdatepicker').datepicker('setStartDate',$scope.addstartdate);
                    $('.editexcludestartdatepicker').datepicker('setEndDate', $scope.addenddate);
                    $('.editexcludeenddatepicker').datepicker('setStartDate',$scope.addstartdate);
                    $('.editexcludeenddatepicker').datepicker('setEndDate', $scope.addenddate);
                }
                
           }
           
           $scope.checkExcludeStartEndDate = function(edit_status,ind){ 
               $scope.curr_date_new = $scope.getDateToday(); //End date should not be greater Start date
               var curr_date = new Date();
               var server_curr_date = new Date(curr_date.getFullYear() + '-' + ('0' + (curr_date.getMonth()+1)).slice( - 2) + '-' + ('0' + curr_date.getDate()).slice( - 2));
               var membership_startdate = new Date($scope.dateformat($scope.addstartdate));
               var membership_enddate = new Date($scope.dateformat($scope.addenddate));
               $scope.current_exclude_edit_status = edit_status;
               
                if((edit_status === 'N' && $scope.excludebillingstartdate && $scope.excludebillingenddate) || (edit_status === 'U' && $scope.edittedexcludestartdate && $scope.edittedexcludeenddate)){
                    var exclude_startdate = '';
                    var exclude_enddate = '';
                    if(edit_status === 'N'){
                        exclude_startdate = new Date($scope.dateformat($scope.excludebillingstartdate));
                        exclude_enddate = new Date($scope.dateformat($scope.excludebillingenddate));
                    }else{
                        exclude_startdate = new Date($scope.dateformat($scope.edittedexcludestartdate));
                        exclude_enddate = new Date($scope.dateformat($scope.edittedexcludeenddate));
                    }
                   
//                   alert('exclude_startdate '+exclude_startdate+' exclude_enddate '+exclude_enddate+' server_curr_date '+server_curr_date);
                    if((+exclude_enddate >= +exclude_startdate) && (+exclude_startdate >= +server_curr_date) && (+exclude_enddate >= +server_curr_date)){
                        $scope.addexcludestartenddateError = false;  
                        $scope.addexcludestartenddateErrortxt = "Start date should be past of End date";
                    }else{
                        $scope.addexcludestartenddateError = true;
                        $scope.addexcludestartenddateErrortxt = "";
                    }
                    
                    if((+membership_startdate > +exclude_startdate) || (+membership_enddate < +exclude_enddate)){
                        $scope.addExcludelimitError = true;
                        $scope.addExcludelimitErrortxt = "Excludes Start and End date should within limit of membership dates";
                        return false;
                    }else{
                        $scope.addExcludelimitError = false; 
                        $scope.addExcludelimitErrortxt = "";
                    }
                    
                    
                    if($scope.exclude_days_array.length > 0){
                        for(var ex_day_count = 0; ex_day_count < $scope.exclude_days_array.length; ex_day_count++ ){
                            var exclude_start_date_exists = new Date($scope.exclude_days_array[ex_day_count].billing_exclude_startdate);
                            var exclude_end_date_exists = new Date($scope.exclude_days_array[ex_day_count].billing_exclude_enddate);
                            if(ex_day_count !== ind){
                                if(((+exclude_start_date_exists <= +exclude_startdate) && (+exclude_startdate <= +exclude_end_date_exists)) || ((+exclude_start_date_exists <= +exclude_enddate) && (+exclude_enddate <= +exclude_end_date_exists)) || ((+exclude_startdate < +exclude_start_date_exists) && (+exclude_enddate > +exclude_end_date_exists))){
                                    $scope.addExcludelimitError = true;
                                    $scope.addExcludelimitErrortxt = "Excludes Start and End date should within limit of membership dates";
                                    return false;  
                                }else{
                                    $scope.addExcludelimitError = false; 
                                    $scope.addExcludelimitErrortxt = "";                           
                                }
                            }else{
                                // updating index of excluding end date and start date
                            }
                        }
                    }else{
                        $scope.addExcludelimitError = false;   
                        $scope.addExcludelimitErrortxt = "";                         
                    }
                }else{
                    $scope.addexcludestartenddateError = true;
                    $scope.addexcludestartenddateErrortxt = "";                    
                }
            };
           
           $scope.catchaddexcludestartdate = function(){ 
                $scope.checkExcludeStartEndDate('N','');
            };
            
            $scope.catchaddexcludeenddate = function(){
                $scope.checkExcludeStartEndDate('N','');
            };
            
            $scope.catcheditexcludestartdate = function(index){                
                $scope.edittedexcludestartdate = this.editexcludestartdate;
                $scope.edittedexcludeenddate = this.editexcludeenddate;
                $scope.checkExcludeStartEndDate('U',index);
            };
            
            $scope.catcheditexcludeenddate = function(index){
                $scope.edittedexcludestartdate = this.editexcludestartdate;
                $scope.edittedexcludeenddate = this.editexcludeenddate;
                $scope.checkExcludeStartEndDate('U',index);
            };
           
           $scope.cancelExcludeDates = function (exdates) { 
               $scope.editexcludestartdate = '';
               $scope.editexcludeenddate = '';
               $scope.selectedExcludeDateindex = '';
               $scope.addexcludestartenddateError = $scope.addExcludelimitError = false;
               $scope.addexcludestartenddateErrortxt = $scope.addExcludelimitErrortxt = $scope.current_exclude_edit_status = ""; 
               $scope.listExcludeDatesView = true;
               $scope.editExcludeDatesView = false;
               $scope.editExcludeDatesStatus= false; 
               this.editexcludestartdate = $scope.formatserverdate(exdates.billing_exclude_startdate); 
               this.editexcludeenddate = $scope.formatserverdate(exdates.billing_exclude_enddate);
           };
           
           $scope.updateExcludeDates = function (exdates, edit_status) { 
               $scope.exdates_edit_status = "";
               $scope.excludeBillingFormSaveButton_status=true;  
               $scope.updateexcludestartdate = $scope.updateexcludeenddate = '';
               if(edit_status === "add"){
                   $scope.exdates_edit_status = "add";
                   $scope.membership_billing_exclude_id = "";
                   $scope.updateexcludestartdate =  $scope.dateformat($scope.excludebillingstartdate);
                   $scope.updateexcludeenddate = $scope.dateformat($scope.excludebillingenddate);                
                   $("#excludeDateUpdateModal").modal('show');
                   $("#excludeDateUpdatetitle").text('Add Exclude Dates'); 
                   $("#excludeDateUpdatecontent").text('Are you sure want to add the exclude dates?');
               }else if(edit_status === "update"){
                   $scope.exdates_edit_status = "update";
                   $scope.membership_billing_exclude_id = exdates.membership_billing_exclude_id;
                   $scope.updateexcludestartdate =  $scope.dateformat($scope.edittedexcludestartdate);
                   $scope.updateexcludeenddate = $scope.dateformat($scope.edittedexcludeenddate); 
                   $("#excludeDateUpdateModal").modal('show');
                   $("#excludeDateUpdatetitle").text('Update Exclude Dates');
                   $("#excludeDateUpdatecontent").text('Are you sure want to update the exclude dates?');
               }else if(edit_status === "delete"){
                   $scope.exdates_edit_status = "delete";
                   $scope.membership_billing_exclude_id = exdates.membership_billing_exclude_id;
                   $("#excludeDateUpdateModal").modal('show');
                   $("#excludeDateUpdatetitle").text('Delete Exclude Dates'); 
                   $("#excludeDateUpdatecontent").text('Are you sure want to delete the exclude dates?');
               }
               
           }
           
           $scope.confirmupdateExcludeDates = function () {   
                $('#progress-full').show();   
                $("#excludeDateUpdateModal").modal('hide');
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membershipExcludeDays',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_option_id": $scope.membership_option_id, 
                        "membership_billing_exclude_id": $scope.membership_billing_exclude_id,
                        "exclude_start_date": $scope.updateexcludestartdate,
                        "exclude_end_date": $scope.updateexcludeenddate,
                        "status": $scope.exdates_edit_status,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();                        
                           $("#AddCategorymessageModal").modal('show');
                           $("#AddCategorymessagetitle").text('Success Message');
                           if($scope.exdates_edit_status === 'add'){
                               $scope.excludebillingstartdate = "";
                               $scope.excludebillingenddate = ""; 
                            $("#AddCategorymessagecontent").text("Exclude days added successfully");
                           }else if($scope.exdates_edit_status === 'update'){
                              $("#AddCategorymessagecontent").text("Exclude days updated successfully");
                           }else if($scope.exdates_edit_status === 'delete'){
                              $("#AddCategorymessagecontent").text("Exclude days deleted successfully");
                           }
                           $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                           
                           $scope.child_membership_options = $scope.membership_details.membership_options;
                           $scope.membership_option_id = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_option_id;
                           
                           for(var i=0; i < $scope.membership_details.membership_options.length; i++){
                               if($scope.membership_details.membership_options[i].membership_option_id === $scope.membership_option_id){
                                   $scope.exclude_days_array = $scope.membership_details.membership_options[i].mem_billing_exclude; 
                                   $localStorage.preview_exclude_days_array = $scope.exclude_days_array;
                               }
                           }
                           if ($scope.exclude_days_array.length > 0) {
                                    $scope.editExcludeDatesStatus = false;
                                    $scope.listExcludeDatesView = true;
                                    $scope.editExcludeDatesView = false;
                            } else {
                                $scope.exclude_days_array = "";
                                $localStorage.preview_exclude_days_array = $scope.exclude_days_array;
                                $scope.editExcludeDatesStatus = false;
                                $scope.listExcludeDatesView = true;
                                $scope.editExcludeDatesView = false;
                            }
                            $scope.selectedExcludeDateindex = "";
                            $scope.openexcludeBilling();
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
          
            $scope.deleteMemOptDetail = function (mem_opt_id) {
                $scope.deletemem_opt_id = mem_opt_id;
                $("#memoptDeleteModal").modal('show');
                $("#memoptDeletetitle").text('Delete');
                $("#memoptDeletecontent").text("Are you sure want to delete membership option? ");
            };

            $scope.memoptiondeleteCancel = function () {
                console.log('cancel delete');
            };

            $scope.memoptiondeleteConfirm = function () {                
                $("#memoptDeleteModal").modal('hide');
                $('#progress-full').show();
                
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMembershipOptions',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_option_id": $scope.deletemem_opt_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        
                           $("#AddCategorymessageModal").modal('show');
                           $("#AddCategorymessagetitle").text('Success Message');
                           $("#AddCategorymessagecontent").text('Membership option deleted successfully');
                           $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;

                           if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            } else {
                                $scope.child_membership_options = '';
                            }
                            $localStorage.preview_membership_childlist = $scope.child_membership_options;   
                            $localStorage.childmembershipindex = '';
                            $localStorage.preview_member_childlisthide = false;
                            $localStorage.preview_membershipchildlist = true;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            
             $scope.copychildmemOption = function (mem_option_id) {
                $('#progress-full').show();

                $http({
                    method: 'POST',
                    url: urlservice.url + 'copyMembershipOptions',
                    data: {
                        "membership_id": $scope.membership_id,
                        "membership_option_id": mem_option_id,
                        'copy_mem_type': 'Child',
                        'mem_template_flag': 'N',
                        'company_id': $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $scope.isAddedMemOptions = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();
                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Duplicate membership option copied successfully');

                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                        } else {
                            $scope.child_membership_options = "";
                        }
                        $localStorage.childmembershipindex = '';
                        $localStorage.preview_member_childlisthide = false;
                        $localStorage.preview_membershipchildlist = true;
                        $localStorage.preview_membership_childlist = $scope.child_membership_options;                        

                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
             //Membership option drag and drop on listing
            $scope.dragmembershipOptionCallback = function (drag_type, ind) {
                if (drag_type === 'start') {
                    $scope.old_location_id = ind;
                    $scope.start_membership_option_id = $scope.child_membership_options[ind].membership_option_id;
                } else if (drag_type === 'end') {
                    setTimeout(function () {
                        for(var i=0;i<$scope.child_membership_options.length;i++){ 
                            if($scope.child_membership_options[i].membership_option_id === $scope.start_membership_option_id){
                                $scope.new_location_id = i;
                            }
                        }
                        
                        if ($scope.old_location_id !== $scope.new_location_id) {
                            var sort_membership_option_id, membership_option_sort_order, previous_sort_order, next_sort_order;
                            if ($scope.new_location_id === 0) {
                                sort_membership_option_id = $scope.child_membership_options[$scope.new_location_id].membership_option_id;
                                previous_sort_order = parseFloat($scope.child_membership_options[$scope.new_location_id + 1].option_sort_order);
                                membership_option_sort_order = previous_sort_order + 1;
                                $scope.sortordermembershipoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            } else if ($scope.new_location_id === $scope.child_membership_options.length - 1) {
                                sort_membership_option_id = $scope.child_membership_options[$scope.new_location_id].membership_option_id;
                                membership_option_sort_order = parseFloat($scope.child_membership_options[$scope.new_location_id - 1].option_sort_order) / 2;
                                $scope.sortordermembershipoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            } else {
                                sort_membership_option_id = $scope.child_membership_options[$scope.new_location_id].membership_option_id;
                                previous_sort_order = parseFloat($scope.child_membership_options[$scope.new_location_id + 1].option_sort_order);
                                next_sort_order = parseFloat($scope.child_membership_options[$scope.new_location_id - 1].option_sort_order);
                                membership_option_sort_order = (previous_sort_order + next_sort_order) / 2;                               
                                $scope.sortordermembershipoptionupdate(sort_membership_option_id, membership_option_sort_order);
                            }
                        }

                    }, 1000);
                }
            };

             //updating membership option sort order after drag and drop on listing
            $scope.sortordermembershipoptionupdate = function(membership_option_id,sort_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url+'updateMembershipOptionsSortOrder',
                    data: {
                        "sort_id": sort_id,
                        "membership_option_id": membership_option_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id
                    },
                    headers:{
                        "Content-Type":'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide();
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        if($scope.membership_details.membership_options.length > 0){
                            $scope.child_membership_options = $scope.membership_details.membership_options;
                        } else {
                            $scope.child_membership_options = "";
                        }
                        $localStorage.childmembershipindex = '';
                        $localStorage.preview_member_childlisthide = false;
                        $localStorage.preview_membershipchildlist = true;
                        $localStorage.preview_membership_childlist = $scope.child_membership_options;
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout(); 
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.mseditdiscount = function (discount_details, ind) {
                $scope.msdiscountView = false;
                $scope.msdiscountEditView = false;
                $scope.singlePaymentUpdateButton = true;

                $scope.selecteddiscountindex = ind;
                $scope.edit_discount_id = discount_details.membership_discount_id;
                $scope.editdiscountcode = discount_details.discount_code;
                $scope.edit_discount_off = discount_details.discount_off; 
                $scope.edit_discnt_type = discount_details.discount_type;
                $scope.editdiscountamount = discount_details.discount_amount;
            }
            
            $scope.mseditdiscountCancel = function (discount_details) {
                this.editdiscountcode = discount_details.discount_code;
                this.edit_discount_off = discount_details.discount_off;                
                this.edit_discnt_type = discount_details.discount_type;
                this.editdiscountamount = discount_details.discount_amount;
                $scope.msdiscountView = true;
                $scope.msdiscountEditView = false;
                $scope.singlePaymentUpdateButton = false;
                $scope.selecteddiscountindex = "";
                $scope.discount_off = 'M';
            };            
            
            $scope.msadddiscount = function () {
                $('#progress-full').show();
                
                if(parseFloat($scope.discountamount) <= 0){
                    $scope.discountamount = '';
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text("Discount amount should be greater than "+$scope.wp_currency_symbol+"0");
                    $('#progress-full').hide();
                    return false;
                }

                    if ($scope.discnt_type === 'V') {
                        var dollar_value;
                    if ($scope.discount_off === 'M') {
                        if ($scope.memfee !== '' && $scope.memfee !== undefined && parseFloat($scope.memfee) > 0 && $scope.memfee !== null) {
                            dollar_value = parseFloat($scope.memfee) - parseFloat($scope.discountamount);
                            if (parseFloat('5') <= parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (parseFloat(dollar_value) - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.discount_type = "V";
                                                $scope.discount_code = $scope.discountcode;
                                                $scope.discount_value = $scope.discountamount;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.discount_type = "V";
                                            $scope.discount_code = $scope.discountcode;
                                            $scope.discount_value = $scope.discountamount;
                                        }                                
                                
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Membership fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                return false;
                            }
                        } else {
                            $scope.discount_type = "V";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                        }
                    } else if ($scope.discount_off === 'S') {
                        if ($scope.memsignupfee !== '' && $scope.memsignupfee !== undefined && parseFloat($scope.memsignupfee) > 0 && $scope.memsignupfee !== null) {
                            dollar_value = parseFloat($scope.memsignupfee) - parseFloat($scope.discountamount);
                            if (parseFloat('5') <= parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                $scope.discount_type = "V";
                                $scope.discount_code = $scope.discountcode;
                                $scope.discount_value = $scope.discountamount;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Sign up fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                return false;
                            }
                        } else {
                            $scope.discount_type = "V";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                        }
                    }


                } else if ($scope.discnt_type === 'P') {
                    if ($scope.discount_off === 'M') {
                        if ($scope.memfee !== '' && $scope.memfee !== undefined && parseFloat($scope.memfee) > 0 && $scope.memfee !== null) {
                            var percent_value = parseFloat($scope.memfee) - ((parseFloat($scope.memfee) * parseFloat($scope.discountamount)) / parseFloat(100));
                            if (parseFloat('5') <= parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (parseFloat(percent_value) - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.discount_type = "P";
                                                $scope.discount_code = $scope.discountcode;
                                                $scope.discount_value = $scope.discountamount;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.discount_type = "P";
                                            $scope.discount_code = $scope.discountcode;
                                            $scope.discount_value = $scope.discountamount;
                                        }
                                
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Membership fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                return false;

                            }
                        } else {
                            $scope.discount_type = "P";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                        }
                    } else if ($scope.discount_off === 'S') {
                         if ($scope.memsignupfee !== '' && $scope.memsignupfee !== undefined && parseFloat($scope.memsignupfee) > 0 && $scope.memsignupfee !== null) {
                            var percent_value = parseFloat($scope.memsignupfee) - ((parseFloat($scope.memsignupfee) * parseFloat($scope.discountamount)) / parseFloat(100));
                            if (parseFloat('5') <= parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                $scope.discount_type = "P";
                                $scope.discount_code = $scope.discountcode;
                                $scope.discount_value = $scope.discountamount;
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Sign up fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                return false;

                            }
                        } else {
                            $scope.discount_type = "P";
                            $scope.discount_code = $scope.discountcode;
                            $scope.discount_value = $scope.discountamount;
                        }
                    }
                }

                    $http({
                    method: 'POST',
                    url: urlservice.url + 'addMembershipDiscount',
                    data: {
                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "discount_type": $scope.discount_type,
                        "discount_off": $scope.discount_off,
                        "discount_code": $scope.discount_code,
                        "discount_amount": $scope.discount_value,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                           $('#progress-full').hide();
                           $("#AddCategorymessageModal").modal('show');
                           $("#AddCategorymessagetitle").text('Success Message');
                           $("#AddCategorymessagecontent").text('Discount value added successfully');
                           $scope.discountcode = $scope.discountamount = '';
                           $scope.discnt_type = 'V'; 
                           $scope.discount_off = 'M';
                           $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                            
                            $scope.msadded = $scope.msoptionadded = true;
                            $scope.membership_id = $scope.membership_details.membership_id;                                
                            $scope.mscategory = $scope.membership_details.category_title;
                            $scope.mssubcategory = $scope.membership_details.category_subtitle;
                            $scope.category_status = $scope.membership_details.category_status;
                            $scope.msurl = $scope.membership_details.category_video_url;
                            $scope.category_picture = $scope.membership_details.category_image_url;
                            $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                            $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                            $scope.msdesc = $scope.membership_details.category_description;
                            $scope.rank_columns = $scope.membership_details.rank_details;
                            if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                                $scope.membership_option_id = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_option_id;
                                $scope.show_in_app_flg = $scope.child_membership_options[$scope.selectedMemOptIndex].show_in_app_flg;
                                if($scope.show_in_app_flg === 'Y'){
                                    $scope.show_in_app = true;
                                }else{
                                    $scope.show_in_app = false;
                                }
                                $scope.msoptActiveMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_active;
                                $scope.msoptOnHoldMembers = $scope.child_membership_options[$scope.selectedMemOptIndex].members_onhold;
                                $scope.msoptCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_title;
                                $scope.msoptSubCategory = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_subtitle;
                                $scope.optioncategorydesc = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_description;
                                $scope.memstructure = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_structure;                                    
                                $scope.memProcessingfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_processing_fee_type;
                                $scope.memsignupfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_signup_fee;                                    
                                $scope.memfee = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_fee;
                                $scope.mem_fee_include = $scope.child_membership_options[$scope.selectedMemOptIndex].initial_payment_include_membership_fee;
                                
                                if($scope.memstructure === 'OE'){                                        
                                    $scope.mempayfrequency = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_recurring_frequency;
                                    $scope.memfreqcustomperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_type;
                                    $scope.memfreqcustomvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].custom_recurring_frequency_period_val;
                                    $scope.prorateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].prorate_first_payment_flg;
                                    $scope.recpayStartdate = $scope.child_membership_options[$scope.selectedMemOptIndex].delay_recurring_payment_start_flg;
                                    $scope.memrecstartperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_type;
                                    $scope.memrecstartvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].delayed_recurring_payment_val;
                                    
                                    $scope.billingoptionvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_options;
                                    $scope.noofclass = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_classes;
                                    $scope.memexpiryperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_type;
                                    $scope.memexpiryvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_val;
                                    $scope.billing_depositamount = $scope.child_membership_options[$scope.selectedMemOptIndex].deposit_amount;
                                    $scope.billing_noofpayment = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_payments;
                                    $scope.billing_paymentstartdateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_payment_start_date_type;
                                    
                                    $localStorage.PreviewMemRecurringFrequency = $scope.mempayfrequency;
                                }else if($scope.memstructure === 'NC' || $scope.memstructure === 'C'){
                                    $scope.billing_paymentfreqValue = $scope.child_membership_options[$scope.selectedMemOptIndex].membership_recurring_frequency;
                                    $scope.billingoptionvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_options;
                                    $scope.noofclass = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_classes;
                                    $scope.exp_date_status = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_status;
                                    $scope.memexpiryperiod = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_type;
                                    $scope.memexpiryvalue = $scope.child_membership_options[$scope.selectedMemOptIndex].expiration_date_val;
                                    $scope.billing_depositamount = $scope.child_membership_options[$scope.selectedMemOptIndex].deposit_amount;
                                    $scope.billing_noofpayment = $scope.child_membership_options[$scope.selectedMemOptIndex].no_of_payments;
                                    $scope.billing_paymentstartdateValue = $scope.child_membership_options[$scope.selectedMemOptIndex].billing_payment_start_date_type;
                                    if($scope.billing_paymentstartdateValue === '4'){
                                        $scope.memcustompaymntstrtdte = $scope.formatserverdate(mem_detail.billing_options_payment_start_date);
                                    }else{
                                        var curr_date = new Date();
                                        $scope.memcustompaymntstrtdte = ('0' + (curr_date.getMonth() + 1)).slice( - 2) + '/' + ('0' + curr_date.getDate()).slice( - 2) + '/' +curr_date.getFullYear();
                                    }
                                }

                                if ($scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc.length > 0) {
                                        $scope.msdiscountList = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc;
                                        $scope.msdiscountListView = true;
                                        $scope.msdiscountView = true;
                                        $scope.msdiscountEditView = false;
                                } else {
                                    $scope.msdiscountList = "";
                                    $scope.msdiscountListView = false;
                                    $scope.msdiscountView = false;
                                    $scope.msdiscountEditView = false;
                                }
                                $scope.selecteddiscountindex = "";
                                
                            }else{
                                $scope.child_membership_options = [];
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            $scope.msupdatediscount = function (discnt_id) {
                $('#progress-full').show();
                
                if(parseFloat(this.editdiscountamount) <= 0){
                    $("#failuremessageModal").modal('show');
                    $("#failuremessagetitle").text('Message');
                    $("#failuremessagecontent").text("Discount amount should be greater than "+$scope.wp_currency_symbol+"0");
                    $('#progress-full').hide();
                    return false;
                }
                
                if (this.edit_discnt_type === 'V') {
                    if (this.edit_discount_off === 'M') {
                        if (parseFloat($scope.memfee) > 0) {
                            var dollar_value = parseFloat($scope.memfee) - parseFloat(this.editdiscountamount);
                            if (parseFloat('5') <= parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                
                                 if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (parseFloat(dollar_value) - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.discount_type = "V";
                                                $scope.discount_code = this.editdiscountcode;
                                                $scope.discount_value = this.editdiscountamount;
                                                $scope.discount_off_val = this.edit_discount_off;
                                                $scope.discount_id = discnt_id;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount value is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.discount_type = "V";
                                            $scope.discount_code = this.editdiscountcode;
                                            $scope.discount_value = this.editdiscountamount;
                                            $scope.discount_off_val = this.edit_discount_off;
                                            $scope.discount_id = discnt_id;
                                        } 
                                
                            } else {
                                $('#progress-full').hide();
                                $("#failuremessageModal").modal('show');
                                $("#failuremessagetitle").text('Message');
                                $("#failuremessagecontent").text("Membership fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                return false;
                            }
                        }else{
                                $scope.discount_type = "V";
                                $scope.discount_code = this.editdiscountcode;
                                $scope.discount_value = this.editdiscountamount;
                                $scope.discount_off_val = this.edit_discount_off;
                                $scope.discount_id = discnt_id;                            
                        }
                        } else if (this.edit_discount_off === 'S') {
                            if (parseFloat($scope.memsignupfee) > 0) {
                                var dollar_value = parseFloat($scope.memsignupfee) - parseFloat(this.editdiscountamount);
                                if (parseFloat('5') <= parseFloat(dollar_value) || parseFloat('0') == parseFloat(dollar_value)) {
                                    $scope.discount_type = "V";
                                    $scope.discount_code = this.editdiscountcode;
                                    $scope.discount_value = this.editdiscountamount;
                                    $scope.discount_off_val = this.edit_discount_off;
                                    $scope.discount_id = discnt_id;
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Sign up fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount value is applied");
                                    return false;
                                }
                            }else{
                                    $scope.discount_type = "V";
                                    $scope.discount_code = this.editdiscountcode;
                                    $scope.discount_value = this.editdiscountamount;
                                    $scope.discount_off_val = this.edit_discount_off;
                                    $scope.discount_id = discnt_id;
                            }
                        }

                    } else if (this.edit_discnt_type === 'P') {
                        if (this.edit_discount_off === 'M') {
                            if (parseFloat($scope.memfee) > 0) {
                                var percent_value = parseFloat($scope.memfee) - ((parseFloat($scope.memfee) * parseFloat(this.editdiscountamount)) / parseFloat(100));
                                if (parseFloat('5') <= parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                     if(($scope.memstructure === "NC" || $scope.memstructure === "C") && $scope.billingoptionvalue === "PP"){
                                            var recurring_amnt = (parseFloat(percent_value) - +$scope.billing_depositamount)/(+$scope.billing_noofpayment);
                                            if(parseFloat(recurring_amnt) >= 5){
                                                $scope.discount_type = "P";
                                                $scope.discount_code = this.editdiscountcode;
                                                $scope.discount_value = this.editdiscountamount;
                                                $scope.discount_off_val = this.edit_discount_off;
                                                $scope.discount_id = discnt_id;
                                            }else{
                                                $('#progress-full').hide();
                                                $("#failuremessageModal").modal('show');
                                                $("#failuremessagetitle").text('Message');
                                                $("#failuremessagecontent").text("Recurring fee must be at-least "+$scope.wp_currency_symbol+"5 when discount percentage is applied");
                                                return false;
                                            }                                            
                                        }else{
                                            $scope.discount_type = "P";
                                            $scope.discount_code = this.editdiscountcode;
                                            $scope.discount_value = this.editdiscountamount;
                                            $scope.discount_off_val = this.edit_discount_off;
                                            $scope.discount_id = discnt_id;
                                        }
                                    
                                } else {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text("Membership fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                    return false;
                                }
                            }else{
                                    $scope.discount_type = "P";
                                    $scope.discount_code = this.editdiscountcode;
                                    $scope.discount_value = this.editdiscountamount;
                                    $scope.discount_off_val = this.edit_discount_off;
                                    $scope.discount_id = discnt_id;
                            }
                            } else if (this.edit_discount_off === 'S') {
                                if (parseFloat($scope.memsignupfee) > 0) {
                                    var percent_value = parseFloat($scope.memsignupfee) - ((parseFloat($scope.memsignupfee) * parseFloat(this.editdiscountamount)) / parseFloat(100));
                                    if (parseFloat('5') <= parseFloat(percent_value) || parseFloat('0') == parseFloat(percent_value)) {
                                        $scope.discount_type = "P";
                                        $scope.discount_code = this.editdiscountcode;
                                        $scope.discount_value = this.editdiscountamount;
                                        $scope.discount_off_val = this.edit_discount_off;
                                        $scope.discount_id = discnt_id;
                                    } else {
                                        $('#progress-full').hide();
                                        $("#failuremessageModal").modal('show');
                                        $("#failuremessagetitle").text('Message');
                                        $("#failuremessagecontent").text("Sign up fee cannot be less than 5"+$scope.wp_currency_symbol+" when discount percentage is applied");
                                        return false;
                                    }
                                }else{
                                    $scope.discount_type = "P";
                                    $scope.discount_code = this.editdiscountcode;
                                    $scope.discount_value = this.editdiscountamount;
                                    $scope.discount_off_val = this.edit_discount_off;
                                    $scope.discount_id = discnt_id;
                                }
                            }
                        }
                   
                        
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipDiscount',
                    data: {
                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "membership_discount_id": $scope.discount_id,
                        "discount_type": $scope.discount_type,
                        "discount_off": $scope.discount_off_val,
                        "discount_code": $scope.discount_code,
                        "discount_amount": $scope.discount_value,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                           $('#progress-full').hide();
                           $("#AddCategorymessageModal").modal('show');
                           $("#AddCategorymessagetitle").text('Success Message');
                           $("#AddCategorymessagecontent").text('Discount value updated successfully');
                           $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;

                           if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            } else {
                                $scope.child_membership_options = '';
                            }  
                            
                            if ($scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc.length > 0) {
                                    $scope.msdiscountList = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc;
                                    $scope.msdiscountListView = true;
                                    $scope.msdiscountView = true;
                                    $scope.msdiscountEditView = false;
                            } else {
                                $scope.msdiscountList = "";
                                $scope.msdiscountListView = false;
                                $scope.msdiscountView = false;
                                $scope.msdiscountEditView = false;
                            }
                            $scope.selecteddiscountindex = "";
                            $scope.ms_registration_columns = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].reg_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies;
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            $scope.msdeletediscount = function (discnt_id) {
                $scope.deletediscount_id = discnt_id;
                $("#msdiscountDeleteModal").modal('show');
                $("#msdiscountDeletetitle").text('Delete');
                $("#msdiscountDeletecontent").text("Are you sure want to delete discount detail? ");
            };


            $scope.msdiscountdeleteCancel = function () {
                console.log('cancel delete');
            };


            $scope.msdiscountdeleteConfirm = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMembershipDiscount',
                    data: {
                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "membership_discount_id": $scope.deletediscount_id,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                         $('#progress-full').hide();
                           $("#AddCategorymessageModal").modal('show');
                           $("#AddCategorymessagetitle").text('Success Message');
                           $("#AddCategorymessagecontent").text('Discount value deleted successfully');
                           $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;

                           if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            } else {
                                $scope.child_membership_options = '';
                            }  
                            
                            if ($scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc.length > 0) {
                                    $scope.msdiscountList = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].membership_disc;
                                    $scope.msdiscountListView = true;
                                    $scope.msdiscountView = true;
                                    $scope.msdiscountEditView = false;
                            } else {
                                $scope.msdiscountList = "";
                                $scope.msdiscountListView = false;
                                $scope.msdiscountView = false;
                                $scope.msdiscountEditView = false;
                            }
                            $scope.selecteddiscountindex = "";
                            $scope.ms_registration_columns = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].reg_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.membership_options[$scope.selectedMemOptIndex].waiver_policies;
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else if (response.data.status == 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    } else {
                        $('#progress-full').hide();
                        $("#failuremessageModal").modal('show');
                        $("#failuremessagetitle").text('Message');
                        $("#failuremessagecontent").text(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            $scope.showregtextview = function () {
                $scope.showregtext = true;
            };
            
             $scope.savemscolregistration = function (reg_type,membership_register_field) {
                
                if (reg_type === 'add') {
                   $scope.registerfieldcount = $scope.ms_registration_columns.length + 1;                   
                   $scope.registerfieldname =  "membership_registration_column_" +$scope.registerfieldcount;
                   $scope.registerfieldmandatoryname =  "membership_reg_col_"+$scope.registerfieldcount+"_mandatory_flag";
                   $scope.registerfieldvalue = $scope.ms_reg_col_newvalue;
                   if ($scope.ms_checked) {
                        $scope.membership_mandatory_flag = 'Y';
                    } else {
                        $scope.membership_mandatory_flag = 'N';
                    }
                } else if (reg_type === 'update') {
                       if (this.ms_register_column_name !== "" ||this.ms_register_column_name !== undefined ) {
                        $scope.registerfieldcount = membership_register_field + 1;
                        $scope.registerfieldname = "membership_registration_column_" + $scope.registerfieldcount;
                        $scope.registerfieldmandatoryname =  "membership_reg_col_"+$scope.registerfieldcount+"_mandatory_flag";
                        $scope.registerfieldvalue = this.ms_register_column_name;
                        if (this.ms_mand_checked) {
                            $scope.membership_mandatory_flag = 'Y';
                        } else {
                            $scope.membership_mandatory_flag = 'N';
                        }
                    } else {
                        $scope.registerfieldname = "";
                        $scope.membership_mandatory_flag= 'N';
                        $scope.registerfieldvalue = "";
                    }
                }

                

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipRegistration',
                    data: {
                        "membership_column_name": $scope.registerfieldname,
                        "membership_mandatory_column_name":$scope.registerfieldmandatoryname,
                        "membership_mandatory_column_value":$scope.membership_mandatory_flag,
                        "membership_column_value": $scope.registerfieldvalue,
//                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.cancelmsnewsave();
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $('#progress-full').hide();
                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Registration field successfully updated');
                        
                            $scope.msadded = true;
                            $scope.membership_id = $scope.membership_details.membership_id;                                
                            $scope.mscategory = $scope.membership_details.category_title;
                            $scope.mssubcategory = $scope.membership_details.category_subtitle;
                            $scope.category_status = $scope.membership_details.category_status;
                            $scope.msurl = $scope.membership_details.category_video_url;
                            $scope.category_picture = $scope.membership_details.category_image_url;
                            $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                            $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                            $scope.msdesc = $scope.membership_details.category_description;
                            $scope.rank_columns = $scope.membership_details.rank_details;
                            $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                            $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.waiver_policies;
                            }
                            if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            }else{
                                $scope.child_membership_options = [];
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            //edit function call for existing registration field
            $scope.editmscol = function (mem_registration_column,mem_mandatory_check,ind) {
                
                this.ms_register_column_name = mem_registration_column;
                this.ms_register_mandatory_check = mem_mandatory_check;
                if(this.ms_register_mandatory_check === 'Y'){
                     this.ms_mand_checked = true;
                }else if(this.ms_register_mandatory_check === 'N'){
                    this.ms_mand_checked = false;
                }
                
                $scope.ms_edit_col_view = false;
                $scope.ms_col_view = false;
                $scope.ms_selectedregcolindex = ind;
                
                $localStorage.preview_mem_reg_fieldindex = ind;
                $localStorage.preview_mem_reg_col_name_edit = mem_registration_column; 
                if(mem_mandatory_check === 'Y'){
                     $localStorage.PreviewOldMemRegFieldMandatory = true;
                }else if(mem_mandatory_check === 'N'){
                    $localStorage.PreviewOldMemRegFieldMandatory = false;
                }
                $localStorage.PreviewOldMemRegCancel = false;               
            };
          
            //Cancel existing registration field updating
            $scope.cancelmssave = function (mem_registration_column,mem_mandatory_check) {
                $localStorage.PreviewOldMemRegCancel = true;
                $localStorage.preview_mem_reg_fieldindex = '';
                $localStorage.preview_mem_reg_col_name_edit = mem_registration_column;
                this.ms_register_column_name = mem_registration_column; 
                if(mem_mandatory_check === 'Y'){
                     this.ms_mand_checked = true;
                }else if(mem_mandatory_check === 'N'){
                    this.ms_mand_checked = false;
                }
                $scope.ms_edit_col_view = false;
                $scope.ms_col_view = true;
                $scope.ms_selectedregcolindex = "";
                if(mem_mandatory_check === 'Y'){
                     $localStorage.PreviewOldMemRegFieldMandatory = true;
                }else if(mem_mandatory_check === 'N'){
                    $localStorage.PreviewOldMemRegFieldMandatory = false;
                }
                $localStorage.PreviewNewMemRegCancel = true;
                $localStorage.PreviewNewMemRegFieldMandatory = false;
                $localStorage.PreviewNewMemRegistrationField = '';
            }
            
            
            $scope.cancelmsnewsave = function () {
                $localStorage.PreviewNewMemRegCancel = true;
                $localStorage.PreviewNewMemRegFieldMandatory = false;
                $localStorage.PreviewNewMemRegistrationField = '';
                $scope.ms_reg_col_newvalue = "";
                $scope.ms_checked = false;
                $scope.showregtext = false;
            }
            
            //confirm existing registration field deletion
            $scope.msdeletecol = function (registration_index) {
                $scope.membership_registration_colunm_count = registration_index+1;
                $scope.membership_registration_colunm_del = "membership_registration_column_"+$scope.membership_registration_colunm_count;
                $("#msregisterDeleteModal").modal('show');
                $("#msregisterDeletetitle").text('Delete');
                $("#msregisterDeletecontent").text("Are you sure want to delete register? ");
            };

         //Cancel existing registration field deletion
            $scope.msregisterdeleteCancel = function () {
                console.log('cancel delete');
            };
            
            //delete existing registration field
            $scope.msregisterdeleteConfirm = function () {

                $http({
                    method: 'POST',
                    url: urlservice.url + 'deleteMembershipRegistration',
                    data: {
                        "membership_column_name": $scope.membership_registration_colunm_del,
//                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $('#progress-full').hide();
                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Registration field deleted successfully');
                            $scope.msadded = true;
                            $scope.membership_id = $scope.membership_details.membership_id;                                
                            $scope.mscategory = $scope.membership_details.category_title;
                            $scope.mssubcategory = $scope.membership_details.category_subtitle;
                            $scope.category_status = $scope.membership_details.category_status;
                            $scope.msurl = $scope.membership_details.category_video_url;
                            $scope.category_picture = $scope.membership_details.category_image_url;
                            $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                            $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                            $scope.msdesc = $scope.membership_details.category_description;
                            $scope.rank_columns = $scope.membership_details.rank_details;
                            $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                            $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.waiver_policies;
                            }
                            if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            }else{
                                $scope.child_membership_options = [];
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
             //Registration field drag and drop sorting
            $scope.msdragregfieldCallback = function(drag_type,ind,reg_col_index) {
               if(drag_type === 'start'){
                   $scope.old_location_id = ind + 1;
                   $scope.old_location_col_index = reg_col_index;
               } else if(drag_type === 'end'){
                    setTimeout(function(){                   
                    for (var i=0;i<$scope.ms_registration_columns.length;i++){ 
                       if($scope.ms_registration_columns[i].reg_col_index === $scope.old_location_col_index){
                           $scope.new_location_id = +i + 1;
                    }
                    }
                    if($scope.old_location_id !== $scope.new_location_id){
                        $scope.msupdate_regfieldSorting();
                    }
                }, 1000);          
            }
           }

//Registration field name update
            $scope.msupdate_regfieldSorting = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'membershipOptionsRegistrationSorting',
                    data: {
                        "old_location_id":$scope.old_location_id,
                        "new_location_id":$scope.new_location_id,
//                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $('#progress-full').hide();
                        $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                        $localStorage.PreviewMembershipRegcols = $scope.ms_registration_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            
    //update existing waiver text
            $scope.updatemswaiver = function () {
                $('#progress-full').show();
                $scope.ms_waiver_policies_encode = document.getElementById("ms_waivertextarea").innerHTML;
                $scope.ms_waiver_policies = $scope.ms_waiver_policies_encode;

                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateMembershipWaiver',
                    data: {
                        "waiver_policies": $scope.ms_waiver_policies,
//                        'membership_option_id': $scope.membership_option_id,
                        "membership_id": $scope.membership_id,
                        "company_id": $localStorage.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $('#progress-full').hide();
                        $("#AddCategorymessageModal").modal('show');
                        $("#AddCategorymessagetitle").text('Success Message');
                        $("#AddCategorymessagecontent").text('Waiver text updated successfully');
                            $scope.msadded = true;
                            $scope.membership_id = $scope.membership_details.membership_id;                                
                            $scope.mscategory = $scope.membership_details.category_title;
                            $scope.mssubcategory = $scope.membership_details.category_subtitle;
                            $scope.category_status = $scope.membership_details.category_status;
                            $scope.msurl = $scope.membership_details.category_video_url;
                            $scope.category_picture = $scope.membership_details.category_image_url;
                            $scope.croppedCategoryImage = $scope.membership_details.category_image_url;
                            $scope.cropper.sourceImage = $scope.croppedCategoryImage;
                            $scope.msdesc = $scope.membership_details.category_description;
                            $scope.rank_columns = $scope.membership_details.rank_details;
                            $scope.ms_registration_columns = $scope.membership_details.reg_columns;
                            if($scope.ms_registration_columns.length >= 10) {
                                $scope.ms_regfieldexceeds = true;
                            } else {
                                $scope.ms_regfieldexceeds = false;
                            }
                            $scope.ms_selectedregcolindex = "";
                            if($scope.membership_details.waiver_policies === null) {
                                $scope.ms_waiverpolicy = '';
                            } else {
                                $scope.ms_waiverpolicy = $scope.membership_details.waiver_policies;
                            }
                            $localStorage.PreviewMembershipWaiver = $scope.ms_waiverpolicy;
                            $scope.exist_ms_waiverpolicy = $scope.ms_waiverpolicy;
                            if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            }else{
                                $scope.child_membership_options = [];
                            }
                    
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });

            };
            
            $scope.showmembershipChecked = function (mem_option_id,show_checked) {
                $('#progress-full').show();
                var show_option = show_checked?'N':'Y';
                $http({
                    method: 'POST',
                    url: urlservice.url + 'showMembershipOptionInApp',
                    data: {
                        "membership_id": $scope.membership_id,
                        "show_in_app_flg": show_option,
                        'membership_option_id': mem_option_id,
                        "company_id": $scope.company_id

                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $localStorage.isAddedCategoryDetails = 'Y';
                        $scope.isAddedMemOptions = 'Y';
                        $localStorage.currentpage = "editmembership";
                        $localStorage.preview_membershipcontent = $localStorage.membership_details = $scope.membership_details = response.data.msg;
                        $scope.category_image_update = 'N';
                        $('#progress-full').hide();

                        if ($localStorage.firstlogin && $localStorage.firstlogin_event === 'Y') {
                            $localStorage.firstlogin_event = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();

                        } else if ($localStorage.firstlogin_event === 'N' || $localStorage.firstlogin_event === undefined) {

                            $("#AddCategorymessageModal").modal('show');
                            $("#AddCategorymessagetitle").text('Success Message');
                            $("#AddCategorymessagecontent").text('Membership option view in app updated');
                               
                            if($scope.membership_details.membership_options.length > 0){
                                $scope.child_membership_options = $scope.membership_details.membership_options;
                            }else{
                                $scope.child_membership_options = "";
                            }
                                
                            $localStorage.childmembershipindex = '';
                            $localStorage.preview_member_childlisthide = false;
                            $localStorage.preview_membershipchildlist = true;
                            $localStorage.preview_membership_childlist = $scope.child_membership_options;
                        }

                    } else if (response.data.status === 'Expired') {
                        $localStorage.isAddedCategoryDetails = 'N';
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    $localStorage.isAddedCategoryDetails = 'N';
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.removeDrag = function (event) {
                $(event.currentTarget).prop("draggable", false);
                $(event.currentTarget.closest('li')).prop("draggable", false);
            };

            $scope.addDrag = function (event) {
                $(event.currentTarget).prop("draggable", true);
                $(event.currentTarget.closest('li')).prop("draggable", true);
            };
          
    }else {
            $location.path('/login');
        }
    
    };
    module.exports = AddMembershipController;


