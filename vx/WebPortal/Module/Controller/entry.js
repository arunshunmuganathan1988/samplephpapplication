
'use strict';

// var angular = require('angular');
 
var app = angular.module('login').controller('LoginController', require('./LoginController'));
var app = angular.module('addevent', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard']).controller('AddEventController', require('./AddEventController')).
directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
var app = angular.module('dashboard').controller('dashBoardController', require('./dashBoardController'));


var app = angular.module('addmembership', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard']).controller('AddMembershipController',require('./AddMembershipController'))
.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

var app = angular.module('addtrial', ['wysiwyg.module', 'angular-img-cropper', 'ngclipboard']).controller('AddTrialController',require('./AddTrialController')).
directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

var app = angular.module('adminclonepanel').controller('AdminClonePanelController',require('./AdminClonePanelController'));

var app = angular.module('adminhome',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('AdminHomeController',require('./AdminHomeController'));

var app = angular.module('adminlogintool').controller('AdminLoginToolController',require('./AdminLoginToolController'));

var app = angular.module('adminpanel').controller('AdminpageController',require('./AdminpageController'));

var app = angular.module('adminstudiomembers',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('AdminStudioMembersController',require('./AdminStudioMembersController'));

var app = angular.module('appverification',['ngclipboard']).controller('AppVerificationController',require('./AppVerificationController')).
service('attendanceFilterDetails', function () {   
    var attendanceFilterValue = [];
    var addattendanceFilter = function (newObj) {
        attendanceFilterValue = [];
        attendanceFilterValue.push(newObj);
    };

    var getattendanceFilter = function () {
        return attendanceFilterValue;
    };

    return {
        addattendanceFilter: addattendanceFilter,
        getattendanceFilter: getattendanceFilter
    };

});

var app = angular.module('attendancedashboard',['ngclipboard']).controller('AttendanceDashboardController',require('./AttendanceDashboardController')).
service('attendanceFilterDetails', function () {   
    var attendanceFilterValue = [];
    var addattendanceFilter = function (newObj) {
        attendanceFilterValue = [];
        attendanceFilterValue.push(newObj);
    };

    var getattendanceFilter = function () {
        return attendanceFilterValue;
    };

    return {
        addattendanceFilter: addattendanceFilter,
        getattendanceFilter: getattendanceFilter
    };

});

var app = angular.module('billingstripe').controller('BillingStripeController',require('./BillingStripeController')).
directive('phnumbersonly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}).filter('dateSuffix',['$filter', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input) {
        if (input !== '0000-00-00' && input) {
            var dtfilter = $filter('date')(input, 'MMMM d,yyyy');
            var month = dtfilter.split(" ")[0];
            var day = dtfilter.split(" ")[1].split(",")[0];
            var year = dtfilter.split(" ")[1].split(",")[1];
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];

            var daysuffix = month + " " + day + suffix + ", " + year;
            return daysuffix;
        } else {
            return "";
        }
    };
}]);


var app = angular.module('curriculum').controller('CurriculumContentController',require('./CurriculumContentController'));

var app = angular.module('customers',  ['datatables', 'datatables.scroller','datatables.select']).controller('CustomerController',require('./CustomerController')).
directive("repeatEnd", function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            if (scope.$last) {
                scope.$eval(attrs.repeatEnd);
            }
        }
    };
}).directive("attachfileinput", ['$sce', function ($sce) {
        return {
            require: 'ngModel',
            $scope: {
                attachfileinput: "=",
                filepreview: "="
            },
            link: function ($scope, element, attributes, ngModel) {
                element.bind("change", function (changeEvent) {
                    $scope.maxClassScheduleSizeError = false;
                    var schedulefileSize = this.files[0].size / 1024 / 1024;
                    if (schedulefileSize >= 10) {
                        $scope.maxEmailAttachmentSizeError = true;
                        changeEvent.target.files[0] = "";
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                        $scope.filepreview = "";
                        ngModel.$setViewValue(element.val());
                        ngModel.$render();
                        });
                     }
                    } else {
                        $scope.maxEmailAttachmentSizeError = false;
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                            $scope.filepreview = loadEvent.target.result;
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                        });
                    }
                }
                    reader.readAsDataURL( $scope.attachfileinput);

                });
            }
        };
    }]);

    var app = angular.module('emailsettings').controller('EmailSettingsController',require('./EmailSettingsController')).
   directive("attachstudentfileinput", ['$sce', function ($sce) {
        return {
            require: 'ngModel',
            $scope: {
                attachfileinput: "=",
                filepreview: "="
            },
            link: function ($scope, element, attributes, ngModel) {
                element.bind("change", function (changeEvent) {
                    $scope.maxClassScheduleSizeError = false;
                    var schedulefileSize = this.files[0].size / 1024 / 1024;
                    if (schedulefileSize >= 10) {
                        $scope.maxEmailAttachmentSizeError = true;
                        changeEvent.target.files[0] = "";
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview = "";
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    } else {
                        $scope.maxEmailAttachmentSizeError = false;
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                    reader.readAsDataURL($scope.attachfileinput);

                });
            }
        };
    }]);

    var app = angular.module('eventdetail').controller('EventDetailController',require('./EventDetailController'));

    var app = angular.module('forgotpassword').controller('ForgotPasswordController',require('./ForgotPasswordController'));

    var app = angular.module('home',['datatables', 'datatables.scroller','datatables.select']).controller('HomeController',require('./HomeController')).
    filter('utcToLocal', function () {
        return function (ddate) {
            var date = new Date(ddate);
            date = new Date(date + " UTC").toString();
            var localdtime = date.split(" ");
            var localdatetime = localdtime[0] + ', ' + localdtime[1] + ' ' + localdtime[2] + ' ' + localdtime[3] + ',  ' + localdtime[4].slice(0, 5);
            return localdatetime;
        };
    }).filter('startFrom', function() {
          return function(input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
    }).directive("attachstudentfileinput", ['$sce', function ($sce) {
            return {
                require: 'ngModel',
                $scope: {
                    attachfileinput: "=",
                    filepreview: "="
                },
                link: function ($scope, element, attributes, ngModel) {
                    element.bind("change", function (changeEvent) {
                        $scope.maxClassScheduleSizeError = false;
                        var schedulefileSize = this.files[0].size / 1024 / 1024;
                        if (schedulefileSize >= 10) {
                            $scope.maxEmailAttachmentSizeError = true;
                            changeEvent.target.files[0] = "";
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                            $scope.filepreview = "";
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                            });
                         }
                        } else {
                            $scope.maxEmailAttachmentSizeError = false;
                            $scope.attachfileinput = changeEvent.target.files[0];
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.filepreview = loadEvent.target.result;
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    }
                        reader.readAsDataURL( $scope.attachfileinput);
    
                    });
                }
            }
        }]);  

        var app = angular.module('integrations').controller('IntegrationsController',require('./IntegrationsController'));

        var app = angular.module('leadsdetail').controller('LeadsDetailController',require('./LeadsDetailController'));
        
        var app = angular.module('manageevent',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('ManageEventController',require('./ManageEventController')).
     filter("trust", ['$sce', function ($sce) {  //waiver text show as it is in html content
         return function (htmlCode) {
             return $sce.trustAsHtml(htmlCode);
         }
     }]).service('eventListDetails', function () {
         var editSingleEventValue = [];
         var addSingleEvent = function (newObj) {
             editSingleEventValue = [];
             editSingleEventValue.push(newObj);
         };

         var getSingleEvent = function () {
             return editSingleEventValue;
         };

         return {
             addSingleEvent: addSingleEvent,
             getSingleEvent: getSingleEvent
         };

     }).directive('validQuantity', function () {
         return {
             require: '?ngModel',
             link: function ($scope, element, attrs, ngModelCtrl) {
                 if (!ngModelCtrl) {
                     return;
                 }

                 ngModelCtrl.$parsers.push(function (val) {
                     if (angular.isUndefined(val)) {
                         var val = '';
                     }

                     var clean = val.replace(/[^0-9]/g, '');
                     var negativeCheck = clean.split('-');

                     if (!angular.isUndefined(negativeCheck[1])) {
                         negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                         clean = negativeCheck[0] + '-' + negativeCheck[1];
                         if (negativeCheck[0].length > 0) {
                             clean = negativeCheck[0];
                         }
                     }

                     if (val !== clean) {
                         ngModelCtrl.$setViewValue(clean);
                         ngModelCtrl.$render();
                     }
                     return clean;
                 });

                 element.bind('keypress', function (event) {
                     if (event.keyCode === 32) {
                         event.preventDefault();
                     }
                 });
             }
         };
     }).directive("attacheventfileinput", ['$sce', function ($sce) {
         return {
             require: 'ngModel',
             $scope: {
                 attachfileinput: "=",
                 filepreview: "="
             },
             link: function ($scope, element, attributes, ngModel) {
                 element.bind("change", function (changeEvent) {
                     $scope.maxClassScheduleSizeError = false;
                     var schedulefileSize = this.files[0].size / 1024 / 1024;
                     if (schedulefileSize >= 10) {
                         $scope.maxEmailAttachmentSizeError = true;
                         changeEvent.target.files[0] = "";
                         $scope.attachfileinput = changeEvent.target.files[0];
                         var reader = new FileReader();
                         reader.onload = function (loadEvent) {
                             $scope.$apply(function () {
                                 $scope.filepreview = "";
                                 ngModel.$setViewValue(element.val());
                                 ngModel.$render();
                             });
                         }
                     } else {
                         $scope.maxEmailAttachmentSizeError = false;
                         $scope.attachfileinput = changeEvent.target.files[0];
                         var reader = new FileReader();
                         reader.onload = function (loadEvent) {
                             $scope.$apply(function () {
                                 $scope.filepreview = loadEvent.target.result;
                                 ngModel.$setViewValue(element.val());
                                 ngModel.$render();
                             });
                         }
                     }
                     reader.readAsDataURL($scope.attachfileinput);

                 });
             }
         }
     }]);


var app = angular.module('manageleads', ['datatables', 'datatables.scroller', 'datatables.select', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('ManageLeadsController',require('./ManageLeadsController')).
directive("leadcsvfileinput", ['$sce', function ($sce) {
    return {
        require: 'ngModel',
        $scope: {
            attachfileinput: "=",
            filepreview: "="
        },
        link: function ($scope, element, attributes, ngModel) {
            element.bind("change", function (changeEvent) {
            $scope.attachfileinput = changeEvent.target.files[0];
            var reader = new FileReader();
            reader.onload = function (loadEvent) {
            $scope.$apply(function () {
                $scope.filepreview = loadEvent.target.result;
                ngModel.$setViewValue(element.val());
                ngModel.$render();
            });
            }
            reader.readAsDataURL($scope.attachfileinput);

            });
        }
    }
}]);


var app = angular.module('managemembership', ['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('ManageMembershipController',require('./ManageMembershipController')).
filter("trust", ['$sce', function($sce) {
    return function(htmlCode){
      return $sce.trustAsHtml(htmlCode);
    }
  }]).service('membershipListDetails', function () {   
      var editMembershipValue = [];
      var addMembershipDetail = function (newObj) {
          editMembershipValue = [];
          editMembershipValue.push(newObj);
      };
  
      var getMembershipDetail = function () {
          return editMembershipValue;
      };
  
      return {
          addMembershipDetail: addMembershipDetail,
          getMembershipDetail: getMembershipDetail
      };
  
  }).directive('validQuantity', function () {
      return {
          require: '?ngModel',
          link: function ($scope, element, attrs, ngModelCtrl) {
              if (!ngModelCtrl) {
                  return;
              }
  
              ngModelCtrl.$parsers.push(function (val) {
                  if (angular.isUndefined(val)) {
                      var val = '';
                  }
  
                  var clean = val.replace(/[^0-9]/g, '');
                  var negativeCheck = clean.split('-');
  
                  if (!angular.isUndefined(negativeCheck[1])) {
                      negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                      clean = negativeCheck[0] + '-' + negativeCheck[1];
                      if (negativeCheck[0].length > 0) {
                          clean = negativeCheck[0];
                      }
                  }
  
                  if (val !== clean) {
                      ngModelCtrl.$setViewValue(clean);
                      ngModelCtrl.$render();
                  }
                  return clean;
              });
  
              element.bind('keypress', function (event) {
                  if (event.keyCode === 32) {
                      event.preventDefault();
                  }
              });
          }
      };
  }).directive("attachmanagefileinput", ['$sce', function ($sce) {
          return {
              require: 'ngModel',
              $scope: {
                  attachfileinput: "=",
                  filepreview: "="
              },
              link: function ($scope, element, attributes, ngModel) {
                  element.bind("change", function (changeEvent) {
                      $scope.maxClassScheduleSizeError = false;
                      var schedulefileSize = this.files[0].size / 1024 / 1024;
                      if (schedulefileSize >= 10) {
                          $scope.maxEmailAttachmentSizeError = true;
                          changeEvent.target.files[0] = "";
                          $scope.attachfileinput = changeEvent.target.files[0];
                          var reader = new FileReader();
                          reader.onload = function (loadEvent) {
                          $scope.$apply(function () {
                          $scope.filepreview = "";
                          ngModel.$setViewValue(element.val());
                          ngModel.$render();
                          });
                       }
                      } else {
                          $scope.maxEmailAttachmentSizeError = false;
                          $scope.attachfileinput = changeEvent.target.files[0];
                          var reader = new FileReader();
                          reader.onload = function (loadEvent) {
                          $scope.$apply(function () {
                              $scope.filepreview = loadEvent.target.result;
                              ngModel.$setViewValue(element.val());
                              ngModel.$render();
                          });
                      }
                  }
                      reader.readAsDataURL( $scope.attachfileinput);
  
                  });
              }
          }
      }]);


var app = angular.module('managetrial',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('ManageTrialController',require('./ManageTrialController')).
service('trialListDetails', function () {   //Transferring event details value from manage event to edit event screen.
    var editTrialValue = [];
    var addTrialDetail = function (newObj) {
        editTrialValue = [];
        editTrialValue.push(newObj);
    };

    var getTrialDetail = function () {
        return editTrialValue;
    };

    return {
        addTrialDetail: addTrialDetail,
        getTrialDetail: getTrialDetail
    };

}).filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]).directive('validQuantity', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9]/g, '');
                var negativeCheck = clean.split('-');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
}).directive("attacheventfileinput", ['$sce', function ($sce) {
        return {
            require: 'ngModel',
            $scope: {
                attachfileinput: "=",
                filepreview: "="
            },
            link: function ($scope, element, attributes, ngModel) {
                element.bind("change", function (changeEvent) {
                    $scope.maxClassScheduleSizeError = false;
                    var schedulefileSize = this.files[0].size / 1024 / 1024;
                    if (schedulefileSize >= 10) {
                        $scope.maxEmailAttachmentSizeError = true;
                        changeEvent.target.files[0] = "";
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                        $scope.filepreview = "";
                        ngModel.$setViewValue(element.val());
                        ngModel.$render();
                        });
                     }
                    } else {
                        $scope.maxEmailAttachmentSizeError = false;
                        $scope.attachfileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                            $scope.filepreview = loadEvent.target.result;
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                        });
                    }
                }
                    reader.readAsDataURL( $scope.attachfileinput);

                });
            }
        }
    }]);


var app = angular.module('members', [ 'angular-img-cropper']).controller('MembersController',require('./MembersController')).
directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}).directive("imageinput", [function () {
        return {
            require: 'ngModel',
            $scope: {
                imageinput: "=",
                croppedImage: "="
            },
            link: function ($scope, element, attributes, ngModel) {
                element.bind("change", function (changeEvent) {
                    $scope.maxSizeError = false;
                    var fileSize = this.files[0].size / 1024 / 1024;
                    if (fileSize >= 2) {
                        $scope.maxSizeError = true;
                        changeEvent.target.files[0] = "";
                        $scope.imageinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            $scope.$apply(function () {
                                $scope.croppedImage = "";
                                ngModel.$setViewValue(element.val());
                                ngModel.$render();
                            });
                        }
                    } else {
                    $scope.imageinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                            $scope.croppedImage = loadEvent.target.result;
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                        });
                    }
                }
                    reader.readAsDataURL($scope.imageinput);
                    $scope.isImageUpdate = 'Y';

                });
            }
        }
    }]).directive("logoResize", [
    function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {},
            link: function (scope, elem, attrs) {
                elem.css('height', '100px');
                elem.css('width', '200px');
            }
        };
    }
]).directive("pdffileinput", ['$sce', function ($sce) {
        return {
            require: 'ngModel',
            $scope: {
                pdffileinput: "=",
                pdfpreview: "="
            },
            link: function ($scope, element, attributes, ngModel) {
                element.bind("change", function (changeEvent) {
                    $scope.maxClassScheduleSizeError = false;
                    var schedulefileSize = this.files[0].size / 1024 / 1024;
//                    alert('file size: '+schedulefileSize);
                    if (schedulefileSize >= 10) {
                        $scope.maxClassScheduleSizeError = true;
                        changeEvent.target.files[0] = "";
                        $scope.pdffileinput = changeEvent.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                        $scope.pdfpreview = "";
                        $scope.pdfiframepreview = '';
                        ngModel.$setViewValue(element.val());
                        ngModel.$render();
                        });
                     }
                    } else {
                    $scope.pdffileinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        $scope.$apply(function () {
                            $scope.pdfpreview = loadEvent.target.result;
                            $scope.pdfiframepreview = $sce.trustAsResourceUrl(loadEvent.target.result);
                            ngModel.$setViewValue(element.val());
                            ngModel.$render();
                        });
                    }
                }
                    reader.readAsDataURL($scope.pdffileinput);
                    $scope.isPdfScheduleUpdate = 'Y';

                });
            }
        }
    }]);


var app = angular.module('membershipdetail').controller('MembershipDetailController',require('./MembershipDetailController')).
service('membershipTransferDetails', function () {   
    var editTransferMembershipValue = [];
    var addTransferMembershipDetail = function (newObj) {
        editTransferMembershipValue = [];
        editTransferMembershipValue.push(newObj);
    };

    var getTransferMembershipDetail = function () {
        return editTransferMembershipValue;
    };

    return {
        addTransferMembershipDetail: addTransferMembershipDetail,
        getTransferMembershipDetail: getTransferMembershipDetail
    };

});

var app = angular.module('message').controller('MessageController',require('./MessageController')).
directive('validDate', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\-]/g, '');
                clean = clean.replace(/\s/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if ((event.keyCode === 32) || (event.keyCode === 32)) {
                    event.preventDefault();
                }
            });

        }
    };
}).directive('validTime', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9^APM\s:]/g, '');
//                clean = clean.replace(/\s/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if ((event.keyCode === 32) || (event.keyCode === 32)) {
                    event.preventDefault();
                }
            });
        }
    };
}).filter('dateView',['$filter', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input) {
        if (input !== '0000-00-00') {
            var splitdate = input.split(",");
            var datePart = splitdate[1]; 
            var newdatepart = datePart.trim();
 //            get current date
            var currentDate = new Date();
            var current_dtfilter = $filter('date')(currentDate, 'MMMM d');
            var current_day = parseInt(current_dtfilter.slice(-2));
            var current_relevantDigits = (current_day < 30) ? current_day % 20 : current_day % 30;
            var current_suffix = (current_relevantDigits <= 3) ? suffixes[current_relevantDigits] : suffixes[0];
            var current_daysuffix = current_dtfilter + current_suffix;
            return (newdatepart === current_daysuffix) ? 'Today' : input;
        } else {
            return "";
        }
    };
 }])
.filter('linebreaks', function() {
    return function(text) {
        return text.replace(/\n/g, "<br>");
    }
})
.directive("keepScroll", function(){
  
  return {

    controller : function(){
      var element = 0;
      
      this.setElement = function(el){
        element = el;
      }

      this.addItem = function(item){
//        console.log("Adding item", item, item.clientHeight);
        element.scrollTop = (element.scrollTop+item.clientHeight); 
      };
      
    },
    
    link : function(scope,el,attr, ctrl) {
      
     ctrl.setElement(el[0]);
      
    }
    
  };
  
})
.directive("scrollItem", function(){
  return{
    require : "^keepScroll",
    link : function(scope, el, att, scrCtrl){
      scrCtrl.addItem(el[0]);
    }
  };
});

var app = angular.module('payment', ['datatables', 'datatables.scroller','datatables.select']).controller('PaymentController',require('./PaymentController')).
directive("attachpayfileinput", ['$sce', function ($sce) {
    return {
        require: 'ngModel',
        $scope: {
            attachfileinput: "=",
            filepreview: "="
        },
        link: function ($scope, element, attributes, ngModel) {
            element.bind("change", function (changeEvent) {
                $scope.maxClassScheduleSizeError = false;
                var schedulefileSize = this.files[0].size / 1024 / 1024;
//                    alert('file size: '+schedulefileSize);
                if (schedulefileSize >= 10) {
                    $scope.maxEmailAttachmentSizeError = true;
                    changeEvent.target.files[0] = "";
                    $scope.attachfileinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                    $scope.$apply(function () {
                    $scope.filepreview = "";
                    ngModel.$setViewValue(element.val());
                    ngModel.$render();
                    });
                 }
                } else {
                    $scope.maxEmailAttachmentSizeError = false;
                    $scope.attachfileinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                    $scope.$apply(function () {
                        $scope.filepreview = loadEvent.target.result;
                        ngModel.$setViewValue(element.val());
                        ngModel.$render();
                    });
                }
            }
                reader.readAsDataURL( $scope.attachfileinput);

            });
        }
    }
}]);


var app = angular.module('referral').controller('ReferralController',require('./ReferralController')).
filter('utcToLocalTime', function () {
    return function (ddate) {
        
        var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                   navigator.userAgent && !navigator.userAgent.match('CriOS');
           if(isSafari){
              //alert('yes safari');
               //alert(ddate+ ' ddate');
               var dda = ddate.toString();
               dda = dda.replace(/ /g,"T");
               //alert(dda+ ' dda');
               var time = new Date(dda);
               var tom = time.toString();
                //date = new Date(date + " UTC").toString();
                // format AM PM start
                 // var time = new Date(date);
                  var hours = time.getHours();
                  var minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'PM' : 'AM';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var localTime = hours + ':' + minutes + ' ' + ampm;
                // format AM PM end 
                var fulldate = tom.replace(/GMT.*/g,"");
                var localdatetime = fulldate.slice(4, -9)+" at "+localTime;
               // alert('localdatetime '+localdatetime);
                return localdatetime;
           }else{
               //alert('no safari');
               var date = new Date(ddate);
                date = new Date(date + " UTC").toString();
                // format AM PM start
                  var time = new Date(date);
                  var hours = time.getHours();
                  var minutes = time.getMinutes();
                  var ampm = hours >= 12 ? 'PM' : 'AM';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var localTime = hours + ':' + minutes + ' ' + ampm;
                // format AM PM end 
                var fulldate = date.replace(/GMT.*/g,"");
                var localdatetime = fulldate.slice(4, -9)+" at "+localTime;
                return localdatetime;
           }
//        var localdtime = date.split(" ");
//        var localdatetime = localdtime[0] + ', ' + localdtime[1] + ' ' + localdtime[2] + ' ' + localdtime[3] + ',  ' + localdtime[4].slice(0, 5);

    };
});

var app = angular.module('register').controller('registrationController',require('./registrationController')).
directive('onlyLettersInput', function onlyLettersInput() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z0-9]/g, '');
                //console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}).directive('popover',['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, elem) {

            var content = $(".popover-content").html();
            var compileContent = $compile(content)(scope);
            var title = $(".popover-head").html();
            var options = {
                content: compileContent,
                html: true,
                title: title
            };
            
            $(elem).popover(options);
        }
    };
}]).directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}).directive('validAlphanumeric', function () {
    return {
        require: '?ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z0-9]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

var app = angular.module('resetpassword').controller('ResetPasswordController',require('./ResetPasswordController'));

var app = angular.module('social').controller('SocialController',require('./SocialController'));

var app= angular.module('template').controller('TemplateController',require('./TemplateController'));

var app= angular.module('templatemembership').controller('TemplateMembershipController',require('./TemplateMembershipController'));

var app = angular.module('trialdetail').controller('TrialDetailController',require('./TrialDetailController'));

var app = angular.module('trials',['datatables', 'datatables.scroller','datatables.select','ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']).controller('TrialsController',require('./TrialsController'))
.directive("attachfileinput", ['$sce', function ($sce) {
    return {
        require: 'ngModel',
        $scope: {
            attachfileinput: "=",
            filepreview: "="
        },
        link: function ($scope, element, attributes, ngModel) {
            element.bind("change", function (changeEvent) {
                $scope.maxClassScheduleSizeError = false;
                var schedulefileSize = this.files[0].size / 1024 / 1024;
                if (schedulefileSize >= 10) {
                    $scope.maxEmailAttachmentSizeError = true;
                    changeEvent.target.files[0] = "";
                    $scope.attachfileinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                    $scope.$apply(function () {
                    $scope.filepreview = "";
                    ngModel.$setViewValue(element.val());
                    ngModel.$render();
                    });
                 }
                } else {
                    $scope.maxEmailAttachmentSizeError = false;
                    $scope.attachfileinput = changeEvent.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                    $scope.$apply(function () {
                        $scope.filepreview = loadEvent.target.result;
                        ngModel.$setViewValue(element.val());
                        ngModel.$render();
                    });
                }
            }
                reader.readAsDataURL( $scope.attachfileinput);

            });
        }
    }
}]);

var app= angular.module('trialtemplate').controller('TrialTemplateController',require('./TrialTemplateController'));
var app= angular.module('wepaycreation').controller('WepayCreationController',require('./WepayCreationController'));
var app = angular.module('miscdetail').controller('MiscDetailController',require('./MiscDetailController'));
var app= angular.module('stripecreation').controller('StripeCreationController',require('./StripeCreationController'));