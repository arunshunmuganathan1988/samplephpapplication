RetailFulfillmentController.$inject=['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'DTDefaultOptions', '$window', '$rootScope'];
 function RetailFulfillmentController($scope, $compile, $location, $http, $localStorage, urlservice, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, DTDefaultOptions, $window, $rootScope) {
        if ($localStorage.islogin === 'Y') {

            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().clearPreviewLocaldata();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            tooltip.hide();
            $rootScope.activepagehighlight = 'Retail';
            $scope.fulfillmentdashboard = true;
            $scope.editordersview = $scope.opendropdownview = $scope.editdropdowntoggleview = false;
            $scope.fulfillmenttab = 'all';
            $localStorage.retailfromliveordraft = '';
            $scope.editselectbox = $scope.order_details_edited = $scope.checkout_details = [];
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.updated_flag = "N";
            $scope.current_order_id= $localStorage.currentRetailorderid;
            $scope.cancelled_note = false;
            $scope.all_products_deleted = false;
            $scope.optional_discount_applied = false;
            $scope.payment_method = 'CC';
            //go retail main page
            $scope.goretailmainpage = function(){
                $location.path('/manageretail');
            };
            $scope.gotoretailsettings = function(){
                $localStorage.retailfromliveordraft = 'retailsettings';
                $location.path('/manageretail');
            };
            //sort retail fulfillment 
            $scope.sortretailfulfillment = function(type){
                $scope.fulfillmenttab = type;
                $scope.getretailfulfillment();
            };
            
            //get fulfillment table details
            $scope.getretailfulfillment = function(){
                    DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
                    $scope.retailfulfillmentdtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            method: 'POST',
                            url: urlservice.url + 'getretailfulfillment',                            
                            xhrFields: {
                                withCredentials: true
                            },
                            dataSrc: function (json) {
                                $scope.retailfulfillmentlistcontents = json.data;
                                 $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                 "company_id": $localStorage.company_id,
                                 "retail_fulfillment_tab": $scope.fulfillmenttab,
//                                 "search":$scope.searchTerm,
                            },
                            
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                }else if(xhr.responseJSON.status === 'Expired'){
                                    $("#failuremessageModal").modal('show');
                                    $("#failuremessagetitle").text('Message');
                                    $("#failuremessagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                }else{
                                    $('#progress-full').hide();
                                }
                            } 
                           
                        })
                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })
  
                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 570)
                        .withOption('scrollX', '100%')
                        .withOption('fnPreDrawCallback', function () {  $('#progress-full').show(); })
                        .withOption('fnDrawCallback', function () {  $('#progress-full').hide(); })
                        .withOption('serverSide', true)
                        .withOption('info', false)
                        .withOption('stateSave',true)
                        .withOption('stateDuration', 60*5)
                        .withOption('order', [0, 'desc'])
                        .withOption('search', false)
                        .withLanguage({
                            "sSearch": "",
                            "searchPlaceholder": "  Q  Search orders",
                        });
//                        .withOption('bFilter', false);
                        $scope.retailfulfillmentdtColumns = [
                                DTColumnBuilder.newColumn('Date').withTitle('Date').withClass('order_date').renderWith(
                                function (data, type, full) {
                                    return '<span class="fulfilletxt">' + full.order_date + '</span>';
                                }).notSortable(),
                                DTColumnBuilder.newColumn('customer_name').withTitle('Customer').withClass('customer_name').renderWith(
                                function (data, type, full, meta) {
                                    return '<span class="buyername fulfilletxt" ng-click="goeditorders(\'' + full.checkout_id + '\')">' + full.customer_name + '</span>';
                                }).notSortable(),
                                DTColumnBuilder.newColumn('item').withTitle('Item').withClass('retail_item').renderWith(
                                function (data, type, full) {
                                    return '<span class="fulfilletxt">' + full.item + '</span>';
                                }).notSortable(),
                                DTColumnBuilder.newColumn('status').withTitle('Status').withClass('fulfillment_status').renderWith(
                                function (data, type, full, meta) {
                                    if(full.status === "F"){
                                        return '<div class="fgreenbg fulfilletxt" ng-click="goeditorders(\'' + full.checkout_id + '\')" style="cursor:pointer;">Fullfilled</div>';
                                    }else if(full.status === "U"){
                                        return '<div class="yellowbg fulfilletxt" ng-click="openfulfillmodal(\'' + full.fulfillment_id + '\',\'' + full.checkout_id + '\',\'F\',\''+ full.order_detail_status+ '\')" style="cursor:pointer;">Unfulfilled</div>';
                                    }else{
                                        return '<div class="fgreybg fulfilletxt" style="cursor:pointer;">Cancelled</div>';   
                                    }
                                }).notSortable(),
                    ];
                   $scope.dtInstance = {};
                   $('#progress-full').hide();
                    angular.element('body').on('search.dt', function () {
                    $scope.searchTerm = document.querySelector('.dataTables_filter input').value;
                })
            };
//            if(($localStorage.retailpagefrom == 'payment')) {
//                $scope.goeditorders($scope.current_order_id);
//            }else{
//                $scope.getretailfulfillment();
//            }
            
            //go back to fulfillment view
            $scope.gobacktofulfillmentview = function(){
                $(".modal-backdrop").removeClass('modal-backdrop fade in');   // REMOVING BACKDROP IN MODAL WHILE REDIRECTING IN MODAL CLICK
                if($localStorage.retailpagefrom === "payment"){
                    $location.path('/payment');
                }
                $localStorage.retailpagefrom = '';
                $scope.fulfillmentdashboard = true;
                $scope.editordersview = $scope.opendropdownview = false;
                $route.reload();
            };
                        
            // CONFIRMATION HERE FOR UNSAVED CHANGES
            $scope.confirmcanceledit = function(){
                if ($scope.updated_flag === 'Y' && $scope.opendropdownview) {
                    $("#editconfirmmodal").modal('show');
                } else {
                    $scope.gobacktofulfillmentview();
                }
            };
                        
            $scope.editorder = function(){
                $("#editOrderModal").modal('show');
            };
            $scope.cancelEditorder = function(){
                $scope.editordernote = '';
            };
            $scope.openEditOrderView = function () {
                $scope.opendropdownview = true;
            };
            $scope.cancelEditNote = function(){
                $scope.editnote = '';
            };
            $scope.editOrderToggle = function(){
                $scope.editdropdowntoggleview = $scope.editdropdowntoggleview === false ? true: false;
            };
            $scope.openFulfillmentModals = function(type,notestatus,note_id,updatenote){
                $scope.editnote = "";
                if(type === 'processchange'){
                   $("#processChangeModal").modal('show'); 
                }else if(type === 'csv'){
                   $("#sendCSVModal").modal('show');  
                }else if(type === 'addnote'){
                    $scope.notestatus = notestatus;
                    if($scope.notestatus === "update"){
                        $scope.editnote = updatenote;
                        $scope.noteid = note_id;
                    }
                    $("#editNoteModal").modal('show');
                }
            };
            
            $scope.confirmSendFulfillmentCSV = function(){
            var searchTerm = document.querySelector('.dataTables_filter input').value;
            $('#progress-full').show();  
            function formatTodayDate() {
                    var d = new Date,
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;

                    return [year, month, day].join('-');
                }
                $scope.today_date = formatTodayDate();
            $http({
                    method: 'POST',
                    url: urlservice.url + 'createRetailFulfillmentCSV',
                    responseType: 'arraybuffer',
                    data: {
                        "company_id": $localStorage.company_id,
                        "fulfillment_tab": $scope.fulfillmenttab,
                        "search":searchTerm,
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true
                }).success(function (data, status, headers) {
                    var response = '';
                    if ('TextDecoder' in window) {
                        // DECODE AS UTF-8
                        var dataView = new DataView(data);
                        var decoder = new TextDecoder('utf8');
                        if ($scope.isJson(decoder.decode(dataView)) === true) {
                            response = JSON.parse(decoder.decode(dataView));
                        }
                    } else {
                        // FALLBACK DECODE AS ASCII
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                        if ($scope.isJson(decodedString) === true) {
                            response = JSON.parse(decodedString);
                        }
                    }
                    if (response.status !== undefined && response.status === 'Failed') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.msg);                        
                        return false;
                    } else {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text("Buyer Details exported as csv file successfully.");
                    }
                    var msg = '';
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], {type: "application/x-download"});
                        var objectUrl = URL.createObjectURL(blob);
                        linkElement.setAttribute('href', objectUrl);
                        linkElement.setAttribute("download", "Retail_" + $scope.today_date + ".csv");
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        msg = ex;
                        console.log(ex);
                    }
                    if (msg === '') {
                        msg = "CSV file downloaded Successfully";
                    }
                    $('#progress-full').hide();
                    $("#fulfillmentmsgmodal").modal('show');
                    $("#fulfillmentmsgcontent").text(msg);

                }).error(function () {
                    $('#progress-full').hide();
                    $("#fulfillmentmsgmodal").modal('show');
                    $("#fulfillmentmsgcontent").text("Unable to reach Server, Please Try again.");
                });
            };
            $scope.isJson = function (str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            };
            
             //open retail fullfillment support link in new tab
            $scope.RetailOrderSupport = function(support){
                if(support === 'edit'){
                    window.open("https://intercom.help/mystudio/retail-and-merchandise-shop/7-editing-and-refunding-orders/editing-and-refunding-retail-orders",'_blank'); 
                }else if(support === 'fulfillment'){
                    window.open("https://intercom.help/mystudio/retail-and-merchandise-shop/6-retail-fulfillment-center/fulfilling-and-managing-retail-orders",'_blank'); 
                }
            };
            
            $scope.openfulfillmodal = function(f_id,c_id,status,payment_status){//fulfilment id and checkout id
                $scope.item_name = '';
                $scope.checkout_id = c_id;
                $scope.fulfill_id = f_id;
                $scope.fulfill_status = status;
                if($scope.retailfulfillmentlistcontents.length > 0){
                    for(var i =0 ; i < $scope.retailfulfillmentlistcontents.length ; i++){                           
                        if(parseInt($scope.retailfulfillmentlistcontents[i].fulfillment_id) == parseInt(f_id)){
                            $scope.item_name = $scope.retailfulfillmentlistcontents[i].item;
                        }
                    }
                }
                $scope.editordernote = "";
                if(payment_status !== 'S'){
                    $("#fulfillmentmsgmodal").modal('show');
                    $("#fulfillmentmsgcontent").text('Fullfillment conversion should be allowed for payment completed orders');
                }else{
                    $("#editOrderModal").modal('show');
                    if(status === "F"){//fullfill
                        $("#editOrderTitle").text('Fulfill Order');
                        $("#editorderbutton").text('Fulfill');     
                    }else{//unfulfill
                        $("#editOrderTitle").text('Unfulfill Order');
                        $("#editorderbutton").text('Unfulfill'); 
                    }
                }                           
            };
            
            $scope.changestatus = function(c_id,f_id,status,name){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'changefulfillmentstatus',
                    data: {
                        "company_id": $localStorage.company_id,
                        "checkout_id": c_id, 
                        "fulfillment_id":f_id,
                        "note":$scope.editordernote,
                        "fulfillment_status":status,
                        "item_name":name
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.goeditorders(c_id);
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };  
            
            //add note for checkout orders
            $scope.updatenote = function(c_id,type,note_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatefulfillmentnote',
                    data: {
                        "company_id": $localStorage.company_id,
                        "checkout_id": c_id, 
                        "type":type,
                        "note":$scope.editnote,
                        "note_id":note_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.order_notes = response.data.msg;
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            
            //go edit orders
            $scope.goeditorders = function(id){
                $scope.checkout_id = id;
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'getRetailOrderDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "order_id":id,
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                      withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.payment_method = response.data.msg.order.payment_method;
                        if(response.data.msg.order.optional_discount_flag === "Y"){
                            $scope.optional_discount_applied = true;
                        }
                        $scope.all_products_deleted = false;
                        $scope.order_notes = response.data.msg.notes;
                        $scope.order_details = response.data.msg.order_details;
                        $scope.order_details_edited = angular.copy($scope.order_details);
                        $scope.checkout_details = response.data.msg.order;
                        $scope.sub_total = 0;
                        $scope.customer_name = $scope.checkout_details.buyer_name;
                        $scope.totalpaid = parseFloat($scope.checkout_details.retail_order_checkout_amount);
                        for(var i = 0; i < $scope.order_details.length ; i++){
                            $scope.sub_total = parseFloat($scope.sub_total) + parseFloat($scope.order_details[i].retail_payment_amount);
                        }
                        setTimeout(function(){ $scope.createselectoptions($scope.order_details); }, 100);
                        $scope.fulfillmentdashboard = false;
                        $scope.editordersview = true;
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Failed') {
                        $scope.all_products_deleted = true;
                        $scope.order_notes = response.data.msg.notes;
                        $scope.order_details = response.data.msg.order_details;
                        $scope.checkout_details = response.data.msg.order;
                        $scope.sub_total = 0;
                        $scope.totalpaid = 0;
                        $scope.fulfillmentdashboard = false;
                        $scope.editordersview = true;
                        $('#progress-full').hide();
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };
        
        $scope.createselectoptions = function(order_details){
            for (var i = 0; i < order_details.length; i++) {
                    var x = document.getElementById("editselectbox[" + i + "]");
                    for (var j = order_details[i].retail_quantity; j >= 0; j--) {
                        var option = document.createElement("option");
                        option.text = j;
                        x.add(option);
                    }
                    document.getElementById('editselectbox['+i+']').value = order_details[i].retail_quantity;
                }
        };
        $scope.catcheditselectbox = function(index,value){
            $scope.order_details_edited[index].updated_quantity = value;
            $scope.retailEditorderDetails();
        };
        $scope.canceleditorder = function () {
                $scope.opendropdownview = $scope.cancelled_note = false;
                $scope.updated_flag = "N";
                $scope.goeditorders($scope.checkout_id);
            };
        
        $scope.retailEditorderDetails = function(){
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'retailEditorderDetails',
                    params: {
                        "company_id": $localStorage.company_id,
                        "order": $scope.checkout_details, 
                        "order_details":(JSON.stringify($scope.order_details_edited))
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true
                        

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.order_details_updated = response.data.msg.order_details;
                        $scope.checkout_details = response.data.msg.order;
                        $scope.sub_total = 0;
                        if($scope.checkout_details.order_updated_flag === 'Y'){
                            $scope.updated_flag = "Y";
                            $scope.cancelled_note = true;
                            $scope.cancelled_note_date = new Date();
                            $scope.cancelled_note_text = response.data.msg.note;
                        }else{
                            $scope.updated_flag = "N";
                            $scope.cancelled_note = false;
                        }
                        $scope.totalpaid = parseFloat($scope.checkout_details.retail_order_checkout_amount);
                        $scope.process_type = $scope.checkout_details.process_type;
                        $scope.refund_amount = $scope.checkout_details.process_amount;
                        for(var i = 0; i < $scope.order_details_updated.length ; i++){
                            $scope.order_details[i].item = $scope.order_details_updated[i].item;
                            $scope.order_details[i].retail_quantity = $scope.order_details_updated[i].retail_quantity;
                            $scope.order_details[i].retail_cost = $scope.order_details_updated[i].retail_cost;
                            $scope.order_details[i].retail_payment_amount = $scope.order_details_updated[i].retail_payment_amount;
                            $scope.order_details[i].updated_flag = $scope.order_details_updated[i].updated_flag;
                            $scope.sub_total = parseFloat($scope.sub_total) + parseFloat($scope.order_details_updated[i].retail_payment_amount);
                        }
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }; 
            
            
            //cancell  order and refund
            $scope.refundorders = function(){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'refundRetailOrders',
                    data: {
                        "company_id": $localStorage.company_id,
                        "order": JSON.stringify($scope.checkout_details), 
                        "order_details":(JSON.stringify($scope.order_details_edited)),
                        "process_type":$scope.process_type
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $scope.cancelled_note = false;
                        $scope.canceleditorder();
                        $('#progress-full').hide();
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#fulfillmentmsgmodal").modal('show');
                        $("#fulfillmentmsgcontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }
            $scope.getretailfulfillment();
            if(($localStorage.retailpagefrom == 'payment')) {
                $scope.goeditorders($scope.current_order_id);
            } 
        } else {
            $location.path('/login');
        }

    };
    
    module.exports=RetailFulfillmentController;