/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

LeadsDetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope'];
 function LeadsDetailController ($scope, $location, $http, $localStorage, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'leads';
            tooltip.hide(); 
            $scope.leadsdetailsview = true;
            $scope.leadshistoryview = false;
            $scope.leadseditStatus = false;
            $scope.currentLeads_reg_id = $localStorage.currentLeadsregid;
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            
            
            $scope.Leads_Detail = function () {
                $('body').removeClass('modal-open');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'leadsRegDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "leads_reg_id": $scope.currentLeads_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.leads_detail = response.data.msg;
                        $scope.leads_detail_all = response.data.msg.All;
                        $scope.currentLeads_reg_id = $scope.leads_detail.leads_reg_id;
                        $scope.buyer_name = $scope.leads_detail.buyer_first_name + $scope.leads_detail.buyer_last_name;
                        $scope.participant_name = $scope.leads_detail.participant_first_name + $scope.leads_detail.participant_last_name;
                        $scope.leads_sourcearray = $scope.leads_detail_all.lead_source;
                        $scope.leads_pgmInteresrarray = $scope.leads_detail_all.program_interest;
                        $scope.editLeadsStatus = $scope.leads_detail.leads_status;
                    } else if (response.data.status === 'Expired') {
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.Leads_History = function () {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'leadsHistoryDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "leads_reg_id" : $scope.currentLeads_reg_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',},
                        withCredentials: true

                }).then(function (response) {
                        $('#progress-full').hide();
                        $(".modal-backdrop").removeClass('modal-backdrop fade in');
                    if (response.data.status === 'Success') {
                        $scope.leads_history = response.data.msg;
                    }  else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                         $scope.handleFailure(response.data.msg);
                    }
                    
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.ShowNewEditLeadsDetails = function () {
                $scope.showLeadsEditconfirm = true;
                $scope.editbuyer_fname = $scope.leads_detail.buyer_first_name;
                $scope.editbuyer_lname = $scope.leads_detail.buyer_last_name;
                $scope.editbuyer_phone = $scope.leads_detail.buyer_phone;
                $scope.editbuyer_email = $scope.leads_detail.buyer_email;
                $scope.editparticipant_fname = $scope.leads_detail.participant_first_name;
                $scope.editparticipant_lname = $scope.leads_detail.participant_last_name;
                $scope.selected_editPgmInterest_id = $scope.leads_detail.program_id;
                $scope.selected_editSource_id = $scope.leads_detail.source_id;
                $scope.leadsstatus = $scope.leads_detail.leads_status;
            };
            
            $scope.showLeadsStatusEdit = function () {
                $scope.leadseditStatus = true;
            };
            $scope.showLeadsStatusEditCancel = function (exist_status) {
                $scope.leadsstatus = exist_status;
                $scope.leadseditStatus = false;
            };
            $scope.performLeadsAction = function (a) { //Show list of Actions 
                $scope.leadseditStatus = false;
                if (a === 'A') {//Active
                    $scope.leadsactionValue = 0;
                    $("#leadsStatusModal").modal('show');
                    $("#leadsStatustitle").text('Update Lead Status');
                    $("#leadsStatuscontent").text('Are you sure you want to change the status to Active');
                }else if (a === 'E') {//Enrolled
                    $scope.leadsactionValue = 1;
                    $("#leadsStatusModal").modal('show');
                    $("#leadsStatustitle").text('Update Lead Status');
                    $("#leadsStatuscontent").text('Are you sure you want to change the status to Enrolled');
                } else if (a === 'NI') {//cancel
                    $scope.leadsactionValue = 2;
                    $("#leadsStatusModal").modal('show');
                    $("#leadsStatustitle").text('Update Lead Status');
                    $("#leadsStatuscontent").text('Are you sure you want to change the status to Not Interested');
                } 
            };
            $scope.actionLeadsConfirm = function (value) {//work
                if (value === 0) {
                      $scope.updateLeadsStatus('A');
                }
                if (value === 1) {
                      $scope.updateLeadsStatus('E');
                }
                if (value === 2) {
                     $scope.updateLeadsStatus('NI');
                }
            };
            $scope.updateLeadsStatus = function (status) { 
                $('#progress-full').show();
                $("#leadsStatusModal").modal('hide');
                  
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updateLeadsParticipantStatus',
                    data: {
                        "company_id": $localStorage.company_id,
                        "leads_reg_id": $scope.currentLeads_reg_id,
                        "leads_status": status,
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.resettabbarclass();
                        $('#li-leadsdetails').addClass('active-event-list').removeClass('event-list-title'); 
                        $('.trialdetail-tabtext-1').addClass('greentab-text-active');
                        $scope.leads_detail = response.data.reg_details.msg;
                        $scope.currentLeads_reg_id = $scope.leads_detail.leads_reg_id;
                        $scope.editLeadsStatus = $scope.leads_detail.leads_status;
                        $scope.showLeadsStatusEditCancel($scope.editLeadsStatus);
                }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                }else{
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.UpdateLeadsDetail = function (){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'editLeadsDetails',
                    data: {
                        "company_id": $localStorage.company_id,
                        "leads_id": $scope.currentLeads_reg_id,
                        "buyer_first_name": $scope.editbuyer_fname,
                        "buyer_last_name": $scope.editbuyer_lname,
                        "phone": $scope.editbuyer_phone,
                        "email": $scope.editbuyer_email,
                        "participant_first_name": $scope.editparticipant_fname,
                        "participant_last_name": $scope.editparticipant_lname,
                        "program_interest_id": $scope.selected_editPgmInterest_id,
                        "source_id": $scope.selected_editSource_id
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.leads_detail = response.data.leads_details.msg;
                        $scope.currentLeads_reg_id = $scope.leads_detail.leads_reg_id;
                        $scope.leadseditStatus = false;
                    }else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                }else{
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.cancelLeadsDetailUpdate = function () { 
               $scope.showLeadsEditconfirm = false;
               $scope.leadseditStatus = false;
            };
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.leadsdetails = function () {
                $scope.leadsdetailsview = true;
                $scope.leadshistoryview = false;
                $scope.resettabbarclass();
                $('#li-leadsdetails').addClass('active-event-list').removeClass('event-list-title'); 
                $('.trialdetail-tabtext-1').addClass('greentab-text-active');
                $scope.Leads_Detail();
            };
            
            $scope.leadshistory = function () {
                $scope.leadsdetailsview = false;
                $scope.leadshistoryview = true;
                $scope.showLeadsEditconfirm = false;
                $scope.resettabbarclass();
                $('#li-leadshistory').addClass('active-event-list').removeClass('event-list-title');
                $('.trialdetail-tabtext-2').addClass('greentab-text-active');
                $scope.Leads_History();
            };
                        
            $scope.editLeadsHistoryNote = function (ind, hist_id, action) {
                if(action === 'add'){
                    $scope.leads_edit_status = 'add';
                    $scope.edited_leads_history_id = '';
                    $scope.leadshistorynote = '';
                    $scope.LeadsHistoryNoteButton = 'Add';
                    $("#leadsHistoryNoteModal").modal('show');
                    $("#leadsHistoryNotetitle").text('Add Note');                    
                }else if(action === 'edit'){
                    $scope.edited_leads_history_id = hist_id ;
                    $scope.leads_edit_status = 'update';
                    $scope.LeadsHistoryNoteButton = 'Update';
                    $scope.leadshistorynote = $scope.leads_history[ind].activity_text;
                    $("#leadsHistoryNoteModal").modal('show');
                    $("#leadsHistoryNotetitle").text('Update Note');
                }else if(action === 'delete'){
                    $scope.edited_leads_history_id = hist_id ;
                    $scope.leadshistorynote =  '';
                    $scope.leads_edit_status = 'delete';
                    $("#leadsHistoryNoteDeleteModal").modal('show');
                    $("#leadsHistoryNoteDeletetitle").text('Delete Note');
                    $("#leadsHistoryNoteDeletecontent").text('Are you sure want to delete the note?');
                }
            };
            
            $scope.confirmleadseditHistoryNote = function () {
                $("#leadsHistoryNoteModal").modal('hide');
                $("#leadsHistoryNoteDeleteModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'LeadsHistoryNotes',
                    data: {
                        "company_id": $localStorage.company_id,
                        "history_id": $scope.edited_leads_history_id,
                        "status":$scope.leads_edit_status,
                        "leads_reg_id": $scope.currentLeads_reg_id,
                        "note_text": $scope.leadshistorynote
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.leads_history = response.data.leads_history.msg; 
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#leadsmessageModal").modal('show');
                        $("#leadsmessagetitle").text('Message');
                        $("#leadsmessagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            };                       
            
            $scope.closeLeadsDetailView = function () {
                $('#progress-full').show();
                $location.path('/manageleads');
            };
            
            $scope.openLeadsDetailsPage = function () {
                  $scope.leadsdetails();
            };
                       
        } else {
            $location.path('/login');
        }
    }
    module.exports =  LeadsDetailController;