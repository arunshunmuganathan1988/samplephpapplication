
ManageEventController.$inject = ['$scope', '$compile', '$location', '$http', '$localStorage', 'urlservice', 'eventListDetails', '$filter', '$route', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'DTDefaultOptions', '$window', '$rootScope'];

function ManageEventController($scope, $compile, $location, $http, $localStorage, urlservice, eventListDetails, $filter, $route, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, DTDefaultOptions, $window, $rootScope) {
    if ($localStorage.islogin === 'Y') {

        angular.element(document.getElementById('sidetopbar')).scope().getName();
        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
        angular.element(document.getElementById('sidetopbar')).scope().regComplete();
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//        angular.element(document.getElementById('sidetopbar')).scope().getWepayStatus();
        angular.element(document.getElementById('sidetopbar')).scope().getStripeStatus();
        angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
        angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
        tooltip.hide();        
            $rootScope.activepagehighlight = 'events';
            //PREVIEW SINGLE EVENT ADD
           $localStorage.PreviewEventName = '';
           $localStorage.PreviewEventDate = '';
           $localStorage.PreviewEventTime = '';
           $localStorage.PreviewEventEndDate = '';
           $localStorage.PreviewEventEndTime = '';
           $localStorage.PreviewEventCost = '';
           $localStorage.PreviewEventCompareCost = '';
           $localStorage.PreviewEventUrl = '';
           $localStorage.PreviewEventDesc = '';
           $localStorage.PreviewImageUrl = '';
           $localStorage.PreviewECapacity = '';
           $localStorage.PreviewESpaceRemaining = '';
           $localStorage.PreviewEProcesFeeSelection = ''; 
           $localStorage.PreviewCEProcesFeeSelection = '';
           
            //PREVIEW MULTIPLE- PARENT EVENT ADD
           $localStorage.PreviewMEventCategory = '';
           $localStorage.PreviewMEventSubcategory = '';
           
            $localStorage.PreviewPEventDesc = '';
            $localStorage.PreviewPEventUrl = '';
            $localStorage.PreviewPImageUrl = '';
            $localStorage.PreviewEditPImageUrl = '';
            
           //PREVIEW EXISTING CHILD EVENT EDIT
           $localStorage.PreviewCfirstedit = '';
           $localStorage.childeventindex = '';
           $localStorage.PreviewCEventDate = '';
           $localStorage.PreviewCEventTime = '';
           $localStorage.PreviewCEventEndDate = '';
           $localStorage.PreviewCEventEndTime = '';
           $localStorage.PreviewCEventCost = '';
           $localStorage.PreviewCEventCompareCost = '';
           $localStorage.PreviewCEventUrl = '';
           $localStorage.PreviewCEventUrlEdit = '';
           $localStorage.PreviewCEventDescEdit = '';
           $localStorage.PreviewCEventName = '';
           $localStorage.PreviewCECapacity = '';
           $localStorage.PreviewCESpaceRemaining = '';
           $localStorage.PreviewCfirstedit = 'N';
           
       //PREVIEW NEW CHILD EVENT ADD
        $localStorage.PreviewMEventName = '';           
        $localStorage.PreviewMEventDate = '';
        $localStorage.PreviewMEventTime = '';
        $localStorage.PreviewMEventEndDate = '';
        $localStorage.PreviewMEventEndTime = '';
        $localStorage.PreviewMEventUrl = '';
        $localStorage.PreviewMEventDesc = '';
        $localStorage.PreviewMEventCost = '';
        $localStorage.PreviewMEventCompareCost = '';
        $localStorage.preview_eventchildlist = $localStorage.preview_childlisthide = false;
        $localStorage.PreviewMECapacity = '';
        $localStorage.PreviewMESpaceRemaining = '';
        $localStorage.PreviewChildEventList = '';

        $localStorage.PreviewOldRegistrationField = '';
        $localStorage.PreviewOldRegFieldMandatory = '';
        $localStorage.preview_reg_col_name_edit = '';
        $localStorage.preview_reg_fieldindex = '';
        $localStorage.PreviewWaiverText = '';
        $localStorage.currentpreviewtab = '';


        $localStorage.eventpagetype = 'new';
        $localStorage.currentpreviewtab = 'events';  
        $localStorage.currentpage = "events";
        $scope.message = "MyStudio";
        $scope.checkin_type = 'R';
        $scope.wepaystatus = $localStorage.preview_wepaystatus;
        $scope.wp_currency_code = $localStorage.wp_currency_code;
        $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;

        $scope.selectedorderlist = [];
        $scope.check_all_order = false;
        $scope.check_all_cancel = false;
        $scope.current_tab = 'O';
        $scope.also_send_email = false;
        $localStorage.buyerName = "";
        $localStorage.participantName = "";
        $localStorage.currentEventregid = ""; 
        $scope.maxEmailAttachmentSizeError = false;
        $scope.filepreview = ""; 
        $scope.attachfile = '';
        $scope.file_name = '';
        $scope.fileurl = '';
        $scope.change_made = $scope.search_value = '';
        $scope.data_loaded_length = 0;
          
        $scope.window_bottom_hit_limit = 0;
        $scope.window_bottom_hit_page = 'P';
        $scope.currentEventPage = false;
        $scope.bottomhitstopcall = false;
        $scope.manage_parent_title = '';
        $localStorage.eventenabledstatus = '';
        $scope.liveEventList = [];
        $scope.draftEventList = [];
        $scope.pastEventList = [];
        $scope.events_payment_method = 'CC';
        $scope.payment_method_id = '';
        $scope.stripe_customer_id = ''; 
        
         //First time login message Pop-up ----START 
        tooltip.hide();

        if ($localStorage.verfication_complete !== 'U') {
            if ($localStorage.firstlogin) {
                if ($localStorage.firstlogin_event === 'Y') {
                    $('#tooltipfadein').css("display", "block");
                    $localStorage.firstlogin_event = 'N';
                    $scope.evnttip = false;
                    tooltip.pop("eventlink", "#eventtip", {position: 1});
                }
            }
        }

        $("#eventdetailNext").click(function () {
            $("#tooltipfadein").css("display", "none");
            $scope.evnttip = true;
            tooltip.hide();
            $location.path('/addevent');
        });

        $scope.noliveeventslisted = $scope.nodrafteventslisted = $scope.nopasteventslisted = true;
        $scope.liveeventsshow = true;
        $scope.drafteventsshow = $scope.pasteventsshow = false;
        $scope.NumofLive = $scope.NumofDraft = $scope.NumofPast = 0;
        $scope.order_list_count = 0, $scope.cancel_order_list_count = 0;
        $localStorage.eventlocalSingledata = "";

        $scope.actionlist = ["Edit Order", "Edit participant", "Resend receipt"];
        $scope.showedit = false;
        $scope.editIndex = "";
        $scope.editSingle = $scope.parfirstname = $scope.parlastname = "";

        $scope.showquantityedit = false;
        $scope.editquantityIndex = "";
        $scope.qtyvalue = "";
        $scope.cancellation_button_text = "Process Cancellation";
        $scope.showChangesinCancellation = false;
        $scope.cancel_type = '';
        $scope.update_Payment_plan_status = 'N';
        $scope.updatedPaymentArray = [];
        $scope.updatedEventArray = [];

        $scope.maineventsview = true;
        $scope.childeventsview = false;
        $scope.eventstudentview = false;
        $scope.recentorders = false;
        $scope.cancelledorders = false;
        $scope.addparticipantview = false;
        $scope.eventtitlestud = false;
        $scope.eventtitleaddpart = false;
        $scope.eventsubtitleaddpart = false;
        $scope.current_event_manage = '';
        $scope.parrent_event_manage = '';
        $scope.manage_event_type = '';
        $scope.processing_fee_show = '';
        $scope.current_eventid_manage = '';
        $scope.current_event_reg_columns = '';
        $scope.noeventslisted = false;
        $scope.childeventarray = [];
        $scope.childeventarray_copy = [];
        $scope.studentlist = [];
        $scope.waiverarea = '';

        //payment related show and hide
        $scope.showpaymentview = true;
        $scope.zerocostview = true;
        $scope.showcardselection = false;
        $scope.compareDiscountCost = true;
        $scope.selectedcardnumber = '';
        $scope.student_name = '';
        $scope.student_id = 0;
        $scope.student_name2 = '';
        $scope.participant_studentname = '';
        $scope.participant_studentname2 = '';
        $scope.student_id2 = 0;
        $scope.credit_card_id = '';
        $scope.credit_card_status = '';
        $scope.credit_card_name = '';
        $scope.credit_card_expiration_month = '';
        $scope.credit_card_expiration_year = '';
        $scope.postal_code = '';
        $scope.event_parent_id = '';
        $scope.remainingDueValue = 0;
        $scope.remDueFeeText = '';
        $scope.showRemDueProccessingFee = false;
        $scope.manual_credit_show = false;
        $scope.manual_credit_refund = 0;
        $scope.current_manual_credit_details = [];
        $scope.new_manual_credit_details = [];

        //For multiple event purchase
        $scope.childlistcontents = [];
        $scope.order_event_once = 'N';
        $scope.payment_times = 'O';
        $scope.recurring_payment_show = false;
        $scope.childquantity = $scope.costquantity = $scope.zeroquantity = $scope.single_event_quantity = 1;
        $scope.register_event_title = $scope.register_event_id = '';
        $scope.register_discount_value = $scope.register_event_cost = 0;
        $scope.discount_show = $scope.childaddedvalidate = true;

        $scope.studentselected = '';
        $scope.message_type = '';
        $scope.buttonText = '';
        $scope.eventtime = '';
        $scope.data = {
            text: ""
        };
        $scope.pushdata = '';
        $scope.ccyearlist = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
        $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
        $scope.onetimecheckout = 0;
        $scope.onetimeinitialload = 0;
        $scope.discount = '';
        $scope.membershiplistcontents = [];
        $scope.selectedData = [];
        $scope.selected = {};
        $scope.selectAll = false;
        $scope.all_select_flag = 'N';
        $scope.color_green = 'N';
        $scope.upgrade_status = $localStorage.upgrade_status; // for inactive studio-registration block
        $scope.eventfromliveordraft = $scope.eventfromviewforback = '';
        $scope.stripe_card = '';
        $scope.stripe = "";
        
        if ($localStorage.wepay_level === 'P') {
            $scope.wepay_env = "production";
            $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
        } else if ($localStorage.wepay_level === 'S') {
            $scope.wepay_env = "stage";
            $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
        }

        WePay.set_endpoint($scope.wepay_env); // stage or production      

        // Shortcuts
        var d = document;
        d.id = d.getElementById,
                valueById = function (id) {
                    return d.id(id).value;
                };

        // For those not using DOM libraries
        var addEvent = function (e, v, f) {
            if (!!window.attachEvent) {
                e.attachEvent('on' + v, f);
            } else {
                e.addEventListener(v, f, false);
            }
        };

        // Attach the event to the DOM
        addEvent(d.id('cc-submit'), 'click', function () {
            $('#progress-full').show();
            var userName = [valueById('firstName')] + ' ' + [valueById('lastName')];
            var user_first_name = valueById('firstName');
            var user_last_name = valueById('lastName');
            var email = valueById('email').trim();
            var phone = valueById('phone');
            var postal_code = valueById('postal_code');
            var country = valueById('country');
            response = WePay.credit_card.create({
                "client_id": $localStorage.wepay_client_id,
                "user_name": userName,
                "email": valueById('email').trim(),
                "cc_number": valueById('cc-number'),
                "cvv": valueById('cc-cvv'),
                "expiration_month": valueById('cc-month'),
                "expiration_year": valueById('cc-year'),
                "virtual_terminal": "web",
                "address": {
                    "country": valueById('country'),
                    "postal_code": valueById('postal_code')
                }
            }, function (data) {
                if (data.error) {
                    $('#progress-full').hide();
                    console.log(data);
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Wepay Message');
                    $("#pushmessagecontent").text(data.error_description);
                    // handle error response
                } else {
                    if ($scope.onetimecheckout == 0) {
                        $scope.onetimecheckout = 1;
                        $scope.checkout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                    }
                    // call your own app's API to save the token inside the data;
                    // show a success page
                }
            });
        });
        
        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;

            // Create an instance of Elements
            var elements = $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });            
        };
        
        $scope.submitStripeForm = function(){
            $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error. 
                    var errorElement = document.getElementById('stripe-card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send paymentMethod.id to server 
                    (result.paymentMethod.id)? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                    $scope.preloader = true;
                    $scope.stripeEventCheckout($scope.firstname, $scope.lastname, $scope.email, $scope.phone, '','');
                }
            });
        };

        //value copied on clipboard message
        $scope.toastmsg = function (type) {
            var id_name = "";
            if (type === 'allevents') {
                id_name = "snackbar";
            } else if (type === 'particularevent') {
                id_name = "snackbar_particularevent";
            }
            var x = document.getElementById(id_name);
            x.className = "show";
            setTimeout(function () {
                x.className = x.className.replace("show", "");
            }, 3000);
        };

        $scope.handleeventpaste = function (e) {
            var pasted_value = e.originalEvent.clipboardData.getData('text/plain');
            var pasted_text_length = pasted_value.length;
            if (pasted_text_length > 100) {
                var x = document.getElementById("snackbarpaste")
                x.className = "showpaste";
                setTimeout(function () {
                    x.className = x.className.replace("showpaste", "");
                }, 3000);
            }
        };



        $scope.getparticipants_list = function (cmp_id, e_id, tab) {
            $scope.current_tab = tab;
//                var vm = this;
            $scope.evet_id = e_id;
            DTDefaultOptions.setLoadingTemplate('<div class="spinner-loader-full"></div>');
            var titleHtml = '<input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)">';
            var phone = '<span class="card-icon eventactionlinks"><i class="fa fa-mobile-phone fa-3x green"></i></span>';
            $scope.selected = {};
            $scope.selectAll = false;

            if ($scope.current_tab === 'O') {
                $scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            method: 'POST',
                            url: urlservice.url + 'geteventparticipants',
                            xhrFields: {
                                withCredentials: true
                            },
                            dataSrc: function (json) {
                                $scope.membershiplistcontents = json.data;
                                $scope.order = json.order;
                                $scope.cancel = json.cancel;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                "company_id": cmp_id,
                                "event_id": e_id,
                                "event_status_tab": tab,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                } else if (xhr.responseJSON.status === 'Expired') {
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                } else {
                                    $('#progress-full').hide();
                                }
                            },

                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                        .withOption('scrollCollapse', true)
//                        .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {
                            $('#progress-full').show();
                        })
                        .withOption('fnDrawCallback', function () {
                            $('#progress-full').hide();
                        })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave', true)
                        .withOption('stateDuration', 60 * 5)
                        .withOption('order', [9, 'asc'])
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                            .renderWith(function (data, type, full, meta) {
                                if ($scope.selectAll == true) {
                                    $scope.selected[full.id] = true;
                                } else {
                                    $scope.selected[full.id] = false;
                                    for (var id in  $scope.selected) {
                                        if ($scope.selectedData.length > 0) {
                                            for (var x = 0; x < $scope.selectedData.length; x++) {
                                                if (id === $scope.selectedData[x].id) {
                                                    $scope.selected[id] = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                            }),
                    DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                            function (data, type, full) {
                                if (data === 'Y')
                                    return  ' <a ng-click="sendindividualpushmail(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                else
                                    return "";
                            }).notSortable(),
                    DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_e1').renderWith(
                            function (data, type, full) {
                                return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="eventDetail(\'' + full.id + '\' ,' + "'order'" + ',' + full.event_id + ');"> ' + full.buyer_name + '</a>';
                            }),
                    DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_e2').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                            }),
                    DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_e3').renderWith(
                            function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="sendindividualpushmail(\'' + full.id + '\' ,' + "'mail'" + ');"> ' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                }
                            }),
                    DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_e4'),

                    DTColumnBuilder.newColumn('quantity').withTitle('Quantity').withClass('col_e5'),
                    DTColumnBuilder.newColumn('paid_amount').withTitle('Paid').withClass('col_e6'),
                    DTColumnBuilder.newColumn('balance_due').withTitle('Balance Due').withClass('col_e7').renderWith(function (data, type, full) {
                        return  $scope.wp_currency_symbol + '' + data;
                    }),
                    DTColumnBuilder.newColumn('registration_date').withTitle('Registration Date').withClass('col_e8'),
                    DTColumnBuilder.newColumn('event_reg_type_user').withTitle('Registration Method').withClass('col_e9'),
                ];
//                   $scope.dtInstance = {};
            } else if ($scope.current_tab === 'C') {
                $scope.dtOptions1 = DTOptionsBuilder.newOptions()
                        .withOption('ajax', {
                            method: 'POST',
                            url: urlservice.url + 'geteventparticipants',
                            xhrFields: {
                                withCredentials: true
                            },
                            dataSrc: function (json) {
                                $scope.membershiplistcontents = json.data;
                                $scope.order = json.order;
                                $scope.cancel = json.cancel;
                                $scope.$apply();
                                return json.data;
                            },
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                "company_id": cmp_id,
                                "event_id": e_id,
                                "event_status_tab": tab,
                            },
                            error: function (xhr, error, thrown) {
                                if (xhr.responseJSON.status === "Failed") {
                                    $('#progress-full').hide();
                                    $("#pushmessageModal").modal('show');
                                    $("#pushmessagetitle").text('Message');
                                    $("#pushmessagecontent").text(xhr.responseJSON.msg);
                                } else if (xhr.responseJSON.status === 'Expired') {
                                    $('#progress-full').hide();
                                    $("#messageModal").modal('show');
                                    $("#messagetitle").text('Message');
                                    $("#messagecontent").text(xhr.responseJSON.msg);
                                    $scope.logout();
                                }else if (xhr.responseJSON.status === 'Version') {
                                    $('#progress-full').hide();
                                    $scope.handleFailure(xhr.responseJSON.msg);
                                } else {
                                    $('#progress-full').hide();
                                }
                            },
                        })

                        .withOption('createdRow', function (row, data, dataIndex) {
                            // Recompiling so we can bind Angular directive to the DT
                            $compile(angular.element(row).contents())($scope);
                        })
                        .withOption('headerCallback', function (header) {
                            // Use this headerCompiled field to only compile header once
                            $scope.headerCompiled = true;
                            $compile(angular.element(header).contents())($scope);
                        })

                        .withDataProp('data')
                        .withDOM('lfrti')
                        .withScroller()
                        .withOption('deferRender', true)
                        .withOption('scrollY', 350)
                        .withOption('scrollX', '100%')
//                            .withOption('scrollCollapse', true)
//                            .withOption('processing', true)
                        .withOption('fnPreDrawCallback', function () {
                            $('#progress-full').show();
                        })
                        .withOption('fnDrawCallback', function () {
                            $('#progress-full').hide();
                        })
                        .withOption('serverSide', true)
                        .withOption('search', true)
                        .withOption('info', false)
                        .withOption('stateSave', true)
                        .withOption('stateDuration', 60 * 5)
                        .withOption('order', [9, 'desc'])

                $scope.dtColumns1 = [
                    DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
                            .renderWith(function (data, type, full, meta) {
                                if ($scope.selectAll == true) {
                                    $scope.selected[full.id] = true;
                                } else {
                                    $scope.selected[full.id] = false;
                                    for (var id in  $scope.selected) {
                                        if ($scope.selectedData.length > 0) {
                                            for (var x = 0; x < $scope.selectedData.length; x++) {
                                                if (id === $scope.selectedData[x].id) {
                                                    $scope.selected[id] = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                return '<input type="checkbox" ng-model="selected[' + data.id + ']" ng-click="toggleOne(selected)"/>';
                            }),
                    DTColumnBuilder.newColumn('show_icon').withTitle('').withClass('col15').renderWith(
                            function (data, type, full) {
                                if (data === 'Y')
                                    return  ' <a ng-click="sendindividualpushmail(\'' + full.id + '\' ,' + "'push'" + ');"> ' + phone + '</a>';
                                else
                                    return "";
                            }).notSortable(),
                    DTColumnBuilder.newColumn('buyer_name').withTitle('Buyer').withClass('col_e_c1').renderWith(
                            function (data, type, full) {
                                return ' <a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="eventDetail(\'' + full.id + '\' ,' + "'cancel'" + ',' + full.event_id + ');"> ' + full.buyer_name + '</a>';
                            }),
                    DTColumnBuilder.newColumn('participant_name').withTitle('Participant').withClass('col_e_c2').renderWith(
                            function (data, type, full) {
                                return '<span  data-toggle="tooltip" title="' + data + '" >' + full.participant_name + '</span>';
                            }),
                    DTColumnBuilder.newColumn('participant_email').withTitle('Email').withClass('col_e_c3').renderWith(
                            function (data, type, full, meta) {
                                if (full.bounce_flag === 'N') {
                                    if (full.email_subscription_status === 'U') {
                                        return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"  ng-click="sendSubscribeEmail(\'' + full.participant_email + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(Unsubscribed) " + full.participant_email + '</a>';
                                    } else {
                                        return '<a class="buyername" data-toggle="tooltip" title="' + data + '"  ng-click="sendindividualpushmail(\'' + full.id + '\' ,' + "'mail'" + ');"> ' + full.participant_email + '</a>';
                                    }
                                } else {
                                    return '<a class="buyername1" data-toggle="tooltip" title="' + data + '"ng-click="showemailerror(\'' + meta.row + '\',\'' + full.bounce_flag + '\',\'' + data + '\',' + '$event' + ',\'' + full.id + '\');"><img src="image/mail_error.png"  width="20" height="20">' + "(" + full.type + ") " + full.participant_email + '</a>';
                                }
                            }),

                    DTColumnBuilder.newColumn('participant_phone').withTitle('Phone').withClass('col_e_c4'),
                    DTColumnBuilder.newColumn('quantity').withTitle('Quantity').withClass('col_e_c5'),
                    DTColumnBuilder.newColumn('registration_date').withTitle('Registration Date').withClass('col_e_c6'),
                    DTColumnBuilder.newColumn('event_reg_type_user').withTitle('Registration Method').withClass('col_e_c7'),
                    DTColumnBuilder.newColumn('last_updt_dt').withTitle('Cancellation Date').withClass('col_e_c8'),
                ];
                $scope.dtInstance1 = {};
            }
            $('#progress-full').hide();
            angular.element('body').on('search.dt', function () {
                $scope.searchTerm = document.querySelector('.dataTables_filter input').value;

            })
            $scope.currentEventPage = false;
        }

        $scope.showemailerror = function (row, type, data, event, id) {
            $scope.email_updt = event.target;
            $scope.row = row;
            $scope.mem_id = id;
            $scope.btype = type;
            $scope.errormsg = $scope.membershiplistcontents[row].error;
            $scope.bouncedemail = data;
            $("#emailerrormessageModal").modal('show');
            $("#emailerrormessagetitle").text('Message');
            $("#emailerrormessagecontent").text($scope.errormsg);
        };

        $scope.unblockbounceemail = function () {
            $('#progress-full').show();
            $("#emailerrormessageModal").modal('hide');
            $http({
                method: 'POST',
                url: urlservice.url + 'unblockbounceemail',
                data: {
                    "bounced_email": $scope.bouncedemail,
                    "company_id": $localStorage.company_id
                },
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    angular.element($scope.email_updt).parent().html('<a class="buyername" data-toggle="tooltip" title="' + $scope.bouncedemail + '" ng-click="sendindividualpushmail(\'' + $scope.mem_id + '\' ,' + "'mail'" + ');">' + $scope.bouncedemail + '</a>');
                    $compile(angular.element(document.getElementById('DataTables_Table_e1')).contents())($scope);
                    $compile(angular.element(document.getElementById('DataTables_Table_e2')).contents())($scope);
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text('Student email unblocked successfully ');
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
            $('#progress-full').hide();
        };

        $scope.toggleAll = function (selectAll, selectedItems) {
            $scope.selectedData = [];
            $scope.color_green = 'N';
            $scope.all_select_flag = 'N';
            for (var id in selectedItems) {
                if (selectedItems.hasOwnProperty(id)) {
                    selectedItems[id] = selectAll;
                    if (selectAll !== false) {
                        $scope.all_select_flag = 'Y';
                        $scope.color_green = 'Y';
                    }
                }
            }
            $scope.maintainselected = selectedItems;
        };

        $scope.toggleOne = function (selectedItems) {
            var check = 0;
            $scope.all_select_flag = 'N';
            $scope.color_green = 'N';
            $scope.maintainselected = selectedItems;
            $scope.selectedData = [];
            for (var id in selectedItems) {
                if (selectedItems.hasOwnProperty(id)) {
                    if (!selectedItems[id]) {
                        check = 1;
                    } else {
                        $scope.selectedData.push({"id": id});
                        $scope.color_green = 'Y';
                    }

                }
            }
            if (check === 1) {
                $scope.selectAll = false;
            } else {
                $scope.selectAll = true;
            }
        }

        //Event list link info message
        $scope.addeventinfo = function (addeventinfo) {
            var msg;
            if (addeventinfo === 'publicurl') {
                msg = "<p>This URL is a web link to your event page, allowing users without the app to browse and register for any of your event.<p><p>Give this link to your web developer to link it your website. Use this link to promote your events on your Facebook page and Google ads.</p>";
            }
            $("#failuremessageModal").modal('show');
            $("#failuremessagetitle").text('Info');
            $("#failuremessagecontent").html(msg);
        };

        //recurring paid towards the event cost info
        $scope.paidinfo = function () {
            var msg = "This amount only shows what the user has paid toward the cost of the event.<br> It does not show any applicable passed on fees.";
            $("#pushmessageModal").modal('show');
            $("#pushmessagetitle").text('Info');
            $("#pushmessagecontent").html(msg);
        };

        //Live Event Listing
        $scope.livelistview = function () {
            $localStorage.currentpage = "events";
            $scope.liveeventsshow = true;
            $scope.drafteventsshow = false;
            $scope.pasteventsshow = false;
            $scope.childeventsview = false;
            $scope.resettabbarclass();
            $('#li-live').addClass('active-event-list').removeClass('event-list-title');
            $('.eventtab-text-1').addClass('greentab-text-active');
            $scope.window_bottom_hit_page = 'P';
            $scope.clearListArr();
            $scope.geteventdetails('P','');
        };

        //Draft Event Listing
        $scope.draftlistview = function () {
            $scope.liveeventsshow = false;
            $scope.drafteventsshow = true;
            $scope.pasteventsshow = false;
            $scope.childeventsview = false;
            $scope.resettabbarclass();
            $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
            $('.eventtab-text-2').addClass('greentab-text-active');
            $localStorage.redirectedfrmtemplate = false;
            $scope.window_bottom_hit_page = 'S';
            $scope.clearListArr();
            $scope.geteventdetails('S','');
        };

        //Past Event Listing
        $scope.pastlistview = function () {
            $scope.liveeventsshow = false;
            $scope.drafteventsshow = false;
            $scope.pasteventsshow = true;
            $scope.childeventsview = false;
            $scope.resettabbarclass();
            $('#li-past').addClass('active-event-list').removeClass('event-list-title');
            $('.eventtab-text-3').addClass('greentab-text-active');
            $scope.window_bottom_hit_page = 'D';
            $scope.clearListArr();
            $scope.geteventdetails('D','');
        };
        $scope.clearListArr = function () {
            $scope.window_bottom_hit_limit = 0;
            $scope.currentEventPage = true;
            $scope.bottomhitstopcall = false;
            $scope.manage_parent_title = '';
            $scope.eventfromliveordraft = '';
            $scope.eventfromviewforback = '';
            $localStorage.eventredirectfromaddevent = '';
            $scope.liveEventList = [];
            $scope.pastEventList = [];
            $scope.draftEventList = [];
        };

        //set active tab using highlighting css class
        $scope.resettabbarclass = function () {
            $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
            $('.greentab-text').removeClass('greentab-text-active');
        };

        //get student list of this company
        $scope.getStudentList = function () {
//                $('#progress-full').show();
            $http({
                method: 'GET',
                url: urlservice.url + 'getstudent',
                params: {
                    "company_id": $localStorage.company_id,
                    "for": "event"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $scope.studentlist = [];
                    $('#progress-full').hide();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        $scope.getStudentList();

        //get event details of live, draft and past
        $scope.geteventdetails = function (list_type,parentid) {
            if($scope.bottomhitstopcall || $rootScope.activepagehighlight !== 'events'){
                    return false;
            }
            $('#progress-full').show();
            $http({
                method: 'GET',
                url: urlservice.url + 'geteventdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "list_type": list_type,
                    "limit": $scope.window_bottom_hit_limit,
                    "parent_id": parentid,
                    "from":'W' //Web
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $localStorage.eventredirectfromaddevent = '';
                    $scope.NumofLive = response.data.count.P;
                    $scope.NumofDraft = response.data.count.S;
                    $scope.NumofPast = response.data.count.D;
                    if(parentid){
                        $scope.childeventarray = response.data.msg;
                        $scope.childeventarray_copy = angular.copy($scope.childeventarray);
                        $localStorage.currentChildArray = $scope.childeventarray_copy;
                    }else{
                        $scope.childeventarray = $scope.childeventarray_copy = $localStorage.currentChildArray =  [];
                        $scope.Eventregurl = response.data.event_list_url;
                        $scope.eventStatus = response.data.event_status.event_enabled;
                        $localStorage.eventenabledstatus = $scope.eventStatus;
                        if (parseInt($scope.NumofLive) > 0) {
                            $scope.noliveeventslisted = false;
                        } else {
                            $scope.noliveeventslisted = true;
                        }
                        if (parseInt($scope.NumofDraft) > 0) {
                            $scope.nodrafteventslisted = false;
                        } else {
                            $scope.nodrafteventslisted = true;
                        }
                        if (parseInt($scope.NumofPast) > 0) {
                            $scope.nopasteventslisted = false;
                        } else {
                            $scope.nopasteventslisted = true;
                        }

                        if (list_type === 'P') {
                            for (var i = 0; i < response.data.msg.live.length; i++) {
                                $scope.liveEventList.push(response.data.msg.live[i]);
                            }
                            $scope.window_bottom_hit_page = 'P';
                            $scope.pastEventList = [];
                            $scope.draftEventList = [];
                        } else if (list_type === 'S') {
                            for (var i = 0; i < response.data.msg.draft.length; i++) {
                                $scope.draftEventList.push(response.data.msg.draft[i]);
                            }
                            $scope.liveEventList = [];
                            $scope.pastEventList = [];
                            $scope.window_bottom_hit_page = 'S';
                        } else if (list_type === 'D') {
                            for (var i = 0; i < response.data.msg.past.length; i++) {
                                $scope.pastEventList.push(response.data.msg.past[i]);
                            }
                            $scope.liveEventList = [];
                            $scope.draftEventList = [];
                            $scope.window_bottom_hit_page = 'D';
                        } 
                        $scope.currentEventPage = true;
                    }
                }else if (response.data.status === 'Failed') {
                    $('#progress-full').hide();
                    if (response.data.msg == "Event details doesn't exist.") {
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.NumofPast = response.data.count.D;
                        $scope.eventStatus = response.data.event_status.event_enabled;
                        $localStorage.eventenabledstatus = $scope.eventStatus;
                        $scope.bottomhitstopcall = true;
                    }
                }else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    if(response.data.count && response.data.count.length > 0){
                        $scope.NumofLive = response.data.count.P;
                        $scope.NumofDraft = response.data.count.S;
                        $scope.NumofPast = response.data.count.D;
                        $('#progress-full').hide();
                    }
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };
        
        //converting server date-time format value into string on event listing screen
        $scope.datetimestring = function (ddate) {
            var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                    navigator.userAgent && !navigator.userAgent.match('CriOS');
            if (isSafari)
            {
                if (ddate !== '0000-00-00 00:00:00') {
                    var safaritimecheck = ddate.split(" ");
                    if (safaritimecheck[1] === "00:00:00") {
                        var dda = ddate.toString();
                        dda = dda.replace(/ /g, "T");
                        var time = new Date(dda);
                        var dateonly = time.toString();
                        var fulldateonly = dateonly.replace(/GMT.*/g, "");
                        var localdate = fulldateonly.slice(0, -9);
                        return localdate;
                    } else {
                        var dda = ddate.toString();
                        dda = dda.replace(/ /g, "T");
                        var date = new Date(dda).toUTCString();   // For date in safari format
                        var datealone = date.slice(0, 12);
                        var time = date.slice(17, 25);
                        // format AM PM start
                        var hours = time.slice(0, 2);
                        var minutes = time.slice(3, 5);
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        var localTime = hours + ':' + minutes + ' ' + ampm;
                        var localdatetime = datealone + "  " + localTime;
                        return localdatetime;
                    }
                } else {
                    var localdatetime = "";
                    return localdatetime;
                }
            } else
            {
                if (ddate !== '0000-00-00 00:00:00') {
                    var timecheck = ddate.split(" ");
                    if (timecheck[1] === "00:00:00") {
                        var ddateonly = timecheck[0] + 'T00:00:00+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        var localdate = $filter('date')(newDate, 'EEE, MMM d');
                        return localdate;
                    } else {
                        var ddateonly = timecheck[0] + 'T' + timecheck[1] + '+00:00';
                        var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                        var localdatetime = $filter('date')(newDate, 'EEE, MMM d h:mm a');
                        return localdatetime;
                    }
                } else {
                    var localdatetime = "";
                    return localdatetime;
                }
            }
        };

        //converting date string into server format for recurring payment date
        $scope.getPaymentstartdate = function (dat) {
            var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                    navigator.userAgent && !navigator.userAgent.match('CriOS');
            if (isSafari)
            {
                var dda = dat.toString();
                dda = dda.replace(/ /g, "T");
                var time = new Date(dda);
                var dateonly = time.toString();
                var fulldateonly = dateonly.replace(/GMT.*/g, "");
                var localdate = fulldateonly.slice(0, -9);
                return localdate;
            } else {
                var ddateonly = dat + 'T00:00:00+00:00';
                var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                var localdate = $filter('date')(newDate, 'MMMM dd, y');
                return localdate;
            }
        };

        //For screen looks on mobile view
        $scope.topreview = function () {
            document.getElementById('previewapp').scrollIntoView();
        };

        //Edit action for single/multiple event
        $scope.editevent = function (e, from) {
            $localStorage.eventpagetype = 'edit';
            $localStorage.isAddedEventDetails = 'N';
            $localStorage.eventredirectfromaddevent = from;
            $scope.getsingleEventDetails(e.event_type,from,e.event_id,'edit');
            $scope.childeventsview = false;
            $scope.window_bottom_hit_limit = 0;
            $scope.bottomhitstopcall = false;
        };
        $scope.getsingleEventDetails = function(type,event_status,eventid,from){
            $('#progress-full').show();
            $http({
                method: 'GET',
                url: urlservice.url + 'getsingleeventdetails',
                params: {
                    "company_id": $localStorage.company_id,
                    "event_type": type,
                    "list_type": event_status,
                    "event_id": eventid
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    },
                    withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide(); 
                    $scope.currentEventPage = false;
                    if(from === 'edit'){
                        eventListDetails.addSingleEvent(response.data.msg);
                        $location.path('/addevent'); 
                    }else if(from === 'addparticipant'){
                        $scope.current_event_manage = response.data.msg;
                    }
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $('#progress-full').hide();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //copy action for single/multiple event
        $scope.copyevent = function (evnt_detail) {
            $('#progress-full').show();

            $http({
                method: 'POST',
                url: urlservice.url + 'copyevents',
                data: {
                    'event_id': evnt_detail.event_id,
                    'list_type': evnt_detail.list_type,
                    'copy_event_type': evnt_detail.event_type === "C" ? 'Child' : 'Main',
                    'event_template_flag': 'N',
                    'company_id': $localStorage.company_id
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#msgModal").modal('show');
                    $("#msgtitle").text('Message');
                    if (evnt_detail.event_type === "C") {
                        $scope.childeventarray = response.data.msg;
                        $scope.childeventarray_copy = angular.copy($scope.childeventarray);
                        $localStorage.currentChildArray = $scope.childeventarray_copy;
                        if(evnt_detail.list_type === 'past')
                            $("#msgcontent").text('Event successfully copied inside live');
                        else 
                            $("#msgcontent").text('Event successfully copied');
                    } else {
                        $scope.clearListArr();
                        for (var i = 0; i < response.data.msg.draft.length; i++) {
                            $scope.draftEventList.push(response.data.msg.draft[i]);
                        }
                        if (response.data.msg.draft.length == 0) {
                            $scope.bottomhitstopcall = true;
                        }
                        $scope.liveeventsshow = false;
                        $scope.drafteventsshow = true;
                        $scope.pasteventsshow = false;
                        $scope.childeventsview = false;
                        $scope.resettabbarclass();
                        $('#li-draft').addClass('active-event-list').removeClass('event-list-title');
                        $('.eventtab-text-2').addClass('greentab-text-active');
                        $localStorage.redirectedfrmtemplate = false;
                        $scope.window_bottom_hit_page = 'S';
                        $("#msgcontent").text('Event successfully copied inside "Draft" folder');
                    }
                    $scope.NumofLive = response.data.count.P;
                    $scope.NumofDraft = response.data.count.S;
                    $scope.NumofPast = response.data.count.D;
                    $scope.Eventregurl = response.data.event_list_url;
                    $scope.currentEventPage = true;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $scope.NumofLive = response.data.count.P;
                    $scope.NumofDraft = response.data.count.S;
                    $scope.NumofPast = response.data.count.D;
                    $('#progress-full').hide();
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //onetime payment selection for single/multiple event
        $scope.onetimepayment = function () {
            $scope.payment_times = 'O';
            if ($scope.manage_event_type === 'C') {
                $scope.final_payment_type = 'onetimemultiple';
                $scope.payment_event_type = 'M';
            } else if ($scope.manage_event_type === 'S') {
                $scope.final_payment_type = 'onetimesingle';
                $scope.payment_event_type = 'S';
            }
        };

        //recurring payment selection for single/multiple event
        $scope.recurringpayment = function () {
            $scope.payment_times = 'R';
            $scope.paymentOptions();
            if ($scope.manage_event_type === 'C') {
                $scope.final_payment_type = 'multiplerecurring';
                $scope.payment_event_type = 'M';
            } else if ($scope.manage_event_type === 'S') {
                $scope.final_payment_type = 'singlerecurring';
                $scope.payment_event_type = 'S';
            }
        };

        //single event quantity change
        $scope.singlequantityChange = function (quantity_type) {
            $scope.single_event_quantity = 1;
            $scope.validatesinglecapacity = false;
            if (quantity_type === '1' || quantity_type === 1) {
                $scope.single_event_quantity = this.costquantity;
            } else if (quantity_type === '0' || quantity_type === 0) {
                $scope.single_event_quantity = this.zeroquantity;
            }

            if (parseFloat($scope.current_event_manage.event_capacity) > 0) {
                if ($scope.single_event_quantity > 0 && parseInt($scope.single_event_quantity) <= (parseInt($scope.current_event_manage.event_capacity) - parseInt($scope.current_event_manage.registrations_count))) {
                    $scope.validatesinglecapacity = false;
                    $scope.showTotal();
                    $scope.paymentOptions();
                } else {
                    $scope.validatesinglecapacity = true;
                }
            } else {
                $scope.validatesinglecapacity = false;
                $scope.showTotal();
                $scope.paymentOptions();
            }

        };

        //multiple event quantity change
        $scope.childquantityTotal = function (event_id, ind) {
            this.validatecapacityenable = this.validatewithoutcapacityenable = false;
            for (var j = 0; j < $scope.childlistcontents.length; j++) {
                if ($scope.childlistcontents[j]['event_id'] === event_id) {
                    if (this.childquantity !== null && this.childquantity !== undefined && this.childquantity !== '') {
                        $scope.childlistcontents[j]['event_quantity'] = this.childquantity;
                        $scope.childlistcontents[j]['total_event_cost'] = +$scope.childlistcontents[j]['event_cost'] * +this.childquantity;
                    } else {
                        $scope.childlistcontents[j]['event_quantity'] = 0;
                        $scope.childlistcontents[j]['total_event_cost'] = +$scope.childlistcontents[j]['event_cost'] * 0;
                    }
                }
            }

            for (var j = 0; j < $scope.childeventarray.length; j++) {
                if ($scope.childeventarray[j]['event_id'] === event_id) {
                    if (parseInt($scope.childeventarray[j]['event_capacity']) > 0) {
                        var balance_event_capacity = (parseInt($scope.childeventarray[j]['event_capacity']) - parseInt($scope.childeventarray[j]['registrations_count']));
                        if (parseInt(this.childquantity) <= balance_event_capacity && (balance_event_capacity > 0) && this.childquantity > 0) {
                            this.validatecapacityenable = false;
                            $scope.paymentform.$setValidity("paymentform", true);
                            $(".disablequantityerror").attr("disabled", false);
                            $(".mulcheckbox").attr("disabled", false);
                            $scope.showTotal();
                            $scope.paymentOptions();
                        } else {
                            $scope.paymentform.$setValidity("paymentform", false);
                            $(".disablequantityerror").attr("disabled", true);
                            $(".mulcheckbox").attr("disabled", true);
                            var quantitybox = "quantity_box" + ind;
                            $('#' + quantitybox).attr("disabled", false);
                            this.validatecapacityenable = true;
                        }

                    } else {
                        this.validatecapacityenable = false;
                        if (parseInt(this.childquantity) <= 0) {
                            this.validatewithoutcapacityenable = true;
                            $scope.paymentform.$setValidity("paymentform", false);
                        } else {
                            $scope.paymentform.$setValidity("paymentform", true);
                        }
                        $(".mulcheckbox").attr("disabled", false);
                        $scope.showTotal();
                        $scope.paymentOptions();
                    }
                }
            }

            for (var j = 0; j < $scope.childeventarray.length; j++) {
                if (parseInt($scope.childeventarray[j]['event_capacity']) > 0) {
                    var balance_event_capacity = (parseInt($scope.childeventarray[j]['event_capacity']) - parseInt($scope.childeventarray[j]['registrations_count']));
                    if (parseInt(balance_event_capacity) <= 0) {
                        var checkbox = "multiplecheckbox" + j;
                        $('#' + checkbox).attr("disabled", true);
                    } else {

                    }
                } else if (parseInt($scope.childeventarray[j]['event_capacity']) == 0) {
                    var checkbox = "multiplecheckbox" + j;
                    $('#' + checkbox).attr("disabled", true);
                }
            }
        };


        //Manage action for single/multiple events
        $scope.manageevent = function (lst, from, listview) {
            $('#progress-full').show();
//            Muthulakshmi
            $scope.currentEventPage = false;
            $scope.eventfromliveordraft = from;
            $scope.eventfromviewforback = listview;
            $scope.validatesinglecapacity = false;
            $scope.validatecapacityenable = false;
            $scope.validatewithoutcapacityenable = false;
            $scope.childlistcontents = [];
            $scope.pay = [];
            $scope.recurrent = {};
            $scope.childindex = '';
            $scope.company_id_tab = lst.company_id;
            $scope.event_id_tab = lst.event_id;
            
            if(from === 'child'){
                $scope.childlistcontents = [];
                $scope.eventstudentview = true;
                $scope.maineventsview = false;
                $scope.recentorders = true;
                $scope.cancelledorders = false;
                $scope.childeventsview = false;
                $scope.eventtitlestud = true;
                $scope.change_made = $scope.search_value = '';
                $scope.event_parent_id = lst.parent_id;
                $scope.register_child_event_id = lst.event_id;
                $scope.event_id_tab = lst.event_id;
                $scope.pushkeyvalue = 1;
                for (var j = 0; j < $scope.childeventarray.length; j++) {
                    if ($scope.childeventarray[j].event_id === lst.event_id) {
                        $scope.childindex = j;
                    }
                }

                if (($scope.childeventarray[$scope.childindex].event_capacity === null) || (parseFloat($scope.childeventarray[$scope.childindex].event_capacity) > 0 && (parseFloat($scope.childeventarray[$scope.childindex].event_capacity) - parseFloat($scope.childeventarray[$scope.childindex].registrations_count) > 0))) {
                    $scope.selectedeventlist($scope.childeventarray);
                } else {
                    // Event capacity as zero
                }
                $scope.recentorderstab();
                $scope.current_event_manage = lst;
                $scope.manage_event_type = lst.event_type;
                $scope.current_eventid_manage = lst.event_id;
                $scope.current_event_title = lst.event_title;
//                var myEl = angular.element(document.querySelector('#eventtitlestud'));
//                myEl.text(lst.event_title);
//                $('#progress-full').hide();
                $scope.getparticipants_list(lst.company_id, lst.event_id, 'O');
            }else if(lst.event_type === 'S'){
                $scope.eventstudentview = true;
                $scope.maineventsview = false;
                $scope.recentorders = true;
                $scope.cancelledorders = false;
                $scope.childeventsview = false;
                $scope.eventtitlestud = true;
                $scope.event_parent_id = '';
                $scope.change_made = $scope.search_value = '';
                $scope.register_event_title = lst.event_title;
                $scope.register_event_id = lst.event_id;
                $scope.event_id_tab = lst.event_id;
                $scope.childeventarray = [];
                $scope.childlistcontents = [];
                $scope.recentorderstab();
                //$scope.getparticipants(lst.company_id, lst.event_id,'O',0);
                $scope.current_event_manage = lst;
                $localStorage.currentManageEventDetails = lst;

                $scope.manage_event_type = lst.event_type;
                $scope.processing_fee_show = lst.processing_fees;
                $scope.current_eventid_manage = lst.event_id;
                $scope.current_event_title = lst.event_title;
                var myEl = angular.element(document.querySelector('#eventtitlestud'));
                myEl.text(lst.event_title);

                $scope.order_event_once = $scope.current_event_manage.event_onetime_payment_flag;
                $scope.recurring_payment_flag = $scope.current_event_manage.event_recurring_payment_flag;
                $scope.total_order_amount = $scope.current_event_manage.total_order_text;
                $scope.total_order_quantity = $scope.current_event_manage.total_quantity_text;
                $scope.total_deposit_amount = $scope.current_event_manage.deposit_amount;
                $scope.total_number_of_payments = $scope.current_event_manage.number_of_payments;
                $scope.total_payment_startdate_type = $scope.current_event_manage.payment_startdate_type;
                if ($scope.total_payment_startdate_type === 4 || $scope.total_payment_startdate_type === '4') { //single recurring payment details
                    $scope.total_payment_startdate = $scope.current_event_manage.payment_startdate;
                } else {
                    var curr_date = new Date();
                    var curr_server_date = curr_date.toISOString().split('T')[0];
                    var newDate = curr_server_date.split('-');
                    var given_start_year = newDate[0];
                    var given_start_month = newDate[1] - 1;
                    var given_start_day = newDate[2];
                    var dup_date = new Date(given_start_year, given_start_month, given_start_day);
                    if ($scope.total_payment_startdate_type === '1') {
                        dup_date.setMonth(dup_date.getMonth() + 1);
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else if ($scope.total_payment_startdate_type === '2') {
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    } else if ($scope.total_payment_startdate_type === '3') {
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;
                        $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                    }
                }
                $scope.total_payment_frequency = $scope.current_event_manage.payment_frequency;
                $scope.getparticipants_list(lst.company_id, lst.event_id, 'O');
            }else if (lst.event_type === 'M') { 
                $scope.bottomhitstopcall = false;
                $scope.maineventsview = true;
                $scope.liveeventsshow = false;
                $scope.drafteventsshow = false;
                $scope.pasteventsshow = false;
                $scope.addparticipantview = false;
                $scope.zerocostview = true;
                    $scope.childeventsview = true;
                    $scope.eventstudentview = false;
                    $scope.recentorders = false;
                    $scope.cancelledorders = false;
                    $scope.manage_parent_title = lst.event_title;
                   
                    $scope.childeventarray = $scope.childeventarray_copy = [];
                    $scope.parrent_event_manage = lst;
                    $scope.manage_event_type = lst.event_type;
                    $scope.processing_fee_show = lst.processing_fees;
//                    $scope.eventtitlestud = false;
                    $scope.event_parent_id = '';
                    $scope.register_event_title = lst.event_title;
                    $scope.register_event_id = lst.event_id;
//                    myEl.text(lst.event_title);
                    $localStorage.currentManageEventDetails = lst;

                    $scope.order_event_once = $scope.parrent_event_manage.event_onetime_payment_flag;
                    $scope.recurring_payment_flag = $scope.parrent_event_manage.event_recurring_payment_flag;
                    $scope.total_order_amount = $scope.parrent_event_manage.total_order_text;
                    $scope.total_order_quantity = $scope.parrent_event_manage.total_quantity_text;
                    $scope.total_deposit_amount = $scope.parrent_event_manage.deposit_amount;
                    $scope.total_number_of_payments = $scope.parrent_event_manage.number_of_payments;
                    $scope.total_payment_startdate_type = $scope.parrent_event_manage.payment_startdate_type;
                    if ($scope.total_payment_startdate_type === 4 || $scope.total_payment_startdate_type === '4') { //multiple recurring payment details
                        $scope.total_payment_startdate = $scope.parrent_event_manage.payment_startdate;
                    } else {
                        var curr_date = new Date();
                        var curr_server_date = curr_date.toISOString().split('T')[0];
                        var newDate = curr_server_date.split('-');
                        var given_start_year = newDate[0];
                        var given_start_month = newDate[1] - 1;
                        var given_start_day = newDate[2];
                        var dup_date = new Date(given_start_year, given_start_month, given_start_day);
                        if ($scope.total_payment_startdate_type === '1') {
                            dup_date.setMonth(dup_date.getMonth() + 1);
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else if ($scope.total_payment_startdate_type === '2') {
                            dup_date.setDate(dup_date.getDate() + 7);
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        } else if ($scope.total_payment_startdate_type === '3') {
                            dup_date.setDate(dup_date.getDate() + 14);
                            var dup = dup_date;
                            $scope.total_payment_startdate = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        }
                    }
                    $scope.total_payment_frequency = $scope.parrent_event_manage.payment_frequency;
                    $scope.geteventdetails((lst.list_type === 'live' ? 'P':(lst.list_type === 'draft' ? 'S':'D')),lst.event_id);
                    $('#progress-full').hide();
                }
//                $scope.maineventsview = true;
//                $scope.liveeventsshow = false;
//                $scope.drafteventsshow = false;
//                $scope.pasteventsshow = false;
//                $scope.addparticipantview = false;
//                $scope.zerocostview = true;
//                $scope.eventtitleaddpart = false;
//                $scope.eventsubtitleaddpart = false;
                //$scope.scrollByEventTableId(lst.company_id, lst.event_id, 'event_table_orders', 'O');
                $('#progress-full').hide();
            };

        //Back action from payment page
        $scope.closemanageview = function () {
            $scope.currentEventPage = true;
            $scope.eventstudentview = false;
            $scope.addparticipantview = false;
            if ($scope.eventfromliveordraft === 'S') {
                $scope.draftlistview();
            } else if ($scope.eventfromliveordraft === 'D') {
                $scope.pastlistview();
            }else if ($scope.eventfromliveordraft === 'child') {
                if ($scope.eventfromviewforback === 'draft') {
                    $scope.draftlistview();
                } else if ($scope.eventfromviewforback === 'past') {
                    $scope.pastlistview();
                }else{
                   $scope.livelistview(); 
                }
            }else {
                $scope.livelistview();
            }
//            if ($scope.manage_event_type === 'S' || $scope.manage_event_type === 'M' || currentpage === 'C') {
//                $route.reload();
//            } else if ($scope.manage_event_type === 'C') {
//                $scope.maineventsview = true;
//                $('#li-live').addClass('active-event-list').removeClass('event-list-title');
//                $scope.childeventsview = true;
//                $scope.eventstudentview = false;
//                $scope.eventtitlestud = false;
//                $scope.addparticipantview = false;
//                this.childeventarray = $scope.childeventarray_copy;
//            }
        };

        //Multiple selection of events on add participant payment screen - Multiple events
        $scope.selectedeventlist = function (choice) {
            $scope.childlistcontents = [];
            angular.forEach(choice, function (value, key) {
                if ($scope.pushkeyvalue === 1 || $scope.pushkeyvalue === '1') {
                    choice[$scope.childindex].checked = true;
                }
                if (choice[key].checked) {
                    var quantity_value;
                    var id = "#quantity_box" + key;
                    if (angular.element(id).val() === null || angular.element(id).val() === undefined || angular.element(id).val() === '') {
                        quantity_value = 1;
                    } else {
                        quantity_value = angular.element(id).val();
                        if ((parseInt(choice[key].event_capacity)) > 0) {
                            if ((parseInt(choice[key].event_capacity) - parseInt(choice[key].registrations_count)) <= 0) {
                                quantity_value = 0;
                                angular.element(document.querySelector(id)).val(0);
                            }
                        }
                    }
                    $scope.childlistcontents.push({event_id: choice[key].event_id, event_title: choice[key].event_title, event_cost: choice[key].event_cost, event_begin_dt: choice[key].event_begin_dt, event_quantity: quantity_value, total_event_cost: parseFloat(choice[key].event_cost) * parseInt(quantity_value), event_payment_amount: '0', event_discount_amount: '0'});
                    $scope.pushkeyvalue = 0;
                    $scope.childindex = '';
                }
            });

            $scope.showTotal();
            $scope.paymentOptions();
        };

        //Add participant form setup view before submit
        $scope.addParticipant = function (event_id) {
            $scope.currentEventPage = false;
            $scope.getsingleEventDetails('P','draft',event_id,'addparticipant');
            $scope.reg_col_value[0] = '';
            $scope.reg_col_value[1] = '';
            $scope.reg_col_value[2] = '';
            $scope.reg_col_value[3] = '';
            $scope.reg_col_value[4] = '';
            $scope.reg_col_value[5] = '';
            $scope.reg_col_value[6] = '';
            $scope.reg_col_value[7] = '';
            $scope.reg_col_value[8] = '';
            $scope.reg_col_value[9] = '';
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.country = '';
            $scope.studentselected = '';
            $scope.discount = '';
            $scope.zerocostview = true;
            $scope.showpaymentview = true;
            $scope.waiverchecked = false;
            $scope.stripeccchecked = false;
            $scope.validatesinglecapacity = false;
            $scope.validatecapacityenable = false;
            $scope.validatewithoutcapacityenable = false;
            $scope.childlistcontents = [];
            $scope.pay = [];
            $scope.recurrent = {};
            $scope.childindex = '';
            $scope.selectedcardtype = 'new';
            angular.forEach($scope.childeventarray, function (value, key) {
                $scope.childeventarray[key].checked = false;
            });
            $scope.paymentform.$setPristine();
            $scope.paymentform.$setUntouched();
            document.getElementById("paymentform").reset();
            if (parseInt($scope.current_event_manage.event_capacity) == 0 || (parseInt($scope.current_event_manage.event_capacity) > 0 && (parseInt($scope.current_event_manage.event_capacity) - parseInt($scope.current_event_manage.registrations_count)) <= 0)) {
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text('Event is sold out');
                return false;
            }

            $scope.maineventsview = false;
            $scope.childeventsview = false;
            $scope.eventstudentview = false;
            $scope.eventtitlestud = false;
            $scope.addparticipantview = true;
            if ($scope.current_event_manage.event_cost === undefined || $scope.current_event_manage.event_cost === '0' || $scope.current_event_manage.event_cost === '0.00') {
                $scope.wepay_text = "";
                $scope.zerocostview = false;
                $scope.showpaymentview = false;
                $scope.part_only_button = true;
                if ($scope.manage_event_type === 'S') {
                    $scope.current_event_reg_columns = $scope.current_event_manage.reg_columns;
                } else if ($scope.manage_event_type === 'C') {
                    $scope.current_event_reg_columns = $scope.parrent_event_manage.reg_columns;
                }
            }
            if ($scope.current_event_manage.event_cost !== undefined && $scope.current_event_manage.event_cost !== '0' && $scope.current_event_manage.event_cost !== '0.00') {
                $scope.zerocostview = true;
                if ($scope.manage_event_type === 'S') {
                    $scope.current_event_reg_columns = $scope.current_event_manage.reg_columns;
                } else if ($scope.manage_event_type === 'C') {
                    $scope.current_event_reg_columns = $scope.parrent_event_manage.reg_columns;
                }
            }

            if ($scope.manage_event_type === 'S') {
                if ($scope.current_event_manage.waiver_policies === null) {
                    $scope.waiverShow = false;
                    $scope.waiverarea = '';
                } else {
                    $scope.waiverShow = true;
                    $scope.waiverarea = $scope.current_event_manage.waiver_policies;
                }
                $scope.payment_event_type = 'S';
                $scope.final_payment_type = 'onetimesingle';
            } else if ($scope.manage_event_type === 'C') {
                if ($scope.parrent_event_manage.waiver_policies === null) {
                    $scope.waiverShow = false;
                    $scope.waiverarea = '';
                } else {
                    $scope.waiverShow = true;
                    $scope.waiverarea = $scope.parrent_event_manage.waiver_policies;
                }
                $scope.payment_event_type = 'M';
                $scope.final_payment_type = 'onetimemultiple';
            }

            if ($scope.manage_event_type === 'C') {
                $scope.pushkeyvalue = 1;
                for (var j = 0; j < $scope.childeventarray.length; j++) {
                    if ($scope.childeventarray[j].event_id === event_id) {
                        $scope.childindex = j;
                    }
                }
                if (($scope.childeventarray[$scope.childindex].event_capacity === null) || (parseFloat($scope.childeventarray[$scope.childindex].event_capacity) > 0 && (parseFloat($scope.childeventarray[$scope.childindex].event_capacity) - parseFloat($scope.childeventarray[$scope.childindex].registrations_count) > 0))) {
                    $scope.selectedeventlist($scope.childeventarray);
                } else {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text('Event is sold out');
                    return false;
                }
            }

            $scope.eventtitleaddpart = true;
            $scope.eventsubtitleaddpart = true;
            $scope.current_eventid_manage = event_id;
            var myEl = angular.element(document.querySelector('#eventtitleaddpart'));
            myEl.text($scope.parrent_event_manage.event_title);
            var myEl = angular.element(document.querySelector('#eventsubtitleaddpart'));
            myEl.text($scope.parrent_event_manage.event_category_subtitle);
            
            if($scope.stripe_enabled){
                $scope.stripe = Stripe($localStorage.stripe_publish_key);
                $scope.getStripeElements();
            }
        };

        //For selecting existing card of a user
        $scope.selectCardDetail = function () {
            $scope.cardnumber = '';
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.cvv = '';
            $scope.country = '';
            $scope.postal_code = '';

            if ($scope.selectedcardnumber === 'Add New credit card') {
                $scope.showpaymentview = true;
                $scope.selectedcardtype = 'new';
                $scope.credit_card_id = '';
                $scope.credit_card_status = '';
                $scope.credit_card_name = '';
                $scope.credit_card_expiration_month = '';
                $scope.credit_card_expiration_year = '';
                $scope.postal_code = '';
                $scope.payment_method_id = '';
                $scope.stripe_customer_id = '';      
            } else {
                $scope.cardindex = $scope.selectedcardnumber;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.carddetails[$scope.cardindex].payment_method_id;
                    $scope.stripe_customer_id = $scope.carddetails[$scope.cardindex].stripe_customer_id;
                }else{                    
                    $scope.credit_card_id = parseInt($scope.carddetails[$scope.cardindex].credit_card_id);
                    $scope.credit_card_status = $scope.carddetails[$scope.cardindex].credit_card_status;
                    $scope.credit_card_name = $scope.carddetails[$scope.cardindex].credit_card_name;
                    $scope.credit_card_expiration_month = $scope.carddetails[$scope.cardindex].credit_card_expiration_month;
                    $scope.credit_card_expiration_year = $scope.carddetails[$scope.cardindex].credit_card_expiration_year;
                    $scope.postal_code = $scope.carddetails[$scope.cardindex].postal_code;
                }
                $scope.showpaymentview = false;
                $scope.selectedcardtype = 'old';
            }
        };

        $scope.$watch(function () {
            return $scope.studentselected;
        }, function (newVal, oldVal) {
            $scope.participant_studentname = newVal;
            if ($scope.participant_studentname.student_name !== '' || $scope.participant_studentname.student_name !== undefined) {
                if ($scope.studentlist.length > 0) {
                    for (i = 0; i < $scope.studentlist.length; i++) {
                        if ($scope.participant_studentname.student_name === $scope.studentlist[i].student_name && $scope.studentlist[i].buyer_first_name !== null && $scope.studentlist[i].buyer_last_name !== null && $scope.participant_studentname.stud_index === $scope.studentlist[i].stud_index) {
                            $scope.student_id = $scope.studentlist[i].student_id;
                            $scope.student_name = $scope.studentlist[i].student_name;
                            $scope.reg_col_value[0] = $scope.studentlist[i].reg_col_1;
                            $scope.reg_col_value[1] = $scope.studentlist[i].reg_col_2;
                            $scope.firstname = $scope.studentlist[i].buyer_first_name;
                            $scope.lastname = $scope.studentlist[i].buyer_last_name;
                            $scope.email = $scope.studentlist[i].buyer_email;
                            $scope.phone = $scope.studentlist[i].buyer_phone;
                            $scope.postal_code = $scope.studentlist[i].buyer_postal_code;
                            if ($scope.studentlist[i].card_details.length > 0 && $scope.studentlist[i].card_details !== undefined) {
                                $scope.showcardselection = true;
                                $scope.showpaymentview = false;
                                $scope.carddetails = $scope.studentlist[i].card_details;
                            } else {
                                $scope.showcardselection = false;
                                $scope.showpaymentview = true;
                                $scope.carddetails = [];
                            }
                            return false;
                        } else {
                            $scope.student_id = 0;
                            $scope.student_name = $scope.participant_studentname;
                            $scope.reg_col_value[0] = '';
                            $scope.reg_col_value[1] = '';
                            $scope.reg_col_value[2] = '';
                            $scope.reg_col_value[3] = '';
                            $scope.reg_col_value[4] = '';
                            $scope.reg_col_value[5] = '';
                            $scope.reg_col_value[6] = '';
                            $scope.reg_col_value[7] = '';
                            $scope.reg_col_value[8] = '';
                            $scope.reg_col_value[9] = '';
                            $scope.firstname = '';
                            $scope.lastname = '';
                            $scope.email = '';
                            $scope.phone = '';
                            $scope.postal_code = '';
                            $scope.carddetails = [];
                            $scope.showcardselection = false;
                            $scope.showpaymentview = true;
                            $scope.cardnumber = '';
                            $scope.ccmonth = '';
                            $scope.ccyear = '';
                            $scope.cvv = '';
                            $scope.country = '';
                            $scope.selectedcardnumber = '';
                        }
                    }
                }

            } else {
                $scope.student_id = 0;
                $scope.student_name = $scope.participant_studentname;
                $scope.reg_col_value[0] = '';
                $scope.reg_col_value[1] = '';
                $scope.reg_col_value[2] = '';
                $scope.reg_col_value[3] = '';
                $scope.reg_col_value[4] = '';
                $scope.reg_col_value[5] = '';
                $scope.reg_col_value[6] = '';
                $scope.reg_col_value[7] = '';
                $scope.reg_col_value[8] = '';
                $scope.reg_col_value[9] = '';
                $scope.firstname = '';
                $scope.lastname = '';
                $scope.email = '';
                $scope.phone = '';
                $scope.postal_code = '';
                $scope.carddetails = [];
                $scope.showcardselection = false;
                $scope.showpaymentview = true;
                $scope.cardnumber = '';
                $scope.ccmonth = '';
                $scope.ccyear = '';
                $scope.cvv = '';
                $scope.country = '';
                $scope.selectedcardnumber = '';
            }

        });
        
        //Registration of a event for single/multiple events
        $scope.stripeEventCheckout = function (user_first_name, user_last_name, email, phone, postal_code, country) {
            $('#progress-full').show();
            for (var i = 0; i < $scope.current_event_reg_columns.length; i++) {
                if ($scope.reg_col_value[i] && $scope.reg_col_value[i] !== undefined && $scope.reg_col_value[i] !== '') {

                } else {
                    $scope.reg_col_value[i] = '';
                }
            }

            for (var j = 0; j < $scope.childlistcontents.length; j++) {
                if ($scope.childlistcontents[j]['event_quantity'] === '0' || $scope.childlistcontents[j]['event_quantity'] == 0) {
                    $scope.childlistcontents.shift();
                    j--;
                }
            }
            if (!user_first_name) {
                user_first_name = $scope.firstname;
            }
            if (!user_last_name) {
                user_last_name = $scope.lastname;
            }

            if ($scope.payment_event_type === 'S') {
                if ($scope.final_payment_type === 'onetimesingle') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'singlerecurring') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = $scope.pay;
                }
            } else if ($scope.payment_event_type === 'M') {
                if ($scope.final_payment_type === 'onetimemultiple') {
                    $scope.final_multiple_event_array = $scope.childlistcontents;
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'multiplerecurring') {
                    $scope.final_multiple_event_array = $scope.childlistcontents;
                    $scope.final_pay = $scope.pay;
                }
            }
            
            $scope.payment_amount = (Math.round(($scope.final_payment_amount + 0.00001) * 100) / 100).toFixed(2);
            var fee = 0;
            fee = $scope.pay_infull_processingfee;
            
            $http({
                method: 'POST',
                url: urlservice.url + 'stripeEventPortalCheckout',
                data: {
                    company_id: $localStorage.company_id,
                    event_id: $scope.register_event_id,
                    studentid: $scope.student_id,
                    event_name: $scope.register_event_title,
                    desc: $scope.register_event_title,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    phone: phone,
                    postal_code: postal_code,
                    country: country,
                    registration_amount: $scope.register_event_cost,
                    discount: $scope.register_discount_value,
                    payment_amount: $scope.payment_amount,
                    quantity: $scope.total_quantity,
                    processing_fee_type: $scope.current_processing_fee_type,
                    fee: fee,
                    payment_frequency: $scope.total_payment_frequency,
                    reg_col1: $scope.reg_col_value[0],
                    reg_col2: $scope.reg_col_value[1],
                    reg_col3: $scope.reg_col_value[2],
                    reg_col4: $scope.reg_col_value[3],
                    reg_col5: $scope.reg_col_value[4],
                    reg_col6: $scope.reg_col_value[5],
                    reg_col7: $scope.reg_col_value[6],
                    reg_col8: $scope.reg_col_value[7],
                    reg_col9: $scope.reg_col_value[8],
                    reg_col10: $scope.reg_col_value[9],
//                    cc_id: credit_card_id,
//                    cc_state: state,
                    event_array: $scope.final_multiple_event_array,
                    payment_array: $scope.final_pay,
                    event_type: $scope.payment_event_type,
                    payment_type: $scope.final_payment_type,
                    upgrade_status: $localStorage.upgrade_status,
                    total_order_amount: $scope.total_order_amount,
                    total_order_quantity: $scope.total_order_quantity,
                    payment_method_id : ($scope.events_payment_method == 'CC') ? $scope.payment_method_id : '',
                    payment_method : $scope.events_payment_method, //CC - credit card
                    cc_type: ($scope.selectedcardtype === 'new' && $scope.zerocostview) ? 'N' : 'E',
                    stripe_customer_id: ($scope.selectedcardtype === 'old' && $scope.zerocostview) ? $scope.stripe_customer_id : ''
                },

                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(response.data.msg);
                    $scope.onetimecheckout = 0;
                    $scope.selectedcardnumber = '';
                    $scope.childlistcontents = [];
                    $scope.childeventarray = [];
                    $scope.getStudentList();
                    $scope.current_event_manage = response.data.event_details.msg;
                    if ($scope.payment_event_type === 'S') {
                        $scope.manageevent($scope.current_event_manage,'P');
                    } else if ($scope.payment_event_type === 'M') {
                        $scope.childeventarray = $scope.current_event_manage.child_events;
                        $scope.childeventarray_copy = angular.copy($scope.childeventarray);
                        for (i = 0; i < $scope.current_event_manage.child_events.length; i++) {
                            if ($scope.current_event_manage.child_events[i].event_id === $scope.register_child_event_id) {
                                $scope.current_event_manage_detail = $scope.current_event_manage.child_events[i];
                            }
                        }
                        $scope.manageevent($scope.current_event_manage_detail,'child');
                    }
                    $scope.addparticipantview = false;
                    $scope.stripe_card.clear();
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.onetimecheckout = 0;
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    if (response.data.error) {
                        $("#pushmessagetitle").text(response.data.error);
                        $("#pushmessagecontent").text(response.data.error_description);
                    } else {
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    }
                    if (response.data.student_id) {
                        $scope.student_id = response.data.student_id;
                        $scope.getStudentList();
                    }
                }
            }, function (response) {
                $scope.onetimecheckout = 0;
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //Registration of a event for single/multiple events
        $scope.checkout = function (credit_card_id, state, user_first_name, user_last_name, email, phone, postal_code, country) {
            $('#progress-full').show();
            for (var i = 0; i < $scope.current_event_reg_columns.length; i++) {
                if ($scope.reg_col_value[i] && $scope.reg_col_value[i] !== undefined && $scope.reg_col_value[i] !== '') {

                } else {
                    $scope.reg_col_value[i] = '';
                }
            }

            for (var j = 0; j < $scope.childlistcontents.length; j++) {
                if ($scope.childlistcontents[j]['event_quantity'] === '0' || $scope.childlistcontents[j]['event_quantity'] == 0) {
                    $scope.childlistcontents.shift();
                    j--;
                }
            }
            if (!user_first_name) {
                user_first_name = $scope.firstname;
            }
            if (!user_last_name) {
                user_last_name = $scope.lastname;
            }

            if ($scope.payment_event_type === 'S') {
                if ($scope.final_payment_type === 'onetimesingle') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'singlerecurring') {
                    $scope.final_multiple_event_array = [];
                    $scope.final_pay = $scope.pay;
                }
            } else if ($scope.payment_event_type === 'M') {
                if ($scope.final_payment_type === 'onetimemultiple') {
                    $scope.final_multiple_event_array = $scope.childlistcontents;
                    $scope.final_pay = [];
                } else if ($scope.final_payment_type === 'multiplerecurring') {
                    $scope.final_multiple_event_array = $scope.childlistcontents;
                    $scope.final_pay = $scope.pay;
                }
            }

            $scope.payment_amount = (Math.round(($scope.final_payment_amount + 0.00001) * 100) / 100).toFixed(2);
            var fee = 0;
            fee = $scope.pay_infull_processingfee;

            $http({
                method: 'POST',
                url: urlservice.url + 'wepayPortalCheckout',
                data: {
                    company_id: $localStorage.company_id,
                    event_id: $scope.register_event_id,
                    studentid: $scope.student_id,
                    event_name: $scope.register_event_title,
                    desc: $scope.register_event_title,
                    buyer_first_name: user_first_name,
                    buyer_last_name: user_last_name,
                    email: email,
                    phone: phone,
                    postal_code: postal_code,
                    country: country,
                    registration_amount: $scope.register_event_cost,
                    discount: $scope.register_discount_value,
                    payment_amount: $scope.payment_amount,
                    quantity: $scope.total_quantity,
                    processing_fee_type: $scope.current_processing_fee_type,
                    fee: fee,
                    payment_frequency: $scope.total_payment_frequency,
                    reg_col1: $scope.reg_col_value[0],
                    reg_col2: $scope.reg_col_value[1],
                    reg_col3: $scope.reg_col_value[2],
                    reg_col4: $scope.reg_col_value[3],
                    reg_col5: $scope.reg_col_value[4],
                    reg_col6: $scope.reg_col_value[5],
                    reg_col7: $scope.reg_col_value[6],
                    reg_col8: $scope.reg_col_value[7],
                    reg_col9: $scope.reg_col_value[8],
                    reg_col10: $scope.reg_col_value[9],
                    cc_id: credit_card_id,
                    cc_state: state,
                    event_array: $scope.final_multiple_event_array,
                    payment_array: $scope.final_pay,
                    event_type: $scope.payment_event_type,
                    payment_type: $scope.final_payment_type,
                    upgrade_status: $localStorage.upgrade_status,
                    total_order_amount: $scope.total_order_amount,
                    total_order_quantity: $scope.total_order_quantity
                },

                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(response.data.msg);
                    $scope.onetimecheckout = 0;
                    $scope.selectedcardnumber = '';
                    $scope.childlistcontents = [];
                    $scope.childeventarray = [];
                    $scope.getStudentList();
                    $scope.current_event_manage = response.data.event_details.msg;
                    if ($scope.payment_event_type === 'S') {
                        $scope.manageevent($scope.current_event_manage,'P');
                    } else if ($scope.payment_event_type === 'M') {
                        $scope.childeventarray = $scope.current_event_manage.child_events;
                        $scope.childeventarray_copy = angular.copy($scope.childeventarray);
                        for (i = 0; i < $scope.current_event_manage.child_events.length; i++) {
                            if ($scope.current_event_manage.child_events[i].event_id === $scope.register_child_event_id) {
                                $scope.current_event_manage_detail = $scope.current_event_manage.child_events[i];
                            }
                        }
                        $scope.manageevent($scope.current_event_manage_detail,'child');
                    }
                    $scope.addparticipantview = false;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $scope.onetimecheckout = 0;
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    if (response.data.error) {
                        $("#pushmessagetitle").text(response.data.error);
                        $("#pushmessagecontent").text(response.data.error_description);
                    } else {
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    }
                    if (response.data.student_id) {
                        $scope.student_id = response.data.student_id;
                        $scope.getStudentList();
                    }
                }
            }, function (response) {
                $scope.onetimecheckout = 0;
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //Event drag and drop on live event listing
        $scope.drageventCallback = function (drag_type, ind) {
            if (drag_type === 'start') {
                $scope.old_location_id = ind;
                $scope.start_event_id = $scope.liveEventList[ind].event_id;
            } else if (drag_type === 'end') {
                setTimeout(function () {
                    for (var i = 0; i < $scope.liveEventList.length; i++) {
                        if ($scope.liveEventList[i].event_id === $scope.start_event_id) {
                            $scope.new_location_id = i;
                        }
                    }

                    if ($scope.old_location_id !== $scope.new_location_id) {
                        var sort_event_id, sort_event_status, event_sort_order, previous_sort_order, next_sort_order;
                        if ($scope.new_location_id === 0) {
                            sort_event_id = $scope.liveEventList[$scope.new_location_id].event_id;
                            sort_event_status = $scope.liveEventList[$scope.new_location_id].event_status;
                            previous_sort_order = parseFloat($scope.liveEventList[$scope.new_location_id + 1].event_sort_order);
                            event_sort_order = previous_sort_order + 1;
                            $scope.sortordereventupdate(sort_event_id, event_sort_order, sort_event_status);
                        } else if ($scope.new_location_id === $scope.liveEventList.length - 1) {
                            sort_event_id = $scope.liveEventList[$scope.new_location_id].event_id;
                            sort_event_status = $scope.liveEventList[$scope.new_location_id].event_status;
                            event_sort_order = parseFloat($scope.liveEventList[$scope.new_location_id - 1].event_sort_order) / 2;
                            $scope.sortordereventupdate(sort_event_id, event_sort_order, sort_event_status);
                        } else {
                            sort_event_id = $scope.liveEventList[$scope.new_location_id].event_id;
                            sort_event_status = $scope.liveEventList[$scope.new_location_id].event_status;
                            previous_sort_order = parseFloat($scope.liveEventList[$scope.new_location_id + 1].event_sort_order);
                            next_sort_order = parseFloat($scope.liveEventList[$scope.new_location_id - 1].event_sort_order);
                            event_sort_order = (previous_sort_order + next_sort_order) / 2;
                            $scope.sortordereventupdate(sort_event_id, event_sort_order, sort_event_status);
                        }
                    } else {
                        $('#progress-full').hide();
                    }

                }, 1000);
            }
        };

        //updating event sort order after drag and drop on live event listing
        $scope.sortordereventupdate = function (event_id, sort_id, sort_status) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updateEventSortingOrder',
                data: {
                    "sort_id": sort_id,
                    "event_id": event_id,
                    "list_type": sort_status,
                    "company_id": $localStorage.company_id
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    setTimeout(function () {
                        $route.reload();
                    }, 200);
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //resend order receipt to participant
        $scope.resendOrderReceipt = function (participant) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'sendEventOrderReceipt',
                data: {
                    "company_id": $localStorage.company_id,
                    "event_reg_id": participant.event_reg_id,
                },
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status == 'Success') {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text('Event Order receipt sent successfully.');
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //save participant of a register zero cost event 
        $scope.saveParticipantDetails = function () {
            $('#progress-full').show();
            var zero_event_id, zero_event_type = '';
            for (var i = 0; i < $scope.current_event_reg_columns.length; i++) {
                if ($scope.reg_col_value[i] && $scope.reg_col_value[i] !== undefined && $scope.reg_col_value[i] !== '') {

                } else {
                    $scope.reg_col_value[i] = '';
                }
            }

            if ($scope.manage_event_type === 'S') {
                $scope.final_multiple_event_array = [];
                zero_event_id = $scope.current_eventid_manage;
                zero_event_type = 'S';
            } else if ($scope.manage_event_type === 'C') {
                $scope.final_multiple_event_array = $scope.childlistcontents;
                zero_event_id = $scope.event_parent_id;
                zero_event_type = 'M';
            }

            $scope.payment_amount = 0;
            var fee = 0;
//                var userName = $scope.firstname+' '+$scope.lastname;
            var email = $scope.email;
            var phone = $scope.phone;
            var zip = $scope.postal_code;

            $http({
                method: 'POST',
                url: urlservice.url + 'addParticipantDetails',
                data: {
                    company_id: $localStorage.company_id,
                    event_id: zero_event_id,
                    student_id: $scope.student_id,
                    event_name: $scope.register_event_title,
                    desc: $scope.register_event_title,
                    registration_amount: $scope.current_event_manage.event_cost,
                    discount: 0,
                    quantity: $scope.total_quantity,
                    payment_amount: $scope.payment_amount,
                    processing_fee_type: $scope.current_event_manage.processing_fees,
                    fee: fee,
                    buyer_first_name: $scope.firstname,
                    buyer_last_name: $scope.lastname,
                    email: email,
                    phone: phone,
                    postal_code: zip,
                    reg_col1: $scope.reg_col_value[0],
                    reg_col2: $scope.reg_col_value[1],
                    reg_col3: $scope.reg_col_value[2],
                    reg_col4: $scope.reg_col_value[3],
                    reg_col5: $scope.reg_col_value[4],
                    reg_col6: $scope.reg_col_value[5],
                    reg_col7: $scope.reg_col_value[6],
                    reg_col8: $scope.reg_col_value[7],
                    reg_col9: $scope.reg_col_value[8],
                    reg_col10: $scope.reg_col_value[9],
                    event_array: $scope.final_multiple_event_array,
                    event_type: zero_event_type
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(response.data.msg);
//                    $scope.geteventdetails('P','');
                    $scope.getStudentList();
                    $scope.childeventarray = [];
                    $scope.final_multiple_event_array = [];
                    $scope.current_event_manage = response.data.event_details.msg;
                    if ($scope.manage_event_type === 'S') {
                        $scope.manageevent($scope.current_event_manage,'P');
                    } else if ($scope.manage_event_type === 'C') {
                        $scope.childeventarray = $scope.current_event_manage.child_events;
                        $scope.childeventarray_copy = angular.copy($scope.childeventarray);
                        for (i = 0; i < $scope.current_event_manage.child_events.length; i++) {
                            if ($scope.current_event_manage.child_events[i].event_id === $scope.register_child_event_id) {
                                $scope.current_event_manage_detail = $scope.current_event_manage.child_events[i];
                            }
                        }
                        $scope.manageevent($scope.current_event_manage_detail,'child');
                    }
                    $scope.addparticipantview = false;
                } else if (response.data.status === 'Expired') {
                    console.log(response.data);
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else if (response.data.status === 'Version') {
                    $('#progress-full').hide();
                    $scope.handleFailure(response.data.msg);
                } else {
                    $scope.onetimecheckout = 0;
                    $('#progress-full').hide();
                    $("#pushmessageModal").modal('show');
                    if (response.data.error) {
                        $("#pushmessagetitle").text(response.data.error);
                        $("#pushmessagecontent").text(response.data.error_description);
                    } else {
                        $("#pushmessagetitle").text('Message');
                        $("#pushmessagecontent").text(response.data.msg);
                    }
                    if (response.data.student_id) {
                        $scope.student_id2 = response.data.student_id;
                        $scope.getStudentList();
                    }
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        //processing fee calculation for recurring payment amount
        $scope.recurringProcessingFee = function (total_amount) {
            var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
            var process_fee_val = parseFloat($localStorage.processing_fee_transaction);
            var recur_process_fee = 0;

            if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                if (parseFloat(total_amount) > 0) {
                    var process_cost1 = +(parseFloat(total_amount) + +(((parseFloat(total_amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val)));
                    var process_cost = +(((parseFloat(process_cost1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val));
                    recur_process_fee = parseFloat(Math.round((+process_cost + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    recur_process_fee = 0;
                }
            } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                if (parseFloat(total_amount) > 0) {
                    var process_fee = +((parseFloat(total_amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                    recur_process_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                } else {
                    recur_process_fee = 0;
                }
            }
            return recur_process_fee;
        };

        //discount value exists event cost validation
        $scope.discountMax = function (disc) {
            $scope.actual_event_cost = $scope.total_event_amount = 0;
            if ($scope.manage_event_type === 'S') {
                $scope.actual_event_cost = +$scope.current_event_manage.event_cost * +$scope.single_event_quantity;
            } else if ($scope.manage_event_type === 'C') {
                if ($scope.childlistcontents.length > 0) {
                    for (var j = 0; j < $scope.childlistcontents.length; j++) {
                        $scope.total_event_amount = (+$scope.childlistcontents[j]['total_event_cost'] + +$scope.total_event_amount);
                    }
                    $scope.actual_event_cost = parseFloat($scope.total_event_amount);
                } else {
                    $scope.actual_event_cost = parseFloat($scope.current_event_manage.event_cost);
                }
            }

            if (disc === '') {
                return false;
            }
            if (parseFloat(disc) > 0 && parseFloat(disc) > parseFloat($scope.actual_event_cost)) {
                return true;
            } else {
//                    $scope.showTotal();     
//                    $scope.paymentOptions();
                return false;
            }
        };

        //discount value applied for event cost below 5 validation
        $scope.EventCostMax = function (disc) {
            $scope.actual_event_cost = $scope.total_event_amount = 0;
            if ($scope.manage_event_type === 'S') {
                $scope.actual_event_cost = +$scope.current_event_manage.event_cost * +$scope.single_event_quantity;
            } else if ($scope.manage_event_type === 'C') {
                if ($scope.childlistcontents.length > 0) {
                    for (var j = 0; j < $scope.childlistcontents.length; j++) {
                        $scope.total_event_amount = (+$scope.childlistcontents[j]['total_event_cost'] + +$scope.total_event_amount);
                    }
                    $scope.actual_event_cost = parseFloat($scope.total_event_amount);
                } else {
                    $scope.actual_event_cost = parseFloat($scope.current_event_manage.event_cost);
                }
            }

            if (disc === '') {
                return false;
            }
            if ((parseFloat(disc) > 0 && parseFloat('5') <= (parseFloat($scope.actual_event_cost) - parseFloat(disc))) || (parseFloat(disc) > 0 && parseFloat('0') == (parseFloat($scope.actual_event_cost) - parseFloat(disc)))) {
//                    $scope.showTotal(); 
//                    $scope.paymentOptions();  
                return false;
            } else {
                return true;
            }

        };

        $scope.resetModalTxt = function () {
            $("#pushmessagetitle").text('');
            $("#pushmessagecontent").text('');
        };

        $scope.showTotalView = function () {
            $scope.showTotal();
            $scope.paymentOptions();
        };


        //For single event & Multiple event total cost calculation based on discount and quantity
        $scope.showTotal = function () {
            $scope.total_amount = 0;
            $scope.multiple_total_amount = 0;
            $scope.total_quantity = 0;
            $scope.total_amount_view = 0;
            $scope.processing_fee_value = 0;
            $scope.final_payment_amount = $scope.pay_infull_processingfee = 0;
            $scope.dynamic_discount = $scope.dynamic_event_cost = $scope.dynamic_disc_cost = 0;
            $scope.actual_event_cost = $scope.total_event_amount = 0;
            $scope.register_discount_value = $scope.register_event_cost = $scope.discount_view_value = 0;
            $scope.discount_show = $scope.childaddedvalidate = true;

            var process_fee_per = 0, process_fee_val = 0, discount_compare_value = 0;
            var process_fee_per = parseFloat($localStorage.processing_fee_percentage);
            var process_fee_val = parseFloat($localStorage.processing_fee_transaction);

            //For single & multiple event total cost
            if ($scope.manage_event_type === 'S') {  //single event
                $scope.actual_event_cost = parseFloat($scope.current_event_manage.event_cost);
                $scope.register_event_cost = $scope.actual_event_cost;
            } else if ($scope.manage_event_type === 'C') { //multiple - child event
                if ($scope.childlistcontents.length > 0) {
                    for (var j = 0; j < $scope.childlistcontents.length; j++) {
                        $scope.total_event_amount = (+$scope.childlistcontents[j]['total_event_cost'] + +$scope.total_event_amount);
                    }
                    $scope.actual_event_cost = parseFloat($scope.total_event_amount);
                } else {
                    $scope.actual_event_cost = 0;
                }
                $scope.register_event_cost = $scope.actual_event_cost;
            }


            //For single event - quantity and discount based calculation
            if ($scope.manage_event_type === 'S') {
                $scope.current_processing_fee_type = $scope.current_event_manage.processing_fees;

                if (parseFloat($scope.current_event_manage.event_cost) > 0) {
                    if ($scope.single_event_quantity > 0) {
                        $scope.total_amount_view = +$scope.current_event_manage.event_cost * +$scope.single_event_quantity;
                        $scope.total_quantity = +$scope.single_event_quantity;
                    } else if ($scope.single_event_quantity === undefined || $scope.single_event_quantity === null || $scope.single_event_quantity === '') {
                        $scope.total_amount_view = +$scope.current_event_manage.event_cost;
                        $scope.total_quantity = 1;
                    }
                } else {
                    $scope.pay = [];
                    $scope.recurrent = {};
                    $scope.discount_show = false;
                    if ($scope.single_event_quantity > 0) {
                        $scope.total_amount_view = 0;
                        $scope.total_quantity = +$scope.single_event_quantity;
                    } else if ($scope.single_event_quantity === undefined || $scope.single_event_quantity === null || $scope.single_event_quantity === '') {
                        $scope.total_amount_view = 0;
                        $scope.total_quantity = 1;
                    }
                }

                if ($scope.discount > 0) {
                    $scope.total_amount_view = +$scope.total_amount_view - +$scope.discount;
                    $scope.register_discount_value = $scope.discount;
                }

                var amount = $scope.total_amount_view;
                $scope.final_payment_amount = amount;

                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                    if (amount > 0) {
                        var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = +parseFloat(amount) + +$scope.processing_fee_value;
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (amount > 0) {
                        var process_fee = +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = amount;
                }
                $scope.pay_infull_processingfee = $scope.processing_fee_value;

                $scope.recurring_payment_show = false;
                var todayDate = new Date();
                var start_date = new Date($scope.total_payment_startdate);
                if (!$scope.total_order_amount) {
                    $scope.total_order_amount = 0;
                }
                if (!$scope.total_order_quantity) {
                    $scope.total_order_quantity = 0;
                }
                if (!$scope.total_number_of_payments) {
                    $scope.total_number_of_payments = 0;
                }
                if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.total_amount_view) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate)
                        && parseInt($scope.total_number_of_payments) > 0) {
                    $scope.recurring_payment_show = true;
                } else {
                    $scope.recurring_payment_show = false;
                    $scope.payment_times = 'O';
                    $scope.final_payment_type = 'onetimesingle';
                    $scope.payment_event_type = 'S';
                    $scope.pay = [];
                    $scope.recurrent = {};
                }


            } else if ($scope.manage_event_type === 'C') {  //multiple - child event - quantity and discount based calculation

                $scope.current_processing_fee_type = $scope.parrent_event_manage.processing_fees;
                $scope.childlistcontents.sort(function (a, b) {
                    return a.total_event_cost - b.total_event_cost
                }); //sorting events list based on total event cost ascending 
                $scope.dynamic_discount = $scope.discount;
                $scope.register_discount_value = $scope.discount;
                $scope.dynamic_event_cost = $scope.actual_event_cost;
                var total_disc_amount = 0;

                if ($scope.childlistcontents.length > 0) {
                    for (var j = 0; j < $scope.childlistcontents.length; j++) {
                        $scope.total_amount = (+$scope.childlistcontents[j]['total_event_cost'] + +$scope.total_amount);
                        $scope.dynamic_disc_cost = parseFloat($scope.dynamic_discount) / parseFloat($scope.dynamic_event_cost);
                        $scope.total_quantity = +$scope.total_quantity + +$scope.childlistcontents[j]['event_quantity'];
                        if ($scope.dynamic_discount > 0) {
                            discount_compare_value = +$scope.childlistcontents[j]['total_event_cost'] - (+$scope.childlistcontents[j]['total_event_cost'] * +$scope.dynamic_disc_cost);

                            if (discount_compare_value >= 5) {                    //event cost within discount cost and (event cost - discount cost) above $5

                                $scope.childlistcontents[j]['event_payment_amount'] = parseFloat(Math.round((+discount_compare_value + +0.00001) * 100) / 100).toFixed(2);
                                $scope.childlistcontents[j]['event_discount_amount'] = parseFloat(Math.round((((+$scope.childlistcontents[j]['total_event_cost'] * +$scope.dynamic_disc_cost) / +$scope.childlistcontents[j]['event_quantity']) + +0.00001) * 100) / 100).toFixed(2);
                                $scope.multiple_total_amount = +$scope.multiple_total_amount + +$scope.childlistcontents[j]['event_payment_amount'];
                                $scope.total_amount_view = $scope.multiple_total_amount;

                            } else {                                              //(event cost - discount cost) below $5 - Free event cost

                                $scope.dynamic_discount = +$scope.dynamic_discount - +$scope.childlistcontents[j]['total_event_cost'];
                                $scope.dynamic_event_cost = +$scope.dynamic_event_cost - +$scope.childlistcontents[j]['total_event_cost'];
                                $scope.childlistcontents[j]['event_payment_amount'] = '0';
                                $scope.childlistcontents[j]['event_discount_amount'] = parseFloat(Math.round((+$scope.childlistcontents[j]['event_cost'] + +0.00001) * 100) / 100).toFixed(2);
                                $scope.multiple_total_amount = +$scope.multiple_total_amount + +$scope.childlistcontents[j]['event_payment_amount'];
                                $scope.total_amount_view = $scope.multiple_total_amount;
                            }
                            if(j == $scope.childlistcontents.length-1){
                                $scope.childlistcontents[j]['event_discount_amount'] = parseFloat(Math.round(((($scope.dynamic_discount - total_disc_amount) / +$scope.childlistcontents[j]['event_quantity']) + +0.00001) * 100) / 100).toFixed(2);
                            }
                            total_disc_amount = (+$scope.childlistcontents[j]['event_discount_amount'] * +$scope.childlistcontents[j]['event_quantity']) + +total_disc_amount;
                        } else {        //zero discount value
                            $scope.total_amount_view = $scope.total_amount;
                            $scope.childlistcontents[j]['event_payment_amount'] = +$scope.childlistcontents[j]['total_event_cost'];
                            $scope.childlistcontents[j]['event_discount_amount'] = '0';
                        }

                        $scope.discount_view_value = +$scope.discount_view_value + +$scope.childlistcontents[j]['event_cost'];
                        if (parseFloat($scope.discount_view_value) > 0) {
                            $scope.discount_show = true;
                        } else {
                            $scope.discount_show = false;
                        }
                    }

                } else {
                    $scope.discount_show = false;
                    $scope.total_amount_view = 0;
                    $scope.total_quantity = 0;
                    $scope.pay = [];
                    $scope.recurrent = {};
                }

                var amount = $scope.total_amount_view;
                $scope.final_payment_amount = amount;

                if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') { //processing fee
                    if (amount > 0) {
                        var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = +parseFloat(amount) + +$scope.processing_fee_value;
                } else if ($scope.current_processing_fee_type === 1 || $scope.current_processing_fee_type === '1') {
                    if (amount > 0) {
                        var process_fee = +((parseFloat(amount) * parseFloat(process_fee_per)) / 100) + +parseFloat(process_fee_val);
                        $scope.processing_fee_value = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    } else {
                        $scope.processing_fee_value = 0;
                    }
                    $scope.total_amount_view = amount;
                }

                $scope.pay_infull_processingfee = $scope.processing_fee_value;

                $scope.recurring_payment_show = false;
                var todayDate = new Date();
                var start_date = new Date($scope.total_payment_startdate);
                if (!$scope.total_order_amount) {
                    $scope.total_order_amount = 0;
                }
                if (!$scope.total_order_quantity) {
                    $scope.total_order_quantity = 0;
                }
                if (!$scope.total_number_of_payments) {
                    $scope.total_number_of_payments = 0;
                }

                if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.total_amount_view) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate)
                        && parseInt($scope.total_number_of_payments) > 0) {
                    $scope.recurring_payment_show = true;
                } else {
                    $scope.recurring_payment_show = false;
                    $scope.payment_times = 'O';
                    $scope.final_payment_type = 'onetimemultiple';
                    $scope.payment_event_type = 'M';
                    $scope.pay = [];
                    $scope.recurrent = {};
                }

                if ($scope.childlistcontents.length > 0) {
                    $scope.childaddedvalidate = true;
                } else {
                    $scope.childaddedvalidate = false;
                }
            }

            if (parseFloat($scope.total_amount_view) == parseFloat('0')) {
                $scope.compareDiscountCost = false;
                $scope.zerocostview = false;
                $scope.showpaymentview = false;
                $scope.selectedcardnumber = '';
            } else {
                $scope.compareDiscountCost = true;
                $scope.zerocostview = true;
                if ($scope.carddetails !== undefined && $scope.carddetails.length > 0 && $scope.carddetails !== '' && $scope.selectedcardnumber !== 'Add New credit card') {
                    $scope.showpaymentview = false;  // belongs to Existing credit card
                } else {
                    $scope.showpaymentview = true;    // belongs to new credit card
                }
            }
        };


        //recurring payment for single/multiple events
        $scope.paymentOptions = function () {

            var todayDate = new Date();
            var start_date = new Date($scope.total_payment_startdate);

            if ($scope.recurring_payment_flag === 'Y' && (parseFloat($scope.total_amount_view) > parseFloat($scope.total_order_amount)) && (parseInt($scope.total_quantity) > parseInt($scope.total_order_quantity)) && (start_date > todayDate)
                    && parseInt($scope.total_number_of_payments) > 0) {

                $scope.balance_amount = $scope.final_payment_amount - $scope.total_deposit_amount;
                $scope.recurring_payment_amount = (Math.round((($scope.balance_amount / $scope.total_number_of_payments) + 0.00001) * 100) / 100).toFixed(2);

//                    alert('payment start date: '+$scope.total_payment_startdate+'num of payments: '+$scope.total_number_of_payments+'recurr amount: '+$scope.recurring_payment_amount+'deposit amount: '+$scope.total_deposit_amount+'balance amount: '+$scope.balance_amount);
                if ($scope.total_payment_frequency === '1') {  //Monthly                         
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate, $scope.total_number_of_payments, 'monthly', $scope.recurring_payment_amount, $scope.total_deposit_amount, $scope.balance_amount);
                } else if ($scope.total_payment_frequency === '2') {  //Weekly - 7 days                        
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate, $scope.total_number_of_payments, 'weekly', $scope.recurring_payment_amount, $scope.total_deposit_amount, $scope.balance_amount);
                } else if ($scope.total_payment_frequency === '3') {  //Bi-Weekly - 14 days
                    $scope.payment_schedule = $scope.payment_recurring_period($scope.total_payment_startdate, $scope.total_number_of_payments, 'biweekly', $scope.recurring_payment_amount, $scope.total_deposit_amount, $scope.balance_amount);
                }
            }


        };

        //recurring payment setup
        $scope.payment_recurring_period = function (startDate, number_of_payments, intervalType, recurring_amnt, deposit_amt, balance_amount) {
            $scope.pay = [];
            $scope.recurrent = {};
            var newDate = startDate.split('-');
            var given_start_year = newDate[0];
            var given_start_month = newDate[1] - 1;
            var given_start_day = newDate[2];
            var curr_date = new Date();
            curr_date = curr_date.toISOString().split('T')[0];

            var dup_date = new Date(given_start_year, given_start_month, given_start_day);
            $scope.recurrent.date = curr_date;
            $scope.recurrent.amount = deposit_amt;
            $scope.recurrent.type = 'D';
            $scope.recurrent.processing_fee = $scope.recurringProcessingFee(deposit_amt);
            $scope.pay.push($scope.recurrent);
            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);


            if ($scope.current_processing_fee_type === 2 || $scope.current_processing_fee_type === '2') {
                $scope.r_total_deposit_amount = parseFloat(deposit_amt) + parseFloat($scope.recurrent.processing_fee);
                $scope.deposit_processingfee = $scope.recurringProcessingFee(deposit_amt);
            } else {
                $scope.r_total_deposit_amount = parseFloat(deposit_amt);
                $scope.deposit_processingfee = 0;
            }
            if (intervalType === 'monthly' && parseFloat(recurring_amnt)>0) {
                for (var i = 0; i < number_of_payments; i++) {
                    $scope.recurrent = {};
                    if (i === 0) {
                        $scope.recurrent['date'] = startDate;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    } else {
                        if (i === number_of_payments - 1) {
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setMonth(dup_date.getMonth() + 1);
                        var dup = dup_date;
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        $scope.recurrent['date'] = date;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            } else if (intervalType === 'weekly' && parseFloat(recurring_amnt)>0) {
                for (var i = 0; i < number_of_payments; i++) {
                    $scope.recurrent = {};
                    if (i === 0) {
                        $scope.recurrent['date'] = startDate;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    } else {
                        if (i === number_of_payments - 1) {
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setDate(dup_date.getDate() + 7);
                        var dup = dup_date;
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        $scope.recurrent['date'] = date;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            } else if (intervalType === 'biweekly' && parseFloat(recurring_amnt)>0) {
                for (var i = 0; i < number_of_payments; i++) {
                    $scope.recurrent = {};
                    if (i === 0) {
                        $scope.recurrent['date'] = startDate;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    } else {
                        if (i === number_of_payments - 1) {
                            recurring_amnt = +balance_amount - (recurring_amnt * (+number_of_payments - +1));
                            $scope.recurring_processing_fee = $scope.recurringProcessingFee(recurring_amnt);
                        }
                        dup_date.setDate(dup_date.getDate() + 14);
                        var dup = dup_date;
                        var date = dup.getFullYear() + '-' + ('0' + (dup.getMonth() + 1)).slice(-2) + '-' + ('0' + dup.getDate()).slice(-2);
                        $scope.recurrent['date'] = date;
                        $scope.recurrent['amount'] = recurring_amnt;
                        $scope.recurrent['type'] = 'R';
                        $scope.recurrent['processing_fee'] = $scope.recurring_processing_fee;
                        $scope.pay.push($scope.recurrent);
                    }
                }
            }
        };

        //reset tab bar highlighted to recent tab access
        $scope.recentorderstab = function () {
            $scope.recentorders = true;
            $scope.cancelledorders = false;
            $scope.current_tab = 'O';
            $scope.change_made = $scope.search_value = '';
            $scope.resettabbarclass();
            $('#li-orders').addClass('active-event-list').removeClass('event-list-title');
            $('.manageeventsub-tabtext-1').addClass('greentab-text-active');
            $scope.getparticipants_list($scope.company_id_tab, $scope.event_id_tab, 'O');
            $scope.dtOptions
                    .withOption('stateLoadParams', function (settings, data) {
                        data.search.search = "";
                    })
//                $scope.scrollByEventTableId($scope.company_id_tab, $scope.event_id_tab, 'event_table_orders', 'O');
        };

        //reset tab bar highlighted to recent tab access
        $scope.cancelrefundtab = function () {
            $scope.recentorders = false;
            $scope.cancelledorders = true;
            $scope.current_tab = 'C';
            $scope.change_made = $scope.search_value = '';
            $scope.resettabbarclass();
            $('#li-cancelorders').addClass('active-event-list').removeClass('event-list-title');
            $('.manageeventsub-tabtext-2').addClass('greentab-text-active');
            $scope.getparticipants_list($scope.company_id_tab, $scope.event_id_tab, 'C');
            $scope.dtOptions1
                    .withOption('stateLoadParams', function (settings, data) {
                        data.search.search = "";
                    })
//                $scope.scrollByEventTableId($scope.company_id_tab,$scope.event_id_tab, 'event_table_cancel', 'C');
        };

        //Pdf file download on listing screen
        $scope.liveexportpdfevent = function (event_id, event_title) {

            $http({
                method: 'POST',
                url: urlservice.url + 'exportData',
                responseType: 'arraybuffer',
                data: {
                    "company_id": $localStorage.company_id,
                    "event_id": event_id,
                    "export_type": "P"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).success(function (data, status, headers) {
                if (data.status !== undefined && data.status === 'Failed') {
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(data.msg);
                    return false;
                }
                var msg = '';
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], {type: "application/x-download"});
                    var objectUrl = URL.createObjectURL(blob);
                    linkElement.setAttribute('href', objectUrl);
                    linkElement.setAttribute("download", event_title + ".pdf");
                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    msg = ex;
                    console.log(ex);
                }
                if (msg === '') {
                    msg = "PDF file downloaded Successfully";
                }
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text(msg);

            }).error(function () {
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
            });
        };

        //CSV file download on listing screen
        $scope.liveexportcsvevent = function (event_id, event_title) {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'exportData',
                responseType: 'arraybuffer',
                data: {
                    "company_id": $localStorage.company_id,
                    "event_id": event_id,
                    "export_type": "C"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).success(function (data, status, headers) {
                if (data.status !== undefined && data.status === 'Failed') {
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text(data.msg);
                    return false;
                } else {
                    $("#pushmessageModal").modal('show');
                    $("#pushmessagetitle").text('Message');
                    $("#pushmessagecontent").text("Participant Details exported as csv file successfully.");
                }
                var msg = '';
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], {type: "application/x-download"});
                    var objectUrl = URL.createObjectURL(blob);
                    linkElement.setAttribute('href', objectUrl);
                    linkElement.setAttribute("download", event_title + ".csv");
                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    msg = ex;
                    console.log(ex);
                }
                if (msg === '') {
                    msg = "CSV file downloaded Successfully";
                }
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text(msg);

            }).error(function () {
                $('#progress-full').hide();
                $("#pushmessageModal").modal('show');
                $("#pushmessagetitle").text('Message');
                $("#pushmessagecontent").text("Unable to reach Server, Please Try again.");
            });
        };

        $scope.clearMessageContent = function () {
            $scope.pushmsgdata = $scope.emailsubjectdata = '';
            $scope.removeAttachfile();
        };

        $scope.removeAttachfile = function () {
            $scope.maxEmailAttachmentSizeError = false;
            $scope.filepreview = "";
            $scope.attachfile = '';
            $scope.file_name = '';
            $scope.fileurl = '';
        };

        $scope.maintain_SelectedOrderList = function (tab) {
            // FOR CHECK BOX FUNCTIONALITIES 
            if (tab === 'O') {
                for (i = 0; i < $scope.participantlist.length; i++) {
                    for (j = 0; j < $scope.selectedorderlist.length; j++) {
                        if ($scope.participantlist[i].event_reg_id === $scope.selectedorderlist[j].event_reg_id) {
                            var mem_checkbox_id = 'individual_check_order' + i;
                            $("#" + mem_checkbox_id).prop("checked", true);
                        }
                    }
                }
            } else if (tab === 'C') {
                for (i = 0; i < $scope.cancelledOrderList.length; i++) {
                    for (j = 0; j < $scope.selectedorderlist.length; j++) {
                        if ($scope.cancelledOrderList[i].event_reg_id === $scope.selectedorderlist[j].event_reg_id) {
                            var mem_checkbox_id = 'individual_check_cancel' + i;
                            $("#" + mem_checkbox_id).prop("checked", true);
                        }
                    }
                }
            }

            if ($scope.check_all_order) {
                $scope.selectedorderlist = [];
                for (i = 0; i < $scope.participantlist.length; i++) {
                    $scope.selectedorderlist.push($scope.participantlist[i]);
                }
            }

            if ($scope.check_all_cancel) {
                $scope.selectedorderlist = [];
                for (i = 0; i < $scope.cancelledOrderList.length; i++) {
                    $scope.selectedorderlist.push($scope.participantlist[i]);
                }
            }
        };



        $scope.clearSelectedOrderList = function () {
            $scope.removeAttachfile();
            $scope.grppushmsgdata = $scope.grpemailsubjectdata = $scope.grpemailmsgdata = '';
            $scope.also_send_email = false;
            $scope.selectedorderlist = [];
            $scope.individual_check_order = $scope.check_all_order = false;
            $scope.individual_check_cancel = $scope.check_all_cancel = false;
            if ($scope.clearfromindividual) {//clear if modal open from individual
                $scope.selectedData = [];
            }
            $scope.clearfromindividual = false;
        };

        $scope.sendindividualpushmail = function (selectedMember, type) {  // INDIVIDUAL PUSH / EMAIL MESSAGE
            $scope.clearfromindividual = true;
            $scope.message_type = type;
            $scope.selectAll = false;
            $scope.toggleAll(false, $scope.maintainselected);
            $scope.selectedData = [];
            $scope.selectedData.push({"id": selectedMember});
            $scope.grpbuttonText = "Send";
            if (type === 'push') {
                $("#grpeventpushmessagecontModal").modal('show');
                $("#grpeventpushmessageconttitle").text('Send Push Message');
            } else if (type === 'mail') {
                $("#grpeventemailmessagecontModal").modal('show');
                $("#grpeventemailmessageconttitle").text('Send Email');
                $("#message_content_1_desclink").hide();
                $("#message_content_1_maillink").show();
            }
        }

        $scope.sendGroupMsg = function (type) {
            $scope.message_type = type;
            $scope.grpbuttonText = "Send";
            if ($scope.selectedData.length > 0 || $scope.all_select_flag === 'Y') {
                if (type === 'push') {
                    $("#grpeventpushmessagecontModal").modal('show');
                    $("#grpeventpushmessageconttitle").text('Send Push Message');
                } else if (type === 'mail') {
                    $("#grpeventemailmessagecontModal").modal('show');
                    $("#grpeventemailmessageconttitle").text('Send Email Message');
                    $("#message_content_1_desclink").hide();
                    $("#message_content_1_maillink").show();
                }
            } else {
                var x = document.getElementById("eventsnackbar")
                x.className = "showevent";
                setTimeout(function () {
                    x.className = x.className.replace("showevent", "");
                }, 3000);
            }
        }

        $scope.sendGroupEmailPush = function (message_type) {
            $('#progress-full').show();
            var msg = '';
            var subject, unsubscribe_email = '';
            if (message_type === 'push') {
                if ($scope.grppushmsgdata.trim().length === 0) {
                    return;
                }
                subject = '';
                msg = $scope.grppushmsgdata;
            } else if (message_type === 'mail') {
                if ($scope.grpemailmsgdata.trim().length === 0 || $scope.grpemailsubjectdata.trim().length === 0) {
                    return;
                }
                subject = $scope.grpemailsubjectdata;
                msg = $scope.grpemailmsgdata;

                if ($scope.attachfile !== '' && $scope.attachfile !== undefined && $scope.attachfile !== null) {
                    $scope.fileurl = $scope.filepreview.substr($scope.filepreview.indexOf(",") + 1);
                    $scope.file_name = $scope.attachfile.split("fakepath")[1].substr(1);
                } else {
                    $scope.fileurl = '';
                    $scope.file_name = '';
                }
            } else if (message_type === 'subscribe') {
                subject = 'Subscribe to our email communications!';
                msg = 'click here to subscribe';
                unsubscribe_email = $scope.unsubscribe_email;
            }

            $http({
                method: 'POST',
                url: urlservice.url + 'sendEventGroupPush',
                data: {
                    "company_id": $localStorage.company_id,
                    "type": message_type,
                    "subject": subject,
                    "message": msg,
                    "also_send_mail": $scope.also_send_email ? 'Y' : 'N',
                    "selected_event_list": $scope.selectedData,
                    "file_name": $scope.file_name,
                    "attached_file": $scope.fileurl,
                    "all_select_flag": $scope.all_select_flag,
                    "event_id": $scope.evet_id,
                    "from": "manageEvent",
                    "status": $scope.current_tab,
                    "search": $scope.searchTerm,
                    "unsubscribe_email": unsubscribe_email

                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8', },
                withCredentials: true

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $("#sendsubscriptioninviteModal").modal('hide');
                    $scope.clearSelectedOrderList();
                    $("#grpeventemailmessagecontModal").modal('hide');
                    $("#grpeventemailmessageconttitle").text('');
                    $("#grpeventpushmessagecontModal").modal('hide');
                    $("#grpeventpushmessageconttitle").text('');
                    $('#progress-full').hide();
                    $("#msgModal").modal('show');
                    $("#msgtitle").text('Message');
                    $("#msgcontent").text(response.data.msg);
                } else if (response.data.status === 'Expired') {
                    $('#progress-full').hide();
                    $("#messageModal").modal('show');
                    $("#messagetitle").text('Message');
                    $("#messagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $("#grpeventemailmessagecontModal").modal('hide');
                    $("#grpeventemailmessageconttitle").text('');
                    $("#grpeventpushmessagecontModal").modal('hide');
                    $("#grpeventpushmessageconttitle").text('');
                    $scope.clearSelectedOrderList();
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        $scope.eventDetail = function (event_reg_id, current_tab, event_id) {
            $localStorage.currentEventregid = event_reg_id;
            $localStorage.currentEventid = event_id;
            $localStorage.page_from = 'events_' + current_tab;
            $location.path('/eventdetail');
        };

        $scope.sendSubscribeEmail = function (mail) {
            $scope.unsubscribe_email = mail;
            $scope.message_type = 'subscribe';
            $("#sendsubscriptioninviteModal").modal('show');
        };
//            Muthulakshmi
        $scope.openorgoaddevent = function (type) {
            if (type === 'C') {
                $("#addnewevent-modal").modal('hide');
                $localStorage.eventpagetype = 'new';
                $scope.currentEventPage = false;
                $localStorage.eventlocalSingledata = "";
                $localStorage.event_details = "";
                $localStorage.isAddedEventDetails = "N";
                $localStorage.currentpage = "addevents";
                $localStorage.preview_eventcontent = '';
                $('#addnewevent-modal').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/addevent';
                });
            } else if (type === 'T') {
                $("#addnewevent-modal").modal('hide');
                $scope.currentEventPage = false;
                $('#addnewevent-modal').on('hidden.bs.modal', function (e) {
                    $window.location.href = window.location.origin + window.location.pathname + '#/template';
                });
            } else {
                $("#addnewevent-modal").modal('show');
            }
        };
        $scope.openEventPublicurl = function () {
            window.open($scope.Eventregurl, "_blank");
        };
        
        $scope.gobackformanageevent = function () {
            $scope.currentEventPage = true;
            if ($scope.eventfromliveordraft === 'S') {
                $scope.draftlistview();
            } else if ($scope.eventfromliveordraft === 'D') {
                $scope.pastlistview();
            } else {
                $scope.livelistview();
            }
        };
        $scope.updateEventOnOffFlag = function (value) {
            if ($scope.eventStatus === true) {
                $(".toggle-tooltip-new").css("visibility", "visible");
                $scope.eventStatus = 'Y';
            } else {
                $(".toggle-tooltip-new").css("visibility", "hidden");
                $scope.eventStatus = 'N';
            }
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'updateEventEnabledStatus',
                data: {
                    'company_id': $localStorage.company_id,
                    'event_enabled_status': $scope.eventStatus
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                },
                withCredentials: true
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    setTimeout(function () {    
                      $(".toggle-tooltip-new").css("visibility", "hidden");  
                    }, 10000);
                    $('#progress-full').hide();
                    $scope.eventStatus = response.data.msg[0].event_enabled;
                    $localStorage.eventenabledstatus = $scope.eventStatus;
                } else if (response.data.status === 'Expired') {
                    $(".toggle-tooltip-new").css("visibility", "hidden");
                    $('#progress-full').hide();
                    $("#retailmanagemodal").modal('show');
                    $("#retailmanagecontent").text(response.data.msg);
                    $scope.logout();
                } else {
                    $(".toggle-tooltip-new").css("visibility", "hidden");
                    $scope.handleFailure(response.data.msg);
                }
            }, function (response) {
                $(".toggle-tooltip-new").css("visibility", "hidden");
                console.log(response.data);
                $scope.handleError(response.data);
            });
        };

        window.onscroll = function (ev) {
            if (!$scope.bottomhitstopcall && $scope.currentEventPage && $rootScope.activepagehighlight === 'events'&& !$scope.childeventsview && ($scope.window_bottom_hit_page === 'P' || $scope.window_bottom_hit_page === 'S' || $scope.window_bottom_hit_page === 'D')) {//Window bottom hit
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    if (!$scope.bottomhitstopcall && $scope.currentEventPage && ($scope.window_bottom_hit_page === 'P' || $scope.window_bottom_hit_page === 'S' || $scope.window_bottom_hit_page === 'D')){
                        $scope.window_bottom_hit_limit += 20;
//                        $scope.geteventdetails($scope.window_bottom_hit_page,'');
                    }
                }
            }
        };
        // redirect from add event page delete or cancel or event order
        if ($localStorage.eventredirectfromaddevent || $localStorage.eventdeleteredirect === 'Y' || $localStorage.redirectedfrmtemplate || $localStorage.page_from === "events_order" || $localStorage.page_from === "events_cancel") {
            $scope.liveEventList = [];
            $scope.pastEventList = [];
            $scope.draftEventList = [];
            if ($localStorage.page_from === "events_order" || $localStorage.page_from === "events_cancel") {
                $scope.current_Order_event_id = $localStorage.currentEventid;
                $scope.current_EventDetails = $localStorage.currentManageEventDetails;
                $scope.current_ChildArray = $localStorage.currentChildArray;
                $localStorage.page_from = $localStorage.currentOrderDetails = $localStorage.currentChildArray = $localStorage.currentManageEventDetails = $localStorage.currentEventid = '';

                $scope.current_order_event_id = $scope.current_Order_event_id;
                if ($scope.current_EventDetails.event_type === 'S') {
                    $scope.manageevent($scope.current_EventDetails,$scope.current_EventDetails.event_status);
                } else {
                    $scope.manageevent($scope.current_EventDetails,$scope.current_EventDetails.event_status);
                    if ($scope.current_ChildArray.length > 0) {                    
                        for (i = 0; i < $scope.current_ChildArray.length; i++) {
                            if ($scope.current_order_event_id === $scope.current_ChildArray[i].event_id) {
                                $scope.manageevent($scope.current_ChildArray[i],'child');
                            }
                        }
                    }
                }

            } else if ($localStorage.eventdeleteredirect === 'Y'){
                $("#DeleteSMEventModal").modal('show');
                $("#DeleteSMEventtitle").text('Message');
                $("#DeleteSMEventcontent").text('Event successfully deleted');
                $localStorage.eventdeleteredirect = 'N';
                if($localStorage.eventredirectfromaddevent === 'draft')
                $scope.draftlistview();
                else if ($localStorage.eventredirectfromaddevent === 'past') 
                $scope.pastlistview();
                else 
                $scope.livelistview();
            }else if($localStorage.redirectedfrmtemplate) {
                $scope.draftlistview();
                $("#msgModal").modal('show');
                $("#msgtitle").text('Message');
                $("#msgcontent").text('Event successfully copied inside "Draft" folder');
                $localStorage.redirectedfrmtemplate = false;
            }else{
                if ($localStorage.eventredirectfromaddevent === 'draft') 
                    $scope.draftlistview();
                else if ($localStorage.eventredirectfromaddevent === 'past') 
                    $scope.pastlistview();
                else 
                    $scope.livelistview();
            } 
        }else{
            $scope.livelistview();
        }
        
    } else {
        $location.path('/login');
    }

}
module.exports = ManageEventController;