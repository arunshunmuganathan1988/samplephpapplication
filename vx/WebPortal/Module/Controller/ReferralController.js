ReferralController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$rootScope','$filter', '$route'];
function ReferralController ($scope, $location, $http, $localStorage, urlservice, $rootScope, $filter, $route) {
//          alert('Referral controller');
        
        if ($localStorage.islogin == 'Y') {
            
        angular.element(document.getElementById('sidetopbar')).scope().getName();
        angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
        angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
        angular.element(document.getElementById('sidetopbar')).scope().regComplete(); 
        angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
        $rootScope.sideBarHide = false;
        $rootScope.activepagehighlight = 'referral';
        $("#datatable-responsive").DataTable();
        $scope.referralmsg = $localStorage.loginDetails.referral_message;
        $scope.currentPage = 0;
        $scope.pageSize = 5;
        $scope.lastFilterNumber = $scope.pageSize;
        $scope.referrallist = [];
        $localStorage.currentpage = "referrals";
        $localStorage.PreviewEventNamee="";
        
//        if($localStorage.subscription_status === 'N' && $localStorage.upgrade_status === 'F'){
//            $scope.referralPremiumCheck = true; 
//            $scope.referralPremiumDone = false;            
//        }else{
//            $scope.referralPremiumDone = true; 
//            $scope.referralPremiumCheck = false; 
//        }
        
           //First time login message Pop-up ----START 
            tooltip.hide();
//            $scope.referraltip = true;
            
            if($localStorage.verfication_complete !== 'U'){
                if($localStorage.firstlogin){
                    if($localStorage.firstlogin_referral === 'Y'){
                    $("#tooltipfadein").css("display", "block");
                    $localStorage.firstlogin_referral = 'N';
                    $scope.referraltip = false; 
                    tooltip.pop("referrallink", "#refraltip", {position:1});
                    }
                }
            }
            
            $("#referraldetailNext").click(function(){   
                $("#tooltipfadein").css("display", "none");
                $scope.referraltip = true; 
                tooltip.hide();
            });
            //First time login message Pop-up ----END
        
        //Previewscroll             
            $scope.topreview=function(){
                document.getElementById('previewapp').scrollIntoView();
            };  
            
            $scope.catchReferralMsg = function(){
            $localStorage.PreviewReferralMsg = $scope.referralmsg;
            };
        
        $scope.getReferralDetails = function(){
            $('#progress-full').show(); 
           $http({
                   method: 'GET',
                   url: urlservice.url+'getReferralDetails',
                   params: {
                    "company_id":$localStorage.company_id
//                    "emailid": $localStorage.usrname,
//                    "password": $localStorage.pswdname
                   },
                   headers:{"Content-Type":'application/json; charset=utf-8',},
                   withCredentials: true  
               
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                        $('#progress-full').hide(); 
                        $scope.referrallist = response.data.msg.referral_details;
//                        $location.path('/referral');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        console.log(response.data);
                        $('#progress-full').hide();
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getReferralDetails();
            
            
        $scope.numberOfPages = function () {  
                if($scope.lastFilterNumber != $scope.pageSize){
                    $scope.currentPage=0;
                }
                $scope.lastFilterNumber = $scope.pageSize;                   
                $scope.lastpage = Math.ceil($scope.studentlist.length / $scope.pageSize);
                return Math.ceil($scope.studentlist.length / $scope.pageSize);
                
            }; 
        
        $scope.postReferralMessage = function(){
                $('#progress-full').show(); 
                
                $http({
                method: 'POST',
                url: urlservice.url+'updateReferralMessage',
                data:{
                    'referral_message':$scope.referralmsg,
                    'company_id':$localStorage.company_id,
                    'user_id':$localStorage.user_id
//                    'emailid': $localStorage.usrname,
//                    'password': $localStorage.pswdname
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                    if(response.data.status == 'Success'){
                       $('#progress-full').hide(); 
                       console.log(response.data);
                       $localStorage.loginDetails = response.data.msg;                       
                       if($localStorage.firstlogin &&  $localStorage.firstlogin_referral === 'Y'){
                            $localStorage.firstlogin_referral = 'N';
                            angular.element(document.getElementById('sidetopbar')).scope().getTutorialMode();
                        }else if($localStorage.firstlogin_referral === 'N' || $localStorage.firstlogin_referral === undefined ){
                           $("#messageModal").modal('show');
                           $("#messagetitle").text('Message');
                           $("#messagecontent").text('Studio Referral Message successfully updated');
                           $location.path('/referral');
                        }
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else{
                        $scope.handleFailure(response.data.msg);
                    }   
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
        
            
        } else {
            $location.path('/login');
        }

    }
    module.exports = ReferralController;




