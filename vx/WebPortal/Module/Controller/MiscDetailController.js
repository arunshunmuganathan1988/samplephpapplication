/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
MiscDetailController.$inject=['$scope', '$location', '$http', '$localStorage', 'urlservice', '$filter', '$rootScope'];
 function  MiscDetailController($scope, $location, $http, $localStorage, urlservice, $filter, $rootScope)
    {
        if ($localStorage.islogin === 'Y') {
//            angular.element(document.getElementById('sidetopbar')).scope().getName();
//            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
//            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
//            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            $rootScope.sideBarHide = false;
            $rootScope.activepagehighlight = 'home';
            $scope.wp_currency_symbol = $localStorage.wp_currency_symbol;
            $scope.applypaymentcredit_option = 'MC';
            $scope.misctabview = "P";//p- payment ,n-note
            tooltip.hide(); 
            
            $scope.resettabbarclass = function () {
                $('.active-event-list').addClass('event-list-title').removeClass('active-event-list');
                $('.greentab-text').removeClass('greentab-text-active');
            };
            
            $scope.miscpayments = function(){
                $scope.misctabview = "P";
                $scope.getpaymentdetails();
                $scope.resettabbarclass();
                $('#li-miscpayment').addClass('active-event-list').removeClass('event-list-title'); 
                $('.trialdetail-tabtext-1').addClass('greentab-text-active');
            }
            $scope.miscnotes = function(){
                $scope.misctabview = "N";
                $scope.getnotesdetails();
                $scope.resettabbarclass();
                $('#li-miscnote').addClass('active-event-list').removeClass('event-list-title'); 
                $('.trialdetail-tabtext-2').addClass('greentab-text-active');
            }
            
            $scope.editmiscNote = function (ind, hist_id, action) {
                if(action === 'add'){
                    $scope.misc_edit_status = 'add';
                    $scope.edited_misc_history_id = '';
                    $scope.mischistorynote = '';
                    $scope.miscHistoryNoteButton = 'Add';
                    $("#miscHistoryNoteModal").modal('show');
                    $("#miscHistoryNotetitle").text('Add Note');                    
                }else if(action === 'edit'){
                    $scope.edited_misc_history_id = hist_id ;
                    $scope.misc_edit_status = 'update';
                    $scope.miscHistoryNoteButton = 'Update';
                    $scope.mischistorynote = $scope.misc_notes[ind].activity_text;
                    $("#miscHistoryNoteModal").modal('show');
                    $("#miscHistoryNotetitle").text('Update Note');
                }else if(action === 'delete'){
                    $scope.edited_misc_history_id = hist_id ;
                    $scope.mischistorynote =  '';
                    $scope.misc_edit_status = 'delete';
                    $("#miscHistoryNoteDeleteModal").modal('show');
                    $("#miscHistoryNoteDeletetitle").text('Delete Note');
                    $("#miscHistoryNoteDeletecontent").text('Are you sure want to delete the note?');
                }
            };
            
            $scope.confirmmisceditHistoryNote  =function(){
                $("#miscHistoryNoteModal").modal('hide');
                $("#miscHistoryNoteDeleteModal").modal('hide');
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'updatemiscnotes',
                    data: {
                        "company_id": $localStorage.company_id,
                        "note_id": $scope.edited_misc_history_id,
                        "type":$scope.misc_edit_status,
                        "misc_order_id": $localStorage.currentmiscorderid,
                        "note_text": $scope.mischistorynote
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',},
                    withCredentials: true

                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.misc_notes = response.data.misc_notes;
                    }else if(response.data.status === 'Failed'){
                        console.log(response.data);
                        $('#progress-full').hide();         
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg); 
                    } else if (response.data.status === 'Expired') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);

                });
            }
            
            //get payment details
            $scope.getpaymentdetails = function () { 
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getmiscpaymentHistoryDetails',
                    params: {
                        "company_id": $localStorage.company_id,
                        "misc_order_id":$localStorage.currentmiscorderid
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.misc_payment_history = response.data.payment_history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            $scope.getpaymentdetails();
            
            //notes get
            
            $scope.getnotesdetails = function () { 
                $('#progress-full').show();
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getmiscnotes',
                    params: {
                        "company_id": $localStorage.company_id,
                        "misc_order_id":$localStorage.currentmiscorderid
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $scope.misc_notes = response.data.misc_notes;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.opencreditmodal = function(applycredit_payment_id){
                $scope.applycredit_payment_id = applycredit_payment_id;
                $("#paymentHistory-manualcredit").modal('show');
            }
            $scope.applypaymentcredit = function(applycredit_payment_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'applypaymentcreditformisc',
                    data: {
                        "company_id": $localStorage.company_id,
                        "misc_payment_id":applycredit_payment_id,
                        "applycredit_payment_method":$scope.applypaymentcredit_option,
                        "misc_order_id":$localStorage.currentmiscorderid
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#paymentHistory-manualcredit").modal('hide');
                        $scope.misc_payment_history = response.data.payment_history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.refundconfirmation = function(payment_id,payment_method){
                $scope.refund_payment_id = payment_id;
                $scope.refund_payment_method = payment_method;
                $("#miscrefundmodal").modal('show');
                $("#miscrefundmodaltitle").text('Refund');
                $("#miscrefundmodalcontent").text('Are you sure want to refund this payment?');
            }
            
            $scope.refundmisc = function(paymentid,paymentmethod){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'refundmisc',
                    data: {
                        "company_id": $localStorage.company_id,
                        "misc_payment_id":paymentid,
                        "refund_payment_method":paymentmethod,
                        "misc_order_id":$localStorage.currentmiscorderid
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#paymentHistory-manualcredit").modal('hide');
                        $scope.misc_payment_history = response.data.payment_history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.rerunmiscpayment = function(payment_id){
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'reRunMiscPayment',
                    data: {
                        "company_id": $localStorage.company_id,
                        "payment_id":payment_id,
                        "reg_id":$localStorage.currentmiscorderid
                    },
                    headers: {
                            "Content-Type": 'application/json; charset=utf-8',
                        },
                        withCredentials: true
                }).then(function (response) {
                    if (response.data.status == 'Success') {
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text('Payment Successful.');
                        $scope.misc_payment_history = response.data.payment_history;
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();
                    } else {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            }
            
            $scope.closemiscDetailView = function(){
                $location.path('/payment');
            }
        } else {
            $location.path('/login');
        }
    }
    module.exports =   MiscDetailController;