TemplateController.$inject=['$scope', '$location', '$http','$route' ,'$localStorage', 'urlservice', '$filter','$rootScope'];
 function TemplateController ($scope, $location, $http,$route, $localStorage, urlservice, $filter,$rootScope) {
        
        if ($localStorage.islogin === 'Y') {
            angular.element(document.getElementById('sidetopbar')).scope().getName();
            angular.element(document.getElementById('sidetopbar')).scope().getCurriculumLevel();
            angular.element(document.getElementById('sidetopbar')).scope().removeAllActive();
            angular.element(document.getElementById('sidetopbar')).scope().checkstudioexpired();
            angular.element(document.getElementById('sidetopbar')).scope().convCountCall();
            $rootScope.activepagehighlight = 'events';
             //PREVIEW SINGLE EVENT ADD
           $localStorage.PreviewEventName = '';
           $localStorage.PreviewEventDate = '';
           $localStorage.PreviewEventTime = '';
           $localStorage.PreviewEventEndDate = '';
           $localStorage.PreviewEventEndTime = '';
           $localStorage.PreviewEventCost = '';
           $localStorage.PreviewEventCompareCost = '';
           $localStorage.PreviewEventUrl = '';
           $localStorage.PreviewEventDesc = '';
           $localStorage.PreviewImageUrl =  '';
           $localStorage.PreviewECapacity = '';
           $localStorage.PreviewESpaceRemaining = '';
           $localStorage.PreviewEProcesFeeSelection = '';
           
            //PREVIEW MULTIPLE- PARENT EVENT ADD
           $localStorage.PreviewMEventCategory = '';
           $localStorage.PreviewMEventSubcategory = '';
           
            $localStorage.PreviewPEventDesc = '';
            $localStorage.PreviewPEventUrl = '';
            $localStorage.PreviewPImageUrl = '';
            $localStorage.PreviewEditPImageUrl = '';
            
           //PREVIEW EXISTING CHILD EVENT EDIT
           $localStorage.PreviewCfirstedit = '';
           $localStorage.childeventindex = '';
           $localStorage.PreviewCEventDate = '';
           $localStorage.PreviewCEventTime = '';
           $localStorage.PreviewCEventEndDate = '';
           $localStorage.PreviewCEventEndTime = '';
           $localStorage.PreviewCEventCost = '';
           $localStorage.PreviewCEventCompareCost = '';
           $localStorage.PreviewCEventUrl = '';
           $localStorage.PreviewCEventUrlEdit = '';
           $localStorage.PreviewCEventDescEdit =  '';
           $localStorage.PreviewCEventName = '';
           $localStorage.PreviewCECapacity = '';
           $localStorage.PreviewCESpaceRemaining = '';
           $localStorage.PreviewCEProcesFeeSelection = '';
           $localStorage.PreviewCfirstedit = 'N';
           
           //PREVIEW NEW CHILD EVENT ADD
            $localStorage.PreviewMEventName = '';           
            $localStorage.PreviewMEventDate = '';
            $localStorage.PreviewMEventTime = '';
            $localStorage.PreviewMEventEndDate = '';
            $localStorage.PreviewMEventEndTime = '';
            $localStorage.PreviewMEventUrl = '';
            $localStorage.PreviewMEventDesc = '';
            $localStorage.PreviewMEventCost = '';
            $localStorage.PreviewMEventCompareCost = '';
            $localStorage.preview_eventchildlist =  $localStorage.preview_childlisthide = false;
            $localStorage.PreviewMECapacity = '';
            $localStorage.PreviewMESpaceRemaining = '';
            $localStorage.PreviewChildEventList = '';
            
            $localStorage.PreviewOldRegistrationField='';
            $localStorage.PreviewOldRegFieldMandatory= '';
            $localStorage.preview_reg_col_name_edit = '';
            $localStorage.preview_reg_fieldindex = '';
            $localStorage.PreviewWaiverText = '';
            $localStorage.currentpreviewtab = '';
            
            $localStorage.templateEvents = '';
            $localStorage.currentpage = 'Template';
          
            $scope.datetimestring = function (ddate) {
                var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                        navigator.userAgent && !navigator.userAgent.match('CriOS');
                if (isSafari)
                {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var safaritimecheck = ddate.split(" ");
                        if (safaritimecheck[1] === "00:00:00") {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var time = new Date(dda);
                            var dateonly = time.toString();
                            var fulldateonly = dateonly.replace(/GMT.*/g, "");
                            var localdate = fulldateonly.slice(0, -9);
                            return localdate;
                        } else {
                            var dda = ddate.toString();
                            dda = dda.replace(/ /g, "T");
                            var date = new Date(dda).toUTCString();   // For date in safari format
                            var datealone = date.slice(0, 13);
                            var time = date.slice(17, 25);
                            // format AM PM start
                            var hours = time.slice(0, 2);
                            var minutes = time.slice(3, 5);
                            var ampm = hours >= 12 ? 'PM' : 'AM';
                            hours = hours % 12;
                            hours = hours ? hours : 12; // the hour '0' should be '12'
                            var localTime = hours + ':' + minutes + ' ' + ampm;
                            var localdatetime = datealone + "  " + localTime;
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                } else
                {
                    if (ddate !== '0000-00-00 00:00:00') {
                        var timecheck = ddate.split(" ");
                        if (timecheck[1] === "00:00:00") {
                            var ddateonly = timecheck[0] + 'T00:00:00+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdate = $filter('date')(newDate, 'EEE, MMM d h:mm a');
                            return localdate;
                        } else {
                            var ddateonly = timecheck[0] + 'T' + timecheck[1] + '+00:00';
                            var newDate = new Date(ddateonly.replace('T', ' ').slice(0, -6));
                            var localdatetime = $filter('date')(newDate, 'EEE, MMM d h:mm a');
                            return localdatetime;
                        }
                    } else {
                        var localdatetime = "";
                        return localdatetime;
                    }
                }
            }
            
            $scope.template=function(){
                  $http({
                    method: 'POST',
                    url: urlservice.url + 'getEventTemplate',
                    data: {
                     "company_id": $localStorage.company_id,
                    },
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    withCredentials: true
                }).then(function (response) {
                    if (response.data.status === 'Success') {  
                       $localStorage.templateEvents = $scope.templateevent = response.data.msg.live;
                       $localStorage.currentpage = "Template";
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }else{
                        console.log(response.data);
                        $('#progress-full').hide();
                    }
                }, function (response) {
                    console.log(response.data);
                    $scope.handleError(response.data);
                });
            };
            
            $scope.template();
            
            $scope.tempcopyevent = function(evnt_id){
                $('#progress-full').show(); 
                
                $http({
                method: 'POST',
                url: urlservice.url+'copyevents',
                
                data:{
                    'event_id':evnt_id,
                    'copy_event_type':'Main',
                    'event_template_flag':'Y',
                    'company_id':$localStorage.company_id
                },
                headers:{
                "Content-Type":'application/json; charset=utf-8',
                },
                withCredentials: true       
                }).then(function (response) {
                   if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $localStorage.redirectedfrmtemplate = true;
//                        $scope.eventlist = response.data.msg.live;
                        $location.path('/manageevent');
                    }else if(response.data.status === 'Expired'){
                        console.log(response.data);
                        $('#progress-full').hide();
                        $("#messageModal").modal('show');
                        $("#messagetitle").text('Message');
                        $("#messagecontent").text(response.data.msg);
                        $scope.logout();  
                    }else if(response.data.status === 'Failed'){
                        $('#progress-full').hide(); 
//                        $scope.eventlist = response.data.msg;
                        $scope.handleFailure('Event duplicate copy failed to add');
                    }else if (response.data.status === 'Version') {
                        $('#progress-full').hide();
                        $scope.handleFailure(response.data.msg);
                    }
                    }, function (response) {
                        console.log(response.data);
                        $scope.handleError(response.data);
                    });  
            };
            $scope.addEvent = function () {
                $scope.alleditClosed();
                $scope.menuhide();
                $localStorage.eventpagetype = 'new';
                $localStorage.eventlocalSingledata = "";
                $localStorage.event_details = "";
                $localStorage.isAddedEventDetails = "N";
                $localStorage.currentpage = "addevents";
                $localStorage.preview_eventcontent = '';
                $location.path('/addevent');
            };
            
        } else {
            $location.path('/login');
        }
    }
    module.exports =  TemplateController;







