<?php
$ms_url=$ms_version='';
 
if(strpos( $_SERVER['HTTP_HOST'], "dev.mystudio.academy") !== false){  //Development
    $ms_url = 'http://dev.mystudio.academy/';
    $ms_version = 'v35';       
}elseif(strpos( $_SERVER['HTTP_HOST'], "dev2.mystudio.academy") !== false){  //Development
    $ms_url = 'http://dev2.mystudio.academy/';
    $ms_version = 'v35';       
}elseif(strpos( $_SERVER['HTTP_HOST'], 'stage.mystudio.academy') !== false){  //Stage
    $ms_url = 'http://stage.mystudio.academy/';
    $ms_version = 'v35';
}elseif( strpos($_SERVER['HTTP_HOST'], 'beta.mystudio.academy') !== false){  //Stage
    $ms_url = 'https://beta.mystudio.academy/';
    $ms_version = 'v35';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'mystudio.academy')!== false){ //Production
    $ms_url = 'https://www.mystudio.academy/';
    $ms_version = 'v35';
}else{
   $ms_url = $_SERVER['REQUEST_URI'];

 }
 header('Location: '.$ms_url.$ms_version.'/WebPortal/#/login');
 ?>