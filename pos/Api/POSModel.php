<?php

require_once __DIR__."/../../Globals/cont_log.php";
require_once 'PHPMailer_5.2.1/class.phpmailer.php';
require_once 'PHPMailer_5.2.1/class.smtp.php';

class POSModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $server_url;

    public function __construct() {
        require_once __DIR__."/../../Globals/config.php";
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev.mystudio.academy";
        }elseif(strpos($host, 'dev2.mystudio.academy') !== false){  //Development
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif(strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://').$_SERVER['SERVER_NAME'];
        }else{
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
//        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
//        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    /* login functionality using email_id */
    protected function checklogin($email, $password, $company_id, $studio_code) {
        $this->verifyCompanyDetails($company_id, $studio_code, $email);
        $password = md5($password);
        $sql = sprintf("SELECT user_id,user_email,user_password,u.company_id,u.deleted_flag, c.upgrade_status, c.studio_expiry_level
                    from user u LEFT JOIN `company` c ON u.`company_id`=c.`company_id`
                    where user_email='%s' AND u.company_id='%s' limit 0,1", mysqli_real_escape_string($this->db,$email), mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db, $sql);
//        $company_id = "";
        $user_id = "";

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    $out['company_id'] = $row['company_id'];
                    $out['user_email'] = $row['user_email'];
                    $company_id = $row['company_id'];
                    $user_id = $row['user_id'];
                    $pwd = $row['user_password'];
                    $deleted_flag = $row['deleted_flag'];
                    $out['upgrade_status'] = $row['upgrade_status'];
                    $out['studio_expiry_level'] = $row['studio_expiry_level'];
                }
                if($pwd!=$password){
                    $error = array('status' => "Failed", "msg" => "Username or password doesn't match our records.");
                    $this->response($this->json($error), 401);       // If "Company id" not available
                }
                if($deleted_flag=='Y'){
                    $error = array('status' => "Failed", "msg" => "User account doesn't exist.");
                    $this->response($this->json($error), 401);       // If "Company id" not available
                }
                $msg = array('status' => "Success", "msg" => $out);
                $this->response($this->json($msg), 200);       // If "Company id" not available
            } else {
                $error = array('status' => "Login Failed", "msg" => "Username or password doesn't match our records.");
                $this->response($this->json($error), 401);
            }
        }
    }
    
    private function verifyCompanyDetails($company_id, $studio_code, $email){
        $query = sprintf("SELECT 1 FROM `company` c JOIN `user` u USING (company_id) WHERE c.`company_id`='%s' AND c.`company_code`='%s' AND u.`user_email`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $studio_code), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    private function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function generateToken($company_id, $email){
        $logo_url = '';
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);           
            $token = str_replace( '/', '', base64_encode(openssl_random_pseudo_bytes(3 * (64 >> 2))));            
            if($num_rows>0){
                $query2 = sprintf("UPDATE `pos_token` SET `token`='%s' WHERE `company_id`='%s' AND `user_email`='%s'", 
                        mysqli_real_escape_string($this->db, $token), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
            }else{
                $query2 = sprintf("INSERT INTO `pos_token`( `company_id`, `user_email`, `token`) VALUES ('%s', '%s', '%s')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email), mysqli_real_escape_string($this->db, $token));
            }
            $result2 = mysqli_query($this->db, $query2);                
            if(!$result2){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $get_logo = sprintf("SELECT  `company_name`, `logo_URL`, upgrade_status, studio_expiry_level  FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
                $res_logo = mysqli_query($this->db, $get_logo);
                if(!$res_logo){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    $num_rows2 = mysqli_num_rows($res_logo);
                    if($num_rows2>0){
                        $row = mysqli_fetch_assoc($res_logo);
                        if(!empty(trim($row['logo_URL']))){
                            $logo_url = $row['logo_URL'];
                        }else{
                            $logo_url = $this->server_url."/uploads/Default/default_logo.png";
                        }
                        $upgrade_status = $row['upgrade_status'];
                        $studio_expiry_level = $row['studio_expiry_level'];
                    }
                }
                $msg = array("status"=>"Success", "msg"=>"$token", "logoURL"=>"$logo_url", "upgrade_status"=>"$upgrade_status", "studio_expiry_level"=>$studio_expiry_level);
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    protected function verifyStudioPOSToken($company_id, $email, $token) {
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['pos_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `pos_token` SET `no_of_access`=`no_of_access`+1 WHERE `pos_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                    return $msg;
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                    $this->response($this->json($msg), 200);
                }
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                $this->response($this->json($msg), 200);
            }
        }
    }

    public function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Success", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function getStudioPOSStatuses($company_id){
        $query = sprintf("SELECT `misc_charge_status`, `retail_status`, `event_status`, `membership_status`, `trial_status`, `leads_status`, UPPER(`event_name`) event_name, UPPER(`trial_name`) trial_name, UPPER(`membership_name`) membership_name, UPPER(`retail_name`) retail_name  FROM `company` c JOIN `studio_pos_settings` s ON s.`company_id`=c.`company_id` WHERE s.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $out = mysqli_fetch_assoc($result);
                $res = array('status' => "Success", "msg" => $out);
            }else{
                $res = array('status' => "Failed", "msg" => "Studio POS settings not yet assigned.");
            }
            $this->response($this->json($res), 200);
        }
    }
    public function sendExpiryEmailDetailsToStudio($company_id){
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];
                
                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br><br>";
                $body_msg .= '<center><a href="'.$this->server_url.'/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmailForStudioExpiry($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }                
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }
    }
    public function sendEmailForStudioExpiry($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if($isHtml){                
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }
    
}

?>
