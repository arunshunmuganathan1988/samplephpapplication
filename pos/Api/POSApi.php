<?php

require_once 'POSModel.php';

class POSApi extends POSModel {
    
    public function __construct(){
        parent::__construct();          // Init parent contructor
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        $company_id = $token = $email = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $email = $this->_request['pos_email'];
        }
        if($company_id==6){
            $error = array('status' => "Failed", "msg" => "No participants available.");
            $this->response($this->json($error), 404);
        }
        if((int)method_exists($this,$func) > 0){
            if($_REQUEST['run']=='generateStudioPOSToken' || $_REQUEST['run']=='login' || $_REQUEST['run']=='sendExpiryEmailToStudio'){
                $this->$func();
            }else{
                $verify = $this->verifyStudioPOSToken($company_id, $email, $token);
                if($verify['status']=='Success'){
                    $this->$func();
                }else{
                    $error = array('status' => "Failed", "msg" => $verify['msg']);
                    $this->response($this->json($error), 200);
                }
            }
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
    
    private function login() {
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $password = $email = $company_id = $studio_code = '';
        
        if (isset($this->_request['email'])) {
            $email = $this->_request['email'];
        }
        if (isset($this->_request['password'])) {
            $password = $this->_request['password'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['studio_code'])) {
            $studio_code = $this->_request['studio_code'];
        }

        // Input validations
        if (!empty($email) && !empty($password)) {
            $this->checklogin($email, $password, $company_id, $studio_code);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function generateStudioPOSToken() {
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $token = $email = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        if(isset($this->_request['pos_email'])){
            $email = $this->_request['pos_email'];
        }
        
        if(!empty($company_id) && !empty($email)){
            $this->generateToken($company_id, $email);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getPOSEnableStatus(){
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id = $token = $email = '';
        if(isset($this->_request['company_id'])){
            $company_id = $this->_request['company_id'];
        }
        
        if(!empty($company_id)){
            $this->getStudioPOSStatuses($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    
    private function getwepaystatus(){
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id='';
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        
        if(!empty($company_id)){
            $this->wepayStatus($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    }
    private function sendExpiryEmailToStudio(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 400);
        }
    } 
    
} 

// Initiiate Library
    $api = new POSApi;
    $api->processApi();



?>