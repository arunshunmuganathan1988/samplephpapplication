//declare modules

angular.module("home", []);

$('#progress-full').hide();

angular.module("myapp", ['ngRoute', 'home', 'angular-md5', 'ngStorage', 'dndLists'])

        .config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
                $routeProvider
                        .when('/home', {
                            templateUrl: 'Module/View/Home.html',
                            controller: 'HomeController'
                        })
//                        .otherwise({
//                            templateUrl: 'index.html',
//                            controller: 'IndexPageController'
//                        });

            }]);

angular.module('myapp').controller("myCtrl", ['$scope', '$http', '$location', '$route', '$rootScope', '$localStorage', '$sessionStorage', 'urlservice', function ($scope, $http, $location, $route, $rootScope, $localStorage, $sessionStorage, urlservice) {
        
        $scope.studio_AuthenticationStatus = true;
        $scope.pos_token = "";
        $scope.studio_Loginmsg = "";
        $scope.studio_LoginStatus = true;

        $scope.getTokenAccess = function () {
            if ($scope.company_id && $scope.user_email_id) {
                $('#progress-full').show();
                $http({
                    method: 'POST',
                    url: urlservice.url + 'generateStudioPOSToken',
                    data: {
                        "company_id": $scope.company_id,
                        "studio_code": $scope.studio_code,
                        "pos_email": $localStorage.pos_user_email_id
                    },
                    headers: {"Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'}
                }).then(function (response) {
                    if (response.data.status === 'Success') {
                        $('#progress-full').hide();
                        $scope.studio_upgrade_status = response.data.upgrade_status;
                        $scope.studio_expiry_level = response.data.studio_expiry_level;
                        if($scope.studio_upgrade_status === 'B'){
                            $("#messageexpiryModal").modal('show');
                            $("#messageexpirycontent").text('This feature is available with our Premium and White Label platform.');
                        }else if ($scope.studio_upgrade_status === 'F' || $scope.studio_expiry_level === 'L2') {
                            $("#studioexpiryModal").modal('show');
                            $("#studioexpirycontent").text('Please contact the business operator to activate this link.');
                        }else{
                            $localStorage.isPOSLogin = 'Y';
                            $scope.pos_token = response.data.msg;
                            $localStorage.company_logo = response.data.logoURL;
                            $location.path('/home');
                        }
                    } else {
                        $('#progress-full').hide();
                        $sessionStorage.isPOSLogin = 'N';
                        $scope.loginuseremail = $scope.loginpassword = '';
                        $scope.studio_LoginStatus = true;
                        $scope.login.$setPristine();
                        $scope.login.$setUntouched();
                        $("#loginmodal").modal('show');
                    }
                }, function (response) {
                    $('#progress-full').hide();
                    console.log(response.data);
                });
            } else {
                $('#progress-full').hide();
                $sessionStorage.isPOSLogin = 'N';
                $scope.loginuseremail = $scope.loginpassword = '';
                $localStorage.pos_user_email_id = '';
                $scope.studio_LoginStatus = true;
                $scope.login.$setPristine();
                $scope.login.$setUntouched();
                $("#loginmodal").modal('show');
            }
        };

        $scope.posLogin = function () {
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'login',
                data: {
                    "email": $scope.loginuseremail,
                    "password": $scope.loginpassword,
                    "company_id": $scope.company_id,
                    "studio_code": $scope.studio_code
                },
                headers: {"Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}
            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#loginmodal").modal('hide');
                    $scope.studio_upgrade_status = response.data.msg.upgrade_status;
                    $scope.studio_expiry_level = response.data.msg.studio_expiry_level;
                    $localStorage.pos_company_id = response.data.msg.company_id;
                    $localStorage.pos_user_email_id = response.data.msg.user_email;
                    $scope.company_id = $localStorage.pos_company_id;
                    $scope.user_email_id = $localStorage.pos_user_email_id;                    
                    if($scope.studio_upgrade_status === 'B'){
                        $("#messageexpiryModal").modal('show');
                        $("#messageexpirycontent").text('This feature is available with our Premium and White Label platform.');
                    }else if ($scope.studio_upgrade_status === 'F' || $scope.studio_expiry_level === 'L2') {
                        $("#studioexpiryModal").modal('show');
                        $("#studioexpirycontent").text('Please contact the business operator to activate this link.');
                    }else{
                        $sessionStorage.isPOSLogin = 'Y'; 
                        $scope.getTokenAccess();
                    }
                } else {
                    $('#progress-full').hide();
                    console.log(response.data);
                    $sessionStorage.isPOSLogin = 'N';
                    if (response.data.msg === "Session expired.") {
                        $("#indexMessageModal").modal('show');
                        $("#indexMessagecontent").text(response.data.msg);
                    } else {
                        $scope.studio_Loginmsg = response.data.msg;
                        $scope.studio_LoginStatus = false;
                    }
                }
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        };

        $scope.posurlredirection = function () {
            $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 ='';
            var decoded_url = decodeURIComponent(window.location.href);
            if (decoded_url.indexOf('?=') > -1) {
                var params = decoded_url.split('?=')[1].split('/');
                $scope.param1 = params[0];  // company code
                $scope.param2 = params[1];  // company_id
                $scope.param3 = params[2].split('-')[0];  // staff (or) public pos
                if(params[2]){
                    $scope.encrypted_data = params[2].split('-')[1];
                }else{
                    $scope.encrypted_data = "";
                }                
                $scope.param4 = params[3];  // launcher (or) copy from clipboard
                $localStorage.pos_company_id = $scope.param2;
                $scope.studio_code = $scope.param1;
            } else {
                $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 ='';
            }
            $scope.company_id = $localStorage.pos_company_id;
            $scope.user_email_id = $localStorage.pos_user_email_id;
            $scope.pos_user_type = $scope.param3;            
            
            if ($scope.encrypted_data && ($scope.encrypted_data.indexOf('spos') > -1 || $scope.encrypted_data.indexOf('ppos') > -1 )) {
                if ($scope.param4.indexOf('l') > -1) {  // FOR POS LAUNCH 
                    $scope.pos_entry_type = 'l';
                    $scope.getTokenAccess();
                } else {   
                    $scope.pos_entry_type = 'c';// FOR POS URL COPY AND GO                    
                    if ($sessionStorage.isPOSLogin === 'Y') {  
                        $scope.getTokenAccess();
                    } else {      
                        $('#progress-full').hide();
                        $sessionStorage.isPOSLogin = 'N';
                        $scope.loginuseremail = $scope.loginpassword = '';
                        $scope.studio_LoginStatus = true;
                        $scope.login.$setPristine();
                        $scope.login.$setUntouched();              
                        $("#loginmodal").modal('show');
                    }
                }
            }else{  
                $scope.pos_entry_type = 'c';
                $('#progress-full').hide();
                $sessionStorage.isPOSLogin = 'N';
                $scope.loginuseremail = $scope.loginpassword = '';
                $scope.studio_LoginStatus = true;
                $scope.login.$setPristine();
                $scope.login.$setUntouched();              
                $("#loginmodal").modal('show');
            }
        };
        
        $scope.studioUpgradeNow = function () {
            $localStorage.viewselectplanpage = true;
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy') {
                hosturl = 'https://www.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'www1.mystudio.academy') {
                hosturl = 'https://www1.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'www2.mystudio.academy') {
                hosturl = 'https://www2.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'prod.mystudio.academy') {
                hosturl = 'https://prod.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'dev.mystudio.academy') {
                 hosturl = 'http://dev.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'dev2.mystudio.academy') {
                 hosturl = 'http://dev2.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'stage.mystudio.academy') {
                hosturl = 'http://stage.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'beta.mystudio.academy') {
                hosturl = 'https://beta.mystudio.academy/mystudioapp.php?pg=billingstripe';
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + project_location;
            } else {
                hosturl = 'http://' + window.location.hostname + project_location;
            }
            window.open(hosturl,"_self");
        };
        $scope.sendExpiryEmailToStudio = function(){
            $('#progress-full').show();
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": $scope.company_id,
                    "pos_email": $localStorage.pos_user_email_id,
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $('#progress-full').hide();
                if (response.data.status === 'Success') {
                    $('#progress-full').hide();
                    $("#studioexpiryModal").modal('hide');
                    $("#indexMessageModal").modal('show');
                    $("#indexMessagecontent").text(response.data.msg);
                }else if(response.data.status === 'Expired'){
                    $('#progress-full').hide();
                    $("#indexMessageModal").modal('show');
                    $("#indexMessagecontent").text(response.data.msg);
                    $scope.logout();  
                }else{ 
                    $scope.handleFailure(response.data.msg);
                }   
            }, function (response) {
                $('#progress-full').hide();
                console.log(response.data);
            });
        };

    }])

        .run(function ($rootScope) {
            $rootScope.online = navigator.onLine ? 'online' : 'offline';
            $rootScope.$apply();

            if (window.addEventListener) {
                window.addEventListener("online", function () {
                    $rootScope.online = "online";
                    $rootScope.$apply();
                }, true);
                window.addEventListener("offline", function () {
                    $rootScope.online = "offline";
                    $rootScope.$apply();
                }, true);
            } else {
                document.body.ononline = function () {
                    $rootScope.online = "online";
                    $rootScope.$apply();
                };
                document.body.onoffline = function () {
                    $rootScope.online = "offline";
                    $rootScope.$apply();
                };
            }
        })

        .directive('valueRepeatComplete', function () {
            return function (scope, element, attrs) {
                if (scope.$last) {
                    // iteration is complete, do whatever post-processing
                    $('#progress-full').hide();
                } else {
                    $('#progress-full').show();
                }
            }
        })


        .factory('urlservice', function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            }  else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }  else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }

            return {
                url: hosturl + '/pos/Api/'
            };
        });