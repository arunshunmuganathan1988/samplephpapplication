
var app = angular.module('home', []);
app.controller("HomeController", ['$scope', '$location', '$http', '$localStorage', 'urlservice', '$route', '$rootScope', '$filter', '$timeout', '$window', function ($scope, $location, $http, $localStorage, urlservice, $route, $rootScope, $filter, $timeout, $window) {

        if ($scope.pos_token && $scope.company_id) {
            $scope.company_logo = $localStorage.company_logo;
            $scope.lead_pos_enable = '';
            $scope.retail_pos_enable = '';
            $scope.events_pos_enable = '';
            $scope.trial_pos_enable = '';
            $scope.membership_pos_enable = '';
            $scope.charge_pos_enable = '';
            
                $scope.getPOSEnableStatus = function () {
                    $('#progress-full').show();
                    $http({
                        method: 'POST',
                        url: urlservice.url + 'getPOSEnableStatus',
                        data: {
                            "company_id": $scope.company_id,
                            "pos_email": $localStorage.pos_user_email_id,
                            "token":  $scope.pos_token
                        },
                        headers: {"Content-Type": 'application/json; charset=utf-8',
                            'Access-Control-Allow-Origin': '*'}
                    }).then(function (response) {
                        if (response.data.status === 'Success') {
                            $('#progress-full').hide();                            
                            $scope.lead_pos_enable = response.data.msg.leads_status;
                            $scope.retail_pos_enable = response.data.msg.retail_status;
                            $scope.events_pos_enable = response.data.msg.event_status;
                            $scope.trial_pos_enable = response.data.msg.trial_status;
                            $scope.membership_pos_enable = response.data.msg.membership_status;
                            $scope.charge_pos_enable = response.data.msg.misc_charge_status;
                            $scope.retail_name = response.data.msg.retail_name;
                            $scope.event_name = response.data.msg.event_name;
                            $scope.trial_name = response.data.msg.trial_name;
                            $scope.membership_name = response.data.msg.membership_name;
                        } else {
                            $('#progress-full').hide();
                            console.log(response.data);
                            $("#homeMessageModal").modal('show');
                            $("#homeMessagecontent").text(response.data.msg);
                        }
                    }, function (response) {
                        $('#progress-full').hide();
                        console.log(response.data);
                    });
            };
            $scope.getPOSEnableStatus();
            
            $scope.openurl = function (page) {
                var hosturl;
                    if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
                           window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                    if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                        var origin = window.location.origin;
                        hosturl = origin;
                    }

                } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'http://' + www_url;
                    }
                } else if (window.location.hostname === 'stage.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'http://' + www_url;
                    }
                } else if (window.location.hostname === 'beta.mystudio.academy') {
                    if (window.location.hostname.indexOf("www") > -1) {
                        hosturl = window.location.href.split('/').slice(0, 3).join('/');
                    } else {
                        var www_url = window.location.href.split('/').slice(2, 3).join('/');
                        hosturl = 'https://' + www_url;
                    }
                } else if (window.location.hostname === 'localhost') {
                    hosturl = 'http://' + window.location.hostname + '/nila/mystudio_webapp';
                } else {
                    hosturl = 'http://' + window.location.hostname + '/nila/mystudio_webapp';
                }
                if (page) {
                    $window.open(hosturl+'/' + page + '/?=' + $scope.studio_code + '/' + $scope.company_id + '///' + Math.floor(Date.now() / 1000) + '/' + $scope.pos_user_type + '/' + $scope.pos_token+ '/' + $scope.pos_entry_type, '_self');
                }
            };
            
            //DOCUMENT READY FUNCTION
            angular.element(document).ready(function () {
                if ($scope.pos_user_type && $scope.pos_user_type === 'spos') {
                    $('#pos-home').addClass('with-banner');
                }
            });
        } else {
            $location.path('/');
        }
        
    }]);


app.filter('dateSuffix', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input) {
        if (input !== '0000-00-00') {
            var dtfilter = $filter('date')(input, 'MMMM d');
            var day = parseInt(dtfilter.slice(-2));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            var daysuffix = dtfilter + suffix;

            var currentDate = new Date();
            var current_dtfilter = $filter('date')(currentDate, 'MMMM d');
            var current_day = parseInt(current_dtfilter.slice(-2));
            var current_relevantDigits = (current_day < 30) ? current_day % 20 : current_day % 30;
            var current_suffix = (current_relevantDigits <= 3) ? suffixes[current_relevantDigits] : suffixes[0];
            var current_daysuffix = current_dtfilter + current_suffix;
            return (daysuffix === current_daysuffix) ? 'Today' : daysuffix;
        } else {
            return "";
        }
    };
});

