<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin, X-Token, x-token');
header('Access-Control-Allow-Credentials: true');

require_once __DIR__."/../../Globals/cont_log.php";
require_once __DIR__."/../class.phpmailer.php";
require_once __DIR__."/../class.smtp.php";
require_once __DIR__."/../WePay.php";
require_once __DIR__.'/../../Stripe/init.php';

class openModel {

    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_method = "";
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '', $wp_level = '';
    private $processing_fee_file_name;
    private $server_url;
    private $stripe_pk = '', $stripe_sk = '';

    public function __construct() {
        require_once __DIR__ . "/../../Globals/config.php";
        require_once  __DIR__ . "/../../Globals/stripe_props.php";
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->dbConnect();     // Initiate Database connection
        $this->processing_fee_file_name = __DIR__ . "/../../Globals/processing_fee.prods";
        $this->wp = new wepay();
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->wp_level = $this->wp->wp_level;
        date_default_timezone_set('UTC');
        mysqli_set_charset($this->db, "UTF8");
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'dev.mystudio.academy') !== false) {  //Development
            $this->server_url = "http://dev.mystudio.academy";
        } elseif(strpos($host, 'dev2.mystudio.academy') !== false){  //Development2
            $this->server_url = "http://dev2.mystudio.academy";
        }elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'];
        } else {
            $this->server_url = "http://dev.mystudio.academy";
        }
    }

    /*
     *  Database connection 
     */

    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }
    
    public function responseWithWepay($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function zap_log_info($msg) {
        $today = gmdate("d.m.Y");
        $filename = __DIR__ . "/../../Log/Zap/$today.txt";
        $fd = fopen($filename, "a");
        $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
        fwrite($fd, $str . PHP_EOL . PHP_EOL);
        fclose($fd);
    }

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    private function getStripeKeys(){        
        $this->stripe_pk = $GLOBALS['stripe_keys']['pk'];
        $this->stripe_sk = $GLOBALS['stripe_keys']['sk'];
    }

    public function checkZapAuthentication($header_api_key) {
        $company_id = '';
        $sql = sprintf("SELECT company_id from custom_api_key  where api_key='%s' AND deleted_flag !='Y'", mysqli_real_escape_string($this->db, $header_api_key));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $company_id = $row['company_id'];
                }
                return $company_id;
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid API Key Provided");
                zap_log_info($this->json($error));
                $this->response($this->json($error), 401);
            }
        }
    }
    
    public function getTriggerDetailsForLead($company_id) {
        $leads = [];
        $sql_reg = sprintf("SELECT `zap_reg_id` as Id, `buyer_first_name` as 'Buyer First Name' , `buyer_last_name` as 'Buyer Last Name',
                        `buyer_email` as Email, `buyer_phone` as Phone,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) 'Source', 
                        (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) 'Program Interest', IF(lr.`leads_status`='A','Active',IF(lr.`leads_status`='E','Enrolled','NotInterested')) LeadStatus,
                        `participant_firstname` as 'Participant First Name', `participant_lastname` as 'Participant Last Name' ,IF(lr.`include_campaign_email`='Y','Yes','No') as 'Campaign Status'
                        FROM `leads_registration` lr  WHERE `company_id`='%s' and `deleted_flag`!='Y'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql_reg);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_reg");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $leads[] = $row;
                }
                $this->response($this->json($leads), 200);
            }else {
                $error = array('status' => "Failed", "msg" => "Leads details doesn't exist.");
                zap_log_info($this->json($error));
                $this->response($this->json($error), 204);   //No data    
            }
        }
    }
    
    public function postActionForLead($company_id,$buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $program_interest, $source, $p_firstname, $p_lastname,$campaign){

        $user_timezone = $this->getUserTimezone($company_id, 500);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        $current_date = date("Y-m-d");
        
        $n_query = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_interest));
        $n_result = mysqli_query($this->db, $n_query);
        if (!$n_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($n_result);
            if ($num_rows == 1) {
                $result_obj = mysqli_fetch_object($n_result);
                $program_id = $result_obj->program_id;
            } else {
                $n_query1 = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id));
                $n_result1 = mysqli_query($this->db, $n_query1);
                if (!$n_result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query1");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $num_rows1 = mysqli_num_rows($n_result1);
                    if ($num_rows1 == 1) {
                        $result_obj = mysqli_fetch_object($n_result1);
                        $program_id = $result_obj->program_id;
                    }
                }
            }
        }

        $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source));
        $n_result2 = mysqli_query($this->db, $n_query2);
        if (!$n_result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows2 = mysqli_num_rows($n_result2);
            if ($num_rows2 == 1) {
                $result_obj1 = mysqli_fetch_object($n_result2);
                $source_id = $result_obj1->source_id;
            } else {
                $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id));
                $n_result2 = mysqli_query($this->db, $n_query2);
                if (!$n_result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $num_rows2 = mysqli_num_rows($n_result2);
                    if ($num_rows2 == 1) {
                        $result_obj1 = mysqli_fetch_object($n_result2);
                        $source_id = $result_obj1->source_id;
                    }
                }
            }
        }

        $this->addLeadsDimensions($company_id, $program_id, $source_id, '', 500);
        $checkold=sprintf("SELECT Max(zap_reg_id)+1 zap_count FROM `leads_registration`  WHERE  `company_id`='%s' ",
                mysqli_real_escape_string($this->db, $company_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
             if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $zap_reg_id = $old_value->zap_count;
            }
        }
        if($campaign == 'No'||$campaign == 'no'|| $campaign == 'NO'){
            $include_campaign_email = 'N';
        }else{
            $include_campaign_email = 'Y';
        }
        $query = sprintf("INSERT INTO `leads_registration`(`company_id`, `program_id`, `source_id`,`zap_reg_id`,`leads_status`, `buyer_first_name`, `buyer_last_name`, `buyer_phone`, `buyer_email`, `participant_firstname`, `participant_lastname`, 
                `registration_date`, `include_campaign_email`, `leads_reg_type_user`, `leads_reg_version_user`, `leads_entry_type`) 
                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), 
                mysqli_real_escape_string($this->db, $source_id), mysqli_real_escape_string($this->db, $zap_reg_id), mysqli_real_escape_string($this->db, 'A'), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_email), 
                mysqli_real_escape_string($this->db, $p_firstname), mysqli_real_escape_string($this->db, $p_lastname),  mysqli_real_escape_string($this->db, $current_date),
                mysqli_real_escape_string($this->db, $include_campaign_email), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, ''), mysqli_real_escape_string($this->db, 'Z'));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $err_no = mysqli_errno($this->db);
            if($err_no == 1062){
                sleep(0.5);
                $this->postActionForLead($company_id,$buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $program_interest, $source, $p_firstname, $p_lastname,$campaign);
            }
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $leads_reg_id = mysqli_insert_id($this->db);

            $activity_text = 'Registration date.';
            $activity_type="Individual Registration";
            if(!empty($buyer_email) && $include_campaign_email=='Y'){
                $this->campaignSendmail($leads_reg_id,$company_id,$buyer_email);
            }
            
            $curr_date = gmdate("Y-m-d H:i:s");
            $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id),  $activity_type, 
                    mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
            $result_history = mysqli_query($this->db, $insert_history);
            if (!$result_history) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }

            $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, 'A', '', $leads_reg_id, '', '', 500);
        }
        date_default_timezone_set($curr_time_zone);
        
        $out = array('status' => "Success", "msg" => "Lead Added Successfully");
        zap_log_info($this->json($out));
        $this->response($this->json($out), 200);
    }

    public function putActionForLead($company_id,$zap_reg_id,$buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $zap_program_interest, $zap_source, $p_firstname, $p_lastname){

        $checkold=sprintf("SELECT `leads_reg_id`,`zap_reg_id`,`registration_date`,`leads_status`,(select `source_name` from `leads_source` where `source_id`=lr.`source_id` and `company_id`=lr.`company_id`) lead_source, 
               (select `program_name` from `leads_program` where `program_id`=lr.`program_id` and `company_id`=lr.`company_id`) program_interest,`program_id`,`source_id` FROM `leads_registration` lr WHERE  `company_id`='%s' and `zap_reg_id`='%s'",
                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $zap_reg_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $program_id = $old_value->program_id;
                $source_id = $old_value->source_id;
                $program_interest = $old_value->program_interest;
                $lead_source = $old_value->lead_source;
                $leads_status = $old_value->leads_status;
                $zap_reg_id = $old_value->zap_reg_id;
                $leads_reg_date = $old_value->registration_date;
                $leads_reg_id = $old_value->leads_reg_id;
            }else{
                $error = array('status' => "Failed", "msg" => "Lead Registration Id Mismatch");
                zap_log_info($this->json($error));
                $this->response($this->json($error), 400);
            }
        }
        $n_query = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $zap_program_interest));
        $n_result = mysqli_query($this->db, $n_query);
        if (!$n_result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($n_result);
            if ($num_rows == 1) {
                $result_obj = mysqli_fetch_object($n_result);
                $program_interest_id = $result_obj->program_id;
                $program_name = $zap_program_interest;
            } else {
                $n_query1 = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id));
                $n_result1 = mysqli_query($this->db, $n_query1);
                if (!$n_result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query1");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $num_rows1 = mysqli_num_rows($n_result1);
                    if ($num_rows1 == 1) {
                        $result_obj = mysqli_fetch_object($n_result1);
                        $program_interest_id = $result_obj->program_id;
                        $program_name = 'Not Specified';
                    }
                }
            }
        }

        $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $zap_source));
        $n_result2 = mysqli_query($this->db, $n_query2);
        if (!$n_result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows2 = mysqli_num_rows($n_result2);
            if ($num_rows2 == 1) {
                $result_obj1 = mysqli_fetch_object($n_result2);
                $lead_source_id = $result_obj1->source_id;
                $source_name = $zap_source;
            } else {
                $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $company_id));
                $n_result2 = mysqli_query($this->db, $n_query2);
                if (!$n_result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $num_rows2 = mysqli_num_rows($n_result2);
                    if ($num_rows2 == 1) {
                        $result_obj1 = mysqli_fetch_object($n_result2);
                        $lead_source_id = $result_obj1->source_id;
                         $source_name = 'Not Specified';
                    }
                }
            }
        }

        $sql = sprintf("UPDATE `leads_registration` SET `buyer_first_name`='%s',`buyer_last_name`='%s',`buyer_email`='%s',`buyer_phone`='%s',`participant_firstname`='%s',`participant_lastname`='%s',`program_id`='%s',`source_id`='%s'  WHERE `company_id`='%s' AND `leads_reg_id`='%s'", 
                mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$buyer_phone), mysqli_real_escape_string($this->db,$p_firstname),mysqli_real_escape_string($this->db,$p_lastname),
                mysqli_real_escape_string($this->db,$program_interest_id),mysqli_real_escape_string($this->db,$lead_source_id), mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$leads_reg_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {

            if ($program_id != $program_interest_id) {
                $activity_type = 'Edit';
                $activity_text = "Program Interest changed from $program_interest to $program_name";

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
             if ($source_id != $lead_source_id) {
                $activity_type = 'Edit';
                $activity_text = "Lead Source changed from $lead_source to $source_name";

                $curr_date = gmdate("Y-m-d H:i:s");
                $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id), $activity_type, mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
                $result_history = mysqli_query($this->db, $insert_history);
                if (!$result_history) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                    zap_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
            if($program_id != $program_interest_id || $source_id != $lead_source_id){
                $this->addLeadsDimensions($company_id, $program_interest_id, $lead_source_id, $leads_reg_date, 500);
                $this->updateLeadsDimensionsMembers($company_id, $program_interest_id, $lead_source_id, $leads_status, '', $leads_reg_id, $program_id, $source_id, 500);
//                $old_check = array('old_status' => $leads_status);
//                $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $old_check, '', $leads_reg_id);
            }
            
           $msg = array('status' => "Success", "msg" => "Leads details updated successfully.");
           zap_log_info($this->json($msg));
           $this->response($this->json($msg), 200);
        }
    }
    
    public function deleteActionForLead($company_id,$reg_id){
        
        $sql = sprintf("SELECT zap_reg_id,program_id,source_id,leads_status,leads_reg_id,registration_date,deleted_flag FROM `leads_registration` WHERE `company_id`='%s' and `zap_reg_id` = '%s'",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $reg_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            if(mysqli_num_rows($result)>0){
                $delete_value= mysqli_fetch_object($result);
                $program_id = $delete_value->program_id;
                $source_id = $delete_value->source_id;
                $leads_status = $delete_value->leads_status;
                $leads_reg_id = $delete_value->leads_reg_id;
                $leads_reg_date = $delete_value->registration_date;
                $leads_deleted_flag = $delete_value->deleted_flag;
                
                if($leads_deleted_flag == 'Y'){
                    $out = array('status' => "Success", "msg" => "Lead Already Deleted");
                    zap_log_info($this->json($out));
                    $this->response($this->json($out), 200);
                }else{
                    $sql1 = sprintf("UPDATE `leads_registration`  SET `deleted_flag` = 'Y' WHERE `company_id` = '%s' and `zap_reg_id` = '%s'",
                    mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $reg_id));
                    $result1 = mysqli_query($this->db, $sql1);
                    if (!$result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        zap_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $this->addLeadsDimensions($company_id, $program_id, $source_id, $leads_reg_date, 500);
                        $this->updateLeadsDimensionsMembers($company_id, '', '', $leads_status, '', $leads_reg_id, $program_id, $source_id, 500);
                        $out = array('status' => "Success", "msg" => "Lead Deleted Successfully");
                        zap_log_info($this->json($out));
                        $this->response($this->json($out), 200);
                    }  
                }
            }else{
                $out = array('status' => "Failed", "msg" => "No Record Found");
                zap_log_info($this->json($out));
                $this->response($this->json($out), 400);
            }
        }
    }
    
    public function getUserTimezone($company_id, $error_code) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone = '';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            zap_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), $error_code);
        } else {
            $num_rows = mysqli_num_rows($result_tz);
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
      //dashboard
    public function addLeadsDimensions($company_id, $program_id, $source_id, $period, $error_code) {
        $user_timezone = $this->getUserTimezone($company_id, $error_code);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
        if (!empty($source_id)) {
            $sql1 = sprintf("SELECT * FROM `leads_dimensions` WHERE `company_id`='%s' AND `program_id`='%s' AND  `source_id`='%s' AND source_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `leads_dimensions`(`company_id`, `period`, `program_id`, `source_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        zap_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), $error_code);
                    }
                }
            }
        }
        if (!empty($program_id)) {
            $sql2 = sprintf("SELECT * FROM `leads_dimensions` WHERE `company_id`='%s' AND `program_id`='%s' AND `source_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `leads_dimensions`(`company_id`, `period`, `program_id`,  `source_id`) VALUES('%s', '%s','%s','0')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $program_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        zap_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), $error_code);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $leads_status, $old_leads_status, $leads_reg_id, $old_program_id, $old_source_id, $error_code) {
        $user_timezone = $this->getUserTimezone($company_id, $error_code);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $leads_string = $leads_sub_string = "";
        $date_str = "%Y-%m";
        $reg_date = '';

            if ($leads_status == 'A') {
                if ($old_leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($old_leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`-1";
                }
            } elseif ($leads_status == 'E') {
                if ($old_leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`-1";
                } else if ($old_leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`-1";
                }
            } elseif ($leads_status == 'NI') {
                if ($old_leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($old_leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`-1";
                }
            }


        $selectquery = sprintf("SELECT registration_date FROM `leads_registration` WHERE `company_id`='%s' and `leads_reg_id`='%s' and `deleted_flag`!='Y'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id));
        $res = mysqli_query($this->db, $selectquery);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selectquery");
            zap_log_info($this->json($selectquery));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), $error_code);
        } else {
            $num_of_rows = mysqli_num_rows($res);
            if ($num_of_rows == 1) {
                $rows = mysqli_fetch_object($res);
                $reg_date = $rows->registration_date;
            }
        }

        if (!empty($old_leads_status)) {
            if ($leads_status == 'A') {
                $leads_string = "$leads_string, `active_count`=`active_count`+1";
            } else if ($leads_status == 'E') {
                $leads_string = "$leads_string, `enrolled_count`=`enrolled_count`+1";
            } else if ($leads_status == 'NI') {
                $leads_string = "$leads_string, `notinterested_count`=`notinterested_count`+1";
            }
        } else {
                if ($leads_status == 'A') {
                    $leads_string = "`active_count`=`active_count`+1";
                    $leads_sub_string = "`active_count`=`active_count`-1";
                } else if ($leads_status == 'E') {
                    $leads_string = "`enrolled_count`=`enrolled_count`+1";
                    $leads_sub_string = "`enrolled_count`=`enrolled_count`-1";
                } else if ($leads_status == 'NI') {
                    $leads_string = "`notinterested_count`=`notinterested_count`+1";
                    $leads_sub_string = "`notinterested_count`=`notinterested_count`-1";
                }
            }
           
            if (!empty(trim($leads_string)) && !empty($program_id) && !empty($source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`=0 AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            }
        }
        if (!empty(trim($leads_string)) && !empty($program_id) && !empty($source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str), mysqli_real_escape_string($this->db,$source_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            }
        }
        
        if (!empty(trim($leads_sub_string)) && !empty($old_program_id) && !empty($old_source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_sub_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`=0 AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            }
        }
        if (!empty(trim($leads_sub_string)) && !empty($old_program_id) && !empty($old_source_id)) {
            $query1 = sprintf("UPDATE `leads_dimensions` SET $leads_sub_string WHERE `company_id`='%s' AND `program_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `source_id`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $old_program_id), mysqli_real_escape_string($this->db,$reg_date), mysqli_real_escape_string($this->db,$date_str), mysqli_real_escape_string($this->db,$old_source_id));
            $result1 = mysqli_query($this->db, $query1);
            if (!$result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                zap_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), $error_code);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function campaignSendmail($reg_id, $company_id, $buyer_email) {
        $check_email=0;
        $sql = sprintf("SELECT c.`campaign_id`,c.`subject`,c.`message`,c.campaign_type,c.`email_banner_image_url`,c.`button_text`,c.`button_url`,    
                        IF(ca.`web_page` IS NULL || TRIM(ca.`web_page`)='', '',ca.`web_page`) web_page, ca.company_name, ca.email_id, ca.`referral_email_list` 
                        FROM `campaign` c join `campaign` cm on c.`parent_id`=cm.`campaign_id` 
                        LEFT join campaign_trigger ct on  c.campaign_id=ct.campaign_id and c.company_id=ct.company_id
                        LEFT JOIN company ca on ca.company_id=c.company_id
                        where  cm.`campaign_type`='L' and c.type='E' and c.status='E' and ct.trigger_event_type='NL' and TRIM(c.`subject`!='') and TRIM(c.`message`!='') 
                        and ct.delay  IS NULL  and ct.deleted_flag='N' and c.deleted_flag='N' and cm.deleted_flag='N'  and c.company_id='%s'
                        and ca.upgrade_status NOT IN ('F', 'B')", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            zap_log_info($this->json($error_log));
            $check_email=1;
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $footer_detail = $this->getunsubscribefooter($company_id);
                while ($row = mysqli_fetch_assoc($result)) {
                    $email_status='';
                    
                    $message = '<div style="max-width:800px;">';
                    if (!empty($row['email_banner_image_url'])) {
                        $message .= '<img src="' . $row['email_banner_image_url'] . '" alt="Banner Image" style="max-width: 100%;display: block;margin-left: auto;margin-right: auto;width: 800px;"/>';
                    }
                    $message .= '<br>'.$row['message'].'<br><br>';
                    $studio_web_page = $row['web_page'];
                    $button_url=$row['button_url'];
                    $button_text=$row['button_text'];
                    
                    // CHECKING GET STARTED BUTTON TEXT AND LINK
                    if (!empty($button_text) && !empty($button_url)) {
                        $message .= '<center><a href="' . $button_url . '"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">' . $button_text . '</button></a></center>';
                    } elseif (!empty($studio_web_page)) {
                        $button_text = "Get started";
                        $message .= '<center><a href="' . $studio_web_page . '"><button type="button" style="width: 230px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;padding: 12px 0;height: 50px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">' . $button_text . '</button></a></center>';
                    }
                    
                    $message .='</div>';
                    
                    $sendEmail_status = $this->sendEmail($buyer_email, $row['subject'], $message, $row['company_name'], $row['email_id'], '', '', $row['referral_email_list'],$company_id,$footer_detail);
                    
                    if ($sendEmail_status['status'] == "true") {
                        $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email, "Email Message" => $sendEmail_status['mail_status']);
                        zap_log_info($this->json($error_log));
                        $updatesql = sprintf("UPDATE `campaign` SET `email_sent_count`=email_sent_count+1 WHERE `campaign_id`='%s'", mysqli_real_escape_string($this->db, $row['campaign_id']));
                        $resultupdtae = mysqli_query($this->db, $updatesql);
                        if (!$resultupdtae) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                            zap_log_info($this->json($error_log));
                            $check_email=1;
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    } else {
                        $email_status='F';
                        $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email, "Email Message" => $sendEmail_status['mail_status']);
                        zap_log_info($this->json($error_log));
                        $updatesql = sprintf("UPDATE `campaign` SET `email_sent_count`=email_sent_count+1,`email_failed_count`=email_failed_count+1 WHERE `campaign_id`='%s'", mysqli_real_escape_string($this->db, $row['campaign_id']));
                        $resultupdtae = mysqli_query($this->db, $updatesql);
                        if (!$resultupdtae) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$updatesql");
                           zap_log_info($this->json($error_log));
                            $check_email=1;
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
//                       
                    }

                    $insertsql = sprintf("INSERT INTO `campaign_email_log`(`company_id`, `reg_id`, `email_id`, `campaign_type`,`email_status`,`buyer_email`) VALUES ('%s','%s','%s','%s','%s','%s')",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $reg_id), mysqli_real_escape_string($this->db, $row['campaign_id']), mysqli_real_escape_string($this->db, $row['campaign_type'])
                            ,mysqli_real_escape_string($this->db, $email_status),mysqli_real_escape_string($this->db, $buyer_email));
                    $resultinsert = mysqli_query($this->db, $insertsql);
                    if (!$resultinsert) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertsql");
                       zap_log_info($this->json($error_log));
                        $check_email=1;
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
        
        if($check_email==1){
            $msg = "Something went wrong in campaign send email for Lead registration with reg_id=$reg_id, check web log or table";
            $this->sendEmailForAdmins($msg);
        }
    }

    private function sendEmail($to, $subject, $message, $cmp_name, $reply_to, $file_name, $attached_file, $cc_email_list, $company_id, $footer_detail) {
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }            
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
            $mail->Subject = $subject;
             if (!empty($company_id)) {
                $curr_loc = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                $curr_loc = implode('/', explode('/', $curr_loc, -4));
//                $tomail = $to;
//                if (!empty(trim($cc_email_list))) {
//                    $tomail = base64_encode($to . "," . "$cc_email_list");
//                } else {
                    $tomail = base64_encode($to);
//                }
                $activation_link = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/EmailSubscription.php?mail=$tomail?comp_id=$company_id?type=U";
                $message = "
            <html>
            <body>
                <p>$message</p><br><br>
                    <footer style='font-size: 13px;color: #746f6f;'>
                    <center>
                    <p>$footer_detail
                    </p>
                    <a href='$activation_link' target='_blank' style='color: #746f6f; !important;text-decoration: none !important;'><u style='color: #746f6f;'>Unsubscribe $to</u></a>
                    </center>
                    </footer>
            </body>
            </html>
            ";
            }
            $mail->Body = $message;
            if(!empty($file_name)){
                $mail->AddStringAttachment(base64_decode($attached_file), $file_name, 'base64');
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $verify_email_for_bounce = $this->verifyBouncedEmailForCC($cc_addresses[$init], $company_id);
                         if($verify_email_for_bounce==1){
                               $mail->AddCC($cc_addresses[$init]);
                         }
//                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    private function sendEmailForAdmins($msg){
        if($this->env=='P'){
            $env = "Production";
        }else{
            $env = "Development";
        }
        $cmp_name = "MyStudio Cron Deamon";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com,muthulakshmi.m@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "$env - Open Model Leads Campaign Handler Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function verifyBouncedEmailForCC($cc_address, $company_id){
        $selecsql=sprintf("SELECT * FROM `bounce_email_verify` b WHERE b.`bounced_mail`='%s' AND (b.`company_id`=0 OR b.`company_id`='%s')", mysqli_real_escape_string($this->db,$cc_address), mysqli_real_escape_string($this->db,$company_id));
        $result= mysqli_query($this->db,$selecsql);
        if(!$result){
             $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
          if(mysqli_num_rows($result)>0){
              return 0;
          }else{
               return 1;
          }
        }
    }
    
    public function getRetailProductDetails($company_id, $product_id, $page_from){
        $prod_str = $retail_sales_agreement = "";
        if(!empty($product_id)){
            $p_id = mysqli_real_escape_string($this->db, $product_id);
            $prod_str = "AND (rp.`retail_product_id`='$p_id' OR rp.`retail_parent_id`='$p_id')";
        }
        $this->getStudioSubscriptionStatus($company_id);
        $response = $retail_category = $retail_product = $category_id_list = $product_id_list = [];
        
        $query = sprintf("SELECT rp.`retail_product_id`, rp.`retail_parent_id`, rp.`company_id`, rp.`retail_product_type`, rp.`retail_product_status`, rp.`retail_product_title`, rp.`retail_product_subtitle`, 
                    rp.`retail_banner_img_url`, rp.`retail_product_desc`, rp.`retail_product_compare_price`, rp.`retail_product_inventory`, rp.`retail_purchase_count`, rp.`retail_sort_order`,
                    rp.`retail_variant_flag`, c.`wp_currency_symbol`, IF(rp.`retail_variant_flag`='N', CONCAT(c.wp_currency_symbol,rp.`retail_product_price`),
                    (SELECT IF(MIN(variant_price)=MAX(variant_price), CONCAT(c.wp_currency_symbol, MIN(variant_price)), CONCAT(c.wp_currency_symbol, MIN(variant_price), ' - ', c.wp_currency_symbol, MAX(variant_price))) FROM `retail_variants` rv WHERE rv.`company_id`='%s' AND rv.`retail_product_id`=rp.`retail_product_id` AND rv.`deleted_flag`!='Y')) retail_product_price,
                    IF(rp.`retail_variant_flag`='Y', (SELECT IF(COUNT(*)>0, 'Y', 'N') FROM retail_variants WHERE rp.`company_id`=`company_id` AND rp.`retail_product_id`=`retail_product_id` AND `deleted_flag`!='Y'), 'Y') show_in_list, c.`retail_sales_agreement` 
                    FROM `retail_products` rp LEFT JOIN `company` c ON rp.`company_id`=c.`company_id` 
                    WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='P' $prod_str ORDER BY rp.`retail_sort_order` desc", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $count_only_check = 1;          //1 - Need only count, 0 - all data
                $disc = $this->getRetailDiscountDetails($company_id, 1, $count_only_check);   //2nd parameter --> 1 - return, 0 - echo and exit
                if($disc['status']=='Success'){
                    if($count_only_check==0){
                        $response['retail_discount'] = $disc['msg'];
                    }
                    $response['retai_discount_available'] = 'Y';
                }else{
                    if($count_only_check==0){
                        $response['retail_discount'] = [];
                    }
                    $response['retai_discount_available'] = 'N';
                }
                while($row = mysqli_fetch_assoc($result)){
                    $retail_sales_agreement = '<div style="max-height:92px;overflow-y:auto;">'.$row['retail_sales_agreement'].'</div>';
                    $unique_id = $row['retail_product_id'];
                    $row_category = $row;
                    if (($row['retail_product_type']=='C' || $row['retail_product_type']=='S') && !empty($unique_id) && !in_array($unique_id, $category_id_list, true)) {
                        if ($row['retail_product_type']=='C' || ($row['retail_product_type']=='S' && $row['show_in_list']=='Y')){
                            $category_id_list[] = $unique_id;
                            $retail_category[] = $row_category;
                        }
                    }
                    if($row['retail_product_type']=='P' && !empty($row['retail_parent_id']) && !empty($unique_id) && !in_array($unique_id, $product_id_list, true)){
                        if($row['show_in_list']=='Y'){
                            $product_id_list[] = $unique_id;
                            $retail_product[] = $row_category;
                            if($page_from=='CL'){
                                $retail_category[] = $row_category;
                            }
                        }
                    }
                }
                if(count($retail_product)>0 && count($retail_category)>0){
                    for($i=0; $i<count($retail_product); $i++){
                        if(in_array($retail_product[$i]['retail_parent_id'], $category_id_list)){
                            $retail_category[array_search($retail_product[$i]['retail_parent_id'], $category_id_list)]['products'][] = $retail_product[$i];
                        }
                    }
                    for($i=0; $i<count($retail_category); $i++){
                        if(empty($retail_category[$i]['products']) && $retail_category[$i]['retail_product_type']=='C'){
                            array_splice($retail_category, $i, 1);
                            $i--;
                        }
                    }
                    usort( $retail_category, function($a, $b) {
                        return $a['retail_sort_order'] < $b['retail_sort_order'];
                    });
                }
                $response['retail_sales_agreement'] = $retail_sales_agreement;
                $response['retail_category'] = $retail_category;
                if (empty($retail_category)) {
                    $error = array('status' => "Failed", "msg" => "Retail Product details doesn't exist.");
                    $this->response($this->json($error), 200); // If no records "No Content" status
                } else {
                    $out = array('status' => "Success", 'msg' => $response);
                    $this->response($this->json($out), 200);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Retail Product details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    public function getRetailVariantDetails($company_id, $product_id){
        $query = sprintf("SELECT rv.`retail_variant_id`, rv.`retail_product_id`, rv.`retail_variants_name`,rv.`variant_compare_price`, rv.`inventory_count`, rv.`variant_price`, rv.`inventory_used_count` 
                    FROM `retail_variants` rv LEFT JOIN `retail_products` rp ON rv.`company_id`=rp.`company_id` AND rv.`retail_product_id`=rp.`retail_product_id`
                    WHERE rv.`company_id`='%s' AND rv.`retail_product_id`='%s' AND rv.`deleted_flag`='N' AND rp.`retail_product_status`='P' AND rp.`retail_variant_flag`='Y' ORDER BY `variant_sort_order` DESC",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result)){
                    $response[] = $row;
                }
                $out = array('status' => "Success", 'msg' => $response);
                $this->response($this->json($out), 200);
            }else{
                $error = array('status' => "Failed", "msg" => "Retail Product Variant details doesn't exist.");
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
    }
    
    
    
    //Get Discount details by Retail ID
    protected function getRetailDiscountDetails($company_id, $call_back, $count_only_check) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $created_date =" DATE(CONVERT_TZ(NOW(),$tzadd_add,'$new_timezone'))";
        
        $sql = sprintf("SELECT `retail_discount_id`, `company_id`, `retail_discount_type`, `retail_discount_code`, `retail_discount_amount`, `min_purchase_flag`, `min_purchase_quantity`, `min_purchase_amount`, `discount_begin_dt`, `discount_end_dt`, `discount_limit_flag`, `discount_limit_value` FROM `retail_discount`
                WHERE `company_id` = '%s' AND `deleted_flag`='N' AND (`discount_end_dt` IS NULL OR `discount_end_dt`>$created_date)", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        $output = $response = [];
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $response['status'] = "Success";
                if($count_only_check==1){
                    $response['msg'] = "Discount code available for this studio";
                    if ($call_back == 1) {
                        return $response;
                    } else {
                        $this->response($this->json($response), 200);
                    }
                }
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
            } else {
                $response['status'] = "Failed";
            }
            $response['msg'] = $output;
            if ($call_back == 1) {
                return $response;
            } else {
                $this->response($this->json($response), 200);
            }
        }
    }

    public function getStudioSubscriptionStatus($company_id){
        $query = sprintf("SELECT email_id,upgrade_status,subscription_status,wepay_status,stripe_status,stripe_subscription,studio_expiry_level,expiry_dt FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_email = $row['email_id'];
                $upgrade_status = $row['upgrade_status'];
                $subscription_status = $row['subscription_status'];
                $studio_expiry_level = $row['studio_expiry_level'];
                if (!empty(trim($upgrade_status)) && $subscription_status=='N' && ($studio_expiry_level=='L2' || $upgrade_status=='F')) {
                    $res = array('status' => "Failed", "msg" => "Please contact the business operator to activate the app.", "company_id" => $company_id, "studio_email"=>$studio_email, "upgrade_status"=>$upgrade_status, "studio_expiry_level" => $studio_expiry_level);
                    $this->response($this->json($res), 200);
                }else{
                    $res = array('status' => "Success", "msg" => "Studio subscription status success", "upgrade_status" => $upgrade_status);
                    return $res;
                }                
            } else {
                $res = array('status' => "Failed", "msg" => "Studio Details not found.");
                $this->response($this->json($res), 200);
            }
        }
    }
    
    public function checkStudioAndEmail($company_id, $email){
        $query = sprintf("SELECT * FROM `user` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Studio details.", "session_status" => "Expired");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function verifyStudioPOSToken($company_id, $email, $token) {
        $this->checkStudioAndEmail($company_id, $email);
        $query = sprintf("SELECT * FROM `pos_token` WHERE `company_id`='%s' AND `user_email`='%s'",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $email));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $old_token = $row['token'];
                $token_id = $row['pos_token_id'];
                if($old_token==$token){
                    $query2 = sprintf("UPDATE `pos_token` SET `no_of_access`=`no_of_access`+1 WHERE `pos_token_id`='%s'", mysqli_real_escape_string($this->db, $token_id));
                    $result2 = mysqli_query($this->db, $query2);                
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                    $msg = array("status"=>"Success", "msg"=>"Session is active.");
                    return $msg;
                }else{
                    $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                    $this->response($this->json($msg), 200);
                }
            }else{
                $msg = array("status"=>"Failed", "msg"=>"Session expired.", "session_status" => "Expired");
                $this->response($this->json($msg), 200);
            }
        }
    }
    
    public function getpubliccompanydetails($company_id, $studio_code, $reg_type_user, $page_from){
        $this->getStripeKeys();
        $stripe_publish_key = $this->stripe_pk;
        $this->getStudioSubscriptionStatus($company_id);
        $wepay_status = '';
        $stripe_upgrade_status = $stripe_subscription = '';
        $query = sprintf("SELECT c.`wp_currency_code`, c.`wp_currency_symbol`, c.`company_name`, c.`company_id`, c.`logo_URL`, c.`upgrade_status`, c.`stripe_status`, c.`stripe_subscription`,c.`retail_enabled`, c.`retail_processingfee`, c.`retail_sales_agreement`, c.country, pos.* FROM `company` c JOIN studio_pos_settings pos USING(company_id) WHERE c.`company_id`='%s' AND c.`company_code`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $studio_code));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $company_name = $row['company_name'];
                if(!empty(trim($row['logo_URL']))){
                    $logo_URL = $row['logo_URL'];
                }else{
                    $logo_URL = $this->server_url."/uploads/Default/default_logo.png";
                }
                $upgrade_status = $row['upgrade_status'];
                $wp_currency_code = $row['wp_currency_code'];
                $wp_currency_symbol = $row['wp_currency_symbol'];
                $stripe_upgrade_status = $row['stripe_status'];
                $stripe_subscription = $row['stripe_subscription'];
                $country = $row['country'];
                if(!empty($row['account_state'])){
                    $account_state = $row['account_state'];
                    $curr = $row['currency'];
                    if(!empty(trim($curr))){
                        $currency = explode(",", trim($curr));
                    }
                    $wepay_status = 'Y';
                }
                $retail_enable_flag = $row['retail_enabled'];
                $retail_processing_fee_type = $row['retail_processingfee'];
                $retail_sales_agreement = '<div style="max-height:92px;overflow-y:auto;">'.$row['retail_sales_agreement'].'</div>';
                
                if($page_from=='R' && $retail_enable_flag=='N'){
                    $res = array('status' => "Failed", "msg" => "Landing page is disabled. Please contact the business operator for more information.");
                    $this->response($this->json($res), 200);
                }
                
                $company_details = [];
                $company_details['company_name'] = $company_name;
                $company_details['logo_URL'] = $logo_URL;
                $company_details['upgrade_status'] = $upgrade_status;
                $company_details['wp_currency_code'] = $wp_currency_code;
                $company_details['wp_currency_symbol'] = $wp_currency_symbol;
                $company_details['wepay_status'] = $wepay_status;
                $company_details['wp_client_id'] = $this->wp_client_id;
                $company_details['wp_level'] = $this->wp_level;
                //pos settings
                $company_details['misc_charge_status'] = $row['misc_charge_status'];
                $company_details['retail_status'] = $row['retail_status'];
                $company_details['event_status'] = $row['event_status'];
                $company_details['membership_status'] = $row['membership_status'];
                $company_details['trial_status'] = $row['trial_status'];
                $company_details['leads_status'] = $row['leads_status'];
                if($reg_type_user!='U'){
                    if($page_from=='C' && $row['misc_charge_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Miscellaneous Charge POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }elseif($page_from=='L' && $row['leads_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Request More Info POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }elseif($page_from=='R' && $row['retail_status']=='D'){
                        $res = array('status' => "Failed", "msg" => "Retail POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($res), 200);
                    }
                }
                
                $company_details['misc_charge_fee_type'] = $row['misc_charge_fee_type'];
                $company_details['misc_sales_agreement'] = $row['misc_sales_agreement'];
                $company_details['misc_payment_method'] = $row['misc_payment_method'];
                $company_details['retail_payment_method'] = $row['retail_payment_method'];
                $company_details['event_payment_method'] = $row['event_payment_method'];
                $company_details['membership_payment_method'] = $row['membership_payment_method'];
                $company_details['trial_payment_method'] = $row['trial_payment_method'];
                $company_details['leads_payment_method'] = $row['leads_payment_method'];
                
                $process_fee = $this->getProcessingFee($company_id,$upgrade_status);
                $country_payment_specific = $this->checkCountryPaymentSupport($company_id,$country,1); // callback - 1/0
                $company_details['processing_fee_percentage'] = $process_fee['PP'];
                $company_details['processing_fee_transaction'] = $process_fee['PT'];
                $company_details['retail_enable_flag'] = $retail_enable_flag;
                $company_details['retail_processing_fee_type'] = $retail_processing_fee_type;
                $company_details['retail_sales_agreement'] = $retail_sales_agreement;
                 //stripe set
                $company_details['stripe_publish_key'] = $stripe_publish_key;  // KEY FROM - Globals/stripe_props.php
                $company_details['stripe_upgrade_status'] = $stripe_upgrade_status;
                $company_details['stripe_subscription'] = $stripe_subscription;
                $company_details['stripe_country_support'] = $stripe_country_support = ($country_payment_specific['status'] == 'Success' && $country_payment_specific['support_status'] =='Y' && $country_payment_specific['payment_support'] =='S') ? "Y":"N";  // CHECK COUNTRY SUPPORT STATUS FOR STRIPE
                $out = array('status' => "Success", "company_details" => $company_details);
                $this->response($this->json($out), 200);
            } else {
                $res = array('status' => "Failed", "msg" => "Company details does not exits.");
                $this->response($this->json($res), 200);
            }
        }
    }
    
    private function getProcessingFee($company_id, $upgrade_status) {
        $temp=[];
        $payment_mode = '';
        // get and confirm company payment mode - arun
        $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $res = mysqli_query($this->db, $query);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($res);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($res);
                $studio_stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
                $studio_wepay_status = $row['wepay_status'];  
            }
        }
        if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
            $payment_mode = 'S';
        } else if ($studio_wepay_status == 'Y') {
            $payment_mode = 'W';
        }
        $sql = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id`='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $payment_mode));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $temp['PP'] = $rows['cc_processing_percentage'];
                    $temp['PT'] = $rows['cc_processing_transaction'];
                }
                return $temp;
            } else {
                $sql2 = sprintf("SELECT `cc_processing_percentage`,`cc_processing_transaction` FROM `processing_fee` WHERE `company_id` in (select if(country='ca' || country='canada', -1, if(country='uk' || country='united kingdom', -2, if(country='au' || country='australia', -3, 0))) from company where company_id='%s') and level='%s' and if(`end_date`is NULL,CURRENT_DATE+1,end_date>=CURRENT_DATE) and `payment_support` = if('%s'='S','S','W')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $upgrade_status), mysqli_real_escape_string($this->db, $payment_mode));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result2);
                    if ($num_of_rows > 0) {
                        while ($rows = mysqli_fetch_assoc($result2)) {
                            $temp['PP'] = $rows['cc_processing_percentage'];
                            $temp['PT'] = $rows['cc_processing_transaction'];
                        }
                        return $temp;
                    }else{                // IF NOT SUPPORTS FOR WEPAY / STRIPE
                        $temp['PP'] = 0;
                        $temp['PT'] = 0;
                    }
                }
            }
        }
    }
    
    protected function wepayStatus($company_id){
        $query = sprintf("SELECT * FROM `wp_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);

        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state']=='active'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "A", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
    
    protected function stripeStatus($company_id){
        $query = sprintf("SELECT * FROM `stripe_account` where `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        
        $account_state = '';
        
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $currency = [];
                $row = mysqli_fetch_assoc($result);
                $curr = $row['currency'];
                if(!empty(trim($curr))){
                    $currency = explode(",", trim($curr));
                }
                if($row['account_state'] === 'Y'){
                    $res = array('status' => "Success", "msg" => "Y", "currency" => $currency, "info" => "Account active");
                }else{
                    $res = array('status' => "Success", "msg" => "Y", "info" => "Deleted/Disabled/Action needed.");
                }
            }else{
                $res = array('status' => "Failed", "msg" => "N", "info" => "Not yet registered.");
            }
            $this->response($this->json($res), 200);
        }
    }
        
    public function applyRetailDiscountForProducts($company_id, $discount_code, $cart_details, $checkout_flag, $callBack, $optional_discount_flag, $optional_discount_amount, $payment_method){
        $disc_pr = $total_array = [];
        $retail_array = $retail_product_id = [];
        $total_qty = 0;
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $created_date = " CONVERT_TZ(NOW(),$tzadd_add,'$new_timezone')";
//        $discount_begin_dt = " DATE(CONVERT_TZ(discount_begin_dt,$tzadd_add,'$new_timezone'))";
//        $discount_end_dt = " DATE(CONVERT_TZ(discount_end_dt,$tzadd_add,'$new_timezone'))";

//        $cart_count = count($cart_details);
        for ($i = 0; $i < count($cart_details); $i++) {
            if (isset($cart_details[$i]['retail_variant_id']) && !empty($cart_details[$i]['retail_variant_id'])) {
//                $retail_variant_id[] = mysqli_real_escape_string($this->db, $cart_details[$i]['variants']);
                $retail_array[$i]['retail_variant_id'] = $cart_details[$i]['retail_variant_id'];
            } else {
                $retail_array[$i]['retail_variant_id'] = '';
            }
            if (isset($cart_details[$i]['retail_parent_id']) && !empty($cart_details[$i]['retail_parent_id'])) {
                $retail_array[$i]['retail_parent_id'] = $cart_details[$i]['retail_parent_id'];
            } else {
                $retail_array[$i]['retail_parent_id'] = '';
            }
            if(isset($cart_details[$i]['quantity']) && !empty($cart_details[$i]['quantity'])){
            $total_qty += $cart_details[$i]['quantity'];
                $retail_array[$i]['quantity'] = mysqli_real_escape_string($this->db, $cart_details[$i]['quantity']);
            }else{
                $retail_array[$i]['quantity'] = 0;
            }
            $retail_array[$i]['deleted_flag'] = $cart_details[$i]['deleted_flag'];
            $retail_array[$i]['retail_product_id'] = $retail_product_id[] = mysqli_real_escape_string($this->db, $cart_details[$i]['retail_product_id']);
            $retail_array[$i]['quantity_error_flag'] = $cart_details[$i]['quantity_error_flag'];
            $retail_array[$i]['quantity_msg'] = $cart_details[$i]['quantity_msg'];
        }
        $checkout_products = implode(",", array_unique($retail_product_id));

        if (empty($discount_code)) {
            $without_disc_query = sprintf("SELECT c.`upgrade_status`, c.`retail_processingfee`,c.`wp_currency_symbol` FROM `company` c 
                 WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result_wodisc = mysqli_query($this->db, $without_disc_query);
            if (!$result_wodisc) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$without_disc_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows_wodisc = mysqli_num_rows($result_wodisc);
                if ($num_rows_wodisc > 0) {
                    $row = mysqli_fetch_assoc($result_wodisc);
                    $currency_symbol = $row['wp_currency_symbol'];
                    $p_f = $this->getProcessingFee($company_id, $row['upgrade_status']);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                     $temp_discount_type = $disc_pr['disc_type'] = 'V';
                    $temp_discount_value = $disc_pr['disc_amount'] = $optional_discount_amount;
                    $temp_discount_usage_type = 'O';
                    if($optional_discount_flag == 'Y'){
                        $final_return_array = $this->calculateCheckoutForRetail($company_id, '', $checkout_products, 'O', $retail_array, $disc_pr);
                        $final_retail_array = $final_return_array['retail_array'];
                        $total_array = $final_return_array['temp'];
                        if ($optional_discount_amount > $total_array['total_order_price']) {
//                            $res = array('status' => "Failed", "msg" => "Required conditions have not been met in order to use this code", "retail_array" => $final_retail_array, 'total'=>$total_array);
                                $response_array = array(
                                "status"=>'Failed',
                                "discount_error_flag"=>'N',
                                "discount_error_msg"=>'Discount Amount is Greater Than Checkout Amount',
                                "discount_usage_type"=>$temp_discount_usage_type,
                                "price_error_flag"=>'N',
                                "price_error_msg"=>'',
                                "optional_discount_error_flag"=>'Y',
                                "discount_type"=>$temp_discount_type,
                                "discount_value"=>$temp_discount_value,
                                "retail_array"=>$final_retail_array,
                                "total_array"=>$total_array
                            );
                            if ($callBack == 0) {
                                $this->response($this->json($response_array), 200);
                            } else {
                                return $response_array;
                            }
                        } 
                        $total_price = $total_discount = $total_tax = $total_order_price = 0; 
                            for($k=0; $k < count($final_retail_array); $k++){
                                $disc_percent_value = ($final_retail_array[$k]['retail_total_product_price']/$total_array['total_order_price']) * $disc_pr['disc_amount'];
                                $final_retail_array[$k]['discounted_value'] = $disc_percent_value;
                                $final_retail_array[$k]['retail_discounted_price'] = $final_retail_array[$k]['retail_discounted_price'] - $disc_percent_value;
                                $tax_percent_value = ($final_retail_array[$k]['retail_discounted_price']) * ($final_retail_array[$k]['product_tax_rate']/100);
                                $final_retail_array[$k]['product_tax_value'] = $tax_percent_value;
                                $total_tax += $tax_percent_value;
                                $total_price += $final_retail_array[$k]['retail_discounted_price']+$final_retail_array[$k]['product_tax_value'];
                                $total_discount += $disc_percent_value;
                                $total_order_price += ($final_retail_array[$k]['retail_product_price'] * $final_retail_array[$k]['quantity']) + $tax_percent_value;
                            }
                            $total_array['total_tax_value'] = round($total_tax,2);
                            $total_array['total_discounted_price'] = $total_price;
                            $total_array['total_discount_value'] = round($total_discount,2);
                            $total_array['total_order_price_without_discount'] = round($total_order_price,2);
                    }else{
                        $final_return_array = $this->retailProductsPriceCalculationForNoDiscount($company_id, $checkout_products, $retail_array);
                        $final_retail_array = $final_return_array['retail_array'];
                        $total_array = $final_return_array['temp'];
                        $temp_discount_type = '';
                        $temp_discount_value = '';
                        $temp_discount_usage_type = '';
                    }
                    $total_array['total_quantity'] = $total_qty;
                    if ($total_array['total_discounted_price'] < 5 && $total_array['total_discounted_price'] > 0 && $checkout_flag == 'Y') {
//                        $res = array('status' => "Failed", "msg" => "Purchase amount should be greater than 5.", "retail_array" => $final_retail_array, 'total' => $total_array);
                    $response_array = array(
                        "status"=>'Failed',
                        "discount_error_flag"=>'N',
                        "discount_error_msg"=>'',
                        "discount_usage_type"=>'',
                        "price_error_flag"=>'Y',
                        "price_error_msg"=>"Purchase amount should be greater than ".$currency_symbol."5.",
                        "optional_discount_error_flag"=>'N',
                        "discount_type"=>'',
                        "discount_value"=>'',
                        "retail_array"=>$final_retail_array,
                        "total_array"=>$total_array
                    );    
                        if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    }
                    $total_array['processing_fee_type'] = $row['retail_processingfee'];
                    if ($total_array['total_discounted_price'] <= 0 || $payment_method == 'CH' || $payment_method == 'CA') {
                        $total_array['total_processing_fee'] = 0;
                    } else {
                        $total_array['total_processing_fee'] = round($this->calculateProcessingFee($total_array['total_discounted_price'], $row['retail_processingfee'], $process_fee_per, $process_fee_val),2);
                    }
                    if ($row['retail_processingfee'] == 2) {
                        $total_array['total_discounted_price'] += $total_array['total_processing_fee'];
                        $total_array['total_order_price_without_discount'] += $total_array['total_processing_fee'];
                        $total_array['total_order_compare_price'] = $total_array['total_order_compare_price'] + 0.00000000111;
                    }
//                    $res = array('status' => "Success", "msg" => "Checkout without discount.", "retail_array" => $final_retail_array, 'total' => $total_array);
                   $total_array['total_discounted_price'] = round($total_array['total_discounted_price'],2);
                    $response_array = array(
                        "status"=>'Success',
                        "discount_error_flag"=>'N',
                        "discount_error_msg"=>'',
                        "discount_usage_type"=>$temp_discount_usage_type,
                        "price_error_flag"=>'N',
                        "price_error_msg"=>'',
                        "optional_discount_error_flag"=>'N',
                        "discount_type"=>$temp_discount_type,
                        "discount_value"=>$temp_discount_value,
                        "retail_array"=>$final_retail_array,
                        "total_array"=>$total_array
                    );
                    if ($callBack == 0) {
                        $this->response($this->json($response_array), 200);
                    } else {
                        return $response_array;
                    }
                }
            }
        }

        $disc_query = sprintf("SELECT rd.*, c.`upgrade_status`, c.`retail_processingfee`,c.`wp_currency_symbol` FROM `retail_discount` rd LEFT JOIN company c ON rd.`company_id`=c.`company_id`
                 WHERE rd.`company_id`='%s' AND `retail_discount_code`='%s' AND rd.`deleted_flag`='N' AND (`discount_end_dt` = '0000-00-00 00:00:00' OR `discount_end_dt`>=$created_date) AND`discount_begin_dt`<=$created_date", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $discount_code));
        $result_disc = mysqli_query($this->db, $disc_query);
        if (!$result_disc) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$disc_query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows_disc = mysqli_num_rows($result_disc);
            if ($num_rows_disc > 0) {
                while ($row = mysqli_fetch_assoc($result_disc)) {
                    $disc_pr['disc_type'] = $row['retail_discount_type'];
                    $disc_pr['disc_amount'] = $row['retail_discount_amount'];
                    $currency_symbol = $row['wp_currency_symbol'];
                    $p_f = $this->getProcessingFee($company_id, $row['upgrade_status']);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                    $final_return_array_wo = $this->retailProductsPriceCalculationForNoDiscount($company_id, $checkout_products, $retail_array);
                    $final_retail_array_wo = $final_return_array_wo['retail_array'];
                    $total_array_wo = $final_return_array_wo['temp'];
                    $total_array_wo['discount_id'] = '';
                    $total_array_wo['total_quantity'] = $total_qty;
                    
                    if ($total_array_wo['total_discounted_price'] < 5 && $total_array_wo['total_discounted_price'] > 0 && $checkout_flag == 'Y') {
//                        $res = array('status' => "Failed", "msg" => "Purchase amount should be greater than 5.", "retail_array" => $final_retail_array, 'total' => $total_array);
                        $response_array = array(
                            "status" => 'Failed',
                            "discount_error_flag" => 'N',
                            "discount_error_msg" => '',
                            "discount_usage_type" => '',
                            "price_error_flag" => 'Y',
                            "price_error_msg" => 'Purchase amount should be greater than '.$currency_symbol.'5.',
                            "optional_discount_error_flag"=>'N',
                            "discount_type" => '',
                            "discount_value" => '',
                            "retail_array" => $final_retail_array_wo,
                            "total_array" => $total_array_wo
                        );
                        if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    }

                    $total_array_wo['processing_fee_type'] = $row['retail_processingfee'];
                    if ($total_array_wo['total_discounted_price'] <= 0 || $payment_method == 'CH' || $payment_method == 'CA') {
                        $total_array_wo['total_processing_fee'] = 0;
                    } else {
                        $total_array_wo['total_processing_fee'] = round($this->calculateProcessingFee($total_array_wo['total_discounted_price'], $row['retail_processingfee'], $process_fee_per, $process_fee_val), 2);
                    }
                    if ($row['retail_processingfee'] == 2) {
                        $total_array_wo['total_discounted_price'] += $total_array_wo['total_processing_fee'];
                        $total_array_wo['total_order_price_without_discount'] += $total_array_wo['total_processing_fee'];
                    }
                    $total_array_wo['total_discounted_price'] = round($total_array_wo['total_discounted_price'], 2);

                    if($total_array_wo['total_order_price'] == 0){
                         $response_array = array(
                            "status"=>'Success',
                            "discount_error_flag"=>'N',
                            "discount_error_msg"=>'',
                            "discount_usage_type"=>'',
                            "price_error_flag"=>'N',
                            "price_error_msg"=>'',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>'',
                            "discount_value"=>'',
                            "retail_array"=>$final_retail_array_wo,
                            "total_array"=>$total_array_wo
                        );
                    if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    }
                    
                    if ($row['min_purchase_flag'] == 'Q' && $total_qty < $row['min_purchase_quantity']) {
                            $response_array = array(
                            "status"=>'Failed',
                            "discount_error_flag"=>'Y',
                            "discount_error_msg"=>'Minimum item quantity ('.$row['min_purchase_quantity'].')  have not been met for this discount',
                            "discount_usage_type"=>$row['retail_discount_usage_type'],
                            "price_error_flag"=>'N',
                            "price_error_msg"=>'',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>$row['retail_discount_type'],
                            "discount_value"=>$row['retail_discount_amount'],
                            "retail_array"=>$final_retail_array_wo,
                            "total_array"=>$total_array_wo
                        );
                        if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    } elseif ($row['discount_limit_flag'] == 'Y' && $row['discount_limit_value'] == $row['discount_used_count']) {
                        $response_array = array(
                            "status"=>'Failed',
                            "discount_error_flag"=>'Y',
                            "discount_error_msg"=>'This discount code has already reached its limit for use',
                            "discount_usage_type"=>$row['retail_discount_usage_type'],
                            "price_error_flag"=>'N',
                            "price_error_msg"=>'',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>$row['retail_discount_type'],
                            "discount_value"=>$row['retail_discount_amount'],
                            "retail_array"=>$final_retail_array_wo,
                            "total_array"=>$total_array_wo
                        );
                        if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    }
                    
                    if ($row['retail_discount_usage_type'] == 'P') {
                        $final_return_array = $this->calculateCheckoutForRetail($company_id, $row['retail_discount_id'], $checkout_products, 'P', $retail_array, $disc_pr);
                        $final_retail_array = $final_return_array['retail_array'];
                        $total_array = $final_return_array['temp'];
                        $total_array['discount_id'] = $row['retail_discount_id'];
                        $total_array['total_quantity'] = $total_qty;
                        if (isset($total_array['discount_error'])) {
                            $response_array = array(
                                    "status" => 'Failed',
                                    "discount_error_flag" => 'Y',
                                    "discount_error_msg" => $total_array['discount_error'],
                                    "discount_usage_type" => $row['retail_discount_usage_type'],
                                    "price_error_flag" => 'N',
                                    "price_error_msg" => '',
                                    "optional_discount_error_flag"=>'N',
                                    "discount_type" => $row['retail_discount_type'],
                                    "discount_value" => $row['retail_discount_amount'],
                                    "retail_array" => $final_retail_array_wo,
                                    "total_array" => $total_array_wo
                                );
                                if ($callBack == 0) {
                                    $this->response($this->json($response_array), 200);
                                } else {
                                    return $response_array;
                                }
                        }
                        
                        if ($row['min_purchase_flag'] == 'V' && $total_array['total_order_price'] < $row['min_purchase_amount']) {
//                            $res = array('status' => "Failed", "msg" => "Required conditions have not been met in order to use this code", "retail_array" => $final_retail_array, 'total'=>$total_array);
                            $response_array = array(
                                    "status" => 'Failed',
                                    "discount_error_flag" => 'Y',
                                    "discount_error_msg" => 'Minimum purchase amount ($'.$row['min_purchase_amount'].') have not been met for this discount',
                                    "discount_usage_type" => $row['retail_discount_usage_type'],
                                    "price_error_flag" => 'N',
                                    "price_error_msg" => '',
                                    "optional_discount_error_flag"=>'N',
                                    "discount_type" => $row['retail_discount_type'],
                                    "discount_value" => $row['retail_discount_amount'],
                                    "retail_array" => $final_retail_array_wo,
                                    "total_array" => $total_array_wo
                                );
                            if ($callBack == 0) {
                                $this->response($this->json($response_array), 200);
                            } else {
                                return $response_array;
                            }
                        }
                        
//                        if ($final_retail_array['discount_usage_count'] > 0) {
                            for ($i = 0; $i < count($final_retail_array); $i++) {
                                if (isset($final_retail_array[$i]['discount_error_flag']) && $final_retail_array[$i]['discount_error_flag'] == 'Y') {
//                                    $res = array('status' => "Failed", "msg" => "Required conditions have not been met in order to use this code", "retail_array" => $final_retail_array, 'total'=>$total_array);
                                $response_array = array(
                                    "status" => 'Failed',
                                    "discount_error_flag" => 'Y',
                                    "discount_error_msg" => 'Required conditions have not been met in order to use this code',
                                    "discount_usage_type" => $row['retail_discount_usage_type'],
                                    "price_error_flag" => 'N',
                                    "price_error_msg" => '',
                                    "optional_discount_error_flag"=>'N',
                                    "discount_type" => $row['retail_discount_type'],
                                    "discount_value" => $row['retail_discount_amount'],
                                    "retail_array" => $final_retail_array_wo,
                                    "total_array" => $total_array_wo
                                );
                                if ($callBack == 0) {
                                    $this->response($this->json($response_array), 200);
                                } else {
                                    return $response_array;
                                }
                            }
                        }
//                        }
                        if ($row['discount_limit_flag'] == 'Y' && $total_array['discount_usage_count'] > ($row['discount_limit_value'] - $row['discount_used_count'])) {
//                            $res = array('status' => "Failed", "msg" => "This discount code has already reached its limit for use", "retail_array" => $retail_array, 'total'=>$total_array);
                            $response_array = array(
                                    "status" => 'Failed',
                                    "discount_error_flag" => 'Y',
                                    "discount_error_msg" => 'This discount code has already reached its limit for use',
                                    "discount_usage_type" => $row['retail_discount_usage_type'],
                                    "price_error_flag" => 'N',
                                    "price_error_msg" => '',
                                    "optional_discount_error_flag"=>'N',
                                    "discount_type" => $row['retail_discount_type'],
                                    "discount_value" => $row['retail_discount_amount'],
                                    "retail_array" => $final_retail_array_wo,
                                    "total_array" => $total_array_wo
                                );
                            if ($callBack == 0) {
                                $this->response($this->json($response_array), 200);
                            } else {
                                return $response_array;
                            }
                        }
                    } else if ($row['retail_discount_usage_type'] == 'O') {
                        $final_return_array = $this->calculateCheckoutForRetail($company_id, '', $checkout_products, 'O', $retail_array, $disc_pr);
                        $final_retail_array = $final_return_array['retail_array'];
                        $total_array = $final_return_array['temp'];
                        $total_array['discount_id'] = $row['retail_discount_id'];
                        $total_array['total_quantity'] = $total_qty;
                        if ($row['min_purchase_flag'] == 'V' && $total_array['total_order_price'] < $row['min_purchase_amount']) {
//                            $res = array('status' => "Failed", "msg" => "Required conditions have not been met in order to use this code", "retail_array" => $final_retail_array, 'total'=>$total_array);
                            $response_array = array(
                                    "status" => 'Failed',
                                    "discount_error_flag" => 'Y',
                                    "discount_error_msg" => 'Minimum purchase amount ($'.$row['min_purchase_amount'].') have not been met for this discount',
                                    "discount_usage_type" => $row['retail_discount_usage_type'],
                                    "price_error_flag" => 'N',
                                    "price_error_msg" => '',
                                    "optional_discount_error_flag"=>'N',
                                    "discount_type" => $row['retail_discount_type'],
                                    "discount_value" => $row['retail_discount_amount'],
                                    "retail_array" => $final_retail_array_wo,
                                    "total_array" => $total_array_wo
                                );
                            if ($callBack == 0) {
                                $this->response($this->json($response_array), 200);
                            } else {
                                return $response_array;
                            }
                        }
                        if ($row['retail_discount_type'] == 'V' && $row['retail_discount_amount'] > $total_array['total_order_price']) {
//                            $res = array('status' => "Failed", "msg" => "Required conditions have not been met in order to use this code", "retail_array" => $final_retail_array, 'total'=>$total_array);
                                $response_array = array(
                                "status"=>'Failed',
                                "discount_error_flag"=>'Y',
                                "discount_error_msg"=>'Required conditions have not been met in order to use this code',
                                "discount_usage_type"=>$row['retail_discount_usage_type'],
                                "price_error_flag"=>'N',
                                "price_error_msg"=>'',
                                "optional_discount_error_flag"=>'N',
                                "discount_type"=>$row['retail_discount_type'],
                                "discount_value"=>$row['retail_discount_amount'],
                                "retail_array"=>$final_retail_array_wo,
                                "total_array"=>$total_array_wo
                            );
                            if ($callBack == 0) {
                                $this->response($this->json($response_array), 200);
                            } else {
                                return $response_array;
                            }
                        } else if ($row['retail_discount_type'] !== 'P') {
                           $total_price = $total_discount = $total_tax = $total_order_price = 0; 
                            for($k=0; $k < count($final_retail_array); $k++){
                                $disc_percent_value = ($final_retail_array[$k]['retail_total_product_price']/$total_array['total_order_price']) * $disc_pr['disc_amount'];
                                $final_retail_array[$k]['discounted_value'] = $disc_percent_value;
                                $final_retail_array[$k]['retail_discounted_price'] = $final_retail_array[$k]['retail_discounted_price'] - $disc_percent_value;
                                $tax_percent_value = ($final_retail_array[$k]['retail_discounted_price']) * ($final_retail_array[$k]['product_tax_rate']/100);
                                $final_retail_array[$k]['product_tax_value'] = $tax_percent_value;
                                $total_tax += $tax_percent_value;
                                $total_price += $final_retail_array[$k]['retail_discounted_price']+$final_retail_array[$k]['product_tax_value'];
                                $total_discount += $disc_percent_value;
                                $total_order_price += ($final_retail_array[$k]['retail_product_price'] * $final_retail_array[$k]['quantity']) + $tax_percent_value;
                            }
                            $total_array['total_tax_value'] = round($total_tax,2);
                            $total_array['total_discounted_price'] = $total_price;
                            $total_array['total_discount_value'] = round($total_discount,2);
                            $total_array['total_order_price_without_discount'] = round($total_order_price,2);
                        }
                    }
                    if ($total_array['total_discounted_price'] < 5 && $total_array['total_discounted_price'] > 0 && $checkout_flag == 'Y') {
//                        $res = array('status' => "Failed", "msg" => "Purchase amount should be greater than 5.", "retail_array" => $final_retail_array, 'total'=>$total_array);
                        $response_array = array(
                            "status"=>'Failed',
                            "discount_error_flag"=>'N',
                            "discount_error_msg"=>'',
                            "discount_usage_type"=>$row['retail_discount_usage_type'],
                            "price_error_flag"=>'Y',
                            "price_error_msg"=>'Purchase amount should be greater than '.$currency_symbol.'5.',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>$row['retail_discount_type'],
                            "discount_value"=>$row['retail_discount_amount'],
                            "retail_array"=>$final_retail_array,
                            "total_array"=>$total_array
                        );
                        if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                    }
                    $total_array['processing_fee_type'] = $row['retail_processingfee'];
                    if ($total_array['total_discounted_price'] <= 0 || $payment_method == 'CH' || $payment_method == 'CA') {
                        $total_array['total_processing_fee'] = 0;
                    } else {
                        $total_array['total_processing_fee'] = round($this->calculateProcessingFee($total_array['total_discounted_price'], $row['retail_processingfee'], $process_fee_per, $process_fee_val),2);
                    }
                    if ($row['retail_processingfee'] == 2) {
                        $total_array['total_discounted_price'] += $total_array['total_processing_fee'];
                        $total_array['total_order_price_without_discount'] += $total_array['total_processing_fee'];
                    }
//                    $res = array('status' => "Success", "msg" => "Discount Code Applied.", "retail_array" => $final_retail_array, 'total'=>$total_array);
                    $total_array['total_discounted_price'] = round($total_array['total_discounted_price'],2);
                    $response_array = array(
                            "status"=>'Success',
                            "discount_error_flag"=>'N',
                            "discount_error_msg"=>'',
                            "discount_usage_type"=>$row['retail_discount_usage_type'],
                            "price_error_flag"=>'N',
                            "price_error_msg"=>'',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>$row['retail_discount_type'],
                            "discount_value"=>$row['retail_discount_amount'],
                            "retail_array"=>$final_retail_array,
                            "total_array"=>$total_array
                        );
                    if ($callBack == 0) {
                            $this->response($this->json($response_array), 200);
                        } else {
                            return $response_array;
                        }
                }
            } else {
                $final_return_array = $this->retailProductsPriceCalculationForNoDiscount($company_id, $checkout_products, $retail_array);
                $final_retail_array = $final_return_array['retail_array'];
                $total_array = $final_return_array['temp'];
                $total_array['discount_id'] = '';
                $total_array['total_quantity'] = $total_qty;
                
                $without_disc_query = sprintf("SELECT c.`upgrade_status`, c.`retail_processingfee`, c.`wp_currency_symbol` FROM `company` c 
                 WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result_wodisc = mysqli_query($this->db, $without_disc_query);
            if (!$result_wodisc) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$without_disc_query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows_wodisc = mysqli_num_rows($result_wodisc);
                if ($num_rows_wodisc > 0) {
                    $row1 = mysqli_fetch_assoc($result_wodisc);
                    $currency_symbol = $row1['wp_currency_symbol'];
                    $p_f = $this->getProcessingFee($company_id, $row1['upgrade_status']);
                    $process_fee_per = $p_f['PP'];
                    $process_fee_val = $p_f['PT'];
                }
            }
                if ($total_array['total_discounted_price'] < 5 && $total_array['total_discounted_price'] > 0 && $checkout_flag == 'Y') {
//                        $res = array('status' => "Failed", "msg" => "Purchase amount should be greater than 5.", "retail_array" => $final_retail_array, 'total'=>$total_array);
                    $response_array = array(
                        "status" => 'Failed',
                        "discount_error_flag" => 'N',
                        "discount_error_msg" => '',
                        "discount_usage_type" => $row['retail_discount_usage_type'],
                        "price_error_flag" => 'Y',
                        "price_error_msg" => 'Purchase amount should be greater than '.$currency_symbol.'5.',
                        "optional_discount_error_flag"=>'N',
                        "discount_type" => $row['retail_discount_type'],
                        "discount_value" => $row['retail_discount_amount'],
                        "retail_array" => $final_retail_array,
                        "total_array" => $total_array
                    );
                    if ($callBack == 0) {
                        $this->response($this->json($response_array), 200);
                    } else {
                        return $response_array;
                    }
                }
                $total_array['processing_fee_type'] = $row1['retail_processingfee'];
                if ($total_array['total_discounted_price'] <= 0 || $payment_method == 'CH' || $payment_method == 'CA') {
                    $total_array['total_processing_fee'] = 0;
                } else {
                    $total_array['total_processing_fee'] = round($this->calculateProcessingFee($total_array['total_discounted_price'], $row1['retail_processingfee'], $process_fee_per, $process_fee_val), 2);
                }
                if ($row1['retail_processingfee'] == 2) {
                    $total_array['total_discounted_price'] += $total_array['total_processing_fee'];
                    $total_array['total_order_price_without_discount'] += $total_array['total_processing_fee'];
                }
                $total_array['total_discounted_price'] = round($total_array['total_discounted_price'],2);
                $response_array = array(
                            "status"=>'Failed',
                            "discount_error_flag"=>'Y',
                            "discount_error_msg"=>'The discount code does not exists',
                            "discount_usage_type"=>'',
                            "price_error_flag"=>'N',
                            "price_error_msg"=>'',
                            "optional_discount_error_flag"=>'N',
                            "discount_type"=>'',
                            "discount_value"=>'',
                            "retail_array"=>$final_retail_array,
                            "total_array"=>$total_array
                        );
//                $res = array('status' => "Failed", "msg" => "The discount code you entered is not valid", 'retail_array' => $retail_array, 'total'=>$total_array);
                if ($callBack == 0) {
                    $this->response($this->json($response_array), 200);
                } else {
                    return $response_array;
                }
            }
        }
    }

    public function calculateCheckoutForRetail($company_id, $discount_id, $retail_product_ids, $type, $retail_array, $discount_array) {
        $product_unique = $temp = [];
        $total_price = $total_tax = $discount_usage_count = $total_discount = $total_order_price = $total_order_price_without_tax = 0;
        $selected_product_ids = explode(",", $retail_product_ids);
        if ($type === 'O') {
            $sql = sprintf("SELECT rp.`retail_product_id`, rp.`retail_product_title`,rv.`retail_variants_name` retail_variant_title,rp.`retail_banner_img_url`,rp.`retail_variant_flag`,IFNULL(rp.`product_tax_rate`,0) product_tax_rate,
                IF(retail_variant_flag='N', rp.`retail_product_price`, rv.`variant_price`) retail_product_price,rv.`retail_variant_id`
                FROM `retail_products` rp LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.`deleted_flag`='N'
                WHERE rp.`company_id` = '%s' AND rp.`retail_product_id` in (%s) AND rp.`retail_product_status`='P'", 
                    mysqli_real_escape_string($this->db, $company_id), $retail_product_ids);
            $result = mysqli_query($this->db, $sql);
//             log_info($sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result);
                if ($num_rows > 0) {
                    while ($rows = mysqli_fetch_assoc($result)) {
                        $product_id = $rows['retail_product_id'];
                        $variant_id = $rows['retail_variant_id'];
                        for ($i = 0; $i < count($retail_array); $i++) {
                            if(!empty($retail_array[$i]['retail_variant_id']) && $retail_array[$i]['retail_product_id'] == $product_id && $retail_array[$i]['retail_variant_id'] == $variant_id){
                                $retail_array[$i]['is_discount_applied'] = 'N';
                                if($rows['retail_product_price'] > 0){
                                    $discount_usage_count = 1;
                                    $retail_array[$i]['is_discount_applied'] = 'Y';
                                }
                                $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                if ($discount_array['disc_type'] == 'P'){
                                    $disc_percent_value = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) * ($discount_array['disc_amount'] / 100);
                                }else{
                                    $disc_percent_value = 0;
                                }
                                $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value) * ($rows['product_tax_rate']/100);
                                $total_tax += $tax_percent_value;
                                $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                $retail_array[$i]['discount_error_flag'] = 'N';
                                $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                $retail_array[$i]['discounted_value'] = $disc_percent_value;
                                $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value;
                                $total_price += ($retail_array[$i]['retail_discounted_price'])+$tax_percent_value;
                                $total_discount += $disc_percent_value;
                                $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                break;
                            }else if ($retail_array[$i]['retail_product_id'] == $product_id && empty($retail_array[$i]['retail_variant_id'])) {
                                $retail_array[$i]['is_discount_applied'] = 'N';
                                if($rows['retail_product_price'] > 0){
                                    $discount_usage_count = 1;
                                    $retail_array[$i]['is_discount_applied'] = 'Y';
                                }
                                $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                if ($discount_array['disc_type'] == 'P'){
                                    $disc_percent_value = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) * ($discount_array['disc_amount'] / 100);
                                }else{
                                    $disc_percent_value = 0;
                                }
                                $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value) * ($rows['product_tax_rate']/100);
                                $total_tax += $tax_percent_value;
                                $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                $retail_array[$i]['discount_error_flag'] = 'N';
                                $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                $retail_array[$i]['discounted_value'] = $disc_percent_value;
                                $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value;
                                $total_price += ($retail_array[$i]['retail_discounted_price'])+$tax_percent_value;
                                $total_discount += $disc_percent_value;
                                $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                break;
                            }
                        }
                    }
                }
                $temp['total_order_price'] = $total_order_price_without_tax;
                $temp['total_order_price_without_discount'] = round($total_order_price,2);
                $temp['total_discounted_price'] = $total_price;
                $temp['total_order_compare_price'] = 0;
                $temp['total_tax_value'] = round($total_tax,2);
                $temp['discount_usage_count'] = $discount_usage_count;
                $temp['total_discount_value'] = round($total_discount,2);
                $return_array = array('retail_array'=>$retail_array, 'temp'=>$temp);
                return $return_array;
            }
        } else if ($type === 'P') {
            $sql = sprintf("SELECT rp.`retail_product_id`, rp.`retail_product_title`,rv.`retail_variants_name` retail_variant_title,rp.`retail_banner_img_url`,rp.`retail_variant_flag`,IFNULL(rp.`product_tax_rate`,0) product_tax_rate,
                IF(retail_variant_flag='N', rp.`retail_product_price`, rv.`variant_price`) retail_product_price,rv.`retail_variant_id`
                FROM `retail_products` rp LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.`deleted_flag`='N'
                WHERE rp.`company_id` = '%s' AND rp.`retail_product_id` in (SELECT retail_product_id FROM `retail_discount_products` rdp 
                WHERE rdp.`company_id` = '%s' AND retail_product_id in(%s) AND rdp.`retail_discount_id`='$discount_id') AND rp.`retail_product_status`='P'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_ids));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result);
                if ($num_rows > 0) {
                    while ($rows = mysqli_fetch_assoc($result)) {
                        $product_id = $rows['retail_product_id'];
                        if (!in_array($rows['retail_product_id'], $product_unique)) {
                            $product_unique[] = $rows['retail_product_id'];
                        }
                        $variant_id = $rows['retail_variant_id'];
                        for ($i = 0; $i < count($retail_array); $i++) {
                            if (!empty($retail_array[$i]['retail_variant_id']) && $retail_array[$i]['retail_product_id'] == $product_id && $retail_array[$i]['retail_variant_id'] == $variant_id) {
                                $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                if ($discount_array['disc_type'] == 'V'){
                                    if(($rows['retail_product_price'] * $retail_array[$i]['quantity']) < $discount_array['disc_amount']) {
                                        $retail_array[$i]['discount_error_flag'] = 'Y';
                                        $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity']) * ($rows['product_tax_rate']/100);
                                        $total_tax += $tax_percent_value;
                                        $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']);
                                        $retail_array[$i]['discounted_value'] = 0;
                                        $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                        $total_discount += $discount_array['disc_amount'];
                                        $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                        $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                        $retail_array[$i]['is_discount_applied'] = 'N';
                                        $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                        $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                    }else{
                                        $retail_array[$i]['discount_error_flag'] = 'N';
                                        $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $discount_array['disc_amount']) * ($rows['product_tax_rate'] / 100);
                                        $total_tax += $tax_percent_value;
                                        $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) - $discount_array['disc_amount'];
                                        $retail_array[$i]['discounted_value'] = $discount_array['disc_amount'];
                                        $total_discount += $discount_array['disc_amount'];
                                        $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                        $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                        $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                        $retail_array[$i]['is_discount_applied'] = 'Y';
                                        $retail_array[$i]['retail_product_title'] = $rows['retail_product_title'];
                                        $retail_array[$i]['retail_variant_title'] = $rows['retail_variant_title'];
                                        if ($rows['retail_product_price'] > 0) {
                                            $discount_usage_count++;
                                        }
                                    }
                                } else if ($discount_array['disc_type'] == 'P') {
                                    $retail_array[$i]['discount_error_flag'] = 'N';
                                    $disc_percent_value = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) * ($discount_array['disc_amount'] / 100);
                                    $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value) * ($rows['product_tax_rate'] / 100);
                                    $total_tax += $tax_percent_value;
                                    $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) - $disc_percent_value;
                                    $retail_array[$i]['discounted_value'] = $disc_percent_value;
                                    $total_discount += $disc_percent_value;
                                    $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                    $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                    $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                    $retail_array[$i]['retail_product_title'] = $rows['retail_product_title'];
                                    $retail_array[$i]['retail_variant_title'] = $rows['retail_variant_title'];
                                    if ($rows['retail_product_price'] > 0) {
                                        $discount_usage_count++;
                                         $retail_array[$i]['is_discount_applied'] = 'Y';
                                    }else{
                                         $retail_array[$i]['is_discount_applied'] = 'N';
                                    }
                                }
                                $total_price += $retail_array[$i]['retail_discounted_price']+$tax_percent_value;
                                $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                break;
                            }elseif ($retail_array[$i]['retail_product_id'] == $product_id && empty($retail_array[$i]['retail_variant_id'])) {
                                $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                if ($discount_array['disc_type'] == 'V'){
                                    if(($rows['retail_product_price'] * $retail_array[$i]['quantity']) < $discount_array['disc_amount']) {
                                       $retail_array[$i]['discount_error_flag'] = 'Y';
                                        $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity']) * ($rows['product_tax_rate']/100);
                                        $total_tax += $tax_percent_value;
                                        $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']);
                                        $retail_array[$i]['discounted_value'] = 0;
                                        $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                        $total_discount += $discount_array['disc_amount'];
                                        $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                        $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                        $retail_array[$i]['is_discount_applied'] = 'N';
                                        $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                        $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                    } else {
                                        $retail_array[$i]['discount_error_flag'] = 'N';
                                        $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $discount_array['disc_amount']) * ($rows['product_tax_rate'] / 100);
                                        $total_tax += $tax_percent_value;
                                        $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) - $discount_array['disc_amount'];
                                        $retail_array[$i]['discounted_value'] = $discount_array['disc_amount'];
                                        $total_discount += $discount_array['disc_amount'];
                                        $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                        $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                        $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                        $retail_array[$i]['is_discount_applied'] = 'Y';
                                        $retail_array[$i]['retail_product_title'] = $rows['retail_product_title'];
                                        $retail_array[$i]['retail_variant_title'] = $rows['retail_variant_title'];
                                        if ($rows['retail_product_price'] > 0) {
                                            $discount_usage_count++;
                                        }
                                    }
                                } else if ($discount_array['disc_type'] == 'P') {
                                    $retail_array[$i]['discount_error_flag'] = 'N';
                                    $disc_percent_value = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) * ($discount_array['disc_amount'] / 100);
                                    $tax_percent_value = ($rows['retail_product_price'] * $retail_array[$i]['quantity'] - $disc_percent_value) * ($rows['product_tax_rate'] / 100);
                                    $total_tax += $tax_percent_value;
                                    $retail_array[$i]['retail_discounted_price'] = ($retail_array[$i]['retail_product_price'] * $retail_array[$i]['quantity']) - $disc_percent_value;
                                    $retail_array[$i]['discounted_value'] = $disc_percent_value;
                                    $total_discount += $disc_percent_value;
                                    $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                    $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                    $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                    $retail_array[$i]['retail_product_title'] = $rows['retail_product_title'];
                                    $retail_array[$i]['retail_variant_title'] = $rows['retail_variant_title'];
                                    if ($rows['retail_product_price'] > 0) {
                                        $discount_usage_count++;
                                         $retail_array[$i]['is_discount_applied'] = 'Y';
                                    }else{
                                         $retail_array[$i]['is_discount_applied'] = 'N';
                                    }
                                }
                                $total_price += $retail_array[$i]['retail_discounted_price']+$tax_percent_value;
                                $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                break;
                            }
                        }
                    }
                    $product_diff_array = array_diff($selected_product_ids, $product_unique);
                    $product_without_disconts = implode(",", $product_diff_array);
                    if (!empty($product_without_disconts)) {
                        $sql1 = sprintf("SELECT rp.`retail_product_id`, rp.`retail_product_title`,rv.`retail_variants_name` retail_variant_title,rp.`retail_banner_img_url`,rp.`retail_variant_flag`,IFNULL(rp.`product_tax_rate`,0) product_tax_rate,
                                IF(retail_variant_flag='N', rp.`retail_product_price`, rv.`variant_price`) retail_product_price,rv.`retail_variant_id`
                                FROM `retail_products` rp LEFT JOIN `retail_variants` rv ON rp.`company_id`=rv.`company_id` AND rp.`retail_product_id`=rv.`retail_product_id` AND rv.`deleted_flag`='N'
                                WHERE rp.`company_id` = '%s' AND rp.`retail_product_id` in (%s) AND rp.`retail_product_status`='P'",
                                mysqli_real_escape_string($this->db, $company_id), $product_without_disconts);
                        $result1 = mysqli_query($this->db, $sql1);
                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        } else {
                            $num_rows = mysqli_num_rows($result1);
                            if ($num_rows > 0) {
                                while ($rows = mysqli_fetch_assoc($result1)) {
                                    $product_id = $rows['retail_product_id'];
                                    $variant_id = $rows['retail_variant_id'];
                                    for ($i = 0; $i < count($retail_array); $i++) {
                                        if (!empty($retail_array[$i]['retail_variant_id']) && $retail_array[$i]['retail_product_id'] == $product_id && $retail_array[$i]['retail_variant_id'] == $variant_id) {
                                            $retail_array[$i]['discount_error_flag'] = 'N';
                                            $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                            $tax_percent_value = $rows['retail_product_price'] * $retail_array[$i]['quantity'] * ($rows['product_tax_rate'] / 100);
                                            $total_tax += $tax_percent_value;
                                            $total_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                            $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                            $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['discounted_value'] = 0;
                                            $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                            $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                            $retail_array[$i]['is_discount_applied'] = 'N';
                                            $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                            $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                            break;
                                        } else if ($retail_array[$i]['retail_product_id'] == $product_id && empty($retail_array[$i]['retail_variant_id'])) {
                                            $retail_array[$i]['discount_error_flag'] = 'N';
                                            $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                                            $tax_percent_value = $rows['retail_product_price'] * $retail_array[$i]['quantity'] * ($rows['product_tax_rate'] / 100);
                                            $total_tax += $tax_percent_value;
                                            $total_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                            $total_order_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                                            $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                                            $retail_array[$i]['discounted_value'] = 0;
                                            $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                                            $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                                            $retail_array[$i]['is_discount_applied'] = 'N';
                                            $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                                            $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $temp['discount_usage_count'] = $discount_usage_count;
                    $temp['total_order_price'] = $total_order_price_without_tax;
                    $temp['total_order_price_without_discount'] = round($total_order_price,2);
                    $temp['total_discounted_price'] = $total_price;
                    $temp['total_tax_value'] = round($total_tax,2);
                    $temp['total_discount_value'] = round($total_discount,2);
                    $return_array = array('retail_array'=>$retail_array, 'temp'=>$temp);
                    return $return_array;
                }else{
                    $return_array = $this->retailProductsPriceCalculationForNoDiscount($company_id,$retail_product_ids,$retail_array);
                    $return_array['temp']['discount_error'] = "Given Discount Code not applicable for the selected products.";
                    return $return_array;
                }
            }
        }
    }

    private function calculateProcessingFee($amount, $fee_type, $process_fee_per, $process_fee_val) {
        $p1_fee = ($amount * ($process_fee_per / 100)) + $process_fee_val;
        $p2_fee = ($amount + $p1_fee) * ($process_fee_per / 100) + $process_fee_val;
        if ($fee_type === 2 || $fee_type === '2') {
            $p_fee = $p2_fee;
        } else {
            $p_fee = $p1_fee;
        }
        return $p_fee;
    }
    
    public function retailProductsPriceCalculationForNoDiscount($company_id, $retail_product_ids, $retail_array) {
        $total_price = $total_tax = $total_order_price_without_tax = $total_order_compare_price = 0;
        
        $sql = sprintf("SELECT rp.`retail_product_id`, rp.`retail_product_title`,rv.`retail_variants_name` retail_variant_title,rp.`retail_banner_img_url`,rp.`retail_variant_flag`,IFNULL(rp.`product_tax_rate`,0) product_tax_rate,
                IF(retail_variant_flag='N', rp.`retail_product_price`, rv.`variant_price`) retail_product_price,IF(retail_variant_flag='N', rp.`retail_product_compare_price`, rv.`variant_compare_price`) retail_product_compare_price,rv.`retail_variant_id`
                FROM `retail_products` rp LEFT JOIN `retail_variants` rv ON rp.`retail_product_id`=rv.`retail_product_id` AND rv.`deleted_flag`='N'
                WHERE rp.`company_id` = '%s' AND rp.`retail_product_id` in (%s) AND rp.`retail_product_status`='P'", 
                mysqli_real_escape_string($this->db, $company_id), $retail_product_ids);
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    $product_id = $rows['retail_product_id'];
                    $variant_id = $rows['retail_variant_id'];
                    for ($i = 0; $i < count($retail_array); $i++) {
                        if (!empty($retail_array[$i]['retail_variant_id']) && $retail_array[$i]['retail_product_id'] == $product_id && $retail_array[$i]['retail_variant_id'] == $variant_id) {
                            $tax_percent_value = $rows['retail_product_price'] * $retail_array[$i]['quantity'] * ($rows['product_tax_rate'] / 100);
                            $total_tax += $tax_percent_value;
                            $total_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                            $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                            $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                            $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                            $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $retail_array[$i]['retail_product_compare_price'] = $rows['retail_product_compare_price'] * $retail_array[$i]['quantity'];
                            $retail_array[$i]['discount_error_flag'] = 'N';
                            $retail_array[$i]['is_discount_applied'] = 'N';
                            $retail_array[$i]['discounted_value'] = 0;
                            $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                            $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                            $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $total_order_compare_price += $retail_array[$i]['retail_product_compare_price'];
                            break;
                        } else if ($retail_array[$i]['retail_product_id'] == $product_id && empty($retail_array[$i]['retail_variant_id'])) {
                            $tax_percent_value = $rows['retail_product_price'] * $retail_array[$i]['quantity'] * ($rows['product_tax_rate'] / 100);
                            $total_tax += $tax_percent_value;
                            $total_price += ($rows['retail_product_price'] * $retail_array[$i]['quantity']) + $tax_percent_value;
                            $retail_array[$i]['retail_product_price'] = $rows['retail_product_price'];
                            $retail_array[$i]['product_tax_rate'] = $rows['product_tax_rate'];
                            $retail_array[$i]['product_tax_value'] = $tax_percent_value;
                            $retail_array[$i]['retail_total_product_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $retail_array[$i]['retail_product_compare_price'] = $rows['retail_product_compare_price'] * $retail_array[$i]['quantity'];
                            $retail_array[$i]['discount_error_flag'] = 'N';
                            $retail_array[$i]['is_discount_applied'] = 'N';
                            $retail_array[$i]['discounted_value'] = 0;
                            $retail_array[$i]['retail_product_title'] =$rows['retail_product_title'];
                            $retail_array[$i]['retail_variant_title'] =$rows['retail_variant_title'];
                            $retail_array[$i]['retail_discounted_price'] = $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $total_order_price_without_tax += $rows['retail_product_price'] * $retail_array[$i]['quantity'];
                            $total_order_compare_price += $retail_array[$i]['retail_product_compare_price'];
                            break;
                        }
                    }
                }
            }
            $temp['total_order_price'] = $total_order_price_without_tax;
            $temp['total_order_price_without_discount'] = round($total_price,2);
            $temp['total_discounted_price'] = $total_price;
            $temp['total_order_compare_price'] = $total_order_compare_price;
            $temp['total_tax_value'] = $total_tax;
            $temp['discount_usage_count'] = 0;
            $temp['total_discount_value'] = 0;
            $return_array = array('retail_array'=>$retail_array, 'temp'=>$temp);
            return $return_array;
        }
    }

    public function wepayCreateretailCheckout($optional_discount_flag,$optional_discount_amount,$check_number,$payment_method,$company_id, $actual_student_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code,$buyer_street,$buyer_city,$buyer_state,$buyer_zip, $country, $cc_id, $cc_state, $discount_code, $cart_details, $order_type_user,$order_version_user){
        $this->getStudioSubscriptionStatus($company_id);
        
        $selecsql=sprintf("SELECT c.retail_enabled FROM `company` c WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $selectresult= mysqli_query($this->db,$selecsql);
        if (!$selectresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else{
            if (mysqli_num_rows($selectresult) > 0) {
                $statusoutput = mysqli_fetch_assoc($selectresult);
                $retail_enable_flag = $statusoutput['retail_enabled'];
                if($statusoutput['retail_enabled']=='N'){
                    $res = array('status' => "Failed", "msg" => "Retail Setting is disabled. Please contact the business operator for more information.");
                    $this->response($this->json($res), 200);
                }
            }
        }
        
        $retail_array=$this->checkRetailQuantityDuringCheckout($company_id,trim($discount_code),$cart_details, 'Y', 1, $optional_discount_flag, $optional_discount_amount, $payment_method);
        if($retail_array['price_error_flag'] == 'Y'){
            $error = array("status" => "Failed", "msg" => $retail_array['price_error_msg']);
            $this->response($this->json($error),200);
        }
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;  
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $actual_student_id;
        }
            
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $current_date = date("Y-m-d");
//        $check_deposit_failure = 0;
        $gmt_date=gmdate(DATE_RFC822);
        usort($retail_array['retail_array'], function($a, $b) {
            return $a['retail_product_id'] > $b['retail_product_id'];
        });
        
        if(count($retail_array['retail_array'])>0){
            for($i=0;$i<count($retail_array['retail_array']);$i++){
                if(isset($retail_array['retail_array'][$i]['retail_product_id']) && empty($retail_array['retail_array'][$i]['retail_product_id'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }            
                if(isset($retail_array['retail_array'][$i]['quantity']) && empty($retail_array['retail_array'][$i]['quantity'])){
                    $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                    $this->response($this->json($error),200);
                }                  
            }
        }
        
        $actual_order_amount=$retail_array['total_array']['total_order_price'];
        $payment_amount=$retail_array['total_array']['total_discounted_price'];
        $retail_order_discount_amount=$retail_array['total_array']['total_discount_value'];
        $discount_usage_count=$retail_array['total_array']['discount_usage_count'];        
        $retail_products_tax=$retail_array['total_array']['total_tax_value'];
        $fee = $retail_array['total_array']['total_processing_fee'];
        if(isset($retail_array['total_array']['discount_id'])){
            $discount = $retail_array['total_array']['discount_id'];
        }else{
            $discount = "";
        }

        $processing_fee_type = $retail_array['total_array']['processing_fee_type'];
        $retail_product_id1=$retail_array['retail_array'][0]['retail_product_id'];
        $retail_product_title1=$retail_array['retail_array'][0]['retail_product_title'];
        
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$retail_product_title1,"gmt_date"=>$gmt_date);               
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, wp.`account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency`, `wp_currency_symbol` FROM `wp_account` wp LEFT JOIN `company` c ON wp.company_id = c.company_id  WHERE wp.`company_id`='%s' ORDER BY `wp_user_id` DESC LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                $wp_currency_symbol = $row['wp_currency_symbol'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);$paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    $w_paid_amount = 0;
                    
                    if($payment_amount>0 && $payment_method == 'CC'){
                        $payment_type='CO';
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=retail";
                        $unique_id = $company_id."_".$retail_product_id1."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$retail_product_id1."_".$date;
                        $fee_payer = "payee_from_app";
                        $w_paid_amount = $paid_amount = $payment_amount;    //payment amount has added processing fee if fee_type is pass-on, no processing fee added if type is absorb
                        $paid_fee = $fee;
                                                
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$retail_product_title1","type"=>"goods","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $retail_product_title1","to_payer"=>"Payment has been made for $retail_product_title1"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$retail_product_title1", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
                    }else if($payment_amount == 0){
                        $payment_type='F';
                        $checkout_id_flag = 0;                        
                    }else{
                        $checkout_id_flag = 0;   
                        $payment_type='CO';
                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id;      
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            } 
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n";     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),200);
                                }
                            }
                        }
                    }
                    
                    $retail_order_discount_id = $retail_discount_value =  $retail_discount_min_req_value = 0;
                    $retail_order_discount_type = $retail_discount_applied_type = $retail_order_discount_code = $retail_discount_min_req_flag = '' ;
                    $select_discount=sprintf("SELECT * FROM `retail_discount` WHERE `retail_discount_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$discount),mysqli_real_escape_string($this->db,$company_id));
                    $result_disc=mysqli_query($this->db, $select_discount);
                    if(!$result_disc){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_discount");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $num_of_rows = mysqli_num_rows($result_disc);
                        if ($num_of_rows > 0) {
                            $row = mysqli_fetch_assoc($result_disc);
                            $retail_discount_value = $row['retail_discount_amount'];
                            $retail_order_discount_code = $row['retail_discount_code'];
                            $retail_discount_min_req_flag = $row['min_purchase_flag'];
                            $retail_discount_min_req_value = $row['min_purchase_quantity'];
                            $retail_order_discount_type = $row['retail_discount_type'];
                            $retail_discount_applied_type = $row['retail_discount_usage_type'];
                            $retail_order_discount_id = $row['retail_discount_id'];
                        }else {
                            if ($optional_discount_flag == 'Y' && $optional_discount_amount > 0) {
                                $retail_discount_value = $optional_discount_amount;
                                $retail_discount_min_req_flag = 'N';
                                $retail_order_discount_type = 'V';
                                $retail_discount_applied_type = 'O';
                            }
                        }
                    }
                    if($payment_method === "CA" || $payment_method === "CH"){//retail_order_paid_amount = 0 in db for cash and check
                        $query = sprintf("INSERT INTO `retail_orders`(retail_order_paid_amount,`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `student_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`,`buyer_street`,`buyer_city`,`buyer_state`,`buyer_zip`,`buyer_country`,`retail_order_date`,
                            `retail_order_amount`, `processing_fee_type`, `processing_fee_val`, `payment_type`, `retail_order_checkout_amount`,`retail_order_discount_amount`,`retail_discount_id`,`retail_discount_code`,`retail_discount_type`,`retail_discount_applied_type`,`retail_discount_value`,`retail_discount_min_req_flag`,
                            `retail_discount_min_req_value`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `retail_order_user_type`,`retail_order_user_version`,`retail_products_tax`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $student_id),
                            mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name),mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone),mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $buyer_street),mysqli_real_escape_string($this->db, $buyer_city),mysqli_real_escape_string($this->db, $buyer_state),mysqli_real_escape_string($this->db, $buyer_zip),mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $current_date),
                            mysqli_real_escape_string($this->db, $actual_order_amount),mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type),mysqli_real_escape_string($this->db, $payment_amount),mysqli_real_escape_string($this->db, $retail_order_discount_amount),
                            mysqli_real_escape_string($this->db, $retail_order_discount_id),mysqli_real_escape_string($this->db, $retail_order_discount_code),mysqli_real_escape_string($this->db, $retail_order_discount_type),mysqli_real_escape_string($this->db, $retail_discount_applied_type),mysqli_real_escape_string($this->db, $retail_discount_value),
                            mysqli_real_escape_string($this->db, $retail_discount_min_req_flag),mysqli_real_escape_string($this->db, $retail_discount_min_req_value),mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),mysqli_real_escape_string($this->db, $cc_month),
                            mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $order_type_user), mysqli_real_escape_string($this->db, $order_version_user), mysqli_real_escape_string($this->db, $retail_products_tax));
                        $payment_cash_string = "`actual_paid_date`, "; 
                        $payment_cash_date = "CURDATE(),"; 
                        $temp_payment_status = 'M';
                    }else{
                    $query = sprintf("INSERT INTO `retail_orders`(`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `student_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`,`buyer_street`,`buyer_city`,`buyer_state`,`buyer_zip`,`buyer_country`,`retail_order_date`,
                            `retail_order_amount`, `processing_fee_type`, `processing_fee_val`, `payment_type`, `retail_order_checkout_amount`,`retail_order_discount_amount`,`retail_discount_id`,`retail_discount_code`,`retail_discount_type`,`retail_discount_applied_type`,`retail_discount_value`,`retail_discount_min_req_flag`,
                            `retail_discount_min_req_value`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `retail_order_user_type`,`retail_order_user_version`,`retail_products_tax`)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $student_id),
                            mysqli_real_escape_string($this->db, $buyer_first_name),mysqli_real_escape_string($this->db, $buyer_last_name),mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone),mysqli_real_escape_string($this->db, $buyer_postal_code),
                            mysqli_real_escape_string($this->db, $buyer_street),mysqli_real_escape_string($this->db, $buyer_city),mysqli_real_escape_string($this->db, $buyer_state),mysqli_real_escape_string($this->db, $buyer_zip),mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $current_date),
                            mysqli_real_escape_string($this->db, $actual_order_amount),mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type),mysqli_real_escape_string($this->db, $payment_amount),mysqli_real_escape_string($this->db, $retail_order_discount_amount),
                            mysqli_real_escape_string($this->db, $retail_order_discount_id),mysqli_real_escape_string($this->db, $retail_order_discount_code),mysqli_real_escape_string($this->db, $retail_order_discount_type),mysqli_real_escape_string($this->db, $retail_discount_applied_type),mysqli_real_escape_string($this->db, $retail_discount_value),
                            mysqli_real_escape_string($this->db, $retail_discount_min_req_flag),mysqli_real_escape_string($this->db, $retail_discount_min_req_value),mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name),mysqli_real_escape_string($this->db, $cc_month),
                            mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $order_type_user), mysqli_real_escape_string($this->db, $order_version_user), mysqli_real_escape_string($this->db, $retail_products_tax));
                        $payment_cash_string = "";
                        $payment_cash_date = "";
                        $temp_payment_status = 'S';
                    }
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $retail_order_id = mysqli_insert_id($this->db);
                        
                        $payment_query = sprintf("INSERT INTO `retail_payment`($payment_cash_string`credit_method`,`check_number`,`retail_order_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `retail_tax_payment`) VALUES ($payment_cash_date'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number),
                                mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $fee),
                                mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $retail_products_tax));
                        $payment_result = mysqli_query($this->db, $payment_query);
                        if(!$payment_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
                            $update_discount=sprintf("UPDATE `retail_discount` SET `discount_used_count`=`discount_used_count`+$discount_usage_count WHERE `retail_discount_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$discount), mysqli_real_escape_string($this->db,$company_id));
                            $update_result = mysqli_query($this->db, $update_discount);
                            if(!$update_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_discount");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                        }
                        
                        
                        if(count($retail_array['retail_array'])>0){
                            $qty = $net_sales = 0;
                            $temp = $temp2= $temp3 = $cretail_product_cost=$cprocessing_fee=$cquantity=$cpayment_amount=0;
                            $cdiscount = $cretail_variant_id = $cretail_product_id = $cretail_parent_id = $note = $cretail_product_name = $cretail_variant_name= '';

                            for($i=0;$i<count($retail_array['retail_array']);$i++){
                                $cquantity =  $retail_array['retail_array'][$i]['quantity'];
                                $cdiscount = $retail_array['retail_array'][$i]['discounted_value'];
                                $cpayment_amount = $retail_array['retail_array'][$i]['retail_total_product_price'];
                                $cdiscounted_price = $retail_array['retail_array'][$i]['retail_discounted_price'];
                               
//                                $cprocessing_fee = $retail_array['retail_array'][$i]['processing_fee'];
                                if(!empty(trim($retail_array['retail_array'][$i]['retail_variant_id']))){
                                    $cretail_variant_id = $retail_array['retail_array'][$i]['retail_variant_id'];
                                }else{
                                    $cretail_variant_id = '0';
                                }
                                $cretail_product_id = $retail_array['retail_array'][$i]['retail_product_id'];
                                $cretail_parent_id = $retail_array['retail_array'][$i]['retail_parent_id'];
                                $cretail_product_name = $retail_array['retail_array'][$i]['retail_product_title'];
                                $cretail_variant_name = $retail_array['retail_array'][$i]['retail_variant_title'];
                                $cretail_product_cost = $retail_array['retail_array'][$i]['retail_product_price'];
                                $cretail_tax_rate = $retail_array['retail_array'][$i]['product_tax_rate'];
                                $cretail_tax_value = $retail_array['retail_array'][$i]['product_tax_value'];
                                $final_net_update = $cpayment_amount-$cdiscount+$cretail_tax_value;
                                
                                $cstatus = 'N';
                                if($cdiscounted_price==0){
                                    $cstatus = 'S';
                                }
                                $str = $str1 = '';
                                if (empty($cretail_parent_id) || $cretail_parent_id == 0) {
                                    $str = "IN (%s)";
                                    $str1 = 'NULL';
                                    $this->addretailDimensions($company_id, $cretail_product_id, '', '');
                                    if ($payment_method === 'CH' || $payment_method === 'CA') {
                                        $this->updateRetailDimensionsNetSales($company_id, $cretail_product_id, '', '', $final_net_update);
                                    }
                                } else {
                                    $str = "IN (%s, $cretail_parent_id)";
                                    $str1 = "$cretail_parent_id";
                                    $this->addretailDimensions($company_id, $cretail_parent_id, $cretail_product_id, '');
                                    if ($payment_method === 'CH' || $payment_method === 'CA') {
                                        $this->updateRetailDimensionsNetSales($company_id, $cretail_parent_id, $cretail_product_id, '', $final_net_update);
                                    }
                                }
                                if($payment_method === 'CH' || $payment_method === 'CA'){
                                    $reg_det_query = sprintf("INSERT INTO `retail_order_details`(`company_id`,`retail_order_id`, `retail_product_id`, `retail_parent_id`,`retail_variant_id`,`retail_product_name`,`retail_variant_name`, `retail_cost`, `retail_discount`, `retail_quantity`, `retail_payment_amount` , `retail_payment_status`, `retail_tax_percentage`, `rem_due`) VALUES ('%s','%s','%s',%s,%s,'%s','%s','%s','%s','%s','%s','S','%s',0)",
                                        mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $cretail_product_id), mysqli_real_escape_string($this->db, $str1), mysqli_real_escape_string($this->db, $cretail_variant_id), mysqli_real_escape_string($this->db, $cretail_product_name), mysqli_real_escape_string($this->db, $cretail_variant_name),
                                        mysqli_real_escape_string($this->db, $cretail_product_cost),mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cretail_tax_rate));

                                    $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str",
                                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $cretail_product_id));
                                    $result_count = mysqli_query($this->db, $update_count);
                                    if(!$result_count){
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }else{                      
                                    $reg_det_query = sprintf("INSERT INTO `retail_order_details`(`company_id`,`retail_order_id`, `retail_product_id`, `retail_parent_id`,`retail_variant_id`,`retail_product_name`,`retail_variant_name`, `retail_cost`, `retail_discount`, `retail_quantity`, `retail_payment_amount` , `retail_payment_status`, `retail_tax_percentage`, `rem_due`) VALUES ('%s','%s','%s',%s,%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $cretail_product_id), mysqli_real_escape_string($this->db, $str1), mysqli_real_escape_string($this->db, $cretail_variant_id), mysqli_real_escape_string($this->db, $cretail_product_name), mysqli_real_escape_string($this->db, $cretail_variant_name),
                                        mysqli_real_escape_string($this->db, $cretail_product_cost),mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cstatus), mysqli_real_escape_string($this->db, $cretail_tax_rate), mysqli_real_escape_string($this->db, $cpayment_amount-$cdiscount+$cretail_tax_value));
                                }
                                $reg_det_result = mysqli_query($this->db, $reg_det_query);
                                if(!$reg_det_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }else{
                                    $inserted_retail_order_detail_id = mysqli_insert_id($this->db);
                                }
                                
                                $qty += $cquantity;
                                if(!empty($cretail_variant_id)){
                                    $update_retail_query = sprintf("UPDATE retail_variants SET inventory_used_count=inventory_used_count+'%s', `inventory_count`=`inventory_count`-'%s' WHERE `retail_variant_id`='%s'",
                                            mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cretail_variant_id));
                                }else{
                                    $update_retail_query = sprintf("UPDATE `retail_products` SET `retail_purchase_count` = `retail_purchase_count`+'%s', retail_product_inventory=retail_product_inventory-'%s' WHERE `retail_product_id` $str", 
                                            mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cretail_product_id));
                                }
                                $update_retail_result = mysqli_query($this->db, $update_retail_query);
                                if(!$update_retail_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_query");
                                    log_info($this->json($error_log));                                    
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 200);
                                }else{
                                    for($k=1;$k<=$cquantity;$k++){
                                        $insert_full=sprintf("INSERT INTO `retail_order_fulfillment`(`company_id`,`retail_order_id`,`retail_order_detail_id`,`fulfillment_serial_no`,`fulfillment_status` )VALUES('%s','%s','%s',$k,'U')",
                                                mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $retail_order_id),mysqli_real_escape_string($this->db, $inserted_retail_order_detail_id));
                                        $insert_fullresult = mysqli_query($this->db, $insert_full);
                                        if(!$insert_fullresult){
                                           $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_full");
                                           log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                            $this->response($this->json($error), 200);
                                        }
                                    }                                    
                                }                                
                            }
                            $note="Ordered $qty items. Total paid $wp_currency_symbol$payment_amount";
                            
                            if(trim(!empty($note))){
                               $insert_note = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_id`, `student_id`, `retail_order_fulfillment_id`, `activity_type`, `activity_text`) VALUES ('%s', %s, '%s', '%s', '%s', '%s')",mysqli_real_escape_string($this->db, $company_id), 
                                       mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $student_id), '-1', 'order', mysqli_real_escape_string($this->db, $note));
                               $ins_result = mysqli_query($this->db, $insert_note);
                               if(!$ins_result) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_note");
                                    log_info($this->json($error_log));
                                }
                            }
                        }
                        $msg = array("status" => "Success", "msg" => "Retail purchase successful. Email confirmation sent.");
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sendOrderReceiptForRetailPayment($company_id,$retail_order_id,0);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function checkRetailQuantityDuringCheckout($company_id,$discount_code, $cart_details, $checkout_flag, $call_back, $optional_discount_flag, $optional_discount_amount, $payment_method) {
        $retail_array=$cart_details;
        $error_present="N";
        $is_deleted = "N";
        for($i=0;$i<count($retail_array);$i++){
            $used_qty=$child_qty=$remaining_qty=$registration_count=$retail_capacity=$retail_product_id=$retail_variant_id=$retail_quantity=0;
            $string=$retail_product_name=$retail_product_status=$retail_variant_name=$retail_variant_status=$product_type=$retail_variant_flag=$retail_product_title='';
            if(isset($retail_array[$i]['quantity']) && !empty($retail_array[$i]['quantity'])){
                $retail_quantity = $retail_array[$i]['quantity'];
            }
            $retail_array[$i]['deleted_flag']="N";
            $retail_product_id = $retail_array[$i]['retail_product_id'];
            $retail_variant_id = $retail_array[$i]['retail_variant_id'];
            $product_type=sprintf("SELECT `retail_product_id`,`retail_product_title`,`retail_variant_flag`,`retail_product_status`, `retail_payment_method` FROM `retail_products` rp LEFT JOIN `studio_pos_settings` pos ON rp.`company_id` = pos.`company_id` WHERE rp.`retail_product_id`='%s' AND rp.`company_id`='%s' AND rp.`retail_product_type`!='C'",
                                 mysqli_real_escape_string($this->db,$retail_product_id),mysqli_real_escape_string($this->db,$company_id));
            $prodtype_result = mysqli_query($this->db, $product_type);
                if (!$prodtype_result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$product_type");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($prodtype_result) > 0) {
                    $row_1 = mysqli_fetch_assoc($prodtype_result);
                    $retail_variant_flag = $row_1['retail_variant_flag'];
                    $retail_product_id = $row_1['retail_product_id'];
                    $retail_product_title = $row_1['retail_product_title'];
                    $retail_product_status = $row_1['retail_product_status'];
                    $pos_settings_payment_method=$row_1['retail_payment_method'];
                    if($retail_product_status!='P'){
                        $error_present="Y";
                        $is_deleted = "Y";
                        $retail_array[$i]['deleted_flag']="Y";
                        $retail_array[$i]['quantity_error_flag']="Y";
                        $retail_array[$i]['quantity_msg']="Retail Product $retail_product_title is currently not available for sale.";
                        continue;
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => "No Retail product Details Found.");
                    $this->response($this->json($error), 200);
                }
                if($retail_variant_flag!=='Y'){
                    $capacity_query = sprintf("SELECT `retail_product_id`,'' retail_variant_id,`retail_product_title`,`retail_product_type`,''retail_variants_name,IFNULL(`retail_product_inventory`, 100000) AS retail_capacity,`retail_product_price` AS retail_price,`retail_purchase_count`AS used_count, '' deleted_flag FROM `retail_products` WHERE `retail_product_id`='%s' AND `company_id`='%s' LIMIT 0,1 ",
                                mysqli_real_escape_string($this->db, $retail_product_id), mysqli_real_escape_string($this->db, $company_id));
                }else if($retail_variant_flag==='Y'){
                    $capacity_query = sprintf("SELECT rv.`retail_product_id`,`retail_variant_id`,rp.`retail_product_title`,`retail_product_type`,rv.`retail_variants_name`,IFNULL(`inventory_count`, 100000) AS retail_capacity,`variant_price` AS retail_price,`inventory_used_count` AS used_count, rv.`deleted_flag` FROM `retail_variants` rv LEFT JOIN `retail_products` rp ON rp.`retail_product_id`=rv.`retail_product_id` WHERE rv.`retail_variant_id`='%s' AND rp.`company_id`='%s' LIMIT 0,1 ",
                                mysqli_real_escape_string($this->db, $retail_variant_id), mysqli_real_escape_string($this->db, $company_id));
                }
                $cap_result = mysqli_query($this->db, $capacity_query);
                    if (!$cap_result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$capacity_query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($cap_result) > 0) {
                        $row_2 = mysqli_fetch_assoc($cap_result);
                        $used_qty=$row_2['used_count'];
                        $retail_capacity=$row_2['retail_capacity'];
                        $retail_product_type=$row_2['retail_product_type'];
                        $retail_product_name=$row_2['retail_product_title'];
                        $retail_product_id=$row_2['retail_product_id'];
                        $retail_variant_id=$row_2['retail_variant_id'];
                        $retail_variant_name=$row_2['retail_variants_name'];
                        $retail_variant_status=$row_2['deleted_flag'];
//                        $retail_variant_name=$row_2['retail_variants_name'];
                    }
                    if($retail_variant_status=='Y'){
                        $error_present="Y";
                        $is_deleted = "Y";
                        $retail_array[$i]['deleted_flag']='Y';
                        $retail_array[$i]['quantity_msg']="Retail Product $retail_product_title, $retail_variant_name  is currently not available for sale.";
                        $retail_array[$i]['quantity_error_flag']="Y";
                        continue;
                    }else{
                        $retail_array[$i]['quantity_msg']="";
                        $retail_array[$i]['quantity_error_flag']="N";
                    }
                    if (!isset($retail_quantity) || $retail_quantity == 0|| empty($retail_quantity)||is_null($retail_quantity)) {
                        $error_present="Y";
                        $retail_array[$i]['quantity_msg']="Please enter a valid quantity.";
                        $retail_array[$i]['quantity_error_flag']="Y";
                    }elseif (isset($retail_quantity) && $retail_quantity > 0) {
                        if (isset($retail_capacity) && $retail_capacity == 0) {
                            $error_present="Y";
                            $retail_array[$i]['quantity_msg']="Retail Product is Sold Out.";
                            $retail_array[$i]['quantity_error_flag']="Y";
                        }else{
                            $remaining_qty = $retail_capacity;
                            if (isset($retail_capacity) && $remaining_qty == 0) {
                                $error_present="Y";
                                $retail_array[$i]['quantity_msg']="Retail Product is Sold Out.";
                                $retail_array[$i]['quantity_error_flag']="Y";
                            }else{
                                if ($remaining_qty < $retail_quantity) {
                                    if($retail_variant_flag==='Y'){
                                        $string=', '.$retail_variant_name;
                                    }
                                    $error_present="Y";
                                    $retail_array[$i]['quantity_msg']="Only $remaining_qty Quantity is Available for $retail_product_name$string";
                                    $retail_array[$i]['quantity_error_flag']="Y";
                                }else{
                                    $retail_array[$i]['quantity_msg']="";
                                    $retail_array[$i]['quantity_error_flag']="N";
                                }
                            }
                        }
                    } elseif (isset($retail_capacity) && $retail_capacity == 0) {
                        $error_present="Y";
                        $retail_array[$i]['quantity_msg']="Retail Product is Sold Out.";
                        $retail_array[$i]['quantity_error_flag']="Y";
                    }else{
                        $retail_array[$i]['quantity_msg']="";
                        $retail_array[$i]['quantity_error_flag']="N";
                    }
                    
                }
            }
        }
        if ($error_present == 'Y') {
            $final_retail_array = $this->applyRetailDiscountForProducts($company_id, $discount_code, $retail_array, $checkout_flag, 1, $optional_discount_flag, $optional_discount_amount, $payment_method);
            $final_retail_array['quantity_error_flag'] = $error_present;
            $final_retail_array['deleted_error_flag'] = $is_deleted;
            $final_retail_array['status'] = "Failed";
            $final_retail_array['pos_settings_payment_method'] = $pos_settings_payment_method;
            
            $this->response($this->json($final_retail_array), 200);
        } else {
            $final_retail_array = $this->applyRetailDiscountForProducts($company_id, $discount_code, $retail_array, $checkout_flag, 1, $optional_discount_flag, $optional_discount_amount, $payment_method);
            $final_retail_array['deleted_error_flag'] = $is_deleted;
             $final_retail_array['pos_settings_payment_method'] = $pos_settings_payment_method;
            if ($final_retail_array['status'] == "Failed") {
                $final_retail_array['quantity_error_flag'] = $error_present;
                $final_retail_array['status'] = "Failed";
                if ($call_back == 0) {
                    $this->response($this->json($final_retail_array), 200);
                } else {
                    return $final_retail_array;
                }
            } elseif ($final_retail_array['status'] == "Success") {
                $final_retail_array['quantity_error_flag'] = $error_present;
                $final_retail_array['status'] = "Success";
                if ($call_back == 0) {
                    $this->response($this->json($final_retail_array), 200);
                } else {
                    return $final_retail_array;
                }
            }
        }
    }

    private function sendSnsforfailedcheckout($sub,$msg){
    
            $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
         
                    $subject = $sub;
    //                $subject = $sns_sub . " -transaction failed";
                    $message = $msg;

                    $credentials = new Aws\Credentials\Credentials('AKIAIGUWYNJGAHDPJGIA', 'w4oKbP0UhJZr1W0hGuULhkdEoxEHY2YxEq/6uycT');               
                    $sns = SnsClient::factory([
                        'credentials' => $credentials,
                        'region' => 'us-east-1',
                        'version' => 'latest'
                    ]);
    //                log_info("object cretaed");
                    $result1 = $sns->publish(array(
                        'TopicArn' => $topicarn,
                        // Message is required
                        'Message' => $message,
                        'Subject' => $subject,
                        //'MessageStructure' => 'string',
                        'MessageAttributes' => array(
                            // Associative array of custom 'String' key names
                            'String' => array(
                                // DataType is required
        //            'DataType' => '',
                                'DataType' => 'String',
                                'StringValue' => '200',
                            ),
                        // ... repeated
                        ),
                    ));
               
    }
    
    public function getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, $payment_processor, $studio_name) {
        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db,$buyer_email), mysqli_real_escape_string($this->db,$company_id));
        $result_select_student = mysqli_query($this->db, $select_student);
        if (!$result_select_student) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result_select_student) > 0) {
                $r = mysqli_fetch_assoc($result_select_student);
                $student_id = $r['student_id'];
                if(is_null($r['stripe_customer_id']) || empty(trim($r['stripe_customer_id']))){
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = $r['stripe_customer_id'];
                    }
                }else{
                    $stripe_customer_id = $r['stripe_customer_id'];
                }
            } else {
                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_lastname`, `student_email`) VALUES('%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db,$company_id), 
                        mysqli_real_escape_string($this->db,$buyer_first_name), mysqli_real_escape_string($this->db,$buyer_last_name), mysqli_real_escape_string($this->db,$buyer_email));
                $result_insert_student = mysqli_query($this->db, $insert_student);
                if (!$result_insert_student) {
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                    $this->response($this->json($error), 200);
                } else {
                    $student_id = mysqli_insert_id($this->db);
                    if($payment_processor=='Stripe'){
                        $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                    }else{
                        $stripe_customer_id = '';
                    }
                }
            }
            return array("student_id" => $student_id, "stripe_customer_id" => $stripe_customer_id);
        }
    }
    
    public function sendOrderReceiptForRetailPayment($company_id,$retail_order_id, $return_value) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false){
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
//        $ro_created_date = " DATE(CONVERT_TZ(ro.created_dt,$tzadd_add,'$new_timezone'))";
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        $full_amt=$total=$processing_fee=$tax_fee=$pr_fee=0;
        $processing_fee_type = "";
        $ret_prod=[];
        $retail_sales_agreement=$retail_order_detail_id=$retail_order_checkout_amount=$retail_order_discount_amount=$retail_product_id=$retail_cost=$retail_quantity=$retail_discount=$retail_payment_status=$retail_payment_amount=$subject=$message=$htmlContent='';
        $buyer_name=$retail_sales_agreement=$retail_products_tax=$credit_card_name=$retail_order_date = $studio_mail = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies =$retail_variant_id=$retail_parent_id= '';
        $retail_product_name=$prod_name=$student_cc_email_list=$retail_variant_name=$retail_order_amount=$buyer_phone=$buyer_postal_code=$buyer_street=$buyer_city=$buyer_state=$buyer_zip=$buyer_country=$phone_number=$email_id=$country=$postal_code=$state=$city=$street_address=$wp_currency_symbol=$wp_currency_code=$referral_email_list=$company_name='';
        $query = sprintf("SELECT ro.`company_id`, ro.`buyer_name`, ro.`retail_order_date`, ro.`retail_order_amount`, ro.`credit_card_name`, ro.`retail_products_tax`,ro.`buyer_email`,ro.`buyer_phone`,ro.`buyer_postal_code`,ro.`buyer_street`,ro.`buyer_city`,ro.`buyer_state`,ro.`buyer_zip`,ro.`buyer_country`,ro.`retail_order_checkout_amount`,ro.`retail_order_discount_amount`,ro.`processing_fee_val`,ro.processing_fee_type,
            rod.`retail_order_detail_id`, rod.`retail_product_id`, rod.`retail_parent_id`, rod.`retail_variant_id`, rod.`retail_product_name`, rod.`retail_variant_name`, rod.retail_cost,rod.`retail_discount`,rod.`retail_quantity`,rod.`retail_payment_status`,rod.`retail_payment_amount`,
            c.`retail_sales_agreement`, c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`,c.`street_address`,c.`city`,c.`state`,c.`postal_code`,c.`country`,c.`phone_number`,s.`student_cc_email`,IF(ro.`payment_method`='CA','(Cash)',IF(ro.`payment_method`='CH',concat('(Check',' #',ro.`check_number`,')'),'')) payment_method
            FROM `retail_orders` ro LEFT JOIN `company` c ON c.`company_id`= ro.`company_id` LEFT JOIN `student` s ON s.`company_id`=ro.`company_id` AND s.`student_id`=ro.`student_id`
            LEFT JOIN `retail_order_details` rod ON rod.`company_id`=ro.`company_id` AND ro.`retail_order_id`=rod.`retail_order_id`
            WHERE ro.`retail_order_id` = '%s' ORDER BY ro.`retail_order_id`", mysqli_real_escape_string($this->db, $retail_order_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
            exit();
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while ($row = mysqli_fetch_assoc($result)) {
                    $ret_prod[] = array_slice($row, 18, 11);
                    $company_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $buyer_name=$row['buyer_name'];
                    $retail_sales_agreement='<div style="max-height:92px;overflow-y:auto;">'.$row['retail_sales_agreement'].'</div>';
                    $retail_products_tax=$row['retail_products_tax'];
                    $credit_card_name=$row['credit_card_name'];
                    if(isset($row['retail_order_date'])&&($row['retail_order_date']!='0000-00-00')){
                        $retail_order_date = date("M jS, Y", strtotime($row['retail_order_date']));
                    }
                    $buyer_email=$row['buyer_email'];
                    $buyer_phone=$row['buyer_phone'];
                    $buyer_postal_code=$row['buyer_postal_code'];
                    $buyer_street=$row['buyer_street'];
                    $buyer_city=$row['buyer_city'];
                    $buyer_state=$row['buyer_state'];
                    $buyer_zip=$row['buyer_zip'];
                    $buyer_country=$row['buyer_country'];
                    $street_address=$row['street_address'];
                    $city=$row['city'];
                    $state=$row['state'];
                    $postal_code=$row['postal_code'];
                    $country=$row['country'];
                    $phone_number=$row['phone_number'];
                    $retail_order_checkout_amount=$row['retail_order_checkout_amount'];
                    $retail_order_discount_amount=$row['retail_order_discount_amount'];
                    $processing_fee=$row['processing_fee_val'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_method=$row['payment_method'];
                }
                $subject =" Purchase Order Confirmation - $company_name";
                $message .= "<b>Thank you for your order. Please see below for your payment confirmation and order details.</b><br><br>";
                $message .= "<b>Buyer information:</b><br>";
                $message .= "Buyer Name: $buyer_name <br>";
                $message .= "Order Date: $retail_order_date <br>";
                $message .= "Buyer Address: $buyer_street, $buyer_city <br>";
                $message .="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $buyer_state, $buyer_zip, $buyer_country <br><br>";
                $message .= "<b>Product Details</b><br><br>";
                $message .= '
                <html>
                <head>
                </head>
                <body>
                <table width="650" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                <tr>
                    <th><b> &nbsp; S.No &nbsp; <b></th>
                    <th><b> &nbsp; Product Description &nbsp;</b></th>
                    <th><b> &nbsp; Product Cost &nbsp;</b></th>
                    <th><b> &nbsp; Product Quantity &nbsp; </b></th>
                    <th><b> &nbsp; Total &nbsp; </b></th>
                </tr>';
                $sn=1;
                for($i=0;$i<count($ret_prod);$i++){                    
                $retail_product_name=$ret_prod[$i]['retail_product_name'];
                $retail_variant_name=$ret_prod[$i]['retail_variant_name'];
                    $retail_order_detail_id=$ret_prod[$i]['retail_order_detail_id'];
                    $retail_product_id=$ret_prod[$i]['retail_product_id'];
                    $retail_parent_id=$ret_prod[$i]['retail_parent_id'];
                    $retail_variant_id=$ret_prod[$i]['retail_variant_id'];
                    $retail_cost=$ret_prod[$i]['retail_cost'];
                    $retail_quantity=$ret_prod[$i]['retail_quantity'];
                    $retail_discount=$ret_prod[$i]['retail_discount'];
                    $retail_payment_status=$ret_prod[$i]['retail_payment_status'];
                    $retail_payment_amount=$ret_prod[$i]['retail_payment_amount'];
                    $total = number_format($retail_cost*$retail_quantity,2);
                    $full_amt=number_format(($retail_order_checkout_amount-$retail_products_tax-$processing_fee)+$retail_order_discount_amount,2);
                    $tax_fee=number_format(($retail_products_tax),2);
                    $pr_fee=number_format(($processing_fee),2);
                    if(!empty($retail_variant_name)){
                        $prod_name=$retail_product_name.','. $retail_variant_name;
                    }else{
                        $prod_name=$retail_product_name;
                    }
                    $message .= " <tr>
                    <td align=".'center'.">$sn </td>
                    <td align=".'left'.">$prod_name </td>
                    <td align=".'right'.">$wp_currency_symbol$retail_cost</td>
                    <td align=".'center'.">$retail_quantity</td>
                    <td align=".'right'.">$wp_currency_symbol$total</td>
                    </tr> ";
                    $sn++;
                }
                $message.="<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Order Total</td>
                    <td align=".'right'.">$wp_currency_symbol$full_amt</td>
                    </tr>";
                $message.="<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Discount Amount</td>
                    <td align=".'right'.">$wp_currency_symbol$retail_order_discount_amount</td>
                    </tr>";                
                $message.="<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total Taxes </td>
                    <td align=".'right'.">$wp_currency_symbol$tax_fee</td>
                    </tr>";
                if($processing_fee_type == '2'){
                $message.="<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td> Admin fee</td>
                    <td align=".'right'.">$wp_currency_symbol$pr_fee</td>
                    </tr>";
                }
                $message.="<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Grand total</td>
                    <td align=".'right'.">$wp_currency_symbol$retail_order_checkout_amount $payment_method</td>
                    </tr>";
                
                $message .= "</table>
                </body>
                </html> ";
                $waiver_present = 0;
        $file = '';
        if(!empty(trim($retail_sales_agreement))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = __DIR__."/../../uploads/".$dt."_retail_sales_agreement.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $retail_sales_agreement);
            fclose($ifp);
        }
              
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                exit();
            }
        }
                   
        $sendEmail_status = $this->sendEmailForRetail($studio_mail, $buyer_email, $subject, $message, $company_name, $studio_mail, $file, $waiver_present, $cc_email_list,$student_cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    private function getRealIpAddr(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet        
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip=$_SERVER['REMOTE_ADDR'];
}
        return $ip;
    }
    private function sendEmailForRetail($from, $to, $subject, $message, $cmp_name, $reply_to, $waiver_file, $waiver_present, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "sales_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }
    
    public function getRetailProductDetailsforSocialSharing($company_id, $product_id){
        $prod_str = "";
        if(!empty($product_id)){
            $p_id = mysqli_real_escape_string($this->db, $product_id);
            $prod_str = "AND (rp.`retail_product_id`='$p_id' OR rp.`retail_parent_id`='$p_id')";
        }
        $response = $retail_category = $retail_product = $category_id_list = $product_id_list = [];
        
        $query = sprintf("SELECT rp.`retail_product_id`, rp.`retail_parent_id`, rp.`company_id`, rp.`retail_product_type`, rp.`retail_product_status`, rp.`retail_product_title`, rp.`retail_product_subtitle`, 
                    rp.`retail_banner_img_url`, rp.`retail_product_desc`, rp.`retail_product_compare_price`, rp.`retail_product_inventory`, rp.`retail_purchase_count`,
                    rp.`retail_variant_flag`, c.`wp_currency_symbol`, IF(rp.`retail_variant_flag`='N', CONCAT(c.wp_currency_symbol,rp.`retail_product_price`),
                    (SELECT IF(MIN(variant_price)=MAX(variant_price), CONCAT(c.wp_currency_symbol, MIN(variant_price)), CONCAT(c.wp_currency_symbol, MIN(variant_price), ' - ', c.wp_currency_symbol, MAX(variant_price))) FROM `retail_variants` rv WHERE rv.`company_id`='%s' AND rv.`retail_product_id`=rp.`retail_product_id` AND rv.`deleted_flag`!='Y')) retail_product_price,
                    c.`wp_currency_code`, c.`company_name`, c.`logo_URL`, c.`upgrade_status`
                    FROM `retail_products` rp
                    LEFT JOIN `company` c ON rp.`company_id`=c.`company_id`
                    WHERE rp.`company_id`='%s' AND rp.`retail_product_status`='P' $prod_str
                    ORDER BY rp.`retail_sort_order` desc LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            return $error;
        } else {
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                    $response['logo_URL'] = $row['logo_URL'];
                    $response['retail_product_title'] = $row['retail_product_title'];
                    $response['retail_banner_img_url'] = $row['retail_banner_img_url'];
                    $response['retail_product_desc'] = $row['retail_product_desc'];
                $out = array('status' => "Success", 'msg' => $response);
                return $out;
            }else{
                $error = array('status' => "Failed", "msg" => "Retail Product details doesn't exist." ,"code"=>404, "title"=>"404 Error");
                return $error; // If no records "No Content" status
            }
        }
    }
    
     //retail dashboard
    public function addretailDimensions($company_id, $product_id, $child_id, $period) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));
        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND  `child_id`='%s' AND child_id !=0  AND `period`='%s' AND `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`, `child_id`) VALUES('%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query2" => "$insertquery");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        if (!empty($product_id)) {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s' AND `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result2 = mysqli_query($this->db, $sql2);
            if (!$result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows1 = mysqli_num_rows($result2);
                if ($num_of_rows1 == 0) {
                    $insertquery1 = sprintf("INSERT INTO `retail_dimensions`(`company_id`, `period`, `product_id`,  `child_id`) VALUES('%s', '%s','%s',0)",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $product_id));
                    $res2 = mysqli_query($this->db, $insertquery1);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function sendExpiryEmailDetailsToStudio($company_id){
        $query = sprintf("SELECT c.`email_id`, c.`company_name`, c.`referral_email_list` FROM `company` c WHERE c.`company_id`='%s'", $company_id);
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            mlog_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $company_name = $reply_to = "";
                $row = mysqli_fetch_assoc($result);
                $owner_email_id = $row['email_id'];
                $referral_email_list = $row['referral_email_list'];
                
                $subject = "A member has requested that you activate your MyStudio account.";
                $body_msg = '<div style="max-width:520px;">';
                $body_msg .= "Members are trying to access your app and/or URL registration links.<br><br>However, your account is currently inactive, please click here to activate your account.<br><br>";
                $body_msg .= '<center><a href="'.$this->server_url.'/mystudioapp.php?pg=login"><button type="button" style="width: 140px;border-radius: 5px;background: #4a90e2;color: #ffffff;border: 1px solid #4a90e2;font-weight: 500;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 40px;font-size: 15px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;display: inline-block;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;text-transform: none;overflow: visible;box-sizing: border-box;">Activate</button></a></center>';
                $body_msg .= "</div>";
                $sendEmail_status = $this->sendEmailForStudioExpiry($owner_email_id, $referral_email_list, $subject, $body_msg, $company_name, $reply_to, true);
                if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
                    $refer_check = 1;
                    $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $owner_email_id . "   " . $sendEmail_status['mail_status']);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                } else {
                    $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $owner_email_id);
                    mlog_info($this->json($error_log));
                    $this->response($this->json($error_log), 200);
                }                
            }else{
                $error = array('status' => "Failed", "msg" => "Studio details doesn't exist.");
                $this->response($this->json($error), 200);
            }
        }
    }

    public function sendEmailForStudioExpiry($to, $cc_email_list, $subject, $message, $cmp_name, $reply_to, $isHtml) {

        if (empty(trim($cmp_name))) {
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try {
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if (filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)) {
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML($isHtml);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if (!empty(trim($cc_email_list))) {
                $cc_addresses = explode(',', $cc_email_list);
                for ($init = 0; $init < count($cc_addresses); $init++) {
                    $mail->AddCC($cc_addresses[$init]);
                }
            }
            if($isHtml){                
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
            }
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        } catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        mlog_info($this->json($error_log));
        return $response;
    }
    
    protected function getunsubscribefooter($company_id) {
        $sql = sprintf("SELECT company_name, CONCAT(street_address, IF(street_address = '' , city, CONCAT(', ',city)),IF(street_address = '' OR city = '', state, CONCAT(', ',state)),IF(street_address = '' OR city = '' OR state = '', postal_code, CONCAT(' ',postal_code))) address 
                    from company where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                $studio_name = $row['company_name'];
                $studio_address = trim($row['address']);
                if(!empty(trim($studio_address))){
                    $studio_address = ($studio_address[strlen($studio_address) - 1] === "," ? substr($studio_address, 0, -1) : $studio_address);
                }
                $footer_detail = $studio_name . ($studio_address === "" ? '' : " | $studio_address");
                return $footer_detail;
            }
        }
    }   

    //Get Student details
    protected function getStudentPaymentInfoDetails($company_id, $call_back) {
        $group_by_text = '';
        $query = sprintf("SELECT c.`wepay_status`,c.`stripe_status`,s.`account_state` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if (mysqli_num_rows($result1) > 0) {
                $row = mysqli_fetch_assoc($result1);
                $wepay_status = $row['wepay_status'];
                $stripe_status = $row['stripe_status'];
                $stripe_account_state = $row['account_state'];
            }
        }
        if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
            $group_by_text = ', payment_method_id';
        }elseif($wepay_status == 'Y'){
            $group_by_text = ', credit_card_id';
        }else{
            $group_by_text = '';
        }

            $sql = sprintf("SELECT mr.student_id,mr.`participant_id`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) student_name, mr.`buyer_first_name`,mr.`buyer_last_name`, `buyer_email`, `buyer_phone`,     
                            `buyer_postal_code`, mr.`payment_method_id`, mr.`stripe_customer_id`, mr.`credit_card_id`, `credit_card_status`, `credit_card_name`, `stripe_card_name`,`credit_card_expiration_month`, `credit_card_expiration_year`,       
                            `membership_registration_column_1` reg_col_1, `membership_registration_column_2`  reg_col_2, `membership_registration_column_3` reg_col_3, `membership_registration_column_4` reg_col_4, `membership_registration_column_5` reg_col_5,           
                            `membership_registration_column_6`  reg_col_6, `membership_registration_column_7` reg_col_7, `membership_registration_column_8` reg_col_8, `membership_registration_column_9` reg_col_9, `membership_registration_column_10` reg_col_10,         
                            `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, concat(`membership_registration_column_1`,' ', `membership_registration_column_2`) sname, created_dt , last_updt_dt, mr.registration_from  
                            FROM `membership_registration` mr LEFT JOIN student s ON s.`company_id`= mr.`company_id` AND mr.`student_id`= s.`student_id` AND s.`deleted_flag`!='Y' WHERE mr.company_id = '%s' AND (TRIM(mr.`payment_method_id`)!='' || TRIM(mr.`credit_card_id`)!='') GROUP BY `buyer_email`, sname %s
                            UNION
                            SELECT er.student_id,er.`participant_id`, concat(`event_registration_column_1`,' ', `event_registration_column_2`) student_name, er.`buyer_first_name`,er.`buyer_last_name`, er.`buyer_email`, er.`buyer_phone`,           
                            er.`buyer_postal_code`, er.`payment_method_id`,er.`stripe_customer_id`, er.`credit_card_id`, er.`credit_card_status`, er.`credit_card_name`,`stripe_card_name`, er.`credit_card_expiration_month`, er.`credit_card_expiration_year`,     
                            `event_registration_column_1` reg_col_1, `event_registration_column_2` reg_col_2, `event_registration_column_3` reg_col_3, `event_registration_column_4` reg_col_4, `event_registration_column_5` reg_col_5,     
                            `event_registration_column_6` reg_col_6, `event_registration_column_7` reg_col_7, `event_registration_column_8` reg_col_8, `event_registration_column_9` reg_col_9, `event_registration_column_10` reg_col_10,  
                            '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country, concat(`event_registration_column_1`,' ', `event_registration_column_2`) sname, created_dt, last_updt_dt, er.registration_from 
                            FROM `event_registration` er LEFT JOIN student s ON s.`company_id`= er.`company_id` AND er.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y' WHERE er.company_id = '%s' AND (TRIM(er.`payment_method_id`)!='' || TRIM(er.`credit_card_id`)!='')
                            GROUP BY `buyer_email`, sname %s
                            UNION
                            SELECT tr.student_id, tr.`participant_id`,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) student_name, tr.`buyer_first_name`,tr.`buyer_last_name`, tr.`buyer_email`, tr.`buyer_phone`,           
                            tr.`buyer_postal_code`, tr.`payment_method_id`,tr.`stripe_customer_id`, tr.`credit_card_id`, tr.`credit_card_status`, tr.`credit_card_name`,`stripe_card_name`, tr.`credit_card_expiration_month`, tr.`credit_card_expiration_year`,     
                            `trial_registration_column_1` reg_col_1, `trial_registration_column_2` reg_col_2, `trial_registration_column_3` reg_col_3, `trial_registration_column_4` reg_col_4, `trial_registration_column_5` reg_col_5,     
                            `trial_registration_column_6` reg_col_6, `trial_registration_column_7` reg_col_7, `trial_registration_column_8` reg_col_8, `trial_registration_column_9` reg_col_9, `trial_registration_column_10` reg_col_10,  
                            '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,concat(`trial_registration_column_1`,' ', `trial_registration_column_2`) sname,created_dt, last_updt_dt, tr.registration_from 
                            FROM `trial_registration` tr LEFT JOIN student s ON s.`company_id`= tr.`company_id` AND tr.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y'WHERE tr.company_id = '%s' AND (TRIM(tr.`payment_method_id`)!='' || TRIM(tr.`credit_card_id`)!='') GROUP BY `buyer_email` %s
                            UNION
                            SELECT ms.student_id, '' as participant_id, '' as student_name, ms.`buyer_first_name`,ms.`buyer_last_name`, ms.`buyer_email`, ms.`buyer_phone`,           
                            '' as buyer_postal_code, ms.`payment_method_id`,ms.`stripe_customer_id`, ms.`credit_card_id`, ms.`credit_card_status`, ms.`credit_card_name`,`stripe_card_name`, ms.`credit_card_expiration_month`, ms.`credit_card_expiration_year`,     
                            '' as reg_col_1, '' as reg_col_2, '' as reg_col_3, '' as reg_col_4, '' as reg_col_5,     
                            '' as reg_col_6, '' as reg_col_7, '' as reg_col_8, '' as reg_col_9, '' as reg_col_10,  
                            '' participant_street, '' participant_city, '' participant_state, '' participant_zip, '' participant_country ,'' as sname,created_dt, last_updt_dt, ms.registration_from 
                            FROM `misc_order` ms LEFT JOIN student s ON s.`company_id`= ms.`company_id` AND ms.`student_id`= s.`student_id` AND s.`deleted_flag` !='Y'WHERE ms.company_id = '%s' AND (TRIM(ms.`payment_method_id`)!='' || TRIM(ms.`credit_card_id`)!='') GROUP BY `buyer_email` %s
                             ORDER BY `buyer_email` ASC ,`created_dt` DESC, `last_updt_dt` DESC", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text),mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $group_by_text), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $group_by_text));                
            $result = mysqli_query($this->db, $sql);
            if(!$result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    $output = $output['student_details'] = [];
                    $stud_list = $obj_list = $out = [];
                    while ($row = mysqli_fetch_assoc($result)) {
                        $out[] = $row;
                    }
                    for($i=0; $i<count($out); $i++){
                        $card = [];
                        $row = $out[$i];
                        $email = strtolower(trim($row['buyer_email']));
                        $card_id = $row['payment_method_id'];
                        $name = strtolower(trim($row['sname']));
                        $obj = array("email"=>"$email", "name"=>"$name");
                        if(!in_array($obj, $obj_list)){
                            $obj_list[] = $obj;
                            if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                                if (!empty($row['payment_method_id'])) {
                                    $card['payment_method_id'] = $row['payment_method_id'];
                                    $card['stripe_customer_id'] = $row['stripe_customer_id'];
                                    $card['credit_card_name'] = $row['stripe_card_name'];
                                    $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                    $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                    $card['postal_code'] = $row['buyer_postal_code'];
                                    $row['card_details'][] = $card;
                                }               
                                unset($row['credit_card_id']); 
                                unset($row['credit_card_status']);
                                unset($row['credit_card_name']);
                                unset($row['payment_method_id']);
                                unset($row['stripe_customer_id']);
                                unset($row['stripe_card_name']);
                                unset($row['credit_card_expiration_month']);
                                unset($row['credit_card_expiration_year']);
                                if(isset($output['student_details'])){
                                    $row['stud_index'] = count($output['student_details']);
                                }else{
                                    $row['stud_index'] = 0;
                                }
                            }else if ($wepay_status == 'Y'){
                                if (!empty($row['credit_card_id'])) {
                                    $card['credit_card_id'] = $row['credit_card_id'];
                                    $card['credit_card_status'] = $row['credit_card_status'];
                                    $card['credit_card_name'] = $row['credit_card_name'];
                                    $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                    $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                    $card['postal_code'] = $row['buyer_postal_code'];
                                    $row['card_details'][] = $card;
                                }
                                unset($row['payment_method_id']);
                                unset($row['stripe_customer_id']);
                                unset($row['stripe_card_name']);
                                unset($row['credit_card_id']);
                                unset($row['credit_card_status']);
                                unset($row['credit_card_name']);
                                unset($row['credit_card_expiration_month']);
                                unset($row['credit_card_expiration_year']);
                                if(isset($output['student_details'])){
                                    $row['stud_index'] = count($output['student_details']);
                                }else{
                                    $row['stud_index'] = 0;
                                }                             
                            }
                            $output['student_details'][] = $row;
                        }else{
                            $obj_index = array_search($obj, $obj_list);
                            if ($stripe_status == 'Y' && $stripe_account_state != 'N'){
                                if (!empty($row['payment_method_id'])) {
                                    $card['payment_method_id'] = $row['payment_method_id'];
                                    $card['stripe_customer_id'] = $row['stripe_customer_id'];
                                    $card['credit_card_name'] = $row['stripe_card_name'];
                                    $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                    $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                    $card['postal_code'] = $row['buyer_postal_code'];
        //                            if(isset($output['student_details'])){
                                }
                            }else if ($wepay_status == 'Y'){
                                if (!empty($row['credit_card_id'])) {
                                    $card['credit_card_id'] = $row['credit_card_id'];
                                    $card['credit_card_status'] = $row['credit_card_status'];
                                    $card['credit_card_name'] = $row['credit_card_name'];
                                    $card['credit_card_expiration_month'] = $row['credit_card_expiration_month'];
                                    $card['credit_card_expiration_year'] = $row['credit_card_expiration_year'];
                                    $card['postal_code'] = $row['buyer_postal_code'];
        //                            if(isset($output['student_details'])){    
                                }
                            }
                            $output['student_details'][$obj_index]['card_details'][] = $card; 
                        }
                    }
                    $out2 = array('status' => "Success", 'msg' => $output);
                    if ($call_back == 0) {
                        // If success everythig is good send header as "OK" and user details
                        $this->response($this->json($out2), 200);
                    } else {
                        return $out2;
                    }
                } else {
                    $error = array('status' => "Failed", "msg" => "Student details doesn't exist.");
                    if ($call_back == 0) {
                        $this->response($this->json($error), 200); // If no records "No Content" status
                    } else {
                        return $error;
                    }
                }
            }           
    }
    
    protected function checkpossettings($company_id, $payment_method, $from, $pos_email, $token) {
        $this->verifyStudioPOSToken($company_id, $pos_email, $token);
        if($from=='R'){
            $sql = sprintf("SELECT retail_status charge_status,retail_payment_method payment_method from studio_pos_settings where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        }else{
            $sql = sprintf("SELECT misc_charge_status charge_status,misc_payment_method payment_method from studio_pos_settings where company_id = '%s'", mysqli_real_escape_string($this->db, $company_id));
        }
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if($row['charge_status'] === "D"){
                    if($from=='R'){
                        $error = array('status' => "Failed", "msg" => "Retail Charge POS is disabled");
                    }else{
                        $error = array('status' => "Failed", "msg" => "Miscellaneous Charge POS is disabled");
                    }
                    $this->response($this->json($error), 200);
                }
                $payment_binary = $row['payment_method'];
                if($payment_method === "CA"){
                    if($payment_binary[1] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method cash is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
                if($payment_method === "CH"){
                    if($payment_binary[2] === "0"){
                        $error = array('status' => "Failed", "msg" => "Payment method check is disabled");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
    }   
    
    public function addMiscDimensions($company_id, $period,$amount) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add =" DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        if (!empty($period)) {
            $curr_dt = date("Y-m", strtotime($period));
        } else {
            $curr_dt = date("Y-m");
        }
//        $pre_dt = date("Y-m", strtotime("first day of previous month"));

        $sql2 = sprintf("SELECT * FROM `misc_dimensions` WHERE `company_id`='%s' AND `period`='%s'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `misc_dimensions`(`company_id`, `period`,`sales_amount`) VALUES('%s','%s','0')", 
                        mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $amount));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$insertquery1");
                    $this->response($this->json($error), 200);
                }
            }
            
            $updatequery1 = sprintf("UPDATE `misc_dimensions` SET `sales_amount`=sales_amount+'%s' where company_id='%s' AND period = '%s'", 
                        mysqli_real_escape_string($this->db, $amount),mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt));
                $res2 = mysqli_query($this->db, $updatequery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$updatequery1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query3" => "$updatequery1");
                    $this->response($this->json($error), 200);
                }
            
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function wepayCheckoutForMiscellaneousINDB($company_id,$student_id,$buyer_first_name,$buyer_last_name,$buyer_email,$buyer_phone,$order_note_details,$reg_type_user,$payment_method,$payment_amount,$cc_id,$cc_state,$check_number){
        $sts = $this->getStudioSubscriptionStatus($company_id);
        $upgrade_status = $sts['upgrade_status'];
        $buyer_name = $buyer_first_name.' '.$buyer_last_name;
        if (is_null($student_id) || $student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Wepay', '');
            $student_id = $stud_array['student_id'];
        } else {
            $student_id = $student_id;
        }
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
        
        $gmt_date=gmdate(DATE_RFC822);
        $sns_msg1=array("buyer_email"=>$buyer_email,"membership_title"=>$order_note_details,"gmt_date"=>$gmt_date);
        
        $querypf=sprintf("SELECT c.upgrade_status, s.misc_charge_fee_type FROM `company` c LEFT JOIN `studio_pos_settings` s USING(company_id) WHERE c.`company_id`='%s' ",mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $querypf);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
              if(mysqli_num_rows($result)>0){  
                $row=mysqli_fetch_assoc($result);
//                $upgrade_status= $row['upgrade_status'];
                $processing_fee_type=$row['misc_charge_fee_type'];
              }  
        
        }
        $p_f = $this->getProcessingFee($company_id, $upgrade_status);
        $process_fee_per = $p_f['PP'];
        $process_fee_val = $p_f['PT'];
        
        if ($payment_amount > 0 && $payment_method == "CC") {
            $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
        } else {
           $processing_fee = 0;
        }
        
        $query1 = sprintf("SELECT `wp_user_id`, `wp_user_state`, `access_token`, `account_id`, `account_state`, `action_reasons`, `disabled_reasons`, `currency` FROM `wp_account` WHERE `company_id`='%s' ORDER BY `wp_user_id` DESC", mysqli_real_escape_string($this->db, $company_id));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                $row = mysqli_fetch_assoc($result1);
                $access_token = $row['access_token'];
                $account_id = $row['account_id'];
                $currency = $row['currency'];
                $user_state = $row['wp_user_state'];
                $acc_state = $row['account_state'];
                if($user_state=='deleted'){
                    $msg = "Wepay user registered was deleted.";
                    $log = array("status" => "Failed", "msg" => "Payment cannot be done. $msg");
                    $this->response($this->json($log),200);
                }
                
                if($acc_state=='deleted'){
                    $log = array("status" => "Failed", "msg" => "Wepay Account has been deleted.");
                    $this->response($this->json($log),200);
                }elseif($acc_state=='disabled'){
                    $log = array("status" => "Failed", "msg" => "The account has been disabled by WePay and can no longer accept payments.");
                    $this->response($this->json($log),200);
                }
                
                if(!empty($access_token) && !empty($account_id)){
                    if (!function_exists('getallheaders')){
                        if(!function_exists ('apache_request_headers')){
                            $user_agent = $_SERVER['HTTP_USER_AGENT'];
                        }else{
                            $headers = apache_request_headers();
                            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                        }
                    }else{
                        $headers = getallheaders();
                        $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
                    }
                    $temp_password = $this->wp_pswd;
                    $method = 'aes-256-cbc';
                    // Must be exact 32 chars (256 bit)
                    $password = substr(hash('sha256', $temp_password, true), 0, 32);
                    // IV must be exact 16 chars (128 bit)
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        
                    $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                    
                    $w_paid_amount = $paid_amount = $paid_fee = $gross = 0;
                    $checkout_id = $checkout_state = '';                    
                    $cc_name = $cc_month = $cc_year = '';
                    $cc_status = $cc_state;
                    $temp_payment_status = 'M';
                    
                    if($payment_amount>0 && $payment_method == "CC"){
                        $temp_payment_status = 'S';
                        $checkout_id_flag = 1;
                        $rand = mt_rand(1,99999);
                        $ip = $this->getRealIpAddr();
                        $checkout_callback_url = $this->server_url."/Wepay2/updateCheckout?for=misc";
                        $unique_id = $company_id."_".$ip."_".$rand."_".time();
                        $date = date("Y-m-d_H:i:s");
                        $ref_id = $company_id."_".$date;
//                        if($processing_fee_type==2){
//                            $fee_payer = "payer_from_app";
//                        }elseif($processing_fee_type==1){
                            $fee_payer = "payee_from_app";
//                        }
                        $paid_amount = $payment_amount;
                        $paid_fee = $processing_fee;
                        
                        if($processing_fee_type==2){
                            $w_paid_amount = $payment_amount + $processing_fee;
                        }elseif($processing_fee_type==1){
                            $w_paid_amount = $payment_amount;
                        }
                        
                        $buyer_postal_code =  $country = $desc = '';//
                        $time = time();
                        $json2 = array("account_id"=>"$account_id","short_description"=>"$desc","type"=>"event","amount"=>"$w_paid_amount","currency"=>"$currency",
                            "fee"=>array("app_fee"=>$paid_fee,"fee_payer"=>"$fee_payer"), "reference_id"=>"$ref_id", "unique_id"=>"$unique_id",
                            "email_message"=>array("to_payee"=>"Payment has been added for $order_note_details","to_payer"=>"Payment has been made for $order_note_details"),
                            "payment_method"=>array("type"=>"credit_card","credit_card"=>array("id"=>$cc_id)),"callback_uri"=>"$checkout_callback_url",
                            "payer_rbits"=>array(array("receive_time"=>$time,"type"=>"phone","source"=>"user","properties"=>array("phone"=>"$buyer_phone","phone_type"=>"home")),
                                array("receive_time"=>$time,"type"=>"email","source"=>"user","properties"=>array("email"=>"$buyer_email")),
                                array("receive_time"=>$time,"type"=>"address","source"=>"user","properties"=>array("address"=>array("zip"=>"$buyer_postal_code","country"=>"$country")))),
                            "transaction_rbits"=>array(array("receive_time"=>$time,"type"=>"transaction_details","source"=>"partner_database",
                                "properties"=>array("itemized_receipt"=>array(array("description"=>"$order_note_details", "item_price"=>$w_paid_amount, "quantity"=>1, "amount"=>$w_paid_amount, "currency"=>"$currency"))))));
                        $postData2 = json_encode($json2);
                        $sns_msg1['call'] = "Checkout";
                        $sns_msg1['type'] = "Checkout Creation";
                        $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                        if($response2['status']=="Success"){
                            $res2 = $response2['msg'];
                            $checkout_id = $res2['checkout_id'];
                            $checkout_state = $res2['state'];
                            $gross = $res2['gross'];
//                            $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                        }else{
                            if($response2['msg']['error_code']==1008){
                                $response2 = $this->wp->accessWepayApi("checkout/create",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                $this->wp->log_info("wepay_checkout_create: ".$response2['status']);
                                if($response2['status']=="Success"){
                                    $res2 = $response2['msg'];
                                    $checkout_id = $res2['checkout_id'];
                                    $checkout_state = $res2['state'];
                                    $gross = $res2['gross'];
//                                    $this->rbitCallForCheckout($token, $user_agent, $checkout_id, $buyer_name, $buyer_email, $buyer_phone, $buyer_postal_code, $country, $event_name, $w_paid_amount, $currency);
                                }else{
                                    if($response2['msg']['error_code']==1008){
                                        $error = array("status" => "Failed", "msg" => "There was an error on WePay’s end. Contact your app administrator.");
                                        $this->response($this->json($error),200);
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }
                    }else{
                        $checkout_id_flag = 0;
                        if(!empty(trim($cc_id))){
                            $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                            $postData3 = json_encode($json3);
                            $sns_msg1['call'] = "Credit Card";
                            $sns_msg1['type'] = "Credit card details";
                            $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                            $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                            if($response3['status']=="Success"){
                                $res3 = $response3['msg'];
                                if($res3['state']=='new'){
                                    $json2 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id,"account_id"=>$account_id);
                                    $postData2 = json_encode($json2);                                    
                                    $sns_msg1['call'] = "Credit Card";
                                    $sns_msg1['type'] = "Credit Card Authorization";
                                    $response2 = $this->wp->accessWepayApi("credit_card/authorize",'POST',$postData2,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card_authorize: ".$response2['status']);
                                    if($response2['status']=="Success"){
                                        $res2 = $response2['msg'];
                                        $cc_name = $res2['credit_card_name'];
                                        $cc_new_state = $res2['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res2['expiration_month'];
                                        $cc_year = $res2['expiration_year'];
                                    }else{
                                        $error = array("status" => "Failed", "msg" => $response2['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }elseif($res3['state']=='authorized'){
                                    $cc_name = $res3['credit_card_name'];
                                    $cc_new_state = $res3['state'];
                                    $cc_month = $res3['expiration_month'];
                                    $cc_year = $res3['expiration_year'];
                                }else{
                                    if($res3['state']=='expired'){
                                        $msg = "Given credit card is expired.";
                                    }elseif($res3['state']=='deleted'){
                                        $msg = "Given credit card was deleted.";
                                    }else{
                                        $msg = "Given credit card details is invalid.";
                                    }
                                    $error = array("status" => "Failed", "msg" => $msg);
                                    $this->response($this->json($error),200);
                                }
                            }else{
                                $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                $this->response($this->json($error),200);
                            }
                        }

                    }
                    
                    if(!empty(trim($cc_id))){
                        $json3 = array("client_id"=>"$this->wp_client_id","client_secret"=>"$this->wp_client_secret","credit_card_id"=>$cc_id);
                        $postData3 = json_encode($json3);
                        $sns_msg1['call'] = "Credit Card";
                        $sns_msg1['type'] = "Credit Card details";
                        $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                        $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                        if($response3['status']=="Success"){
                            $res3 = $response3['msg'];
                            $cc_name = $res3['credit_card_name'];
                            $cc_new_state = $res3['state'];
                            if($cc_state!=$cc_new_state){
                                $cc_status = $cc_new_state;
                            }
                            $cc_month = $res3['expiration_month'];
                            $cc_year = $res3['expiration_year'];
                        }else{
                            for($k=0;$k<2;$k++){
                                if($response3['msg']['error_code'] == 503){
                                    sleep(2);
                                    $response3 = $this->wp->accessWepayApi("credit_card",'POST',$postData3,$token,$user_agent,$sns_msg1);
                                    $this->wp->log_info("wepay_credit_card: ".$response3['status']);
                                    if($response3['status']=="Success"){
                                        $res3 = $response3['msg'];
                                        $cc_name = $res3['credit_card_name'];
                                        $cc_new_state = $res3['state'];
                                        if($cc_state!=$cc_new_state){
                                            $cc_status = $cc_new_state;
                                        }
                                        $cc_month = $res3['expiration_month'];
                                        $cc_year = $res3['expiration_year'];
                                        break;
                                    }
                                    if($k==1){
                                        if($checkout_id_flag == 1){
                                            $type = $response3['msg']['error'];
                                            $description =  $response3['msg']['error_description']." Buyer Name:".$buyer_name." Buyer Email:".$buyer_email." Checkout Id:".$checkout_id." Participant Name:".$reg_col1." ".$reg_col2;      
                                            $insert_alertquery = sprintf("INSERT INTO `wepay_critical_alert`(`company_id`, `type`, `description`) VALUES ('%s','%s','%s')",mysqli_real_escape_string($this->db, $company_id),mysqli_real_escape_string($this->db, $type),mysqli_real_escape_string($this->db, $description));
                                            $result_alert = mysqli_query($this->db, $insert_alertquery);
                                            if(!$result_alert){
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_alertquery");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                                $this->response($this->json($error), 200);
                                            } 
                                            $type1 = $response3['msg']['error']." Regarding Credit Card";
                                            $description1 = "Fetching Credit Card details failed"."\n"." Buyer Name:".$buyer_name."\n"." Buyer Email:".$buyer_email."\n"." Checkout Id:".$checkout_id."\n"." Participant Name:".$reg_col1." ".$reg_col2;     
                                            $this->sendSnsforfailedcheckout($type1,$description1);
                                        }
                                        $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                        $this->response($this->json($error),200);
                                    }
                                }else{
                                    $error = array("status" => "Failed", "msg" => $response3['msg']['error_description']);
                                    $this->response($this->json($error),200);
                                }
                            }
                        }
                    }
                     
                    $query = sprintf("INSERT INTO `misc_order` (`processing_fee_type`,`company_id`,`student_id`,`buyer_first_name`,`buyer_last_name`,`buyer_email`,`buyer_phone`,`order_note_details`,payment_method,payment_amount,credit_card_id,credit_card_status,credit_card_name,credit_card_expiration_month,credit_card_expiration_year,check_number,reg_type_user)
                            VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", $processing_fee_type, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $buyer_first_name)
                            , mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone)
                            , mysqli_real_escape_string($this->db, $order_note_details), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $payment_amount)
                            , mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_status), mysqli_real_escape_string($this->db, $cc_name)
                            , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $check_number)
                            , mysqli_real_escape_string($this->db, $reg_type_user));
                    $result = mysqli_query($this->db, $query);
                    if(!$result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        
                        $misc_order_id = mysqli_insert_id($this->db);
                        $current_date = date("Y-m-d");
                        if ($payment_amount > 0) {
                            $payment_query = sprintf("INSERT INTO `misc_payment`(`credit_method`,`check_number`,`misc_order_id`, `student_id`, `checkout_id`, `checkout_status`, `processing_fee`, `payment_amount`, `payment_date`, `payment_status`, `cc_id`, `cc_name`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $checkout_id), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $cc_id), mysqli_real_escape_string($this->db, $cc_name));
                            $payment_result = mysqli_query($this->db, $payment_query);
                            if (!$payment_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                                log_info($this->json($error_log));
                            }
                        }
                        
                        if ($payment_method != 'CC') {
                            $update_sales_in_reg = sprintf("UPDATE `misc_order` SET `paid_amount`=`paid_amount`+$payment_amount WHERE `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $misc_order_id));
                            $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                            if (!$result_sales_in_reg) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            }
                            $this->addMiscDimensions($company_id, '', $payment_amount);
                        }
//                        Add Note Misc
                        if(!empty(trim($order_note_details))){
                            $note_text = "Order note: ".$order_note_details;
                        }else{
                            $note_text = "Order note: N/A";
                        }
                        $sql = sprintf("INSERT INTO `misc_notes` (`company_id`,`student_id`, `misc_order_id`, `activity_text`) VALUES ('%s','%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $note_text));
                        $result = mysqli_query($this->db, $sql);
                        if (!$result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                        $msg = array("status" => "Success", "msg" => "Miscellaneous charge was successful. Email confirmation sent.");
                        $this->responseWithWepay($this->json($msg),200);
                        $this->sendOrderReceiptForMiscPayment($company_id,$misc_order_id);
                    }
                }
            }else{
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error),200);
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
      public function sendOrderReceiptForMiscPayment($company_id, $misc_order_id){
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
       
        $waiver_policies = $total_due_amount = '';
        $query = sprintf("SELECT m.buyer_email, m.order_note_details,m.processing_fee_type,if(mp.`payment_from`='S',mp.`stripe_card_name`,mp.`cc_name`) as cc_name, mp.`payment_amount`, mp.`check_number`,mp.`processing_fee`, sp.misc_sales_agreement, mp.payment_status, c.wp_currency_symbol, c.company_name, c.email_id, c.referral_email_list, c.wp_currency_symbol, s.student_cc_email, m.`payment_method` 
                 FROM `misc_order` m 
                join `misc_payment` mp USING(misc_order_id) 
                inner join `studio_pos_settings`sp USING(company_id) 
                inner join `company` c USING(company_id) join student s ON m.student_id = s.student_id WHERE m.`company_id`='%s' AND m.`misc_order_id`='%s'", mysqli_real_escape_string($this->db, $company_id), $misc_order_id);
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $studio_name = $studio_mail = $participant_name = $buyer_email = $message = $cc_email_list = $student_cc_email_list = $wp_currency_code = $wp_currency_symbol =$processing_fee_type= '';
                 while($row = mysqli_fetch_assoc($result)){
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['misc_sales_agreement'];
                    $payment_status = $row['payment_status'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $student_cc_email_list = $row['student_cc_email'];
                    $payment_method = $row['payment_method'];
                    $processing_fee = $row['processing_fee'];
                    $cc_name = $row['cc_name'];
                    $check_number = $row['check_number'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $order_note_details = $row['order_note_details'];
                    $total_due_amount = $row['payment_amount'];
                    if ($payment_method == 'CA' || $payment_method == 'CH') {
                        $payment_amount = $row['payment_amount'];
                    } else {
                        if ($row['processing_fee_type'] == 2) {
                            $payment_amount = $row['payment_amount'] + $row['processing_fee'];
                        } else {
                            $payment_amount = $row['payment_amount'];
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                log_info($this->json($error_log));
                exit();
            }
        }
        
        $subject = "Payment Confirmation";
        $message .= "<b>Thank you for your order. Please see below for your payment confirmation and purchase details.</b><br><br>";
        $message .= "Miscellaneous Charge"."<br>";
          
        if ($payment_amount > 0 && ($payment_status == 'S' || $payment_status == 'M')) {
            if ($processing_fee_type == 2) {
                $pf_str = " (includes $wp_currency_symbol" . $processing_fee . " administrative fees)";
            } else {
                $pf_str = '';
            }

            if ($payment_method == 'CA'){
                $pay_type = "(Cash) ";
                $pf_str = '';
            }else if ($payment_method == 'CH'){
                $pay_type = "(Check #$check_number) ";
                $pf_str= '';
            }else{
                $pay_type = "($cc_name) ";
            }
            $message .= "Total Due: "."$wp_currency_symbol$total_due_amount"."<br>";
            $message .= "Amount paid: " . "$wp_currency_symbol$payment_amount" . "$pf_str$pay_type";
            if(!empty(trim($order_note_details))) {
                $message .= "<br>" . "Order note: " . "$order_note_details";
            }
            $message .= "<br><br><br>";
        }

        $waiver_present = 0;
        $file = '';
        if(!empty(trim($waiver_policies))){
            $waiver_present = 1;
            $file_name_initialize = 10;
            for($z=0; $z<$file_name_initialize; $z++){
                $dt = time();
                $file = "../../uploads/".$dt."_waiver_policies.html";        
                if(file_exists($file)){
                    sleep(1);
                    $file_name_initialize++;
                }else{
                    break;
                }
            }
            $ifp = fopen($file, "w");
            fwrite($ifp, $waiver_policies);
            fclose($ifp);
        }

        $sendEmail_status = $this->sendEmailForMisc($studio_mail ,$buyer_email, $subject, $message, $studio_name, $studio_mail, $waiver_present, $file, $cc_email_list, $student_cc_email_list);
        if(isset($sendEmail_status['status']) && $sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            log_info($this->json($error_log));
         }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
        date_default_timezone_set($curr_time_zone);
    }

    
    public function updateRetailDimensionsNetSales($company_id, $product_id, $child_id, $date, $amount) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);

        if (!empty($date)) {
            $every_month_dt = date("Y-m", strtotime($date));
        } else {
            $every_month_dt = date("Y-m");
        }

//        $date_init = 0;
//        $date_str = "%Y-%m";
//        $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));

        if (!empty($child_id)) {
            $sql1 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
            $res1 = mysqli_query($this->db, $sql1);
            if (!$res1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res1);
                if ($num_of_rows == 1) {
                    $sql2 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), mysqli_real_escape_string($this->db, $child_id), $every_month_dt);
                    $res2 = mysqli_query($this->db, $sql2);
                    if (!$res2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                        $res3 = mysqli_query($this->db, $sql3);
                        if (!$res3) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }
        } else {
            $sql2 = sprintf("SELECT * FROM `retail_dimensions` WHERE `company_id`='%s' AND `product_id`='%s' AND `child_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
            $res2 = mysqli_query($this->db, $sql2);
            if (!$res2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_of_rows = mysqli_num_rows($res2);
                if ($num_of_rows == 1) {
                    $sql3 = sprintf("UPDATE `retail_dimensions` SET `net_sales`=`net_sales`+%s WHERE `company_id`='%s' AND `product_id`='%s' AND `period`='%s' AND `child_id`='0'", mysqli_real_escape_string($this->db, $amount), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $product_id), $every_month_dt);
                    $res3 = mysqli_query($this->db, $sql3);
                    if (!$res3) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }
        date_default_timezone_set($curr_time_zone);
    }
    
     public function getleadsProgramDetailsforpos($company_id, $call_back) {//call_back, 0 -> returned & exit, 1 -> return response
        //check array_slice in following before alter or add any columns in this query
        $sql = sprintf("SELECT 'program' as type , `program_id`, `company_id`, `program_name`, `program_sort_order` sort_order FROM `leads_program` 
                 WHERE `company_id`='%s' and `deleted_flg`!='D' 
                 UNION 
                 SELECT 'source' as type , `source_id`, `company_id`, `source_name`, `source_sort_order` sort_order FROM `leads_source` 
                 WHERE `company_id`='%s' and `deleted_flg`!='D' order by sort_order",
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 1) {
                return $error;
            } else {
                $this->response($this->json($error), 200);
            }
        } else {
            $leads_program_list = $leads_source_list = $output = [];
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                $r = $b = 0;
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($row['type'] == 'program') {
                        $leads_program_list[$r]['id'] = $row['program_id'];
                        $leads_program_list[$r]['name'] = $row['program_name'];
                        $leads_program_list[$r]['category_sort_order'] = $row['sort_order'];
                        $r++;
                    } else if ($row['type'] == 'source') {
                        $leads_source_list[$b]['id'] = $row['program_id'];
                        $leads_source_list[$b]['name'] = $row['program_name'];
                        $leads_source_list[$b]['category_sort_order'] = $row['sort_order'];
                        $b++;
                    }
                }
                $output['program_interest'] = $leads_program_list;
                $output['lead_source'] = $leads_source_list;
                $out = array('status' => "Success", 'msg' => $output);
                // If success everythig is good send header as "OK" and user details
                if ($call_back == 1) {
                    return $out;
                } else {
                    $this->response($this->json($out), 200);
                }
            } else {
                $type = 'add';
                $source_name = ['Not Specified', 'Google', 'Facebook'];
                $program_name = 'Not Specified';
                for ($i = 0; $i < 3; $i++) {
                    $check = $this->updateleadsourcedetails($company_id, $source_name[$i], '', $type, 1);
                }
                $this->updateprograminterestdetails($company_id, $program_name, '', $type, 1);
                $this->getAllCampaignDetails($company_id);
                $this->getleadsProgramDetailsforpos($company_id, 0);
            }
        }
    }
    
    public function addIndividualLeadsMemberFORPOSINDB($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type){
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone=$user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
//        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
//        
//        //Get Leads POS Status
        $selecsql=sprintf("SELECT leads_email_status,s.leads_status FROM `studio_pos_settings` s WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $selectresult= mysqli_query($this->db,$selecsql);
        if (!$selectresult) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else {
                $leads_email_status_value = mysqli_fetch_assoc($selectresult);
                if($reg_type_user != 'U'){
                    if($leads_email_status_value['leads_status'] === 'D'){
                        $error = array('status' => "Failed", "msg" => "Request More Info POS Setting is disabled. Please enable this in POS Settings.");
                        $this->response($this->json($error), 200);
                    }
                }
                if($leads_email_status_value['leads_email_status'] === "I"){
                    $include_campaign_email = 'Y';
                }else{
                    $include_campaign_email = 'N';
                }
        }
               
        
        $current_date = date("Y-m-d");
        if ($program_id == 1) {
            $n_query1 = sprintf("select `program_id` from `leads_program` where `company_id`='%s' and `program_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db,$company_id));
            $n_result1 = mysqli_query($this->db, $n_query1);
            if (!$n_result1) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows1 = mysqli_num_rows($n_result1);
                if ($num_rows1 == 1) {
                    $result_obj = mysqli_fetch_object($n_result1);
                    $program_id = $result_obj->program_id;
                }
            }
        }
        if ($source_id == 1) {
            $n_query2 = sprintf("select `source_id` from `leads_source` where `company_id`='%s' and `source_name`='Not Specified' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db,$company_id));
            $n_result2 = mysqli_query($this->db, $n_query2);
            if (!$n_result2) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$n_query2");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows2 = mysqli_num_rows($n_result2);
                if ($num_rows2 == 1) {
                    $result_obj1 = mysqli_fetch_object($n_result2);
                    $source_id = $result_obj1->source_id;
                }
            }
        }
        
         $this->addLeadsDimensions($company_id, $program_id, $source_id, '', 200);
          $checkold=sprintf("SELECT Max(zap_reg_id)+1 zap_count FROM `leads_registration`  WHERE  `company_id`='%s' ",
                mysqli_real_escape_string($this->db, $company_id));
        $resultold = mysqli_query($this->db, $checkold);
        if (!$resultold) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$checkold");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
             if(mysqli_num_rows($resultold)>0){
                $old_value= mysqli_fetch_object($resultold);
                $zap_reg_id = $old_value->zap_count;
            }
        }

        $query = sprintf("INSERT INTO `leads_registration`(`company_id`, `program_id`, `source_id`,`zap_reg_id`, `leads_status`, `buyer_first_name`, `buyer_last_name`, `buyer_phone`, `buyer_email`, `participant_firstname`, `participant_lastname`, 
                `registration_date`, `include_campaign_email`, `leads_reg_type_user`, `leads_reg_version_user`, `leads_entry_type`) 
                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id), 
                mysqli_real_escape_string($this->db, $source_id), mysqli_real_escape_string($this->db, $zap_reg_id),mysqli_real_escape_string($this->db, $leads_status), mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_email), 
                mysqli_real_escape_string($this->db, $p_firstname), mysqli_real_escape_string($this->db, $p_lastname),  mysqli_real_escape_string($this->db, $current_date),
                mysqli_real_escape_string($this->db, $include_campaign_email), mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $reg_version_user), mysqli_real_escape_string($this->db, $reg_entry_type));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $err_no = mysqli_errno($this->db);
            if($err_no == 1062){
                sleep(0.5);
                $this->addIndividualLeadsMember($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type);
            }
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $leads_reg_id = mysqli_insert_id($this->db);

            $activity_text = 'Registration date.';
            $activity_type="Individual Registration";
            if(!empty($buyer_email) && $include_campaign_email=='Y'){
                $this->campaignSendmail($leads_reg_id,$company_id,$buyer_email);
            }


            $curr_date = gmdate("Y-m-d H:i:s");
            $insert_history = sprintf("INSERT INTO `leads_history`(`company_id`, `leads_reg_id`, `activity_type`, `activity_text`, `activity_date_time`) VALUES ('%s','%s','%s','%s','%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $leads_reg_id),  $activity_type, 
                    mysqli_real_escape_string($this->db, $activity_text), mysqli_real_escape_string($this->db, $curr_date));
            $result_history = mysqli_query($this->db, $insert_history);
            if (!$result_history) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_history");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }

            $this->updateLeadsDimensionsMembers($company_id, $program_id, $source_id, $leads_status, '', $leads_reg_id, '', '', 200);
            
            //  For zapier Implementation
            if($include_campaign_email === 'Y'){
                $zap_include_campaign_email = 'Yes';
            }else{
                $zap_include_campaign_email = 'No';
            }
            $target_url_arr = [];
            $zapsql = sprintf("SELECT target_url from zap_subscribe where company_id='%s' AND (event_type='A' OR event_type='B') AND category_type='L' AND deleted_flag !='Y'", mysqli_real_escape_string($this->db, $company_id));
            $zapresult = mysqli_query($this->db, $zapsql);
            if (!$zapresult) {
                $error = array('status' => "Failed","msg" => "Internal Server Error.", "query" => "$zapsql");
                log_info($this->json($error));
            } else {
                $zap_num_of_rows = mysqli_num_rows($zapresult);
                if ($zap_num_of_rows > 0) {
                    while ($zaprow = mysqli_fetch_assoc($zapresult)) {
                        $target_url_arr[] = $zaprow['target_url'];
                    }
                    $tmpkey_arr = $arr_check = [];
                    for($i=0;$i<count($target_url_arr);$i++){
                        $tmp = explode( '/', $target_url_arr[$i]);
                        $last_two = array_splice($tmp,count($tmp)-3);
                        $tmpkey = implode("/",$tmp);
                        $tmpkey .= '/'.$last_two[0].'/';
                        if(!in_array($tmpkey,$tmpkey_arr)){
                            $tmpkey_arr[] = $tmpkey;
                        }
                        $arr_check[$tmpkey][] = $last_two[1];
                    }
                    for ($i = 0; $i < count($tmpkey_arr); $i++) {
                        foreach ($arr_check as $key => $value) {
                            if ($tmpkey_arr[$i] == $key) {
                                for ($j = 0; $j < count($value); $j++) {
                                    $tmpkey_arr[$i] = $tmpkey_arr[$i].$value[$j];
                                    if($j!== count($value) -1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].',';
                                    }else if($j == count($value)-1){
                                        $tmpkey_arr[$i] = $tmpkey_arr[$i].'/';
                                    }
                                }
                            }
                        }
                    }
                    $auth[] = "Accept: application/json";
                    $auth[] = "Content-Type: application/json";
                    $new_query = sprintf("SELECT 'pi' as type, `program_name` name from `leads_program` where `program_id`='%s' and `deleted_flg`!='D'
                                UNION
                    SELECT 'ls' as type, `source_name` name from `leads_source` where `source_id`='%s' and `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $program_id), mysqli_real_escape_string($this->db, $source_id));
                    $new_res = mysqli_query($this->db, $new_query);
                    if (!$new_res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$new_query");
                        log_info($this->json($error_log));
                    } else {
                        if(mysqli_num_rows($new_res)>0){
                            while($row = mysqli_fetch_assoc($new_res)){
                                $type = $row['type'];
                                if ($type == 'pi') {
                                    $program_name = $row['name'];
                                } 
                                if ($type == 'ls') {
                                    $source_name = $row['name'];
                                }
                            }
                        }
                    }
                    for($i=0;$i<count($tmpkey_arr);$i++){
                        $temp_body=["Id"=>"$zap_reg_id","Buyer First Name" =>"$buyer_first_name","Buyer Last Name" =>"$buyer_last_name","Mobile Phone" =>"$buyer_phone","Email" =>"$buyer_email","Status" =>"Active","Program Interest" =>"$program_name","Source" =>"$source_name","Participant First Name" =>"$p_firstname","Participant Last Name" =>"$p_lastname","Communication Campaign Status"=>$zap_include_campaign_email];
                        $body = JSON_ENCODE($temp_body);

                        $ch = curl_init(); 
                        curl_setopt($ch, CURLOPT_URL, "$tmpkey_arr[$i]");
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);

                        $result = curl_exec($ch);
                        $resultArr = json_decode($result, true);
                        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        if(!isset($resultArr['error']) && ($returnCode==200 && $returnCode!=0)){
                            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result: " => $result);
                            log_info($this->json($succ_log));
                        }else{
                            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => "POST", "Url: " => $tmpkey_arr[$i], "Body: " => $body, "Result : " => $result);
                            log_info($this->json($error_log));
                        }
                        curl_close($ch);
                    } 
                }
            }
             // End Zapier Implementation
            $msg = array("status" => "Success", "msg" => "Your information was submited successfully.");
            $this->response($this->json($msg), 200);
        }
        date_default_timezone_set($curr_time_zone);
     
    }
    
    public function sendEmailForMisc($from, $to, $subject, $message, $cmp_name, $reply_to,$waiver_present, $waiver_file, $cc_email_list,$student_cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            if(!empty(trim($student_cc_email_list))){
                $student_cc_addresses = explode(',', $student_cc_email_list);
                for($init_1=0;$init_1<count($student_cc_addresses);$init_1++){
                    if(filter_var(trim($student_cc_addresses[$init_1]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($student_cc_addresses[$init_1]);
                    }
                }
            }
            
            $mail->Subject = $subject;
            $mail->Body = $message;
            if($waiver_present==1){
    //            $mail->AddAttachment("../../../uploads/waiver_policies.html");
                $mail->AddAttachment($waiver_file, "waiver_agreement.html");
            }
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        if(file_exists($waiver_file)){
            unlink($waiver_file);
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        log_info($this->json($error_log));
        return $response;
    }

    public function updateleadsourcedetails($company_id, $source_name, $source_id, $type, $call_back) {
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";
        
        if ($type == 'update') {
            $pi_ls_name = 'Not Specified';
            $default_check = sprintf("select * from `leads_source` where `company_id`='%s' and `source_id`='%s' and `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_id));
            $default_result_check = mysqli_query($this->db, $default_check);
            if (!$default_result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$default_check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $default_rows = mysqli_num_rows($default_result_check);
                if ($default_rows > 0) {
                    $default = mysqli_fetch_assoc($default_result_check);
                    if ($default['source_name'] == $pi_ls_name) {
                        $error = array('status' => "Failed", "msg" => "$pi_ls_name can not be changed to $source_name");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }

         $check = sprintf("select * from `leads_source` where `company_id`='%s' and `source_name`='%s' and `deleted_flg`!='D'",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_name));
            $result_check = mysqli_query($this->db, $check);
            if (!$result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $check_rows = mysqli_num_rows($result_check);
                if ($check_rows > 0) {
                    $error = array('status' => "Failed", "msg" => "source name already exits for this studio");
                    if ($call_back == 0) {
                        $this->response($this->json($error), 200);
                    } else {
                        return $error;
                    }
                }
            }
        if ($type == 'add') {
            $sql1 = sprintf("INSERT INTO `leads_source` (`company_id`, `source_name`, `source_sort_order`) VALUES ('%s', '%s',  NextVal('source_sort_order_seq'))", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_name));
        } else if ($type == 'update') {
            $sql1 = sprintf("update `leads_source` set `source_name` = '%s' where `company_id`='%s' and `source_id`='%s'", 
                    mysqli_real_escape_string($this->db, $source_name), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $source_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        }
        if ($call_back == 0) {
            $this->getleadsProgramDetailsforpos($company_id, 0);   //call_back, 0 -> returned & exit, 1 -> return response
        } else {
            return 'success';
        }

        date_default_timezone_set($curr_time_zone);
    }
    
    public function updateprograminterestdetails($company_id, $program_name, $program_id, $type, $call_back) {// call_back  = 0 response 1 =return// 
        $user_timezone = $this->getUserTimezone($company_id, 200);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add = " (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))";
        } else {
            $tzadd_add = "'" . $curr_time_zone . "'";
        }
        $currdate_new = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))";

        if ($type == 'update') {
            $pi_ls_name = 'Not Specified';
            $default_check = sprintf("select * from `leads_program` where `company_id`='%s' and `program_id`='%s' and `deleted_flg`!='D'", 
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id));
            $default_result_check = mysqli_query($this->db, $default_check);
            if (!$default_result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$default_check");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            } else {
                $default_rows = mysqli_num_rows($default_result_check);
                if ($default_rows > 0) {
                    $default = mysqli_fetch_assoc($default_result_check);
                    if ($default['program_name'] == $pi_ls_name) {
                        $error = array('status' => "Failed", "msg" => "$pi_ls_name can not be changed to $program_name");
                        $this->response($this->json($error), 200);
                    }
                }
            }
        }

        $check = sprintf("select * from `leads_program` where `company_id`='%s' and `program_name`='%s' and `deleted_flg`!='D'", 
                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_name));
        $result_check = mysqli_query($this->db, $check);
        if (!$result_check) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        } else {
            $check_rows = mysqli_num_rows($result_check);
            if ($check_rows > 0) {
                $error = array('status' => "Failed", "msg" => "program name already exits for this studio");
                if ($call_back == 0) {
                    $this->response($this->json($error), 200);
                } else {
                    return $error;
                }
            }
        }
        if ($type == 'add') {
            $sql1 = sprintf("INSERT INTO `leads_program` (`company_id`, `program_name`, `program_sort_order`) VALUES ('%s', '%s',  NextVal('program_sort_order_seq'))",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_name));
        } else if ($type == 'update') {
            $sql1 = sprintf("update `leads_program` set `program_name` = '%s' where `company_id`='%s' and `program_id`='%s'", 
                    mysqli_real_escape_string($this->db, $program_name), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $program_id));
        }
        $result1 = mysqli_query($this->db, $sql1);
        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            if ($call_back == 0) {
                $this->response($this->json($error), 200);
            } else {
                return $error;
            }
        }
        if ($call_back == 0) {//call_back, 0 -> returned & exit, 1 -> return response
            $this->getleadsProgramDetailsforpos($company_id, 0);
        } else {
            return 'success';
        }
        date_default_timezone_set($curr_time_zone);
    }
    
    public function getAllCampaignDetails($company_id){
        $check = $this->checkCampaignAvailability($company_id, 1);
        if($check['status']=='Success'){
        }else{
            $this->createCampaignForFirstTime($company_id, 1);
        }
        
    }
    
    public function checkCampaignAvailability($company_id, $call_back){
        $query = sprintf("SELECT * FROM `campaign` WHERE `company_id`='%s' ORDER BY `cmpgn_sort_order`", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $output = [];
                $deleted_count = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $deleted_flag = $row['deleted_flag'];
                    $type = $row['type'];
                    if($deleted_flag!='Y' && $type=='C'){
                        $output[] = $row;
                    }else{
                        $deleted_count++;
                    }
                }
                
                $error =  array('status' => "Success", "msg" => "Available.", "category_list" => $output);
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }else{
                $error =  array('status' => "Failed", "msg" => "Not Available.", "category_list" => []);
                if($call_back==1){
                    return $error;
                }else{
                    $this->response($this->json($error), 200);
                }
            }
        }
    }
    
    public function createCampaignForFirstTime($company_id, $call_back){
        $query = sprintf("INSERT INTO `campaign`( `company_id`, `campaign_title`, `campaign_type`, `type`, `cmpgn_sort_order`) VALUES 
                ('%s', 'New leads email flow', 'L', 'C', NextVal('cmpgn_sort_order'))", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_affected_rows($this->db)>0){
                $campaign_id = mysqli_insert_id($this->db);
                $query2 = sprintf("INSERT INTO `campaign`( `company_id`, `type`, `email_title`, `parent_id`, `cmpgn_sort_order`) VALUES
                    ('%s', 'E', 'Email 1', $campaign_id, NextVal('cmpgn_sort_order'))", mysqli_real_escape_string($this->db, $company_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }else{
                    $campaign_email_id = mysqli_insert_id($this->db);
                    $query3 = sprintf("INSERT INTO `campaign_trigger`( `company_id`, `campaign_id`, `trigger_event_type`) VALUES ('%s', '%s', '%s')",
                        mysqli_real_escape_string($this->db, $company_id), $campaign_email_id, 'NL');
                    $result3 = mysqli_query($this->db, $query3);
                    if(!$result3){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        if($call_back==1){
                            return $error;
                        }else{                        
//                            $this->checkCampaignAvailability($company_id, 0);
                        }
                    }
                }
            }else{
//                $error = array('status' => "Failed", "msg" => "First Campaign failed to insert.");
//                $this->response($this->json($error), 200);
            }
        }
    }
    

    public function stripeCheckoutForMiscellaneousINDB($company_id, $actual_student_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $order_note_details, $reg_type_user, $payment_method, $payment_amount, $payment_method_id, $check_number,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id) {
        $payment_from = $registration_from = '';
        $stripe_account_id = $stripe_currency = $studio_name = '';
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
        if (($payment_method == "CC" && $stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) || ($payment_method == "CA" || $payment_method == "CH")|| $payment_amount == 0) {  // ALLOWS FOR CASH & CHECK, IF CC, CHECK THE CHARGES ENABLED TO BE 'Y'
            $sts = $this->getStudioSubscriptionStatus($company_id);
            $upgrade_status = $sts['upgrade_status'];
            
            $querypf = sprintf("SELECT c.`company_name`, s.`misc_charge_fee_type`, sa.`account_id`, sa.`currency` FROM `company` c 
                        LEFT JOIN `studio_pos_settings` s ON c.company_id=s.company_id
                        LEFT JOIN `stripe_account` sa ON c.`company_id`=sa.`company_id`
                        WHERE c.`company_id`='%s' ", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $querypf);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$querypf");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency = $row['currency'];
                    $studio_name = $row['company_name'];
                    $processing_fee_type = $row['misc_charge_fee_type'];
                }
            }
            
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }
            $user_timezone = $this->getUserTimezone($company_id, 200);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);

            $p_f = $this->getProcessingFee($company_id, $upgrade_status);
            $process_fee_per = $p_f['PP'];
            $process_fee_val = $p_f['PT'];

            if ($payment_amount > 0 && $payment_method == "CC") {
                $processing_fee = $this->calculateProcessingFee($payment_amount, $processing_fee_type, $process_fee_per, $process_fee_val);
            } else {
                $processing_fee = 0;
            }

            $w_paid_amount = $paid_fee = $amount_in_dollars = $dest_amount = $dest_amount_in_dollars = $w_paid_amount_round = $dest_amount_round = $insert_paid_amount = 0;
            $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = '';
            $next_action_type = $next_action_type2 = $button_url = $misc_payment_id ='';
            $has_3D_secure = 'N';
            
            if (!empty($stripe_customer_id) && !(is_null($stripe_customer_id)) && !empty($payment_method_id) && $payment_method == 'CC') {
                
                $this->getStripeKeys();
                \Stripe\Stripe::setApiKey($this->stripe_sk);
                \Stripe\Stripe::setApiVersion("2019-05-16");
                    
                if ($payment_amount > 0) {
                    if ($processing_fee_type == 2) {
                        $w_paid_amount = $payment_amount + $processing_fee;
                    } elseif ($processing_fee_type == 1) {
                        $w_paid_amount = $payment_amount;
                    }
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $processing_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100;
                    $p_type = 'misc';
                    $payment_intent_result = $this->payment_intent_creation($company_id, $student_id, $payment_method_id, $stripe_customer_id, $amount_in_dollars, $stripe_currency, $studio_name, $buyer_email, $stripe_account_id, $dest_amount_in_dollars, $p_type, $cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded'){
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    }elseif($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                }
            }
            $payment_from = 'S';
            $registration_from = 'S';
            if ($payment_method == 'CA' || $payment_method == 'CH'){
                $payment_status = 'M';
                $insert_paid_amount = $payment_amount; 
            }else if($checkout_state == 'released'){
                if ($processing_fee_type == 2) {
                    $insert_paid_amount = $payment_amount;
                } elseif ($processing_fee_type == 1) {
                    $insert_paid_amount = $payment_amount - $processing_fee;
                }
            }else{
                $insert_paid_amount = 0; 
            }
            $query = sprintf("INSERT INTO `misc_order` (`stripe_customer_id`,`processing_fee_type`,`company_id`,`student_id`,`buyer_first_name`,`buyer_last_name`,`buyer_email`,`buyer_phone`,`order_note_details`,`payment_method`,`payment_amount`,`registration_from`,`payment_method_id`,`stripe_card_name`,`credit_card_expiration_month`,`credit_card_expiration_year`,`check_number`,`reg_type_user`, `paid_amount`)
                        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $stripe_customer_id), $processing_fee_type, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $buyer_first_name)
                    , mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone)
                    , mysqli_real_escape_string($this->db, $order_note_details), mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $payment_amount)
                    , mysqli_real_escape_string($this->db, $registration_from), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $stripe_card_name)
                    , mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $check_number)
                    , mysqli_real_escape_string($this->db, $reg_type_user), mysqli_real_escape_string($this->db, $insert_paid_amount));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $misc_order_id = mysqli_insert_id($this->db);
                $current_date = date("Y-m-d");
                if ($payment_amount > 0) {
                    $payment_query = sprintf("INSERT INTO `misc_payment`(`credit_method`,`check_number`,`misc_order_id`, `student_id`, `processing_fee`, `checkout_status`, `payment_amount`, `payment_date`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `payment_from`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                            mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $processing_fee), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $payment_status), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $payment_from));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $misc_payment_id = mysqli_insert_id($this->db);
                }
                // update card name and card expiration month and year in table
                if(!empty($payment_method_id)) { 
                    try {
                        $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $brand = $retrive_payment_method->card->brand;
                        $cc_month = $retrive_payment_method->card->exp_month;
                        $cc_year = $retrive_payment_method->card->exp_year;
                        $last4 = $retrive_payment_method->card->last4;
                        $temp = ' xxxxxx';
                        $stripe_card_name = $brand . $temp . $last4;
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                    $update_card_query1 = sprintf("UPDATE `misc_order` SET `stripe_card_name`='%s', `credit_card_expiration_month`='%s', `credit_card_expiration_year`='%s' WHERE `misc_order_id`='%s'", mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $misc_order_id));
                    $update_card_result1 = mysqli_query($this->db, $update_card_query1);
                    if (!$update_card_result1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_card_query1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $update_card_query2 = sprintf("UPDATE `misc_payment` SET `stripe_card_name`='%s' WHERE `misc_payment_id`='%s' AND `misc_order_id`='%s' AND `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $stripe_card_name),mysqli_real_escape_string($this->db, $misc_payment_id),mysqli_real_escape_string($this->db, $misc_order_id),mysqli_real_escape_string($this->db, $payment_intent_id));
                    $update_card_result2 = mysqli_query($this->db, $update_card_query2);
                    if (!$update_card_result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_card_query2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
                $this->addMiscDimensions($company_id, '', $insert_paid_amount);
                // Add Note Misc
                if (!empty(trim($order_note_details))) {
                    $note_text = "Order note: " . $order_note_details;
                } else {
                    $note_text = "Order note: N/A";
                }
                $sql = sprintf("INSERT INTO `misc_notes` (`company_id`,`student_id`, `misc_order_id`, `activity_text`) VALUES ('%s','%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $misc_order_id), mysqli_real_escape_string($this->db, $note_text));
                $result = mysqli_query($this->db, $sql);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
                if($has_3D_secure=='N'){
                    $text = "Miscellaneous charge was successful. Email confirmation sent.";
                }else{
                    $text = "Miscellaneous charge was processed and waiting for customer action. Kindly review and authenticate the payment.";
                }
                $msg = array("status" => "Success", "msg" => $text);
                $this->responseWithWepay($this->json($msg), 200);
                if($has_3D_secure=='N'){
                    $this->sendOrderReceiptForMiscPayment($company_id, $misc_order_id);
                }else{
                    $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Miscellaneous charge. Kindly click the launch button for redirecting to Stripe.</p>';
                    $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                    $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                }
            }
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
        date_default_timezone_set($curr_time_zone);
    }

    public function payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type){
        if ($cc_type == 'N') {   // ONLY CREATE CUSTOMER FOR NEW CREDIT CARDS
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");
            try {
                $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                $payment_method->attach(['customer' => $stripe_customer_id]);
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }
            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200);
            }
        }

        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try {
            $payment_intent_create = \Stripe\PaymentIntent::create([
                        'payment_method' => $payment_method_id,
                        'customer' => $stripe_customer_id,
                        'amount' => $amount_in_dollars,
                        'currency' => $stripe_currency,
                        'confirmation_method' => 'automatic',
                        'capture_method' => 'automatic',
                        'confirm' => true,
                        'return_url' => $this->server_url."/stripeUpdation.php?type=authenticate&c_id=$company_id&p_type=$p_type&s_id=$student_id",
                        'metadata' =>['c_id' => $company_id,'p_type' => $p_type,'s_id' => $student_id ,'s_name' => $studio_name],
                        'setup_future_usage' => 'off_session',
                        'description' => 'Payment Intent for ' . $buyer_email,
                        'payment_method_types' => ['card'],
                        'payment_method_options' => ['card' => ['request_three_d_secure' => 'any']],
                        'receipt_email' => $buyer_email,
                        'transfer_data' => ['destination' => $stripe_account_id, 'amount' => $dest_amount_in_dollars],
                        'on_behalf_of' => $stripe_account_id,
                        'statement_descriptor' => substr($studio_name,0,21),
            ]);
            $body3 = json_encode($payment_intent_create);
            stripe_log_info("Stripe Payment Creation : $body3");
            $return_array['temp_payment_status'] = $payment_intent_create->status;
            if ($return_array['temp_payment_status'] == 'succeeded'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['cc_month'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_month;
                $return_array['cc_year'] = $payment_intent_create->charges->data[0]->payment_method_details->card->exp_year;
                $brand = $payment_intent_create->charges->data[0]->payment_method_details->card->brand;
                $last4 = $payment_intent_create->charges->data[0]->payment_method_details->card->last4;
                $temp = ' xxxxxx';
                $return_array['stripe_card_name'] = $brand . $temp . $last4;
            }else if ($return_array['temp_payment_status'] == 'requires_action'){
                $return_array['payment_intent_id'] = $payment_intent_create->id;
                $return_array['payment_intent_client_secret'] = $payment_intent_create->client_secret;
                $return_array['next_action'] = $payment_intent_create->next_action;
//                $return_array['next_action']['type'] = $payment_intent_create->next_action->type;
//                $return_array['next_action']['redirect_to_url']['url'] = $payment_intent_create->next_action->redirect_to_url->url;
            }
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe     
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        return $return_array;
    }

    protected function checkCountryPaymentSupport($company_id, $country, $callback) { // GET COUNTRY PAYMENT SUPPORT
        $row = [];
        $sql = sprintf("SELECT * FROM country_payment_specific WHERE `payment_support` = 'S' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        UNION
                        SELECT * FROM country_payment_specific WHERE `payment_support` = 'W' AND `country` = '%s' AND `support_status` = 'Y' AND `company_id` in (0,'%s')
                        ORDER BY `payment_support` ASC, `company_id` DESC", mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id)
                , mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($callback == 0) {
                    $out = array("status" => "Success", "msg" => $row);
                } else {
                    $row['status'] = 'Success';
                    return $row;
                }
                $this->response($this->json($out), 200);
            } else {
                $out = array("status" => "Failed", "msg" => "Studio country is not yet available for payment support.");
                return $out;
            }
        }
    }
    
    protected function setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id){
        $out = [];
        $stripe_customer_id = "";
        
        $query2 = sprintf("SELECT c.`company_name`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
        $result2 = mysqli_query($this->db, $query2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $row = mysqli_fetch_assoc($result2);
            $stripe_account_id = $row['account_id'];
            $studio_name = $row['company_name'];
        }
        if (is_null($actual_student_id) || $actual_student_id == 0) {
            $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
            $student_id = $stud_array['student_id'];
            $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
        } else {
            $student_id = $actual_student_id;
            if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
            }
        }
        
        //attaching customer to payment method - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
            $payment_method->attach(['customer' => $stripe_customer_id]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        // creatiing setup indent to process payment - arun 
        $this->getStripeKeys();
        \Stripe\Stripe::setApiKey($this->stripe_sk);
        \Stripe\Stripe::setApiVersion("2019-05-16");
        try{
            $create_setup_indent = \Stripe\SetupIntent::create([
                                'payment_method_types' => ['card'],
                                'customer' => $stripe_customer_id,
                                'payment_method' => $payment_method_id,
                                'payment_method_options' => ['card'=>['request_three_d_secure'=>'any']],
                                'confirm' => true,
                                'on_behalf_of' => $stripe_account_id, // only accepted for card payments
            ]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught       
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed (maybe you changed API keys recently)      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe      
            $body = $e->getJsonBody();
            $err = $body['error'];
            $err['status_code'] = $e->getHttpStatus();
        }
        if (isset($err) && !empty($err)) {
            $error = array('status' => "Failed", "msg" => $err['message']);
            $this->response($this->json($error), 200);
        }
        if ($create_setup_indent->status == 'requires_action' && $create_setup_indent->next_action->type == 'redirect_to_url') {
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
            $out['requires_action'] = true;
            $out['payment_intent_client_secret'] = $create_setup_indent->client_secret;
        } else if ($create_setup_indent->status == 'succeeded') {
            # The payment didn’t need any additional actions and completed!
            # # Handle post-payment fulfillment
            $out['status'] = 'Success';
            $out['msg'] = 'Setup Intent created successfully.';
        } else {
            # Invalid status
            $out['status'] = 'Failed';
            $out['msg'] = 'Invalid SetupIntent status.';
            $out['error'] = 'Invalid SetupIntent status';
        }
       $this->response($this->json($out), 200);
    }
    
    public function stripeCreateretailCheckout($optional_discount_flag, $optional_discount_amount, $check_number, $payment_method, $company_id, $actual_student_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code, $buyer_street, $buyer_city, $buyer_state, $buyer_zip, $country, $payment_method_id, $discount_code, $cart_details, $order_type_user, $order_version_user, $payment_amount,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id) {
        
        $this->getStudioSubscriptionStatus($company_id);
        $stripe_account = $this->getCompanyStripeAccount($company_id, 1); // CALL BACK - 0/1
         
        if ( ($stripe_account != '' && $stripe_account['charges_enabled'] == 'Y' && $payment_amount > 0) ||  $payment_amount == 0) {
            $selecsql = sprintf("SELECT c.retail_enabled FROM `company` c WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $selectresult = mysqli_query($this->db, $selecsql);
            if (!$selectresult) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$selecsql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                if (mysqli_num_rows($selectresult) > 0) {
                    $statusoutput = mysqli_fetch_assoc($selectresult);
                    $retail_enable_flag = $statusoutput['retail_enabled'];
                    if ($statusoutput['retail_enabled'] == 'N') {
                        $res = array('status' => "Failed", "msg" => "Retail Setting is disabled. Please contact the business operator for more information.");
                        $this->response($this->json($res), 200);
                    }
                }
            }

            $query = sprintf("SELECT c.`company_name`,c.`wepay_status`,c.`stripe_status`,c.`wp_currency_symbol`,s.`account_state`,s.`currency`,s.`account_id` FROM `company` c LEFT JOIN `stripe_account` s ON c.`company_id` = s.`company_id` WHERE c.`company_id`='%s'", mysqli_real_escape_string($this->db, $company_id));
            $result = mysqli_query($this->db, $query);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $num_rows = mysqli_num_rows($result);
                if ($num_rows > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $studio_name = $row['company_name'];
                    $studio_stripe_status = $row['stripe_status'];
                    $stripe_account_state = $row['account_state'];
                    $studio_wepay_status = $row['wepay_status'];  
                    $stripe_currency = $row['currency'];
                    $stripe_account_id = $row['account_id'];
                    $stripe_currency_symbol = $row['wp_currency_symbol'];
                }
            }

            $retail_array = $this->checkRetailQuantityDuringCheckout($company_id, trim($discount_code), $cart_details, 'Y', 1, $optional_discount_flag, $optional_discount_amount, $payment_method);
            if ($retail_array['price_error_flag'] == 'Y') {
                $error = array("status" => "Failed", "msg" => $retail_array['price_error_msg']);
                $this->response($this->json($error), 200);
            }
            
            if (is_null($actual_student_id) || $actual_student_id == 0) {
                $stud_array = $this->getStudentId($company_id, $buyer_first_name, $buyer_last_name, $buyer_email, 'Stripe', $studio_name);
                $student_id = $stud_array['student_id'];
                $stripe_customer_id = !empty(trim($stud_array['stripe_customer_id'])) ? $stud_array['stripe_customer_id'] : '';
            } else {
                $student_id = $actual_student_id;
                if (is_null($stripe_customer_id) || empty(trim($stripe_customer_id))) {
                    $stripe_customer_id = $this->createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name);
                }
            }

            $user_timezone = $this->getUserTimezone($company_id, 200);
            $curr_time_zone = date_default_timezone_get();
            $new_timezone = $user_timezone['timezone'];
            date_default_timezone_set($new_timezone);
    //        $currdate_add ="SELECT DATE(CONVERT_TZ(now(),@@global.time_zone,'$new_timezone'))";
            $current_date = date("Y-m-d");
            usort($retail_array['retail_array'], function($a, $b) {
                return $a['retail_product_id'] > $b['retail_product_id'];
            });

            if (count($retail_array['retail_array']) > 0) {
                for ($i = 0; $i < count($retail_array['retail_array']); $i++) {
                    if (isset($retail_array['retail_array'][$i]['retail_product_id']) && empty($retail_array['retail_array'][$i]['retail_product_id'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 200);
                    }
                    if (isset($retail_array['retail_array'][$i]['quantity']) && empty($retail_array['retail_array'][$i]['quantity'])) {
                        $error = array("status" => "Failed", "msg" => "Invalid Request Parameters.");
                        $this->response($this->json($error), 200);
                    }
                }
            }

            $actual_order_amount = $retail_array['total_array']['total_order_price'];
            $payment_amount = $retail_array['total_array']['total_discounted_price'];
            $retail_order_discount_amount = $retail_array['total_array']['total_discount_value'];
            $discount_usage_count = $retail_array['total_array']['discount_usage_count'];
            $retail_products_tax = $retail_array['total_array']['total_tax_value'];
            $fee = $retail_array['total_array']['total_processing_fee'];
            if (isset($retail_array['total_array']['discount_id'])) {
                $discount = $retail_array['total_array']['discount_id'];
            } else {
                $discount = "";
            }

            $processing_fee_type = $retail_array['total_array']['processing_fee_type'];
            $retail_product_id1 = $retail_array['retail_array'][0]['retail_product_id'];
            $retail_product_title1 = $retail_array['retail_array'][0]['retail_product_title'];

            if ($stripe_account_state != 'N' && $studio_stripe_status == 'Y') {
                $cc_name = $cc_month = $cc_year = $stripe_card_name =  $payment_intent_id = '';
                $checkout_state = $cc_month = $cc_year = $stripe_card_name = $payment_status = '';
                $has_3D_secure = 'N';
                $w_paid_amount = 0;    //payment amount has added processing fee if fee_type is pass-on, no processing fee added if type is absorb
                if ($payment_amount > 0 && $payment_method == 'CC') {
                    $payment_type = 'CO';
                    $w_paid_amount = $paid_amount = $payment_amount;
                    $paid_fee = $fee;
                    $w_paid_amount_round = round($w_paid_amount, 2);
                    $dest_amount = $w_paid_amount - $paid_fee;
                    $dest_amount_round = round($dest_amount, 2);
                    $dest_amount_in_dollars = $dest_amount_round * 100;
                    $amount_in_dollars = $w_paid_amount_round * 100; 
                    $p_type = 'retail';
                    $payment_intent_result = $this->payment_intent_creation($company_id,$student_id,$payment_method_id,$stripe_customer_id,$amount_in_dollars,$stripe_currency,$studio_name,$buyer_email,$stripe_account_id,$dest_amount_in_dollars,$p_type,$cc_type);
                    if ($payment_intent_result['temp_payment_status'] == 'succeeded'){
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'released';
                    }elseif($payment_intent_result['temp_payment_status'] == 'requires_action') {
                        $payment_intent_id = $payment_intent_result['payment_intent_id'];
                        $checkout_state = 'requires_action';
                        $has_3D_secure = 'Y';
                        $next_action_type = $payment_intent_result['next_action']['type'];
                        if($next_action_type != "redirect_to_url"){
                            # Payment not supported
                            $out['status'] = 'Failed';
                            $out['msg'] = 'We are unable to authenticate your payment method. Please choose a different payment method and try again.';
                            $this->response($this->json($out), 200);
                        }
                        $button_url = $payment_intent_result['next_action']['redirect_to_url']['url'];
                    } else {
                        # Invalid status
                        $out['status'] = 'Failed';
                        $out['msg'] = 'Invalid SetupIntent status.';
                        $out['error'] = 'Invalid SetupIntent status';
                        $this->response($this->json($out), 200);
                    }
                    $payment_status = 'S';
                } else if ($payment_amount == 0) {
                    $payment_type = 'F';
                    $payment_status = 'S';
                }else{
                    $payment_type = 'CO';
                    $payment_status = 'S';
                }
                
                
                if(!empty($payment_method_id)){
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    try {
                        $retrive_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                        $brand = $retrive_payment_method->card->brand;
                        $cc_month = $retrive_payment_method->card->exp_month;
                        $cc_year = $retrive_payment_method->card->exp_year;
                        $last4 = $retrive_payment_method->card->last4;
                        $temp = ' xxxxxx';
                        $stripe_card_name = $brand . $temp . $last4;
                    } catch (\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send yourself an email     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe     
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        $err['status_code'] = $e->getHttpStatus();
                    }
                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                }
                
                $payment_from = 'S';
                $registration_from = 'S';            
                $retail_order_discount_id = $retail_discount_value = $retail_discount_min_req_value = 0;
                $retail_order_discount_type = $retail_discount_applied_type = $retail_order_discount_code = $retail_discount_min_req_flag = '';
                $select_discount = sprintf("SELECT * FROM `retail_discount` WHERE `retail_discount_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $company_id));
                $result_disc = mysqli_query($this->db, $select_discount);
                if (!$result_disc) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select_discount");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $num_of_rows = mysqli_num_rows($result_disc);
                    if ($num_of_rows > 0) {
                        $row = mysqli_fetch_assoc($result_disc);
                        $retail_discount_value = $row['retail_discount_amount'];
                        $retail_order_discount_code = $row['retail_discount_code'];
                        $retail_discount_min_req_flag = $row['min_purchase_flag'];
                        $retail_discount_min_req_value = $row['min_purchase_quantity'];
                        $retail_order_discount_type = $row['retail_discount_type'];
                        $retail_discount_applied_type = $row['retail_discount_usage_type'];
                        $retail_order_discount_id = $row['retail_discount_id'];
                    } else {
                        if ($optional_discount_flag == 'Y' && $optional_discount_amount > 0) {
                            $retail_discount_value = $optional_discount_amount;
                            $retail_discount_min_req_flag = 'N';
                            $retail_order_discount_type = 'V';
                            $retail_discount_applied_type = 'O';
                        }
                    }
                }
                if ($payment_method === "CA" || $payment_method === "CH") {//retail_order_paid_amount = 0 in db for cash and check
                    $query = sprintf("INSERT INTO `retail_orders`(retail_order_paid_amount,`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `student_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`,`buyer_street`,`buyer_city`,`buyer_state`,`buyer_zip`,`buyer_country`,`retail_order_date`,
                                `retail_order_amount`, `processing_fee_type`, `processing_fee_val`, `payment_type`, `retail_order_checkout_amount`,`retail_order_discount_amount`,`retail_discount_id`,`retail_discount_code`,`retail_discount_type`,`retail_discount_applied_type`,`retail_discount_value`,`retail_discount_min_req_flag`,
                                `retail_discount_min_req_value`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `retail_order_user_type`,`retail_order_user_version`,`retail_products_tax`)
                                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $optional_discount_flag), 
                            mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $payment_method), 
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $student_id), 
                            mysqli_real_escape_string($this->db, $buyer_first_name), mysqli_real_escape_string($this->db, $buyer_last_name),
                            mysqli_real_escape_string($this->db, $buyer_email), mysqli_real_escape_string($this->db, $buyer_phone), 
                            mysqli_real_escape_string($this->db, $buyer_postal_code), mysqli_real_escape_string($this->db, $buyer_street),
                            mysqli_real_escape_string($this->db, $buyer_city), mysqli_real_escape_string($this->db, $buyer_state), 
                            mysqli_real_escape_string($this->db, $buyer_zip), mysqli_real_escape_string($this->db, $country),
                            mysqli_real_escape_string($this->db, $current_date), mysqli_real_escape_string($this->db, $actual_order_amount), 
                            mysqli_real_escape_string($this->db, $processing_fee_type), mysqli_real_escape_string($this->db, $fee), 
                            mysqli_real_escape_string($this->db, $payment_type), mysqli_real_escape_string($this->db, $payment_amount),
                            mysqli_real_escape_string($this->db, $retail_order_discount_amount), mysqli_real_escape_string($this->db, $retail_order_discount_id), 
                            mysqli_real_escape_string($this->db, $retail_order_discount_code), mysqli_real_escape_string($this->db, $retail_order_discount_type), 
                            mysqli_real_escape_string($this->db, $retail_discount_applied_type), mysqli_real_escape_string($this->db, $retail_discount_value), 
                            mysqli_real_escape_string($this->db, $retail_discount_min_req_flag), mysqli_real_escape_string($this->db, $retail_discount_min_req_value), 
                            mysqli_real_escape_string($this->db, $cc_name), mysqli_real_escape_string($this->db, $cc_month), 
                            mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $order_type_user), 
                            mysqli_real_escape_string($this->db, $order_version_user), mysqli_real_escape_string($this->db, $retail_products_tax));
                    $payment_cash_string = "`actual_paid_date`, ";
                    $payment_cash_date = "CURDATE(),";
                    $temp_payment_status = 'M';
                } else {
                    $query = sprintf("INSERT INTO `retail_orders`(`optional_discount_flag`,`check_number`,`payment_method`,`company_id`, `student_id`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`,`buyer_street`,`buyer_city`,`buyer_state`,`buyer_zip`,`buyer_country`,`retail_order_date`,
                                `retail_order_amount`, `processing_fee_type`, `processing_fee_val`, `payment_type`, `retail_order_checkout_amount`,`retail_order_discount_amount`,`retail_discount_id`,`retail_discount_code`,`retail_discount_type`,`retail_discount_applied_type`,`retail_discount_value`,`retail_discount_min_req_flag`,
                                `retail_discount_min_req_value`,`stripe_customer_id`, `payment_method_id`,`registration_from`, `stripe_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `retail_order_user_type`,`retail_order_user_version`,`retail_products_tax`)
                                VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                            mysqli_real_escape_string($this->db, $optional_discount_flag), mysqli_real_escape_string($this->db, $check_number), 
                            mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $company_id), 
                            mysqli_real_escape_string($this->db, $student_id), mysqli_real_escape_string($this->db, $buyer_first_name), 
                            mysqli_real_escape_string($this->db, $buyer_last_name), mysqli_real_escape_string($this->db, $buyer_email), 
                            mysqli_real_escape_string($this->db, $buyer_phone), mysqli_real_escape_string($this->db, $buyer_postal_code), 
                            mysqli_real_escape_string($this->db, $buyer_street), mysqli_real_escape_string($this->db, $buyer_city), 
                            mysqli_real_escape_string($this->db, $buyer_state), mysqli_real_escape_string($this->db, $buyer_zip), 
                            mysqli_real_escape_string($this->db, $country), mysqli_real_escape_string($this->db, $current_date), 
                            mysqli_real_escape_string($this->db, $actual_order_amount), mysqli_real_escape_string($this->db, $processing_fee_type), 
                            mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $payment_type), 
                            mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $retail_order_discount_amount), 
                            mysqli_real_escape_string($this->db, $retail_order_discount_id), mysqli_real_escape_string($this->db, $retail_order_discount_code), 
                            mysqli_real_escape_string($this->db, $retail_order_discount_type), mysqli_real_escape_string($this->db, $retail_discount_applied_type), 
                            mysqli_real_escape_string($this->db, $retail_discount_value), mysqli_real_escape_string($this->db, $retail_discount_min_req_flag), 
                            mysqli_real_escape_string($this->db, $retail_discount_min_req_value), mysqli_real_escape_string($this->db, $stripe_customer_id), 
                            mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $registration_from), 
                            mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $cc_month), 
                            mysqli_real_escape_string($this->db, $cc_year), mysqli_real_escape_string($this->db, $order_type_user), 
                            mysqli_real_escape_string($this->db, $order_version_user), mysqli_real_escape_string($this->db, $retail_products_tax));
                    $payment_cash_string = "";
                    $payment_cash_date = "";
                    $temp_payment_status = 'S';
                }
                $result = mysqli_query($this->db, $query);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    $retail_order_id = mysqli_insert_id($this->db);

                    $payment_query = sprintf("INSERT INTO `retail_payment`($payment_cash_string`credit_method`,`check_number`,`retail_order_id`, `processing_fee`, `checkout_status`, `payment_amount`, `payment_date`,`payment_from`, `payment_status`, `payment_method_id`, `payment_intent_id`, `stripe_card_name`, `retail_tax_payment`) VALUES ($payment_cash_date'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", 
                            mysqli_real_escape_string($this->db, $payment_method), mysqli_real_escape_string($this->db, $check_number), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $fee), mysqli_real_escape_string($this->db, $checkout_state), mysqli_real_escape_string($this->db, $payment_amount), mysqli_real_escape_string($this->db, $current_date), 
                            mysqli_real_escape_string($this->db,$payment_from), mysqli_real_escape_string($this->db, $temp_payment_status), mysqli_real_escape_string($this->db, $payment_method_id), mysqli_real_escape_string($this->db, $payment_intent_id), mysqli_real_escape_string($this->db, $stripe_card_name), mysqli_real_escape_string($this->db, $retail_products_tax));
                    $payment_result = mysqli_query($this->db, $payment_query);
                    if (!$payment_result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$payment_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    } else {
                        $update_discount = sprintf("UPDATE `retail_discount` SET `discount_used_count`=`discount_used_count`+$discount_usage_count WHERE `retail_discount_id`='%s' AND `company_id`='%s'", mysqli_real_escape_string($this->db, $discount), mysqli_real_escape_string($this->db, $company_id));
                        $update_result = mysqli_query($this->db, $update_discount);
                        if (!$update_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_discount");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                    if (count($retail_array['retail_array']) > 0) {
                        $qty = $net_sales = 0;
                        $temp = $temp2 = $temp3 = $cretail_product_cost = $cprocessing_fee = $cquantity = $cpayment_amount = 0;
                        $cdiscount = $cretail_variant_id = $cretail_product_id = $cretail_parent_id = $note = $cretail_product_name = $cretail_variant_name = '';

                        for ($i = 0; $i < count($retail_array['retail_array']); $i++) {
                            $cquantity = $retail_array['retail_array'][$i]['quantity'];
                            $cdiscount = $retail_array['retail_array'][$i]['discounted_value'];
                            $cpayment_amount = $retail_array['retail_array'][$i]['retail_total_product_price'];
                            $cdiscounted_price = $retail_array['retail_array'][$i]['retail_discounted_price'];

    //                                $cprocessing_fee = $retail_array['retail_array'][$i]['processing_fee'];
                            if (!empty(trim($retail_array['retail_array'][$i]['retail_variant_id']))) {
                                $cretail_variant_id = $retail_array['retail_array'][$i]['retail_variant_id'];
                            } else {
                                $cretail_variant_id = '0';
                            }
                            $cretail_product_id = $retail_array['retail_array'][$i]['retail_product_id'];
                            $cretail_parent_id = $retail_array['retail_array'][$i]['retail_parent_id'];
                            $cretail_product_name = $retail_array['retail_array'][$i]['retail_product_title'];
                            $cretail_variant_name = $retail_array['retail_array'][$i]['retail_variant_title'];
                            $cretail_product_cost = $retail_array['retail_array'][$i]['retail_product_price'];
                            $cretail_tax_rate = $retail_array['retail_array'][$i]['product_tax_rate'];
                            $cretail_tax_value = $retail_array['retail_array'][$i]['product_tax_value'];
                            $final_net_update = $cpayment_amount - $cdiscount + $cretail_tax_value;

                            $cstatus = 'N';
                            if ($cdiscounted_price == 0) {
                                $cstatus = 'S';
                            }
                            $str = $str1 = '';
                            if (empty($cretail_parent_id) || $cretail_parent_id == 0) {
                                $str = "IN (%s)";
                                $str1 = 'NULL';
                                $this->addretailDimensions($company_id, $cretail_product_id, '', '');
                                if ($payment_method === 'CH' || $payment_method === 'CA') {
                                    $this->updateRetailDimensionsNetSales($company_id, $cretail_product_id, '', '', $final_net_update);
                                }
                            } else {
                                $str = "IN (%s, $cretail_parent_id)";
                                $str1 = "$cretail_parent_id";
                                $this->addretailDimensions($company_id, $cretail_parent_id, $cretail_product_id, '');
                                if ($payment_method === 'CH' || $payment_method === 'CA') {
                                    $this->updateRetailDimensionsNetSales($company_id, $cretail_parent_id, $cretail_product_id, '', $final_net_update);
                                }
                            }
                            if ($payment_method === 'CH' || $payment_method === 'CA') {
                                $reg_det_query = sprintf("INSERT INTO `retail_order_details`(`company_id`,`retail_order_id`, `retail_product_id`, `retail_parent_id`,`retail_variant_id`,`retail_product_name`,`retail_variant_name`, `retail_cost`, `retail_discount`, `retail_quantity`, `retail_payment_amount` , `retail_payment_status`, `retail_tax_percentage`, `rem_due`) VALUES ('%s','%s','%s',%s,%s,'%s','%s','%s','%s','%s','%s','S','%s',0)", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $cretail_product_id), mysqli_real_escape_string($this->db, $str1), mysqli_real_escape_string($this->db, $cretail_variant_id), mysqli_real_escape_string($this->db, $cretail_product_name), mysqli_real_escape_string($this->db, $cretail_variant_name), mysqli_real_escape_string($this->db, $cretail_product_cost), mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cretail_tax_rate));

                                $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $cretail_product_id));
                                $result_count = mysqli_query($this->db, $update_count);
                                if (!$result_count) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            } else {
                                $reg_det_query = sprintf("INSERT INTO `retail_order_details`(`company_id`,`retail_order_id`, `retail_product_id`, `retail_parent_id`,`retail_variant_id`,`retail_product_name`,`retail_variant_name`, `retail_cost`, `retail_discount`, `retail_quantity`, `retail_payment_amount` , `retail_payment_status`, `retail_tax_percentage`, `rem_due`) VALUES ('%s','%s','%s',%s,%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $cretail_product_id), mysqli_real_escape_string($this->db, $str1), mysqli_real_escape_string($this->db, $cretail_variant_id), mysqli_real_escape_string($this->db, $cretail_product_name), mysqli_real_escape_string($this->db, $cretail_variant_name), mysqli_real_escape_string($this->db, $cretail_product_cost), mysqli_real_escape_string($this->db, $cdiscount), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cpayment_amount), mysqli_real_escape_string($this->db, $cstatus), mysqli_real_escape_string($this->db, $cretail_tax_rate), mysqli_real_escape_string($this->db, $cpayment_amount - $cdiscount + $cretail_tax_value));
                            }
                            
                            $reg_det_result = mysqli_query($this->db, $reg_det_query);
                            if (!$reg_det_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$reg_det_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                $inserted_retail_order_detail_id = mysqli_insert_id($this->db);
                            }

                            $qty += $cquantity;
                            if (!empty($cretail_variant_id)) {
                                $update_retail_query = sprintf("UPDATE retail_variants SET inventory_used_count=inventory_used_count+'%s', `inventory_count`=`inventory_count`-'%s' WHERE `retail_variant_id`='%s'", mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cretail_variant_id));
                            } else {
                                $update_retail_query = sprintf("UPDATE `retail_products` SET `retail_purchase_count` = `retail_purchase_count`+'%s', retail_product_inventory=retail_product_inventory-'%s' WHERE `retail_product_id` $str", mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cquantity), mysqli_real_escape_string($this->db, $cretail_product_id));
                            }
                            $update_retail_result = mysqli_query($this->db, $update_retail_query);
                            if (!$update_retail_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_retail_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 200);
                            } else {
                                for ($k = 1; $k <= $cquantity; $k++) {
                                    $insert_full = sprintf("INSERT INTO `retail_order_fulfillment`(`company_id`,`retail_order_id`,`retail_order_detail_id`,`fulfillment_serial_no`,`fulfillment_status` )VALUES('%s','%s','%s',$k,'U')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $inserted_retail_order_detail_id));
                                    $insert_fullresult = mysqli_query($this->db, $insert_full);
                                    if (!$insert_fullresult) {
                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_full");
                                        log_info($this->json($error_log));
                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                        $this->response($this->json($error), 200);
                                    }
                                }
                            }
                        }
                        $note = "Ordered $qty items. Total paid $stripe_currency_symbol.$payment_amount";

                        if (trim(!empty($note))) {
                            $insert_note = sprintf("INSERT INTO `retail_history` (`company_id`, `retail_order_id`, `student_id`, `retail_order_fulfillment_id`, `activity_type`, `activity_text`) VALUES ('%s', %s, '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $student_id), '-1', 'order', mysqli_real_escape_string($this->db, $note));
                            $ins_result = mysqli_query($this->db, $insert_note);
                            if (!$ins_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_note");
                                log_info($this->json($error_log));
                            }
                        }
                    }
                    //update cc information if release state
                    if ($checkout_state == 'released') {
                         $this->updateStripeRetailCCDetailsAfterRelease($payment_intent_id);
                    }
                    //End update cc information if release state
                    if ($has_3D_secure == 'N') {
                        $text = "Retail purchase successful. Email confirmation sent.";
                    } else {
                        $text = "Retail purchase was processed and waiting for customer action. Kindly review and authenticate the payment.";
                    }
                    $msg = array("status" => "Success", "msg" => $text);
                    $this->responseWithWepay($this->json($msg), 200);
                    if ($has_3D_secure == 'N') {
                        $this->sendOrderReceiptForRetailPayment($company_id, $retail_order_id, 0);
                    } else {
                        $email_msg = 'Hi '.$buyer_first_name.' '.$buyer_last_name.',<br>';
                        $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">This is an authentication email for Retail purchase. Kindly click the launch button for redirecting to Stripe.</p>';
                        $email_msg .= '<p style="letter-spacing: 0.5px;color:#000000;font-weight: 500;">Complete a required action for this payment. Once the redirect is completed for this PaymentIntent, the payment will proceed.</p>';
                        $this->sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name,$email_msg);
                    }
//                    $msg = array("status" => "Success", "msg" => "Retail purchase successful. Email confirmation sent.");
//                    $this->responseWithWepay($this->json($msg), 200);
//                    $this->sendOrderReceiptForRetailPayment($company_id, $retail_order_id, 0);
                }
            } else {
                $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
                $this->response($this->json($error), 200);
            }

            date_default_timezone_set($curr_time_zone);
        } else {
            $error = array("status" => "Failed", "msg" => "Payment cannot be done. Contact your app administrator to enable payments.");
            $this->response($this->json($error), 200);
        }
    }
    public function updateStripeRetailCCDetailsAfterRelease($payment_intent_id){
        $checkout_state = $retail_order_id = $payment_id = $processing_fee_type = '';
        $retail_total_processing_fee = $fee = $payment_amount = 0;
        $retail_arr=[];
        $query = sprintf("SELECT rto.`company_id`,rod.`retail_order_detail_id`,rod.`retail_payment_amount`, rod.`retail_product_id`, rto.`retail_order_id`,rod.`retail_parent_id`,rod.`rem_due`, rp.`retail_payment_id`, rp.`checkout_id`, rp.`checkout_status`, rp.`payment_amount`, rp.`processing_fee`, rto.`processing_fee_type`,rto.`retail_discount_id`,rod.`retail_tax_percentage`,rod.`retail_discount`
                    FROM `retail_orders` rto 
                    LEFT JOIN `retail_payment` rp ON rto.`retail_order_id`=rp.`retail_order_id`
                    LEFT JOIN `retail_order_details` rod ON rto.`retail_order_id`=rod.`retail_order_id`
                    WHERE rp.`payment_intent_id` = '%s' AND rp.`payment_status` IN ('S', 'PR') ORDER BY rp.`retail_payment_id`", mysqli_real_escape_string($this->db, $payment_intent_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $k=0;
                while($row = mysqli_fetch_assoc($result)){
                    $retail_order_id = $row['retail_order_id'];
                    $payment_id = $row['retail_payment_id'];
                    $retail_total_processing_fee = $fee = $row['processing_fee'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_amount = $row['payment_amount'];
                    $company_id = $row['company_id'];
                    $retail_arr[$k]['retail_order_detail_id'] = $row['retail_order_detail_id'];
                    $retail_arr[$k]['retail_product_id'] = $row['retail_product_id'];
                    $retail_arr[$k]['retail_parent_id'] = $row['retail_parent_id'];
                    $retail_arr[$k]['retail_price'] = $row['retail_payment_amount'];
                    $retail_arr[$k]['retail_tax_percentage'] = $row['retail_tax_percentage'];
                    $retail_arr[$k]['retail_discount'] = $row['retail_discount'];
                    $retail_arr[$k]['rem_due'] = $row['rem_due'];
                    $k++;
                }
                $payment_status = "";
                $checkout_state = "released";
                $update_amount = 0;
                $paid_amount = $payment_amount;
                if($processing_fee_type==1){
                    $total_amount = $update_amount = $payment_amount;
                }elseif($processing_fee_type==2){
                    $total_amount = $update_amount = $payment_amount - $fee;
                }
                $update_sales_in_reg = sprintf("UPDATE `retail_orders` SET `retail_order_paid_amount`=`retail_order_paid_amount`+$paid_amount WHERE `retail_order_id`='%s'", mysqli_real_escape_string($this->db, $retail_order_id));
                $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);

                if(!$result_sales_in_reg){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                    stripe_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
                for($i=0;$i<count($retail_arr);$i++){
                    $retail_order_detail_id = $retail_arr[$i]['retail_order_detail_id'];
                    $retail_parent_id=$retail_arr[$i]['retail_parent_id'];
                    $retail_product_id=$retail_arr[$i]['retail_product_id'];
                    $rem_due = $retail_arr[$i]['rem_due'];
                    $str = '';

                    if($rem_due>0 && $update_amount>0){
                        $due = $sales_due = 0;
                        if($rem_due>$update_amount){
                            $sales_due = $update_amount;
                            $due = $rem_due - $update_amount;
                            $update_amount = 0;
                            if($due<.5){
                                $sts = 'S';
                            }else{
                                $sts = 'P';
                            }
                        }else{
                            $sales_due = $rem_due;
                            $due = 0;
                            $sts = 'S';
                            $update_amount -= $rem_due;
                        }
                        $product_processing_fee = $retail_total_processing_fee * ($retail_arr[$i]['rem_due']/$total_amount);
                        $final_net_update = $sales_due;
                        if($processing_fee_type==1){
                            $final_net_update = $sales_due-$product_processing_fee;
                        }
                        if (empty($retail_parent_id) || is_null($retail_parent_id)) {
                            $str = "IN (%s)";
                            $this->addretailDimensions($company_id, $retail_arr[$i]['retail_product_id'], '', '');
                            $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_product_id'], '', '', $final_net_update);
                        } else {
                            $str = "IN (%s,$retail_parent_id)";
                            $this->addretailDimensions($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '');
                            $this->updateRetailDimensionsNetSales($company_id, $retail_arr[$i]['retail_parent_id'], $retail_arr[$i]['retail_product_id'], '', $final_net_update);
                        }

                        $update_order_details = sprintf("UPDATE `retail_order_details` SET `rem_due`='%s', `retail_payment_status`='$sts' WHERE `company_id`='%s' AND `retail_order_id`='%s' AND `retail_order_detail_id`='%s'", mysqli_real_escape_string($this->db, $due),
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $retail_order_detail_id));
                        $result_order_details = mysqli_query($this->db, $update_order_details);
                        if(!$result_order_details){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_order_details");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }

                        $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$final_net_update WHERE `company_id`='%s' AND `retail_product_id` $str",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            stripe_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
                
                $update_query = sprintf("UPDATE `retail_payment` SET `checkout_status`='%s', `actual_paid_date`=CURDATE() $payment_status WHERE `retail_order_id`='%s' and `retail_payment_id`='%s' and `payment_intent_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $payment_intent_id));
                $update_result = mysqli_query($this->db, $update_query);
                if(!$update_result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                    stripe_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
    public function getCompanyStripeAccount($company_id, $call_back){
        $query = sprintf("SELECT * FROM `stripe_account` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
        $result = mysqli_query($this->db,$query);
        if(!$result){           
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            if(mysqli_num_rows($result) >0){
                $row = mysqli_fetch_assoc($result);
                if($call_back == 1){
                    return $row;
                }else{
                    $msg = array("status" => "Success", "msg" => "Stripe Account details", "stripe_details" => "$row");
                    $this->response($this->json($msg), 200);
                }
            }else{
                if($call_back == 1){
                    return '';
                }else{
                    $msg = array("status" => "Failed", "msg" => "Stripe Account details", "stripe_details" => "");
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    
    public function createStripeCustomer($buyer_email, $student_id, $company_id, $studio_name){
        $stripe_customer_id = '';
        $query = sprintf("SELECT `stripe_customer_id` FROM `student` WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $student_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $stripe_customer_id = $row['stripe_customer_id'];
            }
        }
        if(is_null($stripe_customer_id) || empty(trim($stripe_customer_id))){
            $this->getStripeKeys();
            \Stripe\Stripe::setApiKey($this->stripe_sk);
            \Stripe\Stripe::setApiVersion("2019-05-16");

            try {
                $stripe_customer_create = \Stripe\Customer::create([
                            "email" => $buyer_email,
                            "description" => "Customer created with $buyer_email for $studio_name($company_id)",
                ]);
                $body = json_encode($stripe_customer_create);
                stripe_log_info("Customer_new : $body");
                $body_arr = json_decode($body, true);
                $stripe_customer_id = $body_arr['id'];
                
                //update stripe customer id to db - arun 
                $query2 = sprintf("UPDATE `student` SET `stripe_customer_id` = '%s' WHERE `student_id`='%s'", mysqli_real_escape_string($this->db, $stripe_customer_id), mysqli_real_escape_string($this->db, $student_id));
                $result2 = mysqli_query($this->db, $query2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                }
            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught       
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed (maybe you changed API keys recently)      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send yourself an email      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe      
                $body = $e->getJsonBody();
                $err = $body['error'];
                $err['status_code'] = $e->getHttpStatus();
            }

            if (isset($err) && !empty($err)) {
                $error = array('status' => "Failed", "msg" => $err['message']);
                $this->response($this->json($error), 200); // If no records "No Content" status
            }
        }
        
        return $stripe_customer_id;
    }
    
    public function sendAuthenticationEmailFor3DSecure($company_id, $button_url, $buyer_email, $studio_name, $email_msg){
        
        // EMAIL CONTENT
        $button_text = 'Launch';
        $subject = 'Stripe authenticate';
        $message = $email_msg.'<br>'.'<center><a href="'.$button_url.'"><button type="button" style="width: 190px;border-radius: 5px;background: #009a61;border:1px solid 009a61;color: #ffffff;font-weight: 600;box-shadow: 0 1px 2px rgba(0,0,0,.1) inset;height: 45px;font-size: 16px;letter-spacing: 0.5px;margin: 20px 0;font-family: avenir;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;text-transform: none;overflow: visible;box-sizing: border-box;cursor: pointer;">'.$button_text.'</button></a></center>';
        $sendEmail_status = $this->sendEmail($buyer_email, $subject, $message, $studio_name, '', '', '', '', $company_id, '');
        if (isset($sendEmail_status['status']) && $sendEmail_status['status'] == "true") {
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email . "   " . $sendEmail_status['mail_status']);
            log_info($this->json($error_log));
        } else {
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            log_info($this->json($error_log));
        }
    }
    
    public function getIntentDetails($c_id, $s_id, $p_type, $intent_id, $intent_type, $call_back) {
        $intent_retrieve = "";
        if ($p_type == 'misc') {
            $query = sprintf("SELECT ms.`company_id`, ms.`student_id`, msp.`checkout_status` as intent_status
                    FROM `misc_order` ms LEFT JOIN `misc_payment` msp ON ms.`misc_order_id`=msp.`misc_order_id`
                    WHERE  ms.`company_id` = '%s' AND ms.`student_id` = '%s' AND msp.`payment_intent_id` = '%s' ORDER BY msp.`misc_payment_id` LIMIT 0,1", 
                    mysqli_real_escape_string($this->db, $c_id), mysqli_real_escape_string($this->db, $s_id),mysqli_real_escape_string($this->db, $intent_id));
        } else if ($p_type == 'trial') {
            $query = sprintf("SELECT tr.`company_id`, tr.`student_id`, tp.`checkout_status` as intent_status
                    FROM `trial_registration` tr LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id`
                    WHERE  tr.`company_id` = '%s' AND tr.`student_id` = '%s' AND tp.`payment_intent_id` = '%s' ORDER BY tp.`trial_payment_id` LIMIT 0,1", 
                    mysqli_real_escape_string($this->db, $c_id), mysqli_real_escape_string($this->db, $s_id),mysqli_real_escape_string($this->db, $intent_id));
        } else if ($p_type == 'retail') {
            $query = sprintf("SELECT ro.`company_id`, ro.`student_id`, rp.`checkout_status` as intent_status
                    FROM `retail_orders` ro LEFT JOIN `retail_payment` rp ON ro.`retail_order_id`=rp.`retail_order_id`
                    WHERE  ro.`company_id` = '%s' AND ro.`student_id` = '%s' AND rp.`payment_intent_id` = '%s' ORDER BY rp.`retail_payment_id` LIMIT 0,1", 
                    mysqli_real_escape_string($this->db, $c_id), mysqli_real_escape_string($this->db, $s_id), mysqli_real_escape_string($this->db, $intent_id));
        } else if ($p_type == 'event') {
            $query = sprintf("SELECT er.`company_id`, er.`student_id`, if($intent_type =='PI',ep.`checkout_status`, er.`setup_intent_status`) as intent_status
                    FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id`
                    WHERE  er.`company_id` = '%s' AND er.`student_id` = '%s' AND (ep.`payment_intent_id` = '%s' || er.`setup_intent_id` = '%s') ORDER BY ep.`event_payment_id` LIMIT 0,1",
                    mysqli_real_escape_string($this->db, $c_id), mysqli_real_escape_string($this->db, $s_id),mysqli_real_escape_string($this->db, $intent_id), mysqli_real_escape_string($this->db, $intent_id));
        } else if ($p_type == 'membership') {
            $query = sprintf("SELECT mr.`company_id`, mr.`student_id`, if($intent_type =='PI',mp.`checkout_status`, mr.`setup_intent_status`) as intent_status
                    FROM `membership_registration` mr LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id`
                    WHERE  mr.`company_id` = '%s' AND mr.`student_id` = '%s' AND (mp.`payment_intent_id` = '%s' || mr.`setup_intent_id` = '%s') ORDER BY mp.`membership_payment_id` LIMIT 0,1",
                    mysqli_real_escape_string($this->db, $c_id), mysqli_real_escape_string($this->db, $s_id),mysqli_real_escape_string($this->db, $intent_id), mysqli_real_escape_string($this->db, $intent_id));
        }

        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            stripe_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                $row = mysqli_fetch_assoc($result);
                if ($row['intent_status'] == 'released' || $row['intent_status'] == 'succeeded') {
                    $msg = array("status" => "Success", "msg" => "Authentication Process Successfully completed.");
                    if ($call_back == 1) {
                        return $msg;
                    } else {
                        $this->response($this->json($msg), 200);
                    }
                } else {
                    //retrieving payment intent / Setup intent
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");

                    if ($intent_type == "PI") {
                        try {
                            $intent_retrieve = \Stripe\PaymentIntent::retrieve($intent_id);
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                    } else if ($intent_type == "SI") {
                        try {
                            $intent_retrieve = \Stripe\SetupIntent::retrieve($intent_id);
                        } catch (\Stripe\Error\Card $e) {
                            // Since it's a decline, \Stripe\Error\Card will be caught     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\RateLimit $e) {
                            // Too many requests made to the API too quickly     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // Invalid parameters were supplied to Stripe's API     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Authentication $e) {
                            // Authentication with Stripe's API failed (maybe you changed API keys recently)     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network communication with Stripe failed     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (\Stripe\Error\Base $e) {
                            // Display a very generic error to the user, and maybe send yourself an email     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe     
                            $body = $e->getJsonBody();
                            $err = $body['error'];
                            $err['status_code'] = $e->getHttpStatus();
                        }
                    }

                    if (isset($err) && !empty($err)) {
                        $error = array('status' => "Failed", "msg" => $err['message']);
                        $this->response($this->json($error), 200);
                    }

                    if ($intent_retrieve->status === "succeeded") {
                        $msg = array("status" => "Success", "msg" => "Authentication Process completed Successfully.");
                        if ($call_back == 1) {
                            return $msg;
                        } else {
                            $this->response($this->json($msg), 200);
                        }
                    } else {
                        $msg = array("status" => "Failed", "msg" => "Authentication Process Declined.");
                        if ($call_back == 1) {
                            return $msg;
                        } else {
                            $this->response($this->json($msg), 200);
                        }
                    }
                }
            } else {
                $msg = array("status" => "Failed", "msg" => "Authentication Process Declined.");
                if ($call_back == 1) {
                    return $msg;
                } else {
                    $this->response($this->json($msg), 200);
                }
            }
        }
    }
    // create stripe onboardinglink 
    public function createStripeOnboardinglink($company_id) { // stripe onboarding view for update personal details
        $query = sprintf("SELECT `account_id` FROM `stripe_account` WHERE `company_id` = '%s'", mysqli_real_escape_string($this->db, $company_id));
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            if(mysqli_num_rows($result) > 0){
                $row = mysqli_fetch_assoc($result);
                $account_id = $row['account_id'];
                $curr_loc = $_SERVER['SERVER_NAME'];
                
//                $curr_loc = implode('/', explode('/', $curr_loc, -4));
                $success_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/stripeUpdation.php?comp_id=$company_id&type=success";
                $failure_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . "$curr_loc/stripeUpdation.php?comp_id=$company_id&type=failure";        
                if(!empty($account_id)){
                     // STRIPE CONNECTED ACCOUNT CREATION PROCESS
                    $this->getStripeKeys();
                    \Stripe\Stripe::setApiKey($this->stripe_sk);
                    \Stripe\Stripe::setApiVersion("2019-05-16");
                    try {
                        $update_account = \Stripe\AccountLink::create([
                            "account" => $account_id,
                            "success_url" => $success_url,
                            "failure_url" => $failure_url,
                            "type" => "custom_account_verification"
                        ]);
                    } catch (Stripe\Error\Permission $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Account Link for connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }catch (Stripe\Error\InvalidRequest $e) {
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        stripe_log_info("Account Link for connected Account - Error:".json_encode($body));
                        $error = array('status' => "Failed", 'msg' => $err['message']);
                        $this->response($this->json($error), 200);
                    }
                    $body = json_encode($update_account);                   
                    $account_updated_details = json_decode($body, true);
                    $out = array("status" => "Success", "msg" => $account_updated_details);
                    return $out;
                }else{
                    $error = array('status' => "Failed", "msg" => "Create stripe account first"); // Dont change Msg text - used in front end
                    $this->response($this->json($error), 200);
                }
            }else {
                $error = array('status' => "Failed", "msg" => "Create stripe account first"); // Dont change Msg text - used in front end
                $this->response($this->json($error), 200);
            }
        }
    }

}

?>