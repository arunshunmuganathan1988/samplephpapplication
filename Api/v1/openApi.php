<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once 'openModel.php';

class openApi extends openModel {

    public function __construct() {
        parent::__construct();          // Init parent contructor
    }

    public function processApi() {
        $func = '';
        
        if(isset($_REQUEST['run'])){
            $func = strtolower(trim(str_replace("/", "", $_REQUEST['run'])));
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
                $this->_request = json_decode(file_get_contents('php://input'), true);
            }
            if ($_SERVER['REQUEST_METHOD'] == 'PUT'){
                $this->_request = json_decode(file_get_contents('php://input'), true);
            }
            if($func=='leads' && $_SERVER['REQUEST_METHOD'] == 'DELETE'){
                $header_api_key = $delete_regid = $c_id = $url_delete_reg_id = '';

                if(isset($_SERVER["HTTP_ZAPHEADERAPIKEY"])){
                    $header_api_key  = $_SERVER["HTTP_ZAPHEADERAPIKEY"]; //For API_key In header
                }
                if(isset($_SERVER["HTTP_ID"])){
                    $delete_regid = $_SERVER["HTTP_ID"];
                }
                $new_url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); //For Delete Get Id from Header
                $delete_url_split = explode("/", trim($new_url, "/"));
                $url_delete_reg_id = $delete_url_split[3];

                if (empty($header_api_key)) {
                    $error = array('status' => "Failed", "msg" => "Authentication failed");
                    zap_log_info($this->json($error));
                    $this->response($this->json($error), 401);
                } else {
                    $c_id = $this->checkZapAuthentication($header_api_key);
                }

                if (!empty($c_id) && !empty($delete_regid)){
                    $this->deleteActionForLead($c_id,$delete_regid); 
                }else if(!empty($c_id) && !empty($url_delete_reg_id)){
                    $this->deleteActionForLead($c_id,$url_delete_reg_id);
                }else {
                    $error = array('status' => "Failed", "msg" => "Id Field is Required for deletion");
                    zap_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
            }
        }
        
        if ((int) method_exists($this, $func) > 0) {
            $this->$func();
        }else{
            $error = array("status" => "Failed", "code" => 404, "msg" => "Page not found.", "func" => $func);
            $this->response($this->json($error), 404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
    
    public function Leads() {
        
        $c_id = $header_api_key = $delete_regid = $url_delete_reg_id = $reg_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $program_interest = $source = $p_firstname = $p_lastname = $campaign = '';
        $testcheck = 'false';
        
        if(isset($_SERVER["HTTP_ZAPHEADERAPIKEY"])){
            $header_api_key  = $_SERVER["HTTP_ZAPHEADERAPIKEY"]; //For API_key In header
        }
        if(isset($_SERVER["HTTP_ID"])){
            $delete_regid = $_SERVER["HTTP_ID"]; // Delete ID
        }
        
        if (isset($this->_request['Id'])) {
            $reg_id = $this->_request['Id'];
        }
        if (isset($this->_request['Buyer_first_name'])) {
            $buyer_first_name = $this->_request['Buyer_first_name'];
        }
        if (isset($this->_request['Buyer_last_name'])) {
            $buyer_last_name = $this->_request['Buyer_last_name'];
        }
        if (isset($this->_request['Email'])) {
            $buyer_email = $this->_request['Email'];
        }
        if (isset($this->_request['Phone'])) {
            $buyer_phone = $this->_request['Phone'];
        }
        if (isset($this->_request['Program_interest'])) {
            $program_interest = $this->_request['Program_interest'];
        }
        if (isset($this->_request['Source'])) {
            $source = $this->_request['Source'];
        }
        if (isset($this->_request['Participant_first_name'])) {
            $p_firstname = $this->_request['Participant_first_name'];
        }
        if (isset($this->_request['Participant_last_name'])) {
            $p_lastname = $this->_request['Participant_last_name'];
        }
        if (isset($this->_request['Campaign'])) {
            $campaign = $this->_request['Campaign'];
        }
        if (empty($header_api_key)) {
            $error = array('status' => "Failed", "msg" => "Authentication failed");
            zap_log_info($this->json($error));
            $this->response($this->json($error), 401);
        } else {
            $c_id = $this->checkZapAuthentication($header_api_key);
        }
        
        // For Retrieve poll trigger
        if ($this->get_request_method() == "GET") {
            
            if (!empty($c_id)) {
                $this->getTriggerDetailsForLead($c_id);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid API Key provided");
                zap_log_info($this->json($error));
                $this->response($this->json($error), 401);
            }
        }
        
        // For POST Action
        else if ($this->get_request_method() == "POST") {
            if (!empty($c_id) && (!empty($buyer_first_name) || !empty($buyer_last_name)) && (!empty($buyer_email) || !empty($buyer_phone))) {
                $this->postActionForLead($c_id,$buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $program_interest, $source, $p_firstname, $p_lastname, $campaign);
            } else {
                $error = array('status' => "Failed", "msg" => "Required Fields Missing [(Buyer Firstname or Buyer Lastname) and (Buyerphone or Email) needed]. Kindly fill proper key in Data field ");
                zap_log_info($this->json($error));
                $this->response($this->json($error), 400);
            }
        }
        
        // For PUT Action
        else if($this->get_request_method() == "PUT") {
            if(!empty($c_id)){
                if(empty($reg_id)){
                    $error = array('status' => "Failed", "msg" => "Id Field is Required For Updation");
                    zap_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }else{
                    if ((empty($buyer_first_name) && empty($buyer_last_name)) || (empty($buyer_email) && empty($buyer_phone))) {
                        $error = array('status' => "Failed", "msg" => "Required Fields Missing [(Buyer Firstname or Buyer Lastname)and(Buyerphone or Email needed)]. Kindly fill proper key in Data field");
                        zap_log_info($this->json($error));
                        $this->response($this->json($error), 400);
                    }else{
                        $this->putActionForLead($c_id,$reg_id,$buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $program_interest, $source, $p_firstname, $p_lastname);
                    }
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid API Key Provided");
                $this->response($this->json($error), 401);
            }
        }
//        
//        // For Delete action
//        else if ($_SERVER['REQUEST_METHOD'] == 'DELETE'){
//            
//            $new_url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); //For Delete Get Id from Header
//            $delete_url_split = explode("/", trim($new_url, "/"));
//            $url_delete_reg_id = $delete_url_split[3];
//            
//            if (!empty($c_id) && !empty($delete_regid)){
//                $this->deleteActionForLead($c_id,$delete_regid); 
//            }else if(!empty($c_id) && !empty($url_delete_reg_id)){
//                $this->deleteActionForLead($c_id,$url_delete_reg_id);
//            }else {
//                $error = array('status' => "Failed", "msg" => "Id Field is Required for Deleting Lead");
//                zap_log_info($this->json($error));
//                $this->response($this->json($error), 400);
//            }
//        }
//        
        // Other Request Method
        else {
            $error = array('status' => "Failed", "msg" => "HTTP Method is Not Acceptable");
            zap_log_info($this->json($error));
            $this->response($this->json($error), 406);
        }
    }
    
    private function retailProducts(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $product_id = $token = $pos_email = '';
        $reg_type_user = 'U';
        $page_from = 'ML';      //ML - Main List for get all details from Mobile & Open URL, CL - Child list only for public URL(when particular product under Category only needed)

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['product_id'])) {
            $product_id = $this->_request['product_id'];
        }
        if (isset($this->_request['page_from']) && !empty($this->_request['page_from'])) {
            $page_from = $this->_request['page_from'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            if($reg_type_user!='U'){
                $this->verifyStudioPOSToken($company_id, $pos_email, $token);
            }
            $this->getRetailProductDetails($company_id, $product_id, $page_from);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function retailVariants(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $product_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['product_id'])) {
            $product_id = $this->_request['product_id'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($product_id)) {
            $this->getRetailVariantDetails($company_id, $product_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function publiccompanydetails(){
        // ERROR WHEN THE METHOD IS NOT - GET
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = $studio_code = $token = $pos_email = '';
        $reg_type_user = 'U';
        $page_from = 'R';
        
        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studio_code'])) {
            $studio_code = $this->_request['studio_code'];
        }
        if (isset($this->_request['token'])) {
            $token = $this->_request['token'];
        }
        if (isset($this->_request['pos_email'])) {
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['page_from'])) {
            $page_from = $this->_request['page_from'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty($studio_code)) {
            if($reg_type_user!='U'){
                $this->verifyStudioPOSToken($company_id, $pos_email, $token);
            }
            $this->getpubliccompanydetails($company_id, $studio_code, $reg_type_user, $page_from);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getwepaystatus(){
        if($this->get_request_method() != "GET"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error),406);
        }
        
        $company_id='';
        if(isset($this->_request['companyid'])){
            $company_id = $this->_request['companyid'];
        }
        
        if(!empty($company_id)){
            $this->wepayStatus($company_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function getstripestatus() {
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->stripeStatus($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    
    private function retailDiscountCodeVerication(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        // INITIALIZATION OF VARIABLES
        $company_id = $discount_code = '';
        $cart_details = [];
        $callBack = $total_order_cost = 0;

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['cart_details'])) {
            $cart_details = $this->_request['cart_details'];
        }
//        $cart_details = array(
//            array('retail_product_id'=>'2','retail_variant_id'=>'1','quantity'=>3)
//        );
//        
        
        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id) && !empty(trim($discount_code)) && !empty($cart_details)) {
            $this->applyRetailDiscountForProducts($company_id, trim($discount_code), $cart_details, $callBack);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function checkQuantityOrDiscountOnCheckout(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id =$discount_code='';
        $cart_details=[];
        $checkout_flag = 'Y';
        $optional_discount_flag = 'N';
        $optional_discount_amount = 0;
        $payment_method = 'CC';
        
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['cart_details'])) {
            $cart_details = $this->_request['cart_details'];
        }
        if (isset($this->_request['checkout_flag'])) {
            $checkout_flag = $this->_request['checkout_flag'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount_amount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        
        if (!empty($company_id)  && (!is_array($cart_details)||is_array($cart_details) && !empty($cart_details))) {
            $this->checkRetailQuantityDuringCheckout($company_id,$discount_code,$cart_details,$checkout_flag,0, $optional_discount_flag, $optional_discount_amount, $payment_method);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function wepayretailCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id  = $optional_discount_flag =  $buyer_first_name =$discount_code= $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $cc_id = $cc_state = $buyer_street=$buyer_city=$buyer_state=$buyer_zip= '';
        $student_id  = $optional_discount =   0;
        $check_number = 0;
        $cart_details = [];
        $country = 'US';
        $reg_type_user = 'U';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $pos_email = $token = '';
        $payment_method = 'CC';

        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['cart_details'])) {
            $cart_details = $this->_request['cart_details'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['buyer_street'])){
            $buyer_street = $this->_request['buyer_street'];
        }
        if(isset($this->_request['buyer_city'])){
            $buyer_city = $this->_request['buyer_city'];
        }
        if(isset($this->_request['buyer_state'])){
            $buyer_state = $this->_request['buyer_state'];
        }
        if(isset($this->_request['buyer_zip'])){
            $buyer_zip = $this->_request['buyer_zip'];
        }       
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        
        if($reg_type_user !== 'U' && $reg_type_user !== 'M'){
            $this->checkpossettings($company_id,$payment_method,'R', $pos_email, $token);   //R - retail, M - Miscellaneous
        }
        
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && (is_array($cart_details) && !empty($cart_details))) {
            $this->wepayCreateretailCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$company_id, $student_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code,$buyer_street,$buyer_city,$buyer_state,$buyer_zip, $country, $cc_id, $cc_state, $discount_code, $cart_details, $reg_type_user, $reg_version_user);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    public function sendRetailOrderReceipt(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['retail_order_id'])) {
            $retail_order_id = $this->_request['retail_order_id'];
        }
        if(!empty($company_id)&&!empty($retail_order_id)){
            $this->sendOrderReceiptForRetailPayment($company_id,$retail_order_id,1);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function sendExpiryEmailToStudio(){
        // ERROR WHEN THE METHOD IS NOT - POST
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        // INITIALIZATION OF VARIABLES
        $company_id = '';

        // REQUESTING THE PARAMETERS
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        

        // CHECKING EMPTY ON REQUESTED PARAMETERS
        if (!empty($company_id)) {
            $this->sendExpiryEmailDetailsToStudio($company_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }    

    private function getstudent() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() == "GET") {
           $company_id=  '';
           
            if (isset($this->_request['companyid'])) {
                $company_id = $this->_request['companyid'];
            }

            if (!empty($company_id)) {
                    $this->getStudentPaymentInfoDetails($company_id, 0);
            } else {
                $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
                $this->response($this->json($error), 200);
            }
        } else {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
    }
    
    private function wepayMiscCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $student_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $order_note_details = $payment_method = $payment_amount = $credit_card_id = $credit_card_status = $credit_card_name = $credit_card_expiration_month = $credit_card_expiration_year= $check_number=  $created_dt=  "";
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $pos_email = $token = '';
        $reg_type_user = 'U';
       
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['optional_note'])) {
            $order_note_details = $this->_request['optional_note'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['cc_id'])) {
            $cc_id = $this->_request['cc_id'];
        }
        if (isset($this->_request['cc_state'])) {
            $cc_state = $this->_request['cc_state'];
        }
        if (isset($this->_request['check_number'])) {
            $check_number = $this->_request['check_number'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id,$payment_method,'M', $pos_email, $token);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        if (!empty($buyer_first_name) && !empty($buyer_last_name) &&  !empty($buyer_email) && !empty($payment_method) && !empty($payment_amount)
                && (($payment_method=="CC" && !empty($cc_id) && !empty($cc_state)) || ($payment_method=="CH" && !empty($check_number)) || $payment_method=="CA")){
                 $this->wepayCheckoutForMiscellaneousINDB($company_id,$student_id,$buyer_first_name,$buyer_last_name,$buyer_email,$buyer_phone,$order_note_details,$reg_type_user,$payment_method,$payment_amount,$cc_id,$cc_state,$check_number);
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
        }
    }
    
    private function stripeMiscCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        
        $company_id = $student_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $order_note_details = $payment_method = $payment_amount = $payment_method_id = $check_number=  "";
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $pos_email = $token = '';
        $reg_type_user = 'U';
        $intent_method_type = $verification_status = $payment_intent_id = $cc_type = $stripe_customer_id = '';
       
        if (isset($this->_request['companyid'])) {
            $company_id = $this->_request['companyid'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['optional_note'])) {
            $order_note_details = $this->_request['optional_note'];
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['payment_amount'])) {
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['check_number'])) {
            $check_number = $this->_request['check_number'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['verification_status'])) {
            $verification_status = $this->_request['verification_status'];
        }
        if (isset($this->_request['payment_intent_id'])) {
            $payment_intent_id = $this->_request['payment_intent_id'];
        }
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if($reg_type_user !== 'U'){
            $this->checkpossettings($company_id,$payment_method,'M', $pos_email, $token);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        if (!empty($buyer_first_name) && !empty($buyer_last_name) &&  !empty($buyer_email) && !empty($payment_method) && !empty($payment_amount)
                && (($payment_method=="CC" && !empty($payment_method_id) && !empty($cc_type)) || ($payment_method=="CH" && !empty($check_number)) || $payment_method=="CA" || $payment_method=="ACH")){
                 $this->stripeCheckoutForMiscellaneousINDB($company_id,$student_id,$buyer_first_name,$buyer_last_name,$buyer_email,$buyer_phone,$order_note_details,$reg_type_user,$payment_method,$payment_amount,$payment_method_id,$check_number,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id);
            }else{
                $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
                $this->response($this->json($error), 200);
        }
    }
    //leads pos
    private function getleadssettingsforpos() {
        // Cross validation if the request method is GET else it will return "Not Acceptable" status
        if ($this->get_request_method() != "GET") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = '';

        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }

        if (!empty($company_id)) {
            $this->getleadsProgramDetailsforpos($company_id,0);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
    }
    
    private function addLeadsMemberFromPOS() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }

        $company_id = $buyer_first_name = $buyer_last_name = $buyer_email = $buyer_phone = $p_firstname = $p_lastname = '';
        $include_campaign_email = '';
        $leads_status = 'A';
        $reg_type_user = 'U';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = $version;
        $reg_entry_type = 'I';
        $pos_email = $token = '';
        
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['pi_id'])) {
            $program_id = $this->_request['pi_id'];
        }
        if (isset($this->_request['ls_id'])) {
            $source_id = $this->_request['ls_id'];
        }
         if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
         if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if(empty($program_id)){
             $program_id = 1;
        }
        if(empty($source_id)){
             $source_id = 1;
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        
        $this->verifyStudioPOSToken($company_id, $pos_email, $token);
       
        if (!empty($company_id) && (!empty($buyer_first_name) || !empty($buyer_last_name)) && (!empty($buyer_email) || !empty($buyer_phone))) {
            $this->addIndividualLeadsMemberFORPOSINDB($company_id, $program_id, $source_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $p_firstname, $p_lastname, $leads_status, $include_campaign_email, $reg_type_user, $reg_version_user, $reg_entry_type);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 200);
        }
        
    }
    
    private function setupIntent(){
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id = $buyer_email = $student_id = $payment_method_id = $actual_student_id = $stripe_customer_id = '';
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['buyer_email'])) {
            $buyer_email = $this->_request['buyer_email'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['student_id'])) {
            $actual_student_id = $this->_request['student_id'];
        }
        if (isset($this->_request['buyer_first_name'])) {
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if (isset($this->_request['buyer_last_name'])) {
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if (isset($this->_request['stripe_customer_id'])) {
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if (!empty($company_id) && !empty($payment_method_id) && !empty($buyer_email) && !empty($buyer_first_name) && !empty($buyer_last_name)) {
            $this->setupIntentCreation($company_id, $actual_student_id, $payment_method_id, $buyer_email, $buyer_first_name, $buyer_last_name, $stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Paramaters.");
            $this->response($this->json($error), 400);
        }
    }
    
    public function striperetailCheckout() {
        if ($this->get_request_method() != "POST") {
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            $this->response($this->json($error), 406);
        }
        $company_id  = $optional_discount_flag =  $buyer_first_name =$discount_code= $buyer_last_name = $buyer_email = $buyer_phone = $buyer_postal_code = $payment_method_id = $buyer_street=$buyer_city=$buyer_state=$buyer_zip= '';
        $student_id  = $optional_discount =   0;
        $check_number = 0;
        $cart_details = [];
        $country = 'US';
        $reg_type_user = 'U';
        $temp = explode('/', $_SERVER['PHP_SELF']);
        $version = $temp[1];
        $reg_version_user = '';
        $version_number = $build_number = '';
        $pos_email = $token = $payment_amount = '';
        $payment_method = 'CC';
        $device_type = $app_id = $push_device_id = '';
        $intent_method_type = $verification_status = $payment_intent_id = $cc_type = $stripe_customer_id = $registration_date = $default_cc = '';
        
        if (isset($this->_request['push_device_id'])) {
            $push_device_id = $this->_request['push_device_id'];
        }
        if (isset($this->_request['device_type'])) {
            $device_type = $this->_request['device_type'];
        }
        if (isset($this->_request['app_id'])) {
            $app_id = $this->_request['app_id'];
        }
        if (isset($this->_request['version_number'])) {
            $version_number = $this->_request['version_number'];
        }
        if (isset($this->_request['build_number'])) {
            $build_number = $this->_request['build_number'];
        }
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['studentid'])) {
            $student_id = $this->_request['studentid'];
        }
        if (isset($this->_request['cart_details'])) {
            $cart_details = $this->_request['cart_details'];
        }
        if(isset($this->_request['buyer_first_name'])){
            $buyer_first_name = $this->_request['buyer_first_name'];
        }
        if(isset($this->_request['buyer_last_name'])){
            $buyer_last_name = $this->_request['buyer_last_name'];
        }
        if(isset($this->_request['buyer_street'])){
            $buyer_street = $this->_request['buyer_street'];
        }
        if(isset($this->_request['buyer_city'])){
            $buyer_city = $this->_request['buyer_city'];
        }
        if(isset($this->_request['buyer_state'])){
            $buyer_state = $this->_request['buyer_state'];
        }
        if(isset($this->_request['buyer_zip'])){
            $buyer_zip = $this->_request['buyer_zip'];
        }       
        if (isset($this->_request['email'])) {
            $buyer_email = $this->_request['email'];
        }
        if (isset($this->_request['phone'])) {
            $buyer_phone = $this->_request['phone'];
        }
        if (isset($this->_request['postal_code'])) {
            $buyer_postal_code = $this->_request['postal_code'];
        }
        if (isset($this->_request['country'])) {
            $country = $this->_request['country'];
        }
        if (isset($this->_request['payment_method_id'])) {
            $payment_method_id = $this->_request['payment_method_id'];
        }
        if (isset($this->_request['discount_code'])) {
            $discount_code = $this->_request['discount_code'];
        }
        if (isset($this->_request['reg_type_user'])) {
            $reg_type_user = $this->_request['reg_type_user'];
        }
        if($reg_type_user!='U'){
            $reg_version_user = "mob_ver-" . $version . ", ver_num-" . $version_number . ", build_num-" . $build_number;
        }
        if (isset($this->_request['payment_method'])) {
            $payment_method = $this->_request['payment_method'];
        }
        if (isset($this->_request['check_number'])) {
            if(!empty($this->_request['check_number'])){
            $check_number = $this->_request['check_number'];
            }
        }
        if (isset($this->_request['optional_discount'])) {
            if(!empty($this->_request['optional_discount'])){
            $optional_discount = $this->_request['optional_discount'];
            }
        }
        if (isset($this->_request['optional_discount_flag'])) {
            $optional_discount_flag = $this->_request['optional_discount_flag'];
        }
        if(isset($this->_request['token'])){
            $token = $this->_request['token'];
        }
        if(isset($this->_request['pos_email'])){
            $pos_email = $this->_request['pos_email'];
        }
        if(isset($this->_request['payment_amount'])){
            $payment_amount = $this->_request['payment_amount'];
        }
        if (isset($this->_request['intent_method_type'])) {
            $intent_method_type = $this->_request['intent_method_type'];
        }
        if (isset($this->_request['payment_intent_id'])) {
            $payment_intent_id = $this->_request['payment_intent_id'];
        }  
        if (isset($this->_request['cc_type'])) {    // cc_type - N - New, E - Existing
            $cc_type = $this->_request['cc_type'];
        }
        if (isset($this->_request['stripe_customer_id'])) {    // cc_type - N - New, E - Existing
            $stripe_customer_id = $this->_request['stripe_customer_id'];
        }
        
        if($reg_type_user !== 'U' && $reg_type_user !== 'M'){
            $this->checkpossettings($company_id,$payment_method,'R', $pos_email, $token);   //R - retail, M - Miscellaneous
        }
        
        if($reg_type_user === "PP" && (((int)$optional_discount > 0) || $optional_discount_flag === "Y")){
            $error = array('status' => "Failed", "msg" => "Optional discount is not applicable");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CH" && empty(trim($check_number))){
            $error = array('status' => "Failed", "msg" => "Please provide check number");
            $this->response($this->json($error), 200);
        }
        if($payment_method === "CC" && empty($payment_method_id) && ($payment_amount > 0 ) && !empty($cc_type) ){
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
        if(empty($reg_type_user) || ($reg_type_user === 'PP' && !empty(trim($student_id)))){
            $error = array('status' => "Failed", "msg" => "Authentication Error");
            $this->response($this->json($error), 200);
        }
        if (!empty($company_id) && !empty($buyer_first_name) && !empty($buyer_last_name) && !empty($buyer_email) && (is_array($cart_details) && !empty($cart_details))) {
            $this->stripeCreateretailCheckout($optional_discount_flag,$optional_discount,$check_number,$payment_method,$company_id, $student_id, $buyer_first_name, $buyer_last_name, $buyer_email, $buyer_phone, $buyer_postal_code,$buyer_street,$buyer_city,$buyer_state,$buyer_zip, $country, $payment_method_id, $discount_code, $cart_details, $reg_type_user, $reg_version_user,$payment_amount,$intent_method_type,$verification_status,$payment_intent_id,$cc_type,$stripe_customer_id);
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            $this->response($this->json($error), 200);
        }
    }
    
     
}

// Initiiate Library
$api = new openApi;
$api->processApi();
?>