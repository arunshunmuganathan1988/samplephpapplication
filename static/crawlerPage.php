<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
$ms_url = 'http://dev.mystudio.academy/';
if(strpos( $_SERVER['HTTP_HOST'], 'dev.mystudio.academy')){  //Development
    $ms_url = 'http://dev.mystudio.academy/';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'dev2.mystudio.academy')){  //Development
    $ms_url = 'http://dev2.mystudio.academy/';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'stage.mystudio.academy')){  //Stage
    $ms_url = 'http://stage.mystudio.academy/';
}elseif(strpos( $_SERVER['HTTP_HOST'], 'mystudio.academy')){ //Production
    $ms_url = 'https://www.mystudio.academy/';
}

//echo json_encode($_REQUEST)."<br>";
//echo json_encode($_SERVER)."<br>";
include_once '../e/Api/EventModel.php';
$model = new EventModel();
//log_info("Ser : ".json_encode($_SERVER)."Req : ". json_encode($_REQUEST));
if(isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '?=') !== false){
    $req_url = explode("?=", stripslashes($_SERVER['REQUEST_URI']));
}elseif(isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '&=') !== false){
    $req_url = explode("&=", stripslashes($_SERVER['REQUEST_URI']));
}
if(!empty($req_url[1]) && count($req_url)>1){
    $url_params = explode("/", $req_url[1]);    
}
$company_id = $event_id = '';
//if(isset($url_params[1]) && !empty($url_params[1])){
//    $company_id = $url_params[1];
////    echo '<br> company_id:'.$company_id;
//}
//if(isset($url_params[3]) && !empty($url_params[3])){
//    $event_id = $url_params[3];
////    echo '<br> event_id:'.$event_id;
//}
//if(isset($_REQUEST['cid']) && !empty($_REQUEST['cid'])){
//    $company_id = $_REQUEST['cid'];
////    echo '<br> company_id:'.$company_id;
//}
//if(isset($_REQUEST['eid']) && !empty($_REQUEST['eid'])){
//    $event_id = $_REQUEST['eid'];
////    echo '<br> event_id:'.$event_id;
//}

//$req_url = stripslashes($_REQUEST['run']);
//if(!empty($req_url)){
//    $url_params = explode("/", str_replace('?=','',$req_url));
//}
if(isset($url_params[1]) && !empty($url_params[1])){
    $company_id = $url_params[1];
    //log_info("C-id : ".$company_id);
//    echo '<br> company_id:'.$company_id;
}
if(isset($url_params[3]) && !empty($url_params[3])){
    $event_id = $url_params[3];
//    echo '<br> event_id:'.$event_id;
}else{
    if(isset($url_params[2]) && !empty($url_params[2]) && is_numeric($url_params[2])){
        $event_id = $url_params[2];
    //    echo '<br> event_id:'.$event_id;
    }
}
//log_info("E-id : ".$company_id);
if(empty($company_id)){
    $data = array("status"=>"Failed", "code"=>404, "msg"=>"File Not Found.", "title"=>"404 Error");
    makeFailedPage($data,$ms_url);
    exit();
}
if(empty($event_id)){
    $data = $model->getEventsDetailByCompanyForSocialMedia($company_id);
}else{
    $data = $model->getEventsDetailByEventIdForSocialMedia($company_id,$event_id);
}
if($data['status']=='Failed'){
    makeFailedPage($data, $ms_url);
}else{
    makeSuccessPage($data);
}

function makeFailedPage($data, $ms_url) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta name="twitter:card" content="summary" />
            <meta property="og:title" content="<?php echo $data['title']; ?>" />
            <meta property="og:description" content="<?php echo $data['msg']; ?>" />
            <meta property="og:image" content=<?php echo $ms_url."static/error-404.png"; ?> />
            <!-- etc. -->
        </head>
        <body>
            <!--Jim Test <br>-->
            <?php // echo json_encode($_REQUEST)."<br>".json_encode($_SERVER)."<br>"; ?>
            <p><?php echo $data['msg']; ?></p>
            <img src="error-404.png"/>
        </body>
    </html>
    <?php
}

function makeSuccessPage($data) {
    $title = $data['msg']['event_title'];
    $deschtml = str_replace("<br>"," ",nl2br($data['msg']['event_desc']));
    $desc = substr(strip_tags($deschtml),0,100);
    $image = $data['msg']['event_banner_img_url'];
    if(!empty($data['msg']['logo_URL'])){
        $logo = $data['msg']['logo_URL'];
    }else{
        $logo = $data['msg']['event_banner_img_url'];
    }
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta name="twitter:card" content="summary" />
            <meta property="og:title" content="<?php echo $title; ?>" />
            <meta property="og:description" content="<?php echo $desc; ?>" />
            <meta property="og:image" content="<?php echo $image; ?>" />
            <meta property="og:image:width" content="250" />
            <meta property="og:image:height" content="125" />
            <!-- etc. -->
        </head>
        <body>
            <p><?php echo $desc; ?></p>            
        </body>
    </html>
    <?php
}
?>