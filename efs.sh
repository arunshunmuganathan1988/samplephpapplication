#!/bin/bash
file_system_dns=fs-9b55e21b.efs.us-east-1.amazonaws.com
efs_directory=/var/www/html
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $file_system_dns:/ $efs_directory

