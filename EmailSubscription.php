
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email Subscription</title>
        <link href="custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body onload="confirmation()" style="background: #f7f7f7">
        
    <center>
        <br><br><br><br>
        <div id="selctemailview">
            <label style="font-size: 25px;color: #808080;font-weight: 600;" >Please select email!</label><br><br>
            <select id="selectemail" onchange="catchemail()" style="width: 380px;height: 41px;">
            </select>
            <br><br>
            <button id="confirmbutton" style="background: #00ad68;height: 27px;border: 1px solid #00ad68;border-radius: 5px;color: white" onclick="changestatus()"></button>
        </div>
        <div id='unsubscription' style='display: none'>
        <label id="mail1" style="font-size: 25px;color: #808080;font-weight: 600;" ></label><br><br>
        <label  style="font-size: 40px;color: #c74b3e;font-weight: 600;" >Opt Out Successful</label><br><br>
        <label  style="font-size: 24px;color: red;font-weight: 600;" >You'll no longer receive email communication from this sender.</label>
        </div>
        <div id='subscription' style='display: none'>
        <label id="mail2"  style="font-size: 25px;color: #808080;font-weight: 600;" ></label><br><br>
        <label  style="font-size: 40px;color: #019a61;font-weight: 600;" >Success</label><br><br>
        <label  style="font-size: 24px;color: #019a61;font-weight: 600;" >You are now subscribed to this email list.</label>
        </div>
    </center>
    <script type="text/javascript">
        var subscription_path = "<?php
        $file_from = "EMAIL";
        include_once 'mystudioapp.php';
        echo $full_url;
        ?>";
        function unsubscribeEmail(jsonparams)
        {       
            var xhttp = new XMLHttpRequest();
            parameters = JSON.stringify(jsonparams);
            xhttp.onreadystatechange = function() {
                var status,type;
                if(this.readyState == 4){
                    var response_fromserver = JSON.parse(this.responseText);
                    status = response_fromserver.status;
                    type = response_fromserver.type;
                    if (type === 'unsubscribed' && status == "Success") {
                        document.getElementById("selctemailview").style.display = 'none';
                        document.getElementById("unsubscription").style.display = 'block';
                      }else if(type === 'subscribed' && status == "Success"){
                        document.getElementById("selctemailview").style.display = 'none';
                          document.getElementById("subscription").style.display = 'block';
                      }
                }
            };
            
//            if (window.location.hostname === 'mystudio.academy' || window.location.hostname === 'www.mystudio.academy'){
//                if (window.location.hostname.indexOf("www") < 0 || window.location.protocol !== 'https:') {
//                    var www_url = window.location.hostname;
//                    var pathname = window.location.pathname;
//                    var newpath = (pathname.split('/'))[1];
//                    pathname = '/'+newpath;
//                    if (window.location.hostname.indexOf("www") > - 1){
//                        subscription_path = 'https://' + www_url + pathname;
//                    } else{
//                        subscription_path = 'https://www.' + www_url + pathname;
//                    }
//                }else{
//                    var www_url = window.location.origin;
//                    var pathname = window.location.pathname;
//                    var newpath = (pathname.split('/'))[1];
//                    pathname = '/'+newpath;
//                    var subscription_path = ''; 
//                    subscription_path = www_url + pathname;
//                }
//            }else if (window.location.hostname === 'stage.mystudio.academy' || window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
//                if (window.location.hostname.indexOf("www") < 0 || window.location.protocol !== 'http:') {
//                    var www_url = window.location.hostname;
//                    var pathname = window.location.pathname;
//                    var newpath = (pathname.split('/'))[1];
//                    pathname = '/'+newpath;
//                    var subscription_path = ''; 
//                        subscription_path = 'http://' + www_url + pathname;
//                    
//                }else{
//                    var www_url = window.location.origin;
//                    var pathname = window.location.pathname;
//                    var newpath = (pathname.split('/'))[1];
//                    pathname = '/'+newpath;
//                    var subscription_path = ''; 
//                    subscription_path = www_url + pathname;
//                }
//            }else{
//                var subscription_path = ''; 
//                subscription_path = "http://localhost/mystudio.mystudiowebapp/vx";
//            }
            xhttp.open("POST", subscription_path+"/Api/PortalApi/changeEmailSubscriptionStatus", true);
            xhttp.setRequestHeader("Content-Type", 'application/json; charset=utf-8','Access-Control-Allow-Origin', '*');
            xhttp.send(parameters);
        }
        function confirmation() {
        <?php    
        $url =  $_SERVER['REQUEST_URI'];
        $after_url = explode("mail=",$url);
        $encoded_mail_ids = explode("?comp_id=",$after_url[1]);
        $decoded_mails = base64_decode($encoded_mail_ids[0]);
        ?>
                var decoded_emails = "<?php echo $decoded_mails ?>"; 
                var decoded_emails_array = decoded_emails.split(",");
                console.log(decoded_emails_array);
                var selecttag = document.getElementById("selectemail");
                for (var j = 0; j < decoded_emails_array.length; j++) {
                        var option = document.createElement("option");
                        option.text = decoded_emails_array[j];
                        selecttag.add(option);
                    }
//            var splitat = email.split("@");
//            var before_at = splitat[0];
//            var after_at = splitat[1];
//            var split_dot = after_at.split(".");
//            var before_dot = split_dot[0]; 
//            var after_dot = split_dot[1]; 
//            var email_view = before_at[0] + '*****' + before_at[before_at.length-1] + '@' +
//                                             before_dot[0]+ '*****' + before_dot[before_dot.length-1] +'.'+after_dot;
            var full_url,substring,email,confiramtion_text;
            full_url = window.location.href;
            substring =  full_url.split("mail=");
            var emailwithextra = substring[1].split("?comp_id=");
            email = emailwithextra[0];
            substring = emailwithextra[1].split("?type=");
            if(substring[1] === "S"){
                document.getElementById("confirmbutton").innerHTML = "Subscribe";
            }else{
                document.getElementById("confirmbutton").innerHTML = "Unsubscribe";
            }
        }
        function changestatus(){   
            var selected_email = document.getElementById("selectemail").value;
            document.getElementById("mail1").innerHTML =  selected_email; 
            document.getElementById("mail2").innerHTML =  selected_email; 
            var full_url,substring,email,confiramtion_text;
            full_url = window.location.href;
            substring =  full_url.split("mail=");
            var emailwithextra = substring[1].split("?comp_id=");
            email = emailwithextra[0];
            substring = emailwithextra[1].split("?type=");
            var emails = document.getElementById("selectemail").value;
            var params = {email:emails,company_id:substring[0],status:substring[1]};
            unsubscribeEmail(params);
        }
    </script>
</body>
</html> 