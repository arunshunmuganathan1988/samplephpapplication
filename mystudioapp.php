<?php
    
$ms_url=$ms_version=$full_url=$GLOBALS['MYSTUDIO_CURRENT_VERSION2']='';
$GLOBALS['MYSTUDIO_CURRENT_VERSION1'] = "v";

if( strpos($_SERVER['HTTP_HOST'], 'dev.mystudio.academy') !== false){  //Development
    $ms_url = 'http://dev.mystudio.academy/';
    $ms_version = 35;
}elseif( strpos($_SERVER['HTTP_HOST'], 'dev2.mystudio.academy') !== false){  //Development
    $ms_url = 'http://dev2.mystudio.academy/';
    $ms_version = 35;
}elseif( strpos($_SERVER['HTTP_HOST'], 'stage.mystudio.academy') !== false){  //Stage
    $ms_url = 'http://stage.mystudio.academy/';
    $ms_version = 35;
}elseif( strpos($_SERVER['HTTP_HOST'], 'beta.mystudio.academy') !== false){  //Stage
    $ms_url = 'http://beta.mystudio.academy/';
    $ms_version = 35;
}elseif( strpos($_SERVER['HTTP_HOST'], 'mystudio.academy') !== false){ //Production
    $ms_url = 'https://www.mystudio.academy/';
    $ms_version = 34;
}else{
    $ms_url = "http://localhost/mystudio.mystudiowebapp/";
    $ms_version = 'x';
}


if(isset($file_from) && $file_from=='EMAIL'){
    $full_url = $ms_url.'v'.$ms_version;
}if(isset($file_from) && $file_from=='VERSION_CHECK'){  //to check current version for Webapp
    $GLOBALS['MYSTUDIO_CURRENT_VERSION2'] = ($ms_version==='x' || $ms_version==='')?1:$ms_version;
}elseif(isset($_REQUEST['pg'])){
    if($_REQUEST['pg'] =='registration'){
        if(isset($_REQUEST['email'])){
            header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/registration?email='.$_REQUEST['email']);
        }else{
            header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/registration');
        }
    }else if($_REQUEST['pg'] =='billingstripe'){
        header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/billingstripe');
    }else if($_REQUEST['pg'] =='payment'){
        if(isset($_REQUEST['stripe_status'])){
            header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/payment?stripe_status='.$_REQUEST['stripe_status']);
        }else{
            header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/payment');
        }
    }else{
        header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/login');
    }
}else{
    header("Location: ".$ms_url.'v'.$ms_version.'/WebPortal/#/login');
}

    
?>