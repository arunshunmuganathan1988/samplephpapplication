<?php
//require_once '../aws/aws-autoloader.php';
require_once __DIR__.'/../aws/aws.phar';

use Aws\Sns\SnsClient;
class wepay {

    public $client_id = '';
    public $client_secret = '';
    public $access_token = '';
    public $pswd = '';
    public $wp_level = '';
    private $file_name = '';
    
    public function __construct() {
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'dev.mystudio.academy') !== false || strpos($host, 'dev2.mystudio.academy') !== false){
            $this->file_name = "../Globals/wp_dev.props";
            $this->url = "https://stage.wepayapi.com/v2/";
            $this->wp_level = 'S';
        }elseif (strpos($host, 'mystudio.academy') !== false) {
            $this->file_name = "../Globals/wp_prod.props";
            $this->url = "https://wepayapi.com/v2/";
            $this->wp_level = 'P';
        }else{
            $this->file_name = "../Globals/wp_dev.props";
            $this->url = "https://stage.wepayapi.com/v2/";
            $this->wp_level = 'S';
        }
        $this->getUserandSecret();        
    }
    
    private function getUserandSecret(){
        $file = explode( PHP_EOL, file_get_contents($this->file_name));
        $i=0;
        foreach( $file as $line ) {
            if($i<=3 && $line!=""){
                $api = explode("=", $line);
                if($i==0){
                    $this->client_id = $api[1];
                    $i++;
                }elseif($i==1){
                    $this->client_secret = $api[1];
                    $i++;
                }elseif($i==2){
                    $temp = '';
                    if(count($api)>2){
                        for($j=1;$j<count($api)-1;$j++){
                            $temp .= $api[$j]."=";
                        }
                        $this->access_token = $temp;
                    }else{
                        $this->access_token = $api[1];
                    }
                    $i++;
                }elseif($i==3){
                    $this->pswd = $api[1];
                    $i++;
                }
            }
        }
    }
    
    public function accessWepayApi($method,$request_type,$params,$token,$user_agent,$sns_msg){
      
        $sns_mss=$sns_msg["buyer_email"];
        $sns_sub=$sns_msg["membership_title"];
        $sns_sub_date=$sns_msg["gmt_date"];
        $call = $sns_msg["call"];
        $type = $sns_msg["type"];
        $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
        
        $temp_password = $this->pswd;
        $enc_method = 'aes-256-cbc';
        // Must be exact 32 chars (256 bit)
        $password = substr(hash('sha256', $temp_password, true), 0, 32);
        // IV must be exact 16 chars (128 bit)
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//        // encrypt
//        $encrypted = base64_encode(openssl_encrypt($plaintext, $method, $password, OPENSSL_RAW_DATA, $iv));

        // My secret message 1234
        $access_token = openssl_decrypt(base64_decode($this->access_token), $enc_method, $password, OPENSSL_RAW_DATA, $iv);
        $auth[] = "Accept: application/json";
        $auth[] = "Content-Type: application/json";
        if(empty($token)){
            $auth[] = "Authorization: BEARER $access_token";
        }else{
            $auth[] = "Authorization: BEARER $token";
        }
        $auth[] = "User-Agent: $user_agent";
        
        $url = $this->url.$method;
        if(strtoupper($request_type)=='GET'){
            $body='{}';
            $post_option = false;
            $url = $url.$params;
        }else{
            $body = $params;
            if(strtoupper($request_type)=='POST'){
                $post_option = true;
            }else{
                $post_option = false;
            }
        }
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "$url");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$request_type");
        curl_setopt($ch, CURLOPT_POST, $post_option);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        if($this->wp_level=='S'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }elseif($this->wp_level=='P'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);
        
        

        $result = curl_exec($ch);
            $resultArr = json_decode($result, true);
            $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            if ($returnCode >= 400 || $returnCode==0) {
                $subject = "Wepay call for $call during $type failed";
//                $subject = $sns_sub . " -transaction failed";
                $message = "$sns_sub transaction failed for this user " . $sns_mss . " on " . $sns_sub_date;

                $sns = SnsClient::factory([
                    'Key' => '{AKIAIGUWYNJGAHDPJGIA}',
                    'secret' => '{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}',
                    'region' => 'us-east-1',
                    'version' => 'latest'
                ]);
//                log_info("object cretaed");
                $result1 = $sns->publish(array(
                    'TopicArn' => $topicarn,
                    // Message is required
                    'Message' => $message,
                    'Subject' => $subject,
                    //'MessageStructure' => 'string',
                    'MessageAttributes' => array(
                        // Associative array of custom 'String' key names
                        'String' => array(
                            // DataType is required
    //            'DataType' => '',
                            'DataType' => 'String',
                            'StringValue' => '200',
                        ),
                    // ... repeated
                    ),
                ));
//            log_info("msg send");
        }
            


        if(isset($resultArr)&&!empty($resultArr)&&(!isset($resultArr['error'])) && ($returnCode<400 && $returnCode!=0)) {
            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result: " => $result);
            $this->log_info($this->json($succ_log));
            $res['status'] = "Success";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }else{
            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result : " => $result);
            $this->log_info($this->json($error_log));
            $res['status'] = "Failed";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }
        curl_close($ch);
        return $res;
    }
    
    public function accessWepayApiForBatchCalls($method,$request_type,$params,$user_agent,$sns_msg){
      
        $sns_sub=$sns_msg["title"];
        $sns_sub_date=$sns_msg["gmt_date"];
        $call = $sns_msg["call"];
        $type = $sns_msg["type"];
        $topicarn = 'arn:aws:sns:us-east-1:241334827363:mystudio';
        
//        $password = $this->pswd;
//        $enc_method = 'aes-256-cbc';
//        // Must be exact 32 chars (256 bit)
//        $password = substr(hash('sha256', $password, true), 0, 32);
//        // IV must be exact 16 chars (128 bit)
//        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
//        // encrypt
//        $encrypted = base64_encode(openssl_encrypt($plaintext, $method, $password, OPENSSL_RAW_DATA, $iv));

        // My secret message 1234
//        $access_token = openssl_decrypt(base64_decode($this->access_token), $enc_method, $password, OPENSSL_RAW_DATA, $iv);
        $auth[] = "Accept: application/json";
        $auth[] = "Content-Type: application/json";
//        if(empty($token)){
//            $auth[] = "Authorization: BEARER $access_token";
//        }else{
//            $auth[] = "Authorization: BEARER $token";
//        }
        $auth[] = "User-Agent: $user_agent";
        
        $url = $this->url.$method;
        if(strtoupper($request_type)=='GET'){
            $body='{}';
            $post_option = false;
            $url = $url.$params;
        }else{
            $body = $params;
            if(strtoupper($request_type)=='POST'){
                $post_option = true;
            }else{
                $post_option = false;
            }
        }
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "$url");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$request_type");
        curl_setopt($ch, CURLOPT_POST, $post_option);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        if($this->wp_level=='S'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }elseif($this->wp_level=='P'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $auth);
        
        

        $result = curl_exec($ch);
            $resultArr = json_decode($result, true);
            $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            if ($returnCode >= 400 || $returnCode==0) {
                $subject = "Wepay call for $call during $type failed";
//                $subject = $sns_sub . " -transaction failed";
                $message = "$sns_sub transaction failed on $sns_sub_date";

                $sns = SnsClient::factory([
                    'Key' => '{AKIAIGUWYNJGAHDPJGIA}',
                    'secret' => '{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}',
                    'region' => 'us-east-1',
                    'version' => 'latest'
                ]);
//                log_info("object cretaed");
                $result1 = $sns->publish(array(
                    'TopicArn' => $topicarn,
                    // Message is required
                    'Message' => $message,
                    'Subject' => $subject,
                    //'MessageStructure' => 'string',
                    'MessageAttributes' => array(
                        // Associative array of custom 'String' key names
                        'String' => array(
                            // DataType is required
    //            'DataType' => '',
                            'DataType' => 'String',
                            'StringValue' => '200',
                        ),
                    // ... repeated
                    ),
                ));
//            log_info("msg send");
        }
            


        if(isset($resultArr)&&!empty($resultArr)&& (!isset($resultArr['error'])) && ($returnCode<400 && $returnCode!=0)) {
            $succ_log = array("Curl : " => "Success($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result: " => $result);
            $this->log_info($this->json($succ_log));
            $res['status'] = "Success";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }else{
            $error_log = array("Curl error: " => curl_error($ch)."($returnCode)", "Method: " => $method, "Type: " => $request_type, "Url: " => $this->url, "Params: " => $params, "Result : " => $result);
            $this->log_info($this->json($error_log));
            $res['status'] = "Failed";
            $res['msg'] = $resultArr;
            $res['curl_status'] = $returnCode;
        }
        curl_close($ch);
        return $res;
    }
    
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function log_info($msg) {        
        $today = gmdate("d.m.Y");
        $filename = "../Log/Wepay/$today.txt";
        $fd = fopen($filename, "a");
        $str = "[" . gmdate("d/m/Y H:i:s") . "] " . $msg;
        fwrite($fd, $str . PHP_EOL . PHP_EOL);
        fclose($fd);
    }
    
}