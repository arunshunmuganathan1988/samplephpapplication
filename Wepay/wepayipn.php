<?php

require_once '../Globals/cont_log.php';
require_once 'WePay.php';
require_once('class.phpmailer.php');
require_once('class.smtp.php');

class wepayipn{
    
    public $data = "";
    private $db = NULL;
    public $_allow = array();
    public $_content_type = "application/json";
    public $_request = array();
    private $_code = 200;
    private $dbhost = '';
    private $dbusername = '';
    private $dbpassword = '';
    private $dbName = '';
    public $upload_folder_name = '';
    private $wp;
    private $wp_client_id = '', $wp_client_secret = '', $wp_pswd = '';
    private $user_agent = '';
    
    public function __construct(){
        require_once '../Globals/config.php';
        $this->wp = new wepay();        
        $this->inputs();
        $this->dbhost = $GLOBALS['dbserver'];
        $this->dbusername = $GLOBALS['dbuser'];
        $this->dbpassword = $GLOBALS['dbpassword'];
        $this->dbName = $GLOBALS['dbName'];
        $this->dbConnect();     // Initiate Database connection
        $this->upload_folder_name = $GLOBALS['uploads_folder'];
        $this->wp_client_id = $this->wp->client_id;
        $this->wp_client_secret = $this->wp->client_secret;
        $this->wp_pswd = $this->wp->pswd;
        $this->user_agent = $this->getUserAgent();
        date_default_timezone_set('UTC');
        }
    
    private function dbConnect() {
        $this->db = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbName);
        if (!$this->db) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "additional" => "Database Login failed! Please make sure that the DB login credentials provided are correct");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }
    }
    
    private function getUserAgent(){
        if (!function_exists('getallheaders')){
            if(!function_exists ('apache_request_headers')){
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }else{
                $headers = apache_request_headers();
                $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
            }
        }else{
            $headers = getallheaders();
            $user_agent = isset($headers['User-Agent'])?$headers['User-Agent']:(isset($headers['user-agent'])?$headers['user-agent']:'');
        }
        return $user_agent;
    }

    public function get_referer() {
        return $_SERVER['HTTP_REFERER'];
    }

    public function response($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
        exit;
    }

    public function responseWithMail($data, $status) {
        $this->_code = ($status) ? $status : 200;
        $this->set_headers();
        echo $data;
    }

    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$this->_code]) ? $status[$this->_code] : $status[500];
    }

    public function get_request_method() {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function inputs() {
        switch ($this->get_request_method()) {
            case "OPTIONS":
                $this->response('', 200);
                break;
            case "POST":
                $this->_request = $this->cleanInputs($_REQUEST);
                break;
            case "GET":
            case "DELETE":
                $this->_request = $this->cleanInputs($_GET);
                break;
            case "PUT":
                parse_str(file_get_contents("php://input"), $this->_request);
                $this->_request = $this->cleanInputs($this->_request);
                break;
            default:
                $this->response('', 406);
                break;
        }
    }

    private function cleanInputs($data) {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_code . " " . $this->get_status_message());
        header("Content-Type:" . $this->_content_type);
    }

    /*
     * 	Encode array into JSON
     */

    public function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
    
    public function processApi(){
        $func = strtolower(trim(str_replace("/","",$_REQUEST['run'])));
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $this->_request = json_decode(file_get_contents('php://input'), true);
 	}
        
        if((int)method_exists($this,$func) > 0){
            $this->$func();
        }else{
            $error = array("status" => "Failed", "msg" => "Page not found.");
            $this->response($this->json($error),404); // If the method not exist with in this class, response would be "Page not found".
        }
    }
    
    private function updateUser(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $user_id = '';
        
        if(isset($this->_request['user_id'])){
            $user_id = $this->_request['user_id'];
        }
        
        if(!empty($user_id)){
            $this->getUserDetails($user_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getUserDetails($user_id){
        $query = sprintf("SELECT `wp_account_id`, `company_id`, `wp_user_id`, `wp_user_state`, `access_token`, `first_name`, `last_name`, `user_name`, 
                `user_email` FROM `wp_account` WHERE `wp_user_id`='%s'",  mysqli_real_escape_string($this->db, $user_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $access_token = $row['access_token'];
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $response = $this->wp->accessWepayApi("user",'GET','',$token,$this->user_agent);
                if($response['status']=="Success"){
                    $res = $response['msg'];
                    $user_state = $res['state'];
                    
                    $update_query = sprintf("UPDATE `wp_account` SET `wp_user_state`='%s' WHERE `wp_user_id`='%s'", mysqli_real_escape_string($this->db, $user_state),
                            mysqli_real_escape_string($this->db, $user_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "User status updated successfully.");
                        ipn_log_info($this->json($error));
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "User doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 401);
            }
        }
    }
    
    private function updateAccount(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $account_id = '';
        
        if(isset($this->_request['account_id'])){
            $account_id = $this->_request['account_id'];
        }
        
        if(!empty($account_id)){
            $this->getAccountDetails($account_id);
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getAccountDetails($account_id){
        $query = sprintf("SELECT `wp_account_id`, `company_id`, `access_token`, `account_id`, `account_state`, `first_name`, `last_name`, `user_name`, 
                `user_email` FROM `wp_account` WHERE `account_id`='%s' limit 0,1",  mysqli_real_escape_string($this->db, $account_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $company_id = $row['company_id'];
                $wp_account_id = $row['wp_account_id'];
                $access_token = $row['access_token'];
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("account_id"=>"$account_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('account','POST',$postData,$token,$this->user_agent);
                ipn_log_info("wepay_get_account: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $account_state = $res['state'];
                    
                    $update_query = sprintf("UPDATE `wp_account` SET `account_state`='%s' WHERE `wp_account_id`='%s' and `account_id`='%s'", mysqli_real_escape_string($this->db, $account_state),
                            mysqli_real_escape_string($this->db, $wp_account_id), mysqli_real_escape_string($this->db, $account_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        if($account_state=="active" || $account_state=="action_required"){
                            $wp_status = 'Y';
                        }else{
                            $wp_status = 'N';
                        }
                        $update_company = sprintf("UPDATE `company` SET `wepay_status`='$wp_status' WHERE `company_id`='%s'", mysqli_real_escape_string($this->db,$company_id));
                        $update_company_result = mysqli_query($this->db, $update_company);
                        if(!$update_company_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_company");
                            ipn_log_info($this->json($error_log));
                        }
                        $error = array('status' => "Success", "msg" => "Account status updated successfully.");
                        ipn_log_info($this->json($error));
                        $this->response($this->json($error), 200);
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "Account details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    private function updateCheckout(){
        // Cross validation if the request method is POST else it will return "Not Acceptable" status
        if($this->get_request_method() != "POST"){
            $error = array("status" => "Failed", "code" => 406, "msg" => "Not Acceptable.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error),406);
        }
        
        $checkout_id = $for = '';
        
        if(isset($this->_request['checkout_id'])){
            $checkout_id = $this->_request['checkout_id'];
        }
        if(isset($_REQUEST['for'])){
            $for = $_REQUEST['for'];
        }
        
        if(!empty($checkout_id)){
            if(!empty($for) && $for == "membership"){
                $this->getMembershipCheckoutDetails($checkout_id);
            }else if(!empty($for) && $for == "trial"){
                $this->gettrialCheckoutDetails($checkout_id);
            }else if(!empty($for) && $for == "retail"){
                $this->getretailCheckoutDetails($checkout_id);
            }else{
                $this->getCheckoutDetails($checkout_id);
            }
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid Request Parameters.");
            ipn_log_info($this->json($error));
            $this->response($this->json($error), 400);
        }
    }
    
    private function getCheckoutDetails($checkout_id){
        $query = sprintf("SELECT er.`event_reg_id`, ep.`event_payment_id`, ep.`checkout_id`, ep.`checkout_status`, `access_token`, e.`event_title`, ep.`payment_amount`, ep.`processing_fee`, er.`processing_fee_type`, er.`event_id`, er.`payment_type`
                FROM `event_registration` er 
                LEFT JOIN `event_payment` ep ON er.`event_reg_id`=ep.`event_reg_id` 
                LEFT JOIN `wp_account` wp ON er.`company_id` = wp.`company_id` 
                LEFT JOIN `event` e ON e.`event_id` = er.`event_id`
                WHERE ep.`checkout_id` = '%s' ORDER BY ep.`event_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $event_reg_id = $row['event_reg_id'];
                $access_token = $row['access_token'];
                $event_id = $row['event_id'];
                $payment_id = $row['event_payment_id'];
                $payment_amount = $row['payment_amount'];
                $processing_fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_type = $row['payment_type'];
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $schedule_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $schedule_status = ", `schedule_status`='F'";
                        $send_email = 1;
                    }
                    $update_query = sprintf("UPDATE `event_payment` SET `checkout_status`='%s' $schedule_status WHERE `event_reg_id`='%s' and `event_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $event_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query", "ipn" => "1");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForEventPayment($event_reg_id,$checkout_state,0);
                        }else{
                            $this->response($this->json($error), 200);
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    protected function sendFailedOrderReceiptForEventPayment($event_reg_id, $checkout_state, $return_value){
       
        $waiver_policies = '';
        $query = sprintf("SELECT er.`company_id`, er.`event_id`, `event_title`, ep.`cc_name`, `waiver_policies`, concat(er.event_registration_column_2, ',', er.event_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `registration_date`, `registration_amount`, `payment_type`, 
            er.`payment_amount` total_due, er.`processing_fee_type`, er.`processing_fee` total_processing_fee, `paid_amount`, ep.`payment_amount`, ep.`processing_fee`, ep.`refunded_amount`, `schedule_date`, `schedule_status`, DATE(ep.`created_dt`) created_dt
            FROM `event_registration` er LEFT JOIN `event_payment` ep ON er.`event_reg_id` = ep.`event_reg_id` AND ep.`schedule_status` IN ('N', 'S', 'PR', 'M')
            LEFT JOIN `event` e ON e.`event_id` = er.`event_id` AND e.`company_id` =  er.`company_id`
            LEFT JOIN `company` c ON c.`company_id` =  er.`company_id`
            WHERE er.`event_reg_id` = '%s'", mysqli_real_escape_string($this->db, $event_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                
                $event_name = $participant_name = $buyer_name = $buyer_email = $cc_email_list = '';
                $inital_stage = $initial_down_pay = 0;
                $current_plan_details = $event_details = $bill_due_details = $payment_details = [];
                $total_due = $paid_due = $rem_due = 0;
                while($row = mysqli_fetch_assoc($result)){
                    $paid_amt = 0;
                    $event_id = $row['event_id'];
                    $event_name = $row['event_title'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $temp=[];
                    
                    if($row['processing_fee_type']==2){
                        $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                    }else{
                        $temp['amount'] = $row['payment_amount'];
                    }
//                    $temp['amount'] = $row['payment_amount'];
                    $temp['date'] = $row['schedule_date'];
                    $temp['created_dt'] = $row['created_dt'];
                    $temp['refunded_amount'] = $row['refunded_amount'];
                    $temp['edit_status'] = 'N';
                    $temp['schedule_status'] = $row['schedule_status'];
                    $temp['processing_fee'] = $row['processing_fee'];
                    $temp['credit_card_name'] = $row['cc_name'];
                    if($row['schedule_status']=='N'){
                        $temp['status'] = 'N';
                    }elseif($row['schedule_status']=='S'){
                        $temp['status'] = 'S';
                        $paid_amt = $temp['amount'];
                        $inital_stage++;
                    }elseif($row['schedule_status']=='PR' || $row['schedule_status']=='M'){
                        $paid_amt = $temp['amount']-$temp['refunded_amount'];
                    }
//                    $total_due = $current_plan_details['total_due'] = $row['total_due'];
//                    $paid_due = $current_plan_details['paid_due'] = $row['paid_amount'];
                    $total_due += $temp['amount'];
                    $current_plan_details['total_due'] = $total_due;
                    $paid_due += $paid_amt;
                    $current_plan_details['paid_due'] = $paid_due;
                    
                    $current_plan_details['success_count'] = $inital_stage;
                    $rem_due = $current_plan_details['rem_due'] = number_format((float)($total_due - $paid_due), 2, '.', '');
//                    $payment_details[] = $temp;
                    if ($row['schedule_status'] == 'N') {
                        $bill_due_details[] = $temp;
                    } elseif ($row['schedule_status'] == 'S') {
                        $payment_details[] = $temp;
                        if ($initial_down_pay > 0) {
                            $bill_due_details[] = $temp;
                        }
                        $initial_down_pay++;
                    } elseif ($row['schedule_status'] == 'M') {
                        $payment_details[] = $temp;
                    }
                }
                
                $studio_name = $studio_mail = $message = '';
                $query2 = sprintf("SELECT c.`company_name`, c.`email_id`, c.`referral_email_list`, e.`event_id`, `event_reg_details_id`, `event_title`, `event_begin_dt`, `event_end_dt`, erd.`event_cost`, `discount`, `payment_amount`, `quantity`, `payment_status`, `balance_due` 
                    FROM `event` e LEFT JOIN `event_reg_details` erd ON e.`event_id` = erd.`event_id` 
                    LEFT JOIN `company` c ON e.`company_id` =  c.`company_id`
                    WHERE e.`event_id` in (SELECT `event_id` FROM `event_reg_details` WHERE `event_id`='%s' OR `event_parent_id`='%s') AND `event_reg_id`='%s' AND `payment_status` NOT IN ('RF','CP') ORDER BY `event_begin_dt` ASC", 
                        mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_id), mysqli_real_escape_string($this->db, $event_reg_id));
                $result2 = mysqli_query($this->db, $query2);
                if(!$result2){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                    ipn_log_info($this->json($error_log));
                    if($return_value==1){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    return;
                }else{
                    $temp_quantity = $temp_char_count = 0;
                    while($row2 = mysqli_fetch_assoc($result2)){
                        $temp2 = [];
                        $temp2['title'] = $row2['event_title'];
                        if($temp2['title']>$temp_char_count){
                            $temp_char_count = strlen($temp2['title']);
                        }
                        
                        if (!empty(trim($row2['event_begin_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_begin_dt']) {
                                $start = " - ";
                            } else {
                                $start = $row2['event_begin_dt'];

                                $split_start = explode(" ", $start);
                                $start_date_value = $split_start[0];
                                $start_time_value = $split_start[1];
                                if ("00:00:00" == $start_time_value) {
                                    $start = date('M d, Y', strtotime($start));
                                } else {
                                    $start = date('M d, Y @ h:i A', strtotime($start));
                                }
                            }
                        }
                        
                        if (!empty(trim($row2['event_end_dt']))) {
                            if ("0000-00-00 00:00:00" == $row2['event_end_dt']) {
                                $end = " - ";
                            } else {
                                $end = $row2['event_end_dt'];

                                $split_end = explode(" ", $end);
                                $end_date_value = $split_end[0];
                                $end_time_value = $split_end[1];
                                if ("00:00:00" == $end_time_value) {
                                    $end = date('M d, Y', strtotime($end));
                                } else {
                                    $end = date('M d, Y @ h:i A', strtotime($end));
                                }
                            }
                        }

                        $temp2['start'] = $start;
                        $temp2['end'] = $end;
                        $temp2['event_cost'] = $row2['event_cost'];
                        $temp2['quantity'] = $row2['quantity'];
                        $temp2['event_disc'] = $row2['discount'];
                        $temp2['discounted_price'] = $row2['event_cost'] - $row2['discount'];
                        $temp_quantity += $row2['quantity'];
                        $temp2['payment_amount'] = $row2['payment_amount'];
                        $temp2['paid_due'] = $temp2['payment_amount'] - $row2['balance_due'];
                        $temp2['rem_due'] = $row2['balance_due'];
                        $temp2['state'] = 'N';
                        $event_details[] = $temp2;
                        $studio_name = $row2['company_name'];
                        $studio_mail = $row2['email_id'];
                        $cc_email_list = $row2['referral_email_list'];
                    }
                    $current_plan_details['total_event'] = $temp_quantity;
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                ipn_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }
        
        $failed_str = 'failed';
        if($checkout_state=='charged back'){
            $failed_str = 'charged back';
        }
        
        $subject = $event_name." - Payment $failed_str";
                
        $message .= "<b>The following payment for the Buyer $buyer_name $failed_str.</b><br><br>";
        $message .= "<b>Event Details</b><br><br>";
        for($i=0;$i<count($event_details);$i++){
            $message .= "Event: ".$event_details[$i]['title']."<br>Starts: ".$event_details[$i]['start']."<br>Ends: ".$event_details[$i]['end']."<br>Participant: ".$participant_name."<br><br>";
        }

        if($total_due>0){
            $message .= "<b>Billing Details</b><br><br>";
            if(count($bill_due_details)>0){
                $message .= "Total due: $".$total_due."<br>";
                $message .= "Balance due: $".$rem_due."<br>";                
                for($j=0;$j<count($bill_due_details);$j++){
                    $x= $j+1;
                    $amount = $bill_due_details[$j]['amount'];
                    $refunded_amount = $bill_due_details[$j]['refunded_amount'];
                    $final_amt = $amount-$refunded_amount;
                    $schedule_status = $bill_due_details[$j]['schedule_status'];
                    $date = date("M dS, Y", strtotime($bill_due_details[$j]['date']));
                    if($schedule_status=='N'){
                        $paid_str = "due";
                    }
                    $message .= "Bill ".$x.": $".$final_amt." ".$paid_str." ".$date."  <br>";
                }
                
                if(count($payment_details)>0){
                    $message.="<br><br><b>Payemt Details</b><br><br>";
                    $down_pay = $payment_details[0]['amount']-$payment_details[0]['refunded_amount']." paid on ".date("M dS, Y", strtotime($payment_details[0]['date']))."(".$payment_details[0]['credit_card_name'].")";
                    $message .= "Down Payment: $".$down_pay."<br>";
                    for($k=1;$k<count($payment_details);$k++){
                        $x= $k;
                        $amount = $payment_details[$k]['amount'];
                        $refunded_amount = $payment_details[$k]['refunded_amount'];
                        $final_amt = $amount-$refunded_amount;
                        $schedule_status = $payment_details[$k]['schedule_status'];
                        $date = date("M dS, Y", strtotime($payment_details[$k]['date']));
                        $paid_str = "paid on";
                        if($schedule_status=='S'){
                            $pay_type= "(".$payment_details[$k]['credit_card_name'].")";
                        }else{
                            $pay_type="(manual credit applied)";
                        }
                        $message .= "Payment ".$x.": $".$final_amt." ".$paid_str." ".$date." ".$pay_type." <br>";
                    }
                }
            }else{
                $amount = $payment_details[0]['amount'];
                $refunded_amount = $payment_details[0]['refunded_amount'];
                $schedule_status = $payment_details[0]['schedule_status'];
                $final_amount = $amount-$refunded_amount;
                $message .= "Total Due: $".$final_amount."<br>Amount Paid: $".$final_amount."(".$payment_details[0]['credit_card_name'].")<br>";
            }
            $message .= "<br><br>";
        }

        $sendEmail_status = $this->sendEmailForEvent('', $studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $studio_mail."   ".$sendEmail_status['mail_status']);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Event order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $studio_mail);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Event order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function sendEmailForEvent($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }
    
    private function getMembershipCheckoutDetails($checkout_id){
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_registration_id`, mr.`membership_category_title`, mr.`membership_title`, mp.`membership_payment_id`, mp.`checkout_id`, mp.`checkout_status`, `access_token`, mp.`payment_amount`, mp.`processing_fee`, mr.`processing_fee_type`
                    FROM `membership_registration` mr 
                    LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id`=mp.`membership_registration_id` 
                    LEFT JOIN `wp_account` wp ON mr.`company_id` = wp.`company_id` 
                    WHERE mp.`checkout_id` = '%s' ORDER BY mp.`membership_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $membership_reg_id = $row['membership_registration_id'];
                $access_token = $row['access_token'];
                $payment_id = $row['membership_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $membership_id = $row['membership_id'];
                $membership_option_id = $row['membership_option_id'];
                $membership_title = $desc = $row['membership_title'];
                $membership_category_title=$row['membership_category_title'];      
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    $update_query = sprintf("UPDATE `membership_payment` SET `checkout_status`='%s' $payment_status WHERE `membership_registration_id`='%s' and `membership_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $membership_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                            $update_query2 = sprintf("UPDATE `membership_registration` SET `preceding_payment_status`='F' WHERE `membership_registration_id`='%s'",  mysqli_real_escape_string($this->db, $membership_reg_id));
                            $update_result2 = mysqli_query($this->db, $update_query2);
                            if(!$update_result2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query2");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                            $this->responseWithMail($this->json($error), 200);
                            $this->sendFailedOrderReceiptForMembershipPayment($membership_reg_id,0);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            $this->addMembershipDimensions($company_id, $membership_id, $membership_option_id, $membership_category_title, $membership_title, '');
                            $this->updateMembershipDimensionsNetSales1($company_id, $membership_id, '');
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    protected function sendFailedOrderReceiptForMembershipPayment($membership_reg_id, $return_value){
       
        $membership_category_title = $membership_title = $studio_name = $studio_mail = $participant_name = $buyer_name = $buyer_email = $message = $cc_email_list = $wp_currency_code = $wp_currency_symbol = $waiver_policies = $payment_type = '';
        $payment_details = [];
        $paid_amt = $signup_fee = $signup_fee_disc = $mem_fee = $mem_fee_disc = $initial_payment = $inital_processing_fee = $recurring_amount = $recurring_processing_fee = 0;
        $recurring_start_date = $payment_start_date = $payment_frequency = $custom_recurring_frequency_period_type = $msg1 = $msg2 = $msg3 = '';
        $membership_structure = $billing_options = $billing_options_no_of_payments = '';
        
        $query = sprintf("SELECT mr.`company_id`, mr.`membership_id`, mr.`membership_option_id`, mr.`membership_category_title`, mr.`membership_title`, mr.`membership_structure`, mr.`signup_fee`, mr.`signup_fee_disc`, mr.`membership_fee`, mr.`membership_fee_disc`, mr.`initial_payment`, mp.`cc_name`, `waiver_policies`, mr.credit_card_name, 
            mr.`payment_start_date`, mr.`recurring_start_date`, mr.`next_payment_date`, mr.`payment_frequency`, mr.`custom_recurring_frequency_period_type`, concat(mr.membership_registration_column_2, ',', mr.membership_registration_column_1) participant_name, `buyer_name`, `buyer_email`, `buyer_phone`, mr.`payment_type`,
            `buyer_postal_code`, mr.`payment_amount` recurring_amount, mr.`processing_fee_type`, mr.`processing_fee` initial_processing_fee, `paid_amount`, membership_payment_id, mp.`payment_amount`, mp.`processing_fee`, mp.`payment_status`, mp.`paytime_type`, DATE(mp.`created_dt`) created_dt, mp.`payment_date`, mr.`prorate_first_payment_flg`, mr.`first_payment`, 
            mr.`billing_options`, mr.`no_of_classes`, mr.`billing_options_expiration_date`, mr.`billing_options_deposit_amount`, mr.`billing_options_no_of_payments`,
            c.`company_name`, c.`email_id`, c.`referral_email_list`, c.`wp_currency_code`, c.`wp_currency_symbol`
            FROM `membership_registration` mr LEFT JOIN `company` c ON c.`company_id`=mr.`company_id`
            LEFT JOIN `membership` mo ON mo.`company_id`=mr.`company_id` AND mr.`membership_id`=mo.`membership_id`
            LEFT JOIN `membership_payment` mp ON mr.`membership_registration_id` = mp.`membership_registration_id` AND mp.`payment_status` IN ('S','N','F')
            WHERE mr.`membership_registration_id` = '%s' ORDER BY membership_payment_id", mysqli_real_escape_string($this->db, $membership_reg_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }
            return;
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){                
                while($row = mysqli_fetch_assoc($result)){
                    $studio_name = $row['company_name'];
                    $studio_mail = $row['email_id'];
                    $cc_email_list = $row['referral_email_list'];
                    $wp_currency_code = $row['wp_currency_code'];
                    $wp_currency_symbol = $row['wp_currency_symbol'];
//                    $company_id = $row['company_id'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
                    $payment_type = $row['payment_type'];
                    $membership_category_title = $row['membership_category_title'];
                    $membership_title = $row['membership_title'];
                    $membership_structure = $row['membership_structure'];
                    $participant_name = $row['participant_name'];
                    $buyer_name = $row['buyer_name'];
                    $buyer_email = $row['buyer_email'];
                    $waiver_policies = $row['waiver_policies'];
                    $signup_fee = $row['signup_fee'];
                    $signup_fee_disc = $row['signup_fee_disc'];
                    $mem_fee = $row['membership_fee'];
                    $mem_fee_disc = $row['membership_fee_disc'];
                    $initial_payment = $row['initial_payment'];
                    $inital_processing_fee = $row['initial_processing_fee'];
                    $payment_start_date = $row['payment_start_date'];
                    $recurring_start_date = $row['recurring_start_date'];
                    $next_payment_date = $row['next_payment_date'];
                    $payment_frequency = $row['payment_frequency'];
                    $custom_recurring_frequency_period_type = $row['custom_recurring_frequency_period_type'];
                    $recurring_amount = $row['recurring_amount'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $prorate_first_payment_flg = $row['prorate_first_payment_flg'];
                    $first_payment = $row['first_payment'];
                    $billing_options = $row['billing_options'];
                    $no_of_classes = $row['no_of_classes'];
                    $billing_options_expiration_date = $row['billing_options_expiration_date'];
                    $billing_options_deposit_amount = $row['billing_options_deposit_amount'];
                    $billing_options_no_of_payments = $row['billing_options_no_of_payments'];
                    $credit_card_name = $row['credit_card_name'];
                    $temp=[];
                    if(!empty($row['membership_payment_id'])){
                        if($recurring_amount == $row['payment_amount']){
                            $recurring_processing_fee = $row['processing_fee'];
                        }
                        if($row['processing_fee_type']==2){
                            $temp['amount'] = $row['payment_amount']+$row['processing_fee'];
                        }else{
                            $temp['amount'] = $row['payment_amount'];
                        }
                        $temp['date'] = $row['payment_date'];
                        $temp['created_dt'] = $row['created_dt'];
                        $temp['processing_fee'] = $row['processing_fee'];
                        $temp['payment_status'] = $row['payment_status'];
                        $temp['paytime_type'] = $row['paytime_type'];
                        $temp['cc_name'] = $row['cc_name'];
                        $paid_amt += $temp['amount'];
                        $payment_details[] = $temp;
                    }
                }
                if(($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    if($billing_options_no_of_payments>1){
                        $already_recurring_records_count = 0;
                        $next_date = '';
                        for($i=0;$i<count($payment_details);$i++){
                            if($payment_details[$i]['paytime_type']=='R'){
                                $already_recurring_records_count++;
                                $next_date = $payment_details[$i]['date'];
                            }
                        }
                        if($billing_options_no_of_payments>$already_recurring_records_count){
                            $count = $billing_options_no_of_payments - $already_recurring_records_count;
                            $future_date = $next_date;
                            for($j=0;$j<$count;$j++){
                                if($payment_frequency=='W'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+1 weeks', strtotime($future_date)));
                                }elseif($payment_frequency=='B'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('+14 days', strtotime($future_date)));
                                }elseif($payment_frequency=='M'){
                                    $next_payment_date2 = date('Y-m-d', strtotime('next month', strtotime($future_date)));
                                }
                                
                                $temp=[];
                                if($processing_fee_type==2){
                                    $temp['amount'] = $recurring_amount+$recurring_processing_fee;
                                }else{
                                    $temp['amount'] = $recurring_amount;
                                }
                                $temp['date'] = $next_payment_date2;
                                $temp['processing_fee'] = $recurring_processing_fee;
                                $temp['payment_status'] = 'N';
                                $temp['paytime_type'] = 'R';
                                $temp['cc_name'] = $credit_card_name;
                                $payment_details[] = $temp;
                                $future_date = $next_payment_date2;
                            }
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "No records found.");
                ipn_log_info($this->json($error_log));
                if($return_value==1){
                    $error = array('status' => "Failed", "msg" => "No records found.");
                    $this->response($this->json($error), 200);
                }
                return;
            }
        }
        
        $subject = $membership_category_title." - Payment failed";
        
        $message .= "<b>The following payment for the Buyer $buyer_name failed.</b><br><br>";
        $message .= "<b>Membership Details</b><br><br>";
        $message .= "Membership Start Date: ".date("M dS, Y", strtotime($payment_start_date))."<br>";
        $message .= "Membership: $membership_category_title, $membership_title<br>Participant: ".$participant_name."<br>";
        if($membership_structure=='NC' && $billing_options=='PP' && $billing_options_expiration_date!='0000-00-00'){
            $message .= "$no_of_classes Class Package Expires: ".date("D, M d, Y", strtotime($billing_options_expiration_date))."<br>";
        }
        $message .= "<br>";
            
        $prorated = "";
        if ($payment_frequency === 'B' || ($payment_frequency == 'M' && $prorate_first_payment_flg == 'Y')) {
            $prorated = "(Pro-rated)";
        }

        if($paid_amt>0){
//            if($initial_payment>0){
                $message .= "<br><b>Registration Details</b><br><br>";
                $message .= "Sign-up Fee: $wp_currency_symbol".(number_format(($signup_fee-$signup_fee_disc),2))."<br>";
                if($processing_fee_type==2){
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment+$inital_processing_fee),2))." (includes $wp_currency_symbol".$inital_processing_fee." administrative fees) $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }else{
                    if($initial_payment>0){
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))." $prorated<br>";
                    }else{
                        $message .= "Total Payment: $wp_currency_symbol".(number_format(($initial_payment),2))."<br>";
                    }
                }
//            }
        }

        if(count($payment_details)>0){
            if($payment_frequency!='N' && $payment_type=='R'){
                $freq_str = "";
                if($membership_structure=='OE' || $membership_structure==1){
                    if($payment_frequency=='B'){
                        $freq_str = "Semi-Monthly Recurring: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "Monthly Recurring: ";
                    }elseif($payment_frequency=='W'){
                        $freq_str = "Weekly Recurring: ";
                    }elseif($payment_frequency=='A'){
                        $freq_str = "Annual Recurring: ";
                    }elseif($payment_frequency=='C'){
                        if($custom_recurring_frequency_period_type=='CW'){
                            $freq_str = "Weekly Recurring: ";
                        }elseif($custom_recurring_frequency_period_type=='CM'){
                            $freq_str = "Monthly Recurring: ";
                        }
                    }
                }elseif($billing_options=='PP'){
                    if($payment_frequency=='W'){
                        $freq_str = "$billing_options_no_of_payments Weekly Payments: ";
                    }elseif($payment_frequency=='B'){
                        $freq_str = "$billing_options_no_of_payments Bi-Weekly Payments: ";
                    }elseif($payment_frequency=='M'){
                        $freq_str = "$billing_options_no_of_payments Monthly Payments: ";
                    }
                }
                
                $message .= "<br><br><b>Membership Payment Plan</b><br><br>";
                $recur_pf_str = "";
                if($processing_fee_type==2){
                    $recur_pf_str = " (plus $wp_currency_symbol".$recurring_processing_fee." administrative fees)";
                }
                if($freq_str!=""){
                    $message .= "$freq_str $wp_currency_symbol".$recurring_amount.$recur_pf_str."<br>";
                }
                $message .= "Next Payment Date: ".date("M dS, Y", strtotime($next_payment_date))."<br>";
            }
            
            $message.="<br><br><b>Payment Details</b><br><br>";
            for($init_success_payments=0;$init_success_payments<count($payment_details);$init_success_payments++){
                $pf_str = "";
                if($processing_fee_type==2){
                    $pf_str = " (includes $wp_currency_symbol".$payment_details[$init_success_payments]['processing_fee']." administrative fees)";
                }
                if($initial_payment>0 && $payment_details[$init_success_payments]['paytime_type']=='I'){
                    $down_pay = $payment_details[$init_success_payments]['amount'].$pf_str." paid on ".date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']))." (".$payment_details[$init_success_payments]['cc_name'].") " .$prorated;
                    $msg1 .= "Payment: $wp_currency_symbol".$down_pay."<br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='F'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    if($payment_details[$init_success_payments]['payment_status']=='S'){
                        $paid_str = "paid on";
                        $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    }else{
                        $paid_str = "due";
                        $pay_type= "";
                    }
                    $msg2 .= "First Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='S'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "paid on";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
                if($payment_details[$init_success_payments]['paytime_type']=='R' && $payment_details[$init_success_payments]['payment_status']=='N' && ($membership_structure=='NC' || $membership_structure=='C') && $billing_options=='PP'){
                    $amount = $payment_details[$init_success_payments]['amount'];
                    $date = date("M dS, Y", strtotime($payment_details[$init_success_payments]['date']));
                    $paid_str = "due";
                    $pay_type= "(".$payment_details[$init_success_payments]['cc_name'].")";
                    $msg3 .= "Payment : $wp_currency_symbol".$amount.$pf_str." ".$paid_str." ".$date." ".$pay_type." <br>";
                }
            }
            $message .= $msg1.$msg2.$msg3;
            $message .= "<br><br><br>";
        }
        
        $sendEmail_status = $this->sendEmailForMembership('',$studio_mail, $subject, $message, '', '', $cc_email_list);
        if(isset($sendEmail_status['status'])&&$sendEmail_status['status']=="true"){
            $error_log = array('status' => "Success", "msg" => "Email sent successfully.", "Email_id" => $buyer_email."   ".$sendEmail_status['mail_status']);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $log = array('status' => "Success", "msg" => "Membership order receipt sent successfully.");
                $this->response($this->json($log), 200);
            }
        }else{
            $error_log = array('status' => "Failed", "msg" => "Email not sent.", "Email_id" => $buyer_email);
            ipn_log_info($this->json($error_log));
            if($return_value==1){
                $error = array('status' => "Failed", "msg" => "Membership order receipt sent failed.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    protected function sendEmailForMembership($from, $to, $subject, $message, $cmp_name, $reply_to, $cc_email_list) {
        
        if(empty(trim($cmp_name))){
            $cmp_name = "MyStudio";
        }
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            if(filter_var(trim($reply_to), FILTER_VALIDATE_EMAIL)){
                $mail->AddReplyTo($reply_to, $cmp_name);
//                $mail->Sender=$reply_to;
            }
//            if(filter_var($from, FILTER_VALIDATE_EMAIL)){
//                $mail->setFrom($from, $cmp_name);
//                $mail->AddCC($from);
//            }else{
//                $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
//            }
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(filter_var(trim($from), FILTER_VALIDATE_EMAIL)){
                $mail->AddCC($from);
            }
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'base64';
//            $mail->Subject = $subject;
            $mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
            $mail->Body = $message;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array('status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }

    protected function clean($string) {
        $str = preg_replace('/\s+/', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }
    
    protected function updateEvent(){
        $sql = sprintf("SELECT e.event_id, e.event_title, e.company_id, c.company_code FROM `event` e LEFT JOIN `company` c ON e.company_id = c.company_id WHERE `event_type` IN ('M','S')");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            while($output = mysqli_fetch_assoc($result)){
                $company_id = $output['company_id'];
                $company_code = $output['company_code'];
                $event_id = $output['event_id'];
                $event_title = $output['event_title'];

                $company_code_new = $this->clean($company_code);
                $event_title_new = $this->clean($event_title);
                $event_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=$company_code_new/$company_id/$event_id";

                $sql3 = sprintf("UPDATE `event` SET `event_url` = '%s' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $event_url), mysqli_real_escape_string($this->db, $event_id));
                $result3 = mysqli_query($this->db, $sql3);
                if (!$result3) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                    ipn_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }
    
    protected function updateEventWaiver(){
        $count=0;
//        $sql = sprintf("SELECT event_id, company_id, waiver_policies, 'E' as type FROM `event` WHERE waiver_policies!='' UNION
//                    SELECT membership_option_id, company_id, waiver_policies, 'M' as type FROM membership_options WHERE waiver_policies!=''");
        $sql = sprintf("SELECT event_id, company_id, waiver_policies, 'E' as type FROM `event` WHERE waiver_policies!=''");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                while($output = mysqli_fetch_assoc($result)){
                    $company_id = $output['company_id'];
                    $type = $output['type'];
                    $event_id = $output['event_id'];
                    $waiver_policies = urldecode($output['waiver_policies']);

                    if($type=='E'){
                        $sql2 = sprintf("UPDATE `event` SET `waiver_policies` = '%s' WHERE company_id='%s' AND `event_id` = '%s' ", mysqli_real_escape_string($this->db, $waiver_policies), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
                    }else{
                        $sql2 = sprintf("UPDATE `membership_options` SET `waiver_policies` = '%s' WHERE company_id='%s' AND `membership_option_id` = '%s' ", mysqli_real_escape_string($this->db, $waiver_policies), mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $event_id));
                    }
                    $result2 = mysqli_query($this->db, $sql2);
                    if (!$result2) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $count++;
                    }
                }
                $error = array('status' => "Success", "msg" => "$count records updated.");
                $this->response($this->json($error), 500);
            }else{
                $error = array('status' => "Failed", "msg" => "No records found.");
                $this->response($this->json($error), 500);
            }
        }
    }
//    
//    protected function addMembershipDimensions(){
//        $start = 1; $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $query1 = sprintf("SELECT m.company_id, m.membership_id, m.category_title, m.created_dt m_cd, mo.membership_option_id, mo.membership_title, mo.created_dt mo_cd
//                FROM `membership` m LEFT JOIN `membership_options` mo ON m.`membership_id` = mo.`membership_id` AND m.`company_id`=mo.`company_id`
//                WHERE m.`company_id` between $start and $end");
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $category = $option = $membership_id_arr = $membership_options_id_arr = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = $temp2 = [];
//                    $temp['company_id'] = $temp2['company_id'] = $company_id = $row['company_id'];
//                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
//                    $temp['category_title'] = $temp2['category_title'] = $category_title = $row['category_title'];
//                    $temp['m_cd'] = $row['m_cd'];
//                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp2['option_title'] = $option_title = $row['membership_title'];
//                    $temp2['mo_cd'] = $row['mo_cd'];
//                    
//                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
//                        $membership_id_arr[] = $membership_id;
//                        $category[] = $temp;
//                    }
//                    if(!empty($membership_option_id) && !in_array($membership_option_id, $membership_options_id_arr, true)){
//                        $membership_options_id_arr[] = $membership_option_id;
//                        $option[] = $temp2;
//                    }
//                }
//                for($i=0;$i<count($category);$i++){
//                    $date_init = 0;
//                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("first day of +$date_init month", strtotime($category[$i]['m_cd'])));
//                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
//                            $every_month_dt = $curr_dt;
//                        }
//                        if($every_month_dt>$curr_dt){
//                            continue;
//                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $category[$i]['company_id'], $category[$i]['category_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            if($curr_dt>$every_month_dt){
//                                $date_init++;
//                                $j--;
//                            }
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==0){
//                                $sql2 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `category_title`) VALUES('%s', DATE_FORMAT('%s', '%s'),'%s','%s')",
//                                        $category[$i]['company_id'], $every_month_dt, $date_str, $category[$i]['category_id'], mysqli_real_escape_string($this->db, $category[$i]['category_title']));
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }
//                                
//                            }
//                        }
//                    }
//                }
//                for($i=0;$i<count($option);$i++){
//                    $date_init = 0;
//                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("first day of +$date_init month", strtotime($option[$i]['mo_cd'])));
//                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
//                            $every_month_dt = $curr_dt;
//                        }
//                        if($every_month_dt>$curr_dt){
//                            continue;
//                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            if($curr_dt>$every_month_dt){
//                                $date_init++;
//                                $j--;
//                            }
//                        
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==0){
//                                $sql2 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`) VALUES('%s', DATE_FORMAT('%s', '%s'),'%s','%s','%s','%s')",
//                                        $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id'], mysqli_real_escape_string($this->db, $option[$i]['category_title']), mysqli_real_escape_string($this->db, $option[$i]['option_title']));
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }
//                                
//                            }
//                        }
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
//    
//    protected function updateMembershipDimensionsNetSales(){
//        $start = 1;
//        $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $query1 = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, mp.payment_date mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id between $start and $end AND mp.payment_status='S' GROUP BY mr.`membership_id`, mr.`membership_option_id`, month(mp.payment_date)");
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $category = $option = $membership_id_arr = $membership_options_id_arr = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = $temp2 = [];
//                    $temp['company_id'] = $temp2['company_id'] = $company_id = $row['company_id'];
//                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
//                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
//                    $temp2['amount'] = $amount = $row['amount'];
//                    $temp2['mp_ud'] = $row['mp_ud'];
//                    
//                    if(!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)){
//                        $membership_id_arr[] = $membership_id;
//                        $category[] = $temp;
//                    }
////                    $category[count($category)-1]['amount'] = 
////                    if(!empty($membership_option_id) && !in_array($membership_option_id, $membership_options_id_arr, true)){
////                        $membership_options_id_arr[] = $membership_option_id;
//                        $option[] = $temp2;
////                    }
//                }
//                for($i=0;$i<count($option);$i++){
//                    $date_init = 0;
////                    for($j=0;$j<1;$j++){
//                        $date_str = "%Y-%m";
//                        $curr_dt = date("Y-m-d");
//                        $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
////                        if($every_month_dt=='0000-00-00' || $every_month_dt=='1970-01-01'){
////                            $every_month_dt = $curr_dt;
////                        }
////                        if($every_month_dt>$curr_dt){
////                            continue;
////                        }
////                        if($option[$i]['amount']==0){
//////                            $date_init++;
//////                            $j--;
////                            continue;
////                        }
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
////                            if($curr_dt>$every_month_dt){
////                                $date_init++;
////                                $j--;
////                            }
//                        
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==1){
//                                
//                                $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'",
//                                        $option[$i]['amount'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }else{
//                                    $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=`sales_amount`+'%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",
//                                        $option[$i]['amount'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id']);
//                                    $res3 = mysqli_query($this->db, $sql3);
//                                    if(!$res3){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//                                        ipn_log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 500);
//                                    }
//                                }
//                                
//                            }
//                        }
////                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
//    
//    protected function updateMembershipDimensionsMembers(){
//        $start = 1;
//        $end = 1000;
//        if(isset($_REQUEST['start'])){
//            $start = $_REQUEST['start'];
//        }
//        if(isset($_REQUEST['end'])){
//            $end = $_REQUEST['end'];
//        }
//        $date_str = "%Y-%m";
//        $first_date_str = "%Y-%m-01";
//        
//        $query1 = sprintf("SELECT 'New' as type, count(*) total, DATE_FORMAT(created_dt,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(created_dt,'%s') as date  FROM membership_registration 
//                    WHERE company_id between $start and $end AND created_dt>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(created_dt)
//                    UNION
//                    SELECT 'Pause' as type, count(*) total, DATE_FORMAT(hold_date,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(hold_date,'%s') as date FROM membership_registration 
//                    WHERE company_id between $start and $end AND hold_date>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(hold_date)
//                    UNION
//                    SELECT 'Cancel' as type, count(*) total, DATE_FORMAT(cancelled_date,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(cancelled_date,'%s') as date FROM membership_registration 
//                    WHERE company_id between $start and $end AND cancelled_date>=DATE_FORMAT(CURDATE(), '%s') - INTERVAL 3 MONTH GROUP BY membership_id, membership_option_id, month(cancelled_date)  
//                    UNION
//                    SELECT 'Active' as type, count(*) total, DATE_FORMAT(created_dt,'%s') as month, company_id, membership_id, membership_option_id, DATE_FORMAT(created_dt,'%s') as date FROM `membership_registration` 
//                    WHERE company_id between $start and $end AND `membership_status`='R' GROUP BY membership_id, membership_option_id, month(created_dt) 
//                    ORDER BY membership_id, membership_option_id, month", $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str, $first_date_str, $date_str, $first_date_str);
//        $result1 = mysqli_query($this->db, $query1);
//        
//        if(!$result1){
//            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
//            ipn_log_info($this->json($error_log));
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//            $this->response($this->json($error), 500);
//        }else{
//            $membership_options_id_arr = $option = $new = $pause = $cancel = $active = [];
//            $num_rows = mysqli_num_rows($result1);
//            if($num_rows>0){
//                while($row = mysqli_fetch_assoc($result1)){
//                    $temp = [];
//                    $temp['company_id'] = $row['company_id'];
//                    $temp['category_id'] = $membership_id = $row['membership_id'];
//                    $temp['option_id'] = $membership_option_id = $row['membership_option_id'];
//                    $temp['month'] = $row['month'];
//                    $temp['type'] = $row['type'];
//                    $temp['total'] = $row['total'];
//                    $temp['date'] = $row['date'];
//                    
//                    if($temp['type']=='Active'){
//                        $active[] = $temp;
//                    }elseif($temp['type']=='New'){
//                        $new[] = $temp;
//                    }elseif($temp['type']=='Pause'){
//                        $pause[] = $temp;
//                    }elseif($temp['type']=='Cancel'){
//                        $cancel[] = $temp;
//                    }
//                }
//                $opt_array = [$active, $new, $pause, $cancel];
//                $col_arr = ['active_members','reg_new','reg_hold','reg_cancelled'];
//                for($k=0;$k<4;$k++){
//                    $option = $opt_array[$k];
//                    $col_str = $col_arr[$k];
//                    for($i=0;$i<count($option);$i++){
//                        
//                        $every_month_dt = date("Y-m-d", strtotime($option[$i]['date']));
//
//                        $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')",
//                                $option[$i]['company_id'], $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);                            
//                        $res1 = mysqli_query($this->db, $sql1);
//                        if(!$res1){
//                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
//                            ipn_log_info($this->json($error_log));
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                            $this->response($this->json($error), 500);
//                        }else{
//                            $num_of_rows = mysqli_num_rows($res1);
//                            if($num_of_rows==1){
//                                $sql2 = sprintf("UPDATE `membership_dimensions` SET $col_str='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'",
//                                        $option[$i]['total'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
//                                $res2 = mysqli_query($this->db, $sql2);
//                                if(!$res2){
//                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
//                                    ipn_log_info($this->json($error_log));
//                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                    $this->response($this->json($error), 500);
//                                }else{
//                                    $sql3 = sprintf("UPDATE `membership_dimensions` SET $col_str=$col_str+'%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`=0",
//                                        $option[$i]['total'], $option[$i]['company_id'], $every_month_dt, $date_str, $option[$i]['category_id']);
//                                    $res3 = mysqli_query($this->db, $sql3);
//                                    if(!$res3){
//                                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
//                                        ipn_log_info($this->json($error_log));
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
//                                        $this->response($this->json($error), 500);
//                                    }
//                                }
//
//                            }
//                        }
//                    }
//                }
//            }else{
//                $error_log = array('status' => "Failed", "msg" => "No records.", "query" => "$query1");
//                ipn_log_info($this->json($error_log));
//                $error = array('status' => "Failed", "msg" => "No records.");
//                $this->response($this->json($error), 204);
//            }
//        }
//    }
    
    
    
     public function addMembershipDimensions($company_id, $membership_id, $mem_option_id, $category_title, $option_title, $period) {
        $reg_id = 0;
        if(!empty($period)){
            $curr_dt = date("Y-m", strtotime($period));
        }else{
            $curr_dt = date("Y-m");
        }
       
        if(!empty($mem_option_id)){
            $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND  `option_id`='%s' AND option_id !=0  AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $curr_dt));
            $result = mysqli_query($this->db, $sql1);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                ipn_log_info($this->json($error_log));
                $reg_id = 1;
            } else{
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows == 0) {
                    $insertquery = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`, `option_id`, `category_title`, `option_title`) VALUES('%s','%s','%s','%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $mem_option_id), mysqli_real_escape_string($this->db, $category_title), mysqli_real_escape_string($this->db, $option_title));
                    $res = mysqli_query($this->db, $insertquery);
                    if (!$res) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    }
                }
            }
        }

        $sql2 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`=0 AND `period`='%s'", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $curr_dt));
        $result2 = mysqli_query($this->db, $sql2);
        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else{
            $num_of_rows1 = mysqli_num_rows($result2);
            if ($num_of_rows1 == 0) {
                $insertquery1 = sprintf("INSERT INTO `membership_dimensions`(`company_id`, `period`, `category_id`,  `category_title`) VALUES('%s', '%s','%s','%s')", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $curr_dt), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $category_title));
                $res2 = mysqli_query($this->db, $insertquery1);
                if (!$res2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insertquery");
                    ipn_log_info($this->json($error_log));
                    $reg_id = 1;
                }
            }
        }
        
        if($reg_id==1){
            $msg = "Something went wrong for addMembershipDimensions(), check log";
            $this->sendEmailForAdmins($msg);
        }
        
    }
    
    public function updateMembershipDimensionsNetSales($company_id, $membership_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(mp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
        
        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(
                         IF(mp.payment_status = 'R', (if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) )*-1,
                         if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) ) ) amount, 
                         mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
                         ON mr.`membership_registration_id` = mp.`membership_registration_id` 
                         WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
                         AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status IN ('S','R','FR') GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status IN ('S','R','FR') GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));

        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $result_rows= mysqli_num_rows($result);
//            log_info("number of rows : ".$result_rows);
            $category = $membership_id_arr = $membership_options_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['category_id'] = $temp2['category_id'] = $membership_id = $row['membership_id'];
                    $temp2['option_id'] = $membership_option_id = $row['membership_option_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($membership_id) && !in_array($membership_id, $membership_id_arr, true)) {
                        $membership_id_arr[] = $membership_id;
                        $category[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));                     
                    $sql1 = sprintf("SELECT * FROM `membership_dimensions` WHERE `company_id`='%s' AND `category_id`='%s' AND `option_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['category_id'], $option[$i]['option_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            $sql2 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='%s'", $option[$i]['amount'], mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str, $option[$i]['category_id'], $option[$i]['option_id']);
                            $res2 = mysqli_query($this->db, $sql2);
                            if (!$res2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $reg_id = 1;
                            } else {
                                if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `category_id`='%s' AND `option_id`='0'",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $membership_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                            }
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No records.","query" => "$sql");
                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateMembershipDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    private function getUserTimezone($company_id) {
        $query = sprintf("SELECT `timezone` FROM `company` WHERE `company_id`='%s' LIMIT 0,1", mysqli_real_escape_string($this->db, $company_id));
        $result_tz = mysqli_query($this->db, $query);
        $user_timezone='';
        if (!$result_tz) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
        } else {
            $num_rows = mysqli_num_rows($result_tz);                       
            if ($num_rows > 0) {
                $user_timezone = mysqli_fetch_assoc($result_tz);
            }
            return $user_timezone;
        }
    }
    
    private function sendEmailForAdmins($msg){
        $cmp_name = "MyStudio WepayIpn";
        $to = 'jim.p@technogemsinc.com';
        $cc_email_list = 'rajeshsankar.k@technogemsinc.com';
        $mail = new phpmailer(true);
        try{
            $mail->SMTPDebug = 1; // 1 tells it to display SMTP errors and messages, 0 turns off all errors and messages, 2 prints messages only.
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->isSMTP();
            $mail->SMTPAuth = true; //Username to use for SMTP authentication
            $mail->Username = "AKIAITXY437BUFVGL32Q"; //Password to use for SMTP authentication
            $mail->Password = "AgdGUNTdhsgZqW9L/pCT50ZLoxxG2B1Dw6zUfRmdsA4S"; //Set who the message is to be sent from
            $mail->setFrom('no-reply@mystudio.academy', $cmp_name);
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->FromName = $cmp_name;
            $mail->AddAddress($to);
            if(!empty(trim($cc_email_list))){
                $cc_addresses = explode(',', $cc_email_list);
                for($init=0;$init<count($cc_addresses);$init++){
                    if(filter_var(trim($cc_addresses[$init]), FILTER_VALIDATE_EMAIL)){
                        $mail->AddCC($cc_addresses[$init]);
                    }
                }
            }
            
            $mail->Subject = "Wepay Ipn Handling Error.";
            $mail->Body = $msg;
            $response['mail_status'] = $mail->Send();
            $response['status'] = "true";
        }catch (phpmailerException $e) {
            $response['mail_status'] = $e->errorMessage(); //Pretty error messages from PHPMailer
            $response['status'] = "false";
        } catch (Exception $e) {
            $response['mail_status'] = $e->getMessage(); //Boring error messages from anything else!
            $response['status'] = "false";
        }
        $error_log = array("For"=>"Admin", 'status' => $response['status'], "msg" => "Email Status.", "Email_id" => $to, "Return Status" => $response['mail_status']);
        ipn_log_info($this->json($error_log));
        return $response;
    }
    
    private function setExpDateForCustomMembership() {
        
        $query = sprintf("SELECT o.`company_id`,o.`membership_structure`,o.`membership_id` ,o.`membership_option_id`,o.`expiration_status` , 
         o.`expiration_date_type` , o.`expiration_date_val`,r.`membership_registration_id`,r.`payment_start_date`
        FROM `membership_options` o
        JOIN `membership_registration` r ON r.`membership_option_id`= o.`membership_option_id` AND  o.`membership_structure` = 'C'
        WHERE  r.`membership_status`  in ('R','P')
        AND r.`membership_structure` = 'C' AND r.`billing_options_expiration_date`='0000-00-00'
        ORDER BY o.`membership_structure`, o.`membership_option_id`,`membership_registration_id` ASC");


        $result_query = mysqli_query($this->db, $query);
        if (!$result_query) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
            exit();
        } else {
            $num_rows = mysqli_num_rows($result_query);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_query)) {
                    $mebership_option_id = $row['membership_option_id'];
                    $mebership_registration_id = $row['membership_registration_id'];
                    $company_id = ['company_id'];
                    $membership_structure = $row['membership_structure'];
                    $membership_id = $row['membership_id'];
                    $expiration_status = $row['expiration_status'];
                    $expiration_date_type = $row['expiration_date_type'];
                    $expiration_date_val = $row['expiration_date_val'];
                    $payment_start_date = $row['payment_start_date'];


                    if (!empty($expiration_status) && !empty($expiration_date_type) && $expiration_date_val != 0 && $expiration_status == 'Y') {
                        if ($expiration_date_type == 'W') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val weeks", strtotime($payment_start_date)));
                        } elseif ($expiration_date_type == 'M') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val months", strtotime($payment_start_date)));
                        } elseif ($expiration_date_type == 'Y') {
                            $new_billing_expiration_date = date('Y-m-d', strtotime("+$expiration_date_val years", strtotime($payment_start_date)));
                        }

                        $update = sprintf("UPDATE `membership_registration` SET `billing_options_expiration_date`= '$new_billing_expiration_date',`end_email_date`= NULL,`last_updt_dt`=`last_updt_dt` WHERE `membership_registration_id`='%s'", $mebership_registration_id);
                        $result_update = mysqli_query($this->db, $update);
                        if (!$result_update) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                            exit();
                        } else {
                            $log = array("status" => "Success", "msg" => "Membership registration id $mebership_registration_id updated.");
                            ipn_log_info($this->json($log));
                        }
                    }
                }
            }
        }
    }
    private function updatingEventPaymentRecordforCheckoutChanges(){
        $temp = $payment_detail = [];
        $query = sprintf("select * from event_payment where checkout_id  NOT IN (select checkout_id from event_payment where `schedule_status` IN('CR','PR' )) and `schedule_status` ='R' AND  checkout_id <> ''  order by event_reg_id");
        $result_query = mysqli_query($this->db, $query);
        if(!$result_query){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
            exit();
        }else{
            $num_rows = mysqli_num_rows($result_query);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_query)) {
                   $temp['event_reg_id'] = $row['event_reg_id'];
                   $temp['student_id'] = $row['student_id'];
                   $check_checkout_id = $temp['checkout_id'] = $row['checkout_id'];
                   $temp['checkout_status'] = $row['checkout_status'];
                   $temp['payment_amount'] = $row['payment_amount'];
                   $temp['schedule_status'] = $row['schedule_status'];
                   $temp['refunded_amount'] = $row['refunded_amount'];
                   $temp['refunded_date'] = $row['refunded_date'];
                   $schedule_date = $temp['schedule_date'] = $row['schedule_date'];
                   $temp['cc_id'] = $row['cc_id'];
                   $temp['cc_name'] = $row['cc_name'];
                   $temp['created_dt'] = $row['created_dt'];
                   $temp['last_updt_dt'] = $row['last_updt_dt'];
                   $temp['processing_fee'] = $row['processing_fee'];
                   $payment_detail[] = $temp;
                   
                    $query1 = sprintf("select * from event_payment where checkout_id = '%s' and `schedule_status`='FR'", mysqli_escape_string($this->db, $check_checkout_id));   
                    $result_query1 = mysqli_query($this->db, $query1);
                    if(!$result_query1){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $num_rows1 = mysqli_num_rows($result_query1);
                    if ($num_rows1 > 0) {
                       $row1 = mysqli_fetch_assoc($result_query1);
                       $last_update_date =  $row1['last_updt_dt'];
                       $event_payment_id = $row1['event_payment_id'];
//                      $last_up_Date = strtotime($row['last_updt_dt']);
//                      $tempplus1minute = $last_up_Date+(60*1);//USE FOR ALTER TIME DIFFERENCE FOR FR RECORDS
//                      $plusoneminute = date("Y-m-d H:i:s", $tempplus1minute);
//                      $tempminus1minute = $last_up_Date-(60*1);
//                      $minusoneminute = date("Y-m-d H:i:s", $tempminus1minute);
                      if($last_update_date == $row['last_updt_dt']){//USe FOR INITIAL INSERT
//                      if($last_update_date <= $plusoneminute && $last_update_date >= $minusoneminute){//USE FOR UPDATE FR RECORDS WITH ONE MINUTE DIFFERENCE
//                           echo "plus_one_minute  ".$plusoneminute."<br>"."minus_one_minute  ".$minusoneminute."<br>"."  last_updt_date  ".$last_update_date;
                           $update_query = sprintf("UPDATE `event_payment` SET `refunded_date` = null,`checkout_status`='released', `refunded_amount`= 0, `last_updt_dt` = IF(`schedule_date` is not null && `schedule_date`!='0000-00-00',concat(`schedule_date`,' ',time('%s')),`created_dt`) where event_payment_id = '%s'",mysqli_escape_string($this->db, $row1['last_updt_dt']),mysqli_escape_string($this->db,$event_payment_id)); 
                           $update_result = mysqli_query($this->db, $update_query);
                           if(!$update_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                       }
                    }else{
                        if($schedule_date == '0000-00-00' || is_null($schedule_date)){
                            $new_schedule_date = $row['created_dt'];
                            $schedule_date = 'NULL';
                        }else{
                            $new_schedule_date = $schedule_date." ".date("H:i:s",strtotime($row['last_updt_dt']));
                            $schedule_date = "'".$row['schedule_date']."'";
                        }
                       $released='released';
                       $insert_query = sprintf("INSERT INTO `event_payment`(`event_reg_id`, `student_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `schedule_date`, `schedule_status`, `cc_id`, `cc_name`, `created_dt`, `last_updt_dt`) values ('%s','%s','%s','%s','%s','%s',%s,'FR','%s','%s','%s','%s')",
                              mysqli_escape_string($this->db, $row['event_reg_id']), mysqli_escape_string($this->db, $row['student_id']), mysqli_escape_string($this->db, $row['checkout_id']), $released, 
                              mysqli_escape_string($this->db, $row['payment_amount']),mysqli_escape_string($this->db, $row['processing_fee']),$schedule_date ,mysqli_escape_string($this->db, $row['cc_id']), mysqli_escape_string($this->db, $row['cc_name']),
                              mysqli_escape_string($this->db, $row['created_dt']),mysqli_escape_string($this->db, $new_schedule_date)); 
                       $insert_result = mysqli_query($this->db, $insert_query);
                            if(!$insert_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
            }else{
                 $error_log = array('status' => "Failed", "msg" => "there is no need for update.", "query" => "$query");
                 log_info($this->json($error_log));
                 exit();
            }
        }
            
    }
    
    protected function updateMembership(){
        $sql = sprintf("SELECT m.membership_id, m.company_id, c.company_code FROM `membership` m LEFT JOIN `company` c ON m.company_id = c.company_id WHERE `membership_category_url`=''");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            while($output = mysqli_fetch_assoc($result)){
                $company_id = $output['company_id'];
                $company_code = $output['company_code'];
                $membership_id = $output['membership_id'];

                $company_code_new = $this->clean($company_code);
                $category_url = "https://www.mystudio.academy/m/?=$company_code_new/$company_id/$membership_id";

                $sql2 = sprintf("UPDATE `membership` SET `membership_category_url` = '%s' WHERE `membership_id` = '%s' ", mysqli_real_escape_string($this->db, $category_url), mysqli_real_escape_string($this->db, $membership_id));
                $result2 = mysqli_query($this->db, $sql2);
                if (!$result2) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                    ipn_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
        
        $sql3 = sprintf("SELECT m.membership_id, m.membership_option_id, m.company_id, c.company_code FROM `membership_options` m LEFT JOIN `company` c ON m.company_id = c.company_id WHERE 1");
        $result3 = mysqli_query($this->db, $sql3);
        if (!$result3) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            while($output = mysqli_fetch_assoc($result3)){
                $company_id = $output['company_id'];
                $company_code = $output['company_code'];
                $membership_id = $output['membership_id'];
                $membership_option_id = $output['membership_option_id'];

                $company_code_new = $this->clean($company_code);
                $option_url = "https://www.mystudio.academy/m/?=$company_code_new/$company_id/$membership_id/$membership_option_id";

                $sql4 = sprintf("UPDATE `membership_options` SET `membership_option_url` = '%s' WHERE `membership_option_id` = '%s' ", mysqli_real_escape_string($this->db, $option_url), mysqli_real_escape_string($this->db, $membership_option_id));
                $result4 = mysqli_query($this->db, $sql4);
                if (!$result4) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql4");
                    ipn_log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }
    }

    private function updatingMembershipPaymentRecordforCheckoutChanges(){
        $temp = $payment_detail = [];
        $query = sprintf("select * from membership_payment where `payment_status` ='R' and  checkout_id <> ''   order by membership_registration_id");
        $result_query = mysqli_query($this->db, $query);
        if(!$result_query){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
            exit();
        }else{
            $num_rows = mysqli_num_rows($result_query);
            if ($num_rows > 0) {
                while ($row = mysqli_fetch_assoc($result_query)) {
                   $temp['company_id'] = $row['company_id'];
                   $temp['membership_registration_id'] = $row['membership_registration_id'];
                   $check_checkout_id = $temp['checkout_id'] = $row['checkout_id'];
                   $temp['checkout_status'] = $row['checkout_status'];
                   $temp['payment_amount'] = $row['payment_amount'];
                   $temp['payment_status'] = $row['payment_status'];
                   $temp['refunded_amount'] = $row['refunded_amount'];
                   $temp['refunded_date'] = $row['refunded_date'];
                   $payment_date = $temp['payment_date'] = $row['payment_date'];
                   $temp['cc_id'] = $row['cc_id'];
                   $temp['cc_name'] = $row['cc_name'];
                   $temp['created_dt'] = $row['created_dt'];
                   $temp['last_updt_dt'] = $row['last_updt_dt'];
                   $temp['processing_fee'] = $row['processing_fee'];
                   $payment_detail[] = $temp;
                   
                    $query1 = sprintf("select * from membership_payment where checkout_id = '%s' and `payment_status`='FR'", mysqli_escape_string($this->db, $check_checkout_id));   
                    $result_query1 = mysqli_query($this->db, $query1);
                    if(!$result_query1){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query1");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $num_rows1 = mysqli_num_rows($result_query1);
                    if ($num_rows1 > 0) {
                       $row1 = mysqli_fetch_assoc($result_query1);
                       $last_update_date =  $row1['last_updt_dt'];
                       $membership_payment_id = $row1['membership_payment_id'];
                       $fr_company_id = $row1['company_id'];
//                      $last_up_Date = strtotime($row['last_updt_dt']);
//                      $tempplus1minute = $last_up_Date+(60*1);//USE FOR ALTER TIME DIFFERENCE FOR FR RECORDS
//                      $plusoneminute = date("Y-m-d H:i:s", $tempplus1minute);
//                      $tempminus1minute = $last_up_Date-(60*1);
//                      $minusoneminute = date("Y-m-d H:i:s", $tempminus1minute);
//                      if($last_update_date == $row['last_updt_dt']){//USe FOR INITIAL INSERT
                       if($fr_company_id==0){
                      
//                       if($last_update_date <= $plusoneminute && $last_update_date >= $minusoneminute){//USE FOR UPDATE FR RECORDS WITH ONE MINUTE DIFFERENCE
                           $update_query = sprintf("UPDATE `membership_payment` SET `company_id`='%s',`refunded_date` = null,`checkout_status`='released', `refunded_amount`= 0, `last_updt_dt` = IF(`payment_date` is not null && `payment_date`!='0000-00-00',concat(`payment_date`,' ',time('%s')),`created_dt`) where membership_payment_id = '%s'",
                                   mysqli_escape_string($this->db, $row['company_id']),mysqli_escape_string($this->db, $row1['last_updt_dt']),mysqli_escape_string($this->db,$membership_payment_id)); 
                           $update_result = mysqli_query($this->db, $update_query);
                           if(!$update_result){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                       }
                    }else{
                        if($payment_date == '0000-00-00' || is_null($payment_date)){
                            $new_payment_date = $row['created_dt'];
                            $payment_date = 'NULL';
                        }else{
                            $new_payment_date = $payment_date." ".date("H:i:s",strtotime($row['last_updt_dt']));
                            $payment_date = "'".$row['payment_date']."'";
                        }
                       $released='released';
                       $insert_query = sprintf("INSERT INTO `membership_payment`(`company_id`,`membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `cc_id`, `cc_name`, `created_dt`, `last_updt_dt`) values ('%s','%s','%s','%s','%s','%s',%s,'FR','%s','%s','%s','%s')",
                              mysqli_escape_string($this->db, $row['company_id']),mysqli_escape_string($this->db, $row['membership_registration_id']), mysqli_escape_string($this->db, $row['checkout_id']), $released, 
                              mysqli_escape_string($this->db, $row['payment_amount']),mysqli_escape_string($this->db, $row['processing_fee']),$payment_date ,mysqli_escape_string($this->db, $row['cc_id']), mysqli_escape_string($this->db, $row['cc_name']),
                              mysqli_escape_string($this->db, $row['created_dt']),mysqli_escape_string($this->db, $new_payment_date)); 
                       $insert_result = mysqli_query($this->db, $insert_query);
                            if(!$insert_result){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                }
            }else{
                 $error_log = array('status' => "Failed", "msg" => "there is no need for update.", "query" => "$query");
                 log_info($this->json($error_log));
                 exit();
            }
        }

    }
    
    private function cloneMembership(){
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['new_company_id']) && !empty($this->_request['new_company_id'])) {
            $new_company_id = $this->_request['new_company_id'];
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }
        if (isset($this->_request['membership_id']) && !empty($this->_request['membership_id'])) {
            $query1 = sprintf("select * from membership where company_id='%s' and membership_id='%s' and deleted_flg!='D'",mysqli_escape_string($this->db,$company_id),mysqli_escape_string($this->db,$this->_request['membership_id']));
        }else{
            $query1 = sprintf("select * from membership where company_id='%s' and deleted_flg!='D'",mysqli_escape_string($this->db,$company_id));
        }
        
        $result1 = mysqli_query($this->db, $query1);
        if($result1){
            $num_rows = mysqli_num_rows($result1);
            if($num_rows>0){
                while($row = mysqli_fetch_assoc($result1)){
                    $membership_id = $row['membership_id'];
                    $this->copyMembershipDetails($company_id, $membership_id, '', $new_company_id);
                }
            }
        }
    }
    
    //Copy membership details
    private function copyMembershipDetails($company_id, $membership_id, $membership_template_flag, $new_company_id){
        $num_of_rows1 = $num_of_rows2 = $num_of_rows3 = $num_of_rows_exclude = $num_of_rows_discount = 0;
        if ($membership_template_flag == "Y") {
            $comp_id = "6";
        } else {
            $comp_id = $company_id;
        }

        $sql_membership_category = sprintf("SELECT * FROM `membership` WHERE `company_id`='%s' AND `membership_id`='%s' AND `deleted_flg`!='D'", mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result1 = mysqli_query($this->db, $sql_membership_category);

        if (!$result1) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_category");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows1 = mysqli_num_rows($result1);
            if ($num_of_rows1 > 0) {
                $output1 = mysqli_fetch_assoc($result1);
            }else{
                $error = array('status' => "Failed", "msg" => "Membership details doesn't exist.");
                $this->response($this->json($error), 401);
            }
        }

        $sql_membership_rank = sprintf("SELECT * FROM `membership_ranks` WHERE `company_id`='%s' AND `membership_id`='%s' AND `rank_deleted_flg`!='D' ORDER BY `rank_order`", 
                mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result2 = mysqli_query($this->db, $sql_membership_rank);

        if (!$result2) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_rank");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows2 = mysqli_num_rows($result2);
            if ($num_of_rows2 > 0) {
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    $output2[] = $row2;
                }
            }
        }

        $sql_membership_options = sprintf("SELECT  * FROM `membership_options` WHERE `company_id`='%s' AND `membership_id`='%s' AND `deleted_flg`!='D' ORDER BY `option_sort_order`", 
                mysqli_real_escape_string($this->db, $comp_id), mysqli_real_escape_string($this->db, $membership_id));
        $result3 = mysqli_query($this->db, $sql_membership_options);

        if (!$result3) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_membership_options");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows3 = mysqli_num_rows($result3);
            if ($num_of_rows3 > 0) {
                while ($row3 = mysqli_fetch_assoc($result3)) {
                    $output3[] = $row3;
                }
            }
        }
        
        $new_membership_id = 0;
        if($num_of_rows1>0){
            $sql1 = sprintf("INSERT INTO `membership` (`company_id`, `category_status`, `category_title`, `category_subtitle`, `category_image_url`, `category_video_url`, `category_description`, `category_sort_order`,`waiver_policies`, 
                    `membership_registration_column_1`, `membership_reg_col_1_mandatory_flag`, `membership_registration_column_2`, `membership_reg_col_2_mandatory_flag`, `membership_registration_column_3`, `membership_reg_col_3_mandatory_flag`, 
                    `membership_registration_column_4`, `membership_reg_col_4_mandatory_flag`, `membership_registration_column_5`, `membership_reg_col_5_mandatory_flag`, `membership_registration_column_6`, `membership_reg_col_6_mandatory_flag`, 
                    `membership_registration_column_7`, `membership_reg_col_7_mandatory_flag`, `membership_registration_column_8`, `membership_reg_col_8_mandatory_flag`, `membership_registration_column_9`, `membership_reg_col_9_mandatory_flag`, 
                    `membership_registration_column_10`, `membership_reg_col_10_mandatory_flag` )
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', NextVal('membership_sort_order_seq'), '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", mysqli_real_escape_string($this->db, $new_company_id),
                    mysqli_real_escape_string($this->db, $output1['category_status']),
                    mysqli_real_escape_string($this->db, $output1['category_title']),
                    mysqli_real_escape_string($this->db, $output1['category_subtitle']),
                    mysqli_real_escape_string($this->db, $output1['category_image_url']),
                    mysqli_real_escape_string($this->db, $output1['category_video_url']),
                    mysqli_real_escape_string($this->db, $output1['category_description']),
                    mysqli_real_escape_string($this->db, $output1['waiver_policies']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_1']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_1_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_2']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_2_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_3']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_3_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_4']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_4_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_5']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_5_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_6']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_6_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_7']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_7_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_8']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_8_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_9']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_9_mandatory_flag']), 
                    mysqli_real_escape_string($this->db, $output1['membership_registration_column_10']), 
                    mysqli_real_escape_string($this->db, $output1['membership_reg_col_10_mandatory_flag']));
            $result4 = mysqli_query($this->db, $sql1);

            if (!$result4) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            } else {
                $new_membership_id = mysqli_insert_id($this->db);
                
                $get_studio_code = sprintf("SELECT `company_code` FROM `company` WHERE `company_id`='%s'", mysqli_real_escape_string($this->db, $new_company_id));
                $result_studio_code = mysqli_query($this->db, $get_studio_code);
                if (!$result_studio_code) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_studio_code");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $output_value = mysqli_fetch_assoc($result_studio_code);
                    $company_code = $output_value['company_code'];
                }
                $company_code_new = $this->clean($company_code);
                $category_url = "https://www.mystudio.academy/m/?=$company_code_new/$new_company_id/$new_membership_id";

                $sql_url_update = sprintf("UPDATE `membership` SET `membership_category_url` = '$category_url' WHERE `membership_id` = '%s' ", mysqli_real_escape_string($this->db, $new_membership_id));
                $result_url_update = mysqli_query($this->db, $sql_url_update);
                if (!$result_url_update) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_url_update");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }
            }
        }

        if($new_membership_id>0){
            if($num_of_rows2>0){
                for($j=0;$j<$num_of_rows2;$j++){
                    $sql2 = sprintf("INSERT INTO `membership_ranks` (`company_id`, `membership_id`,`rank_name`,`rank_order`) VALUES ('%s', '%s', '%s', NextVal('membership_rank_sort_order_seq'))", 
                            mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $output2[$j]['rank_name']));
                    $result5 = mysqli_query($this->db, $sql2);
                    
                    if (!$result5) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }
            
            if ($num_of_rows3 > 0) { //ravi
                for($i=0;$i<$num_of_rows3;$i++){
                    $output_exclude = $output_discount = [];
                    $exclude_query = sprintf("SELECT * FROM `membership_billing_exclude` WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s' AND `deleted_flag`!='D' order by `membership_billing_exclude_id`",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$i]['membership_id']), mysqli_real_escape_string($this->db, $output3[$i]['membership_option_id']));
                    $result_exclude = mysqli_query($this->db, $exclude_query);
                    if (!$result_exclude) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$exclude_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows_exclude = mysqli_num_rows($result_exclude);
                        if ($num_of_rows_exclude > 0) {
                            while ($exclude_row = mysqli_fetch_assoc($result_exclude)) {
                                $output_exclude[] = $exclude_row;
                            }
                        }
                    }
                    $discount_query = sprintf("SELECT * FROM `membership_discount` WHERE `company_id`='%s' AND `membership_id`='%s' AND `membership_option_id`='%s' AND `is_expired`!='Y'  order by `membership_discount_id`",
                            mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $output3[$i]['membership_id']), mysqli_real_escape_string($this->db, $output3[$i]['membership_option_id']));
                    $result_discount = mysqli_query($this->db, $discount_query);
                    if (!$result_discount) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_query");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $num_of_rows_discount = mysqli_num_rows($result_discount);
                        if ($num_of_rows_discount > 0) {
                            while ($discount_row = mysqli_fetch_assoc($result_discount)) {
                                $output_discount[] = $discount_row;
                            }
                        }
                    }
                    $sql3 = sprintf("INSERT INTO `membership_options` (`company_id`, `membership_id`,`membership_title`,`membership_subtitle`,`membership_description`,
                            `membership_structure`, `membership_processing_fee_type`, `membership_signup_fee`, `membership_fee`, `show_in_app_flg`, `membership_recurring_frequency`, `custom_recurring_frequency_period_type`, 
                            `custom_recurring_frequency_period_val`, `prorate_first_payment_flg`, `delay_recurring_payment_start_flg`, `delayed_recurring_payment_type`, `delayed_recurring_payment_val`, `initial_payment_include_membership_fee`,
                            `billing_options`, `no_of_classes`, `expiration_date_type`, `expiration_date_val`, `deposit_amount`, `no_of_payments`, `billing_payment_start_date_type`,`specific_start_date`,`specific_end_date`,`specific_payment_frequency`,`week_days_order`,`billing_days`,`exclude_from_billing_flag`, `option_sort_order`)
                             VALUES ('%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',                     
                             NextVal('membership_option_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id),
                            mysqli_real_escape_string($this->db, $new_membership_id),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_title']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_subtitle']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_description']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_structure']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_processing_fee_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_signup_fee']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_fee']), 
                            mysqli_real_escape_string($this->db, $output3[$i]['show_in_app_flg']),
                            mysqli_real_escape_string($this->db, $output3[$i]['membership_recurring_frequency']),
                            mysqli_real_escape_string($this->db, $output3[$i]['custom_recurring_frequency_period_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['custom_recurring_frequency_period_val']),
                            mysqli_real_escape_string($this->db, $output3[$i]['prorate_first_payment_flg']),
                            mysqli_real_escape_string($this->db, $output3[$i]['delay_recurring_payment_start_flg']),
                            mysqli_real_escape_string($this->db, $output3[$i]['delayed_recurring_payment_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['delayed_recurring_payment_val']),
                            mysqli_real_escape_string($this->db, $output3[$i]['initial_payment_include_membership_fee']),
                            mysqli_real_escape_string($this->db, $output3[$i]['billing_options']),
                            mysqli_real_escape_string($this->db, $output3[$i]['no_of_classes']),
                            mysqli_real_escape_string($this->db, $output3[$i]['expiration_date_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['expiration_date_val']),
                            mysqli_real_escape_string($this->db, $output3[$i]['deposit_amount']),
                            mysqli_real_escape_string($this->db, $output3[$i]['no_of_payments']),
                            mysqli_real_escape_string($this->db, $output3[$i]['billing_payment_start_date_type']),
                            mysqli_real_escape_string($this->db, $output3[$i]['specific_start_date']),
                            mysqli_real_escape_string($this->db, $output3[$i]['specific_end_date']),
                            mysqli_real_escape_string($this->db, $output3[$i]['specific_payment_frequency']),
                            mysqli_real_escape_string($this->db, $output3[$i]['week_days_order']),
                            mysqli_real_escape_string($this->db, $output3[$i]['billing_days']),
                            mysqli_real_escape_string($this->db, $output3[$i]['exclude_from_billing_flag']));
                    $result6 = mysqli_query($this->db, $sql3);

                    if (!$result6) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    } else {
                        $current_option_id = mysqli_insert_id($this->db);
                        
                        $option_url = "https://www.mystudio.academy/m/?=$company_code_new/$new_company_id/$new_membership_id/$current_option_id";
                        $sql_url_update2 = sprintf("UPDATE `membership_options` SET `membership_option_url` = '$option_url' WHERE `membership_option_id` = '%s'", mysqli_real_escape_string($this->db, $current_option_id));
                        $result_url_update2 = mysqli_query($this->db, $sql_url_update2);
                        if (!$result_url_update2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_url_update2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        if ($num_of_rows_exclude > 0) {
                            for ($j = 0; $j < $num_of_rows_exclude; $j++) {
                                $exclude_query1 = sprintf("INSERT INTO `membership_billing_exclude`(`company_id`, `membership_id`, `membership_option_id`, `billing_exclude_startdate`, `billing_exclude_enddate`, `deleted_flag`) VALUES ('%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id), mysqli_real_escape_string($this->db, $current_option_id), 
                                        mysqli_real_escape_string($this->db, $output_exclude[$j]['billing_exclude_startdate']), mysqli_real_escape_string($this->db, $output_exclude[$j]['billing_exclude_enddate']),
                                        mysqli_real_escape_string($this->db, $output_exclude[$j]['deleted_flag']));
                                $result_exclude1 = mysqli_query($this->db, $exclude_query1);
                                if (!$result_exclude1) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$exclude_query1");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                        if ($num_of_rows_discount > 0) {
                            for ($j = 0; $j < $num_of_rows_discount; $j++) {
                                $discount_query1 = sprintf("INSERT INTO `membership_discount`(`company_id`, `membership_id`, `membership_option_id`,`discount_off`,`discount_type`,`discount_code`,`discount_amount`,`is_expired`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')",
                                        mysqli_real_escape_string($this->db, $new_company_id), mysqli_real_escape_string($this->db, $new_membership_id),
                                        mysqli_real_escape_string($this->db, $current_option_id), mysqli_real_escape_string($this->db, $output_discount[$j]['discount_off']),
                                        mysqli_real_escape_string($this->db, $output_discount[$j]['discount_type']), mysqli_real_escape_string($this->db, $output_discount[$j]['discount_code']),
                                        mysqli_real_escape_string($this->db, $output_discount[$j]['discount_amount']), mysqli_real_escape_string($this->db, $output_discount[$j]['is_expired']));
                                $result_discount1 = mysqli_query($this->db, $discount_query1);

                                if (!$result_discount1) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$discount_query1");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                }
            }
        }
//        $this->getAllMembershipDetails($company_id, 0);
    }
     private function cloneCurriculam(){//ravi
            if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['new_company_id'])) {
            $new_company_id = $this->_request['new_company_id'];
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }
           if (isset($this->_request['user_id'])) {
            $user_id = $this->_request['user_id'];
        }
        if (isset($this->_request['new_user_id'])) {
            $new_user_id = $this->_request['new_user_id'];
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }
        $query1 = sprintf("select * from `curriculum` where `company_id`='%s' and `created_user_id`='%s'",mysqli_escape_string($this->db,$company_id),mysqli_escape_string($this->db,$user_id));
        $result_query = mysqli_query($this->db,$query1);
        if(!$result_query){
          echo "internal servor errrrorr";
        }else{
              $numrows = mysqli_num_rows($result_query);
            if($numrows > 0 ){
                $initial_check = 0;
                while($rows = mysqli_fetch_assoc($result_query)){
//                    echo"table 1"."<br>";
                    $curriculam_id = $rows['curriculum_id'] ;
                    $cur_name = $rows['curr_name'];
                    $begin_dt = $rows['begin_dt'];
                    $end_dt = $rows['end_dt'];
                    $this->clonecuriculamdetail($company_id,$new_company_id,$user_id,$new_user_id,$curriculam_id,$cur_name,$begin_dt,$end_dt,$initial_check);
                       $initial_check++;
                }
            }
        }
     }
     private function clonecuriculamdetail($company_id,$new_company_id,$user_id,$new_user_id,$curriculam_id,$cur_name,$begin_dt,$end_dt,$initial_check){
         static $new_content_id=[];
         static $k=0;
         static $result2=true;
//         echo"table2"."k count $k"."<br>";
         if((is_null($begin_dt) || $begin_dt=='0000-00-00') && (is_null($end_dt) || $end_dt=='0000-00-00')){
             $begin ="null"; 
             $end ="null";    
         }else{
            $begin = "$begin_dt";
            $end = "$end_dt";   
         }
         $sql = sprintf("INSERT INTO `curriculum`(`company_id`, `curr_name`,`begin_dt`,`end_dt`, `created_user_id`) VALUES ('%s','%s',%s,%s,'%s')",
                 mysqli_real_escape_string($this->db,$new_company_id), mysqli_real_escape_string($this->db,$cur_name),$begin,$end,
                 mysqli_real_escape_string($this->db,$new_user_id));
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error1.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error1.");
            $this->response($this->json($error), 500);
        } else {
            $curr_id = mysqli_insert_id($this->db);
            log_info("new curriculam id  $curr_id");
//            echo"$curr_id curr_id"."<br>";
            $sql_sort_order = sprintf("SELECT `curriculum_id` FROM `curriculum` WHERE company_id='%s'", $company_id);
            $result_sort_order = mysqli_query($this->db, $sql_sort_order);
            if (!$result_sort_order) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error2.", "query" => "$sql_sort_order");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error2.");
                $this->response($this->json($error), 500);
            } else {
                $topic_srt_ord = 0;
                $num_of_rows = mysqli_num_rows($result_sort_order);
                if ($num_of_rows >= 0) {
                    $topic_srt_ord = $num_of_rows + 1;
                }
            }
            $cur_sql = sprintf("SELECT * FROM `curriculum_detail` WHERE `curriculum_id`='%s'",$curriculam_id);
            $cur_result = mysqli_query($this->db, $cur_sql);
            if(!$cur_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error3.", "query" => "$cur_sql");
                log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error3.");
                $this->response($this->json($error), 500);
            } else {
                $nowrows1 = mysqli_num_rows($cur_result);
                if ($nowrows1 > 0) {
                    while ($rows1 = mysqli_fetch_assoc($cur_result)) {
                        if((is_null($rows1['topic_img_url']) || $rows1['topic_img_url']=='') && (is_null($rows1['topic_img_content']) || $rows1['topic_img_content']=='')){
                          $img_url ="null"; 
                          $img_content ="null";     
                        }else{
                          $temp_url  = $rows1['topic_img_url'];
                          $img_url = "$temp_url";
                          $temp_content = $rows1['topic_img_content'];
                          $img_content = "$temp_content";
                        }
                        $sql1 = sprintf("INSERT INTO `curriculum_detail` (`curriculum_id`,`topic_srt_ord`,`topic_name`,`topic_img_url`,`topic_img_content`) VALUES ('%s','%s','%s',%s,%s)", $curr_id, $topic_srt_ord, mysqli_real_escape_string($this->db, $rows1['topic_name']), mysqli_real_escape_string($this->db, $img_url), mysqli_real_escape_string($this->db, $img_content));
                        $result1 = mysqli_query($this->db, $sql1);
                        if (!$result1) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error4.", "query" => "$sql1");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error4.");
                            $this->response($this->json($error), 500);
                        } else {
                            $curr_detailid = mysqli_insert_id($this->db);
                            log_info("new curriculam detailid  $curr_detailid");
//                            echo"new curriculam detailid  $curr_detailid"."<br>";
                            $content_sql = sprintf("select * from `content_definition` where `creator_company_id`='%s' and `created_user_id`='%s'", mysqli_escape_string($this->db, $company_id), mysqli_escape_string($this->db, $user_id));
                            $content_result = mysqli_query($this->db, $content_sql);
                            if (!$content_result) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error5.", "query" => "$content_sql");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error5.");
                                $this->response($this->json($error), 500);
                            } else {
                                $numrows2 = mysqli_num_rows($content_result);
                                if($numrows2 > 0) {
                                    while($rows2 = mysqli_fetch_assoc($content_result)){
                                        if($initial_check == 0){
                                        $sql2 = sprintf("INSERT INTO `content_definition`(`content_title`, `content_description`, `ContentVideoUrl`, `video_type`, `video_id`, `created_user_id`, `creator_company_id`) VALUES ('%s','%s','%s','%s','%s','%s','%s')",
                                                mysqli_real_escape_string($this->db, $rows2['content_title']),  mysqli_real_escape_string($this->db, $rows2['content_description']), mysqli_real_escape_string($this->db, $rows2['ContentVideoUrl']), mysqli_real_escape_string($this->db, $rows2['video_type']),
                                                mysqli_real_escape_string($this->db, $rows2['video_id']),mysqli_real_escape_string($this->db, $new_user_id),mysqli_real_escape_string($this->db, $new_company_id));
                                         $result2 = mysqli_query($this->db, $sql2);
                                        }
                                        if (!$result2) {
                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error6.", "query" => "$sql2");
                                            log_info($this->json($error_log));
                                            $error = array('status' => "Failed", "msg" => "Internal Server Error6.");
                                            $this->response($this->json($error), 500);
                                        }else{
                                           if($initial_check == 0){
                                            $content_id = mysqli_insert_id($this->db);
                                            $new_content_id[] = $content_id;
                                           }
//                                            echo json_encode($new_content_id)."array"."<br>";
                                            $cur_content_sql = sprintf("select * from `curriculum_content` where curriculum_id='%s' and curriculum_det_id='%s' and contentId='%s'", mysqli_escape_string($this->db, $curriculam_id), mysqli_escape_string($this->db, $rows1['curr_detail_id']),mysqli_escape_string($this->db, $rows2['content_id']));
                                            $cur_content_result = mysqli_query($this->db, $cur_content_sql);
                                            if(!$cur_content_result) {
                                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error7.", "query" => "$cur_content_sql");
                                                log_info($this->json($error_log));
                                                $error = array('status' => "Failed", "msg" => "Internal Server Error7.");
                                                $this->response($this->json($error), 500);
                                            }else{
                                                $numrows3 = mysqli_num_rows($cur_content_result);
                                                if($numrows3 > 0){
                                                     while($rows3 = mysqli_fetch_assoc($cur_content_result)){
                                                     $sql3 = sprintf("INSERT INTO `curriculum_content`(`curriculum_id`, `curriculum_det_id`, `content_srt_ord`, `contentId`) VALUES ('%s','%s','%s','%s')", mysqli_escape_string($this->db, $curr_id), mysqli_escape_string($this->db, $curr_detailid), mysqli_escape_string($this->db, $rows3['content_srt_ord']), mysqli_escape_string($this->db, $new_content_id[$k]));
                                                     $result3 = mysqli_query($this->db, $sql3);
                                                          if (!$result3) {
                                                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error8.", "query" => "$sql3");
                                                            log_info($this->json($error_log));
                                                            $error = array('status' => "Failed", "msg" => "Internal Server Error8.");
                                                            $this->response($this->json($error), 500);
                                                        }else{
                                                            $k++;
                                                        } 
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
                                                 
     }
       private function cloneEvents(){//work
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['new_company_id']) && !empty($this->_request['new_company_id'])) {
            $new_company_id = $this->_request['new_company_id'];
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }
        if (isset($this->_request['event_id']) && !empty($this->_request['event_id'])) {
            $query = sprintf("select * from event where company_id='%s' and event_id='%s' and event_status!='D' order by event_id",mysqli_escape_string($this->db,$company_id),mysqli_escape_string($this->db,$this->_request['event_id']));
        }else{
            $query = sprintf("select * from event where company_id='%s' and event_status!='D' order by event_id",mysqli_escape_string($this->db,$company_id));
        }
        $result = mysqli_query($this->db,$query);
        if(!$result){
            echo "internal servor errrror";
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows > 0){
                while($rows = mysqli_fetch_assoc($result)){
                    $event_id = $rows['event_id'];
                    $type_check = $rows['event_type'];
                    if($type_check !== 'C'){
                    $this->cloneEventDetails($company_id,$new_company_id,$event_id);
                    }
                }
            }
        }
    }
    
    public function cloneEventDetails($company_id,$new_company_id,$event_id) {
        $parent_id = $company_code = $last_inserted_event_id = "";
      
        $sql_company_code = sprintf("SELECT  `company_code` FROM  `company` WHERE  `company_id` = '%s'", mysqli_real_escape_string($this->db, $new_company_id));
        $result_company_code = mysqli_query($this->db, $sql_company_code);
        if (!$result_company_code) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_company_code");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $output_value = mysqli_fetch_assoc($result_company_code);
            $company_code = $output_value['company_code'];
        }
        
        $sql = sprintf("SELECT `parent_id`, `event_type`, `event_status`, `event_title`, 
            `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
            `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
            `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, `number_of_payments`, 
            `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, `event_banner_img_content`, 
            `event_banner_img_url`, `event_registration_column_1`, `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, 
            `event_reg_col_2_mandatory_flag`, `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, 
            `event_registration_column_4`, `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, 
            `event_reg_col_5_mandatory_flag`, `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, 
            `event_registration_column_7`, `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, 
            `event_reg_col_8_mandatory_flag`, `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, 
            `event_registration_column_10`, `event_reg_col_10_mandatory_flag` FROM `event` WHERE `company_id`='%s' AND 
            (`event_id`='%s' OR `parent_id`='%s') AND `event_status`!='D' order by event_type desc", $company_id, $event_id, $event_id);
        $result = mysqli_query($this->db, $sql);

        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $output[] = $row;
                }
                
                if (empty(trim($output[0]['parent_id']))) {
                    $parent_id = 'null';
                    if(is_null($output[0]['event_capacity'])){
                                $capacity = 'NULL';
                            }else{
                                $capacity = $output[0]['event_capacity'];
                            }
                    $sql1 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                        `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                        `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                        `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                        `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                        `event_banner_img_content`, `event_banner_img_url`, `event_registration_column_1`, 
                        `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                        `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                        `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                        `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                        `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                        `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                        `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s',%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                        '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                        NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), 
                            mysqli_real_escape_string($this->db, $parent_id), 
                            mysqli_real_escape_string($this->db, $output[0]['event_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_status']),
                            mysqli_real_escape_string($this->db, $output[0]['event_title']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_category_subtitle']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_desc']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_more_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_video_detail_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_cost']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_compare_price']), 
                            mysqli_real_escape_string($this->db, $capacity), 
                            mysqli_real_escape_string($this->db, $output[0]['capacity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['processing_fees']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_onetime_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_recurring_payment_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_order_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['total_quantity_text']), 
                            mysqli_real_escape_string($this->db, $output[0]['deposit_amount']), 
                            mysqli_real_escape_string($this->db, $output[0]['number_of_payments']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate_type']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_startdate']), 
                            mysqli_real_escape_string($this->db, $output[0]['payment_frequency']), 
                            mysqli_real_escape_string($this->db, $output[0]['waiver_policies']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_banner_img_content']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_banner_img_url']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_1']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_1_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_2']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_2_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_3']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_3_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_4']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_4_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_5']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_5_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_6']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_6_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_7']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_7_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_8']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_8_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_9']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_9_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_registration_column_10']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_reg_col_10_mandatory_flag']), 
                            mysqli_real_escape_string($this->db, $output[0]['event_banner_img_url']));
                }
                $result1 = mysqli_query($this->db, $sql1);
                 if (!$result1) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                } else {
                    $last_inserted_event_id = mysqli_insert_id($this->db);
                    if($output[0]['event_type'] == 'M' || $output[0]['event_type'] == 'S'){
                        $company_code_new = $this->clean($company_code);
                        $event_url = "https://www.mystudio.academy/e/?=$company_code_new/$new_company_id/$last_inserted_event_id";

                        $sql_update_url = sprintf("UPDATE `event` SET `event_url` = '$event_url' WHERE `event_id` = '%s' ", mysqli_real_escape_string($this->db, $last_inserted_event_id));
                        $result_update_url = mysqli_query($this->db, $sql_update_url);
                        if (!$result_update_url) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update_url");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                    }
                    if ($num_of_rows > 1) {
                        
                        for ($i = 1; $i < count($output); $i++) {
                            if(is_null($output[$i]['event_capacity'])){
                                $capacity = 'NULL';
                            }else{
                                $capacity = $output[$i]['event_capacity'];
                            }
                            $sql2 = sprintf("INSERT INTO `event` (`company_id`, `parent_id`, `event_type`, `event_status`, `event_title`, 
                                `event_category_subtitle`, `event_desc`, `event_more_detail_url`,  `event_video_detail_url`, `event_cost`, 
                                `event_compare_price`, `event_capacity`, `capacity_text`, `processing_fees`, `event_onetime_payment_flag`, 
                                `event_recurring_payment_flag`, `total_order_text`, `total_quantity_text`, `deposit_amount`, 
                                `number_of_payments`, `payment_startdate_type`, `payment_startdate`, `payment_frequency`, `waiver_policies`, 
                                `event_banner_img_content`, `event_banner_img_url`, `event_registration_column_1`, 
                                `event_reg_col_1_mandatory_flag`, `event_registration_column_2`, `event_reg_col_2_mandatory_flag`, 
                                `event_registration_column_3`, `event_reg_col_3_mandatory_flag`, `event_registration_column_4`, 
                                `event_reg_col_4_mandatory_flag`, `event_registration_column_5`, `event_reg_col_5_mandatory_flag`, 
                                `event_registration_column_6`, `event_reg_col_6_mandatory_flag`, `event_registration_column_7`, 
                                `event_reg_col_7_mandatory_flag`, `event_registration_column_8`, `event_reg_col_8_mandatory_flag`, 
                                `event_registration_column_9`, `event_reg_col_9_mandatory_flag`, `event_registration_column_10`, 
                                `event_reg_col_10_mandatory_flag`, `event_sort_order`) VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', 
                                NextVal('event_sort_order_seq'))", mysqli_real_escape_string($this->db, $new_company_id), 
                                mysqli_real_escape_string($this->db, $last_inserted_event_id), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_type']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_status']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_title']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_category_subtitle']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_desc']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_more_detail_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_video_detail_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_cost']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_compare_price']), 
                                mysqli_real_escape_string($this->db, $capacity), 
                                mysqli_real_escape_string($this->db, $output[$i]['capacity_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['processing_fees']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_onetime_payment_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_recurring_payment_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['total_order_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['total_quantity_text']), 
                                mysqli_real_escape_string($this->db, $output[$i]['deposit_amount']), 
                                mysqli_real_escape_string($this->db, $output[$i]['number_of_payments']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_startdate_type']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_startdate']), 
                                mysqli_real_escape_string($this->db, $output[$i]['payment_frequency']), 
                                mysqli_real_escape_string($this->db, $output[$i]['waiver_policies']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_content']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_url']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_1']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_1_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_2']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_2_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_3']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_3_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_4']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_4_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_5']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_5_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_6']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_6_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_7']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_7_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_8']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_8_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_9']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_9_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_registration_column_10']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_reg_col_10_mandatory_flag']), 
                                mysqli_real_escape_string($this->db, $output[$i]['event_banner_img_url']));
                            $result2 = mysqli_query($this->db, $sql2);

                            if (!$result2) {
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            } 
                        }
                    } 
                }
                if(!empty($last_inserted_event_id)){
                    $get_event_discount = sprintf("SELECT * FROM `event_discount` WHERE `company_id`='%s' AND `event_id`='%s' AND `is_expired`='N'", $company_id, $event_id);
                    $get_event_discount_result = mysqli_query($this->db, $get_event_discount);
                    if(!$get_event_discount_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$get_event_discount");
                        log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        if(mysqli_num_rows($get_event_discount_result)>0){
                            while($row_disc = mysqli_fetch_assoc($get_event_discount_result)){
                                $insert_disc_query = sprintf("INSERT INTO `event_discount`(`company_id`, `event_id`, `discount_type`, `discount_code`, `discount_amount`) VALUES ('%s', '%s', '%s', '%s', '%s')",
                                        $new_company_id, $last_inserted_event_id, $row_disc['discount_type'], $row_disc['discount_code'], $row_disc['discount_amount']);
                                $insert_disc_result = mysqli_query($this->db, $insert_disc_query);
                                if(!$insert_disc_result){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_disc_query");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }
                            }
                        }
                    }
                }
           }else {
                $error = array('status' => "Failed", "msg" => "Event details doesn't exist.");
                $this->response($this->json($error), 401); // If no records "No Content" status
            }
        }
    }
    private function updateStudentID(){
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }

            $this->updateStudentIdInRegistrationTables($company_id);
    }
    
    private function updateStudentIdInRegistrationTables($c_id){
    if (empty($c_id)|| $c_id==0){
            $company_id_new = "SELECT `company_id` from company where 1";
        }else{
            $company_id_new = $c_id;
        }
        $query1 = sprintf("SELECT er.`event_reg_id`, er.`company_id`, er.`buyer_name`, er.`buyer_email`, er.`buyer_phone`, s.`student_id` FROM `event_registration` er LEFT JOIN `student` s ON er.`buyer_email`=s.`student_email` AND er.`company_id`=s.`company_id` WHERE er.`student_id`=0 AND er.`company_id` IN (%s)",mysqli_real_escape_string($this->db,$company_id_new));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query1);
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $event_reg_id = $row1['event_reg_id'];
                    $buyer_name = $row1['buyer_name'];
                    $buyer_email = $row1['buyer_email'];
                    $buyer_phone = $row1['buyer_phone'];
                    $student_id = $row1['student_id'];
                    if(is_null($student_id)){
                        $select_student = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'",$buyer_email,$company_id);
                        $result_select_student = mysqli_query($this->db, $select_student);
                        if(!$result_select_student){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_student)>0){
                                $r = mysqli_fetch_assoc($result_select_student);
                                $student_id = $r['student_id'];
                            }else{
                                $insert_student = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_email`) VALUES('%s', '%s', '%s')",mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$buyer_name),mysqli_real_escape_string($this->db,$buyer_email));
                                $result_insert_student = mysqli_query($this->db, $insert_student);
                                if(!$result_insert_student){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student);
                                    $this->response($this->json($error), 500);
                                }else{
                                    $student_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table = sprintf("UPDATE `event_registration` SET `student_id`='%s' WHERE `event_reg_id`='%s'",$student_id,$event_reg_id);
                    $result_update_reg_table = mysqli_query($this->db, $update_reg_table);
                    if(!$result_update_reg_table){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table);
                        $this->response($this->json($error), 500);
                    } else {
                        $update_regdetails_table = sprintf("UPDATE `event_reg_details` SET `student_id`='%s' WHERE `event_reg_id`='%s'",$student_id,$event_reg_id);
                        $result_update_regdetails_table = mysqli_query($this->db, $update_regdetails_table);
                        if(!$result_update_regdetails_table){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_regdetails_table);
                            $this->response($this->json($error), 500);                           
                        
                        }
                    }
                }
            }
        }
        
        $query2 = sprintf("SELECT mr.`membership_registration_id`, mr.`company_id`, mr.`buyer_name`, mr.`buyer_email`, mr.`buyer_phone`, s.`student_id` FROM `membership_registration` mr LEFT JOIN `student` s ON mr.`buyer_email`=s.`student_email` AND mr.`company_id`=s.`company_id` WHERE mr.`student_id`=0 AND mr.`company_id`IN (%s)",mysqli_real_escape_string($this->db,$company_id_new));
        $result2 = mysqli_query($this->db, $query2);
        if(!$result2){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query2);
            $this->response($this->json($error), 500);
        }else{
            $num_rows2 = mysqli_num_rows($result2);
            if($num_rows2>0){
                while($row2 = mysqli_fetch_assoc($result2)){
                    $company_id = $row2['company_id'];
                    $mem_reg_id = $row2['membership_registration_id'];
                    $buyer_name = $row2['buyer_name'];
                    $buyer_email = $row2['buyer_email'];
                    $buyer_phone = $row2['buyer_phone'];
                    $student_id = $row2['student_id'];
                    if(is_null($student_id)){
                        $select_student2 = sprintf("SELECT * FROM `student` WHERE `student_email`='%s' AND `company_id`='%s'",$buyer_email,$company_id);
                        $result_select_student2 = mysqli_query($this->db, $select_student2);
                        if(!$result_select_student2){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_student2);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_student2)>0){
                                $r2 = mysqli_fetch_assoc($result_select_student2);
                                $student_id = $r2['student_id'];
                            }else{
                                $insert_student2 = sprintf("INSERT INTO `student`(`company_id`, `student_name`, `student_email`) VALUES('%s', '%s', '%s')",mysqli_real_escape_string($this->db,$company_id),mysqli_real_escape_string($this->db,$buyer_name),mysqli_real_escape_string($this->db,$buyer_email));
                                $result_insert_student2 = mysqli_query($this->db, $insert_student2);
                                if(!$result_insert_student2){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_student2);
                                    $this->response($this->json($error), 500);
                                }else{
                                    $student_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table2 = sprintf("UPDATE `membership_registration` SET `student_id`='%s' WHERE `membership_registration_id`='%s'",$student_id,$mem_reg_id);
                    $result_update_reg_table2 = mysqli_query($this->db, $update_reg_table2);
                    if(!$result_update_reg_table2){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table2);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }
    private function updateParticipantID() {
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        } else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }

        $this->updateParticipantIdInRegistrationTables($company_id);
    }

    private function updateParticipantIdInRegistrationTables($c_id){ 
         if (empty($c_id)|| $c_id==0){
            $company_id_new = "SELECT `company_id` from company where 1";
        }else{
            $company_id_new = $c_id;
        }
        
        $query2 = sprintf("SELECT mr.`membership_registration_id`, mr.`company_id`, mr.`student_id`, mr.`membership_registration_column_1`, mr.`membership_registration_column_2`, mr.`membership_registration_column_3`, p.`participant_id` 
                FROM `membership_registration` mr LEFT JOIN `participant` p ON mr.`company_id`=p.`company_id` AND mr.`student_id`=p.`student_id` AND 
                mr.`membership_registration_column_1`=p.`participant_first_name` AND mr.`membership_registration_column_2`=p.`participant_last_name` AND mr.`membership_registration_column_3`=p.`date_of_birth`
                WHERE mr.`participant_id`=0 AND mr.`company_id`IN(%s)",mysqli_real_escape_string($this->db,$company_id_new));
        $result2 = mysqli_query($this->db, $query2);
        if(!$result2){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query2);
            $this->response($this->json($error), 500);
        }else{
            $num_rows2 = mysqli_num_rows($result2);
            if($num_rows2>0){
                while($row2 = mysqli_fetch_assoc($result2)){
                    $company_id = $row2['company_id'];
                    $mem_reg_id = $row2['membership_registration_id'];
                    $student_id = $row2['student_id'];
                    $first_name = $row2['membership_registration_column_1'];
                    $last_name = $row2['membership_registration_column_2'];
                    $dob = $row2['membership_registration_column_3'];
                    $participant_id = $row2['participant_id'];
                    if(is_null($participant_id)){
                        $select_participant2 = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s' AND `date_of_birth`='%s'",
                        mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$student_id), mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name), mysqli_real_escape_string($this->db,$dob));
                        $result_select_participant2 = mysqli_query($this->db, $select_participant2);
                        if(!$result_select_participant2){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant2);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_participant2)>0){
                                $r = mysqli_fetch_assoc($result_select_participant2);
                                $participant_id = $r['participant_id'];
                            }else{
                                $insert_participant2 = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`, `date_of_birth`) VALUES('%s', '%s', '%s', '%s', '%s')",
                                        $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name), $dob);
                                $result_insert_participant2 = mysqli_query($this->db, $insert_participant2);
                                if(!$result_insert_participant2){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant2);
                                    log_info("insert failiure  ".$insert_participant2);
                                    continue;
//                                    $this->response($this->json($error), 500);
//                                    $select_mem_reg=sprintf("Select * from `membership_registration` where `student_id` in('%s')",$student_id);
//                                    $result_select_mem_reg = mysqli_query($this->db, $select_mem_reg);
//                                    if(!$result_select_mem_reg){
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_mem_reg);
//                                        $this->response($this->json($error), 500);
//                                    }else{
//                                        if(mysqli_num_rows($result_select_mem_reg)>0){
//                                            while($row_reg=mysqli_fetch_assoc($result_select_mem_reg)){
//                                                $mem_reg_id1=$row_reg['membership_registration_id'];
//                                                $update_oldmem_reg = sprintf("UPDATE `membership_registration` SET `student_id`=0 WHERE `membership_registration_id`='%s'",$mem_reg_id1);
//                                                $result_update_oldmem_reg = mysqli_query($this->db, $update_oldmem_reg);
//                                                if(!$result_update_oldmem_reg){
//                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_oldmem_reg);
//                                                   $this->response($this->json($error), 500);
//                                                }else{
//                                                    $this->updateStudentID();
//                                                }
//                                            }
//                                        }
//                                    }
                                }else{
                                    $participant_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table2 = sprintf("UPDATE `membership_registration` SET `participant_id`='%s' WHERE `membership_registration_id`='%s'",$participant_id,$mem_reg_id);
                    $result_update_reg_table2 = mysqli_query($this->db, $update_reg_table2);
                    if(!$result_update_reg_table2){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table2);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
        
        
        $query1 = sprintf("SELECT er.`event_reg_id`, er.`company_id`, er.`student_id`, er.`event_registration_column_1`, er.`event_registration_column_2`, p.`participant_id` 
                FROM `event_registration` er LEFT JOIN `participant` p ON er.`company_id`=p.`company_id` AND er.`student_id`=p.`student_id` AND 
                er.`event_registration_column_1`=p.`participant_first_name` AND er.`event_registration_column_2`=p.`participant_last_name`
                WHERE er.`participant_id`=0 AND er.`company_id`IN (%s)",mysqli_real_escape_string($this->db,$company_id_new));
        $result1 = mysqli_query($this->db, $query1);
        if(!$result1){
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $query1);
            $this->response($this->json($error), 500);
        }else{
            $num_rows1 = mysqli_num_rows($result1);
            if($num_rows1>0){
                while($row1 = mysqli_fetch_assoc($result1)){
                    $company_id = $row1['company_id'];
                    $event_reg_id = $row1['event_reg_id'];
                    $student_id = $row1['student_id'];
                    $first_name = $row1['event_registration_column_1'];
                    $last_name = $row1['event_registration_column_2'];
                    $participant_id = $row1['participant_id'];
                    if(is_null($participant_id)){
                        $select_participant = sprintf("SELECT * FROM `participant` WHERE `company_id`='%s' AND `student_id`='%s' AND `participant_first_name`='%s' AND `participant_last_name`='%s'",
                                $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name));
                        $result_select_participant = mysqli_query($this->db, $select_participant);
                        if(!$result_select_participant){
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_participant);
                            $this->response($this->json($error), 500);
                        }else{
                            if(mysqli_num_rows($result_select_participant)>0){
                                $r = mysqli_fetch_assoc($result_select_participant);
                                $participant_id = $r['participant_id'];
                            }else{
                                $insert_participant = sprintf("INSERT INTO `participant`(`company_id`, `student_id`, `participant_first_name`, `participant_last_name`) VALUES('%s', '%s', '%s', '%s')",
                                        $company_id, $student_id, mysqli_real_escape_string($this->db,$first_name), mysqli_real_escape_string($this->db,$last_name));
                                $result_insert_participant = mysqli_query($this->db, $insert_participant);
                                if(!$result_insert_participant){
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $insert_participant);
                                    log_info("insert failiure  ".$insert_participant);
                                    continue;
//                                    $this->response($this->json($error), 500);                                    
//                                    $select_event_reg=sprintf("Select * from `event_registration` where `student_id` in('%s')",$student_id);
//                                    $result_select_event_reg = mysqli_query($this->db, $select_event_reg);
//                                    if(!$result_select_event_reg){
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $select_event_reg);
//                                        $this->response($this->json($error), 500);
//                                    }else{
//                                        if(mysqli_num_rows($result_select_event_reg)>0){
//                                            while($row_reg=mysqli_fetch_assoc($result_select_event_reg)){
//                                                $event_reg_id1=$row_reg['event_reg_id'];
//                                                $update_oldevent_reg = sprintf("UPDATE `event_registration` SET `student_id`=0 WHERE `event_reg_id`='%s'",$event_reg_id1);
//                                                $result_update_oldevent_reg = mysqli_query($this->db, $update_oldevent_reg);
//                                                if(!$result_update_oldevent_reg){
//                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_oldevent_reg);
//                                                   $this->response($this->json($error), 500);
//                                                }else{
    //                                                $update_oldevent_reg_details = sprintf("UPDATE `event_reg_details` SET `student_id`=0 WHERE `event_reg_id`='%s'",$event_reg_id1);
    //                                                $result_update_oldevent_reg_details = mysqli_query($this->db, $update_oldevent_reg_details);
    //                                                if(!$result_update_oldevent_reg_details){
    //                                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_oldevent_reg_details);
    //                                                    $this->response($this->json($error), 500);
    //                                                }
//                                                    $this->updateStudentID();
//                                                }
//                                            }
//                                        }
//                                    }
                                }else{
                                    $participant_id = mysqli_insert_id($this->db);
                                }
                            }
                        }
                    }
                    $update_reg_table = sprintf("UPDATE `event_registration` SET `participant_id`='%s' WHERE `event_reg_id`='%s'",$participant_id,$event_reg_id);
                    $result_update_reg_table = mysqli_query($this->db, $update_reg_table);
                    if(!$result_update_reg_table){
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $update_reg_table);
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }
    }
    private function inputcomapnyForSalesAmountUpdate(){
        if (isset($this->_request['company_id'])) {
            $company_id = $this->_request['company_id'];
        }
        if (isset($this->_request['month_period'])) {
            $month_period = $this->_request['month_period'];
        }
        if (isset($this->_request['year_period'])) {
            $year_period = $this->_request['year_period'];
        }else {
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 400);
        }

            $this->loadDataForDimensionUpdate($company_id,$month_period,$year_period);
    }
    

    protected function loadDataForDimensionUpdate($c_id,$month_period,$year_period){
        if (empty($c_id)|| $c_id==0){
            $company_id_new = "SELECT `company_id` from company where 1";
        }else{
            $company_id_new = $c_id;
        }
        if (empty($month_period)|| empty($year_period)){
            log_info("inside if");
            $period_new="";
        }else{
        log_info("inside else");
            $period_new = " AND YEAR(mp.`last_updt_dt`) in($year_period) AND MONTH(mp.`last_updt_dt`) in($month_period)";
        }
        $date_format="'%Y-%m'";
        $sql_dim= sprintf("SELECT * from (SELECT  mr.`company_id`,mr.`membership_id`,mp.`last_updt_dt` period FROM `membership_payment` mp left join `membership_registration` mr on mp.`membership_registration_id`=mr.`membership_registration_id` and mr.`company_id`=mp.`company_id` where mr.`company_id`IN(%s) $period_new and `payment_status`IN ('S','R','FR') group by `membership_id`,DATE_FORMAT(mp.`last_updt_dt`,%s))t order by period", mysqli_real_escape_string($this->db,$company_id_new),$date_format);
        $result_sql_dim = mysqli_query($this->db, $sql_dim);
        log_info("select result :  ".$sql_dim);
        if (!$result_sql_dim) {
            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_dim);
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result_sql_dim);
            if($num_rows>0) {
                while($row = mysqli_fetch_assoc($result_sql_dim)){
                    $membership_id = $row['membership_id'];
                    $period = $row['period'];
                    $company_id =$row['company_id'];
                    $this->updateMembershipDimensionsNetSales($company_id, $membership_id, $period);
                }
            }
        }       
        
    }
    
//    private function updateNetSalesInDimensions() {
//        $date_format="'%Y-%m'";
//        $sql_dim = sprintf("SELECT * from (SELECT 'category' as type,sum(
//                         IF(mp.payment_status = 'R', concat('-',if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) ),
//                         if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) )         
//                         ) total_amount,
//                        mr.`membership_id`,'' membership_option_id,DATE_FORMAT(mp.`last_updt_dt`,%s) period FROM `membership_payment` mp left join `membership_registration` mr on(mp.`membership_registration_id`=mr.`membership_registration_id` and mr.`company_id`=mp.`company_id`) where mp.`company_id`=368  and `payment_status` IN ('S','R','FR')  group by `membership_id`,DATE_FORMAT(mp.`last_updt_dt`,%s),payment_status
//
//                         UNION
// 
//                          SELECT 'options' as type,sum(
//                           IF(mp.payment_status = 'R', concat('-',if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) ),
//                                 if(mr.processing_fee_type=1 ,(mp.payment_amount-mp.processing_fee),mp.payment_amount) ) 
//                          )total_amount,
//                          mr.`membership_id`,mr.`membership_option_id`,DATE_FORMAT(mp.`last_updt_dt`,%s) period FROM `membership_payment` mp left join `membership_registration` mr on(mp.`membership_registration_id`=mr.`membership_registration_id` and mr.`company_id`=mp.`company_id`) where mp.`company_id`=368  and `payment_status`IN ('S','R','FR')  group by `membership_id`,`membership_option_id`,date_format(mp.`last_updt_dt`,%s),payment_status)t order by type,period",$date_format,$date_format,$date_format,$date_format,$date_format,$date_format);
//        $result_sql_dim = mysqli_query($this->db, $sql_dim);
//        if (!$result_sql_dim) {
//            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_dim);
//            $this->response($this->json($error), 500);
//        }else{
//            $num_rows = mysqli_num_rows($result_sql_dim);
//            $output[]='';
//            if($num_rows>0) {
//                while($row = mysqli_fetch_assoc($result_sql_dim)){
////                    $output[] = $row;
////                }
////                for ($idx = 0; $idx < count($output); $idx++) {
////                    $obj = (Array) $output[$idx];
//                    $type = $row['type'];
//                    $total_amount = $row['total_amount'];
//                    $membership_id = $row['membership_id'];
//                    $membership_option_id = $row['membership_option_id'];
//                    $period = $row['period'];
//                    if($type=="category"){
//                        $sql_cat_select=sprintf("SELECT * FROM `membership_dimensions` WHERE `category_id`='%s' AND `period`='%s'",mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db,$period));
//                        $result_sql_cat_select = mysqli_query($this->db, $sql_cat_select);
//                        if (!$result_sql_cat_select) {
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_cat_select);
//                            log_info($this->json($error));
//                            $this->response($this->json($error), 500);
//                        } else {
//                            $num_rows_select_dim = mysqli_num_rows($result_sql_cat_select);
//                            if ($num_rows_select_dim > 0) {
//                                $row_d1 = mysqli_fetch_assoc($result_sql_cat_select);
//                                $dim_total=$row_d1['sales_amount'];
////                                if($total_amount!=$dim_total){
//                                    $sql_update_cat_dim = sprintf("UPDATE `membership_dimensions` SET `sales_amount`=`sales_amount`+'%s' WHERE `category_id`='%s' AND `period`='%s' and `option_id`=0", mysqli_real_escape_string($this->db, $total_amount), mysqli_real_escape_string($this->db, $membership_id), mysqli_real_escape_string($this->db, $period));
//                                    $result_sql_update_cat_dim = mysqli_query($this->db, $sql_update_cat_dim);
//                                    if (!$result_sql_update_cat_dim) {
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_update_cat_dim);
//                                        log_info($this->json($error));
//                                        $this->response($this->json($error), 500);
//                                    } else {
//                                        $num_affected_rows = mysqli_affected_rows($this->db);
//                                        if ($num_affected_rows > 0) {
//                                            log_info("category id::  " . $membership_id . "   period::  " . $period . " updated");
//                }
//                                    }
////                                }
//                            }
//                        }
//                    } else if($type=="options") {
//                        $sql_opt_select=sprintf("SELECT * FROM `membership_dimensions` WHERE `category_id`='%s' AND `option_id`='%s' AND `period`='%s'",mysqli_real_escape_string($this->db,$membership_id),mysqli_real_escape_string($this->db,$membership_option_id),mysqli_real_escape_string($this->db,$period));
//                        $result_sql_opt_select = mysqli_query($this->db, $sql_opt_select);
//                        if (!$result_sql_opt_select) {
//                            $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_opt_select);
//                            log_info($this->json($error));
//                            $this->response($this->json($error), 500);
//                        } else {
//                            $num_rows_select_dim = mysqli_num_rows($result_sql_opt_select);
//                            if ($num_rows_select_dim > 0) {
//                                $row_d1 = mysqli_fetch_assoc($result_sql_opt_select);
//                                $dim_total=$row_d1['sales_amount'];
//                                if($total_amount!=$dim_total){
//                                    $sql_update_opt_dim = sprintf("UPDATE `membership_dimensions` SET `sales_amount`='%s' WHERE `category_id`='%s' AND `option_id`='%s' AND `period`='%s'", mysqli_real_escape_string($this->db, $total_amount), mysqli_real_escape_string($this->db, $membership_id),mysqli_real_escape_string($this->db,$membership_option_id), mysqli_real_escape_string($this->db, $period));
//                                    $result_sql_update_opt_dim = mysqli_query($this->db, $sql_update_opt_dim);
//                                    if (!$result_sql_update_opt_dim) {
//                                        $error = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => $sql_update_opt_dim);
//                                        log_info($this->json($error));
//                                        $this->response($this->json($error), 500);
//                                    } else {
//                                        $num_affected_rows = mysqli_affected_rows($this->db);
//                                        if ($num_affected_rows > 0) {
//                                            log_info("option id::  " . $membership_id . "   period::  " . $period . " updated");
//                                }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    private function gettrialCheckoutDetails($checkout_id){
        $query = sprintf("SELECT tr.`company_id`, tr.`trial_id`, tr.`trial_reg_id`, t.trial_title, tp.`trial_payment_id`, tp.`checkout_id`, tp.`checkout_status`, `access_token`, tp.`payment_amount`, tp.`processing_fee`, tr.`processing_fee_type`
                    FROM `trial_registration` tr 
                    LEFT JOIN `trial_payment` tp ON tr.`trial_reg_id`=tp.`trial_reg_id`
                    LEFT JOIN `trial` t ON t.`trial_id`=tr.`trial_id`
                    LEFT JOIN `wp_account` wp ON tr.`company_id` = wp.`company_id` 
                    WHERE tp.`checkout_id` = '%s' ORDER BY tp.`trial_payment_id` LIMIT 0,1", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $row = mysqli_fetch_assoc($result);
                $trial_reg_id = $row['trial_reg_id'];
                $access_token = $row['access_token'];
                $payment_id = $row['trial_payment_id'];
                $fee = $row['processing_fee'];
                $processing_fee_type = $row['processing_fee_type'];
                $payment_amount = $row['payment_amount'];
                $company_id = $row['company_id'];
                $trial_id = $row['trial_id'];
                $trial_title = $desc = $row['trial_title'];
                   
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    
                    if($checkout_state=='released'){
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `trial_registration` SET `paid_amount`=`paid_amount`+$paid_amount WHERE `trial_reg_id`='%s'", mysqli_real_escape_string($this->db, $trial_reg_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        
                        $update_count = sprintf("UPDATE `trial` SET `net_sales`=`net_sales`+$paid_amount WHERE `company_id`='%s' AND `trial_id`='%s'",
                                mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id));
                        $result_count = mysqli_query($this->db, $update_count);
                        
                        if(!$result_count){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                     }
                    $update_query = sprintf("UPDATE `trial_payment` SET `checkout_status`='%s' $payment_status WHERE `trial_reg_id`='%s' and `trial_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $trial_reg_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                           $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                            $this->updateTrialDimensionsNetSales($company_id,$trial_id,'');
                        }
                    }
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    private function updateMobilePushCertificate() {
        $query = sprintf("SELECT * from `mobile_push_certificate` where `type`!='I'");
        $result = mysqli_query($this->db, $query);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            $num_rows = mysqli_num_rows($result);
            if ($num_rows > 0) {
                while ($rows = mysqli_fetch_assoc($result)) {
                    if ($rows['type'] == 'A') {
                        $a_bundle_id = $rows['bundle_id'];
                    } elseif ($rows['type'] == 'M') {
                        $m_bundle_id = $rows['bundle_id'];
                    }
                }
            }
        }
        $dir_name = "../Globals/PushCert/";
//                        chdir($dir_name);
        $push_cert = array_diff(scandir($dir_name), array('..', '.'));
//                         echo "push  cert  ".json_encode($push_cert);
        foreach ($push_cert as $key => $value) {
            if (($value == ".DS_Store" && $key == 2) || $a_bundle_id == $value || $m_bundle_id == $value) {
                continue;
            }
            $dir_name = "../Globals/PushCert/$value/";
            $push_props = array_diff(scandir($dir_name), array('..', '.'));
//            $olddir = getcwd();
//            chdir($dir_name);
            $file = [];
            foreach ($push_props as $k => $v) { 
                $dir_name = "../Globals/PushCert/$value/$v";
                if($v!="android.props"){
                    $file[] = file_get_contents($dir_name);
                }
            }
            $check_qyery = sprintf("select * from `mobile_push_certificate` where type='I' and bundle_id='%s'",mysqli_real_escape_string($this->db, $value));
            $result_check = mysqli_query($this->db, $check_qyery);
            if (!$result_check) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$check_qyery");
                ipn_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 500);
            }else{
                $num_rows = mysqli_num_rows($result_check);
                if ($num_rows > 0) {
                    $rows = mysqli_fetch_object($result_check);
                    $sql_update = sprintf("update `mobile_push_certificate` set `dev_pem`='%s',`prod_pem`='%s' where type='I' and bundle_id='%s'",mysqli_real_escape_string($this->db, $file[0]), mysqli_real_escape_string($this->db, $file[1]), mysqli_real_escape_string($this->db, strtolower($rows->bundle_id)));
                    $result_update = mysqli_query($this->db, $sql_update);
                    if (!$result_update) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_update");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }else{
                     $sql_insert = sprintf("INSERT INTO `mobile_push_certificate`(`type`, `bundle_id`, `dev_pem`, `prod_pem`) VALUES ('I','%s','%s','%s')",
                            mysqli_real_escape_string($this->db, strtolower($value)), mysqli_real_escape_string($this->db, $file[0]), mysqli_real_escape_string($this->db, $file[1]));
                    $result_insert = mysqli_query($this->db, $sql_insert);
                    if (!$result_insert) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql_insert");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                }
            }

       }
    }
    
     public function updateTrialDimensionsNetSales($company_id, $trial_id, $date) {
        $reg_id = 0;
        $user_timezone = $this->getUserTimezone($company_id);
        $curr_time_zone = date_default_timezone_get();
        $new_timezone = $user_timezone['timezone'];
        date_default_timezone_set($new_timezone);
        $host = $_SERVER['HTTP_HOST'];
        if(strpos($host, 'mystudio.academy') !== false) {
            $tzadd_add =" (IF(@@global.time_zone='SYSTEM',IF(@@session.time_zone='SYSTEM',@@system_time_zone,@@session.time_zone),@@global.time_zone))" ;
        }else{
            $tzadd_add ="'".$curr_time_zone."'";
        }
        $currdate_add = " SELECT DATE(CONVERT_TZ(now(),$tzadd_add,'$new_timezone'))" ;
        $mp_last_upd_date=" DATE(CONVERT_TZ(tp.last_updt_dt,$tzadd_add,'$new_timezone'))";
        $last_upd_date = " DATE(CONVERT_TZ(last_updt_dt,$tzadd_add,'$new_timezone'))";
        if(empty($date)){
            $curr_dt = "($currdate_add)";
        }else{
            $curr_dt = "'$date'";
        }
//        $sql = sprintf("SELECT mr.company_id, mr.membership_id, mr.membership_option_id, $mp_last_upd_date as mp_ud, sum(if(mr.processing_fee_type=1,mp.payment_amount-mp.processing_fee,mp.payment_amount)) amount, 
//                  mp.payment_date FROM `membership_registration` mr LEFT JOIN `membership_payment` mp 
//                  ON mr.`membership_registration_id` = mp.`membership_registration_id` 
//                  WHERE mr.company_id='%s' AND mr.membership_id='%s' AND  MONTH( $mp_last_upd_date) = MONTH($curr_dt)
//                  AND YEAR($mp_last_upd_date) = YEAR($curr_dt) AND mp.payment_status='S' AND mp.checkout_status='released' GROUP BY mr.`membership_id`, mr.`membership_option_id`", mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $membership_id));
        $date_inc1= "DATE_FORMAT($curr_dt,'%Y-%m')";
        $date_inc2= "DATE_FORMAT($mp_last_upd_date,'%Y-%m')";
        $date_inc3= "DATE_FORMAT($last_upd_date,'%Y-%m')";
        
        $sql = sprintf("SELECT company_id, trial_id,  mp_ud, sum(amount) amount, payment_date FROM (

                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND tp.payment_status='S' AND tp.checkout_status='released'
                  AND %s = %s 
                  
                  UNION
                  
                  SELECT tr.company_id, tr.trial_id, $mp_last_upd_date as mp_ud, concat('-',if(tr.processing_fee_type=1,tp.payment_amount-tp.processing_fee,tp.payment_amount)) amount, 
                  tp.payment_date, tp.trial_payment_id FROM `trial_registration` tr LEFT JOIN `trial_payment` tp 
                  ON tr.`trial_reg_id` = tp.`trial_reg_id` 
                  WHERE tr.company_id='%s' AND tr.trial_id='%s' AND  tp.`payment_status`='R' 
                  AND %s = %s
                  AND tp.checkout_id NOT IN 
                  (SELECT checkout_id FROM trial_payment WHERE payment_status='FR' AND %s = %s)
                  )t1
                  GROUP BY t1.`trial_id`"

                 , mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $trial_id),
                 $date_inc2, $date_inc1, $date_inc3, $date_inc1);
        
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $reg_id = 1;
        } else {
            $trial = $trial_id_arr = [];
            $number_rows = mysqli_num_rows($result);
            if ($number_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $temp = $temp2 = [];
                    $temp['trial_id'] = $temp2['trial_id'] = $trial_id = $row['trial_id'];
                    $temp['payment_date'] = $temp2['payment_date'] = $payment_date = $row['payment_date'];
                    $temp2['amount'] = $amount = $row['amount'];
                    $temp2['mp_ud'] = $row['mp_ud'];

                    if (!empty($trial_id) && !in_array($trial_id, $trial_id_arr, true)) {
                        $trial_id_arr[] = $trial_id;
                        $trial[] = $temp;
                    }
                    $option[] = $temp2;
                }

                for ($i = 0; $i < count($option); $i++) {
                    $date_init = 0;
                   
                    $date_str = "%Y-%m";
                    $curr_dt = date("Y-m-d");
                    $every_month_dt = date("Y-m-d", strtotime("+$date_init month", strtotime($option[$i]['mp_ud'])));
//                     
                    $sql1 = sprintf("SELECT * FROM `trial_dimensions` WHERE `company_id`='%s' AND `trial_id`='%s' AND `period`=DATE_FORMAT('%s', '%s')", mysqli_real_escape_string($this->db, $company_id), $option[$i]['trial_id'], $every_month_dt, $date_str);
                    $res1 = mysqli_query($this->db, $sql1);
                    if (!$res1) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                        ipn_log_info($this->json($error_log));
                        $reg_id = 1;
                    } else {
//                           

                        $num_of_rows = mysqli_num_rows($res1);
                        if ($num_of_rows == 1) {

                            if($i==0){
                                    $sales_value = "'%s'";
                                }else{
                                    $sales_value = "`sales_amount`+'%s'";
                                }
                                $sql3 = sprintf("UPDATE `trial_dimensions` SET `sales_amount`=$sales_value WHERE `company_id`='%s' AND `period`=DATE_FORMAT('%s', '%s') AND `trial_id`='%s' ",mysqli_real_escape_string($this->db, $option[$i]['amount']), mysqli_real_escape_string($this->db, $company_id), $every_month_dt, $date_str,mysqli_real_escape_string($this->db, $trial_id));
                                $res3 = mysqli_query($this->db, $sql3);
                                if (!$res3) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql3");
                                    ipn_log_info($this->json($error_log));
                                    $reg_id = 1;
                                }
                           
                        }
                    }
                }
            } else {
                $error = array('status' => "Failed", "msg" => "No records.","query" => "$sql");
                $this->response($this->json($error), 204);
            }
        }
        date_default_timezone_set($curr_time_zone);
        
        if($reg_id==1){
            $msg = "Something went wrong for updateTrialDimensionsNetSales(), check log";
            $this->sendEmailForAdmins($msg);
        }
    }
    
    private function updateBundleIdInCompanyTable(){
        $query = sprintf("SELECT c.company_id, IFNULL( app_id, 'com.mystudio.app') app_id, device_type, COUNT(*) 
                FROM company c LEFT JOIN student s ON c.company_id = s.company_id
                WHERE upgrade_status =  'W' AND c.company_id !=1
                AND app_id IS NOT NULL AND TRIM( app_id ) !=  ''
                AND device_type IS NOT NULL 
                GROUP BY s.company_id, app_id, device_type
                HAVING COUNT( * ) >10");
        $result = mysqli_query($this->db, $query);
        if(!$result){            
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $company_list = $output = [];
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($row['app_id'] != 'com.mystudio.app') {
                        $c_id = $row['company_id'];
                        if (!in_array($c_id, $company_list)) {
                            $company_list[] = $c_id;
                        }
                        if ($row['device_type'] == 'A') {
                            $output["$c_id"]['A'] = $row['app_id'];
                        } elseif ($row['device_type'] == 'I') {
                            $output["$c_id"]['I'] = $row['app_id'];
                        }
                    }
                }
                if (count($company_list) > 0){
                    $updated_count = 0;
                    for($i=0;$i<count($company_list);$i++){
                        $str = '';
                        $company_id = $company_list[$i];
                        if(isset($output[$company_id]['I']) && !empty($output[$company_id]['I'])){
                            $str .= "`ios_bundle_id`='".$output[$company_id]['I']."'";
                            if(isset($output[$company_id]['A']) && !empty($output[$company_id]['A'])){
                                $str .= ", `android_bundle_id`='".$output[$company_id]['A']."'";
                            }
                        }elseif(isset($output[$company_id]['A']) && !empty($output[$company_id]['A'])){
                            $str .= "`android_bundle_id`='".$output[$company_id]['A']."'";
                        }
                        $query2 = sprintf("UPDATE `company` SET %s, `last_updt_dt`=`last_updt_dt` WHERE `company_id`='%s'",$str,$company_id);
                        $result2 = mysqli_query($this->db, $query2);
                        if(!$result2){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }else{
                            $updated_count += mysqli_affected_rows($this->db);
                        }
                    }
                    ipn_log_info("Total rows updated : $updated_count");
                }else{
                    $error = array('status' => "Failed", "msg" => "Only mystudio app exists.");
                    $this->response($this->json($error), 500);
                }
            }else{
                $error = array('status' => "Failed", "msg" => "No bundle id available");
                $this->response($this->json($error), 500);
            }
        }
    }
//    Zapier
    private function updateZapUniqueidForstudio() {
         $updated_count = 0;
        $sql = sprintf("SELECT `company_id` FROM `leads_registration` group by `company_id`");
        $res = mysqli_query($this->db, $sql);
        if (!$res) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
        } else {
            $num_rows_c = mysqli_num_rows($res);
            if ($num_rows_c > 0) {
                while ($row_c = mysqli_fetch_assoc($res)) {
                    $company_id = $row_c['company_id'];
                    $query = sprintf("SELECT `leads_reg_id`, `company_id`, `program_id`, `source_id`, `zap_reg_id`, `registration_date`,`created_dt` 
                 FROM `leads_registration` WHERE `company_id`='%s' order by `created_dt`", $company_id);
                    $result = mysqli_query($this->db, $query);
                    if (!$result) {
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
                        ipn_log_info($this->json($error_log));
                    } else {
                        $num_rows = mysqli_num_rows($result);
                        if ($num_rows > 0) {
                            $output = [];
                            $comp_zap_id = [];
                            while ($row = mysqli_fetch_assoc($result)) {
                                $output[] = $row;
                            }
                            if (count($output) > 0) {
                                $comp_zap_id = 0;
                                $str = '';
                                for ($i = 0; $i < count($output); $i++) {
                                    $leads_reg_id = $output[$i]['leads_reg_id'];
                                    $comp_zap_id += 1;
                                    $str .= "UPDATE `leads_registration` SET `last_updt_dt`=`last_updt_dt`, `zap_reg_id`='$comp_zap_id' WHERE `leads_reg_id`='$leads_reg_id';";
                                }
                                $query2 = sprintf($str);
                                $result2 = mysqli_multi_query($this->db, $query2);
                                if (!$result2) {
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                                    ipn_log_info($this->json($error_log));
                                } else {
                                    do {
                                     $updated_count += mysqli_affected_rows($this->db);
                                    } while (mysqli_more_results($this->db) && mysqli_next_result($this->db));
                                }
                            }
                        } else {
                            $error = array('status' => "Failed", "msg" => "There is no registration for this studio.");
                            ipn_log_info($this->json($error));
                        }
                    }
                }
                ipn_log_info("Total rows updated : $updated_count");
            }else{
                $error = array('status' => "Failed", "msg" => "There is no studio details.");
                $this->response($this->json($error), 500);
            }
        }
    }
    private function getretailCheckoutDetails($checkout_id){
        $retail_arr=[];
        $query = sprintf("SELECT rto.`company_id`,rod.`retail_payment_amount`, rod.`retail_product_id`, rto.`retail_order_id`,rod.`retail_parent_id`, rp.`retail_payment_id`, rp.`checkout_id`, rp.`checkout_status`, `access_token`, rp.`payment_amount`, rp.`processing_fee`, rto.`processing_fee_type`
                    FROM `retail_orders` rto 
                    LEFT JOIN `retail_payment` rp ON rto.`retail_order_id`=rp.`retail_order_id`
                    LEFT JOIN `retail_order_details` rod ON rto.`retail_order_id`=rod.`retail_order_id`
                    LEFT JOIN `wp_account` wp ON rto.`company_id` = wp.`company_id` 
                    WHERE rp.`checkout_id` = '%s' ORDER BY rp.`retail_payment_id`", mysqli_real_escape_string($this->db, $checkout_id));
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        }else{
            $num_rows = mysqli_num_rows($result);
            if($num_rows>0){
                $k=0;
                while($row = mysqli_fetch_assoc($result)){
                    $retail_order_id = $row['retail_order_id'];
                    $access_token = $row['access_token'];
                    $payment_id = $row['retail_payment_id'];
                    $fee = $row['processing_fee_val'];
                    $processing_fee_type = $row['processing_fee_type'];
                    $payment_amount = $row['payment_amount'];
                    $company_id = $row['company_id'];
                    $retail_arr[$k]['retail_product_id'] = $row['retail_product_id'];
                    $retail_arr[$k]['retail_parent_id'] = $row['retail_parent_id'];
                    $retail_arr[$k]['retail_price'] = $row['retail_payment_amount'];
                    $k++;
                }
                
                $password = $this->wp_pswd;
                $method = 'aes-256-cbc';
                // Must be exact 32 chars (256 bit)
                $password = substr(hash('sha256', $password, true), 0, 32);
                // IV must be exact 16 chars (128 bit)
                $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                
                $token = openssl_decrypt(base64_decode($access_token), $method, $password, OPENSSL_RAW_DATA, $iv);
                $dummy_sns=[];
                $json = array("checkout_id"=>"$checkout_id");
                $postData = json_encode($json);
                $response = $this->wp->accessWepayApi('checkout','POST',$postData,$token,$this->user_agent,$dummy_sns);
                ipn_log_info("wepay_get_checkout: ".$response['status']);
                if($response['status']=='Success'){
                    $res = $response['msg'];
                    $checkout_state = $res['state'];
                    $payment_status = "";
                    $send_email = 0;
                    if($checkout_state=='expired' || $checkout_state=='failed' || $checkout_state=='charged back'){
                        $payment_status = ", `payment_status`='F'";
                        $send_email = 1;
                    }
                    
                    if($checkout_state=='released'){
                        $paid_amount = 0;
                        if($processing_fee_type==1){
                            $paid_amount = $payment_amount-$fee;
                        }elseif($processing_fee_type==2){
                            $paid_amount = $payment_amount;
                        }
                        $update_sales_in_reg = sprintf("UPDATE `retail_orders` SET `retail_order_paid_amount`=$paid_amount WHERE `retail_order_id`='%s'", mysqli_real_escape_string($this->db, $retail_order_id));
                        $result_sales_in_reg = mysqli_query($this->db, $update_sales_in_reg);
                        
                        if(!$result_sales_in_reg){
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_sales_in_reg");
                            ipn_log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 500);
                        }
                        for($i=0;$i<count($retail_arr);$i++){
                            $retail_parent_id=$retail_arr[$i]['retail_parent_id'];
                            $retail_product_id=$retail_arr[$i]['retail_product_id'];
                            $retail_product_price=$retail_arr[$i]['retail_price'];
                            $str = '';
                            if (empty($retail_parent_id) || is_null($retail_parent_id)) {
                                $str = "IN(%s)";
                            } else {
                                $str = "IN(%s,$retail_parent_id)";
                            }
                            $update_count = sprintf("UPDATE `retail_products` SET `retail_net_sales`=`retail_net_sales`+$retail_product_price WHERE `company_id`='%s' AND `retail_product_id` $str",
                                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $retail_product_id));
                            $result_count = mysqli_query($this->db, $update_count);

                            if(!$result_count){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_count");
                                ipn_log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }
                        }
                     }
                    $update_query = sprintf("UPDATE `retail_payment` SET `checkout_status`='%s' $payment_status WHERE `retail_order_id`='%s' and `retail_payment_id`='%s' and `checkout_id`='%s'", mysqli_real_escape_string($this->db, $checkout_state),
                            mysqli_real_escape_string($this->db, $retail_order_id), mysqli_real_escape_string($this->db, $payment_id), mysqli_real_escape_string($this->db, $checkout_id));
                    $update_result = mysqli_query($this->db, $update_query);
                    if(!$update_result){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }else{
                        $error = array('status' => "Success", "msg" => "Checkout status updated successfully.");
                        ipn_log_info($this->json($error));
                        if($send_email==1){
                           $this->responseWithMail($this->json($error), 200);
                        }else{
                            $this->responseWithMail($this->json($error), 200);
                        }
                    }
                    
                }else{
                    $error = array('status' => "Failed", "msg" => $response['msg']['error_description']);
                    ipn_log_info($this->json($error));
                    $this->response($this->json($error), 400);
                }
                
            }else{
                $error = array('status' => "Failed", "msg" => "Checkout details doesn't exist.");
                ipn_log_info($this->json($error));
                $this->response($this->json($error), 404);
            }
        }
    }
    
    public function membershipBulkRegistration() {
        $start = $end = 0;
        if(isset($_REQUEST['start']) && !empty($_REQUEST['start'])){
            $start = $_REQUEST['start'];
        }
        if(isset($_REQUEST['end']) && !empty($_REQUEST['end'])){
            $end = $_REQUEST['end'];
        }
        if(!empty($start) && !empty($end)){
            for($i=0;$i<2;$i++){
                $select = sprintf("SELECT * FROM membership_registration WHERE membership_registration_id between $start and $end");
                $result = mysqli_query($this->db, $select);
                if(!$result){
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$select");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 500);
                }else{
                    if(mysqli_num_rows($result)>0){
                        while($row = mysqli_fetch_assoc($result)){
                            $insert2 = sprintf("INSERT INTO `membership_registration`( `company_id`, `membership_id`, `membership_option_id`, `student_id`, `participant_id`, `membership_category_title`, `membership_title`, `membership_status`, `transferred_from`, `rank_status`, `rank_id`, `last_advanced_date`, `buyer_name_old`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, `billing_options`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, `no_of_classes`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `membership_reg_type_user`, `membership_reg_version_user`, `mem_reg_completion_date`, `specific_start_date`, `specific_end_date`, `exclude_billing_flag`, `start_email_date`, `end_email_date`, `payment_amount`, `paid_amount`, `refunded_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `hold_date`, `resume_date`, `cancelled_date`) "
                                    . "SELECT `company_id`, `membership_id`, `membership_option_id`, `student_id`, `participant_id`, `membership_category_title`, `membership_title`, `membership_status`, `transferred_from`, `rank_status`, `rank_id`, `last_advanced_date`, `buyer_name_old`, `buyer_first_name`, `buyer_last_name`, `buyer_email`, `buyer_phone`, `buyer_postal_code`, `processing_fee_type`, `processing_fee`, `payment_type`, `membership_structure`, `prorate_first_payment_flg`, `delay_recurring_payment_start_flg`, `delay_recurring_payment_start_date`, `delay_recurring_payment_amount`, `custom_recurring_frequency_period_type`, `custom_recurring_frequency_period_val`, `initial_payment_include_membership_fee`, `billing_options`, `billing_options_expiration_date`, `billing_options_deposit_amount`, `billing_options_no_of_payments`, `no_of_classes`, `credit_card_id`, `credit_card_status`, `credit_card_name`, `credit_card_expiration_month`, `credit_card_expiration_year`, `signup_fee`, `signup_fee_disc`, `membership_fee`, `membership_fee_disc`, `initial_payment`, `initial_payment_date`, `initial_payment_success_flag`, `first_payment`, `membership_reg_type_user`, `membership_reg_version_user`, `mem_reg_completion_date`, `specific_start_date`, `specific_end_date`, `exclude_billing_flag`, `start_email_date`, `end_email_date`, `payment_amount`, `paid_amount`, `refunded_amount`, `payment_frequency`, `membership_start_date`, `payment_start_date`, `recurring_start_date`, `next_payment_date`, `preceding_payment_date`, `preceding_payment_status`, `no_of_payments`, `membership_registration_column_1`, `membership_registration_column_2`, `membership_registration_column_3`, `membership_registration_column_4`, `membership_registration_column_5`, `membership_registration_column_6`, `membership_registration_column_7`, `membership_registration_column_8`, `membership_registration_column_9`, `membership_registration_column_10`, `participant_street`, `participant_city`, `participant_state`, `participant_zip`, `participant_country`, `hold_date`, `resume_date`, `cancelled_date` FROM membership_registration WHERE membership_registration_id='%s'", mysqli_real_escape_string($this->db, $row['membership_registration_id']));
                            $result2 = mysqli_query($this->db, $insert2);
                            if(!$result2){
                                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert2");
                                log_info($this->json($error_log));
                                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                $this->response($this->json($error), 500);
                            }else{
                                $reg_id = mysqli_insert_id($this->db);
                                $insert3 = sprintf("INSERT INTO `membership_payment`(`company_id`, `membership_registration_id`, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `refunded_amount`, `refunded_date`, `paytime_type`, `prorate_flag`, `cc_id`, `cc_name`) "
                                        . "SELECT `company_id`, $reg_id, `checkout_id`, `checkout_status`, `payment_amount`, `processing_fee`, `payment_date`, `payment_status`, `refunded_amount`, `refunded_date`, `paytime_type`, `prorate_flag`, `cc_id`, `cc_name` FROM `membership_payment` WHERE membership_registration_id='%s' LIMIT 0,2", mysqli_real_escape_string($this->db, $row['membership_registration_id']));
                                $result3 = mysqli_query($this->db, $insert3);
                                if(!$result3){
                                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert3");
                                    log_info($this->json($error_log));
                                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                                    $this->response($this->json($error), 500);
                                }                            
                            }
                        }
                    }else{
                        $error = array('status' => "Failed", "msg" => "No records found.");
                        $this->response($this->json($error), 500);
                    }
                }
            }
        }else{
            $error = array('status' => "Failed", "msg" => "Invalid request parameters.");
            $this->response($this->json($error), 500);
        }
    }
    
    public function addMessageUsers(){
        $query = sprintf("SELECT m.company_id, m.message_id, c.timezone
                FROM  `message` m LEFT JOIN company c ON m.company_id = c.company_id WHERE message_to != 'I'
                AND `message_id` NOT IN (SELECT  `message_id` FROM  `message_mapping` WHERE 1 GROUP BY `message_id`) ORDER BY message_id");
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 500);
        } else {
            if(mysqli_num_rows($result)>0){
                while($row = mysqli_fetch_assoc($result)){
                    $student_id = [];
                    $query2 = sprintf("UPDATE message SET `message_delivery_date`=IF(`message_delivery_date` IS NULL, NULL, CONVERT_TZ(`message_delivery_date`, '%s', 'GMT')), 
                            `push_delivered_date`=IF(`push_delivered_date` IS NULL, NULL, CONVERT_TZ(`push_delivered_date`, '%s', 'GMT')) WHERE message_id='%s'", 
                            mysqli_real_escape_string($this->db, $row['timezone']), mysqli_real_escape_string($this->db, $row['timezone']), mysqli_real_escape_string($this->db, $row['message_id']));
                    $result2 = mysqli_query($this->db, $query2);
                    if(!$result2){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query2");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 500);
                    }
                    $this->sendGroupMessage($row['company_id'], $row['message_id'], $student_id);
                }
            }
        }
    }
    
    //send group message
    public function sendGroupMessage($company_id, $message_id, $student_id){
        if(empty($student_id)){
            $sql = sprintf("SELECT student_id FROM `student` WHERE `company_id`='%s' AND `deleted_flag`!='Y'
                        AND student_id NOT IN (SELECT student_id FROM message_mapping WHERE message_id='%s')", 
                    mysqli_real_escape_string($this->db,$company_id), mysqli_real_escape_string($this->db,$message_id));
            $result = mysqli_query($this->db, $sql);
            if (!$result) {
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
                ipn_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            } else {
                $student_id=[];
                $num_of_rows = mysqli_num_rows($result);
                if ($num_of_rows > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $student_id[] = $row['student_id'];
                    }
                }
            }
        }
        
        for($i=0; $i<count($student_id);$i++){
            $insert_query = sprintf("INSERT INTO `message_mapping`(`company_id`, `message_id`, `student_id`) VALUES('%s', '%s', '%s')",
                    mysqli_real_escape_string($this->db, $company_id), mysqli_real_escape_string($this->db, $message_id), mysqli_real_escape_string($this->db, $student_id[$i]));
            $insert_result = mysqli_query($this->db, $insert_query);
            if(!$insert_result){
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$insert_query");
                ipn_log_info($this->json($error_log));
                $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                $this->response($this->json($error), 200);
            }
        }
    }
    
    public function addEventURL(){
        $server_url = 'http' . (isset($_SERVER['HTTPS']) ? 's://' : '://') . $_SERVER['SERVER_NAME'] . "/e/?=";
        $query = sprintf("UPDATE event e, company c SET event_url = CONCAT('$server_url',c.company_code,'/',c.company_id,'/',e.event_id)
             WHERE e.company_id=c.company_id AND (e.event_url IS NULL OR TRIM(e.event_url)='');");
        $result = mysqli_query($this->db, $query);
        if(!$result){
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$query");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        }else{
            $num_rows = mysqli_affected_rows($this->db);
            $res = array('status' => "Success", "msg" => "Event URL added successfully.", "total_records_updated" => $num_rows);
            $this->response($this->json($res), 200);
        }
    }
    
    public function updateCompanyIdInWhiteLabelApps(){
        $dir_name = "../$this->upload_folder_name/whitelabel";
        chdir($dir_name);
        $studio_code_array = [];
        $row = 0;
        if (($handle = fopen("sample.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    if($c == 2){
                        $studio_code_array[$row]['studio_code'] = $data[$c];
                    }elseif($c == 8){
                        $studio_code_array[$row]['bundle_id'] = $data[$c];
                    }
                }
                $row++;
            }
            array_splice($studio_code_array,0,1);
//            echo json_encode($studio_code_array)."<br>";
            fclose($handle);
        }
        
        for($i=0 ; $i < count($studio_code_array) ; $i++){
            $temp_status =$studio_code_array_temp=$temp_company_code= "";
            if(!empty(trim($studio_code_array[$i]['studio_code']))){
                $studio_code_array_temp = str_replace(",","','", mysqli_real_escape_string($this->db, $studio_code_array[$i]['studio_code']));
    //            echo $studio_code_array_temp."<br>";
                $sql1 = sprintf("SELECT upgrade_status,company_id,company_code FROM `company` where company_code IN ('$studio_code_array_temp') AND `upgrade_status` IN ('W', 'WM') LIMIT 0,1");
                $result = mysqli_query($this->db, $sql1);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if (mysqli_num_rows($result) > 0) {
                        $row = mysqli_fetch_assoc($result);
                        $temp_status = $row['upgrade_status'];
                        $company_id = $row['company_id'];
                        $temp_company_code = $row['company_code'];

                        $sql2 = sprintf("UPDATE `white_label_apps` SET company_id='$company_id', `studio_code`='$temp_company_code' WHERE `ios_bundle_id`='%s'", 
                            mysqli_real_escape_string($this->db, $studio_code_array[$i]['bundle_id']));
                        $result2 = mysqli_query($this->db, $sql2);
                        if (!$result2) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }
                    }
                }
            }else{
                $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql2");
                log_info($this->json($error_log));
            }
        }
    }
    
    public function checkCompanyIdInWhiteLabelApps(){
        $dir_name = "../$this->upload_folder_name/whitelabel";
        chdir($dir_name);
        $studio_code_array = [];
        $row = 0;
        if (($handle = fopen("sample2.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    if($c == 0){
                        $studio_code_array[$row]['SNO'] = $data[$c];
                    }elseif($c == 2){
                        $studio_code_array[$row]['studio_code'] = $data[$c];
                    }elseif($c == 8){
                        $studio_code_array[$row]['bundle_id'] = $data[$c];
                    }
                }
                $row++;
            }
            array_splice($studio_code_array,0,1);
//            echo json_encode($studio_code_array)."<br>";
            fclose($handle);
        }
        
        for($i=0 ; $i < count($studio_code_array) ; $i++){
            $temp_status =$studio_code_array_temp=$temp_company_code= "";
            if(!empty(trim($studio_code_array[$i]['studio_code']))){
                $studio_code_array_temp = str_replace(",","','", mysqli_real_escape_string($this->db, $studio_code_array[$i]['studio_code']));
    //            echo $studio_code_array_temp."<br>";
                $sql1 = sprintf("SELECT upgrade_status,company_id,company_code FROM `company` where company_code IN ('$studio_code_array_temp') AND `upgrade_status` IN ('W', 'WM') LIMIT 0,1");
                $result = mysqli_query($this->db, $sql1);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if(mysqli_num_rows($result)==0){
                        log_info("Not Whitelabel(".$studio_code_array[$i]['studio_code'].") - ".$studio_code_array[$i]['SNO']);
                    }
                }
            }else{
                log_info("Empty Studio code - ".$studio_code_array[$i]['SNO']);
            }
        }
    }
    
    public function checkTeamIdInWhiteLabelApps(){
        $dir_name = "../$this->upload_folder_name/whitelabel";
        chdir($dir_name);
        $studio_code_array = [];
        $row = 0;
        if (($handle = fopen("WL.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    if($c == 2 && strtolower($data[$c])=='mystudio academy'){
                        $studio_code_array[$row]['s_no'] = $data[0];
                        $studio_code_array[$row]['team_id'] = $data[2];
                        $studio_code_array[$row]['bundle_id'] = $data[5];
                    }
                }
                $row++;
            }
            array_splice($studio_code_array,0,1);
//            echo json_encode($studio_code_array)."<br>";
            fclose($handle);
        }
//        exit();
        $transferred = $mystudio = 0;
        for($i=0 ; $i < count($studio_code_array) ; $i++){
            if(!empty(trim($studio_code_array[$i]['team_id'])) && !empty(trim($studio_code_array[$i]['bundle_id']))){
                $sql1 = sprintf("SELECT * FROM `white_label_apps` where ios_bundle_id='%s' and ios_teamid is null and ios_developerid is null",
                        mysqli_real_escape_string($this->db, $studio_code_array[$i]['bundle_id']));
                $result = mysqli_query($this->db, $sql1);
                if (!$result) {
                    $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql1");
                    log_info($this->json($error_log));
                    $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                    $this->response($this->json($error), 200);
                } else {
                    if(mysqli_num_rows($result)>0){
                        $mystudio++;
                        log_info("Team id empty to (".$studio_code_array[$i]['bundle_id']."(".mysqli_num_rows($result).")) - ".$studio_code_array[$i]['s_no']);
                        $update = sprintf("UPDATE `white_label_apps` SET mystudio_legacy_app='Y' WHERE ios_bundle_id='%s'", mysqli_real_escape_string($this->db, $studio_code_array[$i]['bundle_id']));
                        $update_result = mysqli_query($this->db, $update);
                        if (!$update_result) {
                            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update");
                            log_info($this->json($error_log));
                            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                            $this->response($this->json($error), 200);
                        }else{
//                            log_info("Update count ". mysqli_affected_rows($this->db));
                        }
                    }else{
                        $transferred++;
                        log_info("Transferred already(".$studio_code_array[$i]['bundle_id'].") - ".$studio_code_array[$i]['s_no']);
                    }
                }
            }else{
                log_info("Invalid bundle id(".$studio_code_array[$i]['bundle_id'].") - ".$studio_code_array[$i]['sno']);
            }
//            exit();
        }
        log_info("Total transferred - $transferred, Still in MyStudio - $mystudio");
    }
    
    public function updateRetailPaymentPFForFRRecords() {
        $affected_rows = [];
        $sql = sprintf("SELECT * FROM `retail_payment` WHERE payment_status='R' and TRIM(checkout_id)!='' ORDER BY retail_payment_id ASC");
        $result = mysqli_query($this->db, $sql);
        if (!$result) {
            $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$sql");
            ipn_log_info($this->json($error_log));
            $error = array('status' => "Failed", "msg" => "Internal Server Error.");
            $this->response($this->json($error), 200);
        } else {
            $num_of_rows = mysqli_num_rows($result);
            if ($num_of_rows > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $affected_row = 0;
                    $update_query = sprintf("UPDATE retail_payment SET last_updt_dt=last_updt_dt, processing_fee='%s' WHERE payment_status='FR' and checkout_id='%s'", 
                            mysqli_real_escape_string($this->db, $row['processing_fee']), mysqli_real_escape_string($this->db, $row['checkout_id']));
                    $result_update = mysqli_query($this->db, $update_query);
                    if(!$result_update){
                        $error_log = array('status' => "Failed", "msg" => "Internal Server Error.", "query" => "$update_query");
                        ipn_log_info($this->json($error_log));
                        $error = array('status' => "Failed", "msg" => "Internal Server Error.");
                        $this->response($this->json($error), 200);
                    }else{
                        $affected_row = mysqli_affected_rows($this->db);
                        echo "<br>Affected rows = $affected_row";
                        if($affected_row>0){
                            $affected_rows[] = $row['checkout_id'];
                        }
                    }
                }
            }
        }
        if(count($affected_rows)>0){
            echo " <br>".implode("','", $affected_rows);
        }else{
            echo " <br>No rows updated.";
        }
    }
    
}
        
$api = new wepayipn;
$api->processApi();