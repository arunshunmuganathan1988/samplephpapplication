/*jslint browser: true*/
/*global console, MyApp*/
//www
MyApp.angular.controller('IndexPageController', ['$rootScope', '$scope', '$http', 'urlservice', 'InitService', '$localStorage', '$window', function ($rootScope, $scope, $http, urlservice, InitService, $localStorage, $window) {
        'use strict';
        //No Internet - show alert
        $scope.istatus = function () {
            MyApp.alert('Please turn on internet', 'No Internet');
            $scope.preloaderVisible = false;
            return;
        };
        
        $scope.mc_search_member = $scope.mc_buyer_Phone = "";
        $scope.showcardselection = false;   
        $scope.shownewcard = true;
        $scope.total_processing_fee = $scope.total_misc_charge = 0;   
        $scope.mc_payment_method = 'CC';
        $scope.mc_ach_route_no = $scope.mc_ach_acc_number = $scope.mc_ach_holder_name = $scope.mc_ach_acc_type = $scope.mc_chk_ach ='';
        $scope.wepaystatus = $scope.stripestatus = '';
        $scope.wepay_enabled = $scope.stripe_enabled = $scope.mc_ach_chkbx = $scope.mc_cc_chkbx = false;
        $scope.student_name = [];
        $scope.stripe_card = $scope.stripe = $scope.payment_method_id = $scope.stripe_customer_id = '';
        
        $scope.initial_load_payment = function () {
            $scope.onetimecheckout = 0;
            $scope.ccyearlist = ["2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037"];
            $scope.ccmonthlist = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
            $scope.ccmonth = '';
            $scope.ccyear = '';
            $scope.country = '';
            if ($scope.wp_level === 'P') {
                $scope.wepay_env = "production";
                $scope.redirect_uri = "https://www.mystudio.academy/Wepay/wepay_redirect.php";
            } else if ($scope.wp_level === 'S') {
                $scope.wepay_env = "stage";
                $scope.redirect_uri = "http://dev.mystudio.academy/Wepay/wepay_redirect.php";
            }
            WePay.set_endpoint($scope.wepay_env); // change to "production" when live  
        }; 
        
         // Shortcuts
        var valueById = '';
        var d = document;
        d.id = d.getElementById, valueById = function(id) {
            return d.id(id).value;
        };

        // For those not using DOM libraries
        var addEvent = function(e,v,f) {
            if (!!window.attachEvent) { e.attachEvent('on' + v, f); }
            else { e.addEventListener(v, f, false); }
        };

        // ATTACH THE RETAIL TO THE DOM - WEB VIEW SUBMIT BUTTON
        addEvent(d.id('cc-submit-mc'), 'click', function () {
            $scope.miscWepayCheckout();
        });
       
       // ATTACH THE RETAIL TO THE DOM - MOBILE VIEW SUBMIT BUTTON
        addEvent(d.id('cc-submit-mc1'), 'click', function () {        
            $scope.miscWepayCheckout();
        });
        
        $scope.miscWepayCheckout = function(){
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var checkout_message1 = '';
            if($scope.mc_payment_method === 'CC' && parseFloat($scope.misc_charge) < 5){
                MyApp.alert('Charge amount should be greater than '+$scope.wp_currency_symbol+'5.00<br>', 'Message');
                return;
            }
            
            if (!$scope.mc_buyer_First_Name || !$scope.mc_buyer_Last_Name || !$scope.mc_buyer_Email)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.mc_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.mc_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.mc_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
            }
            if($scope.mc_payment_method === 'CH'){
                checkout_message1 += 'Payment Info:';
                if (!$scope.mc_check_number) {
                    checkout_message1 += '"Check Number"<br>';
                }
            }else if (!$scope.mc_cc_name || !$scope.mc_cc_number || !$scope.mc_cc_expirymonth  || !$scope.mc_cc_expiryyear || !$scope.mc_cc_cvv  || !$scope.mc_cc_country || !$scope.mc_cc_zipcode){
                    checkout_message1 += 'Payment Info:';
                    if (!$scope.mc_cc_name) {
                        checkout_message1 += '"Name on card"<br>';
                    }
                    if (!$scope.mc_cc_number) {
                        checkout_message1 += '"Card number"<br>';
                    }
                    if (!$scope.mc_cc_expirymonth) {
                        checkout_message1 += '"Expiration Date:Month"<br>';
                    }
                    if (!$scope.mc_cc_expiryyear) {
                        checkout_message1 += '"Expiration Date:Year"<br>';
                    }
                    if (!$scope.mc_cc_cvv) {
                        checkout_message1 += '"CVV"<br>';
                    }
                    if (!$scope.mc_cc_country) {
                        checkout_message1 += '"Choose country"<br>';
                    }
                    if (!$scope.mc_cc_zipcode) {
                        checkout_message1 += '"Zip code"<br>';
                    }
            }
            if ($scope.mc_sales_agreement && !$scope.mc_waiver_checked) {
                checkout_message1 += "Click the check box to accept sales agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            var userName = $scope.mc_buyer_First_Name + ' ' + $scope.mc_buyer_Last_Name;
            var user_first_name = $scope.mc_buyer_First_Name;
            var user_last_name = $scope.mc_buyer_Last_Name;
            var email = valueById('mc_buyer_Email').trim();
            var phone = valueById('mc_buyer_Phone');
            var postal_code = valueById('mc_cc_zipcode');
            var country = valueById('mc_cc_country');
            var response = WePay.credit_card.create({
                "client_id": $scope.wp_client_id,
                "user_name": userName,
                "email": valueById('mc_buyer_Email').trim(),
                "cc_number": valueById('mc_cc_number'),
                "cvv": valueById('mc_cc_cvv'),
                "expiration_month": valueById('mc_cc_expirymonth'),
                "expiration_year": valueById('mc_cc_expiryyear'),
                "address": {
                    "country": valueById('mc_cc_country'),
                    "postal_code": valueById('mc_cc_zipcode')
                }
            }, function (data) {
                if (data.error) {
                    $scope.preloader = false;
                    $scope.$apply();
                    MyApp.closeModal();
                    MyApp.alert(data.error_description, 'Message');
                    // HANDLES ERROR RESPONSE
                } else {
                    $scope.mcCheckout(data.credit_card_id, data.state, user_first_name, user_last_name, email, phone, postal_code, country);
                }
            });
        };
        
        $scope.mcStripeCheckout = function () {
            if ($scope.stripeFormValidation()) { // Validate stripe form
                $scope.preloader = true;
                $http({
                    method: 'POST',
                    url: urlservice.url + 'stripeMiscCheckout',
                    data: {
                        "companyid": $scope.companyid,
                        "studentid": $scope.mc_selected_student_id,
                        "buyer_first_name": $scope.mc_buyer_First_Name,
                        "buyer_last_name": $scope.mc_buyer_Last_Name,
                        "buyer_email": $scope.mc_buyer_Email,
                        "email": $scope.mc_buyer_Email,
                        "phone": $scope.mc_buyer_Phone,
                        "optional_note": $scope.mc_optional_note,
                        "payment_method": ($scope.misc_charge > 0) ? $scope.mc_payment_method : '',
                        "payment_amount": $scope.misc_charge,
                        "payment_method_id": ($scope.mc_payment_method == 'CC') ? $scope.payment_method_id : '',
                        "check_number": $scope.mc_check_number,
                        "reg_type_user": $scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U', // TYPE OF REGISTRATION
                        "token": $scope.pos_token,
                        "pos_email": $localStorage.pos_user_email_id,
                        "intent_method_type": parseFloat($scope.misc_charge) > 0 ? "PI" : "", // SI - SETUP INTENT /  PI - PAYMENT INTENT
                        "cc_type": ($scope.shownewcard) ? 'N' : 'E',
                        "stripe_customer_id": $scope.stripe_customer_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8',
                        'Access-Control-Allow-Origin': '*'
                    }
                }).then(function (response) {
                    $scope.preloader = false;
                    if (response.data.status === 'Success') {
                        $scope.preloader = false;
                        // Show success message 
                        $scope.resetmcForm();
                        $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                        MyApp.alert(response.data.msg, 'Thank You', function () {
                            $scope.backToPOS();
                        });
                    } else {
                        if (response.data.session_status === "Expired") {
                            MyApp.alert('Session Expired', '', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        } else {
                            MyApp.alert(response.data.msg, 'Failure');
                        }
                    }
                }, function (response) {
                    $scope.preloader = false;
                    MyApp.alert('Connection failed', 'Invalid server response');
                });
            } else {
                $scope.preloader = false;
            }
        };
        
        $scope.stripeFormValidation = function(){
            $scope.preloader = true;
            var checkout_message1 = '';
             
            if (!$scope.mc_buyer_First_Name || !$scope.mc_buyer_Last_Name || !$scope.mc_buyer_Email)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.mc_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.mc_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.mc_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
            }
            if (($scope.pos_user_type === 'S' && $scope.mc_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex) 
                    || (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.mc_payment_method == 'CH' && !$scope.mc_check_number)
                    || ($scope.mc_payment_method == 'CC' && $scope.shownewcard && !$scope.valid_stripe_card )
                    ||  ($scope.mc_payment_method == 'ACH' && (!$scope.mc_ach_route_no || !$scope.mc_ach_acc_number  || !$scope.mc_ach_holder_name || !$scope.mc_ach_acc_type)) ){
                checkout_message1 += 'Payment Info:';
                if($scope.pos_user_type === 'S' && $scope.mc_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
                   checkout_message1 += '"Select Card"<br>'; 
                }
                if (($scope.pos_user_type === 'S' || $scope.pos_user_type === 'P') && $scope.mc_payment_method == 'CH' && !$scope.mc_check_number) {
                    checkout_message1 += '"Check Number"<br>';
                }
                if ($scope.mc_payment_method == 'CC' && $scope.shownewcard && !$scope.valid_stripe_card ) {
                    checkout_message1 += '"Invalid card details"<br>';
                }
                
                if ($scope.mc_payment_method == 'ACH') {
                    checkout_message1 += '"ACH details"<br>';
                    if (!$scope.mc_ach_route_no) {
                        checkout_message1 += '"Route number"<br>';
                    }
                    if (!$scope.mc_ach_acc_number) {
                        checkout_message1 += '"Account number"<br>';
                    }
                    if (!$scope.mc_ach_holder_name) {
                        checkout_message1 += '"Account holder name"<br>';
                    }
                    if (!$scope.mc_ach_acc_type) {
                        checkout_message1 += '"Account type"<br>';
                    }
                }
            }
            if ($scope.mc_sales_agreement && !$scope.mc_waiver_checked && $scope.mc_payment_method != 'CC') {
                checkout_message1 += "Click the check box to accept Waiver & Agreement<br>";
            }
            if ($scope.stripe_enabled && $scope.mc_payment_method == 'ACH' && !$scope.mc_ach_chkbx) {
                checkout_message1 += '"Click the check box to accept the ACH plugin agreement"<br>';
            }
            if ($scope.stripe_enabled && $scope.mc_payment_method == 'CC' && !$scope.mc_cc_chkbx) {
                checkout_message1 += '"Click the check box to accept the Credit card agreement"<br>';
            }

            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }else{
                return true;
            }
        };
        
        // STRIPE TOKEN CALL
        $scope.getStripeElements = function () {
            // Create a Stripe client
            $scope.valid_stripe_card = false;
            
            // Create an instance of Elements
            var elements =  $scope.stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: 'rgb(74,74,74)',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: ($scope.mobiledevice)? '14px':'16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            // Create an instance of the card Element
            $scope.stripe_card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>
            $scope.stripe_card.mount('#stripe-card-element');

            // Handle real-time validation errors from the card Element.
            $scope.stripe_card.addEventListener('change', function (event) {
                var displayError = document.getElementById('stripe-card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                    $scope.valid_stripe_card = false;
                } else {
                    displayError.textContent = '';
                    $scope.valid_stripe_card = true;
                }
            });
        };

        // Handle form submission
        $scope.submitmiscform = function () {
           if($scope.stripeFormValidation()){
               $scope.payment_method_id = "";
                $scope.stripe.createPaymentMethod('card', $scope.stripe_card).then(function (result) {
                    if (result.error) {
                        setTimeout(function () {
                            $scope.preloader = false;
                            $scope.$apply();
                        }, 1000);
                        // Inform the user if there was an error. 
                        MyApp.alert(result.error.message, '');
                        $scope.valid_stripe_card = false;
                    } else {
                        // Send paymentMethod.id to server 
                        (result.paymentMethod.id) ? $scope.payment_method_id = result.paymentMethod.id : $scope.payment_method_id = "";
                        $scope.preloader = true;
                        $scope.mcStripeCheckout();
                    }
                });
            }else{
                $scope.preloader = false;
            }
        };
        
        // GET WEPAY STATUS
        $scope.getWepayStatus = function (companyid) {
            $scope.preloader = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'getwepaystatus',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'}

            }).then(function (response) {
                if (response.data.status === 'Success') {
                    $scope.wepaystatus = response.data.msg;
                    if($scope.wepaystatus !== 'N'){
                        $scope.wepay_enabled = true;
                    }
                } else {
                    $scope.wepaystatus = 'N';
                }
                $scope.preloader = false;
            }, function (response) {
                $scope.preloader = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        //get Stripe status of this company
        $scope.getStripeStatus = function (cmp_id) {
            
            if ($scope.stripestatus !== 'Y') {
                $scope.preloader = true;
                $scope.wepay_enabled = $scope.stripe_enabled = false;
                $http({
                    method: 'GET',
                    url: urlservice.url + 'getstripestatus',
                    params: {
                        "company_id": cmp_id
                    },
                    headers: {
                        "Content-Type": 'application/json; charset=utf-8'},
                        withCredentials: true

                }).then(function (response) {
                    $scope.preloader = false;
                    if (response.data.status === 'Success') {
                        $scope.stripestatus = response.data.msg;
                        if ($scope.stripestatus !== 'N') {
                            if ($scope.stripe_country_support === 'Y') {
                                $scope.stripe_enabled = true;
                                $scope.stripe = Stripe($scope.stripe_publish_key);
                                $scope.getStripeElements();       // AFTER GETTING STRIPE PUBLISH KEY WE GET STRIPE PAYMENT FORM
                            }else{
                                $scope.getWepayStatus(cmp_id);
                            }
                        }else{
                            $scope.getWepayStatus(cmp_id);
                        }      
                    } else if (response.data.status === 'Expired') {
                        console.log(response.data);
                        MyApp.alert(response.data.msg, '');
                    } else {
                        if(response.data.msg && response.data.msg !== 'N'){
                            MyApp.alert(response.data.msg, '');
                        } else{
                            $scope.getWepayStatus(cmp_id);
                        }
                    }
                }, function (response) {
                    $scope.preloader = false;
                    console.log(response.data);
                    MyApp.alert(response.data, '');
                });
            } else {
                if ($scope.stripestatus !== 'N' && $scope.stripe_country_support === 'Y') {
                    $scope.stripe_enabled = true;
                } else {
                    $scope.wepay_enabled = $scope.stripe_enabled = false;
                    $scope.getWepayStatus(cmp_id);
                }
            }
        };
        
       
        
        $scope.sendExpiryEmailToStudio = function(companyid){
            $scope.preloader = true;
            $http({
                method: 'POST',
                url: urlservice.url + 'sendExpiryEmailToStudio',
                data: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                if (response.data.status == 'Success') {
                    MyApp.alert(response.data.msg, '');
                } else {
                    MyApp.alert(response.data.msg, 'Failure');
                }
            }, function (response) {
                $scope.preloader = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        // GET COMPANY DETAILS
        $scope.getCompanyDetails = function (studio_code,companyid) {
            $scope.companyid = companyid;
            $scope.studio_code = studio_code;
            $scope.fav_company_name = "";
            
            $scope.preloader = true;
            $http({
                method: 'GET',
                url: urlservice.url + 'publiccompanydetails',
                params: {
                    "companyid": companyid,
                    "studio_code":studio_code,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',
                    "page_from": "C"
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                if (response.data.status == 'Success') {
                    // Load page:                                       
                    mcView.router.load({
                        pageName: 'misc_charge_checkout'
                    });
                    $scope.resetmcForm();
                    $scope.companydetails = response.data.company_details;
                    $scope.logoUrl = $scope.companydetails.logo_URL;
                    $scope.wp_level = $scope.companydetails.wp_level;
                    $scope.wp_client_id = $scope.companydetails.wp_client_id;
                    $scope.company_status = $scope.companydetails.upgrade_status;
                    $scope.processing_fee_percentage = $scope.companydetails.processing_fee_percentage;
                    $scope.processing_fee_transaction = $scope.companydetails.processing_fee_transaction;
                    $scope.fav_company_name = $scope.companydetails.company_name;
                    $scope.wp_currency_symbol = $scope.companydetails.wp_currency_symbol;
                    $scope.wp_currency_code = $scope.companydetails.wp_currency_code;
                    $scope.mc_enable_flag = $scope.companydetails.misc_charge_status;
                    $scope.mc_processing_fee_type = $scope.companydetails.misc_charge_fee_type;
                    $scope.mc_sales_agreement = $scope.companydetails.misc_sales_agreement;
                    $scope.misc_payment_method = $scope.companydetails.misc_payment_method;
                    //  STRIPE CREDENTIALS
                    $scope.stripe_upgrade_status = $scope.companydetails.stripe_upgrade_status;
                    $scope.stripe_subscription = $scope.companydetails.stripe_subscription;
                    $scope.stripe_publish_key = $scope.companydetails.stripe_publish_key;
                    $scope.stripe_country_support = $scope.companydetails.stripe_country_support;
                    $scope.getStripeStatus(companyid);
                    
                    $scope.mccarddetails = [];
                    if($scope.pos_user_type === 'S'){
                        $scope.getstudentlist(companyid);
                    }
                    $scope.initial_load_payment(); 
                } else {
                   // PREVENT FURTHUR EXECUTION & BLOCK THE URL IF STUDIO EXPIRED
                    
                    if (response.data.upgrade_status && response.data.studio_expiry_level) {
                        $scope.cmpny_email = response.data.studio_email;
                        $scope.company_status = response.data.upgrade_status;
                        $scope.company_expiry_level = response.data.studio_expiry_level;
                    }

                    if (($scope.company_status && $scope.company_status === 'F') || ($scope.company_expiry_level && $scope.company_expiry_level === 'L2')) {
                        // PREVENT FURTHUR EXECUTION & BLOCK THE APP
                        MyApp.modal({
                            title: '',
                            afterText:
                                    '<br>' +
                                    '<div id="studioExpiryText" class="modal-text custom-modal-content">Please contact the business operator to activate this link.</div>' +
                                    '<br>' +
                                    '<button class="button custom-modal-green-button button-text modalresetButton" id="send_expiry_email"></button>'
                        });

                        if ($scope.cmpny_email) {
                            $("#send_expiry_email").text("Send email message");
                        } else {
                            $("#send_expiry_email").text("Ok");
                        }

                        $('#send_expiry_email ').click(function () {
                            $scope.$apply(function () {
                                if ($("#send_expiry_email").text() === 'Send email message') {
                                    $scope.sendExpiryEmailToStudio(companyid);
                                }
                                MyApp.closeModal();
                            });
                        });

                    } else {
                        if(response.data.session_status === "Expired"){         
                            MyApp.alert('Session Expired','', function () {
                                $localStorage.pos_user_email_id = "";
                                $scope.backToPOS();
                            });
                        }else{
                            MyApp.alert(response.data.msg, 'Message');
                        }
                    }                    
                    
                }
            }, function (response) {
                $scope.preloader = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };        
        
        $scope.urlredirection = function () {
            $scope.param1 = $scope.param2 = $scope.param3 = $scope.param4 = '';
            var decoded_url = decodeURIComponent(window.location.href);
            var params = decoded_url.split('?=')[1].split('/');

            $scope.param1 = params[0];
            $scope.param2 = params[1];
            $scope.param3 = params[2];
            $scope.param4 = params[3];
            
            if(params[5]){
                if(params[5] === 'spos'){ //If staff
                    $scope.pos_user_type = 'S';
                    // ENABLE ADMIN BANNER, SEARCH MEMBER, OPTIONAL DISCOUNTS
                    $('#view-1').addClass('with-banner');
                }else{
                    $scope.pos_user_type = 'P'; //If public
                }
                if(params[6]){ //For access token
                    $scope.pos_token = params[6];
                }
                if(params[7]){
                   $scope.pos_entry_type = params[7];
                }
                $('#view-1').addClass('from-pos with-footer');
            }else{
                $scope.pos_user_type = '';
                $scope.pos_token = $scope.pos_entry_type = '';
            }
//            alert('param1: '+$scope.param1+' param2: '+$scope.param2+' param3: '+$scope.param3+' param4: '+$scope.param4+'  param5: '+params[5]);

            if ($scope.param1 !== undefined && $scope.param2 !== undefined && ($scope.param3 === undefined || $scope.param3 === '') && $scope.param4 === undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && ($scope.param4 === undefined || $scope.param4 === '')) {
                if (isNaN($scope.param3)) {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, '', '');
                } else {
                    $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, '');         // Is a number
                }
            } else if ($scope.param1 !== undefined && $scope.param2 !== undefined && $scope.param3 !== undefined && $scope.param4 !== undefined) {
                $scope.getCompanyDetails($scope.param1,$scope.param2, $scope.param3, $scope.param4);
            } else {
                window.location.href = window.location.origin;
            }
        };
        
        $scope.mcCheckout = function (credit_card_id, credit_card_state, user_first_name, user_last_name, email, phone, postal_code, country) {
            $scope.preloader = true;
            $scope.preloaderVisible = true;
            var checkout_message1 = '';   
            
            if($scope.mc_payment_method === 'CC' && parseFloat($scope.misc_charge) < 5){
                MyApp.alert('Charge amount should be greater than '+$scope.wp_currency_symbol+'5.00<br>', 'Message');
                return;
            }
            
            if (!$scope.mc_buyer_First_Name || !$scope.mc_buyer_Last_Name || !$scope.mc_buyer_Email)
            {
                checkout_message1 += "Buyer's Info:";
                if (!$scope.mc_buyer_First_Name) {
                    checkout_message1 += '"First Name"<br>';
                }
                if (!$scope.mc_buyer_Last_Name) {
                    checkout_message1 += '"Last Name"<br>';
                }
                if (!$scope.mc_buyer_Email) {
                    checkout_message1 += '"Email"<br>';
                }
            }
            if($scope.pos_user_type === 'S' && $scope.mc_payment_method === 'CC' && $scope.showcardselection && !$scope.selectedcardindex){
               checkout_message1 += '"Select Card"<br>'; 
            }
            if($scope.mc_payment_method === 'CH' && !$scope.mc_check_number){
                checkout_message1 += 'Payment Info:"Check Number"<br>';
            }           
            if ($scope.mc_sales_agreement && !$scope.mc_waiver_checked) {
                checkout_message1 += "Click the check box to accept sales agreement<br>";
            }
            if (checkout_message1 !== '') {
                MyApp.alert('Please complete required field(s)<br>' + checkout_message1, 'Message');
                setTimeout(function () {
                    $scope.preloader = false;
                    $scope.$apply();
                }, 1000);
                return false;
            }
            if($scope.mc_payment_method ==='CH'){                
                credit_card_id = '';
                credit_card_state = '';
            }else if($scope.mc_payment_method ==='CA'){
                credit_card_id = '';
                credit_card_state = '';
                $scope.mc_check_number = '';
            }else{
                $scope.mc_check_number = '';
            }
            
            $http({
                method: 'POST',
                url: urlservice.url + 'wepayMiscCheckout',
                data: {
                    "companyid": $scope.companyid,
                    "studentid": $scope.mc_selected_student_id,
                    "buyer_first_name":user_first_name,
                    "buyer_last_name":user_last_name,
                    "email":email,
                    "phone":phone,
                    "cc_id":credit_card_id,
                    "cc_state":credit_card_state,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U',         // TYPE OF REGISTRATION
                    "payment_method" :$scope.mc_payment_method, 
                    "check_number":$scope.mc_check_number,
                    "payment_amount":$scope.misc_charge,
                    "optional_note":$scope.mc_optional_note,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token": $scope.pos_token
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.resetmcForm();
                    $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                    MyApp.alert(response.data.msg,'Thank You', function () {
                       $scope.backToPOS();
                    });
                } else {
                    if(response.data.session_status === "Expired"){
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                        MyApp.alert(response.data.msg, 'Failure');
                    }                    
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert('Connection failed', 'Invalid server response');
            });
        };
        
        $scope.getstudentlist = function(companyid){
           $scope.preloader = false;
            $scope.preloaderVisible = false;
            $http({
                method: 'GET',
                url: urlservice.url + 'getstudent',
                params: {
                    "companyid": companyid,
                    "pos_email": $localStorage.pos_user_email_id,
                    "token":  $scope.pos_token,
                    "reg_type_user":$scope.pos_user_type === 'S' ? 'SP' : $scope.pos_user_type === 'P' ? 'PP' : 'U'
                },
                headers: {
                    "Content-Type": 'application/json; charset=utf-8',
                    'Access-Control-Allow-Origin': '*'
                }
            }).then(function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                if (response.data.status === 'Success') {
                    $scope.studentlist = response.data.msg.student_details;
                    for(var i=0;i<$scope.studentlist.length;i++){
                        $scope.student_name.push($scope.studentlist[i].student_name);
                    }
                    
                    var autocompleteDropdownSimple  =  MyApp.autocomplete ({
                        input: '#autocomplete-dropdown',
                        openIn: 'dropdown',

                        source: function (autocomplete, query, render) {
                           var results  =  [];
                           if (query.length === 0) {
                              render(results);
                              return;
                           }

                           // You can find matched items
                           for (var i = 0; i < $scope.student_name.length; i++) {
                              if ($scope.student_name[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) 
                                results.push($scope.student_name[i]);
                           }
                           // Display the items by passing array with result items
                           render(results);
                        }
                     });
                } else {
//                    MyApp.alert(response.data.msg, 'Failure');
                    if(response.data.session_status === "Expired"){
                        $scope.preloader = false;
                        $scope.preloaderVisible = false;
                        MyApp.alert('Session Expired','', function () {
                            $localStorage.pos_user_email_id = "";
                            $scope.backToPOS();
                        });
                    }else{
                    }
                }
            }, function (response) {
                $scope.preloader = false;
                $scope.preloaderVisible = false;
                MyApp.alert(response.data.msg, 'Failure');
            }); 
        };
        
        $scope.resetmcForm = function(){
            $scope.misc_checkout_form.$setPristine();
            $scope.misc_checkout_form.$setUntouched();
            $scope.mc_buyer_First_Name = '';
            $scope.mc_buyer_Last_Name = ''; 
            $scope.mc_buyer_Email = ''; 
            $scope.mc_buyer_Phone = '';
            $scope.mc_optional_note = '';
            $scope.misc_charge = '';
            $scope.mc_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.mc_credit_card_id = '';
            $scope.mc_credit_card_status = '';
            $scope.mc_cc_name = '';
            $scope.mc_cc_number = '';
            $scope.mc_cc_expirymonth = '';
            $scope.mc_cc_expiryyear = '';
            $scope.mc_cc_cvv = '';
            $scope.mc_cc_country = '';
            $scope.mc_cc_zipcode = '';
            $scope.mc_search_member = "";
            $scope.mc_waiver_checked = $scope.mc_ach_chkbx = $scope.mc_cc_chkbx = false;
            $scope.showcardselection = false;        
            $scope.mc_payment_method = 'CC';
            $scope.shownewcard = true;
            $scope.total_processing_fee = 0;
            $scope.total_misc_charge = 0;        
            $scope.student_name = [];
            // CLEAR STRIPE FORM
            if ($scope.stripe_card) {
                $scope.stripe_card.clear();
            }
            $('.form-group').removeClass('focused');
            // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
            $('.ph-text').css('display','block');
        };
         
                
         $scope.catchmsCheck = function(){
             $scope.mc_check_number = this.mc_check_number;
         };
         
         $scope.catchSearchMember = function(){
             if($scope.mc_search_member){
                if ($scope.studentlist && $scope.studentlist.length > 0) {
                    for (var i = 0; i < $scope.studentlist.length; i++) { 
                        if ($scope.mc_search_member === $scope.studentlist[i].student_name){
                            $scope.mc_selected_student_id = $scope.studentlist[i].student_id;
                            $scope.mc_buyer_First_Name = $scope.studentlist[i].buyer_first_name;
                            $scope.mc_buyer_Last_Name = $scope.studentlist[i].buyer_last_name;
                            $scope.mc_buyer_Phone = $scope.studentlist[i].buyer_phone;
                            $scope.mc_buyer_Email = $scope.studentlist[i].buyer_email;
                            $(".form-group > input.ng-valid").parents('.form-group').addClass('focused');
                            //For credit card
                            if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                $scope.mccarddetails = $scope.studentlist[i].card_details;
                            }else{
                                $scope.mccarddetails = [];
                            } 
                            if($scope.mc_payment_method === 'CC'){  //kamal
                               if ($scope.studentlist[i].card_details && $scope.studentlist[i].card_details.length > 0){
                                    $scope.showcardselection = true;
                                    $scope.shownewcard = false; 
                                } else{
                                    $scope.showcardselection = false;
                                    $scope.shownewcard = true; 
                                }
                            }else{
                                $scope.showcardselection = false;
                                $scope.shownewcard = false; 
                            }
                        }
                    }
                }else{
                    $scope.shownewcard = true; 
                }
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','none');
            }else{
                $scope.mc_selected_student_id = '';
                $scope.mc_buyer_First_Name = '';
                $scope.mc_buyer_Last_Name = '';
                $scope.mc_buyer_Phone = '';
                $scope.mc_buyer_Email = ''; 
                $scope.mc_cc_name = '';
                $scope.mc_cc_number = '';
                $scope.mc_cc_expirymonth = '';
                $scope.mc_cc_expiryyear = '';
                $scope.mc_cc_cvv = '';
                $scope.mc_cc_country = '';
                $scope.mc_cc_zipcode = '';
                $(".form-group > input.ng-valid").parents('.form-group').removeClass('focused');
                $scope.showcardselection = false;
                $scope.shownewcard = true; 
                $scope.mccarddetails = [];
                // SHOW / HIDE SEARCH MEMBER PLACEHOLDER
                $('.ph-text').css('display','block');
            }
         };
         
         $scope.chargeOnChange = function(amount){
             if($scope.mc_payment_method === 'CC' && ($scope.mc_processing_fee_type === 2 || $scope.mc_processing_fee_type === '2')){
                var process_fee_per = $scope.processing_fee_percentage;                
                var process_fee_val = $scope.processing_fee_transaction;
                
                if(parseFloat(amount)>0){
                    var process_fee1 = +(parseFloat(amount)) + +((parseFloat(amount) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    var process_fee = +((parseFloat(process_fee1) * parseFloat(process_fee_per))/100) + +parseFloat(process_fee_val);
                    $scope.total_processing_fee = parseFloat(Math.round((+process_fee + +0.00001) * 100) / 100).toFixed(2);
                    $scope.total_misc_charge = +parseFloat(amount) + +$scope.total_processing_fee;
                }else{
                    $scope.total_processing_fee = 0;
                    $scope.total_misc_charge = 0;
                }                
            }else{
                if(amount){
                    $scope.total_misc_charge = amount;
                }else{
                    $scope.total_misc_charge = 0;
                }
                $scope.total_processing_fee = 0;
            }
         };
         
         // RESET PAYMENT METHOD SELECTION         
         $scope.selectPaymentMethod = function () { //kamal
            $scope.mc_check_number = '';   
            $scope.selectedcardindex = '';
            $scope.mc_credit_card_id = '';
            $scope.mc_credit_card_status = '';
            $scope.mc_cc_name = '';
            $scope.mc_cc_number = '';
            $scope.mc_cc_expirymonth = '';
            $scope.mc_cc_expiryyear = '';
            $scope.mc_cc_cvv = '';
            $scope.mc_cc_country = '';
            $scope.mc_cc_zipcode = '';
            if($scope.mc_payment_method === 'CC'){  //kamal
               if ($scope.mccarddetails && $scope.mccarddetails.length > 0){
                    $scope.showcardselection = true;
                    $scope.shownewcard = false; 
                } else{
                    $scope.showcardselection = false;
                    $scope.shownewcard = true; 
                }
            }else{
                $scope.showcardselection = false;
                $scope.shownewcard = false; 
            }
            $scope.chargeOnChange($scope.misc_charge);
        };
         
         //For selecting existing card of a user
        $scope.selectMCCardDetail = function (ind) {
            if ($scope.selectedcardindex === 'Add New credit card') {
                $scope.shownewcard = true;
                $scope.mc_credit_card_id = '';
                $scope.mc_credit_card_status = '';
                $scope.mc_cc_number = '';
                $scope.mc_cc_cvv = '';
                $scope.mc_cc_name = '';
                $scope.mc_cc_expirymonth = '';
                $scope.mc_cc_expiryyear = '';
                $scope.mc_cc_country = '';
                $scope.mc_cc_zipcode = '';
                $("#mc_cc_name").parents('.form-group').removeClass('focused');
                $("#mc_cc_number").parents('.form-group').removeClass('focused');
                $("#mc_cc_zipcode").parents('.form-group').removeClass('focused');
                $("#mc_cc_cvv").parents('.form-group').removeClass('focused');
            } else {
                $scope.shownewcard = false;
                if($scope.stripe_enabled) {
                    $scope.payment_method_id = $scope.mccarddetails[ind].payment_method_id;
                    $scope.stripe_customer_id = $scope.mccarddetails[ind].stripe_customer_id;
                } else {
                    $scope.mc_credit_card_id = parseInt($scope.mccarddetails[ind].credit_card_id);
                    $scope.mc_credit_card_status = $scope.mccarddetails[ind].credit_card_status;
                    $scope.mc_cc_name = $scope.mccarddetails[ind].credit_card_name;
                    $scope.mc_cc_expirymonth = $scope.mccarddetails[ind].credit_card_expiration_month;
                    $scope.mc_cc_expiryyear = $scope.mccarddetails[ind].credit_card_expiration_year;
                    $scope.mc_cc_country = $scope.mccarddetails[ind].credit_card_countryr;
                    $scope.mc_cc_zipcode = $scope.mccarddetails[ind].postal_code;
                }
            }
        };
        
        $scope.openevents = function (eventurl) {
               MyApp.closePanel();
            if ($rootScope.online != "online") {
                $scope.istatus();
                return;
            }
            if(eventurl ==='' && eventurl === undefined && eventurl === null){
                return false;
            }else if(eventurl !=='' && eventurl !== undefined && eventurl !== null){
                window.open(eventurl, '_blank');            
            }

        };
    
        $scope.formatDate = function (dat) {
            var date = dat.split("-").join("/");
            var dateOut = new Date(date);
            return dateOut;
        };
        $scope.backToPOS = function () {
            var hosturl;
            if (window.location.hostname === 'www.mystudio.academy'|| window.location.hostname === 'www1.mystudio.academy' ||
               window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') {
                if ((window.location.hostname.indexOf("www") > -1 || window.location.hostname === 'www1.mystudio.academy' ||
                       window.location.hostname === 'www2.mystudio.academy' || window.location.hostname === 'prod.mystudio.academy') && window.location.protocol === 'https:') {
                    var origin = window.location.origin;
                    hosturl = origin;
                }

            } else if (window.location.hostname === 'dev.mystudio.academy' || window.location.hostname === 'dev2.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            }  else if (window.location.hostname === 'beta.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'https://' + www_url;
                }
            }   else if (window.location.hostname === 'stage.mystudio.academy') {
                if (window.location.hostname.indexOf("www") > -1) {
                    hosturl = window.location.href.split('/').slice(0, 3).join('/');
                } else {
                    var www_url = window.location.href.split('/').slice(2, 3).join('/');
                    hosturl = 'http://' + www_url;
                }
            } else if (window.location.hostname === 'localhost') {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            } else {
                hosturl = 'http://' + window.location.hostname + '/nila/Mystudio_webapp';
            }
            var pos_type, pos_string, pos_entry_type;
            if ($scope.pos_user_type === 'S'){
                pos_type = 'spos';
                pos_string = 'e92213e6721525d7spos712627e6db9b98b4';
            }else if($scope.pos_user_type === 'P'){
                pos_type = 'ppos';
                pos_string = '8f2f44c8580cc72ppos700ff5d197b5a18f0';
            }else{
                pos_type = '';
                pos_string = '';
            }
            if($scope.pos_entry_type){
                pos_entry_type = $scope.pos_entry_type;
            }else{
                pos_entry_type = 'l';
            }
            if (pos_type) {
                $window.open(hosturl+'/' + 'pos' + '/?=' + $scope.param1 + '/' + $scope.param2 + '/' + pos_type + '-' + pos_string + '/'+pos_entry_type+'/', '_self');
            }
        };
        
        $scope.handleCheckout = function(){
            if($scope.stripe_enabled){
                $scope.mcStripeCheckout();
            }else{
                $scope.mcCheckout('','', $scope.mc_buyer_First_Name, $scope.mc_buyer_Last_Name, $scope.mc_buyer_Email, $scope.mc_buyer_Phone, $scope.mc_cc_zipcode, $scope.mc_cc_country);
            }
        };
        
        //DOCUMENT READY FUNCTION
        angular.element(document).ready(function () {
            // NEW ANIMATION FORM PLACEHOLDER TO LABELS
            $('input').focus(function () {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input').blur(function () {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).addClass('filled');
                }
            });
            ($(window).width() < 641) ? $scope.mobiledevice = true : $scope.mobiledevice = false;
            $scope.urlredirection();
        });
    }]);

MyApp.angular.filter('date2', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function (input, format) {
        console.clear();
        console.log(input, format);
        var dtfilter = $filter('date')(input, format);
        if (dtfilter != undefined) {
            var day = parseInt($filter('date')(input, 'dd'));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            console.log(day, relevantDigits);
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            return dtfilter;
            //return dtfilter+suffix;
        }
    };
});

MyApp.angular.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);
MyApp.angular.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
MyApp.angular.directive('compileTemplate', function($compile, $parse){
    return {
          link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
          scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
});
//compile-template

//valid number and only 2 digit after decimal
MyApp.angular.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');

                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});

